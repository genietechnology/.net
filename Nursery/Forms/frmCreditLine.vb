﻿Imports Care.Global

Public Class frmCreditLine

    Private m_Credit As Business.Credit
    Private m_LineID As Guid?
    Private m_Child As Business.Child = Nothing
    Private m_NewLine As Boolean = False

    Public Sub New(ByVal CreditObject As Business.Credit, ByVal LineID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Credit = CreditObject
        m_LineID = LineID

        If Not LineID.HasValue Then m_NewLine = True

    End Sub

    Private Sub frmCreditLine_Load(sender As Object, e As EventArgs) Handles Me.Load

        lblDayName.Text = ""

        lblRef1.Hide()
        lblRef2.Hide()
        lblRef3.Hide()

        With cbxNLCode
            .AllowBlank = True
            .PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Codes"))
        End With

        With cbxNLTracking
            .AllowBlank = True
            .PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Tracking"))
        End With

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls, Care.Shared.ControlHandler.DebugMode.None)

        DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        Dim _Title As String = ""

        If m_NewLine Then
            _Title = "Add New"
        Else
            _Title = "Edit"
        End If

        _Title += " Line on Create Note: " + m_Credit._CreditNo.ToString

        If m_Credit._ChildId.HasValue Then
            m_Child = Business.Child.RetreiveByID(m_Credit._ChildId.Value)
            _Title += " (" + m_Credit._ChildName + ")"
        End If

        Me.Text = _Title

        If Not m_NewLine Then

            Dim _Line As Business.CreditLine = Business.CreditLine.RetreiveByID(m_LineID.Value)

            txtLineNo.Text = _Line._LineNo.ToString

            cdtLineActionDate.Value = _Line._ActionDate.Value
            lblDayName.Text = _Line._ActionDate.Value.DayOfWeek.ToString

            txtLineDescription.Text = _Line._Description

            cbxNLCode.Text = _Line._NlCode
            cbxNLTracking.Text = _Line._NlTracking

            txtLineValue.Text = Format(_Line._Value, "0.00")

            lblRef1.Text = _Line._Ref1
            lblRef2.Text = _Line._Ref2
            lblRef3.Text = _Line._Ref3

        End If

    End Sub

    Private Function SaveLine() As Boolean

        If Not MyControls.Validate(Me.Controls) Then Return False

        Dim _Value As Decimal = ValueHandler.ConvertDecimal(txtLineValue.Text)
        If _Value >= 0 Then
            CareMessage("Credit Lines must be negative in value. e.g. -200.00", MessageBoxIcon.Exclamation, "Credit Note Value")
            Return False
        End If

        'if tracking is on, check we have selected an item
        If FinanceShared.TrackingCategory <> "" Then
            If cbxNLTracking.Text = "" Then
                CareMessage("Please select a NL Tracking item.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        Dim _Line As Business.CreditLine

        If m_NewLine Then
            _Line = New Business.CreditLine
            _Line._CreditId = m_Credit._ID
        Else
            _Line = Business.CreditLine.RetreiveByID(m_LineID.Value)
        End If

        _Line._LineNo = Integer.Parse(txtLineNo.Text)
        _Line._ActionDate = cdtLineActionDate.Value
        _Line._Description = txtLineDescription.Text

        _Line._NlCode = cbxNLCode.Text
        _Line._NlTracking = cbxNLTracking.Text

        _Line._Value = Decimal.Parse(txtLineValue.Text)

        _Line.Store()
        _Line = Nothing

        Return True

    End Function

    Private Sub btnLineOK_Click(sender As Object, e As EventArgs) Handles btnLineOK.Click
        If SaveLine() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub btnLineCancel_Click(sender As Object, e As EventArgs) Handles btnLineCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cdtLineActionDate_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles cdtLineActionDate.Validating
        If cdtLineActionDate.Value.HasValue Then
            lblDayName.Text = cdtLineActionDate.Value.Value.DayOfWeek.ToString
        Else
            lblDayName.Text = ""
        End If
    End Sub

    Private Sub gbxLine_DoubleClick(sender As Object, e As EventArgs) Handles gbxLine.DoubleClick
        lblRef1.Show()
        lblRef2.Show()
        lblRef3.Show()
    End Sub

End Class