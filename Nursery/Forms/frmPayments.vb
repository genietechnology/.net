﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmPayments

    Dim m_Loading As Boolean = True
    Dim m_Payments As New List(Of Business.Payment)

    Private Sub frmTransactions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        gbxNew.Hide()

        calDate.Value = Today

        With cbxMethod
            .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, name from PaymentMethods order by name").Rows
            .ValueMember = "ID"
            .DisplayMember = "Name"
            .SelectedIndex = 0
        End With

        m_Loading = False

    End Sub

    Private Sub EnterTransaction(ByVal RowIndex As Integer)

        txtFamily.Tag = ""
        txtFamily.Text = ""

        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)

        If RowIndex = -1 Then
            If FindFamily() = False Then Exit Sub
        Else
            SetFields()
        End If

        gbxNew.Show()
        txtValue.Focus()

    End Sub

    Private Sub SetFields()




    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        EnterTransaction(-1)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gbxNew.Hide()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        FindFamily()
    End Sub

    Private Function FindFamily() As Boolean

        Dim _ReturnValue As String = Business.Family.FindFamily(Me)

        If _ReturnValue <> "" Then

            txtFamily.Tag = _ReturnValue
            Dim _Family As Business.Family = Business.Family.RetreiveByID(New Guid(txtFamily.Tag.ToString))
            txtFamily.Text = _Family._Surname
            txtBalance.Text = Format(_Family._Balance, "0.00")

            _Family = Nothing

            Return True

        Else
            Return False
        End If

    End Function

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        Dim _Payment As New Business.Payment
        With _Payment
            ._FamilyId = New Guid(txtFamily.Tag.ToString)
            ._PayDate = Date.Parse(calDate.Text)
            ._PayMethod = New Guid(cbxMethod.SelectedValue.ToString)
            ._MethodName = cbxMethod.Text
            ._PayAmount = Decimal.Parse(txtValue.Text)
            ._StampUser = Session.CurrentUser.UserCode
            ._Stamp = Date.Now
        End With

        m_Payments.Add(_Payment)

        _Payment = Nothing

        ugPayments.Populate(m_Payments)

        ugPayments.Columns("_ID").Visible = False
        ugPayments.Columns("_FamilyId").Visible = False
        ugPayments.Columns("_PayDate").Caption = "Payment Date"
        ugPayments.Columns("_PayMethod").Visible = False
        ugPayments.Columns("_MethodName").Caption = "Payment Method"
        ugPayments.Columns("_Ref1").Caption = "Ref 1"
        ugPayments.Columns("_Ref2").Caption = "Ref 2"
        ugPayments.Columns("_Ref3").Caption = "Ref 3"
        ugPayments.Columns("_PayAmount").Caption = "Value"
        ugPayments.Columns("_StampUser").Visible = False
        ugPayments.Columns("_Stamp").Visible = False
        ugPayments.Columns("IsNew").Visible = False
        ugPayments.Columns("IsDeleted").Visible = False

        gbxNew.Hide()

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        For Each Pmnt In m_Payments
            Business.Payment.SavePayment(Pmnt)
            Business.InvoicingEngine.UpdateCurrentStatement(Pmnt._FamilyId.Value)
        Next

        m_Payments.Clear()
        ugPayments.Clear()

    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click





    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click
        EnterTransaction(ugPayments.RowIndex)
    End Sub

    Private Sub ugPayments_GridDoubleClick(sender As Object, e As System.EventArgs) Handles ugPayments.GridDoubleClick
        EnterTransaction(ugPayments.RowIndex)
    End Sub

End Class
