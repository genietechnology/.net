﻿Imports Care.Global
Imports Care.Data

Public Class frmBookingsBuild

    Private Sub btnBookings_Click(sender As Object, e As EventArgs) Handles btnBookings.Click

        btnBookings.Enabled = False
        Session.CursorWaiting()

        Dim _Eng As New BookingsEngine
        _Eng.BuildBookingsForEveryone()

        btnBookings.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub btnSpaces_Click(sender As Object, e As EventArgs) Handles btnSpaces.Click

        btnSpaces.Enabled = False
        Session.CursorWaiting()

        DAL.ExecuteSQL(Session.ConnectionString, "truncate table Spaces")

        Dim _Eng As New SpaceEngine
        _Eng.InitialiseSpaces()

        btnSpaces.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub frmBookingsBuild_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim _SQL As String = ""
        _SQL += "select top 20 last_run as 'Last Run',"
        _SQL += " CASE WHEN last_result = 0 THEN 'Successful' ELSE 'Failed' END as 'Status'"
        _SQL += " from AppTasks t"
        _SQL += " where command = 'BuildBookingsForEveryone'"
        _SQL +=" order by last_run desc"

        grdTasks.Populate(Session.ConnectionString, _SQL)

    End Sub

End Class