﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffGlobalPriceChange
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxMatrix = New Care.Controls.CareFrame(Me.components)
        Me.chkBoltOns = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMonthly = New Care.Controls.CareCheckBox(Me.components)
        Me.chkVariable = New Care.Controls.CareCheckBox(Me.components)
        Me.chkAgeMatrix = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.chkStandard = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxRound = New Care.Controls.CareComboBox(Me.components)
        Me.lblValue = New Care.Controls.CareLabel(Me.components)
        Me.txtIncrease = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.cbxMethod = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        CType(Me.gbxMatrix, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMatrix.SuspendLayout()
        CType(Me.chkBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMonthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkVariable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAgeMatrix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkStandard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRound.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIncrease.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.cbxMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxMatrix
        '
        Me.gbxMatrix.Controls.Add(Me.CareLabel7)
        Me.gbxMatrix.Controls.Add(Me.cbxMethod)
        Me.gbxMatrix.Controls.Add(Me.cbxRound)
        Me.gbxMatrix.Controls.Add(Me.lblValue)
        Me.gbxMatrix.Controls.Add(Me.txtIncrease)
        Me.gbxMatrix.Controls.Add(Me.CareLabel1)
        Me.gbxMatrix.Location = New System.Drawing.Point(12, 11)
        Me.gbxMatrix.Name = "gbxMatrix"
        Me.gbxMatrix.Size = New System.Drawing.Size(290, 115)
        Me.gbxMatrix.TabIndex = 0
        Me.gbxMatrix.Text = "Enter Price Change Parameters"
        '
        'chkBoltOns
        '
        Me.chkBoltOns.EnterMoveNextControl = True
        Me.chkBoltOns.Location = New System.Drawing.Point(137, 129)
        Me.chkBoltOns.Name = "chkBoltOns"
        Me.chkBoltOns.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBoltOns.Properties.Appearance.Options.UseFont = True
        Me.chkBoltOns.Size = New System.Drawing.Size(20, 19)
        Me.chkBoltOns.TabIndex = 9
        Me.chkBoltOns.Tag = "A"
        '
        'chkMonthly
        '
        Me.chkMonthly.EnterMoveNextControl = True
        Me.chkMonthly.Location = New System.Drawing.Point(137, 104)
        Me.chkMonthly.Name = "chkMonthly"
        Me.chkMonthly.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMonthly.Properties.Appearance.Options.UseFont = True
        Me.chkMonthly.Size = New System.Drawing.Size(20, 19)
        Me.chkMonthly.TabIndex = 7
        Me.chkMonthly.Tag = "A"
        '
        'chkVariable
        '
        Me.chkVariable.EnterMoveNextControl = True
        Me.chkVariable.Location = New System.Drawing.Point(137, 79)
        Me.chkVariable.Name = "chkVariable"
        Me.chkVariable.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVariable.Properties.Appearance.Options.UseFont = True
        Me.chkVariable.Size = New System.Drawing.Size(20, 19)
        Me.chkVariable.TabIndex = 5
        Me.chkVariable.Tag = "A"
        '
        'chkAgeMatrix
        '
        Me.chkAgeMatrix.EnterMoveNextControl = True
        Me.chkAgeMatrix.Location = New System.Drawing.Point(137, 54)
        Me.chkAgeMatrix.Name = "chkAgeMatrix"
        Me.chkAgeMatrix.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAgeMatrix.Properties.Appearance.Options.UseFont = True
        Me.chkAgeMatrix.Size = New System.Drawing.Size(20, 19)
        Me.chkAgeMatrix.TabIndex = 3
        Me.chkAgeMatrix.Tag = "A"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 131)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel6.TabIndex = 8
        Me.CareLabel6.Text = "Bolt Ons"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 81)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Variable Rate Tariffs"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 106)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(112, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Monthly Matrix Rates"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 56)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Age Matrix Rates"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Standard Tariff Rate"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkStandard
        '
        Me.chkStandard.EnterMoveNextControl = True
        Me.chkStandard.Location = New System.Drawing.Point(137, 29)
        Me.chkStandard.Name = "chkStandard"
        Me.chkStandard.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStandard.Properties.Appearance.Options.UseFont = True
        Me.chkStandard.Size = New System.Drawing.Size(20, 19)
        Me.chkStandard.TabIndex = 1
        Me.chkStandard.Tag = "A"
        '
        'cbxRound
        '
        Me.cbxRound.AllowBlank = False
        Me.cbxRound.DataSource = Nothing
        Me.cbxRound.DisplayMember = Nothing
        Me.cbxRound.EnterMoveNextControl = True
        Me.cbxRound.Location = New System.Drawing.Point(137, 84)
        Me.cbxRound.Name = "cbxRound"
        Me.cbxRound.Properties.AccessibleName = "Group"
        Me.cbxRound.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRound.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRound.Properties.Appearance.Options.UseFont = True
        Me.cbxRound.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRound.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRound.SelectedValue = Nothing
        Me.cbxRound.Size = New System.Drawing.Size(141, 22)
        Me.cbxRound.TabIndex = 5
        Me.cbxRound.Tag = ""
        Me.cbxRound.ValueMember = Nothing
        '
        'lblValue
        '
        Me.lblValue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblValue.Location = New System.Drawing.Point(10, 59)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(90, 15)
        Me.lblValue.TabIndex = 2
        Me.lblValue.Text = "Increase Amount"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIncrease
        '
        Me.txtIncrease.CharacterCasing = CharacterCasing.Normal
        Me.txtIncrease.EnterMoveNextControl = True
        Me.txtIncrease.Location = New System.Drawing.Point(137, 56)
        Me.txtIncrease.MaxLength = 5
        Me.txtIncrease.Name = "txtIncrease"
        Me.txtIncrease.NumericAllowNegatives = False
        Me.txtIncrease.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtIncrease.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtIncrease.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtIncrease.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtIncrease.Properties.Appearance.Options.UseFont = True
        Me.txtIncrease.Properties.Appearance.Options.UseTextOptions = True
        Me.txtIncrease.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtIncrease.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtIncrease.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtIncrease.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtIncrease.Properties.MaxLength = 5
        Me.txtIncrease.Size = New System.Drawing.Size(57, 22)
        Me.txtIncrease.TabIndex = 3
        Me.txtIncrease.Tag = ""
        Me.txtIncrease.TextAlign = HorizontalAlignment.Left
        Me.txtIncrease.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 87)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Round to nearest"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(227, 295)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Cancel"
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(146, 295)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 2
        Me.btnRun.Text = "Run"
        '
        'CareFrame1
        '
        Me.CareFrame1.Controls.Add(Me.chkBoltOns)
        Me.CareFrame1.Controls.Add(Me.chkMonthly)
        Me.CareFrame1.Controls.Add(Me.chkVariable)
        Me.CareFrame1.Controls.Add(Me.chkAgeMatrix)
        Me.CareFrame1.Controls.Add(Me.CareLabel6)
        Me.CareFrame1.Controls.Add(Me.CareLabel2)
        Me.CareFrame1.Controls.Add(Me.CareLabel5)
        Me.CareFrame1.Controls.Add(Me.chkStandard)
        Me.CareFrame1.Controls.Add(Me.CareLabel4)
        Me.CareFrame1.Controls.Add(Me.CareLabel3)
        Me.CareFrame1.Location = New System.Drawing.Point(12, 133)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(290, 156)
        Me.CareFrame1.TabIndex = 1
        Me.CareFrame1.Text = "Select Tariff types for price changes"
        '
        'cbxMethod
        '
        Me.cbxMethod.AllowBlank = False
        Me.cbxMethod.DataSource = Nothing
        Me.cbxMethod.DisplayMember = Nothing
        Me.cbxMethod.EnterMoveNextControl = True
        Me.cbxMethod.Location = New System.Drawing.Point(137, 28)
        Me.cbxMethod.Name = "cbxMethod"
        Me.cbxMethod.Properties.AccessibleName = "Group"
        Me.cbxMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMethod.SelectedValue = Nothing
        Me.cbxMethod.Size = New System.Drawing.Size(141, 22)
        Me.cbxMethod.TabIndex = 1
        Me.cbxMethod.Tag = ""
        Me.cbxMethod.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel7.TabIndex = 0
        Me.CareLabel7.Text = "Increase Method"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTariffGlobalPriceChange
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 325)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.gbxMatrix)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnRun)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTariffGlobalPriceChange"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Global Price Changes"
        CType(Me.gbxMatrix, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMatrix.ResumeLayout(False)
        Me.gbxMatrix.PerformLayout()
        CType(Me.chkBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMonthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkVariable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAgeMatrix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkStandard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRound.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIncrease.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        CType(Me.cbxMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxMatrix As Care.Controls.CareFrame
    Friend WithEvents lblValue As Care.Controls.CareLabel
    Friend WithEvents txtIncrease As Care.Controls.CareTextBox
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxRound As Care.Controls.CareComboBox
    Friend WithEvents chkBoltOns As Care.Controls.CareCheckBox
    Friend WithEvents chkMonthly As Care.Controls.CareCheckBox
    Friend WithEvents chkVariable As Care.Controls.CareCheckBox
    Friend WithEvents chkAgeMatrix As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkStandard As Care.Controls.CareCheckBox
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxMethod As Care.Controls.CareComboBox
End Class
