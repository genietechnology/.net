﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFood
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFood))
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtPack = New Care.Controls.CareTextBox(Me.components)
        Me.txtWeight = New Care.Controls.CareTextBox(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.chkBabyFood = New Care.Controls.CareCheckBox(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxManu = New Care.Controls.CareComboBox(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.txtQtyMin = New Care.Controls.CareTextBox(Me.components)
        Me.txtCostUnit = New Care.Controls.CareTextBox(Me.components)
        Me.txtQty = New Care.Controls.CareTextBox(Me.components)
        Me.txtCost = New Care.Controls.CareTextBox(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.Label6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox3 = New Care.Controls.CareFrame()
        Me.txtSalt = New Care.Controls.CareTextBox(Me.components)
        Me.txtFibre = New Care.Controls.CareTextBox(Me.components)
        Me.txtFat = New Care.Controls.CareTextBox(Me.components)
        Me.txtCarbs = New Care.Controls.CareTextBox(Me.components)
        Me.txtProtein = New Care.Controls.CareTextBox(Me.components)
        Me.txtNutKcal = New Care.Controls.CareTextBox(Me.components)
        Me.txtNutKJ = New Care.Controls.CareTextBox(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.Label15 = New Care.Controls.CareLabel(Me.components)
        Me.Label10 = New Care.Controls.CareLabel(Me.components)
        Me.Label11 = New Care.Controls.CareLabel(Me.components)
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox4 = New Care.Controls.CareFrame()
        Me.txtIngredients = New DevExpress.XtraEditors.MemoEdit()
        Me.btnLookup = New Care.Controls.CareButton(Me.components)
        Me.OpenFileDialog1 = New OpenFileDialog()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.chkSulphur = New Care.Controls.CareCheckBox(Me.components)
        Me.chkLupin = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMustard = New Care.Controls.CareCheckBox(Me.components)
        Me.chkCelery = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSoya = New Care.Controls.CareCheckBox(Me.components)
        Me.chkCereals = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSeasameSeeds = New Care.Controls.CareCheckBox(Me.components)
        Me.chkTreenuts = New Care.Controls.CareCheckBox(Me.components)
        Me.chkPeanuts = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMolluscs = New Care.Controls.CareCheckBox(Me.components)
        Me.chkCrustaceans = New Care.Controls.CareCheckBox(Me.components)
        Me.chkFish = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMilk = New Care.Controls.CareCheckBox(Me.components)
        Me.chkEggs = New Care.Controls.CareCheckBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtPack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBabyFood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxManu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtQtyMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCostUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtSalt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFibre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCarbs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProtein.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNutKcal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNutKJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtIngredients.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkSulphur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLupin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMustard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCelery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSoya.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCereals.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeasameSeeds.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTreenuts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPeanuts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMolluscs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCrustaceans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFish.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMilk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEggs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(755, 5)
        Me.btnCancel.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(664, 5)
        Me.btnOK.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnLookup)
        Me.Panel1.Location = New System.Drawing.Point(0, 575)
        Me.Panel1.Size = New System.Drawing.Size(854, 35)
        Me.Panel1.TabIndex = 5
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnLookup, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPack)
        Me.GroupBox1.Controls.Add(Me.txtWeight)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.chkBabyFood)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cbxManu)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxGroup)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(586, 154)
        Me.GroupBox1.TabIndex = 0
        '
        'txtPack
        '
        Me.txtPack.CharacterCasing = CharacterCasing.Normal
        Me.txtPack.EnterMoveNextControl = True
        Me.txtPack.Location = New System.Drawing.Point(469, 98)
        Me.txtPack.MaxLength = 0
        Me.txtPack.Name = "txtPack"
        Me.txtPack.NumericAllowNegatives = False
        Me.txtPack.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPack.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPack.Properties.Appearance.Options.UseFont = True
        Me.txtPack.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPack.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPack.ReadOnly = False
        Me.txtPack.Size = New System.Drawing.Size(100, 20)
        Me.txtPack.TabIndex = 4
        Me.txtPack.Tag = "AEN"
        Me.txtPack.TextAlign = HorizontalAlignment.Left
        Me.txtPack.ToolTipText = ""
        '
        'txtWeight
        '
        Me.txtWeight.CharacterCasing = CharacterCasing.Normal
        Me.txtWeight.EnterMoveNextControl = True
        Me.txtWeight.Location = New System.Drawing.Point(113, 98)
        Me.txtWeight.MaxLength = 0
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.NumericAllowNegatives = False
        Me.txtWeight.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtWeight.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWeight.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWeight.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtWeight.Properties.Appearance.Options.UseFont = True
        Me.txtWeight.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtWeight.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWeight.ReadOnly = False
        Me.txtWeight.Size = New System.Drawing.Size(100, 20)
        Me.txtWeight.TabIndex = 3
        Me.txtWeight.Tag = "AE"
        Me.txtWeight.TextAlign = HorizontalAlignment.Left
        Me.txtWeight.ToolTipText = ""
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(8, 129)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(64, 15)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Baby Food ?"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkBabyFood
        '
        Me.chkBabyFood.EnterMoveNextControl = True
        Me.chkBabyFood.Location = New System.Drawing.Point(111, 126)
        Me.chkBabyFood.Name = "chkBabyFood"
        Me.chkBabyFood.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkBabyFood.Properties.Appearance.Options.UseFont = True
        Me.chkBabyFood.Size = New System.Drawing.Size(20, 19)
        Me.chkBabyFood.TabIndex = 5
        Me.chkBabyFood.Tag = "AE"
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(399, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 15)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Pack Size"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(8, 101)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 15)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Weight"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(8, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Manufacturer"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Group"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxManu
        '
        Me.cbxManu.AllowBlank = False
        Me.cbxManu.DataSource = Nothing
        Me.cbxManu.DisplayMember = Nothing
        Me.cbxManu.EnterMoveNextControl = True
        Me.cbxManu.Location = New System.Drawing.Point(113, 70)
        Me.cbxManu.Name = "cbxManu"
        Me.cbxManu.Properties.AccessibleName = "Manufacturer"
        Me.cbxManu.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxManu.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxManu.Properties.Appearance.Options.UseFont = True
        Me.cbxManu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxManu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxManu.ReadOnly = False
        Me.cbxManu.SelectedValue = Nothing
        Me.cbxManu.Size = New System.Drawing.Size(456, 20)
        Me.cbxManu.TabIndex = 2
        Me.cbxManu.Tag = "AE"
        Me.cbxManu.ValueMember = Nothing
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(113, 14)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Description"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(456, 20)
        Me.txtName.TabIndex = 0
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Description"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(113, 42)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.ReadOnly = False
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(456, 20)
        Me.cbxGroup.TabIndex = 1
        Me.cbxGroup.Tag = "AEM"
        Me.cbxGroup.ValueMember = Nothing
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtQtyMin)
        Me.GroupBox2.Controls.Add(Me.txtCostUnit)
        Me.GroupBox2.Controls.Add(Me.txtQty)
        Me.GroupBox2.Controls.Add(Me.txtCost)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 225)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.ShowCaption = False
        Me.GroupBox2.Size = New System.Drawing.Size(586, 77)
        Me.GroupBox2.TabIndex = 1
        '
        'txtQtyMin
        '
        Me.txtQtyMin.CharacterCasing = CharacterCasing.Normal
        Me.txtQtyMin.EnterMoveNextControl = True
        Me.txtQtyMin.Location = New System.Drawing.Point(469, 42)
        Me.txtQtyMin.MaxLength = 0
        Me.txtQtyMin.Name = "txtQtyMin"
        Me.txtQtyMin.NumericAllowNegatives = False
        Me.txtQtyMin.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtQtyMin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQtyMin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQtyMin.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtQtyMin.Properties.Appearance.Options.UseFont = True
        Me.txtQtyMin.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQtyMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQtyMin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQtyMin.ReadOnly = False
        Me.txtQtyMin.Size = New System.Drawing.Size(100, 20)
        Me.txtQtyMin.TabIndex = 3
        Me.txtQtyMin.Tag = "AEN"
        Me.txtQtyMin.TextAlign = HorizontalAlignment.Left
        Me.txtQtyMin.ToolTipText = ""
        '
        'txtCostUnit
        '
        Me.txtCostUnit.CharacterCasing = CharacterCasing.Normal
        Me.txtCostUnit.EnterMoveNextControl = True
        Me.txtCostUnit.Location = New System.Drawing.Point(469, 14)
        Me.txtCostUnit.MaxLength = 0
        Me.txtCostUnit.Name = "txtCostUnit"
        Me.txtCostUnit.NumericAllowNegatives = False
        Me.txtCostUnit.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCostUnit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCostUnit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCostUnit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCostUnit.Properties.Appearance.Options.UseFont = True
        Me.txtCostUnit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCostUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCostUnit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCostUnit.ReadOnly = False
        Me.txtCostUnit.Size = New System.Drawing.Size(100, 20)
        Me.txtCostUnit.TabIndex = 2
        Me.txtCostUnit.Tag = "AE2DP"
        Me.txtCostUnit.TextAlign = HorizontalAlignment.Left
        Me.txtCostUnit.ToolTipText = ""
        '
        'txtQty
        '
        Me.txtQty.CharacterCasing = CharacterCasing.Normal
        Me.txtQty.EnterMoveNextControl = True
        Me.txtQty.Location = New System.Drawing.Point(113, 42)
        Me.txtQty.MaxLength = 0
        Me.txtQty.Name = "txtQty"
        Me.txtQty.NumericAllowNegatives = False
        Me.txtQty.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtQty.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQty.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtQty.Properties.Appearance.Options.UseFont = True
        Me.txtQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQty.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQty.ReadOnly = False
        Me.txtQty.Size = New System.Drawing.Size(100, 20)
        Me.txtQty.TabIndex = 1
        Me.txtQty.Tag = "AEN"
        Me.txtQty.TextAlign = HorizontalAlignment.Left
        Me.txtQty.ToolTipText = ""
        '
        'txtCost
        '
        Me.txtCost.CharacterCasing = CharacterCasing.Normal
        Me.txtCost.EnterMoveNextControl = True
        Me.txtCost.Location = New System.Drawing.Point(113, 14)
        Me.txtCost.MaxLength = 0
        Me.txtCost.Name = "txtCost"
        Me.txtCost.NumericAllowNegatives = False
        Me.txtCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCost.Properties.Appearance.Options.UseFont = True
        Me.txtCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCost.ReadOnly = False
        Me.txtCost.Size = New System.Drawing.Size(100, 20)
        Me.txtCost.TabIndex = 0
        Me.txtCost.Tag = "AE2DP"
        Me.txtCost.TextAlign = HorizontalAlignment.Left
        Me.txtCost.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(377, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 15)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Min Quantity"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(8, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 15)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Quantity"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(378, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 15)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Cost per Unit"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label6.Location = New System.Drawing.Point(8, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Cost"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtSalt)
        Me.GroupBox3.Controls.Add(Me.txtFibre)
        Me.GroupBox3.Controls.Add(Me.txtFat)
        Me.GroupBox3.Controls.Add(Me.txtCarbs)
        Me.GroupBox3.Controls.Add(Me.txtProtein)
        Me.GroupBox3.Controls.Add(Me.txtNutKcal)
        Me.GroupBox3.Controls.Add(Me.txtNutKJ)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 314)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(586, 123)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.Text = "Nutrition Information per 100g"
        '
        'txtSalt
        '
        Me.txtSalt.CharacterCasing = CharacterCasing.Normal
        Me.txtSalt.EnterMoveNextControl = True
        Me.txtSalt.Location = New System.Drawing.Point(509, 88)
        Me.txtSalt.MaxLength = 0
        Me.txtSalt.Name = "txtSalt"
        Me.txtSalt.NumericAllowNegatives = False
        Me.txtSalt.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSalt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSalt.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSalt.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSalt.Properties.Appearance.Options.UseFont = True
        Me.txtSalt.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSalt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSalt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSalt.ReadOnly = False
        Me.txtSalt.Size = New System.Drawing.Size(60, 20)
        Me.txtSalt.TabIndex = 6
        Me.txtSalt.Tag = "AE"
        Me.txtSalt.TextAlign = HorizontalAlignment.Left
        Me.txtSalt.ToolTipText = ""
        '
        'txtFibre
        '
        Me.txtFibre.CharacterCasing = CharacterCasing.Normal
        Me.txtFibre.EnterMoveNextControl = True
        Me.txtFibre.Location = New System.Drawing.Point(408, 88)
        Me.txtFibre.MaxLength = 0
        Me.txtFibre.Name = "txtFibre"
        Me.txtFibre.NumericAllowNegatives = False
        Me.txtFibre.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFibre.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFibre.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFibre.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFibre.Properties.Appearance.Options.UseFont = True
        Me.txtFibre.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFibre.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFibre.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFibre.ReadOnly = False
        Me.txtFibre.Size = New System.Drawing.Size(60, 20)
        Me.txtFibre.TabIndex = 5
        Me.txtFibre.Tag = "AE"
        Me.txtFibre.TextAlign = HorizontalAlignment.Left
        Me.txtFibre.ToolTipText = ""
        '
        'txtFat
        '
        Me.txtFat.CharacterCasing = CharacterCasing.Normal
        Me.txtFat.EnterMoveNextControl = True
        Me.txtFat.Location = New System.Drawing.Point(311, 88)
        Me.txtFat.MaxLength = 0
        Me.txtFat.Name = "txtFat"
        Me.txtFat.NumericAllowNegatives = False
        Me.txtFat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFat.Properties.Appearance.Options.UseFont = True
        Me.txtFat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFat.ReadOnly = False
        Me.txtFat.Size = New System.Drawing.Size(60, 20)
        Me.txtFat.TabIndex = 4
        Me.txtFat.Tag = "AE"
        Me.txtFat.TextAlign = HorizontalAlignment.Left
        Me.txtFat.ToolTipText = ""
        '
        'txtCarbs
        '
        Me.txtCarbs.CharacterCasing = CharacterCasing.Normal
        Me.txtCarbs.EnterMoveNextControl = True
        Me.txtCarbs.Location = New System.Drawing.Point(208, 88)
        Me.txtCarbs.MaxLength = 0
        Me.txtCarbs.Name = "txtCarbs"
        Me.txtCarbs.NumericAllowNegatives = False
        Me.txtCarbs.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCarbs.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCarbs.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCarbs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCarbs.Properties.Appearance.Options.UseFont = True
        Me.txtCarbs.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCarbs.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCarbs.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCarbs.ReadOnly = False
        Me.txtCarbs.Size = New System.Drawing.Size(60, 20)
        Me.txtCarbs.TabIndex = 3
        Me.txtCarbs.Tag = "AE"
        Me.txtCarbs.TextAlign = HorizontalAlignment.Left
        Me.txtCarbs.ToolTipText = ""
        '
        'txtProtein
        '
        Me.txtProtein.CharacterCasing = CharacterCasing.Normal
        Me.txtProtein.EnterMoveNextControl = True
        Me.txtProtein.Location = New System.Drawing.Point(113, 88)
        Me.txtProtein.MaxLength = 0
        Me.txtProtein.Name = "txtProtein"
        Me.txtProtein.NumericAllowNegatives = False
        Me.txtProtein.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtProtein.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtProtein.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtProtein.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtProtein.Properties.Appearance.Options.UseFont = True
        Me.txtProtein.Properties.Appearance.Options.UseTextOptions = True
        Me.txtProtein.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtProtein.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtProtein.ReadOnly = False
        Me.txtProtein.Size = New System.Drawing.Size(60, 20)
        Me.txtProtein.TabIndex = 2
        Me.txtProtein.Tag = "AE"
        Me.txtProtein.TextAlign = HorizontalAlignment.Left
        Me.txtProtein.ToolTipText = ""
        '
        'txtNutKcal
        '
        Me.txtNutKcal.CharacterCasing = CharacterCasing.Normal
        Me.txtNutKcal.EnterMoveNextControl = True
        Me.txtNutKcal.Location = New System.Drawing.Point(311, 31)
        Me.txtNutKcal.MaxLength = 0
        Me.txtNutKcal.Name = "txtNutKcal"
        Me.txtNutKcal.NumericAllowNegatives = False
        Me.txtNutKcal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNutKcal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNutKcal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNutKcal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNutKcal.Properties.Appearance.Options.UseFont = True
        Me.txtNutKcal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNutKcal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNutKcal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNutKcal.ReadOnly = False
        Me.txtNutKcal.Size = New System.Drawing.Size(60, 20)
        Me.txtNutKcal.TabIndex = 1
        Me.txtNutKcal.Tag = "AE"
        Me.txtNutKcal.TextAlign = HorizontalAlignment.Left
        Me.txtNutKcal.ToolTipText = ""
        '
        'txtNutKJ
        '
        Me.txtNutKJ.CharacterCasing = CharacterCasing.Normal
        Me.txtNutKJ.EnterMoveNextControl = True
        Me.txtNutKJ.Location = New System.Drawing.Point(113, 31)
        Me.txtNutKJ.MaxLength = 0
        Me.txtNutKJ.Name = "txtNutKJ"
        Me.txtNutKJ.NumericAllowNegatives = False
        Me.txtNutKJ.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNutKJ.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNutKJ.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNutKJ.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNutKJ.Properties.Appearance.Options.UseFont = True
        Me.txtNutKJ.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNutKJ.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNutKJ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNutKJ.ReadOnly = False
        Me.txtNutKJ.Size = New System.Drawing.Size(60, 20)
        Me.txtNutKJ.TabIndex = 0
        Me.txtNutKJ.Tag = "AE"
        Me.txtNutKJ.TextAlign = HorizontalAlignment.Left
        Me.txtNutKJ.ToolTipText = ""
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(8, 91)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(77, 15)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Breakdown (g)"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(524, 67)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(19, 15)
        Me.Label17.TabIndex = 17
        Me.Label17.Text = "Salt"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(421, 67)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(26, 15)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Fibre"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label15.Location = New System.Drawing.Point(327, 67)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(16, 15)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "Fat"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label10.Location = New System.Drawing.Point(196, 67)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 15)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Carbohydrate"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label11.Location = New System.Drawing.Point(117, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 15)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Protein"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(237, 34)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 15)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Energy (kcal)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(8, 34)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(57, 15)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Energy (kJ)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtIngredients)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 445)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(831, 122)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.Text = "Ingredients"
        '
        'txtIngredients
        '
        Me.txtIngredients.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtIngredients.Location = New System.Drawing.Point(18, 31)
        Me.txtIngredients.Name = "txtIngredients"
        Me.txtIngredients.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIngredients.Properties.Appearance.Options.UseFont = True
        Me.txtIngredients.Properties.Appearance.Options.UseTextOptions = True
        Me.txtIngredients.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtIngredients.Properties.MaxLength = 4096
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtIngredients, True)
        Me.txtIngredients.Size = New System.Drawing.Size(796, 80)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtIngredients, OptionsSpelling1)
        Me.txtIngredients.TabIndex = 0
        Me.txtIngredients.Tag = "AE"
        Me.txtIngredients.UseOptimizedRendering = True
        '
        'btnLookup
        '
        Me.btnLookup.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnLookup.Appearance.Options.UseFont = True
        Me.btnLookup.CausesValidation = False
        Me.btnLookup.Image = CType(resources.GetObject("btnLookup.Image"), System.Drawing.Image)
        Me.btnLookup.Location = New System.Drawing.Point(12, 5)
        Me.btnLookup.Name = "btnLookup"
        Me.btnLookup.Size = New System.Drawing.Size(131, 25)
        Me.btnLookup.TabIndex = 0
        Me.btnLookup.Text = "Product Lookup"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.CareLabel13)
        Me.GroupControl1.Controls.Add(Me.CareLabel12)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.CareLabel10)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.chkSulphur)
        Me.GroupControl1.Controls.Add(Me.chkLupin)
        Me.GroupControl1.Controls.Add(Me.chkMustard)
        Me.GroupControl1.Controls.Add(Me.chkCelery)
        Me.GroupControl1.Controls.Add(Me.chkSoya)
        Me.GroupControl1.Controls.Add(Me.chkCereals)
        Me.GroupControl1.Controls.Add(Me.chkSeasameSeeds)
        Me.GroupControl1.Controls.Add(Me.chkTreenuts)
        Me.GroupControl1.Controls.Add(Me.chkPeanuts)
        Me.GroupControl1.Controls.Add(Me.chkMolluscs)
        Me.GroupControl1.Controls.Add(Me.chkCrustaceans)
        Me.GroupControl1.Controls.Add(Me.chkFish)
        Me.GroupControl1.Controls.Add(Me.chkMilk)
        Me.GroupControl1.Controls.Add(Me.chkEggs)
        Me.GroupControl1.Location = New System.Drawing.Point(604, 58)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(239, 379)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Allergen Control"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(34, 355)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(145, 15)
        Me.CareLabel14.TabIndex = 27
        Me.CareLabel14.Text = "Sulphur dioxide && sulphites"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel14.ToolTip = "(at concentration of more than ten parts per million)"
        Me.CareLabel14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel14.ToolTipTitle = "Sulphur dioxide and sulphites"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(34, 330)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel13.TabIndex = 25
        Me.CareLabel13.Text = "Lupin"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(34, 305)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel12.TabIndex = 23
        Me.CareLabel12.Text = "Mustard"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(34, 280)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel11.TabIndex = 21
        Me.CareLabel11.Text = "Celery"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(34, 255)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(25, 15)
        Me.CareLabel10.TabIndex = 19
        Me.CareLabel10.Text = "Soya"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(34, 230)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(135, 15)
        Me.CareLabel9.TabIndex = 17
        Me.CareLabel9.Text = "Cereals containing gluten"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel9.ToolTip = "e.g. wheat (such as spelt, Khorasan wheat/Kamut), rye, barley, oats, or their hyb" & _
    "ridised strains"
        Me.CareLabel9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel9.ToolTipTitle = "Cereals containing gluten"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(34, 205)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel8.TabIndex = 15
        Me.CareLabel8.Text = "Seasame Seeds"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(34, 180)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel7.TabIndex = 13
        Me.CareLabel7.Text = "Tree nuts"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel7.ToolTip = "e.g. almonds, hazelnuts, walnuts, cashews, pecans, brazils, pistachios, macadamia" & _
    " nuts or Queensland nuts"
        Me.CareLabel7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel7.ToolTipTitle = "Tree nuts"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(34, 155)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel6.TabIndex = 11
        Me.CareLabel6.Text = "Peanuts"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(34, 129)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel5.TabIndex = 9
        Me.CareLabel5.Text = "Molluscs"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel5.ToolTip = "e.g. mussels, oysters, squid"
        Me.CareLabel5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel5.ToolTipTitle = "Molluscs"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(34, 105)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel4.TabIndex = 7
        Me.CareLabel4.Text = "Crustaceans"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel4.ToolTip = "e.g. crab, lobster, crayfish, shrimp, prawn"
        Me.CareLabel4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel4.ToolTipTitle = "Crustaceans"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(34, 80)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel3.TabIndex = 5
        Me.CareLabel3.Text = "Fish"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(34, 55)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel2.TabIndex = 3
        Me.CareLabel2.Text = "Milk"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(34, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(25, 15)
        Me.CareLabel1.TabIndex = 1
        Me.CareLabel1.Text = "Eggs"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSulphur
        '
        Me.chkSulphur.EnterMoveNextControl = True
        Me.chkSulphur.Location = New System.Drawing.Point(8, 353)
        Me.chkSulphur.Name = "chkSulphur"
        Me.chkSulphur.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSulphur.Properties.Appearance.Options.UseFont = True
        Me.chkSulphur.Size = New System.Drawing.Size(20, 19)
        Me.chkSulphur.TabIndex = 26
        Me.chkSulphur.Tag = "AE"
        '
        'chkLupin
        '
        Me.chkLupin.EnterMoveNextControl = True
        Me.chkLupin.Location = New System.Drawing.Point(8, 328)
        Me.chkLupin.Name = "chkLupin"
        Me.chkLupin.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkLupin.Properties.Appearance.Options.UseFont = True
        Me.chkLupin.Size = New System.Drawing.Size(20, 19)
        Me.chkLupin.TabIndex = 24
        Me.chkLupin.Tag = "AE"
        '
        'chkMustard
        '
        Me.chkMustard.EnterMoveNextControl = True
        Me.chkMustard.Location = New System.Drawing.Point(8, 303)
        Me.chkMustard.Name = "chkMustard"
        Me.chkMustard.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMustard.Properties.Appearance.Options.UseFont = True
        Me.chkMustard.Size = New System.Drawing.Size(20, 19)
        Me.chkMustard.TabIndex = 22
        Me.chkMustard.Tag = "AE"
        '
        'chkCelery
        '
        Me.chkCelery.EnterMoveNextControl = True
        Me.chkCelery.Location = New System.Drawing.Point(8, 278)
        Me.chkCelery.Name = "chkCelery"
        Me.chkCelery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCelery.Properties.Appearance.Options.UseFont = True
        Me.chkCelery.Size = New System.Drawing.Size(20, 19)
        Me.chkCelery.TabIndex = 20
        Me.chkCelery.Tag = "AE"
        '
        'chkSoya
        '
        Me.chkSoya.EnterMoveNextControl = True
        Me.chkSoya.Location = New System.Drawing.Point(8, 253)
        Me.chkSoya.Name = "chkSoya"
        Me.chkSoya.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSoya.Properties.Appearance.Options.UseFont = True
        Me.chkSoya.Size = New System.Drawing.Size(20, 19)
        Me.chkSoya.TabIndex = 18
        Me.chkSoya.Tag = "AE"
        '
        'chkCereals
        '
        Me.chkCereals.EnterMoveNextControl = True
        Me.chkCereals.Location = New System.Drawing.Point(8, 228)
        Me.chkCereals.Name = "chkCereals"
        Me.chkCereals.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCereals.Properties.Appearance.Options.UseFont = True
        Me.chkCereals.Size = New System.Drawing.Size(20, 19)
        Me.chkCereals.TabIndex = 16
        Me.chkCereals.Tag = "AE"
        '
        'chkSeasameSeeds
        '
        Me.chkSeasameSeeds.EnterMoveNextControl = True
        Me.chkSeasameSeeds.Location = New System.Drawing.Point(8, 203)
        Me.chkSeasameSeeds.Name = "chkSeasameSeeds"
        Me.chkSeasameSeeds.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSeasameSeeds.Properties.Appearance.Options.UseFont = True
        Me.chkSeasameSeeds.Size = New System.Drawing.Size(20, 19)
        Me.chkSeasameSeeds.TabIndex = 14
        Me.chkSeasameSeeds.Tag = "AE"
        '
        'chkTreenuts
        '
        Me.chkTreenuts.EnterMoveNextControl = True
        Me.chkTreenuts.Location = New System.Drawing.Point(8, 178)
        Me.chkTreenuts.Name = "chkTreenuts"
        Me.chkTreenuts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTreenuts.Properties.Appearance.Options.UseFont = True
        Me.chkTreenuts.Size = New System.Drawing.Size(20, 19)
        Me.chkTreenuts.TabIndex = 12
        Me.chkTreenuts.Tag = "AE"
        '
        'chkPeanuts
        '
        Me.chkPeanuts.EnterMoveNextControl = True
        Me.chkPeanuts.Location = New System.Drawing.Point(8, 153)
        Me.chkPeanuts.Name = "chkPeanuts"
        Me.chkPeanuts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPeanuts.Properties.Appearance.Options.UseFont = True
        Me.chkPeanuts.Size = New System.Drawing.Size(20, 19)
        Me.chkPeanuts.TabIndex = 10
        Me.chkPeanuts.Tag = "AE"
        '
        'chkMolluscs
        '
        Me.chkMolluscs.EnterMoveNextControl = True
        Me.chkMolluscs.Location = New System.Drawing.Point(8, 128)
        Me.chkMolluscs.Name = "chkMolluscs"
        Me.chkMolluscs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMolluscs.Properties.Appearance.Options.UseFont = True
        Me.chkMolluscs.Size = New System.Drawing.Size(20, 19)
        Me.chkMolluscs.TabIndex = 8
        Me.chkMolluscs.Tag = "AE"
        '
        'chkCrustaceans
        '
        Me.chkCrustaceans.EnterMoveNextControl = True
        Me.chkCrustaceans.Location = New System.Drawing.Point(8, 103)
        Me.chkCrustaceans.Name = "chkCrustaceans"
        Me.chkCrustaceans.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCrustaceans.Properties.Appearance.Options.UseFont = True
        Me.chkCrustaceans.Size = New System.Drawing.Size(20, 19)
        Me.chkCrustaceans.TabIndex = 6
        Me.chkCrustaceans.Tag = "AE"
        '
        'chkFish
        '
        Me.chkFish.EnterMoveNextControl = True
        Me.chkFish.Location = New System.Drawing.Point(8, 78)
        Me.chkFish.Name = "chkFish"
        Me.chkFish.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFish.Properties.Appearance.Options.UseFont = True
        Me.chkFish.Size = New System.Drawing.Size(20, 19)
        Me.chkFish.TabIndex = 4
        Me.chkFish.Tag = "AE"
        '
        'chkMilk
        '
        Me.chkMilk.EnterMoveNextControl = True
        Me.chkMilk.Location = New System.Drawing.Point(8, 53)
        Me.chkMilk.Name = "chkMilk"
        Me.chkMilk.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMilk.Properties.Appearance.Options.UseFont = True
        Me.chkMilk.Size = New System.Drawing.Size(20, 19)
        Me.chkMilk.TabIndex = 2
        Me.chkMilk.Tag = "AE"
        '
        'chkEggs
        '
        Me.chkEggs.EnterMoveNextControl = True
        Me.chkEggs.Location = New System.Drawing.Point(8, 28)
        Me.chkEggs.Name = "chkEggs"
        Me.chkEggs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEggs.Properties.Appearance.Options.UseFont = True
        Me.chkEggs.Size = New System.Drawing.Size(20, 19)
        Me.chkEggs.TabIndex = 0
        Me.chkEggs.Tag = "AE"
        '
        'frmFood
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(854, 610)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MinimumSize = New System.Drawing.Size(544, 289)
        Me.Name = "frmFood"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtPack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBabyFood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxManu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtQtyMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCostUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.txtSalt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFibre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCarbs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProtein.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNutKcal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNutKJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtIngredients.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkSulphur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLupin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMustard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCelery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSoya.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCereals.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeasameSeeds.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTreenuts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPeanuts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMolluscs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCrustaceans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFish.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMilk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEggs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents cbxManu As Care.Controls.CareComboBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents Label6 As Care.Controls.CareLabel
    Friend WithEvents GroupBox3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents Label15 As Care.Controls.CareLabel
    Friend WithEvents Label10 As Care.Controls.CareLabel
    Friend WithEvents Label11 As Care.Controls.CareLabel
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Friend WithEvents GroupBox4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents chkBabyFood As Care.Controls.CareCheckBox
    Friend WithEvents txtPack As Care.Controls.CareTextBox
    Friend WithEvents txtWeight As Care.Controls.CareTextBox
    Friend WithEvents txtQtyMin As Care.Controls.CareTextBox
    Friend WithEvents txtCostUnit As Care.Controls.CareTextBox
    Friend WithEvents txtQty As Care.Controls.CareTextBox
    Friend WithEvents txtCost As Care.Controls.CareTextBox
    Friend WithEvents txtSalt As Care.Controls.CareTextBox
    Friend WithEvents txtFibre As Care.Controls.CareTextBox
    Friend WithEvents txtFat As Care.Controls.CareTextBox
    Friend WithEvents txtCarbs As Care.Controls.CareTextBox
    Friend WithEvents txtProtein As Care.Controls.CareTextBox
    Friend WithEvents txtNutKcal As Care.Controls.CareTextBox
    Friend WithEvents txtNutKJ As Care.Controls.CareTextBox
    Friend WithEvents txtIngredients As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnLookup As Care.Controls.CareButton
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkSulphur As Care.Controls.CareCheckBox
    Friend WithEvents chkLupin As Care.Controls.CareCheckBox
    Friend WithEvents chkMustard As Care.Controls.CareCheckBox
    Friend WithEvents chkCelery As Care.Controls.CareCheckBox
    Friend WithEvents chkSoya As Care.Controls.CareCheckBox
    Friend WithEvents chkCereals As Care.Controls.CareCheckBox
    Friend WithEvents chkSeasameSeeds As Care.Controls.CareCheckBox
    Friend WithEvents chkTreenuts As Care.Controls.CareCheckBox
    Friend WithEvents chkPeanuts As Care.Controls.CareCheckBox
    Friend WithEvents chkMolluscs As Care.Controls.CareCheckBox
    Friend WithEvents chkCrustaceans As Care.Controls.CareCheckBox
    Friend WithEvents chkFish As Care.Controls.CareCheckBox
    Friend WithEvents chkMilk As Care.Controls.CareCheckBox
    Friend WithEvents chkEggs As Care.Controls.CareCheckBox

End Class
