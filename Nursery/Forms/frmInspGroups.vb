﻿Public Class frmInspGroups

    Dim m_InspGroup As Business.InspGroup

    Private Sub frmGroups_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.GridSQL = "select id, name as 'Name', child_capacity as 'Capacity' from InspGroups order by months_min"
        gbx.Hide()

    End Sub

    Protected Overrides Sub FormatGrid()
        'ug.DisplayLayout.Bands(0).Columns(0).Hidden = True
    End Sub

    Protected Overrides Sub SetBindings()

        m_InspGroup = New Business.InspGroup
        bs.DataSource = m_InspGroup

        txtName.DataBindings.Add("Text", bs, "_name")
        txtMonthsMin.DataBindings.Add("Text", bs, "_monthsmin")
        txtMonthsMax.DataBindings.Add("Text", bs, "_monthsmax")
        txtChildCapacity.DataBindings.Add("Text", bs, "_childcapacity")
        txtStaffRatio.DataBindings.Add("Text", bs, "_staffratio")

    End Sub

    Protected Overrides Sub CommitUpdate()

        If txtMonthsMin.Text = "" Then txtMonthsMin.Text = "0"
        If txtMonthsMax.Text = "" Then txtMonthsMax.Text = "0"
        If txtChildCapacity.Text = "" Then txtChildCapacity.Text = "0"
        If txtStaffRatio.Text = "" Then txtStaffRatio.Text = "0"

        m_InspGroup.Store()

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_InspGroup = Business.InspGroup.RetreiveByID(ID)
        bs.DataSource = m_InspGroup
    End Sub

End Class
