﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.Spreadsheet

Public Class frmDataImport

    Private m_Records As New List(Of ImportRecord)

    Private m_Failed As Integer = 0
    Private m_Passed As Integer = 0

    Private m_Importing As Boolean = False
    Private m_ImportPassed As Integer = 0
    Private m_ImportFailed As Integer = 0

    Private m_FamilyID As Guid?
    Private m_ChildID As Guid?
    Private m_Address As String
    Private m_PostCode As String

    Private Sub frmDataImport_Load(sender As Object, e As EventArgs) Handles Me.Load

        ofd.Filter = "XLSX Files|*.xlsx"
        ofd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        cdtStartDate.EditValue = Nothing

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, "select id, name from Sites order by name")
        End With

        With cbxKeyworker
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, fullname from staff where keyworker = 1 order by fullname")
        End With

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
    End Sub

    Private Sub Preview()

        m_Records.Clear()
        m_Passed = 0
        m_Failed = 0

        Dim _wb As IWorkbook = sc.Document
        Dim _ws As Worksheet = _wb.Worksheets(0)

        Dim _Row As Integer = 3
        Dim _FirstCell As Cell = _ws.Columns("A")(_Row)

        While _FirstCell.DisplayText <> ""

            Dim _ChildForename As Cell = _ws.Columns("A")(_Row)
            Dim _ChildSurname As Cell = _ws.Columns("B")(_Row)
            Dim _ChildDOB As Cell = _ws.Columns("C")(_Row)
            Dim _ChildGender As Cell = _ws.Columns("D")(_Row)

            Dim _ChildGroup As Cell = _ws.Columns("E")(_Row)
            Dim _ChildKeyworker As Cell = _ws.Columns("F")(_Row)
            Dim _ChildTermTime As Cell = _ws.Columns("G")(_Row)
            Dim _ChildDiscount As Cell = _ws.Columns("H")(_Row)

            Dim _ChildMonday As Cell = _ws.Columns("I")(_Row)
            Dim _ChildTuesday As Cell = _ws.Columns("J")(_Row)
            Dim _ChildWednesday As Cell = _ws.Columns("K")(_Row)
            Dim _ChildThursday As Cell = _ws.Columns("L")(_Row)
            Dim _ChildFriday As Cell = _ws.Columns("M")(_Row)

            Dim _ChildAlergies As Cell = _ws.Columns("N")(_Row)
            Dim _ChildMedical As Cell = _ws.Columns("O")(_Row)
            Dim _ChildOther As Cell = _ws.Columns("P")(_Row)

            Dim _StartDate As Cell = _ws.Columns("Q")(_Row)
            Dim _LeavingDate As Cell = _ws.Columns("R")(_Row)

            Dim _ConOffsite As Cell = _ws.Columns("S")(_Row)
            Dim _ConOutdoor As Cell = _ws.Columns("T")(_Row)
            Dim _ConPhotos As Cell = _ws.Columns("U")(_Row)
            Dim _ConCalpol As Cell = _ws.Columns("V")(_Row)
            Dim _ConPlasters As Cell = _ws.Columns("W")(_Row)
            Dim _ConSuncream As Cell = _ws.Columns("X")(_Row)

            Dim _Address As Cell = _ws.Columns("Y")(_Row)
            Dim _PostCode As Cell = _ws.Columns("Z")(_Row)

            Dim _C1Forename As Cell = _ws.Columns("AA")(_Row)
            Dim _C1Surname As Cell = _ws.Columns("AB")(_Row)
            Dim _C1Relationship As Cell = _ws.Columns("AC")(_Row)
            Dim _C1Landline As Cell = _ws.Columns("AD")(_Row)
            Dim _C1Mobile As Cell = _ws.Columns("AE")(_Row)
            Dim _C1Email As Cell = _ws.Columns("AF")(_Row)
            Dim _C1Secret As Cell = _ws.Columns("AG")(_Row)

            Dim _C2Forename As Cell = _ws.Columns("AH")(_Row)
            Dim _C2Surname As Cell = _ws.Columns("AI")(_Row)
            Dim _C2Relationship As Cell = _ws.Columns("AJ")(_Row)
            Dim _C2Landline As Cell = _ws.Columns("AK")(_Row)
            Dim _C2Mobile As Cell = _ws.Columns("AL")(_Row)
            Dim _C2Email As Cell = _ws.Columns("AM")(_Row)
            Dim _C2Secret As Cell = _ws.Columns("AN")(_Row)

            Dim _C3Forename As Cell = _ws.Columns("AO")(_Row)
            Dim _C3Surname As Cell = _ws.Columns("AP")(_Row)
            Dim _C3Relationship As Cell = _ws.Columns("AQ")(_Row)
            Dim _C3Landline As Cell = _ws.Columns("AR")(_Row)
            Dim _C3Mobile As Cell = _ws.Columns("AS")(_Row)
            Dim _C3Email As Cell = _ws.Columns("AT")(_Row)
            Dim _C3Secret As Cell = _ws.Columns("AU")(_Row)

            Dim _C4Forename As Cell = _ws.Columns("AV")(_Row)
            Dim _C4Surname As Cell = _ws.Columns("AW")(_Row)
            Dim _C4Relationship As Cell = _ws.Columns("AY")(_Row)
            Dim _C4Landline As Cell = _ws.Columns("AY")(_Row)
            Dim _C4Mobile As Cell = _ws.Columns("AZ")(_Row)
            Dim _C4Email As Cell = _ws.Columns("BA")(_Row)
            Dim _C4Secret As Cell = _ws.Columns("BB")(_Row)

            Dim _GPName As Cell = _ws.Columns("BC")(_Row)
            Dim _GPTel As Cell = _ws.Columns("BD")(_Row)
            Dim _GPAddress As Cell = _ws.Columns("BE")(_Row)

            Dim _r As New ImportRecord
            With _r

                .ChildForename = ReturnString(_ChildForename)
                .ChildSurname = ReturnString(_ChildSurname)
                .ChildDOB = ReturnDate(_ChildDOB)
                .ChildGender = ReturnString(_ChildGender)

                .Group = ReturnOverrideValue(Of String)(ReturnString(_ChildGroup), cbxRoom.Text)
                .Keyworker = ReturnOverrideValue(Of String)(ReturnString(_ChildKeyworker), cbxKeyworker.Text)

                .TermTime = ReturnBoolean(_ChildTermTime)

                .Address = ReturnString(_Address)
                .PostCode = ReturnString(_PostCode)

                .StartDate = ReturnOverrideValue(Of Date?)(ReturnDate(_StartDate), cdtStartDate.Value)
                .LeavingDate = ReturnDate(_LeavingDate)

                .Monday = ReturnString(_ChildMonday)
                .Tuesday = ReturnString(_ChildTuesday)
                .Wednesday = ReturnString(_ChildWednesday)
                .Thursday = ReturnString(_ChildThursday)
                .Friday = ReturnString(_ChildFriday)

                .Alergies = ReturnString(_ChildAlergies)
                .MedicalNotes = ReturnString(_ChildMedical)
                .OtherNotes = ReturnString(_ChildOther)

                .GPName = ReturnString(_GPName)
                .GPTel = ReturnString(_GPTel)

                .ConOffsite = ReturnBoolean(_ConOffsite)
                .ConOutdoors = ReturnBoolean(_ConOutdoor)
                .ConPhotos = ReturnBoolean(_ConPhotos)
                .ConCalpol = ReturnBoolean(_ConCalpol)
                .ConPlasters = ReturnBoolean(_ConPlasters)
                .ConSuncream = ReturnBoolean(_ConSuncream)

                .C1Forename = ReturnString(_C1Forename)
                .C1Surname = ReturnString(_C1Surname)
                .C1Relationship = ReturnString(_C1Relationship)
                .C1Landline = ReturnString(_C1Landline)
                .C1Mobile = ReturnString(_C1Mobile)
                .C1Email = ReturnString(_C1Email)
                .C1Secret = ReturnString(_C1Secret)

                .C2Forename = ReturnString(_C2Forename)
                .C2Surname = ReturnString(_C2Surname)
                .C2Relationship = ReturnString(_C2Relationship)
                .C2Landline = ReturnString(_C2Landline)
                .C2Mobile = ReturnString(_C2Mobile)
                .C2Email = ReturnString(_C2Email)
                .C2Secret = ReturnString(_C2Secret)

                .C3Forename = ReturnString(_C3Forename)
                .C3Surname = ReturnString(_C3Surname)
                .C3Relationship = ReturnString(_C3Relationship)
                .C3Landline = ReturnString(_C3Landline)
                .C3Mobile = ReturnString(_C3Mobile)
                .C3Email = ReturnString(_C3Email)
                .C3Secret = ReturnString(_C3Secret)

                .C4Forename = ReturnString(_C4Forename)
                .C4Surname = ReturnString(_C4Surname)
                .C4Relationship = ReturnString(_C4Relationship)
                .C4Landline = ReturnString(_C4Landline)
                .C4Mobile = ReturnString(_C4Mobile)
                .C4Email = ReturnString(_C4Email)
                .C4Secret = ReturnString(_C4Secret)

                .Valid = ValidateRecord(_r)

                If .Valid Then
                    m_Passed += 1
                Else
                    m_Failed += 1
                End If

            End With

            m_Records.Add(_r)

            _Row += 1
            _FirstCell = _ws.Columns("A")(_Row)

        End While

    End Sub

    Private Function ReturnOverrideValue(Of T)(ByVal SpreadsheetValue As t, ByVal OverrideValue As t) As t
        If SpreadsheetValue.ToString = "" Then
            Return OverrideValue
        Else
            Return SpreadsheetValue
        End If
    End Function

    Private Function ValidateRoom(ByVal Room As String, ByRef RoomID As Guid?) As Boolean

        Dim _Return As Boolean = False

        Dim _SQL As String = ""
        _SQL += "select ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + cbxSite.SelectedValue.ToString + "'"
        _SQL += " and room_name = '" + Room + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            RoomID = New Guid(_DR.Item("ID").ToString)
            _Return = True
        End If

        Return _Return

    End Function

    Private Function ValidateRecord(ByRef RecordIn As ImportRecord) As Boolean

        Dim _Errors As Integer = 0

        If Not ValidateRoom(RecordIn.Group, Nothing) Then
            RecordIn.Group = "! " + RecordIn.Group
            _Errors += 1
        End If

        Dim _Keyworker As New IDandDescription("Staff", "fullname", RecordIn.Keyworker)
        If _Keyworker.Found Then
            Dim _S As Business.Staff = Business.Staff.RetreiveByID(_Keyworker.ID.Value)
            If _S._Keyworker = False Then
                RecordIn.Keyworker = "! " + RecordIn.Keyworker
                _Errors += 1
            End If
        Else
            _Errors += 1
        End If

        If RecordIn.Discount <> "" Then
            Dim _Discount As New IDandDescription("Discounts", "name", RecordIn.Discount)
            If Not _Discount.Found Then _Errors += 1
        End If

        If RecordIn.Monday <> "" Then
            Dim _Monday As New IDandDescription("Tariffs", "name", RecordIn.Monday)
            If Not _Monday.Found Then _Errors += 1
        End If

        If RecordIn.Tuesday <> "" Then
            Dim _Tuesday As New IDandDescription("Tariffs", "name", RecordIn.Tuesday)
            If Not _Tuesday.Found Then _Errors += 1
        End If

        If RecordIn.Wednesday <> "" Then
            Dim _Wednesday As New IDandDescription("Tariffs", "name", RecordIn.Wednesday)
            If Not _Wednesday.Found Then _Errors += 1
        End If

        If RecordIn.Thursday <> "" Then
            Dim _Thursday As New IDandDescription("Tariffs", "name", RecordIn.Thursday)
            If Not _Thursday.Found Then _Errors += 1
        End If

        If RecordIn.Friday <> "" Then
            Dim _Friday As New IDandDescription("Tariffs", "name", RecordIn.Friday)
            If Not _Friday.Found Then _Errors += 1
        End If

        If _Errors > 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub Wiz_CancelClick(sender As Object, e As ComponentModel.CancelEventArgs) Handles Wiz.CancelClick
        If m_Importing Then
            e.Cancel = True
        Else
            If Wiz.SelectedPage.Name = "wpWelcome" Then
                Me.Close()
            Else
                If CareMessage("Are you sure you want to Exit the Import Wizard?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Exit") = DialogResult.Yes Then
                    Me.Close()
                End If
            End If
        End If
    End Sub

    Private Sub Wiz_NextClick(sender As Object, e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles Wiz.NextClick

        Select Case e.Page.Name

            Case "wpWelcome"
                If Not SelectFile() Then e.Handled = True

            Case "wpSpreadsheet"
                'goto overrides page

            Case "wpDefaults"
                If cbxSite.Text = "" Then
                    CareMessage("Please select a Site.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    e.Handled = True
                Else
                    PreviewImport()
                End If

            Case "wpPreview"
                'goto preview page

        End Select

    End Sub

    Private Sub Import()

        m_Importing = True
        Session.SetupProgressBar("Importing Data...", m_Records.Count)

        m_ImportPassed = 0
        m_ImportFailed = 0

        For Each _r As ImportRecord In m_Records

            Dim _OK As Boolean = False
            m_FamilyID = Nothing
            m_ChildID = Nothing
            m_Address = ""
            m_PostCode = ""

            'create family
            If SaveFamily(_r) Then
                If SaveContacts(_r) Then
                    If SaveChild(_r) Then
                        If SaveSessions(_r) Then
                            m_ImportPassed += 1
                        Else
                            m_ImportFailed += 1
                        End If
                    Else
                        m_ImportFailed += 1
                    End If
                Else
                    m_ImportFailed += 1
                End If
            Else
                m_ImportFailed += 1
            End If

            Session.StepProgressBar()

        Next

        m_Importing = False

    End Sub

    Private Sub Memo(ByVal TextIn As String)
        memProgress.Text.Insert(0, TextIn)
        memProgress.Refresh()
    End Sub

    Private Function PreviewImport() As Boolean

        Preview()

        If m_Records.Count > 0 Then

            cgRecords.Populate(m_Records)
            FormatColumns()

            If m_Failed = 0 Then
                CareMessage("Valid Data :-)" + vbCrLf + vbCrLf + "Ready to Import!", MessageBoxIcon.Information, "Data Validation")
                Return True
            Else
                CareMessage(m_Failed.ToString + " record(s) failed validation :-(" + vbCrLf + vbCrLf + m_Passed.ToString + " record(s) passed validation :-)" + vbCrLf + vbCrLf + "Please review and correct accordingly.", MessageBoxIcon.Error, "Data Validation")
            End If

        Else
            CareMessage("No records available for Preview :-(" + vbCrLf + vbCrLf + "Please ensure you are using the Nursery Genie Import Spreadsheet.", MessageBoxIcon.Error, "Data Validation")
        End If

        Return False

    End Function

    Private Sub FormatColumns()

        'make all the columns invisible
        For Each _c As DevExpress.XtraGrid.Columns.GridColumn In cgRecords.Columns
            _c.Visible = False
        Next

        'show the ones we want (in descending order)

        cgRecords.Columns("C4Surname").Visible = True
        cgRecords.Columns("C4Forename").Visible = True

        cgRecords.Columns("C3Surname").Visible = True
        cgRecords.Columns("C3Forename").Visible = True

        cgRecords.Columns("C2Mobile").Visible = True
        cgRecords.Columns("C2Landline").Visible = True
        cgRecords.Columns("C2Surname").Visible = True
        cgRecords.Columns("C2Forename").Visible = True

        cgRecords.Columns("C1Mobile").Visible = True
        cgRecords.Columns("C1Landline").Visible = True
        cgRecords.Columns("C1Surname").Visible = True
        cgRecords.Columns("C1Forename").Visible = True

        cgRecords.Columns("Address").Visible = True
        cgRecords.Columns("PostCode").Visible = True

        cgRecords.Columns("Discount").Visible = True
        cgRecords.Columns("TermTime").Visible = True

        cgRecords.Columns("Friday").Visible = True
        cgRecords.Columns("Thursday").Visible = True
        cgRecords.Columns("Wednesday").Visible = True
        cgRecords.Columns("Tuesday").Visible = True
        cgRecords.Columns("Monday").Visible = True

        cgRecords.Columns("LeavingDate").Visible = True
        cgRecords.Columns("StartDate").Visible = True

        cgRecords.Columns("PostCode").Visible = True
        cgRecords.Columns("Address").Visible = True

        cgRecords.Columns("Keyworker").Visible = True
        cgRecords.Columns("Group").Visible = True

        cgRecords.Columns("ChildGender").Visible = True
        cgRecords.Columns("ChildDOB").Visible = True
        cgRecords.Columns("ChildSurname").Visible = True
        cgRecords.Columns("ChildForename").Visible = True

        cgRecords.Columns("Valid").Visible = True

        cgRecords.AutoSizeColumns()

    End Sub

    Private Function SelectFile() As Boolean
        If ofd.ShowDialog = DialogResult.OK Then
            sc.LoadDocument(ofd.FileName)
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Wiz_SelectedPageChanged(sender As Object, e As DevExpress.XtraWizard.WizardPageChangedEventArgs) Handles Wiz.SelectedPageChanged

        Select Case e.Page.Name

            Case "wpProgress"
                Import()

        End Select

    End Sub

    Private Sub Wiz_SelectedPageChanging(sender As Object, e As DevExpress.XtraWizard.WizardPageChangingEventArgs) Handles Wiz.SelectedPageChanging

        If m_Importing Then
            e.Cancel = True
            Exit Sub
        End If

        If e.Direction = DevExpress.XtraWizard.Direction.Forward Then
            If e.PrevPage.Name = "wpPreview" Then
                If m_Failed > 0 Then
                    CareMessage("You cannot continue with the Import until all the errors are fixed!", MessageBoxIcon.Exclamation, "Import Data")
                    e.Cancel = True
                End If
            End If
        End If

    End Sub

#Region "Import Data"

    Private Function SaveSessions(ByVal DataIn As ImportRecord) As Boolean

        Dim _S As New Business.ChildAdvanceSession
        With _S

            ._ChildId = m_ChildID

            ._DateFrom = ValueHandler.NearestDate(DataIn.StartDate.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
            ._Mode = "Weekly"
            ._Activated = False
            ._RecurringScope = 1
            ._RecurringScopeDesc = "Yes, All the time"
            ._Comments = "Imported via Spreadsheet Import " + Now.ToString
            ._BookingStatus = "Waiting"

            If DataIn.Monday <> "" Then
                Dim _Monday As New IDandDescription("Tariffs", "name", DataIn.Monday)
                If _Monday.Found Then
                    ._Monday = _Monday.ID
                    ._MondayDesc = _Monday.Description
                End If
            End If

            If DataIn.Tuesday <> "" Then
                Dim _Tuesday As New IDandDescription("Tariffs", "name", DataIn.Tuesday)
                If _Tuesday.Found Then
                    ._Tuesday = _Tuesday.ID
                    ._TuesdayDesc = _Tuesday.Description
                End If
            End If

            If DataIn.Wednesday <> "" Then
                Dim _Wednesday As New IDandDescription("Tariffs", "name", DataIn.Wednesday)
                If _Wednesday.Found Then
                    ._Wednesday = _Wednesday.ID
                    ._WednesdayDesc = _Wednesday.Description
                End If
            End If

            If DataIn.Thursday <> "" Then
                Dim _Thursday As New IDandDescription("Tariffs", "name", DataIn.Thursday)
                If _Thursday.Found Then
                    ._Thursday = _Thursday.ID
                    ._ThursdayDesc = _Thursday.Description
                End If
            End If

            If DataIn.Friday <> "" Then
                Dim _Friday As New IDandDescription("Tariffs", "name", DataIn.Friday)
                If _Friday.Found Then
                    ._Friday = _Friday.ID
                    ._FridayDesc = _Friday.Description
                End If
            End If

            .Store()

        End With

        Return True

    End Function

    Private Function SaveFamily(ByVal DataIn As ImportRecord) As Boolean

        Memo("Creating Family " + DataIn.C1Surname + "...")

        Dim _F As New Business.Family
        With _F

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._Surname = DataIn.C1Surname
            ._LetterName = DataIn.C1Forename + " " + DataIn.C1Surname
            ._eInvoicing = False
            ._Address = DataIn.Address
            ._Postcode = DataIn.PostCode

            ._OpeningBalance = 0
            ._Balance = 0

            .Store()

            m_FamilyID = _F._ID

        End With

        _F = Nothing

        Return True

    End Function

    Private Function SaveContacts(ByVal DataIn As ImportRecord) As Boolean

        If Not CreateContactRecord(True, False, DataIn.C1Forename, DataIn.C1Surname, DataIn.C1Relationship, _
                            DataIn.C1Mobile, False, DataIn.C1Landline, DataIn.C1Email, DataIn.C1Secret) Then Return False

        If DataIn.C2Surname <> "" Then
            If Not CreateContactRecord(False, True, DataIn.C2Forename, DataIn.C2Surname, DataIn.C2Relationship, _
                                DataIn.C2Mobile, False, DataIn.C2Landline, DataIn.C2Email, DataIn.C2Secret) Then Return False
        End If

        If DataIn.C3Surname <> "" Then
            If Not CreateContactRecord(False, True, DataIn.C3Forename, DataIn.C3Surname, DataIn.C3Relationship, _
                                DataIn.C3Mobile, False, DataIn.C3Landline, DataIn.C3Email, DataIn.C3Secret) Then Return False
        End If

        If DataIn.C4Surname <> "" Then
            If Not CreateContactRecord(False, True, DataIn.C4Forename, DataIn.C4Surname, DataIn.C4Relationship, _
                                DataIn.C4Mobile, False, DataIn.C4Landline, DataIn.C4Email, DataIn.C4Secret) Then Return False
        End If

        Return True

    End Function

    Private Function SaveChild(ByVal DataIn As ImportRecord) As Boolean

        Dim _C As New Business.Child
        With _C

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._FamilyId = m_FamilyID
            ._FamilyName = DataIn.C1Surname

            ._Status = "On Waiting List"

            ._Forename = DataIn.ChildForename
            ._Surname = DataIn.ChildSurname
            ._Fullname = ._Forename + " " + ._Surname

            ._Dob = DataIn.ChildDOB
            ._Gender = DataIn.ChildGender

            ._Started = DataIn.StartDate
            ._GroupStarted = ._Started

            ._GroupName = DataIn.Group

            If Not ValidateRoom(._GroupName, ._GroupId) Then
                ._GroupId = Nothing
                ._GroupName = ""
            End If

            Dim _Keyworker As New IDandDescription("Staff", "fullname", DataIn.Keyworker)
            If _Keyworker.Found Then
                ._KeyworkerId = _Keyworker.ID
                ._KeyworkerName = _Keyworker.Description
            End If

            Dim _Discount As New IDandDescription("Discounts", "name", DataIn.Discount)
            If _Discount.Found Then
                ._Discount = _Discount.ID
            End If

            ._TermOnly = DataIn.TermTime

            ._MedAllergies = DataIn.Alergies
            ._MedMedication = DataIn.MedicalNotes
            ._MedNotes = DataIn.OtherNotes

            ._SurgDoc = DataIn.GPName
            ._SurgAdd1 = DataIn.GPAddress
            ._SurgTel = DataIn.GPTel

            .Store()

            m_ChildID = ._ID

        End With

        Return True

    End Function

    Private Function CreateContactRecord(ByVal Primary As Boolean, ByVal Emergency As Boolean, ByVal Forename As String, ByVal Surname As String, ByVal Rel As String, _
                                         ByVal Mobile As String, ByVal SMS As Boolean, ByVal Landline As String, ByVal Email As String, ByVal Password As String) As Boolean

        Dim _C As New Business.Contact
        With _C

            ._FamilyId = m_FamilyID

            ._Surname = Surname
            ._Forename = Forename
            ._Fullname = Forename + " " + Surname
            ._Relationship = Rel

            If Primary Then
                ._PrimaryCont = True
                ._EmerCont = True
                ._Parent = True
                ._Collect = True
                ._InvoiceEmail = True
                ._Address = m_Address
                ._Postcode = m_PostCode
            Else
                ._PrimaryCont = False
                ._EmerCont = Emergency
            End If

            ._TelHome = Landline
            ._TelMobile = Mobile
            ._Sms = SMS
            ._Email = Email

            ._Password = Password

            .Store()

        End With

        _C = Nothing

        Return True

    End Function
#End Region

#Region "Grid Events"

    Private Sub cgRecords_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles cgRecords.RowCellStyle
        If e.CellValue IsNot Nothing Then
            If e.CellValue.ToString.Contains("!") Then
                e.Appearance.BackColor = Drawing.Color.LightCoral
            Else
                e.Appearance.Reset()
            End If
        End If
    End Sub

    Private Sub cgRecords_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgRecords.RowStyle
        If ValueHandler.ConvertBoolean(cgRecords.GetRowCellValue(e.RowHandle, "Valid")) Then
            e.Appearance.BackColor = Drawing.Color.LightGreen
        Else
            e.Appearance.Reset()
        End If
    End Sub

#End Region

#Region "Data Helpers"

    Private Function ReturnString(ByVal ValueIn As Cell) As String
        If ValueIn Is Nothing Then Return ""
        If ValueIn.Value Is Nothing Then Return ""
        Return ValueIn.Value.ToString
    End Function

    Private Function ReturnDate(ByVal ValueIn As Cell) As Date?
        If ValueIn.Value.IsDateTime Then
            Return ValueIn.Value.DateTimeValue
        Else
            Return Nothing
        End If
    End Function

    Private Function ReturnBoolean(ByVal ValueIn As Cell) As Boolean

        If ValueIn.Value.IsBoolean Then
            Return ValueIn.Value.BooleanValue
        Else

            If ValueIn.Value.IsText Then

                Select Case ValueIn.Value.TextValue.ToUpper

                    Case "Y", "T", "1", "YES", "TRUE"
                        Return True

                    Case Else
                        Return False

                End Select

            Else
                Return False
            End If
        End If

    End Function

#End Region

#Region "Private Classes"

    Private Class IDandDescription

        Public Sub New(ByVal Table As String, ByVal DescriptionField As String, ByRef Description As String)

            If Description = "" Then
                Me.Found = False
                Me.ID = Nothing
                Description = "!"
                Me.Description = Description
            Else

                Description = Description.Trim()

                Dim _SQL As String = "select id, " + DescriptionField + " from " + Table + " where " + DescriptionField + " like '%" + Description + "%'"
                Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

                If _DR Is Nothing Then
                    Me.Found = False
                    Me.ID = Nothing
                    Description = "! " + Description
                    Me.Description = Description
                Else
                    Me.Found = True
                    Me.ID = New Guid(_DR.Item("id").ToString)
                    Description = _DR.Item(DescriptionField).ToString
                    Me.Description = Description
                End If

            End If

        End Sub

        Property Found As Boolean
        Property ID As Guid?
        Property Description As String

    End Class

    Private Class ImportRecord

        Property Valid As Boolean

        Property ChildForename As String
        Property ChildSurname As String
        Property ChildDOB As Date?
        Property ChildGender As String
        Property Group As String
        Property Keyworker As String

        Property StartDate As Date?
        Property LeavingDate As Date?

        Property TermTime As Boolean
        Property Discount As String

        Property Monday As String
        Property Tuesday As String
        Property Wednesday As String
        Property Thursday As String
        Property Friday As String

        Property Alergies As String
        Property MedicalNotes As String
        Property OtherNotes As String

        Property ConOffsite As Boolean
        Property ConOutdoors As Boolean
        Property ConPhotos As Boolean
        Property ConCalpol As Boolean
        Property ConPlasters As Boolean
        Property ConSuncream As Boolean

        Property Address As String
        Property PostCode As String

        Property C1Forename As String
        Property C1Surname As String
        Property C1Relationship As String
        Property C1Landline As String
        Property C1Mobile As String
        Property C1Email As String
        Property C1Secret As String

        Property C2Forename As String
        Property C2Surname As String
        Property C2Relationship As String
        Property C2Landline As String
        Property C2Mobile As String
        Property C2Email As String
        Property C2Secret As String

        Property C3Forename As String
        Property C3Surname As String
        Property C3Relationship As String
        Property C3Landline As String
        Property C3Mobile As String
        Property C3Email As String
        Property C3Secret As String

        Property C4Forename As String
        Property C4Surname As String
        Property C4Relationship As String
        Property C4Landline As String
        Property C4Mobile As String
        Property C4Email As String
        Property C4Secret As String

        Property GPName As String
        Property GPTel As String
        Property GPAddress As String

    End Class

#End Region

End Class