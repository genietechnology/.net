﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffUsage
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgTariff = New Care.Controls.CareGrid()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgTariff
        '
        Me.cgTariff.AllowBuildColumns = True
        Me.cgTariff.AllowEdit = False
        Me.cgTariff.AllowHorizontalScroll = False
        Me.cgTariff.AllowMultiSelect = False
        Me.cgTariff.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgTariff.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTariff.Appearance.Options.UseFont = True
        Me.cgTariff.AutoSizeByData = True
        Me.cgTariff.DisableAutoSize = False
        Me.cgTariff.DisableDataFormatting = False
        Me.cgTariff.FocusedRowHandle = -2147483648
        Me.cgTariff.HideFirstColumn = False
        Me.cgTariff.Location = New System.Drawing.Point(12, 5)
        Me.cgTariff.Name = "cgTariff"
        Me.cgTariff.PreviewColumn = ""
        Me.cgTariff.QueryID = Nothing
        Me.cgTariff.RowAutoHeight = False
        Me.cgTariff.SearchAsYouType = True
        Me.cgTariff.ShowAutoFilterRow = False
        Me.cgTariff.ShowFindPanel = False
        Me.cgTariff.ShowGroupByBox = False
        Me.cgTariff.ShowLoadingPanel = False
        Me.cgTariff.ShowNavigator = False
        Me.cgTariff.Size = New System.Drawing.Size(460, 418)
        Me.cgTariff.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(376, 429)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(96, 23)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "Close"
        '
        'frmTariffUsage
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(484, 461)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.cgTariff)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "frmTariffUsage"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Tariff Usage"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgTariff As Care.Controls.CareGrid
    Friend WithEvents btnClose As Care.Controls.CareButton

End Class
