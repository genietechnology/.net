﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRoomMoves
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.cbxSite = New Care.Controls.CareComboBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.cbxFilter = New Care.Controls.CareComboBox()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.btnChange = New Care.Controls.CareButton()
        Me.chkKeyWorker = New Care.Controls.CareCheckBox()
        Me.chkRoom = New Care.Controls.CareCheckBox()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.cbxKeyworker = New Care.Controls.CareComboBox()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.cbxRoom = New Care.Controls.CareComboBox()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFilter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkKeyWorker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.cbxFilter)
        Me.GroupControl1.Location = New System.Drawing.Point(9, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(416, 85)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Record Selection"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(50, 27)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(354, 20)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 58)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Filter"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 30)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel5.TabIndex = 0
        Me.CareLabel5.Text = "Site"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFilter
        '
        Me.cbxFilter.AllowBlank = False
        Me.cbxFilter.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxFilter.DataSource = Nothing
        Me.cbxFilter.DisplayMember = Nothing
        Me.cbxFilter.EnterMoveNextControl = True
        Me.cbxFilter.Location = New System.Drawing.Point(50, 55)
        Me.cbxFilter.Name = "cbxFilter"
        Me.cbxFilter.Properties.AccessibleName = "Gender"
        Me.cbxFilter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFilter.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFilter.Properties.Appearance.Options.UseFont = True
        Me.cbxFilter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFilter.SelectedValue = Nothing
        Me.cbxFilter.Size = New System.Drawing.Size(354, 20)
        Me.cbxFilter.TabIndex = 3
        Me.cbxFilter.Tag = ""
        Me.cbxFilter.ValueMember = Nothing
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.btnChange)
        Me.GroupControl2.Controls.Add(Me.chkKeyWorker)
        Me.GroupControl2.Controls.Add(Me.chkRoom)
        Me.GroupControl2.Controls.Add(Me.CareLabel4)
        Me.GroupControl2.Controls.Add(Me.cbxKeyworker)
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.cbxRoom)
        Me.GroupControl2.Location = New System.Drawing.Point(431, 10)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(462, 85)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Options to Change"
        '
        'btnChange
        '
        Me.btnChange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnChange.Appearance.Options.UseFont = True
        Me.btnChange.CausesValidation = False
        Me.btnChange.Location = New System.Drawing.Point(307, 39)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(145, 25)
        Me.btnChange.TabIndex = 6
        Me.btnChange.Tag = ""
        Me.btnChange.Text = "Change Selected Records"
        '
        'chkKeyWorker
        '
        Me.chkKeyWorker.EnterMoveNextControl = True
        Me.chkKeyWorker.Location = New System.Drawing.Point(82, 56)
        Me.chkKeyWorker.Name = "chkKeyWorker"
        Me.chkKeyWorker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkKeyWorker.Properties.Appearance.Options.UseFont = True
        Me.chkKeyWorker.Size = New System.Drawing.Size(20, 19)
        Me.chkKeyWorker.TabIndex = 4
        Me.chkKeyWorker.Tag = ""
        '
        'chkRoom
        '
        Me.chkRoom.EnterMoveNextControl = True
        Me.chkRoom.Location = New System.Drawing.Point(82, 28)
        Me.chkRoom.Name = "chkRoom"
        Me.chkRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRoom.Properties.Appearance.Options.UseFont = True
        Me.chkRoom.Size = New System.Drawing.Size(20, 19)
        Me.chkRoom.TabIndex = 1
        Me.chkRoom.Tag = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 58)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel4.TabIndex = 3
        Me.CareLabel4.Text = "Keyworker"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(108, 55)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Gender"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(193, 22)
        Me.cbxKeyworker.TabIndex = 5
        Me.cbxKeyworker.Tag = ""
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 30)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Room"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(108, 27)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AccessibleName = "Gender"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(193, 22)
        Me.cbxRoom.TabIndex = 2
        Me.cbxRoom.Tag = ""
        Me.cbxRoom.ValueMember = Nothing
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = True
        Me.cgRecords.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = True
        Me.cgRecords.Location = New System.Drawing.Point(9, 132)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = True
        Me.cgRecords.ShowFindPanel = True
        Me.cgRecords.ShowGroupByBox = False
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(887, 369)
        Me.cgRecords.TabIndex = 3
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.CareLabel3)
        Me.GroupControl3.Location = New System.Drawing.Point(9, 101)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(884, 25)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Record Selection"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(5, 5)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(342, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Multi-select records in the grid using the CTRL Key and Left Click"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmRoomMoves
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(903, 513)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.cgRecords)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRoomMoves"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFilter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.chkKeyWorker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxFilter As Care.Controls.CareComboBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents btnChange As Care.Controls.CareButton
    Friend WithEvents chkKeyWorker As Care.Controls.CareCheckBox
    Friend WithEvents chkRoom As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel

End Class
