﻿Imports Care.Global
Imports Care.Shared

Public Class frmInvoiceCreditPrompt

    Private m_ChildID As Guid? = Nothing
    Private m_FamilyID As Guid? = Nothing

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal ChildID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID

    End Sub

    Private Sub frmInvoiceCreditPrompt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim _C As Business.Child = Business.Child.RetreiveByID(m_ChildID.Value)
        m_FamilyID = _C._FamilyId
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

        If Enabled Then
            Session.CursorDefault()
        Else
            Session.CursorWaiting()
        End If

        btnInvoice.Enabled = Enabled
        btnInvoiceBlank.Enabled = Enabled
        btnCreditBlank.Enabled = Enabled
        btnClose.Enabled = Enabled

    End Sub

    Private Sub CreateInvoice(ByVal BuildInvoice As Boolean)

        SetCMDs(False)

        Dim _BatchID As Guid = Guid.NewGuid
        Dim _BatchHeader As New Business.InvoiceBatch
        With _BatchHeader

            ._ID = _BatchID
            ._BatchFreq = "Empty Batch"
            ._BatchNo = Invoicing.LastBatchNo + 1
            ._BatchStatus = "Open"
            ._InvFirst = Invoicing.LastInvoiceNo + 1

            Dim _frm As Care.Shared.frmBaseForm
            If BuildInvoice Then
                _frm = New frmInvoiceAdd(_BatchHeader, m_ChildID.ToString)
            Else
                Dim _InvoiceID As Guid? = Invoicing.GenerateOneLineInvoice(_BatchID, ._BatchNo, m_FamilyID.Value, m_ChildID, Today, Nothing, Nothing, "", "", 0, CInt(._InvFirst))
                _frm = New frmInvoice(frmInvoice.EnumMode.AmendInvoice, _InvoiceID.Value)
            End If

            _frm.ShowDialog()
            If _frm.DialogResult = DialogResult.OK Then
                ._UserId = Session.CurrentUser.ID
                ._Stamp = Now
                .Store()
            End If

            _frm.Dispose()
            _frm = Nothing

        End With

        SetCMDs(True)

    End Sub

    Private Sub CreateCredit()

        SetCMDs(False)

        Dim _BatchID As Guid = Guid.NewGuid
        Dim _Save As Boolean = False

        Dim _BatchHeader As New Business.CreditBatch
        With _BatchHeader

            ._ID = _BatchID
            ._BatchNo = Credits.LastBatchNo + 1
            ._BatchDate = Today
            ._BatchStatus = "Open"
            ._UserId = Session.CurrentUser.ID
            ._Stamp = Now
            .Store()

            Dim _CreditID As Guid = Credits.GenerateOneLineCredit(._ID.Value, CInt(._BatchNo), Credits.LastCreditNo + 1, m_FamilyID.Value, m_ChildID, Today, "", 0)
            Dim _frm As New frmCredit(frmCredit.EnumMode.AmendCreditNote, _CreditID, m_ChildID)
            _frm.ShowDialog()

            _frm.Dispose()
            _frm = Nothing

        End With

        SetCMDs(True)

    End Sub

    Private Sub btnInvoice_Click(sender As Object, e As EventArgs) Handles btnInvoice.Click
        CreateInvoice(True)
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnInvoiceBlank_Click(sender As Object, e As EventArgs) Handles btnInvoiceBlank.Click
        CreateInvoice(False)
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCreditBlank_Click(sender As Object, e As EventArgs) Handles btnCreditBlank.Click
        CreateCredit()
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

End Class