﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamilyMerge
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtSourceFinancials = New Care.Controls.CareTextBox(Me.components)
        Me.txtSourceAddressee = New Care.Controls.CareTextBox(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.txtSourceSurname = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.radChildrenCopy = New Care.Controls.CareRadioButton(Me.components)
        Me.radChildrenDiscard = New Care.Controls.CareRadioButton(Me.components)
        Me.grdSourceChildren = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.radContactCopy = New Care.Controls.CareRadioButton(Me.components)
        Me.radContactDiscard = New Care.Controls.CareRadioButton(Me.components)
        Me.grdSourceContacts = New Care.Controls.CareGrid()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.grdTargetContacts = New Care.Controls.CareGrid()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.grdTargetChildren = New Care.Controls.CareGrid()
        Me.btnMerge = New Care.Controls.CareButton(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.txtTargetFinancials = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtTargetAddressee = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtTargetSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnFind = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtSourceFinancials.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSourceAddressee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSourceSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.radChildrenCopy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChildrenDiscard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radContactCopy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radContactDiscard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtTargetFinancials.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTargetAddressee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTargetSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtSourceFinancials)
        Me.GroupControl1.Controls.Add(Me.txtSourceAddressee)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.txtSourceSurname)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 8)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(818, 56)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Source Family"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(662, 31)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel3.TabIndex = 5
        Me.CareLabel3.Text = "Financials Linked"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSourceFinancials
        '
        Me.txtSourceFinancials.CharacterCasing = CharacterCasing.Normal
        Me.txtSourceFinancials.EnterMoveNextControl = True
        Me.txtSourceFinancials.Location = New System.Drawing.Point(758, 28)
        Me.txtSourceFinancials.MaxLength = 100
        Me.txtSourceFinancials.Name = "txtSourceFinancials"
        Me.txtSourceFinancials.NumericAllowNegatives = False
        Me.txtSourceFinancials.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSourceFinancials.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSourceFinancials.Properties.AccessibleName = "Surname"
        Me.txtSourceFinancials.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSourceFinancials.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSourceFinancials.Properties.Appearance.Options.UseFont = True
        Me.txtSourceFinancials.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSourceFinancials.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSourceFinancials.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSourceFinancials.Properties.MaxLength = 100
        Me.txtSourceFinancials.Properties.ReadOnly = True
        Me.txtSourceFinancials.ReadOnly = True
        Me.txtSourceFinancials.Size = New System.Drawing.Size(51, 20)
        Me.txtSourceFinancials.TabIndex = 4
        Me.txtSourceFinancials.Tag = ""
        Me.txtSourceFinancials.TextAlign = HorizontalAlignment.Left
        Me.txtSourceFinancials.ToolTipText = ""
        '
        'txtSourceAddressee
        '
        Me.txtSourceAddressee.CharacterCasing = CharacterCasing.Normal
        Me.txtSourceAddressee.EnterMoveNextControl = True
        Me.txtSourceAddressee.Location = New System.Drawing.Point(70, 28)
        Me.txtSourceAddressee.MaxLength = 100
        Me.txtSourceAddressee.Name = "txtSourceAddressee"
        Me.txtSourceAddressee.NumericAllowNegatives = False
        Me.txtSourceAddressee.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSourceAddressee.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSourceAddressee.Properties.AccessibleName = "Addressee"
        Me.txtSourceAddressee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSourceAddressee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSourceAddressee.Properties.Appearance.Options.UseFont = True
        Me.txtSourceAddressee.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSourceAddressee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSourceAddressee.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSourceAddressee.Properties.MaxLength = 100
        Me.txtSourceAddressee.Properties.ReadOnly = True
        Me.txtSourceAddressee.ReadOnly = True
        Me.txtSourceAddressee.Size = New System.Drawing.Size(327, 20)
        Me.txtSourceAddressee.TabIndex = 1
        Me.txtSourceAddressee.Tag = ""
        Me.txtSourceAddressee.TextAlign = HorizontalAlignment.Left
        Me.txtSourceAddressee.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(10, 31)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Addressee"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label7.ToolTip = "This is the name that will appear on Invoices or Letters from the nursery."
        Me.Label7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'txtSourceSurname
        '
        Me.txtSourceSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtSourceSurname.EnterMoveNextControl = True
        Me.txtSourceSurname.Location = New System.Drawing.Point(475, 28)
        Me.txtSourceSurname.MaxLength = 100
        Me.txtSourceSurname.Name = "txtSourceSurname"
        Me.txtSourceSurname.NumericAllowNegatives = False
        Me.txtSourceSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSourceSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSourceSurname.Properties.AccessibleName = "Surname"
        Me.txtSourceSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSourceSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSourceSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSourceSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSourceSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSourceSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSourceSurname.Properties.MaxLength = 100
        Me.txtSourceSurname.Properties.ReadOnly = True
        Me.txtSourceSurname.ReadOnly = True
        Me.txtSourceSurname.Size = New System.Drawing.Size(171, 20)
        Me.txtSourceSurname.TabIndex = 3
        Me.txtSourceSurname.Tag = ""
        Me.txtSourceSurname.TextAlign = HorizontalAlignment.Left
        Me.txtSourceSurname.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(422, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Surname"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Appearance.Options.UseFont = True
        Me.GroupBox2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.AppearanceCaption.Options.UseFont = True
        Me.GroupBox2.Controls.Add(Me.radChildrenCopy)
        Me.GroupBox2.Controls.Add(Me.radChildrenDiscard)
        Me.GroupBox2.Controls.Add(Me.grdSourceChildren)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 70)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(406, 162)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.Text = "Children (Double-Click to View Child Details)"
        '
        'radChildrenCopy
        '
        Me.radChildrenCopy.EditValue = True
        Me.radChildrenCopy.Location = New System.Drawing.Point(10, 26)
        Me.radChildrenCopy.Name = "radChildrenCopy"
        Me.radChildrenCopy.Properties.AutoWidth = True
        Me.radChildrenCopy.Properties.Caption = "Copy to Target"
        Me.radChildrenCopy.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radChildrenCopy.Properties.RadioGroupIndex = 0
        Me.radChildrenCopy.Size = New System.Drawing.Size(95, 19)
        Me.radChildrenCopy.TabIndex = 11
        '
        'radChildrenDiscard
        '
        Me.radChildrenDiscard.Location = New System.Drawing.Point(118, 26)
        Me.radChildrenDiscard.Name = "radChildrenDiscard"
        Me.radChildrenDiscard.Properties.AutoWidth = True
        Me.radChildrenDiscard.Properties.Caption = "Discard"
        Me.radChildrenDiscard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radChildrenDiscard.Properties.RadioGroupIndex = 0
        Me.radChildrenDiscard.Size = New System.Drawing.Size(57, 19)
        Me.radChildrenDiscard.TabIndex = 10
        Me.radChildrenDiscard.TabStop = False
        '
        'grdSourceChildren
        '
        Me.grdSourceChildren.AllowBuildColumns = True
        Me.grdSourceChildren.AllowHorizontalScroll = False
        Me.grdSourceChildren.AllowMultiSelect = False
        Me.grdSourceChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdSourceChildren.Appearance.Options.UseFont = True
        Me.grdSourceChildren.AutoSizeByData = True
        Me.grdSourceChildren.DisableAutoSize = False
        Me.grdSourceChildren.DisableDataFormatting = False
        Me.grdSourceChildren.FocusedRowHandle = -2147483648
        Me.grdSourceChildren.HideFirstColumn = False
        Me.grdSourceChildren.Location = New System.Drawing.Point(10, 51)
        Me.grdSourceChildren.Name = "grdSourceChildren"
        Me.grdSourceChildren.QueryID = Nothing
        Me.grdSourceChildren.RowAutoHeight = False
        Me.grdSourceChildren.SearchAsYouType = True
        Me.grdSourceChildren.ShowAutoFilterRow = False
        Me.grdSourceChildren.ShowFindPanel = False
        Me.grdSourceChildren.ShowGroupByBox = False
        Me.grdSourceChildren.ShowNavigator = False
        Me.grdSourceChildren.Size = New System.Drawing.Size(387, 101)
        Me.grdSourceChildren.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.Appearance.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.radContactCopy)
        Me.GroupControl2.Controls.Add(Me.radContactDiscard)
        Me.GroupControl2.Controls.Add(Me.grdSourceContacts)
        Me.GroupControl2.Location = New System.Drawing.Point(424, 70)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(406, 162)
        Me.GroupControl2.TabIndex = 15
        Me.GroupControl2.Text = "Contacts (Double-Click to View Contact Details)"
        '
        'radContactCopy
        '
        Me.radContactCopy.EditValue = True
        Me.radContactCopy.Location = New System.Drawing.Point(8, 26)
        Me.radContactCopy.Name = "radContactCopy"
        Me.radContactCopy.Properties.AutoWidth = True
        Me.radContactCopy.Properties.Caption = "Copy to Target"
        Me.radContactCopy.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radContactCopy.Properties.RadioGroupIndex = 0
        Me.radContactCopy.Size = New System.Drawing.Size(95, 19)
        Me.radContactCopy.TabIndex = 13
        '
        'radContactDiscard
        '
        Me.radContactDiscard.Location = New System.Drawing.Point(116, 26)
        Me.radContactDiscard.Name = "radContactDiscard"
        Me.radContactDiscard.Properties.AutoWidth = True
        Me.radContactDiscard.Properties.Caption = "Discard"
        Me.radContactDiscard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radContactDiscard.Properties.RadioGroupIndex = 0
        Me.radContactDiscard.Size = New System.Drawing.Size(57, 19)
        Me.radContactDiscard.TabIndex = 12
        Me.radContactDiscard.TabStop = False
        '
        'grdSourceContacts
        '
        Me.grdSourceContacts.AllowBuildColumns = True
        Me.grdSourceContacts.AllowHorizontalScroll = False
        Me.grdSourceContacts.AllowMultiSelect = False
        Me.grdSourceContacts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdSourceContacts.Appearance.Options.UseFont = True
        Me.grdSourceContacts.AutoSizeByData = True
        Me.grdSourceContacts.DisableAutoSize = False
        Me.grdSourceContacts.DisableDataFormatting = False
        Me.grdSourceContacts.FocusedRowHandle = -2147483648
        Me.grdSourceContacts.HideFirstColumn = False
        Me.grdSourceContacts.Location = New System.Drawing.Point(10, 49)
        Me.grdSourceContacts.Name = "grdSourceContacts"
        Me.grdSourceContacts.QueryID = Nothing
        Me.grdSourceContacts.RowAutoHeight = False
        Me.grdSourceContacts.SearchAsYouType = True
        Me.grdSourceContacts.ShowAutoFilterRow = False
        Me.grdSourceContacts.ShowFindPanel = False
        Me.grdSourceContacts.ShowGroupByBox = False
        Me.grdSourceContacts.ShowNavigator = False
        Me.grdSourceContacts.Size = New System.Drawing.Size(387, 101)
        Me.grdSourceContacts.TabIndex = 0
        '
        'GroupControl4
        '
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.grdTargetContacts)
        Me.GroupControl4.Location = New System.Drawing.Point(424, 300)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(406, 158)
        Me.GroupControl4.TabIndex = 17
        Me.GroupControl4.Text = "Contacts (Double-Click to View Contact Details)"
        '
        'grdTargetContacts
        '
        Me.grdTargetContacts.AllowBuildColumns = True
        Me.grdTargetContacts.AllowHorizontalScroll = False
        Me.grdTargetContacts.AllowMultiSelect = False
        Me.grdTargetContacts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdTargetContacts.Appearance.Options.UseFont = True
        Me.grdTargetContacts.AutoSizeByData = True
        Me.grdTargetContacts.DisableAutoSize = False
        Me.grdTargetContacts.DisableDataFormatting = False
        Me.grdTargetContacts.FocusedRowHandle = -2147483648
        Me.grdTargetContacts.HideFirstColumn = False
        Me.grdTargetContacts.Location = New System.Drawing.Point(10, 30)
        Me.grdTargetContacts.Name = "grdTargetContacts"
        Me.grdTargetContacts.QueryID = Nothing
        Me.grdTargetContacts.RowAutoHeight = False
        Me.grdTargetContacts.SearchAsYouType = True
        Me.grdTargetContacts.ShowAutoFilterRow = False
        Me.grdTargetContacts.ShowFindPanel = False
        Me.grdTargetContacts.ShowGroupByBox = False
        Me.grdTargetContacts.ShowNavigator = False
        Me.grdTargetContacts.Size = New System.Drawing.Size(387, 120)
        Me.grdTargetContacts.TabIndex = 0
        '
        'GroupControl5
        '
        Me.GroupControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl5.Appearance.Options.UseFont = True
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.grdTargetChildren)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 300)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(406, 158)
        Me.GroupControl5.TabIndex = 16
        Me.GroupControl5.Text = "Children (Double-Click to View Child Details)"
        '
        'grdTargetChildren
        '
        Me.grdTargetChildren.AllowBuildColumns = True
        Me.grdTargetChildren.AllowHorizontalScroll = False
        Me.grdTargetChildren.AllowMultiSelect = False
        Me.grdTargetChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdTargetChildren.Appearance.Options.UseFont = True
        Me.grdTargetChildren.AutoSizeByData = True
        Me.grdTargetChildren.DisableAutoSize = False
        Me.grdTargetChildren.DisableDataFormatting = False
        Me.grdTargetChildren.FocusedRowHandle = -2147483648
        Me.grdTargetChildren.HideFirstColumn = False
        Me.grdTargetChildren.Location = New System.Drawing.Point(10, 30)
        Me.grdTargetChildren.Name = "grdTargetChildren"
        Me.grdTargetChildren.QueryID = Nothing
        Me.grdTargetChildren.RowAutoHeight = False
        Me.grdTargetChildren.SearchAsYouType = True
        Me.grdTargetChildren.ShowAutoFilterRow = False
        Me.grdTargetChildren.ShowFindPanel = False
        Me.grdTargetChildren.ShowGroupByBox = False
        Me.grdTargetChildren.ShowNavigator = False
        Me.grdTargetChildren.Size = New System.Drawing.Size(387, 120)
        Me.grdTargetChildren.TabIndex = 0
        '
        'btnMerge
        '
        Me.btnMerge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMerge.Appearance.Options.UseFont = True
        Me.btnMerge.Location = New System.Drawing.Point(614, 465)
        Me.btnMerge.Name = "btnMerge"
        Me.btnMerge.Size = New System.Drawing.Size(105, 24)
        Me.btnMerge.TabIndex = 18
        Me.btnMerge.Tag = ""
        Me.btnMerge.Text = "Merge"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.txtTargetFinancials)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.txtTargetAddressee)
        Me.GroupControl3.Controls.Add(Me.CareLabel1)
        Me.GroupControl3.Controls.Add(Me.txtTargetSurname)
        Me.GroupControl3.Controls.Add(Me.CareLabel2)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 238)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(818, 56)
        Me.GroupControl3.TabIndex = 4
        Me.GroupControl3.Text = "Target Family"
        '
        'txtTargetFinancials
        '
        Me.txtTargetFinancials.CharacterCasing = CharacterCasing.Normal
        Me.txtTargetFinancials.EnterMoveNextControl = True
        Me.txtTargetFinancials.Location = New System.Drawing.Point(758, 28)
        Me.txtTargetFinancials.MaxLength = 100
        Me.txtTargetFinancials.Name = "txtTargetFinancials"
        Me.txtTargetFinancials.NumericAllowNegatives = False
        Me.txtTargetFinancials.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTargetFinancials.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTargetFinancials.Properties.AccessibleName = "Surname"
        Me.txtTargetFinancials.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTargetFinancials.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTargetFinancials.Properties.Appearance.Options.UseFont = True
        Me.txtTargetFinancials.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTargetFinancials.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTargetFinancials.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTargetFinancials.Properties.MaxLength = 100
        Me.txtTargetFinancials.Properties.ReadOnly = True
        Me.txtTargetFinancials.ReadOnly = True
        Me.txtTargetFinancials.Size = New System.Drawing.Size(51, 20)
        Me.txtTargetFinancials.TabIndex = 22
        Me.txtTargetFinancials.Tag = ""
        Me.txtTargetFinancials.TextAlign = HorizontalAlignment.Left
        Me.txtTargetFinancials.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(662, 31)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel4.TabIndex = 21
        Me.CareLabel4.Text = "Financials Linked"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTargetAddressee
        '
        Me.txtTargetAddressee.CharacterCasing = CharacterCasing.Normal
        Me.txtTargetAddressee.EnterMoveNextControl = True
        Me.txtTargetAddressee.Location = New System.Drawing.Point(70, 28)
        Me.txtTargetAddressee.MaxLength = 100
        Me.txtTargetAddressee.Name = "txtTargetAddressee"
        Me.txtTargetAddressee.NumericAllowNegatives = False
        Me.txtTargetAddressee.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTargetAddressee.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTargetAddressee.Properties.AccessibleName = "Addressee"
        Me.txtTargetAddressee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTargetAddressee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTargetAddressee.Properties.Appearance.Options.UseFont = True
        Me.txtTargetAddressee.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTargetAddressee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTargetAddressee.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTargetAddressee.Properties.MaxLength = 100
        Me.txtTargetAddressee.Properties.ReadOnly = True
        Me.txtTargetAddressee.ReadOnly = True
        Me.txtTargetAddressee.Size = New System.Drawing.Size(327, 20)
        Me.txtTargetAddressee.TabIndex = 1
        Me.txtTargetAddressee.Tag = ""
        Me.txtTargetAddressee.TextAlign = HorizontalAlignment.Left
        Me.txtTargetAddressee.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Addressee"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel1.ToolTip = "This is the name that will appear on Invoices or Letters from the nursery."
        Me.CareLabel1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'txtTargetSurname
        '
        Me.txtTargetSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtTargetSurname.EnterMoveNextControl = True
        Me.txtTargetSurname.Location = New System.Drawing.Point(475, 28)
        Me.txtTargetSurname.MaxLength = 100
        Me.txtTargetSurname.Name = "txtTargetSurname"
        Me.txtTargetSurname.NumericAllowNegatives = False
        Me.txtTargetSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTargetSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTargetSurname.Properties.AccessibleName = "Surname"
        Me.txtTargetSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTargetSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTargetSurname.Properties.Appearance.Options.UseFont = True
        Me.txtTargetSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTargetSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTargetSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTargetSurname.Properties.MaxLength = 100
        Me.txtTargetSurname.Properties.ReadOnly = True
        Me.txtTargetSurname.ReadOnly = True
        Me.txtTargetSurname.Size = New System.Drawing.Size(171, 20)
        Me.txtTargetSurname.TabIndex = 3
        Me.txtTargetSurname.Tag = ""
        Me.txtTargetSurname.TextAlign = HorizontalAlignment.Left
        Me.txtTargetSurname.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(422, 31)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Surname"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(725, 465)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(105, 24)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Tag = ""
        Me.btnCancel.Text = "Cancel"
        '
        'btnFind
        '
        Me.btnFind.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Appearance.Options.UseFont = True
        Me.btnFind.Location = New System.Drawing.Point(12, 464)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(125, 24)
        Me.btnFind.TabIndex = 20
        Me.btnFind.Tag = ""
        Me.btnFind.Text = "Find Target Family"
        '
        'frmFamilyMerge
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(841, 497)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.btnMerge)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFamilyMerge"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Merge Family"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtSourceFinancials.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSourceAddressee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSourceSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.radChildrenCopy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChildrenDiscard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.radContactCopy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radContactDiscard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtTargetFinancials.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTargetAddressee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTargetSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSourceAddressee As Care.Controls.CareTextBox
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents txtSourceSurname As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdSourceChildren As Care.Controls.CareGrid
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdSourceContacts As Care.Controls.CareGrid
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdTargetContacts As Care.Controls.CareGrid
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdTargetChildren As Care.Controls.CareGrid
    Friend WithEvents btnMerge As Care.Controls.CareButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtTargetAddressee As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtTargetSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents radChildrenCopy As Care.Controls.CareRadioButton
    Friend WithEvents radChildrenDiscard As Care.Controls.CareRadioButton
    Friend WithEvents radContactCopy As Care.Controls.CareRadioButton
    Friend WithEvents radContactDiscard As Care.Controls.CareRadioButton
    Friend WithEvents btnFind As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtSourceFinancials As Care.Controls.CareTextBox
    Friend WithEvents txtTargetFinancials As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel

End Class
