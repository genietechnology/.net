﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVoucherProvider
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactEmail = New Care.Controls.CareTextBox(Me.components)
        Me.txtContactTel = New Care.Controls.CareTextBox(Me.components)
        Me.txtContact = New Care.Controls.CareTextBox(Me.components)
        Me.txtTel = New Care.Controls.CareTextBox(Me.components)
        Me.txtReference = New Care.Controls.CareTextBox(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtContactEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactTel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReference.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 352)
        Me.Panel1.Size = New System.Drawing.Size(395, 35)
        Me.Panel1.TabIndex = 1
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(211, 5)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(302, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtContactEmail)
        Me.GroupControl1.Controls.Add(Me.txtContactTel)
        Me.GroupControl1.Controls.Add(Me.txtContact)
        Me.GroupControl1.Controls.Add(Me.txtTel)
        Me.GroupControl1.Controls.Add(Me.txtReference)
        Me.GroupControl1.Controls.Add(Me.txtAddress)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 53)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(375, 293)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 264)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel7.TabIndex = 13
        Me.CareLabel7.Text = "Contact Email"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 236)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(101, 15)
        Me.CareLabel6.TabIndex = 12
        Me.CareLabel6.Text = "Contact Telephone"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 208)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel5.TabIndex = 11
        Me.CareLabel5.Text = "Contact Name"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 180)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel4.TabIndex = 10
        Me.CareLabel4.Text = "Telephone"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 74)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel3.TabIndex = 9
        Me.CareLabel3.Text = "Address"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 45)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Reference"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactEmail
        '
        Me.txtContactEmail.CharacterCasing = CharacterCasing.Lower
        Me.txtContactEmail.EnterMoveNextControl = True
        Me.txtContactEmail.Location = New System.Drawing.Point(119, 261)
        Me.txtContactEmail.MaxLength = 100
        Me.txtContactEmail.Name = "txtContactEmail"
        Me.txtContactEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactEmail.Properties.Appearance.Options.UseFont = True
        Me.txtContactEmail.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactEmail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactEmail.Properties.CharacterCasing = CharacterCasing.Lower
        Me.txtContactEmail.Properties.MaxLength = 100
        Me.txtContactEmail.ReadOnly = False
        Me.txtContactEmail.Size = New System.Drawing.Size(245, 22)
        Me.txtContactEmail.TabIndex = 6
        Me.txtContactEmail.Tag = "AE"
        Me.txtContactEmail.TextAlign = HorizontalAlignment.Left
        Me.txtContactEmail.ToolTipText = ""
        '
        'txtContactTel
        '
        Me.txtContactTel.CharacterCasing = CharacterCasing.Normal
        Me.txtContactTel.EnterMoveNextControl = True
        Me.txtContactTel.Location = New System.Drawing.Point(119, 233)
        Me.txtContactTel.MaxLength = 15
        Me.txtContactTel.Name = "txtContactTel"
        Me.txtContactTel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactTel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactTel.Properties.Appearance.Options.UseFont = True
        Me.txtContactTel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactTel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactTel.Properties.MaxLength = 15
        Me.txtContactTel.ReadOnly = False
        Me.txtContactTel.Size = New System.Drawing.Size(245, 22)
        Me.txtContactTel.TabIndex = 5
        Me.txtContactTel.Tag = "AE"
        Me.txtContactTel.TextAlign = HorizontalAlignment.Left
        Me.txtContactTel.ToolTipText = ""
        '
        'txtContact
        '
        Me.txtContact.CharacterCasing = CharacterCasing.Normal
        Me.txtContact.EnterMoveNextControl = True
        Me.txtContact.Location = New System.Drawing.Point(119, 205)
        Me.txtContact.MaxLength = 30
        Me.txtContact.Name = "txtContact"
        Me.txtContact.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContact.Properties.Appearance.Options.UseFont = True
        Me.txtContact.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContact.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContact.Properties.MaxLength = 30
        Me.txtContact.ReadOnly = False
        Me.txtContact.Size = New System.Drawing.Size(245, 22)
        Me.txtContact.TabIndex = 4
        Me.txtContact.Tag = "AE"
        Me.txtContact.TextAlign = HorizontalAlignment.Left
        Me.txtContact.ToolTipText = ""
        '
        'txtTel
        '
        Me.txtTel.CharacterCasing = CharacterCasing.Normal
        Me.txtTel.EnterMoveNextControl = True
        Me.txtTel.Location = New System.Drawing.Point(119, 177)
        Me.txtTel.MaxLength = 15
        Me.txtTel.Name = "txtTel"
        Me.txtTel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTel.Properties.Appearance.Options.UseFont = True
        Me.txtTel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTel.Properties.MaxLength = 15
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(245, 22)
        Me.txtTel.TabIndex = 3
        Me.txtTel.Tag = "AE"
        Me.txtTel.TextAlign = HorizontalAlignment.Left
        Me.txtTel.ToolTipText = ""
        '
        'txtReference
        '
        Me.txtReference.CharacterCasing = CharacterCasing.Upper
        Me.txtReference.EnterMoveNextControl = True
        Me.txtReference.Location = New System.Drawing.Point(119, 42)
        Me.txtReference.MaxLength = 20
        Me.txtReference.Name = "txtReference"
        Me.txtReference.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtReference.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtReference.Properties.Appearance.Options.UseFont = True
        Me.txtReference.Properties.Appearance.Options.UseTextOptions = True
        Me.txtReference.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtReference.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtReference.Properties.MaxLength = 20
        Me.txtReference.ReadOnly = False
        Me.txtReference.Size = New System.Drawing.Size(245, 22)
        Me.txtReference.TabIndex = 1
        Me.txtReference.Tag = "AE"
        Me.txtReference.TextAlign = HorizontalAlignment.Left
        Me.txtReference.ToolTipText = ""
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(119, 70)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(245, 101)
        Me.txtAddress.TabIndex = 2
        Me.txtAddress.Tag = "AE"
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(119, 14)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleDescription = "Provider Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(245, 22)
        Me.txtName.TabIndex = 0
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 17)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmVoucherProvider
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(395, 387)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmVoucherProvider"
        Me.Text = "Voucher Providers"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtContactEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactTel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReference.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtContactEmail As Care.Controls.CareTextBox
    Friend WithEvents txtContactTel As Care.Controls.CareTextBox
    Friend WithEvents txtContact As Care.Controls.CareTextBox
    Friend WithEvents txtTel As Care.Controls.CareTextBox
    Friend WithEvents txtReference As Care.Controls.CareTextBox
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents txtName As Care.Controls.CareTextBox
End Class
