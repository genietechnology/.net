﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffTrain
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxWeekly = New Care.Controls.CareFrame(Me.components)
        Me.txtVenue = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTo = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtCourse = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtCost = New Care.Controls.CareTextBox(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.txtVenue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCourse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.txtVenue)
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.cdtTo)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.txtCourse)
        Me.gbxWeekly.Controls.Add(Me.CareLabel14)
        Me.gbxWeekly.Controls.Add(Me.CareLabel13)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.txtCost)
        Me.gbxWeekly.Controls.Add(Me.cdtFrom)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(486, 144)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Training Details"
        '
        'txtVenue
        '
        Me.txtVenue.CharacterCasing = CharacterCasing.Normal
        Me.txtVenue.EnterMoveNextControl = True
        Me.txtVenue.Location = New System.Drawing.Point(113, 85)
        Me.txtVenue.MaxLength = 40
        Me.txtVenue.Name = "txtVenue"
        Me.txtVenue.NumericAllowNegatives = False
        Me.txtVenue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtVenue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVenue.Properties.AccessibleName = "Venue"
        Me.txtVenue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVenue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVenue.Properties.Appearance.Options.UseFont = True
        Me.txtVenue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVenue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVenue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVenue.Properties.MaxLength = 40
        Me.txtVenue.Size = New System.Drawing.Size(356, 22)
        Me.txtVenue.TabIndex = 3
        Me.txtVenue.Tag = "AE"
        Me.txtVenue.TextAlign = HorizontalAlignment.Left
        Me.txtVenue.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(219, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel2.TabIndex = 9
        Me.CareLabel2.Text = "to"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = Nothing
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(236, 29)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.AccessibleName = "Training To Date"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtTo.TabIndex = 1
        Me.cdtTo.Tag = "AEM"
        Me.cdtTo.Value = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(14, 88)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Venue"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCourse
        '
        Me.txtCourse.CharacterCasing = CharacterCasing.Normal
        Me.txtCourse.EnterMoveNextControl = True
        Me.txtCourse.Location = New System.Drawing.Point(113, 57)
        Me.txtCourse.MaxLength = 40
        Me.txtCourse.Name = "txtCourse"
        Me.txtCourse.NumericAllowNegatives = False
        Me.txtCourse.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCourse.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCourse.Properties.AccessibleName = "Course Name"
        Me.txtCourse.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCourse.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCourse.Properties.Appearance.Options.UseFont = True
        Me.txtCourse.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCourse.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCourse.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCourse.Properties.MaxLength = 40
        Me.txtCourse.Size = New System.Drawing.Size(356, 22)
        Me.txtCourse.TabIndex = 2
        Me.txtCourse.Tag = "AEM"
        Me.txtCourse.TextAlign = HorizontalAlignment.Left
        Me.txtCourse.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(14, 116)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel14.TabIndex = 6
        Me.CareLabel14.Text = "Cost"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(14, 60)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(72, 15)
        Me.CareLabel13.TabIndex = 2
        Me.CareLabel13.Text = "Course Name"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(14, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "From Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCost
        '
        Me.txtCost.CharacterCasing = CharacterCasing.Normal
        Me.txtCost.EnterMoveNextControl = True
        Me.txtCost.Location = New System.Drawing.Point(113, 113)
        Me.txtCost.MaxLength = 14
        Me.txtCost.Name = "txtCost"
        Me.txtCost.NumericAllowNegatives = True
        Me.txtCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCost.Properties.AccessibleName = "Cost"
        Me.txtCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCost.Properties.Appearance.Options.UseFont = True
        Me.txtCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCost.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCost.Properties.MaxLength = 14
        Me.txtCost.Size = New System.Drawing.Size(100, 22)
        Me.txtCost.TabIndex = 4
        Me.txtCost.Tag = "AE"
        Me.txtCost.TextAlign = HorizontalAlignment.Left
        Me.txtCost.ToolTipText = ""
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(113, 29)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.AccessibleDescription = ""
        Me.cdtFrom.Properties.AccessibleName = "Training From Date"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtFrom.TabIndex = 0
        Me.cdtFrom.Tag = "AEM"
        Me.cdtFrom.Value = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(322, 164)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(413, 164)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmStaffTrain
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(510, 197)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStaffTrain"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Training Record"
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.txtVenue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCourse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtCourse As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtCost As Care.Controls.CareTextBox
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents txtVenue As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents gbxWeekly As Care.Controls.CareFrame
End Class
