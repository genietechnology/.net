﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmCredit

    Public Enum EnumMode
        CreateCreditNote
        AmendCreditNote
        ViewCreditNote
    End Enum

    Private m_Mode As EnumMode

    Private m_CreditID As Guid? = Nothing
    Private m_Credit As Business.Credit = Nothing


    Private m_BatchID As Guid = Nothing
    Private m_ChildID As Guid? = Nothing
    Private m_Child As Business.Child = Nothing

    Private m_FamilyID As Guid = Nothing
    Private m_FamilyName As String
    Private m_FamilyFinancialsID As String

    Private m_SiteID As Guid? = Nothing

    Private m_MemoEdit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

    Public Sub New(ByVal Mode As EnumMode, ByVal ID As Guid, ByVal ChildID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Mode = Mode

        If m_Mode = EnumMode.CreateCreditNote Then
            m_BatchID = ID
            m_CreditID = Guid.NewGuid
            m_ChildID = ChildID
        Else
            m_CreditID = ID
            m_Credit = Business.Credit.RetreiveByID(m_CreditID.Value)
            m_BatchID = m_Credit._BatchId.Value
        End If

        'get the site from the batch header
        Dim _H As Business.CreditBatch = Business.CreditBatch.RetreiveByID(m_BatchID)
        m_SiteID = _H._SiteId

    End Sub

    Private Sub frmCredit_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If m_Mode = EnumMode.AmendCreditNote Then Me.Text = "Amend Credit Note"
        If m_Mode = EnumMode.ViewCreditNote Then Me.Text = "View Credit Note"

        btnFinancials.Enabled = False

        If m_Mode = EnumMode.CreateCreditNote Then
            CreateInvoice()
        End If

        If m_Mode = EnumMode.ViewCreditNote Then
            MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls, ControlHandler.DebugMode.None)
        Else
            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls, ControlHandler.DebugMode.None)
        End If

        DisplayInvoice()

        SetButtons()

    End Sub

    Private Sub CreateInvoice()

        m_Credit = New Business.Credit
        With m_Credit

            ._ID = m_CreditID
            ._BatchId = m_BatchID
            ._CreditNo = Credits.LastCreditNo + 1
            ._CreditStatus = "Generated"
            ._CreditDate = Today
            ._CreditDue = Today

            ._ChildId = m_ChildID
            m_Child = Business.Child.RetreiveByID(m_ChildID.Value)
            ._ChildName = m_Child._Fullname

            ._FamilyId = m_Child._FamilyId
            m_FamilyID = ._FamilyId.Value

            Dim _f As Business.Family = Business.Family.RetreiveByID(._FamilyId.Value)
            ._FamilyName = _f._LetterName
            ._FamilyAccountNo = _f._AccountNo
            ._Address = _f._Address
            ._FinancialsId = _f._FinancialsId

            ._Hours = 0
            ._FundedHours = 0
            ._CreditTotal = 0

        End With

    End Sub

    Private Sub DisplayInvoice()

        'we might not have a child, opening balance batches etc.
        If m_Credit._ChildId.HasValue Then
            m_ChildID = m_Credit._ChildId
            m_Child = Business.Child.RetreiveByID(m_ChildID.Value)
            m_Credit._FamilyId = m_Child._FamilyId
        End If

        btnSessions.Enabled = m_Credit._ChildId.HasValue

        txtInvoiceNo.Text = m_Credit._CreditNo.ToString
        txtStatus.Text = m_Credit._CreditStatus

        If m_Credit._CreditDate.HasValue Then txtInvoiceDate.Text = Format(m_Credit._CreditDate, "dd/MM/yyyy")

        txtChild.Text = m_Credit._ChildName
        txtAddress.Text = m_Credit._Address

        txtTOTAL.Text = Format(m_Credit._CreditTotal, "0.00")

        If m_Credit._CreditStatus = "Posted" AndAlso m_Credit._FinancialsDocId <> "" Then
            btnFinancials.Enabled = True
        Else
            btnFinancials.Enabled = False
        End If

        If m_Credit._ChangedBy.HasValue Then
            Dim _u As Care.Shared.Business.User = Care.Shared.Business.User.RetreiveByID(m_Credit._ChangedBy.Value)
            If _u IsNot Nothing Then
                gbxHeader.Text += " - Last Modified by " + _u._Fullname + " @ " + Format(m_Credit._ChangedStamp, "dd/MM/yyyy HH:mm").ToString
            Else
                gbxHeader.Text += " - Last Modified by Deleted User @ " + Format(m_Credit._ChangedStamp, "dd/MM/yyyy HH:mm").ToString
            End If
        End If

        DisplayGrid()

    End Sub

    Private Sub DisplayGrid()
        Session.CursorWaiting()
        DisplayLines()
        Session.CursorDefault()
    End Sub

    Private Sub DisplayLines()

        Dim _SQL As String = ""
        _SQL += "select id, line_no, action_date, datename(weekday, action_date) as 'weekday',"
        _SQL += " description, nl_code, nl_tracking, hours, value"
        _SQL += " from CreditLines"
        _SQL += " where credit_id = '" & m_CreditID.ToString & "'"
        _SQL += " order by line_no"

        grdItems.Populate(Session.ConnectionString, _SQL)

        grdItems.UnderlyingGridControl.BeginUpdate()

        grdItems.Columns("id").Visible = False
        grdItems.Columns("line_no").Caption = "Line No"
        grdItems.Columns("action_date").Caption = "Date"
        grdItems.Columns("weekday").Caption = " "

        grdItems.Columns("description").Caption = "Description"
        grdItems.Columns("description").ColumnEdit = m_MemoEdit

        grdItems.Columns("nl_code").Caption = "NL Code"
        grdItems.Columns("nl_tracking").Caption = "NL Tracking"

        grdItems.Columns("hours").Caption = "Hours"
        grdItems.Columns("value").Caption = "Amount"

        grdItems.AutoSizeColumns()

        grdItems.UnderlyingGridControl.EndUpdate()

    End Sub

    Private Sub ToggleItemButtons(ByVal Enabled As Boolean)

        If Enabled Then

            btnAdd.Enabled = True

            If grdItems.RecordCount > 0 Then
                btnEdit.Enabled = True
                btnRemove.Enabled = True
            Else
                btnEdit.Enabled = False
                btnRemove.Enabled = False
            End If

        Else
            btnAdd.Enabled = False
            btnEdit.Enabled = False
            btnRemove.Enabled = False
        End If

    End Sub

    Private Sub SetButtons()

        Select Case m_Mode

            Case EnumMode.AmendCreditNote

                btnPrint.Enabled = True

                btnSave.Enabled = True
                btnCancel.Enabled = True

                ToggleItemButtons(True)

            Case EnumMode.ViewCreditNote

                btnPrint.Enabled = True

                btnSave.Enabled = False
                btnCancel.Enabled = True

                ToggleItemButtons(False)

        End Select

    End Sub

    Private Sub CalculateTotals()

        Dim _SQL As String = "select isnull(SUM(value),0) as 'TOTAL'" &
                             " from CreditLines" &
                             " where credit_id = '" & m_CreditID.ToString & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            m_Credit._CreditTotal = Decimal.Parse(_DR("TOTAL").ToString)
        Else
            m_Credit._CreditTotal = 0
        End If

        _DR = Nothing

        m_Credit._ChangedBy = Session.CurrentUser.ID
        m_Credit._ChangedStamp = Now

        m_Credit.Store()

        txtTOTAL.Text = Format(m_Credit._CreditTotal, "0.00")

    End Sub

    Private Sub SaveHeader()

        CalculateTotals()

        m_Credit._CreditTotal = Decimal.Parse(txtTOTAL.Text)
        m_Credit.Store()

    End Sub

    Private Sub Save()
        If Not MyControls.Validate(Me.Controls) Then Exit Sub
        SaveHeader()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Save()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click

        If grdItems.RecordCount < 1 Then Exit Sub
        If grdItems.CurrentRow(0).ToString = "" Then Exit Sub

        btnPrint.Enabled = False
        Session.CursorWaiting()

        Dim _CreditFile As String = ParameterHandler.ReturnString("CREDITFILE")

        If _CreditFile = "" Then
            Session.SetProgressMessage("CREDITFILE parameter does not exist or is blank.", 10)
            Exit Sub
        End If

        Dim _SQL As String = ""

        _SQL += "select c.ID, c.credit_ref, c.credit_no, c.credit_date, c.credit_due, c.family_account_no, c.family_name, c.address, c.credit_total,"
        _SQL += " l.line_no, l.action_date, l.description, l.ref_1, l.ref_2, l.ref_3, l.hours, l.funded_hours, l.value, l.funded_value"
        _SQL += " from Credits c"
        _SQL += " left join CreditLines l on l.credit_id = c.ID"
        _SQL += " where c.ID = '" + m_CreditID.ToString + "'"
        _SQL += " order by l.line_no"

        If btnPrint.ShiftDown Then
            ReportHandler.DesignReport(_CreditFile, Session.ConnectionString, _SQL)
        Else
            ReportHandler.RunReport(_CreditFile, Session.ConnectionString, _SQL)
        End If

        btnPrint.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ToggleLine()
    End Sub

    Private Sub EditLine()

        If m_Mode = EnumMode.ViewCreditNote Then Exit Sub
        If grdItems.RecordCount < 1 Then Exit Sub

        If grdItems.CurrentRow(0).ToString <> "" Then
            Dim _id As New Guid(grdItems.CurrentRow(0).ToString)
            ToggleLine(_id)
        End If

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As System.EventArgs) Handles btnEdit.Click
        EditLine()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As System.EventArgs) Handles btnRemove.Click

        If CareMessage("Are you sure you want To Remove the selected Invoice Line?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Remove") = MsgBoxResult.Yes Then

            If grdItems.RecordCount < 1 Then Exit Sub
            If grdItems.CurrentRow(0).ToString <> "" Then

                Dim _id As New Guid(grdItems.CurrentRow(0).ToString)
                Dim _SQL As String = "delete from InvoiceLines where ID = '" & _id.ToString & "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                CalculateTotals()
                DisplayGrid()
                SetButtons()

            End If

        End If

    End Sub

    Private Sub ToggleLine(Optional LineID As Guid? = Nothing)

        Dim _frmLine As New frmCreditLine(m_Credit, LineID)
        _frmLine.ShowDialog()

        If _frmLine.DialogResult = DialogResult.OK Then
            CalculateTotals()
            DisplayGrid()
            SetButtons()
        End If

    End Sub

    Private Sub cbxDiscount_Validated(sender As Object, e As System.EventArgs)
        CalculateTotals()
    End Sub

    Private Sub btnEmail_Click(sender As System.Object, e As System.EventArgs)

        If txtStatus.Text = "Emailed" Then
            If CareMessage("This credit note has already been emailed. Are you sure you want to send another copy?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Email Credit Note") = DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim _Invoices As New List(Of Guid)
        _Invoices.Add(m_CreditID.Value)

        Invoicing.SendInvoices(_Invoices, True)

    End Sub

    Private Sub ToggleAllButtons(ByVal Enabled As Boolean)

        ToggleItemButtons(Enabled)

        btnPrint.Enabled = Enabled
        btnFinancials.Enabled = Enabled

        btnSave.Enabled = Enabled
        btnCancel.Enabled = Enabled

    End Sub

    Private Sub grdItems_GridDoubleClick(sender As Object, e As EventArgs) Handles grdItems.GridDoubleClick
        EditLine()
    End Sub

    Private Sub btnFinancials_Click(sender As Object, e As EventArgs) Handles btnFinancials.Click
        If m_Credit._CreditStatus = "Posted" AndAlso m_Credit._FinancialsDocId <> "" Then
            Dim _E As New Care.Financials.Engine(FinanceShared.ReturnFinancialsIntegration(m_SiteID))
            If _E IsNot Nothing Then
                _E.OpenInvoice(m_Credit._FinancialsDocId)
                _E = Nothing
            End If
        End If
    End Sub

    Private Sub btnSessions_Click(sender As Object, e As EventArgs) Handles btnSessions.Click

        If m_ChildID.HasValue Then

            btnSessions.Enabled = False
            Session.CursorWaiting()

            Business.Child.DrillDown(m_ChildID.Value, True)

            btnSessions.Enabled = True

        End If

    End Sub

    Private Sub btnFamily_Click(sender As Object, e As EventArgs) Handles btnFamily.Click
        Business.Family.DrillDown(m_FamilyID)
    End Sub

End Class
