﻿Imports System.Drawing
Imports System.Windows.Forms
Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports DevExpress.XtraPivotGrid
Imports Care.Controls

Public Class frmPlanner

    Private m_Loading As Boolean = True
    Private m_MemoEdit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

    Private m_ReportWeekData As New List(Of ReportWeek)
    Private m_GridData As New List(Of GridWeek)
    Private m_Tariffs As New List(Of Business.Tariff)
    Private m_RoomRatios As List(Of Business.RoomRatios) = Business.RoomRatios.RetreiveByAge
    Private m_Rooms As List(Of Business.SiteRoom) = Business.SiteRoom.RetreiveAll

    Private m_LayoutDaily As DataRowCollection
    Private m_LayoutWeekly As DataRowCollection

    Private m_WeekCommencing As Date = DateSerial(2014, 10, 13)
    Private m_PointsForStaff As Integer = 1
    Private m_VisibleTimeSlots As Integer
    Private m_RefreshingGrid As Boolean = False

    Private Sub frmPlanner_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_MemoEdit.WordWrap = True

        m_PointsForStaff = ParameterHandler.ReturnInteger("POINTSSTAFF", False)

        With cbxSite
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            .SelectedIndex = 0
        End With

        With cbxSchool
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, name from Schools order by name")
        End With

        'timeslots
        Dim _QT As IEnumerable(Of Business.TimeSlot) = From _T As Business.TimeSlot In Business.TimeSlot.RetreiveAll
                                                       Order By _T._StartTime

        For Each _t As Business.TimeSlot In _QT
            cbxTimeSlots.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_t._ID.Value.ToString.ToUpper, _t._Name))
        Next

        SetVerticalSize(cbxTimeSlots)

        'bolt-ons
        Dim _Q As IEnumerable(Of Business.TariffBoltOn) = From _B As Business.TariffBoltOn In Business.TariffBoltOn.RetreiveAll
                                                          Order By _B._Name

        For Each _b As Business.TariffBoltOn In _Q
            cbxBoltOns.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_b._ID.Value.ToString.ToUpper, _b._Name))
        Next

        SetVerticalSize(cbxBoltOns)

        chkGridOptions.Properties.Items.Add("Use Room from Child Record")
        chkGridOptions.Properties.Items.Add("Hide Waiting")
        chkGridOptions.Properties.Items.Add("Hide Holidays")
        chkGridOptions.Properties.Items.Add("Hide Start/End")
        chkGridOptions.Properties.Items.Add("Show Months/DOB")
        chkGridOptions.Properties.Items.Add("Show School")
        chkGridOptions.Properties.Items.Add("Show Meals")
        chkGridOptions.Properties.Items.Add("Show Bolt-Ons")
        chkGridOptions.Properties.Items.Add("Show Weekends")
        chkGridOptions.Properties.Items.Add("Show Times in Session List")
        chkGridOptions.Properties.Items.Add("Use Initials for Keyworker")
        SetVerticalSize(chkGridOptions)

        cbxSplit.AddItem("", "0")
        cbxSplit.AddItem("2 Groups", "2")
        cbxSplit.AddItem("3 Groups", "3")
        cbxSplit.AddItem("4 Groups", "4")
        cbxSplit.AddItem("5 Groups", "5")
        cbxSplit.SelectedIndex = 0

        cbxSort.AddItem("Forename, Surname")
        cbxSort.AddItem("Surname, Forename")
        cbxSort.AddItem("Youngest > Oldest")
        cbxSort.AddItem("Oldest > Youngest")
        cbxSort.SelectedIndex = 0

        chkAllergies.Properties.Items.Add("No Allergy Text")
        chkAllergies.Properties.Items.Add("Has Allergy Text")
        chkAllergies.Properties.Items.Add("No Allergy Rating")
        chkAllergies.Properties.Items.Add("Moderate Rating")
        chkAllergies.Properties.Items.Add("Serious Rating")
        chkAllergies.Properties.Items.Add("No Diet Notes")
        chkAllergies.Properties.Items.Add("Has Diet Notes")

        For Each _r As DataRow In ListHandler.ReturnItems("Diet Restrictions")
            chkAllergies.Properties.Items.Add(_r.Item("name").ToString)
        Next

        SetVerticalSize(chkAllergies)

        PopulateLogic(cbxLogicBoltOns)
        PopulateLogic(cbxLogicTariff)
        PopulateLogic(cbxLogicTimes)

        chkMonday.Checked = True
        chkTuesday.Checked = True
        chkWednesday.Checked = True
        chkThursday.Checked = True
        chkFriday.Checked = True

        Dim _Options As String() = UserPreferenceHandler.ReturnString("REGISTEROPTIONS").Split(CChar(","))
        For Each _Option In _Options
            CheckOption(chkGridOptions, _Option)
        Next

        If ParameterHandler.ReturnBoolean("REGROOMCHILD") Then
            CheckOption(chkGridOptions, "Use Room from Child Record")
        End If

        m_LayoutDaily = ListHandler.ReturnItemsWithReferences("Register Daily Layout")
        m_LayoutWeekly = ListHandler.ReturnItemsWithReferences("Register Weekly Layout")

        btnMonday.PopulateMenuItems(m_LayoutDaily)
        btnTuesday.PopulateMenuItems(m_LayoutDaily)
        btnWednesday.PopulateMenuItems(m_LayoutDaily)
        btnThursday.PopulateMenuItems(m_LayoutDaily)
        btnFriday.PopulateMenuItems(m_LayoutDaily)
        btnSaturday.PopulateMenuItems(m_LayoutDaily)
        btnSunday.PopulateMenuItems(m_LayoutDaily)

        btnPrint.PopulateMenuItems(m_LayoutWeekly)

        Dim _WC As Date = ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
        cdtWC.DateTime = _WC
        DisplayWC(_WC)

    End Sub

    Private Sub CheckOption(ByRef CheckList As DevExpress.XtraEditors.CheckedComboBoxEdit, ByVal OptionText As String)
        For Each _i As DevExpress.XtraEditors.Controls.CheckedListBoxItem In CheckList.Properties.Items
            If _i.Value.ToString.Trim = OptionText.Trim Then
                _i.CheckState = CheckState.Checked
                Exit For
            End If
        Next
    End Sub

    Private Sub PopulateLogic(ByRef Combo As Care.Controls.CareComboBox)
        Combo.Clear()
        Combo.AddItem("Any")
        Combo.AddItem("All")
        Combo.AddItem("Not")
        Combo.SelectedIndex = 0
    End Sub

    Private Sub frmPlanner_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        m_Loading = False
        SetLayout()
    End Sub

    Private Sub DisplayRegister(ByVal WC As Date)

        SetLayout()
        SetButtons(False)
        If Not m_Loading Then Session.WaitFormShow()
        Session.CursorWaiting()

        m_WeekCommencing = WC

        If radSessions.Checked Or radTimes.Checked Then
            NewBuildGrid(m_WeekCommencing)
        Else
            BuildPivotGrid(m_WeekCommencing)
        End If

        SetButtons(True)
        Session.CursorDefault()
        If Not m_Loading Then Session.WaitFormClose()

    End Sub

    Private Sub RefreshGrid()

        If cgWC.RecordCount <= 0 Then Exit Sub
        If cgWC.FocusedRowHandle < 0 Then Exit Sub

        If m_RefreshingGrid Then Exit Sub

        m_RefreshingGrid = True
        DisplayRegister(ValueHandler.ConvertDate(cgWC.CurrentRow("WC")).Value)
        m_RefreshingGrid = False

    End Sub

    Private Function BuildClauses(ByVal FieldName As String, Logic As String, ByVal SelectedValues As String) As String

        Dim _Clauses As String = ""

        Dim _AndOr As String = " AND "
        Dim _Like As String = " like "

        If Logic = "Not" Then _Like = " not like "
        If Logic = "Any" Then _AndOr = " OR "

        Dim _Split As String() = SelectedValues.Split(CChar(","))
        For Each _Value In _Split
            If _Clauses = "" Then
                _Clauses += FieldName + _Like + "'%" + _Value.Trim + "%'"
            Else
                _Clauses += _AndOr + FieldName + _Like + "'%" + _Value.Trim + "%'"
            End If
        Next

        Return _Clauses

    End Function

    Private Function FilterTariffs() As String
        If cbxTariff.Text <> "" Then
            Dim _Clauses As String = BuildClauses("b.tariff_name", cbxLogicTariff.Text, cbxTariff.Text)
            Return " AND (" + _Clauses + ")"
        Else
            Return ""
        End If
    End Function

    Private Function FilterTimeSlots() As String
        If cbxTimeSlots.Text <> "" Then
            Dim _Clauses As String = BuildClauses("b.timeslots", cbxLogicTimes.Text, cbxTimeSlots.Text)
            Return " AND (" + _Clauses + ")"
        Else
            Return ""
        End If
    End Function

    Private Function FilterBoltOns() As String
        If cbxBoltOns.Text <> "" Then
            Dim _Clauses As String = BuildClauses("b.bolt_ons", cbxLogicBoltOns.Text, cbxBoltOns.Text)
            Return " AND (" + _Clauses + ")"
        Else
            Return ""
        End If
    End Function

    Private Function FilterMeals() As String

        Dim _Return As String = ""
        Dim _Clauses As String = ""

        If chkBreakfast.Checked Then
            If _Clauses = "" Then
                _Clauses += "b.breakfast = 1"
            Else
                _Clauses += " and b.breakfast = 1"
            End If
        End If

        If chkLunch.Checked Then
            If _Clauses = "" Then
                _Clauses += "b.lunch = 1"
            Else
                _Clauses += " and b.lunch = 1"
            End If
        End If

        If chkTea.Checked Then
            If _Clauses = "" Then
                _Clauses += "b.tea = 1"
            Else
                _Clauses += " and b.tea = 1"
            End If
        End If

        If _Clauses <> "" Then _Return = " AND (" + _Clauses + ")"
        Return _Return

    End Function

    Private Function AndOr(ByRef LogicCombo As Care.Controls.CareComboBox) As String
        If LogicCombo.Text = "Any" Then
            Return " or "
        Else
            Return " and "
        End If
    End Function

    Private Function FilterTags() As String
        If cbxTagFilter.Text <> "" Then
            Dim _Clauses As String = ""
            Dim _Split As String() = cbxTagFilter.Text.Split(CChar(","))
            For Each _Value In _Split
                If _Clauses = "" Then
                    _Clauses += "c.tags like '%" + _Value.Trim + "%'"
                Else
                    _Clauses += " OR c.tags like '%" + _Value.Trim + "%'"
                End If
            Next
            Dim _Return As String = " AND (" + _Clauses + ")"
            Return _Return
        Else
            Return ""
        End If
    End Function

    Private Function FilterAllergiesAndDiet() As String

        Dim _Clauses As String = ""

        If chkAllergies.Text <> "" Then

            Dim _Split As String() = chkAllergies.Text.Split(CChar(","))
            For Each _Value In _Split

                Dim _PresetFilter As String = FilterPresetAllergy(_Value)
                If _PresetFilter <> "" Then
                    If _Clauses <> "" Then _Clauses += " AND "
                    _Clauses += _PresetFilter
                Else
                    'must be a diet restriction then
                    If _Clauses <> "" Then _Clauses += " AND "
                    _Clauses += "c.diet_restrict = '" + _Value.Trim + "'"
                End If

            Next

        End If

        If _Clauses <> "" Then
            Return " AND (" + _Clauses + ")"
        Else
            Return ""
        End If

    End Function

    Private Function FilterPresetAllergy(ByVal SelectedOption As String) As String

        Dim _Clauses As String = ""
        Dim _PresetAllergy As Boolean = False

        SelectedOption = SelectedOption.Trim

        If SelectedOption = "No Allergy Text" Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "len(c.med_allergies) = 0"
        End If

        If SelectedOption = ("Has Allergy Text") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "len(c.med_allergies) > 0"
        End If

        If SelectedOption = ("No Allergy Rating") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "c.allergy_rating = 'None'"
        End If

        If SelectedOption = ("Moderate Rating") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "c.allergy_rating = 'Moderate'"
        End If

        If SelectedOption = ("Serious Rating") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "c.allergy_rating = 'Serious'"
        End If

        If SelectedOption = ("No Diet Notes") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "len(c.diet_notes) = 0"
        End If

        If SelectedOption = ("Has Diet Notes") Then
            _PresetAllergy = True
            If _Clauses <> "" Then _Clauses += " AND "
            _Clauses += "len(c.diet_notes) > 0"
        End If

        Return _Clauses

    End Function

    Private Sub NewBuildGrid(ByVal WC As Date)

        m_GridData.Clear()
        cgPlanner.Clear()

        Dim _SQL As String = ""
        Dim _Sunday As Date = WC.AddDays(6)

        Dim _Fields As String = ""
        Dim _Joins As String = ""

        Dim _DayNo As Integer = 0
        While _DayNo < 7

            Dim _Date As Date = WC.AddDays(_DayNo)
            Dim _Day As String = _Date.DayOfWeek.ToString.ToLower.Substring(0, 3)

            _Fields += ReturnFields(_Day)
            _Joins += ReturnJoin(_Day, _Date)

            _DayNo += 1

        End While

        _SQL += "select distinct(b.child_id),"
        _SQL += vbCrLf
        _SQL += "c.fullname, c.surname_forename, c.dob, c.group_id, stf.forename as 'kw_forename', stf.surname as 'kw_surname', sch.name as 'school',"
        _SQL += " c.started, c.date_left, c.move_mode, c.move_date, c.move_ratio_id, c.school_ref_1, c.school_ref_2, c.tags,"
        _SQL += vbCrLf

        _SQL += _Fields

        _SQL += "from Bookings b"
        _SQL += vbCrLf

        _SQL += _Joins

        _SQL += "left join Children c on c.ID = b.child_id"
        _SQL += vbCrLf
        _SQL += "left join Schools sch on sch.ID = c.school"
        _SQL += vbCrLf
        _SQL += "left join Staff stf on stf.ID = c.keyworker_id"
        _SQL += vbCrLf

        _SQL += FilterDays("b.booking_date", WC, _Sunday)

        'site filtering by user
        _SQL += " and c.site_id not in ("
        _SQL += "select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "'"
        _SQL += ")"
        _SQL += vbCrLf

        If cbxSite.Text <> "" Then
            _SQL += " and c.site_id = '" & cbxSite.SelectedValue.ToString & "'"
            _SQL += vbCrLf
        End If

        If cbxSchool.Text <> "" Then
            _SQL += " and c.school = '" & cbxSchool.SelectedValue.ToString & "'"
            _SQL += vbCrLf
        End If

        If chkGridOptions.Text.Contains("Hide Waiting") Then
            _SQL += " and b.booking_status <> 'Waiting'"
            _SQL += vbCrLf
        End If

        If chkGridOptions.Text.Contains("Hide Holidays") Then
            _SQL += " and b.booking_status <> 'Holiday'"
            _SQL += vbCrLf
        End If

        _SQL += FilterTariffs()
        _SQL += FilterTimeSlots()
        _SQL += FilterBoltOns()
        _SQL += FilterMeals()
        _SQL += FilterTags()
        _SQL += FilterAllergiesAndDiet()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            PopulateWeekGrid(_DT, m_WeekCommencing)
            cgPlanner.Populate(m_GridData)
            FormatGrid()

            If cbxSort.SelectedIndex = 0 Then cgPlanner.Columns("ChildName").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            If cbxSort.SelectedIndex = 1 Then cgPlanner.Columns("ChildName").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            If cbxSort.SelectedIndex = 2 Then cgPlanner.Columns("ChildDOB").SortOrder = DevExpress.Data.ColumnSortOrder.Descending
            If cbxSort.SelectedIndex = 3 Then cgPlanner.Columns("ChildDOB").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

            If cgPlanner.RecordCount > 0 Then cgPlanner.MoveFirst()

        End If

    End Sub

    Private Function FilterDays(ByVal DateField As String, ByVal WC As Date, ByVal Sunday As Date) As String

        Dim _In As String = ""

        If chkMonday.Checked Then
            If _In <> "" Then _In += ", "
            _In += ValueHandler.SQLDate(WC, True)
        End If

        If chkTuesday.Checked Then
            If _In <> "" Then _In += ", "
            _In += ValueHandler.SQLDate(WC.AddDays(1), True)
        End If

        If chkWednesday.Checked Then
            If _In <> "" Then _In += ", "
            _In += ValueHandler.SQLDate(WC.AddDays(2), True)
        End If

        If chkThursday.Checked Then
            If _In <> "" Then _In += ", "
            _In += ValueHandler.SQLDate(WC.AddDays(3), True)
        End If

        If chkFriday.Checked Then
            If _In <> "" Then _In += ", "
            _In += ValueHandler.SQLDate(WC.AddDays(4), True)
        End If

        Dim _SQL As String = ""

        If _In = "" Then
            _SQL += "where " + DateField + " between " + ValueHandler.SQLDate(WC, True) + " and " + ValueHandler.SQLDate(Sunday, True)
        Else
            _SQL += "where " + DateField + " in (" + _In + ")"
        End If

        Return _SQL

    End Function

    Private Sub PopulateWeekGrid(ByRef DT As DataTable, ByVal WC As Date)

        If DT Is Nothing Then Exit Sub
        If DT.Rows.Count = 0 Then Exit Sub

        Session.SetupProgressBar("Processing...", DT.Rows.Count)

        For Each _DR As DataRow In DT.Rows

            Dim _DOB As Date? = ValueHandler.ConvertDate(_DR.Item("dob"))

            Dim _R As New GridWeek
            With _R

                .ChildID = New Guid(_DR.Item("child_id").ToString)

                If cbxSort.SelectedIndex = 1 Then
                    .ChildName = _DR.Item("surname_forename").ToString
                Else
                    .ChildName = _DR.Item("fullname").ToString
                End If

                If _DOB.HasValue Then
                    .ChildAge = ValueHandler.DateDifferenceAsTextYM(_DOB.Value, Today)
                    .ChildDOB = _DOB.Value
                    .ChildMonths = ValueHandler.ReturnExactMonths(_DOB.Value, WC)
                End If

                Dim _MoveMode As String = _DR.Item("move_mode").ToString
                Dim _MoveDate As Date? = ValueHandler.ConvertDate(_DR.Item("move_date"))
                Dim _MoveRatioID As Guid? = Nothing
                If _DR.Item("move_ratio_id").ToString <> "" Then _MoveRatioID = New Guid(_DR.Item("move_ratio_id").ToString)

                SetRoomDetails(.ChildRoom, .RoomSequence, New Guid(_DR.Item("group_id").ToString), WC, _MoveMode, _MoveDate, _MoveRatioID,
                                         _DR.Item("mon_ratio_id").ToString, _DR.Item("tue_ratio_id").ToString,
                                         _DR.Item("wed_ratio_id").ToString, _DR.Item("thu_ratio_id").ToString,
                                         _DR.Item("fri_ratio_id").ToString, _DR.Item("sat_ratio_id").ToString,
                                         _DR.Item("sun_ratio_id").ToString)

                'group filter
                If cbxGroup.Text <> "" Then
                    If cbxGroup.Text <> .ChildRoom Then
                        Continue For
                    End If
                End If

                .ChildKeyworker = BuildKeyworker(_DR.Item("kw_forename").ToString, _DR.Item("kw_surname").ToString)
                .ChildSchool = _DR.Item("school").ToString
                .ChildStart = CleanData.ReturnDate(_DR.Item("started"))
                .ChildLeave = CleanData.ReturnDate(_DR.Item("date_left"))

                .Monday = BuildDayText(_DR.Item("mon_booking_from"), _DR.Item("mon_booking_to"), _DR.Item("mon_tariff_name").ToString, _DR.Item("mon_void_from"), _DR.Item("mon_void_to"), _DR.Item("mon_times").ToString)
                .MondayAM = CleanData.ReturnInteger(_DR.Item("mon_am"))
                .MondayPM = CleanData.ReturnInteger(_DR.Item("mon_pm"))
                .MondayBreakfast = SetCheckbox(_DR.Item("mon_breakfast"))
                .MondayLunch = SetCheckbox(_DR.Item("mon_lunch"))
                .MondayTea = SetCheckbox(_DR.Item("mon_tea"))
                .MondaySnack = SetCheckbox(_DR.Item("mon_snacks"))
                .MondayHeadcount = GetMax(.MondayAM, .MondayPM)
                .MondayColour = GetTariffColour(_DR.Item("mon_tariff_id").ToString)
                .MondayStatus = _DR.Item("mon_booking_status").ToString
                .MondayBoltOns = _DR.Item("mon_bolt_ons").ToString

                .Tuesday = BuildDayText(_DR.Item("tue_booking_from"), _DR.Item("tue_booking_to"), _DR.Item("tue_tariff_name").ToString, _DR.Item("tue_void_from"), _DR.Item("tue_void_to"), _DR.Item("tue_times").ToString)
                .TuesdayAM = CleanData.ReturnInteger(_DR.Item("tue_am"))
                .TuesdayPM = CleanData.ReturnInteger(_DR.Item("tue_pm"))
                .TuesdayBreakfast = SetCheckbox(_DR.Item("tue_breakfast"))
                .TuesdayLunch = SetCheckbox(_DR.Item("tue_lunch"))
                .TuesdayTea = SetCheckbox(_DR.Item("tue_tea"))
                .TuesdaySnack = SetCheckbox(_DR.Item("tue_snacks"))
                .TuesdayHeadcount = GetMax(.TuesdayAM, .TuesdayPM)
                .TuesdayColour = GetTariffColour(_DR.Item("tue_tariff_id").ToString)
                .TuesdayStatus = _DR.Item("tue_booking_status").ToString
                .TuesdayBoltOns = _DR.Item("tue_bolt_ons").ToString

                .Wednesday = BuildDayText(_DR.Item("wed_booking_from"), _DR.Item("wed_booking_to"), _DR.Item("wed_tariff_name").ToString, _DR.Item("wed_void_from"), _DR.Item("wed_void_to"), _DR.Item("wed_times").ToString)
                .WednesdayAM = CleanData.ReturnInteger(_DR.Item("wed_am"))
                .WednesdayPM = CleanData.ReturnInteger(_DR.Item("wed_pm"))
                .WednesdayBreakfast = SetCheckbox(_DR.Item("wed_breakfast"))
                .WednesdayLunch = SetCheckbox(_DR.Item("wed_lunch"))
                .WednesdayTea = SetCheckbox(_DR.Item("wed_tea"))
                .WednesdaySnack = SetCheckbox(_DR.Item("wed_snacks"))
                .WednesdayHeadcount = GetMax(.WednesdayAM, .WednesdayPM)
                .WednesdayColour = GetTariffColour(_DR.Item("wed_tariff_id").ToString)
                .WednesdayStatus = _DR.Item("wed_booking_status").ToString
                .WednesdayBoltOns = _DR.Item("wed_bolt_ons").ToString

                .Thursday = BuildDayText(_DR.Item("thu_booking_from"), _DR.Item("thu_booking_to"), _DR.Item("thu_tariff_name").ToString, _DR.Item("thu_void_from"), _DR.Item("thu_void_to"), _DR.Item("thu_times").ToString)
                .ThursdayAM = CleanData.ReturnInteger(_DR.Item("thu_am"))
                .ThursdayPM = CleanData.ReturnInteger(_DR.Item("thu_pm"))
                .ThursdayBreakfast = SetCheckbox(_DR.Item("thu_breakfast"))
                .ThursdayLunch = SetCheckbox(_DR.Item("thu_lunch"))
                .ThursdayTea = SetCheckbox(_DR.Item("thu_tea"))
                .ThursdaySnack = SetCheckbox(_DR.Item("thu_snacks"))
                .ThursdayHeadcount = GetMax(.ThursdayAM, .ThursdayPM)
                .ThursdayColour = GetTariffColour(_DR.Item("thu_tariff_id").ToString)
                .ThursdayStatus = _DR.Item("thu_booking_status").ToString
                .ThursdayBoltOns = _DR.Item("thu_bolt_ons").ToString

                .Friday = BuildDayText(_DR.Item("fri_booking_from"), _DR.Item("fri_booking_to"), _DR.Item("fri_tariff_name").ToString, _DR.Item("fri_void_from"), _DR.Item("fri_void_to"), _DR.Item("fri_times").ToString)
                .FridayAM = CleanData.ReturnInteger(_DR.Item("fri_am"))
                .FridayPM = CleanData.ReturnInteger(_DR.Item("fri_pm"))
                .FridayBreakfast = SetCheckbox(_DR.Item("fri_breakfast"))
                .FridayLunch = SetCheckbox(_DR.Item("fri_lunch"))
                .FridayTea = SetCheckbox(_DR.Item("fri_tea"))
                .FridaySnack = SetCheckbox(_DR.Item("fri_snacks"))
                .FridayHeadcount = GetMax(.FridayAM, .FridayPM)
                .FridayColour = GetTariffColour(_DR.Item("fri_tariff_id").ToString)
                .FridayStatus = _DR.Item("fri_booking_status").ToString
                .FridayBoltOns = _DR.Item("fri_bolt_ons").ToString

                .Saturday = BuildDayText(_DR.Item("sat_booking_from"), _DR.Item("sat_booking_to"), _DR.Item("sat_tariff_name").ToString, _DR.Item("sat_void_from"), _DR.Item("sat_void_to"), _DR.Item("sat_times").ToString)
                .SaturdayAM = CleanData.ReturnInteger(_DR.Item("sat_am"))
                .SaturdayPM = CleanData.ReturnInteger(_DR.Item("sat_pm"))
                .SaturdayBreakfast = SetCheckbox(_DR.Item("sat_breakfast"))
                .SaturdayLunch = SetCheckbox(_DR.Item("sat_lunch"))
                .SaturdayTea = SetCheckbox(_DR.Item("sat_tea"))
                .SaturdaySnack = SetCheckbox(_DR.Item("sat_snacks"))
                .SaturdayHeadcount = GetMax(.SaturdayAM, .SaturdayPM)
                .SaturdayColour = GetTariffColour(_DR.Item("sat_tariff_id").ToString)
                .SaturdayStatus = _DR.Item("sat_booking_status").ToString
                .SaturdayBoltOns = _DR.Item("sat_bolt_ons").ToString

                .Sunday = BuildDayText(_DR.Item("sun_booking_from"), _DR.Item("sun_booking_to"), _DR.Item("sun_tariff_name").ToString, _DR.Item("sun_void_from"), _DR.Item("sun_void_to"), _DR.Item("sun_times").ToString)
                .SundayAM = CleanData.ReturnInteger(_DR.Item("sun_am"))
                .SundayPM = CleanData.ReturnInteger(_DR.Item("sun_pm"))
                .SundayBreakfast = SetCheckbox(_DR.Item("sun_breakfast"))
                .SundayLunch = SetCheckbox(_DR.Item("sun_lunch"))
                .SundayTea = SetCheckbox(_DR.Item("sun_tea"))
                .SundaySnack = SetCheckbox(_DR.Item("sun_snacks"))
                .SundayHeadcount = GetMax(.SundayAM, .SundayPM)
                .SundayColour = GetTariffColour(_DR.Item("sun_tariff_id").ToString)
                .SundayStatus = _DR.Item("sun_booking_status").ToString
                .SundayBoltOns = _DR.Item("sun_bolt_ons").ToString

                .ChildRef1 = _DR.Item("school_ref_1").ToString
                .ChildRef2 = _DR.Item("school_ref_2").ToString
                .ChildTags = _DR.Item("tags").ToString

            End With

            m_GridData.Add(_R)
            Session.StepProgressBar()

        Next

        Session.HideProgressBar()

    End Sub

    Private Function SetCheckbox(ObjectIn As Object) As Boolean
        Dim _i As Integer = CleanData.ReturnInteger(ObjectIn)
        If _i = 1 Then Return True
        Return False
    End Function

    Private Function GetMax(ByVal AM As Integer, ByVal PM As Integer) As Integer
        If AM >= PM Then
            Return AM
        Else
            Return PM
        End If
    End Function

    Private Function BuildDayText(ByVal BookingFrom As Object, ByVal BookingTo As Object, ByVal TariffName As String, ByVal VoidFrom As Object, ByVal VoidTo As Object, ByVal Times As String) As String

        If radTimes.Checked Then

            Dim _Times As String = ""
            Dim _From As Date? = ValueHandler.ReturnTime(BookingFrom)
            Dim _To As Date? = ValueHandler.ReturnTime(BookingTo)

            If _From.HasValue AndAlso _To.HasValue Then

                Dim _VoidFrom As Date? = ValueHandler.ReturnTime(VoidFrom)
                Dim _VoidTo As Date? = ValueHandler.ReturnTime(VoidTo)

                If _VoidFrom.HasValue AndAlso _VoidTo.HasValue Then

                    'we have a void period, so we split the booking around this
                    Dim _FirstPeriodFrom As Date? = _From
                    Dim _FirstPeriodTo As Date? = _VoidFrom
                    Dim _FirstPeriod As String = Format(_FirstPeriodFrom, "HH:mm") + "-" + Format(_FirstPeriodTo, "HH:mm")

                    Dim _SecondPeriodFrom As Date? = _VoidTo
                    Dim _SecondPeriodTo As Date? = _To
                    Dim _SecondPeriod As String = Format(_SecondPeriodFrom, "HH:mm") + "-" + Format(_SecondPeriodTo, "HH:mm")

                    Dim _BookingPeriod As String = _FirstPeriod + " and " + _SecondPeriod

                    _Times = _BookingPeriod

                Else
                    _Times = Format(_From, "HH:mm") + " - " + Format(_To, "HH:mm")
                End If

            End If

            Return _Times

        Else
            If chkGridOptions.Text.Contains("Show Times in Session List") Then
                Return Times
            Else
                Return TariffName
            End If
        End If

    End Function

    Private Function BuildKeyworker(ByVal Forename As String, ByVal Surname As String) As String
        If chkGridOptions.Text.Contains("Use Initials for Keyworker") Then
            Return Forename.First.ToString + Surname.First.ToString
        Else
            Return Forename + " " + Surname
        End If
    End Function

    Private Function ReturnFields(ByVal DayAlias As String) As String

        Dim _SQL As String = ""

        Dim _IsSunday As Boolean = False
        If DayAlias = "sun" Then _IsSunday = True

        _SQL += ReturnField(DayAlias, "ratio_id", False, False)
        _SQL += ReturnField(DayAlias, "booking_status", False, False)
        _SQL += ReturnField(DayAlias, "booking_from", False, False)
        _SQL += ReturnField(DayAlias, "booking_to", False, False)
        _SQL += ReturnField(DayAlias, "void_from", False, False)
        _SQL += ReturnField(DayAlias, "void_to", False, False)
        _SQL += ReturnField(DayAlias, "tariff_id", False, False)
        _SQL += ReturnField(DayAlias, "tariff_name", False, False)
        _SQL += ReturnField(DayAlias, "bolt_ons", False, False)
        _SQL += ReturnField(DayAlias, "am", True, False)
        _SQL += ReturnField(DayAlias, "pm", True, False)
        _SQL += ReturnField(DayAlias, "breakfast", True, False)
        _SQL += ReturnField(DayAlias, "lunch", True, False)
        _SQL += ReturnField(DayAlias, "tea", True, False)
        _SQL += ReturnField(DayAlias, "snacks", True, False)
        _SQL += ReturnField(DayAlias, "hours", True, False)
        _SQL += ReturnField(DayAlias, "times", False, _IsSunday)
        _SQL += vbCrLf

        Return _SQL

    End Function

    Private Function ReturnField(ByVal DayAlias As String, ByVal Field As String, ByVal IsNull As Boolean, ByVal LastField As Boolean) As String

        Dim _SQL As String = ""

        If IsNull Then _SQL += "isnull("

        _SQL += DayAlias + "." + Field

        If IsNull Then _SQL += ",0)"

        _SQL += " as '" + DayAlias + "_" + Field + "'"

        If Not LastField Then _SQL += ", "

        Return _SQL

    End Function

    Private Function ReturnJoin(ByVal DayAlias As String, ByVal DayDate As Date) As String
        Dim _SQL As String = ""
        _SQL += "left join Bookings " + DayAlias + " on " + DayAlias + ".child_id = b.child_id and " + DayAlias + ".booking_date = " + ValueHandler.SQLDate(DayDate, True)
        _SQL += vbCrLf
        Return _SQL
    End Function

#Region "WC Grid"

    Private Sub DisplayWC(ByVal WCFrom As Date)

        Dim _SQL As String = ""

        _SQL += "select date as 'WC' from Calendar"
        _SQL += " where dayofweek = 'Monday'"
        _SQL += " and date >= " + ValueHandler.SQLDate(WCFrom, True)
        _SQL += " order by date"

        cgWC.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgWC_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgWC.FocusedRowChanged
        RefreshGrid()
    End Sub

#End Region

#Region "Standard Grid"

    Private Sub FormatGrid()

        cgPlanner.UnderlyingGridView.BeginDataUpdate()

        cgPlanner.Columns("ChildID").Visible = False
        cgPlanner.Columns("RoomSequence").Visible = False

        cgPlanner.Columns("Monday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Tuesday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Wednesday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Thursday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Friday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Saturday").ColumnEdit = m_MemoEdit
        cgPlanner.Columns("Sunday").ColumnEdit = m_MemoEdit

        If chkGridOptions.Text.Contains("Show Months/DOB") Then
            cgPlanner.Columns("ChildDOB").Visible = True
            cgPlanner.Columns("ChildMonths").Visible = True
        Else
            cgPlanner.Columns("ChildDOB").Visible = False
            cgPlanner.Columns("ChildMonths").Visible = False
        End If

        If chkGridOptions.Text.Contains("Show School") Then
            cgPlanner.Columns("ChildSchool").Visible = True
        Else
            cgPlanner.Columns("ChildSchool").Visible = False
        End If

        If chkGridOptions.Text.Contains("Show Weekends") Then
            cgPlanner.Columns("Saturday").Visible = True
            cgPlanner.Columns("Sunday").Visible = True
        Else
            cgPlanner.Columns("Saturday").Visible = False
            cgPlanner.Columns("Sunday").Visible = False
            cgPlanner.Columns("SaturdayBreakfast").Visible = False
            cgPlanner.Columns("SaturdayLunch").Visible = False
            cgPlanner.Columns("SaturdayTea").Visible = False
            cgPlanner.Columns("SaturdaySnack").Visible = False
            cgPlanner.Columns("SundayBreakfast").Visible = False
            cgPlanner.Columns("SundayLunch").Visible = False
            cgPlanner.Columns("SundayTea").Visible = False
            cgPlanner.Columns("SundaySnack").Visible = False
        End If

        If chkGridOptions.Text.Contains("Hide Start/End") Then
            cgPlanner.Columns("ChildStart").Visible = False
            cgPlanner.Columns("ChildLeave").Visible = False
        Else
            cgPlanner.Columns("ChildStart").Visible = True
            cgPlanner.Columns("ChildLeave").Visible = True
        End If

        If chkGridOptions.Text.Contains("Show Meals") Then

            cgPlanner.Columns("MondayBreakfast").Visible = True
            cgPlanner.Columns("MondayLunch").Visible = True
            cgPlanner.Columns("MondayTea").Visible = True
            cgPlanner.Columns("MondaySnack").Visible = True
            cgPlanner.Columns("TuesdayBreakfast").Visible = True
            cgPlanner.Columns("TuesdayLunch").Visible = True
            cgPlanner.Columns("TuesdayTea").Visible = True
            cgPlanner.Columns("TuesdaySnack").Visible = True
            cgPlanner.Columns("WednesdayBreakfast").Visible = True
            cgPlanner.Columns("WednesdayLunch").Visible = True
            cgPlanner.Columns("WednesdayTea").Visible = True
            cgPlanner.Columns("WednesdaySnack").Visible = True
            cgPlanner.Columns("ThursdayBreakfast").Visible = True
            cgPlanner.Columns("ThursdayLunch").Visible = True
            cgPlanner.Columns("ThursdayTea").Visible = True
            cgPlanner.Columns("ThursdaySnack").Visible = True
            cgPlanner.Columns("FridayBreakfast").Visible = True
            cgPlanner.Columns("FridayLunch").Visible = True
            cgPlanner.Columns("FridayTea").Visible = True
            cgPlanner.Columns("FridaySnack").Visible = True

            cgPlanner.Columns("MondayBreakfast").Caption = "B"
            cgPlanner.Columns("MondayLunch").Caption = "L"
            cgPlanner.Columns("MondayTea").Caption = "T"
            cgPlanner.Columns("MondaySnack").Caption = "S"
            cgPlanner.Columns("TuesdayBreakfast").Caption = "B"
            cgPlanner.Columns("TuesdayLunch").Caption = "L"
            cgPlanner.Columns("TuesdayTea").Caption = "T"
            cgPlanner.Columns("TuesdaySnack").Caption = "S"
            cgPlanner.Columns("WednesdayBreakfast").Caption = "B"
            cgPlanner.Columns("WednesdayLunch").Caption = "L"
            cgPlanner.Columns("WednesdayTea").Caption = "T"
            cgPlanner.Columns("WednesdaySnack").Caption = "S"
            cgPlanner.Columns("ThursdayBreakfast").Caption = "B"
            cgPlanner.Columns("ThursdayLunch").Caption = "L"
            cgPlanner.Columns("ThursdayTea").Caption = "T"
            cgPlanner.Columns("ThursdaySnack").Caption = "S"
            cgPlanner.Columns("FridayBreakfast").Caption = "B"
            cgPlanner.Columns("FridayLunch").Caption = "L"
            cgPlanner.Columns("FridayTea").Caption = "T"
            cgPlanner.Columns("FridaySnack").Caption = "S"

            If chkGridOptions.Text.Contains("Show Weekends") Then
                cgPlanner.Columns("SaturdayBreakfast").Visible = True
                cgPlanner.Columns("SaturdayLunch").Visible = True
                cgPlanner.Columns("SaturdayTea").Visible = True
                cgPlanner.Columns("SaturdaySnack").Visible = True
                cgPlanner.Columns("SundayBreakfast").Visible = True
                cgPlanner.Columns("SundayLunch").Visible = True
                cgPlanner.Columns("SundayTea").Visible = True
                cgPlanner.Columns("SundaySnack").Visible = True

                cgPlanner.Columns("SaturdayBreakfast").Caption = "B"
                cgPlanner.Columns("SaturdayLunch").Caption = "L"
                cgPlanner.Columns("SaturdayTea").Caption = "T"
                cgPlanner.Columns("SaturdaySnack").Caption = "S"
                cgPlanner.Columns("SundayBreakfast").Caption = "B"
                cgPlanner.Columns("SundayLunch").Caption = "L"
                cgPlanner.Columns("SundayTea").Caption = "T"
                cgPlanner.Columns("SundaySnack").Caption = "S"
            End If

        Else
            cgPlanner.Columns("MondayBreakfast").Visible = False
            cgPlanner.Columns("MondayLunch").Visible = False
            cgPlanner.Columns("MondayTea").Visible = False
            cgPlanner.Columns("MondaySnack").Visible = False
            cgPlanner.Columns("TuesdayBreakfast").Visible = False
            cgPlanner.Columns("TuesdayLunch").Visible = False
            cgPlanner.Columns("TuesdayTea").Visible = False
            cgPlanner.Columns("TuesdaySnack").Visible = False
            cgPlanner.Columns("WednesdayBreakfast").Visible = False
            cgPlanner.Columns("WednesdayLunch").Visible = False
            cgPlanner.Columns("WednesdayTea").Visible = False
            cgPlanner.Columns("WednesdaySnack").Visible = False
            cgPlanner.Columns("ThursdayBreakfast").Visible = False
            cgPlanner.Columns("ThursdayLunch").Visible = False
            cgPlanner.Columns("ThursdayTea").Visible = False
            cgPlanner.Columns("ThursdaySnack").Visible = False
            cgPlanner.Columns("FridayBreakfast").Visible = False
            cgPlanner.Columns("FridayLunch").Visible = False
            cgPlanner.Columns("FridayTea").Visible = False
            cgPlanner.Columns("FridaySnack").Visible = False
            cgPlanner.Columns("SaturdayBreakfast").Visible = False
            cgPlanner.Columns("SaturdayLunch").Visible = False
            cgPlanner.Columns("SaturdayTea").Visible = False
            cgPlanner.Columns("SaturdaySnack").Visible = False
            cgPlanner.Columns("SundayBreakfast").Visible = False
            cgPlanner.Columns("SundayLunch").Visible = False
            cgPlanner.Columns("SundayTea").Visible = False
            cgPlanner.Columns("SundaySnack").Visible = False
        End If

        If chkGridOptions.Text.Contains("Show Bolt-Ons") Then
            If chkGridOptions.Text.Contains("Show Weekends") Then
                cgPlanner.Columns("MondayBoltOns").Visible = True
                cgPlanner.Columns("TuesdayBoltOns").Visible = True
                cgPlanner.Columns("WednesdayBoltOns").Visible = True
                cgPlanner.Columns("ThursdayBoltOns").Visible = True
                cgPlanner.Columns("FridayBoltOns").Visible = True
                cgPlanner.Columns("SaturdayBoltOns").Visible = True
                cgPlanner.Columns("SundayBoltOns").Visible = True
            Else
                cgPlanner.Columns("MondayBoltOns").Visible = True
                cgPlanner.Columns("TuesdayBoltOns").Visible = True
                cgPlanner.Columns("WednesdayBoltOns").Visible = True
                cgPlanner.Columns("ThursdayBoltOns").Visible = True
                cgPlanner.Columns("FridayBoltOns").Visible = True
                cgPlanner.Columns("SaturdayBoltOns").Visible = False
                cgPlanner.Columns("SundayBoltOns").Visible = False
            End If
        Else
            cgPlanner.Columns("MondayBoltOns").Visible = False
            cgPlanner.Columns("TuesdayBoltOns").Visible = False
            cgPlanner.Columns("WednesdayBoltOns").Visible = False
            cgPlanner.Columns("ThursdayBoltOns").Visible = False
            cgPlanner.Columns("FridayBoltOns").Visible = False
            cgPlanner.Columns("SaturdayBoltOns").Visible = False
            cgPlanner.Columns("SundayBoltOns").Visible = False
        End If

        cgPlanner.Columns("MondayAM").Visible = False
        cgPlanner.Columns("TuesdayAM").Visible = False
        cgPlanner.Columns("WednesdayAM").Visible = False
        cgPlanner.Columns("ThursdayAM").Visible = False
        cgPlanner.Columns("FridayAM").Visible = False
        cgPlanner.Columns("SaturdayAM").Visible = False
        cgPlanner.Columns("SundayAM").Visible = False

        cgPlanner.Columns("MondayPM").Visible = False
        cgPlanner.Columns("TuesdayPM").Visible = False
        cgPlanner.Columns("WednesdayPM").Visible = False
        cgPlanner.Columns("ThursdayPM").Visible = False
        cgPlanner.Columns("FridayPM").Visible = False
        cgPlanner.Columns("SaturdayPM").Visible = False
        cgPlanner.Columns("SundayPM").Visible = False

        cgPlanner.Columns("MondayHeadcount").Visible = False
        cgPlanner.Columns("TuesdayHeadcount").Visible = False
        cgPlanner.Columns("WednesdayHeadcount").Visible = False
        cgPlanner.Columns("ThursdayHeadcount").Visible = False
        cgPlanner.Columns("FridayHeadcount").Visible = False
        cgPlanner.Columns("SaturdayHeadcount").Visible = False
        cgPlanner.Columns("SundayHeadcount").Visible = False

        cgPlanner.Columns("MondayStatus").Visible = False
        cgPlanner.Columns("TuesdayStatus").Visible = False
        cgPlanner.Columns("WednesdayStatus").Visible = False
        cgPlanner.Columns("ThursdayStatus").Visible = False
        cgPlanner.Columns("FridayStatus").Visible = False
        cgPlanner.Columns("SaturdayStatus").Visible = False
        cgPlanner.Columns("SundayStatus").Visible = False

        cgPlanner.Columns("MondayColour").Visible = False
        cgPlanner.Columns("TuesdayColour").Visible = False
        cgPlanner.Columns("WednesdayColour").Visible = False
        cgPlanner.Columns("ThursdayColour").Visible = False
        cgPlanner.Columns("FridayColour").Visible = False
        cgPlanner.Columns("SaturdayColour").Visible = False
        cgPlanner.Columns("SundayColour").Visible = False

        cgPlanner.Columns("ChildRef1").Visible = False
        cgPlanner.Columns("ChildRef2").Visible = False
        cgPlanner.Columns("ChildTags").Visible = False

        cgPlanner.AutoSizeColumns()

        cgPlanner.UnderlyingGridView.EndDataUpdate()

    End Sub

    Private Sub cgPlanner_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles cgPlanner.RowCellStyle

        If e.RowHandle < 0 Then Exit Sub

        Select Case e.Column.FieldName

            Case "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"

                Dim _Colour As String = cgPlanner.GetRowCellValue(e.RowHandle, e.Column.FieldName + "Colour").ToString
                Dim _Status As String = cgPlanner.GetRowCellValue(e.RowHandle, e.Column.FieldName + "Status").ToString

                e.Appearance.Reset()

                If _Status = "Holiday" Then
                    e.Appearance.BackColor = System.Drawing.Color.Silver
                    e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Italic)
                Else
                    If _Colour <> "" Then e.Appearance.BackColor = Color.FromName(_Colour)
                    If _Status = "Waiting" Then e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Italic)
                End If

        End Select

    End Sub

    Private Sub cgPlanner_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgPlanner.RowStyle

        If e Is Nothing Then Exit Sub
        If e.RowHandle < 0 Then Exit Sub

        For Each _C As DevExpress.XtraGrid.Columns.GridColumn In cgPlanner.Columns
            If cgPlanner.GetRowCellValue(e.RowHandle, _C.FieldName) IsNot Nothing Then
                If cgPlanner.GetRowCellValue(e.RowHandle, _C.FieldName).ToString = "Waiting" Then
                    e.Appearance.BackColor = txtYellow.BackColor
                Else
                    If cgPlanner.GetRowCellValue(e.RowHandle, _C.FieldName).ToString = "Holiday" Then
                        e.Appearance.BackColor = System.Drawing.Color.Silver
                    End If
                End If
            End If
        Next

    End Sub

    Private Sub cgPlanner_GridDoubleClick(sender As Object, e As EventArgs) Handles cgPlanner.GridDoubleClick

        If cgPlanner Is Nothing Then Exit Sub
        If cgPlanner.RecordCount <= 0 Then Exit Sub

        Dim _ID As String = cgPlanner.GetRowCellValue(cgPlanner.FocusedRowHandle, "ChildID").ToString
        If _ID <> "" Then
            'Me.Cursor = Cursors.WaitCursor
            Business.Child.DrillDown(New Guid(_ID))
        End If

    End Sub

#End Region

#Region "Controls"

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click

        If cdtWC.Text = "" Then Exit Sub

        If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
            CareMessage("Week commencing must be a Monday.", MessageBoxIcon.Exclamation, "Week Commencing Check")
            Exit Sub
        End If

        DisplayWC(cdtWC.DateTime)

    End Sub

    Private Sub btnStaffRegister_Click(sender As Object, e As EventArgs) Handles btnStaffRegister.Click

        If cdtWC.Text = "" Then Exit Sub

        SetButtons(False)
        Session.CursorWaiting()

        Dim _WC As String = "cast('" + ValueHandler.SQLDate(cdtWC.Value.Value) + "' as date) as 'WC'"

        Dim _SQL As String = ""

        _SQL += "select " + _WC + ", fullname, job_title, group_name from Staff"
        _SQL += " where status <> 'X'"

        If cbxSite.Text <> "" Then
            _SQL += " and site_name = '" + cbxSite.Text + "'"
        End If

        _SQL += " order by fullname"

        If btnStaffRegister.ShiftDown Then
            ReportHandler.DesignReport("RegisterStaff.repx", Session.ConnectionString, _SQL)
        Else
            ReportHandler.RunReport("RegisterStaff.repx", Session.ConnectionString, _SQL)
        End If

        SetButtons(True)
        Session.CursorDefault()

    End Sub

    Private Sub radSessions_CheckedChanged(sender As Object, e As EventArgs) Handles radSessions.CheckedChanged
        If radSessions.Checked Then RefreshGrid()
    End Sub

    Private Sub radTimes_CheckedChanged(sender As Object, e As EventArgs) Handles radTimes.CheckedChanged
        If radTimes.Checked Then RefreshGrid()
    End Sub

    Private Sub radGridHeadCount_CheckedChanged(sender As Object, e As EventArgs) Handles radGridHeadCount.CheckedChanged
        If radGridHeadCount.Checked Then RefreshGrid()
    End Sub

    Private Sub radGridPoints_CheckedChanged(sender As Object, e As EventArgs) Handles radGridPoints.CheckedChanged
        If radGridPoints.Checked Then RefreshGrid()
    End Sub

    Private Sub radGridStaff_CheckedChanged(sender As Object, e As EventArgs) Handles radGridStaff.CheckedChanged
        If radGridStaff.Checked Then RefreshGrid()
    End Sub

    Private Sub cbxTariff_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTariff.EditValueChanged
        RefreshGrid()
    End Sub

    Private Sub cbxBoltOns_EditValueChanged(sender As Object, e As EventArgs) Handles cbxBoltOns.EditValueChanged
        RefreshGrid()
    End Sub

    Private Sub cbxTimeSlots_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTimeSlots.EditValueChanged
        RefreshGrid()
    End Sub

    Private Sub chkGridOptions_EditValueChanged(sender As Object, e As EventArgs) Handles chkGridOptions.EditValueChanged
        RefreshGrid()
    End Sub

    Private Sub cbxSplit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSplit.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSort.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxSchool_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSchool.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxGroup.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        PopulateRooms()
        PopulateTariffs()
        RefreshGrid()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cbxLogicTariff_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxLogicTariff.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxLogicBoltOns_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxLogicBoltOns.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub cbxLogicTimes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxLogicTimes.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub chkAllergies_EditValueChanged(sender As Object, e As EventArgs) Handles chkAllergies.EditValueChanged
        RefreshGrid()
    End Sub

    Private Sub chkMonday_CheckedChanged(sender As Object, e As EventArgs) Handles chkMonday.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkTuesday_CheckedChanged(sender As Object, e As EventArgs) Handles chkTuesday.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkWednesday_CheckedChanged(sender As Object, e As EventArgs) Handles chkWednesday.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkThursday_CheckedChanged(sender As Object, e As EventArgs) Handles chkThursday.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkFriday_CheckedChanged(sender As Object, e As EventArgs) Handles chkFriday.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkBreakfast_CheckedChanged(sender As Object, e As EventArgs) Handles chkBreakfast.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkLunch_CheckedChanged(sender As Object, e As EventArgs) Handles chkLunch.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkTea_CheckedChanged(sender As Object, e As EventArgs) Handles chkTea.CheckedChanged
        RefreshGrid()
    End Sub

#End Region

#Region "Pivot Grid"

    Private Sub AddField(ByVal FieldName As String, ByVal Caption As String, ByVal FormatType As DevExpress.Utils.FormatType, ByVal Area As PivotArea, Optional ByVal Sorting As Integer = 0)

        Dim _PGV As New PivotGridField(FieldName, Area)
        With _PGV

            .Caption = Caption

            If Sorting = 1 Then _PGV.SortOrder = PivotSortOrder.Ascending
            If Sorting = 2 Then _PGV.SortOrder = PivotSortOrder.Descending

            If FormatType = DevExpress.Utils.FormatType.DateTime Then

                _PGV.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime

                If FieldName = "dob" Then
                    _PGV.ValueFormat.FormatString = "dd MMM yy"
                Else
                    _PGV.ValueFormat.FormatString = "ddd dd MMM"
                End If

            Else
                If radGridStaff.Checked Then
                    _PGV.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    _PGV.CellFormat.FormatString = "n2"
                Else
                    _PGV.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    _PGV.CellFormat.FormatString = ""
                End If
            End If

        End With

        PivotGridControl1.Fields.Add(_PGV)

    End Sub

    Private Sub BuildSubQueries(ByRef SQL As String)

        Dim _SQL As String = ""
        Dim _ColumnName As String = "name"

        _SQL += "select id, short_name, name from TimeSlots"

        If Not chkAllSlots.Checked Then
            _SQL += " where headcount = 1"
            _ColumnName = "short_name"
        End If

        _SQL += " order by start_time"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                SQL += ", ("

                If radGridHeadCount.Checked Then SQL += " select count(*) from BookingSlots s"
                If radGridPoints.Checked Then SQL += " select (count(*) * b.points) from BookingSlots s"
                If radGridStaff.Checked Then SQL += " select (cast( (count(*) * b.points) as float) / " + m_PointsForStaff.ToString + ") from BookingSlots s"

                SQL += " where s.booking_id = b.ID"
                SQL += " And s.slot_id = '" + _DR.Item("id").ToString + "'"
                SQL += ") as '" + _DR.Item(_ColumnName).ToString + "'"

                SQL += vbCrLf

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub BuildPivotGrid(ByVal WC As Date)

        If cdtWC.Text = "" Then Exit Sub

        Session.CursorWaiting()

        Dim _From As Date = WC
        Dim _To As Date = _From.AddDays(6)

        Dim _SortName As String = "(c.forename + ' ' + c.surname) as 'SortName'"
        If cbxSort.SelectedIndex = 1 Then _SortName = "(c.surname + ', ' + c.forename) as 'SortName'"

        Dim _SQL As String = ""

        _SQL += "select d.date, c.site_name, ' ' as 'room', c.group_name as 'child_room', r.room_name as 'booking_room', c.move_mode, c.move_date, mr.room_name as 'move_room',"
        _SQL += vbCrLf
        _SQL += " c.forename, c.surname, " + _SortName + ", c.dob, b.child_age, b.points, b.booking_from, b.booking_to"
        _SQL += vbCrLf

        BuildSubQueries(_SQL)

        _SQL += " from Calendar d"
        _SQL += vbCrLf
        _SQL += " left join Bookings b on d.date = b.booking_date"
        _SQL += vbCrLf
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += vbCrLf
        _SQL += " left join SiteRoomRatios rr on rr.ID = b.ratio_id"
        _SQL += vbCrLf
        _SQL += " left join SiteRooms r on r.ID = rr.room_id"
        _SQL += vbCrLf
        _SQL += " left join SiteRoomRatios mrr on mrr.ID = c.move_ratio_id"
        _SQL += vbCrLf
        _SQL += " left join SiteRooms mr on mr.ID = mrr.room_id"
        _SQL += vbCrLf
        _SQL += FilterDays("d.date", _From, _To)

        If cbxSite.Text <> "" Then
            _SQL += vbCrLf
            _SQL += " and c.site_name = '" + cbxSite.Text + "'"
        End If

        If cbxSchool.Text <> "" Then
            _SQL += vbCrLf
            _SQL += " and c.school = '" + cbxSchool.SelectedValue.ToString + "'"
        End If

        If chkGridOptions.Text.Contains("Hide Waiting") Then
            _SQL += " and b.booking_status <> 'Waiting'"
            _SQL += vbCrLf
        End If

        If chkGridOptions.Text.Contains("Hide Holidays") Then
            _SQL += " and b.booking_status <> 'Holiday'"
            _SQL += vbCrLf
        End If

        _SQL += FilterTariffs()
        _SQL += FilterTimeSlots()
        _SQL += FilterBoltOns()
        _SQL += FilterMeals()
        _SQL += FilterTags()
        _SQL += FilterAllergiesAndDiet()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            PivotGridControl1.DataSource = ProcessDataTable(_DT)

            PivotGridControl1.Fields.Clear()

            AddField("date", "Date", DevExpress.Utils.FormatType.DateTime, PivotArea.ColumnArea)
            AddField("site_name", "Site", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
            AddField("room", "Room", DevExpress.Utils.FormatType.None, PivotArea.RowArea)

            Select Case cbxSort.SelectedIndex

                Case 0, 1
                    AddField("SortName", "Child", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
                    AddField("dob", "DOB", DevExpress.Utils.FormatType.DateTime, PivotArea.RowArea)

                    'youngest > oldest
                Case 2
                    AddField("dob", "DOB", DevExpress.Utils.FormatType.DateTime, PivotArea.RowArea, 2)
                    AddField("SortName", "Child", DevExpress.Utils.FormatType.None, PivotArea.RowArea)

                    'oldest > youngest
                Case 3
                    AddField("dob", "DOB", DevExpress.Utils.FormatType.DateTime, PivotArea.RowArea, 1)
                    AddField("SortName", "Child", DevExpress.Utils.FormatType.None, PivotArea.RowArea)

            End Select

            AddDataFields()

            PivotGridControl1.BestFit()

        End If

        Session.CursorDefault()

    End Sub

    Private Function ProcessDataTable(ByVal DTIn As DataTable) As DataTable

        Session.SetupProgressBar("Processing Data...", DTIn.Rows.Count)
        For Each _DR As DataRow In DTIn.Rows

            Dim _Room As String = ""

            If chkGridOptions.Text.Contains("Use Room from Child Record") Then
                _Room = _DR.Item("child_room").ToString
            Else
                _Room = _DR.Item("booking_room").ToString
            End If

            'check if we have a manual override
            If _DR.Item("move_mode").ToString = "Manual" AndAlso _DR.Item("move_room").ToString <> "" Then
                If ValueHandler.ConvertDate(_DR.Item("date")) >= ValueHandler.ConvertDate(_DR.Item("move_date")) Then
                    _Room = _DR.Item("move_room").ToString
                End If
            End If

            _DR.Item("room") = _Room

            Session.StepProgressBar()

        Next

        Session.HideProgressBar()

        Return DTIn

    End Function

    Private Sub AddDataFields()

        Dim _SQL As String = ""
        Dim _ColumnName As String = "name"

        _SQL += "select id, short_name, name from TimeSlots"

        If Not chkAllSlots.Checked Then
            _SQL += " where headcount = 1"
            _ColumnName = "short_name"
        End If

        _SQL += " order by start_time"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        If _DT IsNot Nothing Then

            m_VisibleTimeSlots = _DT.Rows.Count

            For Each _DR As DataRow In _DT.Rows
                AddField(_DR.Item(_ColumnName).ToString, _DR.Item(_ColumnName).ToString, DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub PivotGridControl1_CustomCellDisplayText(sender As Object, e As PivotCellDisplayTextEventArgs) Handles PivotGridControl1.CustomCellDisplayText
        If e.DataField Is Nothing Then Exit Sub
        If e.DataField.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric Then
            If e.Value Is Nothing Then
                If radGridStaff.Checked Then
                    e.DisplayText = "0.00"
                Else
                    e.DisplayText = "0"
                End If
            End If
        End If
    End Sub

    Private Sub btnPivotPrint_Click(sender As Object, e As EventArgs) Handles btnPivotPrint.Click

        SetButtons(False)
        Session.CursorWaiting()

        With PivotGridControl1.OptionsPrint.PageSettings
            .PaperKind = System.Drawing.Printing.PaperKind.A4
            .Landscape = True
            .Margins.Top = 25
            .Margins.Bottom = 25
            .Margins.Left = 25
            .Margins.Right = 25
        End With

        PivotGridControl1.ShowPrintPreview()

        SetButtons(True)
        Session.CursorDefault()

    End Sub

    Private Sub PivotGridControl1_CustomDrawCell(sender As Object, e As PivotCustomDrawCellEventArgs) Handles PivotGridControl1.CustomDrawCell
        If IsHightlightColumn(e.ColumnIndex) Then
            Dim _InnerRect As Rectangle = Rectangle.Inflate(e.Bounds, -3, -3)
            e.GraphicsCache.FillRectangle(New SolidBrush(Color.Khaki), e.Bounds)
            e.GraphicsCache.DrawString(e.DisplayText, e.Appearance.Font, New SolidBrush(Color.Black), _InnerRect, e.Appearance.GetStringFormat())
            e.Handled = True
        End If
    End Sub

    Private Function IsHightlightColumn(ByVal Index As Integer) As Boolean
        Dim _Mod As Integer = Index Mod m_VisibleTimeSlots
        If _Mod = 0 Then Return True
        Return False
    End Function

#End Region

#Region "Local Class Definitions"

    Public Class GridWeek

        Public Property ChildID As Guid
        Public Property ChildName As String
        Public Property ChildAge As String
        Public Property ChildRoom As String
        Public Property ChildKeyworker As String
        Public Property ChildSchool As String
        Public Property ChildMonths As Long
        Public Property ChildDOB As Date?

        Public Property ChildStart As Date?
        Public Property ChildLeave As Date?

        Public Property RoomSequence As Integer

        Public Property Monday As String
        Public Property MondayAM As Integer
        Public Property MondayPM As Integer
        Public Property MondayBreakfast As Boolean
        Public Property MondayLunch As Boolean
        Public Property MondayTea As Boolean
        Public Property MondaySnack As Boolean
        Public Property MondayHeadcount As Integer
        Public Property MondayStatus As String
        Public Property MondayColour As String
        Public Property MondayBoltOns As String

        Public Property Tuesday As String
        Public Property TuesdayAM As Integer
        Public Property TuesdayPM As Integer
        Public Property TuesdayBreakfast As Boolean
        Public Property TuesdayLunch As Boolean
        Public Property TuesdayTea As Boolean
        Public Property TuesdaySnack As Boolean
        Public Property TuesdayHeadcount As Integer
        Public Property TuesdayStatus As String
        Public Property TuesdayColour As String
        Public Property TuesdayBoltOns As String

        Public Property Wednesday As String
        Public Property WednesdayAM As Integer
        Public Property WednesdayPM As Integer
        Public Property WednesdayBreakfast As Boolean
        Public Property WednesdayLunch As Boolean
        Public Property WednesdayTea As Boolean
        Public Property WednesdaySnack As Boolean
        Public Property WednesdayHeadcount As Integer
        Public Property WednesdayStatus As String
        Public Property WednesdayColour As String
        Public Property WednesdayBoltOns As String

        Public Property Thursday As String
        Public Property ThursdayAM As Integer
        Public Property ThursdayPM As Integer
        Public Property ThursdayBreakfast As Boolean
        Public Property ThursdayLunch As Boolean
        Public Property ThursdayTea As Boolean
        Public Property ThursdaySnack As Boolean
        Public Property ThursdayHeadcount As Integer
        Public Property ThursdayStatus As String
        Public Property ThursdayColour As String
        Public Property ThursdayBoltOns As String

        Public Property Friday As String
        Public Property FridayAM As Integer
        Public Property FridayPM As Integer
        Public Property FridayBreakfast As Boolean
        Public Property FridayLunch As Boolean
        Public Property FridayTea As Boolean
        Public Property FridaySnack As Boolean
        Public Property FridayHeadcount As Integer
        Public Property FridayStatus As String
        Public Property FridayColour As String
        Public Property FridayBoltOns As String

        Public Property Saturday As String
        Public Property SaturdayAM As Integer
        Public Property SaturdayPM As Integer
        Public Property SaturdayBreakfast As Boolean
        Public Property SaturdayLunch As Boolean
        Public Property SaturdayTea As Boolean
        Public Property SaturdaySnack As Boolean
        Public Property SaturdayHeadcount As Integer
        Public Property SaturdayStatus As String
        Public Property SaturdayColour As String
        Public Property SaturdayBoltOns As String

        Public Property Sunday As String
        Public Property SundayAM As Integer
        Public Property SundayPM As Integer
        Public Property SundayBreakfast As Boolean
        Public Property SundayLunch As Boolean
        Public Property SundayTea As Boolean
        Public Property SundaySnack As Boolean
        Public Property SundayHeadcount As Integer
        Public Property SundayStatus As String
        Public Property SundayColour As String
        Public Property SundayBoltOns As String

        Public Property ChildRef1 As String
        Public Property ChildRef2 As String
        Public Property ChildTags As String

    End Class

    Public Class ReportWeek

        Public Property WeekCommencing As Date
        Public Property ChildID As Guid
        Public Property ChildName As String
        Public Property ChildAge As String
        Public Property ChildGroup As String
        Public Property ChildKeyworker As String
        Public Property ChildSchool As String
        Public Property ChildMonths As Integer
        Public Property ChildDOB As Date
        Public Property ChildMedical As String
        Public Property ChildContacts As String
        Public Property ChildNappies As Boolean
        Public Property ChildBabyFood As Boolean

        Public Property Monday As String
        Public Property MondayDate As Date
        Public Property MondayAM As Integer
        Public Property MondayPM As Integer
        Public Property MondayHeadcount As Integer
        Public Property MondayColour As String
        Public Property MondayBoltOns As String

        Public Property Tuesday As String
        Public Property TuesdayDate As Date
        Public Property TuesdayAM As Integer
        Public Property TuesdayPM As Integer
        Public Property TuesdayHeadcount As Integer
        Public Property TuesdayColour As String
        Public Property TuesdayBoltOns As String

        Public Property Wednesday As String
        Public Property WednesdayDate As Date
        Public Property WednesdayAM As Integer
        Public Property WednesdayPM As Integer
        Public Property WednesdayHeadcount As Integer
        Public Property WednesdayColour As String
        Public Property WednesdayBoltOns As String

        Public Property Thursday As String
        Public Property ThursdayDate As Date
        Public Property ThursdayAM As Integer
        Public Property ThursdayPM As Integer
        Public Property ThursdayHeadcount As Integer
        Public Property ThursdayColour As String
        Public Property ThursdayBoltOns As String

        Public Property Friday As String
        Public Property FridayDate As Date
        Public Property FridayAM As Integer
        Public Property FridayPM As Integer
        Public Property FridayHeadcount As Integer
        Public Property FridayColour As String
        Public Property FridayBoltOns As String

        Public Property Saturday As String
        Public Property SaturdayDate As Date
        Public Property SaturdayAM As Integer
        Public Property SaturdayPM As Integer
        Public Property SaturdayHeadcount As Integer
        Public Property SaturdayColour As String
        Public Property SaturdayBoltOns As String

        Public Property Sunday As String
        Public Property SundayDate As Date
        Public Property SundayAM As Integer
        Public Property SundayPM As Integer
        Public Property SundayHeadcount As Integer
        Public Property SundayColour As String
        Public Property SundayBoltOns As String

        Public Property GroupSeq As Integer

        Public Property ChildYM As String
        Public Property ChildForename As String
        Public Property ChildSurname As String
        Public Property MedAllergies As String
        Public Property MedMedical As String
        Public Property MedOther As String
        Public Property PrimaryName As String
        Public Property PrimaryPassword As String
        Public Property PrimaryMobile As String

        Public Property LogicalGroup As Integer
        Public Property AgeDays As Integer

        Public Property ChildRef1 As String
        Public Property ChildRef2 As String
        Public Property ChildTags As String

    End Class

    Public Class ReportDay
        Public Property ReportDate As Date
        Public Property ChildName As String
        Public Property ChildAge As String
        Public Property ChildGroup As String
        Public Property ChildKeyworker As String
        Public Property ChildSchool As String
        Public Property ChildDOB As Date
        Public Property Tariff As String
        Public Property Times As String
        Public Property ChildMedical As String
        Public Property ChildContacts As String
        Public Property Nappies As Boolean
        Public Property BabyFood As Boolean
        Public Property ChildYM As String
        Public Property ChildForename As String
        Public Property ChildSurname As String
        Public Property MedAllergies As String
        Public Property MedMedical As String
        Public Property MedOther As String
        Public Property PrimaryName As String
        Public Property PrimaryPassword As String
        Public Property PrimaryMobile As String
        Public Property LogicalGroup As Integer
        Public Property AgeDays As Integer
        Public Property ChildRef1 As String
        Public Property ChildRef2 As String
        Public Property ChildTags As String
        Public Property BoltOns As String
    End Class

#End Region

#Region "Printed Registers"

    Private Sub BuildPrintDataWeek(ByVal WeekCommencing As Date)

        m_ReportWeekData.Clear()

        If m_GridData.Count > 0 Then

            Session.SetupProgressBar("Building Register Data...", m_GridData.Count)

            For Each _GW As GridWeek In m_GridData

                Dim _Record As New ReportWeek
                With _Record

                    .WeekCommencing = WeekCommencing

                    .ChildID = _GW.ChildID
                    .ChildName = _GW.ChildName

                    If _GW.ChildDOB.HasValue Then
                        .ChildDOB = _GW.ChildDOB.Value
                        .ChildMonths = ValueHandler.ConvertInteger(_GW.ChildMonths)
                        .ChildAge = ValueHandler.DateDifferenceAsText(.ChildDOB, Today, True, True, False)
                        .ChildYM = ValueHandler.DateDifferenceAsTextYM(.ChildDOB, Today)
                        .AgeDays = CInt(DateDiff(DateInterval.Day, .ChildDOB, Today))
                    End If

                    .ChildKeyworker = _GW.ChildKeyworker
                    .ChildSchool = _GW.ChildSchool

                    .ChildGroup = _GW.ChildRoom
                    .GroupSeq = _GW.RoomSequence

                    'fetch the Child record to get the fields we don't have in the GridWeek class
                    Dim _C As Business.Child = Business.Child.RetreiveByID(_GW.ChildID)
                    If _C IsNot Nothing Then

                        Dim _Medical As String = BuildMedical(_C._MedAllergies, _C._MedMedication, _C._MedNotes)
                        Dim _Contacts As String = BuildContacts(_C._FamilyId.ToString)

                        .ChildMedical = _Medical
                        .ChildContacts = _Contacts
                        .ChildNappies = _C._Nappies
                        .ChildBabyFood = _C._BabyFood

                        .ChildForename = _C._Forename
                        .ChildSurname = _C._Surname
                        .MedAllergies = _C._MedAllergies
                        .MedMedical = _C._MedMedication
                        .MedOther = _C._MedNotes

                        Dim _P As Business.Contact = Business.Contact.RetrievePrimaryContact(_C._FamilyId.ToString)
                        If _P IsNot Nothing Then
                            .PrimaryName = _P._Fullname
                            .PrimaryPassword = _P._Password
                            .PrimaryMobile = _P._TelMobile
                        End If

                    End If

                    'monday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.MondayStatus = "Holiday" Then
                            .Monday = "Holiday"
                            .MondayAM = 0
                            .MondayPM = 0
                            .MondayHeadcount = 0
                        Else
                            .Monday = _GW.Monday.ToString
                            .MondayAM = _GW.MondayAM
                            .MondayPM = _GW.MondayPM
                            .MondayHeadcount = _GW.MondayHeadcount
                        End If
                    Else
                        .Monday = _GW.Monday
                        .MondayAM = _GW.MondayAM
                        .MondayPM = _GW.MondayPM
                        .MondayHeadcount = _GW.MondayHeadcount
                    End If
                    .MondayDate = WeekCommencing
                    .MondayColour = _GW.MondayColour
                    .MondayBoltOns = _GW.MondayBoltOns

                    'tuesday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.TuesdayStatus = "Holiday" Then
                            .Tuesday = "Holiday"
                            .TuesdayAM = 0
                            .TuesdayPM = 0
                            .TuesdayHeadcount = 0
                        Else
                            .Tuesday = _GW.Tuesday.ToString
                            .TuesdayAM = _GW.TuesdayAM
                            .TuesdayPM = _GW.TuesdayPM
                            .TuesdayHeadcount = _GW.TuesdayHeadcount
                        End If
                    Else
                        .Tuesday = _GW.Tuesday.ToString
                        .TuesdayAM = _GW.TuesdayAM
                        .TuesdayPM = _GW.TuesdayPM
                        .TuesdayHeadcount = _GW.TuesdayHeadcount
                    End If
                    .TuesdayDate = WeekCommencing.AddDays(1)
                    .TuesdayColour = _GW.TuesdayColour
                    .TuesdayBoltOns = _GW.TuesdayBoltOns

                    'wednesday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.WednesdayStatus = "Holiday" Then
                            .Wednesday = "Holiday"
                            .WednesdayAM = 0
                            .WednesdayPM = 0
                            .WednesdayHeadcount = 0
                        Else
                            .Wednesday = _GW.Wednesday.ToString
                            .WednesdayAM = _GW.WednesdayAM
                            .WednesdayPM = _GW.WednesdayPM
                            .WednesdayHeadcount = _GW.WednesdayHeadcount
                        End If
                    Else
                        .Wednesday = _GW.Wednesday.ToString
                        .WednesdayAM = _GW.WednesdayAM
                        .WednesdayPM = _GW.WednesdayPM
                        .WednesdayHeadcount = _GW.WednesdayHeadcount
                    End If
                    .WednesdayDate = WeekCommencing.AddDays(2)
                    .WednesdayColour = _GW.WednesdayColour
                    .WednesdayBoltOns = _GW.WednesdayBoltOns

                    'thursday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.ThursdayStatus = "Holiday" Then
                            .Thursday = "Holiday"
                            .ThursdayAM = 0
                            .ThursdayPM = 0
                            .ThursdayHeadcount = 0
                        Else
                            .Thursday = _GW.Thursday.ToString
                            .ThursdayAM = _GW.ThursdayAM
                            .ThursdayPM = _GW.ThursdayPM
                            .ThursdayHeadcount = _GW.ThursdayHeadcount
                        End If
                    Else
                        .Thursday = _GW.Thursday.ToString
                        .ThursdayAM = _GW.ThursdayAM
                        .ThursdayPM = _GW.ThursdayPM
                        .ThursdayHeadcount = _GW.ThursdayHeadcount
                    End If
                    .ThursdayDate = WeekCommencing.AddDays(3)
                    .ThursdayColour = _GW.ThursdayColour
                    .ThursdayBoltOns = _GW.ThursdayBoltOns

                    'friday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.FridayStatus = "Holiday" Then
                            .Friday = "Holiday"
                            .FridayAM = 0
                            .FridayPM = 0
                            .FridayHeadcount = 0
                        Else
                            .Friday = _GW.Friday.ToString
                            .FridayAM = _GW.FridayAM
                            .FridayPM = _GW.FridayPM
                            .FridayHeadcount = _GW.FridayHeadcount
                        End If
                    Else
                        .Friday = _GW.Friday.ToString
                        .FridayAM = _GW.FridayAM
                        .FridayPM = _GW.FridayPM
                        .FridayHeadcount = _GW.FridayHeadcount
                    End If
                    .FridayDate = WeekCommencing.AddDays(4)
                    .FridayColour = _GW.FridayColour
                    .FridayBoltOns = _GW.FridayBoltOns

                    'saturday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.SaturdayStatus = "Holiday" Then
                            .Saturday = "Holiday"
                            .SaturdayAM = 0
                            .SaturdayPM = 0
                            .SaturdayHeadcount = 0
                        Else
                            .Saturday = _GW.Saturday.ToString
                            .SaturdayAM = _GW.SaturdayAM
                            .SaturdayPM = _GW.SaturdayPM
                            .SaturdayHeadcount = _GW.SaturdayHeadcount
                        End If
                    Else
                        .Saturday = _GW.Saturday.ToString
                        .SaturdayAM = _GW.SaturdayAM
                        .SaturdayPM = _GW.SaturdayPM
                        .SaturdayHeadcount = _GW.SaturdayHeadcount
                    End If
                    .SaturdayDate = WeekCommencing.AddDays(5)
                    .SaturdayColour = _GW.SaturdayColour
                    .SaturdayBoltOns = _GW.SaturdayBoltOns

                    'sunday
                    If chkGridOptions.Text.Contains("Hide Holidays") Then
                        If _GW.SundayStatus = "Holiday" Then
                            .Sunday = "Holiday"
                            .SundayAM = 0
                            .SundayPM = 0
                            .SundayHeadcount = 0
                        Else
                            .Sunday = _GW.Sunday.ToString
                            .SundayAM = _GW.SundayAM
                            .SundayPM = _GW.SundayPM
                            .SundayHeadcount = _GW.SundayHeadcount
                        End If
                    Else
                        .Sunday = _GW.Sunday.ToString
                        .SundayAM = _GW.SundayAM
                        .SundayPM = _GW.SundayPM
                        .SundayHeadcount = _GW.SundayHeadcount
                    End If
                    .SundayDate = WeekCommencing.AddDays(6)
                    .SundayColour = _GW.SundayColour
                    .SundayBoltOns = _GW.SundayBoltOns

                    .ChildRef1 = _GW.ChildRef1
                    .ChildRef2 = _GW.ChildRef2
                    .ChildTags = _GW.ChildTags

                End With

                m_ReportWeekData.Add(_Record)
                Session.StepProgressBar()

            Next

            Session.HideProgressBar()

            If m_ReportWeekData.Count > 0 Then
                If cbxSplit.SelectedIndex > 0 Then
                    SplitIntoGroups()
                End If
            End If

        End If

    End Sub

    Private Sub SplitIntoGroups()

        Dim _Groups As Integer = cbxSplit.SelectedIndex + 1
        Dim _Group As Integer = 1
        Dim _GroupSize As Integer = m_ReportWeekData.Count \ _Groups
        Dim _LeftToSplit As Integer = _GroupSize

        For Each _r In m_ReportWeekData

            If _LeftToSplit > 0 Then
                _r.LogicalGroup = _Group
                _LeftToSplit -= 1
            Else
                _Group += 1
                _LeftToSplit = _GroupSize
                _r.LogicalGroup = _Group
            End If

        Next

    End Sub

    Private Function BuildMedical(ByVal Allergies As String, ByVal Medication As String, ByVal Notes As String) As String

        Dim _Return As String = ""

        If Allergies.Trim <> "" Then _Return += Allergies.Trim

        If Medication.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Medication.Trim
        End If

        If Notes.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Notes.Trim
        End If

        Return _Return

    End Function

    Private Function BuildContacts(ByVal FamilyID As String) As String

        Dim _Return As String = ""

        Dim _SQL As String = ""
        _SQL += "select forename, relationship, (tel_home + ' ' + tel_mobile) as 'tel'"
        _SQL += " from Contacts"
        _SQL += " where family_id = '" + FamilyID + "'"
        _SQL += " and emer_cont = 1"
        _SQL += " and (tel_mobile <> '' or tel_home <> '')"
        _SQL += " order by primary_cont desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                If _Return <> "" Then _Return += vbCrLf
                _Return += _DR.Item("forename").ToString + ", " + _DR.Item("relationship").ToString + " : " + _DR.Item("tel").ToString
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Return

    End Function

#End Region

    Private Sub SetButtons(ByVal Enabled As Boolean)

        btnRun.Enabled = Enabled

        btnPrint.Enabled = Enabled
        btnStaffRegister.Enabled = Enabled

        btnMonday.Enabled = Enabled
        btnTuesday.Enabled = Enabled
        btnWednesday.Enabled = Enabled
        btnThursday.Enabled = Enabled
        btnFriday.Enabled = Enabled
        btnSaturday.Enabled = Enabled
        btnSunday.Enabled = Enabled

        Application.DoEvents()

    End Sub

    Private Sub SetLayout()

        If radSessions.Checked OrElse radTimes.Checked Then
            gbxGrid.Visible = True
            gbxPivot.Visible = False
            btnPrint.Text = "Week Register"
        Else
            gbxGrid.Visible = False
            gbxPivot.Visible = True
            btnPrint.Text = "Print"
        End If

        Application.DoEvents()

    End Sub

    Private Function GetTariffColour(ByVal TariffID As String) As String

        If TariffID = "" Then Return ""
        Dim _Return As String = ""
        Dim _Q As IEnumerable(Of Business.Tariff) = From _T As Business.Tariff In m_Tariffs
                                                    Where _T._ID = New Guid(TariffID)

        If _Q IsNot Nothing Then
            If _Q.Count > 0 Then
                _Return = _Q.First._Colour
            End If
            _Q = Nothing
        End If

        Return _Return

    End Function

    Private Sub SetRoomDetails(ByRef RoomName As String, ByRef RoomSequence As Integer, ByVal ChildGroupID As Guid,
                               ByVal WC As Date, ByVal MoveMode As String, ByVal MoveDate As Date?, ByVal MoveRatioID As Guid?,
                               ByVal Monday As String, ByVal Tuesday As String, ByVal Wednesday As String, ByVal Thursday As String,
                               ByVal Friday As String, ByVal Saturday As String, ByVal Sunday As String)

        Dim _Day As String = ""
        If _Day = "" AndAlso Monday <> "" Then _Day = Monday
        If _Day = "" AndAlso Tuesday <> "" Then _Day = Tuesday
        If _Day = "" AndAlso Wednesday <> "" Then _Day = Wednesday
        If _Day = "" AndAlso Thursday <> "" Then _Day = Thursday
        If _Day = "" AndAlso Friday <> "" Then _Day = Friday
        If _Day = "" AndAlso Saturday <> "" Then _Day = Saturday
        If _Day = "" AndAlso Sunday <> "" Then _Day = Sunday

        If _Day = "" Then Exit Sub

        RoomName = ""
        RoomSequence = 0

        If chkGridOptions.Text.Contains("Use Room from Child Record") Then

            If MoveMode = "Manual" AndAlso MoveDate.HasValue AndAlso MoveRatioID.HasValue Then

                'if we are on or have passed the manual move date, we use this, otherwise we use the room from the child record
                If WC >= MoveDate Then

                    Dim _Q As IEnumerable(Of Business.RoomRatios) = From _R As Business.RoomRatios In m_RoomRatios Where _R.RatioID = MoveRatioID.Value
                    If _Q IsNot Nothing Then
                        If _Q.Count = 1 Then
                            RoomName = _Q.First.RoomName
                            RoomSequence = _Q.First.Sequence
                        End If
                        _Q = Nothing
                    End If

                End If

            End If

            If RoomName = "" Then

                Dim _Q As IEnumerable(Of Business.SiteRoom) = From _R As Business.SiteRoom In m_Rooms Where _R._ID = ChildGroupID
                If _Q IsNot Nothing Then
                    If _Q.Count = 1 Then
                        RoomName = _Q.First._RoomName
                        RoomSequence = _Q.First._RoomSequence
                    End If
                    _Q = Nothing
                End If

            End If

        Else

            'we are using the room from the booking

            Dim _Q As IEnumerable(Of Business.RoomRatios) = From _R As Business.RoomRatios In m_RoomRatios Where _R.RatioID = New Guid(_Day)
            If _Q IsNot Nothing Then
                If _Q.Count = 1 Then
                    RoomName = _Q.First.RoomName
                    RoomSequence = _Q.First.Sequence
                End If
                _Q = Nothing
            End If

        End If

    End Sub

    Private Sub PopulateRooms()
        If cbxSite.Text = "" Then
            cbxGroup.Clear()
            cbxGroup.SelectedIndex = -1
        Else

            Dim _SQL As String = ""

            _SQL = "select ID, room_name from SiteRooms"
            _SQL += " where site_id = '" + cbxSite.SelectedValue.ToString + "'"
            _SQL += " order by room_sequence"

            cbxGroup.AllowBlank = True
            cbxGroup.PopulateWithSQL(Session.ConnectionString, _SQL)

        End If
    End Sub

    Private Function TariffAlreadyAdded(ByRef Tariffs As List(Of String), ByVal TariffID As String) As Boolean
        For Each tariff As String In Tariffs
            If tariff = TariffID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub PopulateTariffs()

        If cbxSite.Text = "" Then

            m_Tariffs.Clear()
            cbxTariff.Properties.Items.Clear()

            Dim _Tariffs As New List(Of String)
            Dim _Items As New List(Of DevExpress.XtraEditors.Controls.CheckedListBoxItem)
            For Each _t As Business.Tariff In Business.Tariff.RetreiveAll
                If Not _t._Archived Then
                    If Not _t._Monthly Then
                        If Not TariffAlreadyAdded(_Tariffs, _t._ID.Value.ToString) Then
                            _Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_t._ID.Value.ToString.ToUpper, _t._Name))
                            m_Tariffs.Add(_t)
                            _Tariffs.Add(_t._Name)
                        End If
                    End If
                End If
            Next

            _Items = _Items.OrderBy(Function(x) x.Description).ToList
            For Each _i In _Items

                If cbxTariff.Properties.Items.Count = 0 Then cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_i.Value, _i.Description))

                Dim exists As Boolean = False
                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxTariff.Properties.Items
                    If item.Description = _i.Description Then exists = True
                Next
                If Not exists Then cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_i.Value, _i.Description))

            Next

        Else

            m_Tariffs.Clear()
            cbxTariff.Properties.Items.Clear()

            Dim _SiteID As Guid = New Guid(cbxSite.SelectedValue.ToString)
            For Each _ts As Business.Tariff In Business.Tariff.RetreiveAllWeeklyTariffs(_SiteID)

                If cbxTariff.Properties.Items.Count = 0 Then
                    cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_ts._ID.Value.ToString.ToUpper, _ts._Name))
                    m_Tariffs.Add(_ts)
                End If

                Dim exists As Boolean = False
                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxTariff.Properties.Items
                    If item.Description = _ts._Name Then exists = True
                Next
                If Not exists Then
                    cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_ts._ID.Value.ToString.ToUpper, _ts._Name))
                    m_Tariffs.Add(_ts)
                End If

            Next

            For Each _tm As Business.Tariff In Business.Tariff.RetreiveAllDailyTariffs(_SiteID)

                If cbxTariff.Properties.Items.Count = 0 Then
                    cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_tm._ID.Value.ToString.ToUpper, _tm._Name.TrimEnd + "(MS)"))
                    m_Tariffs.Add(_tm)
                End If

                Dim exists As Boolean = False
                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxTariff.Properties.Items
                    If item.Description = _tm._Name Then exists = True
                Next
                If Not exists Then
                    cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_tm._ID.Value.ToString.ToUpper, _tm._Name.TrimEnd + "(MS)"))
                    m_Tariffs.Add(_tm)
                End If

                cbxTariff.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_tm._ID.Value.ToString.ToUpper, _tm._Name.TrimEnd + "(MS)"))
                m_Tariffs.Add(_tm)
            Next

        End If

        SetVerticalSize(cbxTariff)

    End Sub

    Private Sub frmPlanner_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        SavePreference("REGISTEROPTIONS", chkGridOptions.Text)
    End Sub

    Private Sub SavePreference(ByVal ParamID As String, ByVal ParamValue As String)
        If UserPreferenceHandler.CheckExists(ParamID) Then
            UserPreferenceHandler.SetString(ParamID, ParamValue)
        Else
            UserPreferenceHandler.Create(ParamID, UserPreferenceHandler.EnumUserPreferenceType.StringType, ParamValue)
        End If
    End Sub

    Private Sub SetVerticalSize(ByRef ComboIn As DevExpress.XtraEditors.CheckedComboBoxEdit)
        ComboIn.Properties.DropDownRows = ComboIn.Properties.Items.Count + 1
    End Sub

    Private Sub btnMonday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnMonday.ButtonClick
        DayRegister(DayOfWeek.Monday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnTuesday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnTuesday.ButtonClick
        DayRegister(DayOfWeek.Tuesday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnWednesday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnWednesday.ButtonClick
        DayRegister(DayOfWeek.Wednesday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnThursday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnThursday.ButtonClick
        DayRegister(DayOfWeek.Thursday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnFriday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnFriday.ButtonClick
        DayRegister(DayOfWeek.Friday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnSaturday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnSaturday.ButtonClick
        DayRegister(DayOfWeek.Saturday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnSunday_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnSunday.ButtonClick
        DayRegister(DayOfWeek.Sunday, e.Ref1, e.ShiftDown)
    End Sub

    Private Sub btnPrint_ButtonClick(sender As Object, e As CareDropDownButton.ButtonArgs) Handles btnPrint.ButtonClick

        If e.Ref1 = "" Then Exit Sub

        SetButtons(False)
        Session.CursorWaiting()

        If e.ShiftDown Then
            Dim _EmptyData As New List(Of ReportWeek)
            ReportHandler.DesignReport(e.Ref1, _EmptyData)
        Else

            If cgWC.RecordCount <= 0 Then Exit Sub
            If cgWC.FocusedRowHandle < 0 Then Exit Sub

            BuildPrintDataWeek(ValueHandler.ConvertDate(cgWC.CurrentRow("WC")).Value)
            ReportHandler.RunReport(e.Ref1, m_ReportWeekData)

        End If

        SetButtons(True)
        Session.CursorDefault()

    End Sub

    Private Sub DayRegister(ByVal ForDay As DayOfWeek, ByVal LayoutFile As String, ByVal Designer As Boolean)

        If LayoutFile = "" Then Exit Sub

        SetButtons(False)
        Session.CursorWaiting()

        If Designer Then
            Dim _EmptyData As New List(Of ReportDay)
            ReportHandler.DesignReport(LayoutFile, _EmptyData)
        Else

            Dim _Records As List(Of ReportDay) = ReturnDayRegisterData(ForDay)

            If _Records.Count > 0 Then
                ReportHandler.RunReport(LayoutFile, _Records)
            End If

        End If

        SetButtons(True)
        Session.CursorDefault()

    End Sub

    Private Function ReturnDayRegisterData(ByVal ForDay As DayOfWeek) As List(Of ReportDay)

        Dim _ReportDate As Date = m_WeekCommencing
        If ForDay = DayOfWeek.Tuesday Then _ReportDate = _ReportDate.AddDays(1)
        If ForDay = DayOfWeek.Wednesday Then _ReportDate = _ReportDate.AddDays(2)
        If ForDay = DayOfWeek.Thursday Then _ReportDate = _ReportDate.AddDays(3)
        If ForDay = DayOfWeek.Friday Then _ReportDate = _ReportDate.AddDays(4)
        If ForDay = DayOfWeek.Saturday Then _ReportDate = _ReportDate.AddDays(5)
        If ForDay = DayOfWeek.Sunday Then _ReportDate = _ReportDate.AddDays(6)

        Dim _Records As New List(Of ReportDay)

        For Each _gw In m_GridData

            Dim _Tariff As String = ""
            Dim _BoltOns As String = ""

            If ForDay = DayOfWeek.Monday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Monday <> "" AndAlso _gw.MondayStatus <> "Holiday" Then
                        _Tariff = _gw.Monday
                        _BoltOns = _gw.MondayBoltOns
                    End If
                Else
                    If _gw.Monday <> "" Then _Tariff = _gw.Monday
                    _BoltOns = _gw.MondayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Tuesday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Tuesday <> "" AndAlso _gw.TuesdayStatus <> "Holiday" Then
                        _Tariff = _gw.Tuesday
                        _BoltOns = _gw.TuesdayBoltOns
                    End If
                Else
                    If _gw.Tuesday <> "" Then _Tariff = _gw.Tuesday
                    _BoltOns = _gw.TuesdayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Wednesday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Wednesday <> "" AndAlso _gw.WednesdayStatus <> "Holiday" Then
                        _Tariff = _gw.Wednesday
                        _BoltOns = _gw.WednesdayBoltOns
                    End If
                Else
                    If _gw.Wednesday <> "" Then _Tariff = _gw.Wednesday
                    _BoltOns = _gw.WednesdayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Thursday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Thursday <> "" AndAlso _gw.ThursdayStatus <> "Holiday" Then
                        _Tariff = _gw.Thursday
                        _BoltOns = _gw.ThursdayBoltOns
                    End If
                Else
                    If _gw.Thursday <> "" Then _Tariff = _gw.Thursday
                    _BoltOns = _gw.ThursdayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Friday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Friday <> "" AndAlso _gw.FridayStatus <> "Holiday" Then
                        _Tariff = _gw.Friday
                        _BoltOns = _gw.FridayBoltOns
                    End If
                Else
                    If _gw.Friday <> "" Then _Tariff = _gw.Friday
                    _BoltOns = _gw.FridayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Saturday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Saturday <> "" AndAlso _gw.SaturdayStatus <> "Holiday" Then
                        _Tariff = _gw.Saturday
                        _BoltOns = _gw.SaturdayBoltOns
                    End If
                Else
                    If _gw.Saturday <> "" Then _Tariff = _gw.Saturday
                    _BoltOns = _gw.SaturdayBoltOns
                End If
            End If

            If ForDay = DayOfWeek.Sunday Then
                If chkGridOptions.Text.Contains("Hide Holidays") Then
                    If _gw.Sunday <> "" AndAlso _gw.SundayStatus <> "Holiday" Then
                        _Tariff = _gw.Sunday
                        _BoltOns = _gw.SundayBoltOns
                    End If
                Else
                    If _gw.Sunday <> "" Then _Tariff = _gw.Sunday
                    _BoltOns = _gw.SundayBoltOns
                End If
            End If

            If _Tariff <> "" Then

                Dim _wr As New ReportDay
                With _wr

                    .ReportDate = _ReportDate
                    .ChildName = _gw.ChildName
                    .ChildAge = _gw.ChildAge
                    .ChildGroup = _gw.ChildRoom
                    .ChildKeyworker = _gw.ChildKeyworker
                    .ChildSchool = _gw.ChildSchool
                    .ChildDOB = _gw.ChildDOB.Value
                    .Tariff = _Tariff
                    .BoltOns = _BoltOns

                    Dim _C As Business.Child = Business.Child.RetreiveByID(_gw.ChildID)
                    If _C IsNot Nothing Then

                        Dim _Medical As String = BuildMedical(_C._MedAllergies, _C._MedMedication, _C._MedNotes)
                        Dim _Contacts As String = BuildContacts(_C._FamilyId.ToString)

                        .ChildMedical = _Medical
                        .ChildContacts = _Contacts
                        .Nappies = _C._Nappies
                        .BabyFood = _C._BabyFood
                        .ChildYM = ValueHandler.DateDifferenceAsTextYM(.ChildDOB, Today)
                        .ChildForename = _C._Forename
                        .ChildSurname = _C._Surname
                        .MedAllergies = _C._MedAllergies
                        .MedMedical = _C._MedMedication
                        .MedOther = _C._MedNotes

                        Dim _P As Business.Contact = Business.Contact.RetrievePrimaryContact(_C._FamilyId.ToString)
                        If _P IsNot Nothing Then
                            .PrimaryName = _P._Fullname
                            .PrimaryPassword = _P._Password
                            .PrimaryMobile = _P._TelMobile
                        End If

                    End If

                    .LogicalGroup = 0
                    .AgeDays = CInt(DateDiff(DateInterval.Day, _C._Dob.Value, _ReportDate))

                    .ChildRef1 = _gw.ChildRef1
                    .ChildRef2 = _gw.ChildRef2
                    .ChildTags = _gw.ChildTags

                End With

                _Records.Add(_wr)

            End If

        Next

        Dim _Groups As Integer = cbxSplit.SelectedIndex + 1
        Dim _Group As Integer = 1
        Dim _GroupSize As Integer = _Records.Count \ _Groups
        Dim _LeftToSplit As Integer = _GroupSize

        For Each _r In _Records

            If _LeftToSplit > 0 Then
                _r.LogicalGroup = _Group
                _LeftToSplit -= 1
            Else
                _Group += 1
                _LeftToSplit = _GroupSize
                _r.LogicalGroup = _Group
            End If

        Next

        Return _Records

    End Function

    Private Sub btnPivotExport_Click(sender As Object, e As EventArgs) Handles btnPivotExport.Click

        Dim _sfd As New SaveFileDialog

        _sfd.AddExtension = True
        _sfd.Filter = "Excel Files |.xlsx|All Files |*.*"
        _sfd.DefaultExt = "xlsx"

        _sfd.ShowDialog()

        If Not _sfd.FileName = "" Then
            If _sfd.CheckPathExists Then
                PivotGridControl1.ExportToXlsx(_sfd.FileName)
                Threading.Thread.Sleep(3000)
                FileHandler.OpenFile(_sfd.FileName)
            End If
        End If

    End Sub

End Class