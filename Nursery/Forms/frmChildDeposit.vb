﻿Imports Care.Global

Public Class frmChildDeposit

    Private m_ChildID As Guid
    Private m_DepositID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal ChildID As Guid, ByVal ChargeID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ChildID = ChildID

        If ChargeID.HasValue Then
            m_IsNew = False
            m_DepositID = ChargeID
        Else
            m_IsNew = True
            m_DepositID = Nothing
        End If

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtDepDate.BackColor = Session.ChangeColour
        txtDepValue.BackColor = Session.ChangeColour
        chkDepPost.BackColor = Session.ChangeColour
        txtDepRef.BackColor = Session.ChangeColour
        txtDepNotes.BackColor = Session.ChangeColour

        If m_DepositID.HasValue Then
            Me.Text = "Edit Deposit Transaction"
            DisplayRecord()
        Else
            Me.Text = "Create New Deposit Transaction"
        End If

    End Sub

    Private Sub frmChildDeposit_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtDepDate.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _D As Business.ChildDeposit = Business.ChildDeposit.RetreiveByID(m_DepositID.Value)
        With _D

            If _D._DepType = "Take Deposit" Then
                radTake.Checked = True
            Else
                radReturn.Checked = True
            End If

            cdtDepDate.Value = ._DepDate
            txtDepValue.Text = ValueHandler.MoneyAsText(_D._DepValue)
            chkDepPost.Checked = ._DepPost
            txtDepRef.Text = ._DepRef
            txtDepNotes.Text = ._DepNotes

        End With

        _D = Nothing

    End Sub

    Private Function ValidateDeposit() As Boolean

        If cdtDepDate.Text = "" Then Return False
        If cdtDepDate.Value Is Nothing Then Return False
        If Not cdtDepDate.Value.HasValue Then Return False
        If txtDepValue.Text = "" Then Return False
        If ValueHandler.ConvertDecimal(txtDepValue.Text) <= 0 Then Return False

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateDeposit() Then Exit Sub

        Dim _D As Business.ChildDeposit = Nothing

        If m_IsNew Then
            _D = New Business.ChildDeposit
            _D._ID = Guid.NewGuid
            _D._ChildId = m_ChildID
        Else
            _D = Business.ChildDeposit.RetreiveByID(m_DepositID.Value)
        End If

        With _D

            If radTake.Checked Then
                ._DepType = "Take Deposit"
            Else
                ._DepType = "Return Deposit"
            End If

            ._DepDate = cdtDepDate.Value

            If radTake.Checked Then
                ._DepValue = ValueHandler.ConvertDecimal(txtDepValue.Text)
            Else
                ._DepValue = ValueHandler.ConvertDecimal(txtDepValue.Text) * -1
            End If

            ._DepPost = chkDepPost.Checked
            ._DepRef = txtDepRef.Text
            ._DepNotes = txtDepNotes.Text

            .Store()

        End With

        _D = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
