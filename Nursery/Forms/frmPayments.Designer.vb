﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayments
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.calDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxMethod = New Care.Controls.CareComboBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.gbxNew = New Care.Controls.CareFrame()
        Me.txtValue = New Care.Controls.CareTextBox(Me.components)
        Me.txtBalance = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnSearch = New Care.Controls.CareButton(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef3 = New Care.Controls.CareTextBox(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef2 = New Care.Controls.CareTextBox(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef1 = New Care.Controls.CareTextBox(Me.components)
        Me.Label6 = New Care.Controls.CareLabel(Me.components)
        Me.txtFamily = New Care.Controls.CareTextBox(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnUpdate = New Care.Controls.CareButton(Me.components)
        Me.ugPayments = New Care.Controls.CareGrid()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.calDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.calDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxNew.SuspendLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.calDate)
        Me.GroupBox1.Controls.Add(Me.cbxMethod)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(761, 58)
        Me.GroupBox1.TabIndex = 0
        '
        'calDate
        '
        Me.calDate.EditValue = Nothing
        Me.calDate.EnterMoveNextControl = True
        Me.calDate.Location = New System.Drawing.Point(104, 21)
        Me.calDate.Name = "calDate"
        Me.calDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.calDate.Properties.Appearance.Options.UseFont = True
        Me.calDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.calDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.calDate.ReadOnly = False
        Me.calDate.Size = New System.Drawing.Size(100, 22)
        Me.calDate.TabIndex = 0
        Me.calDate.Value = Nothing
        '
        'cbxMethod
        '
        Me.cbxMethod.AllowBlank = False
        Me.cbxMethod.DataSource = Nothing
        Me.cbxMethod.DisplayMember = Nothing
        Me.cbxMethod.EnterMoveNextControl = True
        Me.cbxMethod.Location = New System.Drawing.Point(543, 22)
        Me.cbxMethod.Name = "cbxMethod"
        Me.cbxMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMethod.ReadOnly = False
        Me.cbxMethod.SelectedValue = Nothing
        Me.cbxMethod.Size = New System.Drawing.Size(206, 22)
        Me.cbxMethod.TabIndex = 1
        Me.cbxMethod.ValueMember = Nothing
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(438, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Payment Method"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(10, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Payment Date"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxNew
        '
        Me.gbxNew.Controls.Add(Me.txtValue)
        Me.gbxNew.Controls.Add(Me.txtBalance)
        Me.gbxNew.Controls.Add(Me.Label2)
        Me.gbxNew.Controls.Add(Me.btnCancel)
        Me.gbxNew.Controls.Add(Me.btnOK)
        Me.gbxNew.Controls.Add(Me.btnSearch)
        Me.gbxNew.Controls.Add(Me.Label9)
        Me.gbxNew.Controls.Add(Me.txtRef3)
        Me.gbxNew.Controls.Add(Me.Label8)
        Me.gbxNew.Controls.Add(Me.txtRef2)
        Me.gbxNew.Controls.Add(Me.Label7)
        Me.gbxNew.Controls.Add(Me.txtRef1)
        Me.gbxNew.Controls.Add(Me.Label6)
        Me.gbxNew.Controls.Add(Me.txtFamily)
        Me.gbxNew.Controls.Add(Me.Label5)
        Me.gbxNew.Location = New System.Drawing.Point(180, 137)
        Me.gbxNew.Name = "gbxNew"
        Me.gbxNew.ShowCaption = False
        Me.gbxNew.Size = New System.Drawing.Size(425, 275)
        Me.gbxNew.TabIndex = 2
        '
        'txtValue
        '
        Me.txtValue.CharacterCasing = CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(81, 173)
        Me.txtValue.MaxLength = 0
        Me.txtValue.Name = "txtValue"
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtValue.ReadOnly = False
        Me.txtValue.Size = New System.Drawing.Size(99, 22)
        Me.txtValue.TabIndex = 6
        Me.txtValue.TabStop = False
        Me.txtValue.Tag = "AEM2DP"
        Me.txtValue.TextAlign = HorizontalAlignment.Right
        Me.txtValue.ToolTipText = ""
        '
        'txtBalance
        '
        Me.txtBalance.CharacterCasing = CharacterCasing.Normal
        Me.txtBalance.EnterMoveNextControl = True
        Me.txtBalance.Location = New System.Drawing.Point(81, 48)
        Me.txtBalance.MaxLength = 0
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBalance.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBalance.Properties.Appearance.Options.UseFont = True
        Me.txtBalance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtBalance.ReadOnly = False
        Me.txtBalance.Size = New System.Drawing.Size(99, 22)
        Me.txtBalance.TabIndex = 2
        Me.txtBalance.TabStop = False
        Me.txtBalance.Tag = "R"
        Me.txtBalance.TextAlign = HorizontalAlignment.Right
        Me.txtBalance.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Balance"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(339, 243)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(258, 243)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 7
        Me.btnOK.Text = "OK"
        '
        'btnSearch
        '
        Me.btnSearch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSearch.Appearance.Options.UseFont = True
        Me.btnSearch.CausesValidation = False
        Me.btnSearch.Location = New System.Drawing.Point(341, 19)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 1
        Me.btnSearch.TabStop = False
        Me.btnSearch.Text = "Search"
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(10, 176)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 15)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Value"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef3
        '
        Me.txtRef3.CharacterCasing = CharacterCasing.Upper
        Me.txtRef3.EnterMoveNextControl = True
        Me.txtRef3.Location = New System.Drawing.Point(81, 145)
        Me.txtRef3.MaxLength = 0
        Me.txtRef3.Name = "txtRef3"
        Me.txtRef3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef3.Properties.Appearance.Options.UseFont = True
        Me.txtRef3.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef3.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtRef3.ReadOnly = False
        Me.txtRef3.Size = New System.Drawing.Size(254, 22)
        Me.txtRef3.TabIndex = 5
        Me.txtRef3.Tag = "AE"
        Me.txtRef3.TextAlign = HorizontalAlignment.Left
        Me.txtRef3.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(10, 148)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(26, 15)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Ref 3"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef2
        '
        Me.txtRef2.CharacterCasing = CharacterCasing.Upper
        Me.txtRef2.EnterMoveNextControl = True
        Me.txtRef2.Location = New System.Drawing.Point(81, 116)
        Me.txtRef2.MaxLength = 0
        Me.txtRef2.Name = "txtRef2"
        Me.txtRef2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef2.Properties.Appearance.Options.UseFont = True
        Me.txtRef2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef2.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtRef2.ReadOnly = False
        Me.txtRef2.Size = New System.Drawing.Size(254, 22)
        Me.txtRef2.TabIndex = 4
        Me.txtRef2.Tag = "AE"
        Me.txtRef2.TextAlign = HorizontalAlignment.Left
        Me.txtRef2.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(10, 119)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 15)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Ref 2"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef1
        '
        Me.txtRef1.CharacterCasing = CharacterCasing.Upper
        Me.txtRef1.EnterMoveNextControl = True
        Me.txtRef1.Location = New System.Drawing.Point(81, 87)
        Me.txtRef1.MaxLength = 0
        Me.txtRef1.Name = "txtRef1"
        Me.txtRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef1.Properties.Appearance.Options.UseFont = True
        Me.txtRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtRef1.ReadOnly = False
        Me.txtRef1.Size = New System.Drawing.Size(254, 22)
        Me.txtRef1.TabIndex = 3
        Me.txtRef1.Tag = "AE"
        Me.txtRef1.TextAlign = HorizontalAlignment.Left
        Me.txtRef1.ToolTipText = ""
        '
        'Label6
        '
        Me.Label6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label6.Location = New System.Drawing.Point(10, 90)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(26, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Ref 1"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFamily
        '
        Me.txtFamily.CharacterCasing = CharacterCasing.Normal
        Me.txtFamily.EnterMoveNextControl = True
        Me.txtFamily.Location = New System.Drawing.Point(81, 20)
        Me.txtFamily.MaxLength = 0
        Me.txtFamily.Name = "txtFamily"
        Me.txtFamily.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFamily.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFamily.Properties.Appearance.Options.UseFont = True
        Me.txtFamily.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFamily.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFamily.ReadOnly = False
        Me.txtFamily.Size = New System.Drawing.Size(254, 22)
        Me.txtFamily.TabIndex = 0
        Me.txtFamily.TabStop = False
        Me.txtFamily.Tag = ""
        Me.txtFamily.TextAlign = HorizontalAlignment.Left
        Me.txtFamily.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(10, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Family"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(12, 485)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(125, 23)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Add Payment"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(143, 485)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(125, 23)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "Edit Payment"
        '
        'btnRemove
        '
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.Location = New System.Drawing.Point(274, 485)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(125, 23)
        Me.btnRemove.TabIndex = 5
        Me.btnRemove.Text = "Remove Payment"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(698, 485)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "Close"
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnUpdate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnUpdate.Appearance.Options.UseFont = True
        Me.btnUpdate.Location = New System.Drawing.Point(617, 485)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 6
        Me.btnUpdate.Text = "Update"
        '
        'ugPayments
        '
        Me.ugPayments.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ugPayments.Appearance.Options.UseFont = True
        Me.ugPayments.HideFirstColumn = False
        Me.ugPayments.Location = New System.Drawing.Point(12, 76)
        Me.ugPayments.Name = "ugPayments"
        Me.ugPayments.ShowGroupByBox = False
        Me.ugPayments.Size = New System.Drawing.Size(761, 403)
        Me.ugPayments.TabIndex = 1
        '
        'frmPayments
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.ClientSize = New System.Drawing.Size(785, 516)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.gbxNew)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ugPayments)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayments"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.calDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.calDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxNew.ResumeLayout(False)
        Me.gbxNew.PerformLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents cbxMethod As Care.Controls.CareComboBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents gbxNew As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnSearch As Care.Controls.CareButton
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents txtRef3 As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents txtRef2 As Care.Controls.CareTextBox
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents txtRef1 As Care.Controls.CareTextBox
    Friend WithEvents Label6 As Care.Controls.CareLabel
    Friend WithEvents txtFamily As Care.Controls.CareTextBox
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnUpdate As Care.Controls.CareButton
    Friend WithEvents txtBalance As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents ugPayments As Care.Controls.CareGrid
    Friend WithEvents txtValue As Care.Controls.CareTextBox
    Friend WithEvents calDate As Care.Controls.CareDateTime

End Class
