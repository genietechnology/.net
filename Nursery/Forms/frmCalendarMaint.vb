﻿Imports Care.Global
Imports Care.Data

Public Class frmCalendarMaint

    Private Sub frmCalendarMaint_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtYear.Text = Today.Year.ToString
        DisplayDays()
        gbxAmend.Hide()

        txtHolidayName.BackColor = Session.ChangeColour
        chkClosed.BackColor = Session.ChangeColour
        chkNoCharge.BackColor = Session.ChangeColour

    End Sub

    Private Sub btnRebuild_Click(sender As Object, e As EventArgs) Handles btnRebuild.Click

        If txtYear.Text = "" Then Exit Sub

        If CareMessage("Are you sure you want to rebuild the calendar for " + txtYear.Text + "?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Confirm Rebuild") = DialogResult.Yes Then

            btnRebuild.Enabled = False
            Session.CursorWaiting()

            Dim _BE As New BookingsEngine
            _BE.BuildCalendar(CInt(txtYear.Text))

            _BE.Dispose()
            _BE = Nothing

            btnRebuild.Enabled = True

            DisplayDays()

            Session.CursorDefault()

            CareMessage("Build Completed.", MessageBoxIcon.Information, "Build Calendar")

        End If

    End Sub

    Private Sub grdDays_GridDoubleClick(sender As Object, e As EventArgs) Handles grdDays.GridDoubleClick
        EditRecord()
    End Sub

    Private Sub DisplayDays()

        If txtYear.Text = "" Then Exit Sub

        Dim _SQL As String = "select id, date as 'Date', dayofweek as 'Day', datename(M,date) as 'Month', holiday as 'Holiday'," & _
                             " holiday_name as 'Holiday Name', closed as 'Closed', no_charge as 'No Charge'" & _
                             " from Calendar" & _
                             " where year(date) = " & txtYear.Text & _
                             " order by date"

        grdDays.HideFirstColumn = True
        grdDays.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub EditRecord()
        If grdDays.RecordCount < 1 Then Exit Sub
        Dim _ID As New Guid(grdDays.CurrentRow.Item("id").ToString)
        Edit(_ID)
    End Sub

    Private Sub Edit(ByVal ID As Guid)

        btnOK.Tag = ID

        txtDate.Text = Format(grdDays.CurrentRow.Item("Date"), "dd/MM/yyyy")
        txtDayOfWeek.Text = grdDays.CurrentRow.Item("Day").ToString

        txtHolidayName.Text = grdDays.CurrentRow.Item("Holiday Name").ToString

        If grdDays.CurrentRow.Item("Holiday").ToString = "True" Then
            txtHoliday.Text = "Yes"
        Else
            txtHoliday.Text = "No"
        End If

        If grdDays.CurrentRow.Item("Closed").ToString = "True" Then
            chkClosed.Checked = True
        Else
            chkClosed.Checked = False
        End If

        If grdDays.CurrentRow.Item("No Charge").ToString = "True" Then
            chkNoCharge.Checked = True
        Else
            chkNoCharge.Checked = False
        End If

        gbxAmend.Show()
        txtHolidayName.Focus()

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        DisplayDays()
    End Sub

    Private Sub btnAmend_Click(sender As Object, e As EventArgs) Handles btnAmend.Click
        EditRecord()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        btnOK.Tag = Nothing
        gbxAmend.Hide()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        grdDays.PersistRowPosition()

        Dim _C As Business.Calendar = Business.Calendar.RetreiveByID(New Guid(btnOK.Tag.ToString))
        With _C
            ._HolidayName = txtHolidayName.Text
            ._Closed = chkClosed.Checked
            ._NoCharge = chkNoCharge.Checked
            .Store()
        End With

        btnOK.Tag = Nothing

        DisplayDays()

        grdDays.RestoreRowPosition()

        gbxAmend.Hide()

    End Sub
End Class
