﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportMilk
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.cdtTo = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtMLPerChild = New Care.Controls.CareTextBox(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.txtMLPerUnit = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtCostPerUnit = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMLPerChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMLPerUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCostPerUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtCostPerUnit)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtMLPerUnit)
        Me.GroupControl1.Controls.Add(Me.txtMLPerChild)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cdtTo)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.cdtFrom)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(342, 142)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Report Criteria && Parameters"
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(9, 30)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Action Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(108, 27)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtFrom.TabIndex = 1
        Me.cdtFrom.Tag = ""
        Me.cdtFrom.Value = Nothing
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = Nothing
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(231, 27)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtTo.TabIndex = 2
        Me.cdtTo.Tag = ""
        Me.cdtTo.Value = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(214, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel1.TabIndex = 3
        Me.CareLabel1.Text = "to"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMLPerChild
        '
        Me.txtMLPerChild.CharacterCasing = CharacterCasing.Normal
        Me.txtMLPerChild.EnterMoveNextControl = True
        Me.txtMLPerChild.Location = New System.Drawing.Point(108, 55)
        Me.txtMLPerChild.MaxLength = 3
        Me.txtMLPerChild.Name = "txtMLPerChild"
        Me.txtMLPerChild.NumericAllowNegatives = False
        Me.txtMLPerChild.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtMLPerChild.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMLPerChild.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMLPerChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMLPerChild.Properties.Appearance.Options.UseFont = True
        Me.txtMLPerChild.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMLPerChild.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMLPerChild.Properties.Mask.EditMask = "##########;"
        Me.txtMLPerChild.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMLPerChild.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMLPerChild.Properties.MaxLength = 3
        Me.txtMLPerChild.Size = New System.Drawing.Size(60, 22)
        Me.txtMLPerChild.TabIndex = 5
        Me.txtMLPerChild.Tag = ""
        Me.txtMLPerChild.TextAlign = HorizontalAlignment.Left
        Me.txtMLPerChild.ToolTipText = ""
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(9, 58)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(68, 15)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "ML per Child"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMLPerUnit
        '
        Me.txtMLPerUnit.CharacterCasing = CharacterCasing.Normal
        Me.txtMLPerUnit.EnterMoveNextControl = True
        Me.txtMLPerUnit.Location = New System.Drawing.Point(108, 83)
        Me.txtMLPerUnit.MaxLength = 4
        Me.txtMLPerUnit.Name = "txtMLPerUnit"
        Me.txtMLPerUnit.NumericAllowNegatives = False
        Me.txtMLPerUnit.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtMLPerUnit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMLPerUnit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMLPerUnit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMLPerUnit.Properties.Appearance.Options.UseFont = True
        Me.txtMLPerUnit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMLPerUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMLPerUnit.Properties.Mask.EditMask = "##########;"
        Me.txtMLPerUnit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMLPerUnit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMLPerUnit.Properties.MaxLength = 4
        Me.txtMLPerUnit.Size = New System.Drawing.Size(60, 22)
        Me.txtMLPerUnit.TabIndex = 8
        Me.txtMLPerUnit.Tag = ""
        Me.txtMLPerUnit.TextAlign = HorizontalAlignment.Left
        Me.txtMLPerUnit.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 86)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel2.TabIndex = 7
        Me.CareLabel2.Text = "ML per Unit"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 114)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(69, 15)
        Me.CareLabel3.TabIndex = 10
        Me.CareLabel3.Text = "Cost per Unit"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCostPerUnit
        '
        Me.txtCostPerUnit.CharacterCasing = CharacterCasing.Normal
        Me.txtCostPerUnit.EnterMoveNextControl = True
        Me.txtCostPerUnit.Location = New System.Drawing.Point(108, 111)
        Me.txtCostPerUnit.MaxLength = 5
        Me.txtCostPerUnit.Name = "txtCostPerUnit"
        Me.txtCostPerUnit.NumericAllowNegatives = False
        Me.txtCostPerUnit.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCostPerUnit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCostPerUnit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCostPerUnit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCostPerUnit.Properties.Appearance.Options.UseFont = True
        Me.txtCostPerUnit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCostPerUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCostPerUnit.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCostPerUnit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCostPerUnit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCostPerUnit.Properties.MaxLength = 5
        Me.txtCostPerUnit.Size = New System.Drawing.Size(60, 22)
        Me.txtCostPerUnit.TabIndex = 11
        Me.txtCostPerUnit.Tag = ""
        Me.txtCostPerUnit.TextAlign = HorizontalAlignment.Left
        Me.txtCostPerUnit.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(174, 58)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(109, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "e.g. 1/3 Pint = 189ml"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(174, 86)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel5.TabIndex = 9
        Me.CareLabel5.Text = "e.g. 568ml per Pint"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(174, 114)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(94, 15)
        Me.CareLabel6.TabIndex = 12
        Me.CareLabel6.Text = "e.g. £0.50 per Pint"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnRun.Location = New System.Drawing.Point(198, 160)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 1
        Me.btnRun.Text = "Run"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(279, 160)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'frmReportMilk
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 190)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupControl1)
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportMilk"
        Me.Text = ""
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMLPerChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMLPerUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCostPerUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtCostPerUnit As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtMLPerUnit As Care.Controls.CareTextBox
    Friend WithEvents txtMLPerChild As Care.Controls.CareTextBox
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
End Class
