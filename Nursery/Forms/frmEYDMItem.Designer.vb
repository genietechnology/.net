﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEYDMItem
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtStatement = New DevExpress.XtraEditors.MemoEdit()
        Me.cbxAge = New Care.Controls.CareComboBox(Me.components)
        Me.cbxArea = New Care.Controls.CareComboBox(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.txtSeq = New Care.Controls.CareTextBox(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.btnSaveClose = New Care.Controls.CareButton(Me.components)
        Me.btnSaveAdd = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtStatement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxAge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtStatement)
        Me.GroupBox1.Controls.Add(Me.cbxAge)
        Me.GroupBox1.Controls.Add(Me.cbxArea)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.txtSeq)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.lblFamily)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(479, 239)
        Me.GroupBox1.TabIndex = 0
        '
        'txtStatement
        '
        Me.txtStatement.Location = New System.Drawing.Point(91, 85)
        Me.txtStatement.Name = "txtStatement"
        Me.txtStatement.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatement.Properties.Appearance.Options.UseFont = True
        Me.txtStatement.Properties.MaxLength = 500
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtStatement, True)
        Me.txtStatement.Size = New System.Drawing.Size(378, 116)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtStatement, OptionsSpelling2)
        Me.txtStatement.TabIndex = 5
        Me.txtStatement.Tag = "M"
        Me.txtStatement.UseOptimizedRendering = True
        '
        'cbxAge
        '
        Me.cbxAge.AllowBlank = False
        Me.cbxAge.DataSource = Nothing
        Me.cbxAge.DisplayMember = Nothing
        Me.cbxAge.EnterMoveNextControl = True
        Me.cbxAge.Location = New System.Drawing.Point(91, 57)
        Me.cbxAge.Name = "cbxAge"
        Me.cbxAge.Properties.AccessibleName = "Classification"
        Me.cbxAge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAge.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAge.Properties.Appearance.Options.UseFont = True
        Me.cbxAge.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAge.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAge.ReadOnly = False
        Me.cbxAge.SelectedValue = Nothing
        Me.cbxAge.Size = New System.Drawing.Size(378, 22)
        Me.cbxAge.TabIndex = 3
        Me.cbxAge.Tag = "M"
        Me.cbxAge.ValueMember = Nothing
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(91, 29)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AccessibleName = "Classification"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.ReadOnly = False
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(378, 22)
        Me.cbxArea.TabIndex = 1
        Me.cbxArea.Tag = "M"
        Me.cbxArea.ValueMember = Nothing
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(11, 88)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(54, 15)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Statement"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSeq
        '
        Me.txtSeq.CharacterCasing = CharacterCasing.Normal
        Me.txtSeq.EnterMoveNextControl = True
        Me.txtSeq.Location = New System.Drawing.Point(91, 207)
        Me.txtSeq.MaxLength = 10
        Me.txtSeq.Name = "txtSeq"
        Me.txtSeq.NumericAllowNegatives = False
        Me.txtSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSeq.Properties.Appearance.Options.UseFont = True
        Me.txtSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSeq.Properties.Mask.EditMask = "##########;"
        Me.txtSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSeq.Properties.MaxLength = 10
        Me.txtSeq.ReadOnly = False
        Me.txtSeq.Size = New System.Drawing.Size(60, 22)
        Me.txtSeq.TabIndex = 8
        Me.txtSeq.Tag = "M"
        Me.txtSeq.TextAlign = HorizontalAlignment.Left
        Me.txtSeq.ToolTipText = ""
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(11, 210)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(60, 15)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Display Seq"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(11, 32)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(71, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Area / Aspect"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(11, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Age"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSaveClose.Appearance.Options.UseFont = True
        Me.btnSaveClose.CausesValidation = False
        Me.btnSaveClose.Location = New System.Drawing.Point(315, 255)
        Me.btnSaveClose.Name = "btnSaveClose"
        Me.btnSaveClose.Size = New System.Drawing.Size(85, 23)
        Me.btnSaveClose.TabIndex = 2
        Me.btnSaveClose.Tag = ""
        Me.btnSaveClose.Text = "Save && Close"
        '
        'btnSaveAdd
        '
        Me.btnSaveAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSaveAdd.Appearance.Options.UseFont = True
        Me.btnSaveAdd.CausesValidation = False
        Me.btnSaveAdd.Location = New System.Drawing.Point(224, 255)
        Me.btnSaveAdd.Name = "btnSaveAdd"
        Me.btnSaveAdd.Size = New System.Drawing.Size(85, 23)
        Me.btnSaveAdd.TabIndex = 1
        Me.btnSaveAdd.Tag = ""
        Me.btnSaveAdd.Text = "Save && Add"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(406, 255)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'frmEYDMItem
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(502, 284)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSaveAdd)
        Me.Controls.Add(Me.btnSaveClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEYDMItem"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmEYDMItem"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtStatement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxAge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtStatement As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cbxAge As Care.Controls.CareComboBox
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents txtSeq As Care.Controls.CareTextBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents btnSaveClose As Care.Controls.CareButton
    Friend WithEvents btnSaveAdd As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
End Class
