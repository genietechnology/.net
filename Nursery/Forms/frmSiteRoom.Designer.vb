﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSiteRoom
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtSequence = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.chkStaff = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.chkChild = New Care.Controls.CareCheckBox(Me.components)
        Me.ceColour = New DevExpress.XtraEditors.ColorPickEdit()
        Me.cdtCloseTo = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtCloseFrom = New Care.Controls.CareDateTime(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtOpenFrom = New Care.Controls.CareTextBox(Me.components)
        Me.txtOpenTo = New Care.Controls.CareTextBox(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.chkMonitor = New Care.Controls.CareCheckBox(Me.components)
        Me.txtMonitorSerial = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.txtSequence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceColour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCloseTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCloseTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCloseFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCloseFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtOpenFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOpenTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkMonitor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonitorSerial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.txtSequence)
        Me.gbxWeekly.Controls.Add(Me.CareLabel9)
        Me.gbxWeekly.Controls.Add(Me.txtName)
        Me.gbxWeekly.Controls.Add(Me.CareLabel13)
        Me.gbxWeekly.Controls.Add(Me.CareLabel21)
        Me.gbxWeekly.Controls.Add(Me.chkStaff)
        Me.gbxWeekly.Controls.Add(Me.CareLabel6)
        Me.gbxWeekly.Controls.Add(Me.chkChild)
        Me.gbxWeekly.Controls.Add(Me.ceColour)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(423, 170)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Room Details"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 142)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 8
        Me.CareLabel1.Text = "Sequence"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSequence
        '
        Me.txtSequence.CharacterCasing = CharacterCasing.Normal
        Me.txtSequence.EnterMoveNextControl = True
        Me.txtSequence.Location = New System.Drawing.Point(112, 139)
        Me.txtSequence.MaxLength = 3
        Me.txtSequence.Name = "txtSequence"
        Me.txtSequence.NumericAllowNegatives = False
        Me.txtSequence.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtSequence.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSequence.Properties.AccessibleName = "Start Time"
        Me.txtSequence.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSequence.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSequence.Properties.Appearance.Options.UseFont = True
        Me.txtSequence.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSequence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSequence.Properties.Mask.EditMask = "##########;"
        Me.txtSequence.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSequence.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSequence.Properties.MaxLength = 3
        Me.txtSequence.Size = New System.Drawing.Size(75, 22)
        Me.txtSequence.TabIndex = 9
        Me.txtSequence.Tag = "AM"
        Me.txtSequence.TextAlign = HorizontalAlignment.Left
        Me.txtSequence.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 59)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel9.TabIndex = 2
        Me.CareLabel9.Text = "Colour"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(112, 29)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Qualification Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.Size = New System.Drawing.Size(300, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel13.TabIndex = 0
        Me.CareLabel13.Text = "Name"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(10, 116)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel21.TabIndex = 6
        Me.CareLabel21.Text = "Check In Staff"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkStaff
        '
        Me.chkStaff.EnterMoveNextControl = True
        Me.chkStaff.Location = New System.Drawing.Point(112, 114)
        Me.chkStaff.Name = "chkStaff"
        Me.chkStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStaff.Properties.Appearance.Options.UseFont = True
        Me.chkStaff.Size = New System.Drawing.Size(20, 19)
        Me.chkStaff.TabIndex = 7
        Me.chkStaff.Tag = "A"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 88)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Check In Child"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkChild
        '
        Me.chkChild.EnterMoveNextControl = True
        Me.chkChild.Location = New System.Drawing.Point(112, 86)
        Me.chkChild.Name = "chkChild"
        Me.chkChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChild.Properties.Appearance.Options.UseFont = True
        Me.chkChild.Size = New System.Drawing.Size(20, 19)
        Me.chkChild.TabIndex = 5
        Me.chkChild.Tag = "A"
        '
        'ceColour
        '
        Me.ceColour.EditValue = System.Drawing.Color.Empty
        Me.ceColour.Location = New System.Drawing.Point(112, 57)
        Me.ceColour.Name = "ceColour"
        Me.ceColour.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ceColour.Properties.ShowColorDialog = False
        Me.ceColour.Properties.ShowCustomColors = False
        Me.ceColour.Properties.ShowSystemColors = False
        Me.ceColour.Size = New System.Drawing.Size(157, 20)
        Me.ceColour.TabIndex = 3
        '
        'cdtCloseTo
        '
        Me.cdtCloseTo.EditValue = Nothing
        Me.cdtCloseTo.EnterMoveNextControl = True
        Me.cdtCloseTo.Location = New System.Drawing.Point(235, 29)
        Me.cdtCloseTo.Name = "cdtCloseTo"
        Me.cdtCloseTo.Properties.AccessibleName = ""
        Me.cdtCloseTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtCloseTo.Properties.Appearance.Options.UseFont = True
        Me.cdtCloseTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCloseTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtCloseTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtCloseTo.TabIndex = 3
        Me.cdtCloseTo.Tag = "A"
        Me.cdtCloseTo.Value = Nothing
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(193, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "to"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtCloseFrom
        '
        Me.cdtCloseFrom.EditValue = Nothing
        Me.cdtCloseFrom.EnterMoveNextControl = True
        Me.cdtCloseFrom.Location = New System.Drawing.Point(112, 29)
        Me.cdtCloseFrom.Name = "cdtCloseFrom"
        Me.cdtCloseFrom.Properties.AccessibleDescription = ""
        Me.cdtCloseFrom.Properties.AccessibleName = "Qualification Date"
        Me.cdtCloseFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtCloseFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtCloseFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCloseFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtCloseFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtCloseFrom.TabIndex = 1
        Me.cdtCloseFrom.Tag = "A"
        Me.cdtCloseFrom.Value = Nothing
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel4)
        Me.GroupControl4.Controls.Add(Me.CareLabel11)
        Me.GroupControl4.Controls.Add(Me.txtOpenFrom)
        Me.GroupControl4.Controls.Add(Me.txtOpenTo)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 188)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(423, 60)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Room Opening Hours"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Open From"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpenFrom
        '
        Me.txtOpenFrom.CharacterCasing = CharacterCasing.Normal
        Me.txtOpenFrom.EnterMoveNextControl = True
        Me.txtOpenFrom.Location = New System.Drawing.Point(112, 29)
        Me.txtOpenFrom.MaxLength = 8
        Me.txtOpenFrom.Name = "txtOpenFrom"
        Me.txtOpenFrom.NumericAllowNegatives = False
        Me.txtOpenFrom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtOpenFrom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOpenFrom.Properties.AccessibleName = "Start Time"
        Me.txtOpenFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOpenFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOpenFrom.Properties.Appearance.Options.UseFont = True
        Me.txtOpenFrom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOpenFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOpenFrom.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtOpenFrom.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtOpenFrom.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtOpenFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOpenFrom.Properties.MaxLength = 8
        Me.txtOpenFrom.Size = New System.Drawing.Size(75, 22)
        Me.txtOpenFrom.TabIndex = 1
        Me.txtOpenFrom.Tag = "AM"
        Me.txtOpenFrom.TextAlign = HorizontalAlignment.Left
        Me.txtOpenFrom.ToolTipText = ""
        '
        'txtOpenTo
        '
        Me.txtOpenTo.CharacterCasing = CharacterCasing.Normal
        Me.txtOpenTo.EnterMoveNextControl = True
        Me.txtOpenTo.Location = New System.Drawing.Point(210, 29)
        Me.txtOpenTo.MaxLength = 8
        Me.txtOpenTo.Name = "txtOpenTo"
        Me.txtOpenTo.NumericAllowNegatives = False
        Me.txtOpenTo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtOpenTo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOpenTo.Properties.AccessibleName = "End Time"
        Me.txtOpenTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOpenTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOpenTo.Properties.Appearance.Options.UseFont = True
        Me.txtOpenTo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOpenTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOpenTo.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtOpenTo.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtOpenTo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtOpenTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOpenTo.Properties.MaxLength = 8
        Me.txtOpenTo.Size = New System.Drawing.Size(75, 22)
        Me.txtOpenTo.TabIndex = 3
        Me.txtOpenTo.Tag = "AM"
        Me.txtOpenTo.TextAlign = HorizontalAlignment.Left
        Me.txtOpenTo.ToolTipText = ""
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(780, 389)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(689, 389)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 4
        Me.btnOK.Text = "OK"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.cdtCloseTo)
        Me.GroupControl1.Controls.Add(Me.cdtCloseFrom)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 254)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(423, 60)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Room Closure"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Closed From"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(218, 32)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "to"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel7)
        Me.GroupControl2.Controls.Add(Me.chkMonitor)
        Me.GroupControl2.Controls.Add(Me.txtMonitorSerial)
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 320)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(423, 60)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Paxton Integration"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(163, 32)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "Serial Number"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkMonitor
        '
        Me.chkMonitor.EnterMoveNextControl = True
        Me.chkMonitor.Location = New System.Drawing.Point(112, 30)
        Me.chkMonitor.Name = "chkMonitor"
        Me.chkMonitor.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMonitor.Properties.Appearance.Options.UseFont = True
        Me.chkMonitor.Size = New System.Drawing.Size(20, 19)
        Me.chkMonitor.TabIndex = 1
        Me.chkMonitor.Tag = "A"
        '
        'txtMonitorSerial
        '
        Me.txtMonitorSerial.CharacterCasing = CharacterCasing.Normal
        Me.txtMonitorSerial.EnterMoveNextControl = True
        Me.txtMonitorSerial.Location = New System.Drawing.Point(244, 29)
        Me.txtMonitorSerial.MaxLength = 40
        Me.txtMonitorSerial.Name = "txtMonitorSerial"
        Me.txtMonitorSerial.NumericAllowNegatives = False
        Me.txtMonitorSerial.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMonitorSerial.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMonitorSerial.Properties.AccessibleName = "Qualification Name"
        Me.txtMonitorSerial.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMonitorSerial.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMonitorSerial.Properties.Appearance.Options.UseFont = True
        Me.txtMonitorSerial.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMonitorSerial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMonitorSerial.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMonitorSerial.Properties.MaxLength = 40
        Me.txtMonitorSerial.Size = New System.Drawing.Size(168, 22)
        Me.txtMonitorSerial.TabIndex = 3
        Me.txtMonitorSerial.Tag = "A"
        Me.txtMonitorSerial.TextAlign = HorizontalAlignment.Left
        Me.txtMonitorSerial.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Monitoring"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Location = New System.Drawing.Point(442, 12)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(423, 368)
        Me.GroupControl3.TabIndex = 6
        Me.GroupControl3.Text = "Photo"
        '
        'frmSiteRoom
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(877, 421)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSiteRoom"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.txtSequence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceColour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCloseTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCloseTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCloseFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCloseFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtOpenFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOpenTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.chkMonitor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonitorSerial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtCloseTo As Care.Controls.CareDateTime
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtCloseFrom As Care.Controls.CareDateTime
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtOpenTo As Care.Controls.CareTextBox
    Friend WithEvents txtOpenFrom As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents ceColour As DevExpress.XtraEditors.ColorPickEdit
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents chkStaff As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkChild As Care.Controls.CareCheckBox
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkMonitor As Care.Controls.CareCheckBox
    Friend WithEvents txtMonitorSerial As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtSequence As Care.Controls.CareTextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl

End Class
