﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmCDAPDefinition

    Private m_Def As Business.CDAPDefinition

    Private Sub frmCDAPDefinition_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PopulateLists()
    End Sub

    Private Sub PopulateLists()

        With cbxArea
            .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, name from CDAPAreas order by seq").Rows
            .ValueMember = "ID"
            .DisplayMember = "name"
        End With

        With cbxStep
            .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, step from CDAPSteps order by step").Rows
            .ValueMember = "ID"
            .DisplayMember = "step"
        End With

        With cbxLetter
            .AddItem("a")
            .AddItem("b")
            .AddItem("c")
            .AddItem("d")
            .AddItem("e")
            .AddItem("f")
            .AddItem("g")
            .AddItem("h")
            .AddItem("i")
            .AddItem("j")
            .AddItem("k")
            .AddItem("l")
            .AddItem("m")
            .AddItem("n")
            .AddItem("o")
            .AddItem("p")
            .AddItem("q")
            .AddItem("r")
            .AddItem("s")
            .AddItem("t")
            .AddItem("u")
            .AddItem("v")
            .AddItem("w")
            .AddItem("x")
            .AddItem("y")
            .AddItem("z")
        End With

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Definition"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = "select area as 'Area', stepletter as 'Step/Letter', name as 'Name' from CDAPDefinitions"
            .GridOrderBy = "order by area, step, letter"
            .ReturnField = "ID"
            .Option1Caption = "Step/Letter"
            .Option1Field = "stepletter"
            .Option2Caption = "name"
            .Option2Field = "Name"
            .FormWidth = 750
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            MyBase.RecordPopulated = True
            m_Def = Business.CDAPDefinition.RetreiveByID(New Guid(_ReturnValue))
            bs.DataSource = m_Def
        End If

    End Sub

    Protected Overrides Sub SetBindings()

        m_Def = New Business.CDAPDefinition
        bs.DataSource = m_Def

        cbxArea.DataBindings.Add("Text", bs, "_Area")
        cbxStep.DataBindings.Add("Text", bs, "_Step")
        cbxLetter.DataBindings.Add("Text", bs, "_Letter")
        txtName.DataBindings.Add("Text", bs, "_Name")

        txtDescription.DataBindings.Add("Text", bs, "_Description")
        txtHow.DataBindings.Add("Text", bs, "_How")
        txtExamples.DataBindings.Add("Text", bs, "_Examples")
        txtProgOutcomes.DataBindings.Add("Text", bs, "_ProgOutcomes")
        txtCreativity.DataBindings.Add("Text", bs, "_Creativity")

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Def As Business.CDAPDefinition = CType(bs.Item(bs.Position), Business.CDAPDefinition)
        _Def._Stepletter = _Def._Step.ToString + _Def._Letter

        _Def.Store()

    End Sub

End Class
