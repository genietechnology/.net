﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceQBPreview
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.cgData = New Care.Controls.CareGrid()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(580, 428)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(145, 25)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'cgData
        '
        Me.cgData.AllowBuildColumns = True
        Me.cgData.AllowEdit = False
        Me.cgData.AllowHorizontalScroll = False
        Me.cgData.AllowMultiSelect = False
        Me.cgData.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgData.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgData.Appearance.Options.UseFont = True
        Me.cgData.AutoSizeByData = True
        Me.cgData.DisableAutoSize = False
        Me.cgData.DisableDataFormatting = False
        Me.cgData.FocusedRowHandle = -2147483648
        Me.cgData.HideFirstColumn = False
        Me.cgData.Location = New System.Drawing.Point(12, 12)
        Me.cgData.Name = "cgData"
        Me.cgData.PreviewColumn = ""
        Me.cgData.QueryID = Nothing
        Me.cgData.RowAutoHeight = False
        Me.cgData.SearchAsYouType = True
        Me.cgData.ShowAutoFilterRow = False
        Me.cgData.ShowFindPanel = True
        Me.cgData.ShowGroupByBox = False
        Me.cgData.ShowLoadingPanel = False
        Me.cgData.ShowNavigator = True
        Me.cgData.Size = New System.Drawing.Size(713, 408)
        Me.cgData.TabIndex = 6
        '
        'frmInvoiceQBPreview
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(734, 461)
        Me.Controls.Add(Me.cgData)
        Me.Controls.Add(Me.btnClose)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MinimizeBox = False
        Me.Name = "frmInvoiceQBPreview"
        Me.WindowState = FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents cgData As Care.Controls.CareGrid

End Class
