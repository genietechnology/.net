﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTouchLinks
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.txtArea = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtPath = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.txtPath)
        Me.gbx.Controls.Add(Me.CareLabel5)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.cbxType)
        Me.gbx.Controls.Add(Me.txtArea)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Size = New System.Drawing.Size(516, 167)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtArea, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxType, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel5, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtPath, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 136)
        Me.Panel2.Size = New System.Drawing.Size(510, 28)
        Me.Panel2.TabIndex = 8
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(325, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(416, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(552, 349)
        '
        'Panel1
        '
        Me.Panel1.Size = New System.Drawing.Size(571, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(477, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 52)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(57, 49)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.ReadOnly = False
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(447, 22)
        Me.cbxType.TabIndex = 3
        Me.cbxType.Tag = "AE"
        Me.cbxType.ValueMember = Nothing
        '
        'txtArea
        '
        Me.txtArea.CharacterCasing = CharacterCasing.Normal
        Me.txtArea.EnterMoveNextControl = True
        Me.txtArea.Location = New System.Drawing.Point(57, 21)
        Me.txtArea.MaxLength = 40
        Me.txtArea.Name = "txtArea"
        Me.txtArea.NumericAllowNegatives = False
        Me.txtArea.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtArea.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtArea.Properties.Appearance.Options.UseFont = True
        Me.txtArea.Properties.Appearance.Options.UseTextOptions = True
        Me.txtArea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtArea.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtArea.Properties.MaxLength = 40
        Me.txtArea.ReadOnly = False
        Me.txtArea.Size = New System.Drawing.Size(447, 22)
        Me.txtArea.TabIndex = 1
        Me.txtArea.Tag = "AE"
        Me.txtArea.TextAlign = HorizontalAlignment.Left
        Me.txtArea.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 24)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Area"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(57, 77)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(447, 22)
        Me.txtName.TabIndex = 5
        Me.txtName.Tag = "AE"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 80)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Name"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPath
        '
        Me.txtPath.CharacterCasing = CharacterCasing.Normal
        Me.txtPath.EnterMoveNextControl = True
        Me.txtPath.Location = New System.Drawing.Point(57, 105)
        Me.txtPath.MaxLength = 1024
        Me.txtPath.Name = "txtPath"
        Me.txtPath.NumericAllowNegatives = False
        Me.txtPath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPath.Properties.Appearance.Options.UseFont = True
        Me.txtPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPath.Properties.MaxLength = 1024
        Me.txtPath.ReadOnly = False
        Me.txtPath.Size = New System.Drawing.Size(447, 22)
        Me.txtPath.TabIndex = 7
        Me.txtPath.Tag = "AE"
        Me.txtPath.TextAlign = HorizontalAlignment.Left
        Me.txtPath.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(11, 108)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel5.TabIndex = 6
        Me.CareLabel5.Text = "Path"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTouchLinks
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(571, 402)
        Me.Name = "frmTouchLinks"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtPath As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents txtArea As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel

End Class
