﻿

Imports Care.Global
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmInvoiceSend

    Private m_PrintedOrEmailed As Boolean = False
    Private m_EmailOnly As Boolean = False
    Private m_Invoices As List(Of Guid)

    Public Sub New(ByVal Invoices As List(Of Guid), ByVal EmailOnly As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Invoices = Invoices
        m_EmailOnly = EmailOnly

    End Sub

    Private Sub frmInvoicePrint_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtSubject.Text = ParameterHandler.ReturnString("INVOICESUBJECT")
        txtBody.Text = ParameterHandler.ReturnString("INVOICEBODY")

        btnPrint.Visible = Not m_EmailOnly
        btnPrintAll.Visible = Not m_EmailOnly

        Session.CursorDefault()

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'always return OK as we will have re-applied the invoice numbers
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnEmail.Enabled = Enabled
        btnPrint.Enabled = Enabled
        btnPrintAll.Enabled = Enabled
        btnClose.Enabled = Enabled
    End Sub

    Private Sub Run(ByVal Mode As Reports.InvoiceRun.EnumOutputMode)

        SetCMDs(False)
        Session.CursorWaiting()
        Application.DoEvents()

        Dim _Errors As String = ""

        Dim _r As New Reports.InvoiceRun
        _r.RunInvoices(Mode, m_Invoices, txtNarrative.Text, _Errors, txtSubject.Text, txtBody.Text)
        _r = Nothing

        If _Errors <> "" Then

            Try
                Clipboard.Clear()
                Clipboard.SetText(_Errors)

            Catch ex As Exception

            End Try

            Session.CursorDefault()
            Application.DoEvents()

            CareMessage("There more one or more errors sending emails. Check Clipboard for more information...", MessageBoxIcon.Exclamation, "")

        End If

        SetCMDs(True)
        Session.CursorDefault()
        Application.DoEvents()

    End Sub

    Private Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click

        If txtSubject.Text = "" Then Exit Sub
        If txtBody.Text = "" Then Exit Sub

        SetCMDs(False)

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        If m_EmailOnly Then
            'user is forcing re-sending the email
            Run(Reports.InvoiceRun.EnumOutputMode.ForceEmail)
            Me.Close()
        Else
            Run(Reports.InvoiceRun.EnumOutputMode.EmailOnly)
        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

        SetCMDs(True)

        m_PrintedOrEmailed = True

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Run(Reports.InvoiceRun.EnumOutputMode.PrintedOnly)
        m_PrintedOrEmailed = True
    End Sub

    Private Sub btnPrintAll_Click(sender As Object, e As EventArgs) Handles btnPrintAll.Click
        Run(Reports.InvoiceRun.EnumOutputMode.ForcePrint)
    End Sub

End Class
