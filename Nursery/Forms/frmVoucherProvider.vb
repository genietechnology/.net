﻿
Imports Care.Global

Public Class frmVoucherProvider

    Private m_VP As Business.VoucherProvider

#Region "Overrides"

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.VoucherProvider.Find(Me)
        If _ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_ReturnValue)
            MyBase.RecordPopulated = True

            m_VP = Business.VoucherProvider.RetreiveByID(New Guid(_ReturnValue))

            bs.DataSource = m_VP

        End If

    End Sub

    Protected Overrides Sub CommitUpdate()
        Dim _VP As Business.VoucherProvider = CType(bs.Item(bs.Position), Business.VoucherProvider)
        Business.VoucherProvider.SaveRecord(_VP)
    End Sub

    Protected Overrides Sub SetBindingsByID(ID As System.Guid)
        MyBase.RecordPopulated = True
        m_VP = Business.VoucherProvider.RetreiveByID(ID)
        bs.DataSource = m_VP
    End Sub

    Protected Overrides Sub SetBindings()

        m_VP = New Business.VoucherProvider
        bs.DataSource = m_VP

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtReference.DataBindings.Add("Text", bs, "_Reference")
        txtAddress.DataBindings.Add("Text", bs, "_Address")
        txtTel.DataBindings.Add("Text", bs, "_Tel")
        txtContact.DataBindings.Add("Text", bs, "_Contact")
        txtContactTel.DataBindings.Add("Text", bs, "_ContactTel")
        txtContactEmail.DataBindings.Add("Text", bs, "_ContactEmail")

    End Sub

    Protected Overrides Sub AfterAdd()

    End Sub

    Protected Overrides Sub AfterEdit()

    End Sub

#End Region

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click

    End Sub
End Class