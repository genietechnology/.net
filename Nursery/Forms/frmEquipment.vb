﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmEquipment

    Private m_Equipment As Business.Equipment
    Private m_FormTitle As String = ""

    Private Sub frmEquipment_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_FormTitle = Me.Text
        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Equipment Category"))
        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)

        cbxStatus.AddItem("Active")
        cbxStatus.AddItem("Removed")
        cbxStatus.AddItem("Archived")

        cbxCondition.AddItem("A")
        cbxCondition.AddItem("B")
        cbxCondition.AddItem("C")
        cbxCondition.AddItem("D")
        cbxCondition.AddItem("E")

        AddItems(cbxInspFreq)
        AddItems(cbxTestFreq)
        AddItems(cbxServFreq)
        AddItems(cbxTestFreq)

        PictureBox.Enabled = False

    End Sub

    Private Sub AddItems(ByVal Combo As Care.Controls.CareComboBox)
        Combo.AddItem("Every week")
        Combo.AddItem("Every fortnight")
        Combo.AddItem("Every month")
        Combo.AddItem("Every 6 weeks")
        Combo.AddItem("Every quarter")
        Combo.AddItem("Every 6 months")
        Combo.AddItem("Every year")
    End Sub

    Private Sub DisplayRecord(ByVal ID As Guid)

        MyBase.RecordID = ID
        MyBase.RecordPopulated = True

        m_Equipment = Business.Equipment.RetreiveByID(ID)
        bs.DataSource = m_Equipment

        PictureBox.Enabled = False
        If m_Equipment._Photo.HasValue Then
            PictureBox.DocumentID = m_Equipment._Photo
            PictureBox.Image = DocumentHandler.GetImageByID(m_Equipment._Photo)
        Else
            PictureBox.DocumentID = Nothing
            PictureBox.Clear()
        End If

        Me.Text = m_FormTitle + " - " + m_Equipment._Name

    End Sub

    Protected Overrides Sub CommitDelete()

        Dim sql As String = ""

        sql = "delete from Equipment where id = '" + m_Equipment._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from EquipmentCyc where equip_id = '" + m_Equipment._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from EquipmentLog where equip_id = '" + m_Equipment._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from EquipmentRisks where equip_id = '" + m_Equipment._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)


    End Sub

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""

        _SQL += "select name as 'Name', status as 'Status', category as 'Category', site_name as 'Site', location as 'Location'"
        _SQL += " from Equipment"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Equipment"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        PictureBox.Enabled = False
    End Sub

    Protected Overrides Sub AfterCommitUpdate()
        PictureBox.Enabled = False
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        PictureBox.Enabled = False
    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Equipment = CType(bs.Item(bs.Position), Business.Equipment)

        Select Case ctMain.SelectedTabPage.Name

            Case "tabGeneral"
                DocumentHandler.UpdatePhoto(m_Equipment._Photo, m_Equipment._ID, PictureBox, m_Equipment._Name)

        End Select

        m_Equipment.Store()

    End Sub

    Protected Overrides Sub SetBindings()

        m_Equipment = New Business.Equipment
        bs.DataSource = m_Equipment

        'general tab
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        txtName.DataBindings.Add("Text", bs, "_Name")
        cbxCategory.DataBindings.Add("Text", bs, "_Category")
        cbxStatus.DataBindings.Add("Text", bs, "_Status")

        txtMake.DataBindings.Add("Text", bs, "_Make")
        txtModel.DataBindings.Add("Text", bs, "_Model")

        cbxSite.DataBindings.Add("Text", bs, "_SiteName")
        txtLocation.DataBindings.Add("Text", bs, "_Location")

        chkUsed.DataBindings.Add("Checked", bs, "_Used")
        cbxCondition.DataBindings.Add("Text", bs, "_Condition")
        cdtPurchaseDate.DataBindings.Add("Value", bs, "_PurchaseDate", True)
        txtPurchasePrice.DataBindings.Add("EditValue", bs, "_PurchasePrice")

        chkAccessStaff.DataBindings.Add("Checked", bs, "_AccessStaff")
        chkAccessChild.DataBindings.Add("Checked", bs, "_AccessChild")

        chkLease.DataBindings.Add("Checked", bs, "_Lease")
        txtLeaseMonths.DataBindings.Add("EditValue", bs, "_LeaseMonths")
        cdtLeaseFrom.DataBindings.Add("Value", bs, "_LeaseFrom", True)
        cdtLeaseComplete.DataBindings.Add("Value", bs, "_LeaseComplete", True)

        chkWarranty.DataBindings.Add("Checked", bs, "_Warranty")
        txtWarrantyMonths.DataBindings.Add("EditValue", bs, "_WarrantyMonths")
        cdtWarrantyFrom.DataBindings.Add("Value", bs, "_WarrantyFrom", True)
        cdtWarrantyExpires.DataBindings.Add("Value", bs, "_WarrantyExpires", True)

        'inspections
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        chkInsp.DataBindings.Add("Checked", bs, "_Insp")
        cbxInspFreq.DataBindings.Add("Text", bs, "_InspFreq")
        cdtInspNext.DataBindings.Add("Value", bs, "_InspNext", True)
        cdtInspLast.DataBindings.Add("Value", bs, "_InspLast", True)

        'servicing
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        chkServ.DataBindings.Add("Checked", bs, "_Serv")
        cbxServFreq.DataBindings.Add("Text", bs, "_ServFreq")
        cdtServNext.DataBindings.Add("Value", bs, "_ServNext", True)
        cdtServLast.DataBindings.Add("Value", bs, "_ServLast", True)

        'testing
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        chkTest.DataBindings.Add("Checked", bs, "_Test")
        cbxTestFreq.DataBindings.Add("Text", bs, "_TestFreq")
        cdtTestNext.DataBindings.Add("Value", bs, "_TestNext", True)
        cdtTestLast.DataBindings.Add("Value", bs, "_TestLast", True)

    End Sub

    Protected Overrides Sub AfterAdd()

        MyBase.RecordID = Nothing
        MyBase.RecordPopulated = False

        m_Equipment = New Business.Equipment
        bs.DataSource = m_Equipment

        PictureBox.Enabled = True
        PictureBox.Clear()

        Me.Text = m_FormTitle + " - New Record"

        txtName.Focus()

    End Sub

    Protected Overrides Sub AfterEdit()

        Select Case ctMain.SelectedTabPage.Name

            Case "tabGeneral"
                PictureBox.Enabled = True
                txtName.Focus()

            Case "tabRisk"
                cgRisk.ButtonsEnabled = True

        End Select

    End Sub

    Private Sub ctMain_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles ctMain.SelectedPageChanged

        Select Case ctMain.SelectedTabPage.Name

            Case "tabGeneral"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabCalendar"
                Me.ToolbarMode = ToolbarEnum.None
                YearView1.Populate(m_Equipment)

            Case "tabRisk"
                Me.ToolbarMode = ToolbarEnum.EditOnly
                DisplayAssociatedEquipment

            Case Else
                Me.ToolbarMode = ToolbarEnum.EditOnly

        End Select

    End Sub

    Private Sub cgRisk_EditClick(sender As Object, e As EventArgs) Handles cgRisk.EditClick
        'drill to risk
    End Sub

    Private Sub cgRisk_AddClick(sender As Object, e As EventArgs) Handles cgRisk.AddClick
        Dim _RiskID As String = Business.Risk.FindRecord
        If _RiskID <> "" Then
            Dim _ER As New Business.EquipmentRisk
            _ER._EquipId = m_Equipment._ID.Value
            _ER._RiskId = New Guid(_RiskID)
            _ER.Store()
            DisplayAssociatedEquipment()
        End If
    End Sub

    Private Sub cgRisk_RemoveClick(sender As Object, e As EventArgs) Handles cgRisk.RemoveClick

    End Sub

    Private Sub DisplayAssociatedEquipment()

        Dim _SQL As String = ""
        _SQL += "select r.ID, r.site_name as 'Site', r.area as 'Area', r.title as 'Title'"
        _SQL += " from EquipmentRisks er"
        _SQL += " left join Risks r on r.ID = er.risk_id"
        _SQL += " where er.equip_id = '" + m_Equipment._ID.ToString + "'"
        _SQL += " order by r.site_name"

        cgRisk.HideFirstColumn = True
        cgRisk.Populate(Session.ConnectionString, _SQL)

    End Sub

End Class