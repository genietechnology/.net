﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmRecruit

    Private m_RecordID As Guid?
    Private m_JobRecord As Business.RecruitJob
    Private m_IsNew As Boolean = True

    Public ReadOnly Property RecordID As Guid?
        Get
            Return m_RecordID
        End Get
    End Property

    Public Sub New(ByVal JobRecord As Business.RecruitJob, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_JobRecord = JobRecord

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Guid.NewGuid
        End If

    End Sub

    Public Sub New(ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_JobRecord = Nothing

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Guid.NewGuid
        End If

        btnEmployee.Enabled = False

    End Sub

    Private Sub frmRecruit_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Session.CursorWaiting()

        lblAge.Text = ""

        With cbxQualLevel
            .AddItem("Unqualified")
            .AddItem("Level 1")
            .AddItem("Level 2")
            .AddItem("Level 3")
            .AddItem("Level 4")
            .AddItem("Level 5")
            .AddItem("Level 6")
            .AddItem("Level 7")
        End With

        cbxStage.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Recruitment Stages"))

        Dim _SQL As String = "select fullname from Staff where status = 'C' and line_manager = 1 order by fullname"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        PopulateList(cbxIDWho, _DT)
        PopulateList(cbxDBSWho, _DT)
        PopulateList(cbxBarrWho, _DT)
        PopulateList(cbxQualWho, _DT)
        PopulateList(cbxDisWho, _DT)
        PopulateList(cbxRUKWho, _DT)
        PopulateList(cbxOverseasWho, _DT)
        PopulateList(cbxRef1Who, _DT)
        PopulateList(cbxRef2Who, _DT)

        cbxDBSCheckType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("DBS Check Type"))

        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)

        If m_IsNew Then
            Me.Text = "Create New Applicant"
            cbxStage.Text = "Applied"
        Else
            MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
            DisplayRecord()
        End If

        Session.CursorDefault()

    End Sub

    Private Sub PopulateList(ByRef Combo As Care.Controls.CareComboBox, ByVal DT As DataTable)
        If DT IsNot Nothing Then
            For Each _DR As DataRow In DT.Rows
                Combo.AddItem(_DR.Item(0).ToString)
            Next
        End If
    End Sub

    Private Sub DisplayRecord()

        Dim _A As Business.RecruitApp = Business.RecruitApp.RetreiveByID(m_RecordID.Value)
        With _A

            Me.Text = "Amend Applicant - " + ._Fullname

            txtFullname.Text = ._Fullname
            txtForename.Text = ._Forename
            txtSurname.Text = ._Surname
            cdtDOB.Value = ._Dob

            txtEmployer.Text = ._CurrentEmployer
            txtJobTitle.Text = ._CurrentPosition
            txtSalary.EditValue = ._CurrentSalary
            txtHours.EditValue = ._CurrentHours

            cbxQualLevel.SelectedIndex = ValueHandler.ConvertInteger(._QualLevel)

            cbxStage.Text = ._Stage

            txtAddress.Text = ._Address
            txtTelHome.Text = ._TelHome
            txtTelMobile.Text = ._TelMobile
            txtEmail.Text = ._Email

            cdtIDWhen.Value = ._ChkIdWhen
            cbxIDWho.Text = ._ChkIdWho
            txtIDWhat.Text = ._ChkIdWhat

            cdtDBSWhen.Value = ._ChkDbsWhen
            cbxDBSWho.Text = ._ChkDbsWho
            cdtDBSIssued.Value = ._ChkDbsIssued
            txtDBSWhat.Text = ._ChkDbsRef
            cbxDBSCheckType.Text = ._ChkDbsType

            cdtBarrWhen.Value = ._ChkBarrWhen
            cbxBarrWho.Text = ._ChkBarrWho

            chkQual.Checked = ._ChkQual
            cdtQualWhen.Value = ._ChkQualWhen
            cbxQualWho.Text = ._ChkQualWho
            txtQualWhat.Text = ._ChkQualWhat

            cdtDisWhen.Value = ._ChkDisWhen
            cbxDisWho.Text = ._ChkDisWho

            cdtRUKWhen.Value = ._ChkRukWhen
            cbxRUKWho.Text = ._ChkRukWho
            txtRUKWhat.Text = ._ChkRukWhat

            chkOverseas.Checked = ._ChkOvs
            cdtOverseasWhen.Value = ._ChkOvsWhen
            cbxOverseasWho.Text = ._ChkOvsWho

            cdtRef1When.Value = ._ChkRef1When
            cbxRef1Who.Text = ._ChkRef1Who
            txtRef1What.Text = ._ChkRef1What

            cdtRef2When.Value = ._ChkRef2When
            cbxRef2Who.Text = ._ChkRef2Who
            txtRef2What.Text = ._ChkRef2What

        End With

        _A = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean
        Return MyControls.Validate(Me.Controls)
    End Function

    Private Function SaveRecord() As Boolean

        If Not ValidateEntry() Then Return False

        Dim _A As Business.RecruitApp = Nothing

        If m_IsNew Then
            _A = New Business.RecruitApp
            _A._ID = m_RecordID
            If m_JobRecord IsNot Nothing Then
                _A._JobId = m_JobRecord._ID
            End If
        Else
            _A = Business.RecruitApp.RetreiveByID(m_RecordID.Value)
        End If

        With _A

            ._Forename = txtForename.Text
            ._Surname = txtSurname.Text
            ._Fullname = ._Forename + " " + ._Surname
            ._Dob = cdtDOB.Value

            ._Stage = cbxStage.Text

            ._CurrentEmployer = txtEmployer.Text
            ._CurrentPosition = txtJobTitle.Text
            ._CurrentSalary = ValueHandler.ConvertDecimal(txtSalary.EditValue)
            ._CurrentHours = ValueHandler.ConvertDecimal(txtHours.EditValue)

            ._QualLevel = ValueHandler.ConvertByte(cbxQualLevel.SelectedIndex)

            ._Address = txtAddress.Text
            ._TelHome = txtTelHome.Text
            ._TelMobile = txtTelMobile.Text
            ._Email = txtEmail.Text

            ._ChkIdWhen = cdtIDWhen.Value
            ._ChkIdWho = cbxIDWho.Text
            ._ChkIdWhat = txtIDWhat.Text

            ._ChkDbsWhen = cdtDBSWhen.Value
            ._ChkDbsWho = cbxDBSWho.Text
            ._ChkDbsIssued = cdtDBSIssued.Value
            ._ChkDbsRef = txtDBSWhat.Text
            ._ChkDbsType = cbxDBSCheckType.Text

            ._ChkBarrWhen = cdtBarrWhen.Value
            ._ChkBarrWho = cbxBarrWho.Text

            ._ChkQual = chkQual.Checked
            ._ChkQualWhen = cdtQualWhen.Value
            ._ChkQualWho = cbxQualWho.Text
            ._ChkQualWhat = txtQualWhat.Text

            ._ChkDisWhen = cdtDisWhen.Value
            ._ChkDisWho = cbxDisWho.Text

            ._ChkRukWhen = cdtRUKWhen.Value
            ._ChkRukWho = cbxRUKWho.Text
            ._ChkRukWhat = txtRUKWhat.Text

            ._ChkOvs = chkOverseas.Checked
            ._ChkOvsWhen = cdtOverseasWhen.Value
            ._ChkOvsWho = cbxOverseasWho.Text

            ._ChkRef1When = cdtRef1When.Value
            ._ChkRef1Who = cbxRef1Who.Text
            ._ChkRef1What = txtRef1What.Text

            ._ChkRef2When = cdtRef2When.Value
            ._ChkRef2Who = cbxRef2Who.Text
            ._ChkRef2What = txtRef2What.Text

            .Store()

        End With

        _A = Nothing

        Return True

    End Function

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SetCMDs(False)
        If SaveRecord() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
        SetCMDs(True)
    End Sub

    Private Sub cdtDOB_LostFocus(sender As Object, e As EventArgs) Handles cdtDOB.LostFocus
        If cdtDOB.Value.HasValue Then
            lblAge.Text = ValueHandler.DateDifferenceAsText(cdtDOB.Value.Value, Date.Today)
        Else
            lblAge.Text = ""
        End If
    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        NotesHandler.OpenNotes(m_RecordID.Value, "Applicant Notes")
    End Sub

    Private Sub btnDocuments_Click(sender As Object, e As EventArgs) Handles btnDocuments.Click
        DocumentHandler.OpenDocumentManagement(m_RecordID.Value, "Applicant Documents")
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnNotes.Enabled = Enabled
        btnDocuments.Enabled = Enabled
        btnEmployee.Enabled = Enabled
        btnOK.Enabled = Enabled
        btnCancel.Enabled = Enabled
        Application.DoEvents()
    End Sub

    Private Sub btnEmployee_Click(sender As Object, e As EventArgs) Handles btnEmployee.Click

        If CareMessage("Are you sure you want to convert this Applicant to an Employee?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Convert Applicant") = DialogResult.Yes Then

            If InputBox("Please enter CONVERT to continue.").ToUpper = "CONVERT" Then

                SetCMDs(False)

                If SaveRecord() Then

                    Dim _A As Business.RecruitApp = Business.RecruitApp.RetreiveByID(m_RecordID.Value)
                    Dim _S As New Business.Staff

                    With _S

                        ._Status = "C"
                        ._Fullname = _A._Fullname
                        ._Forename = _A._Forename
                        ._Surname = _A._Surname

                        ._DateStarted = m_JobRecord._DateStart
                        ._Dob = _A._Dob

                        ._CrbLast = _A._ChkDbsIssued
                        ._CrbRef = _A._ChkDbsRef

                        If _A._ChkDbsIssued.HasValue Then
                            ._CrbDue = _A._ChkDbsIssued.Value.AddYears(3)
                        End If

                        ._DbsIssued = _A._ChkDbsIssued
                        ._DbsCheckType = _A._ChkDbsType
                        ._DbsPosition = m_JobRecord._JobTitle

                        ._JobTitle = m_JobRecord._JobTitle

                        ._Address = _A._Address
                        ._Postcode = _A._Postcode
                        ._TelHome = _A._TelHome
                        ._TelMobile = _A._TelMobile
                        ._Email = _A._Email

                        ._QualLevel = _A._QualLevel

                        ._SiteId = m_JobRecord._SiteId
                        ._SiteName = m_JobRecord._SiteName

                        ._ReportsTo = m_JobRecord._LineMan
                        ._Department = m_JobRecord._Department

                        ._ApplicantId = _A._ID

                        .Store()

                    End With

                    CareMessage("Employee Record Created Successfully - This program will now exit.", MessageBoxIcon.Information, "Staff Record Created")

                    Me.DialogResult = DialogResult.OK
                    Me.Close()

                End If

                SetCMDs(True)

            End If

        End If

    End Sub

End Class
