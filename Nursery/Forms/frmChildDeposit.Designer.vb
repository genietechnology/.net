﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmChildDeposit
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxWeekly = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.chkDepPost = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtDepRef = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtDepValue = New Care.Controls.CareTextBox(Me.components)
        Me.txtDepNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.cdtDepDate = New Care.Controls.CareDateTime(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.radReturn = New Care.Controls.CareRadioButton(Me.components)
        Me.radTake = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.chkDepPost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDepDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDepDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.radReturn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radTake.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.chkDepPost)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.txtDepRef)
        Me.gbxWeekly.Controls.Add(Me.CareLabel14)
        Me.gbxWeekly.Controls.Add(Me.CareLabel13)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.txtDepValue)
        Me.gbxWeekly.Controls.Add(Me.txtDepNotes)
        Me.gbxWeekly.Controls.Add(Me.cdtDepDate)
        Me.gbxWeekly.Location = New System.Drawing.Point(11, 57)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(370, 192)
        Me.gbxWeekly.TabIndex = 1
        Me.gbxWeekly.Text = "Deposit Details"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(231, 60)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(109, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Available for Posting"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel2.Visible = False
        '
        'chkDepPost
        '
        Me.chkDepPost.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkDepPost.EnterMoveNextControl = True
        Me.chkDepPost.Location = New System.Drawing.Point(346, 58)
        Me.chkDepPost.Name = "chkDepPost"
        Me.chkDepPost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepPost.Properties.Appearance.Options.UseFont = True
        Me.chkDepPost.Size = New System.Drawing.Size(20, 19)
        Me.chkDepPost.TabIndex = 5
        Me.chkDepPost.Tag = "AE"
        Me.chkDepPost.Visible = False
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 88)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Reference"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepRef
        '
        Me.txtDepRef.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtDepRef.CharacterCasing = CharacterCasing.Normal
        Me.txtDepRef.EnterMoveNextControl = True
        Me.txtDepRef.Location = New System.Drawing.Point(85, 85)
        Me.txtDepRef.MaxLength = 100
        Me.txtDepRef.Name = "txtDepRef"
        Me.txtDepRef.NumericAllowNegatives = False
        Me.txtDepRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDepRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDepRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDepRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDepRef.Properties.Appearance.Options.UseFont = True
        Me.txtDepRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDepRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDepRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDepRef.Properties.MaxLength = 100
        Me.txtDepRef.Size = New System.Drawing.Size(275, 20)
        Me.txtDepRef.TabIndex = 7
        Me.txtDepRef.Tag = "AE"
        Me.txtDepRef.TextAlign = HorizontalAlignment.Left
        Me.txtDepRef.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel14.TabIndex = 2
        Me.CareLabel14.Text = "Value"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 117)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel13.TabIndex = 8
        Me.CareLabel13.Text = "Notes"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Action Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepValue
        '
        Me.txtDepValue.CharacterCasing = CharacterCasing.Normal
        Me.txtDepValue.EnterMoveNextControl = True
        Me.txtDepValue.Location = New System.Drawing.Point(85, 57)
        Me.txtDepValue.MaxLength = 14
        Me.txtDepValue.Name = "txtDepValue"
        Me.txtDepValue.NumericAllowNegatives = True
        Me.txtDepValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtDepValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDepValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDepValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDepValue.Properties.Appearance.Options.UseFont = True
        Me.txtDepValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDepValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDepValue.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtDepValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDepValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDepValue.Properties.MaxLength = 14
        Me.txtDepValue.Size = New System.Drawing.Size(100, 22)
        Me.txtDepValue.TabIndex = 3
        Me.txtDepValue.Tag = "AEM"
        Me.txtDepValue.TextAlign = HorizontalAlignment.Left
        Me.txtDepValue.ToolTipText = ""
        '
        'txtDepNotes
        '
        Me.txtDepNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtDepNotes.Location = New System.Drawing.Point(85, 113)
        Me.txtDepNotes.Name = "txtDepNotes"
        Me.txtDepNotes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepNotes.Properties.Appearance.Options.UseFont = True
        Me.txtDepNotes.Properties.MaxLength = 100
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtDepNotes, True)
        Me.txtDepNotes.Size = New System.Drawing.Size(275, 71)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtDepNotes, OptionsSpelling1)
        Me.txtDepNotes.TabIndex = 9
        Me.txtDepNotes.Tag = "AE"
        '
        'cdtDepDate
        '
        Me.cdtDepDate.EditValue = Nothing
        Me.cdtDepDate.EnterMoveNextControl = True
        Me.cdtDepDate.Location = New System.Drawing.Point(85, 29)
        Me.cdtDepDate.Name = "cdtDepDate"
        Me.cdtDepDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDepDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDepDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDepDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDepDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtDepDate.TabIndex = 1
        Me.cdtDepDate.Tag = "AEM"
        Me.cdtDepDate.Value = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(205, 259)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(296, 259)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'CareFrame1
        '
        Me.CareFrame1.Controls.Add(Me.radReturn)
        Me.CareFrame1.Controls.Add(Me.radTake)
        Me.CareFrame1.Location = New System.Drawing.Point(11, 12)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.ShowCaption = False
        Me.CareFrame1.Size = New System.Drawing.Size(370, 39)
        Me.CareFrame1.TabIndex = 0
        '
        'radReturn
        '
        Me.radReturn.Location = New System.Drawing.Point(100, 10)
        Me.radReturn.Name = "radReturn"
        Me.radReturn.Properties.AutoWidth = True
        Me.radReturn.Properties.Caption = "Return Deposit"
        Me.radReturn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radReturn.Properties.RadioGroupIndex = 1
        Me.radReturn.Size = New System.Drawing.Size(94, 19)
        Me.radReturn.TabIndex = 1
        Me.radReturn.TabStop = False
        '
        'radTake
        '
        Me.radTake.EditValue = True
        Me.radTake.Location = New System.Drawing.Point(10, 10)
        Me.radTake.Name = "radTake"
        Me.radTake.Properties.AutoWidth = True
        Me.radTake.Properties.Caption = "Take Deposit"
        Me.radTake.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTake.Properties.RadioGroupIndex = 1
        Me.radTake.Size = New System.Drawing.Size(84, 19)
        Me.radTake.TabIndex = 0
        '
        'frmChildDeposit
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(394, 293)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChildDeposit"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Deposit Transaction"
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.chkDepPost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDepDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDepDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        CType(Me.radReturn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radTake.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtDepRef As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtDepValue As Care.Controls.CareTextBox
    Friend WithEvents txtDepNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cdtDepDate As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents gbxWeekly As Care.Controls.CareFrame
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents radReturn As Care.Controls.CareRadioButton
    Friend WithEvents radTake As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkDepPost As Care.Controls.CareCheckBox
End Class
