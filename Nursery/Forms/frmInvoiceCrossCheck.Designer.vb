﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceCrossCheck
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxMode = New Care.Controls.CareFrame()
        Me.radRegister = New Care.Controls.CareRadioButton()
        Me.radComparison = New Care.Controls.CareRadioButton()
        Me.radBasic = New Care.Controls.CareRadioButton()
        Me.gbxComparison = New Care.Controls.CareFrame()
        Me.btnPrintEmail = New Care.Controls.CareButton()
        Me.txtBatchNo = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.radDecrease = New Care.Controls.CareRadioButton()
        Me.radIncrease = New Care.Controls.CareRadioButton()
        Me.cgData = New Care.Controls.CareGrid()
        CType(Me.gbxMode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMode.SuspendLayout()
        CType(Me.radRegister.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radComparison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxComparison, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxComparison.SuspendLayout()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDecrease.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radIncrease.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'gbxMode
        '
        Me.gbxMode.Controls.Add(Me.radRegister)
        Me.gbxMode.Controls.Add(Me.radComparison)
        Me.gbxMode.Controls.Add(Me.radBasic)
        Me.gbxMode.Location = New System.Drawing.Point(12, 12)
        Me.gbxMode.Name = "gbxMode"
        Me.gbxMode.Size = New System.Drawing.Size(374, 60)
        Me.gbxMode.TabIndex = 2
        Me.gbxMode.Text = "Comparison Mode"
        '
        'radRegister
        '
        Me.radRegister.CausesValidation = False
        Me.radRegister.Location = New System.Drawing.Point(123, 30)
        Me.radRegister.Name = "radRegister"
        Me.radRegister.Properties.Appearance.Options.UseTextOptions = True
        Me.radRegister.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radRegister.Properties.AutoWidth = True
        Me.radRegister.Properties.Caption = "Register Cross Check"
        Me.radRegister.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radRegister.Properties.RadioGroupIndex = 0
        Me.radRegister.Size = New System.Drawing.Size(124, 19)
        Me.radRegister.TabIndex = 11
        Me.radRegister.TabStop = False
        '
        'radComparison
        '
        Me.radComparison.CausesValidation = False
        Me.radComparison.Location = New System.Drawing.Point(253, 30)
        Me.radComparison.Name = "radComparison"
        Me.radComparison.Properties.Appearance.Options.UseTextOptions = True
        Me.radComparison.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radComparison.Properties.AutoWidth = True
        Me.radComparison.Properties.Caption = "Batch Comparison"
        Me.radComparison.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radComparison.Properties.RadioGroupIndex = 0
        Me.radComparison.Size = New System.Drawing.Size(108, 19)
        Me.radComparison.TabIndex = 5
        Me.radComparison.TabStop = False
        '
        'radBasic
        '
        Me.radBasic.CausesValidation = False
        Me.radBasic.EditValue = True
        Me.radBasic.Location = New System.Drawing.Point(9, 30)
        Me.radBasic.Name = "radBasic"
        Me.radBasic.Properties.Appearance.Options.UseTextOptions = True
        Me.radBasic.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radBasic.Properties.AutoWidth = True
        Me.radBasic.Properties.Caption = "Basic Cross Check"
        Me.radBasic.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radBasic.Properties.RadioGroupIndex = 0
        Me.radBasic.Size = New System.Drawing.Size(108, 19)
        Me.radBasic.TabIndex = 4
        '
        'gbxComparison
        '
        Me.gbxComparison.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxComparison.Controls.Add(Me.btnPrintEmail)
        Me.gbxComparison.Controls.Add(Me.txtBatchNo)
        Me.gbxComparison.Controls.Add(Me.CareLabel1)
        Me.gbxComparison.Controls.Add(Me.radDecrease)
        Me.gbxComparison.Controls.Add(Me.radIncrease)
        Me.gbxComparison.Location = New System.Drawing.Point(392, 12)
        Me.gbxComparison.Name = "gbxComparison"
        Me.gbxComparison.Size = New System.Drawing.Size(582, 60)
        Me.gbxComparison.TabIndex = 12
        Me.gbxComparison.Text = "Comparison Batch"
        '
        'btnPrintEmail
        '
        Me.btnPrintEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintEmail.Appearance.Options.UseFont = True
        Me.btnPrintEmail.CausesValidation = False
        Me.btnPrintEmail.Location = New System.Drawing.Point(202, 27)
        Me.btnPrintEmail.Name = "btnPrintEmail"
        Me.btnPrintEmail.Size = New System.Drawing.Size(67, 25)
        Me.btnPrintEmail.TabIndex = 13
        Me.btnPrintEmail.Tag = ""
        Me.btnPrintEmail.Text = "Select"
        '
        'txtBatchNo
        '
        Me.txtBatchNo.CharacterCasing = CharacterCasing.Normal
        Me.txtBatchNo.EnterMoveNextControl = True
        Me.txtBatchNo.Location = New System.Drawing.Point(96, 29)
        Me.txtBatchNo.MaxLength = 0
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.NumericAllowNegatives = False
        Me.txtBatchNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBatchNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBatchNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBatchNo.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtBatchNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBatchNo.Properties.Appearance.Options.UseBackColor = True
        Me.txtBatchNo.Properties.Appearance.Options.UseFont = True
        Me.txtBatchNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBatchNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBatchNo.Properties.ReadOnly = True
        Me.txtBatchNo.ReadOnly = True
        Me.txtBatchNo.Size = New System.Drawing.Size(100, 20)
        Me.txtBatchNo.TabIndex = 8
        Me.txtBatchNo.TabStop = False
        Me.txtBatchNo.TextAlign = HorizontalAlignment.Left
        Me.txtBatchNo.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(13, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel1.TabIndex = 7
        Me.CareLabel1.Text = "Batch Number"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDecrease
        '
        Me.radDecrease.CausesValidation = False
        Me.radDecrease.Location = New System.Drawing.Point(421, 30)
        Me.radDecrease.Name = "radDecrease"
        Me.radDecrease.Properties.Appearance.Options.UseTextOptions = True
        Me.radDecrease.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radDecrease.Properties.AutoWidth = True
        Me.radDecrease.Properties.Caption = "Invoice Value Increased"
        Me.radDecrease.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDecrease.Properties.RadioGroupIndex = 0
        Me.radDecrease.Size = New System.Drawing.Size(137, 19)
        Me.radDecrease.TabIndex = 6
        Me.radDecrease.TabStop = False
        '
        'radIncrease
        '
        Me.radIncrease.CausesValidation = False
        Me.radIncrease.EditValue = True
        Me.radIncrease.Location = New System.Drawing.Point(275, 30)
        Me.radIncrease.Name = "radIncrease"
        Me.radIncrease.Properties.Appearance.Options.UseTextOptions = True
        Me.radIncrease.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radIncrease.Properties.AutoWidth = True
        Me.radIncrease.Properties.Caption = "Invoice Value Decreased"
        Me.radIncrease.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radIncrease.Properties.RadioGroupIndex = 0
        Me.radIncrease.Size = New System.Drawing.Size(140, 19)
        Me.radIncrease.TabIndex = 5
        '
        'cgData
        '
        Me.cgData.AllowBuildColumns = True
        Me.cgData.AllowHorizontalScroll = False
        Me.cgData.AllowMultiSelect = False
        Me.cgData.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgData.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgData.Appearance.Options.UseFont = True
        Me.cgData.AutoSizeByData = True
        Me.cgData.FocusedRowHandle = -2147483648
        Me.cgData.HideFirstColumn = False
        Me.cgData.Location = New System.Drawing.Point(12, 78)
        Me.cgData.Name = "cgData"
        Me.cgData.QueryID = Nothing
        Me.cgData.RowAutoHeight = False
        Me.cgData.SearchAsYouType = True
        Me.cgData.ShowAutoFilterRow = False
        Me.cgData.ShowFindPanel = False
        Me.cgData.ShowGroupByBox = True
        Me.cgData.ShowNavigator = False
        Me.cgData.Size = New System.Drawing.Size(962, 413)
        Me.cgData.TabIndex = 13
        '
        'frmInvoiceCrossCheck
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(984, 503)
        Me.Controls.Add(Me.cgData)
        Me.Controls.Add(Me.gbxComparison)
        Me.Controls.Add(Me.gbxMode)
        Me.LoadMaximised = True
        Me.Name = "frmInvoiceCrossCheck"
        CType(Me.gbxMode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMode.ResumeLayout(False)
        CType(Me.radRegister.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radComparison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxComparison, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxComparison.ResumeLayout(False)
        Me.gbxComparison.PerformLayout()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDecrease.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radIncrease.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxMode As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radRegister As Care.Controls.CareRadioButton
    Friend WithEvents radComparison As Care.Controls.CareRadioButton
    Friend WithEvents radBasic As Care.Controls.CareRadioButton
    Friend WithEvents gbxComparison As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radDecrease As Care.Controls.CareRadioButton
    Friend WithEvents radIncrease As Care.Controls.CareRadioButton
    Friend WithEvents txtBatchNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnPrintEmail As Care.Controls.CareButton
    Friend WithEvents cgData As Care.Controls.CareGrid

End Class
