﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEquipment
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctMain = New Care.Controls.CareTab(Me.components)
        Me.tabGeneral = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.txtLocation = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.chkAccessChild = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.chkAccessStaff = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cdtWarrantyExpires = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txtWarrantyMonths = New Care.Controls.CareTextBox(Me.components)
        Me.cdtWarrantyFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.chkWarranty = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupBox3 = New Care.Controls.CareFrame()
        Me.PictureBox = New Care.Controls.PhotoControl()
        Me.GroupControl17 = New Care.Controls.CareFrame()
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.cdtLeaseComplete = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtLeaseMonths = New Care.Controls.CareTextBox(Me.components)
        Me.cdtLeaseFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.chkLease = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.cbxCondition = New Care.Controls.CareComboBox(Me.components)
        Me.chkUsed = New Care.Controls.CareCheckBox(Me.components)
        Me.txtPurchasePrice = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtPurchaseDate = New Care.Controls.CareDateTime(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtMake = New Care.Controls.CareTextBox(Me.components)
        Me.txtModel = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.lblClass = New Care.Controls.CareLabel(Me.components)
        Me.cbxStatus = New Care.Controls.CareComboBox(Me.components)
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxCategory = New Care.Controls.CareComboBox(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.tabCalendar = New DevExpress.XtraTab.XtraTabPage()
        Me.YearView1 = New Nursery.YearView()
        Me.tabInspection = New DevExpress.XtraTab.XtraTabPage()
        Me.cgInsp = New Care.Controls.CareGridWithButtons()
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.cdtInspLast = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.cbxInspFreq = New Care.Controls.CareComboBox(Me.components)
        Me.chkInsp = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInspNext = New Care.Controls.CareDateTime(Me.components)
        Me.tabService = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl7 = New Care.Controls.CareFrame()
        Me.cdtServLast = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.cbxServFreq = New Care.Controls.CareComboBox(Me.components)
        Me.chkServ = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.cdtServNext = New Care.Controls.CareDateTime(Me.components)
        Me.cgServ = New Care.Controls.CareGridWithButtons()
        Me.tabTest = New DevExpress.XtraTab.XtraTabPage()
        Me.cgActivity = New Care.Controls.CareGridWithButtons()
        Me.GroupControl9 = New Care.Controls.CareFrame()
        Me.cdtTestLast = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.cbxTestFreq = New Care.Controls.CareComboBox(Me.components)
        Me.chkTest = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel33 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel34 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTestNext = New Care.Controls.CareDateTime(Me.components)
        Me.tabRisk = New DevExpress.XtraTab.XtraTabPage()
        Me.cgRisk = New Care.Controls.CareGridWithButtons()
        Me.tabIncidents = New DevExpress.XtraTab.XtraTabPage()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.CareGrid1 = New Care.Controls.CareGrid()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctMain.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.chkAccessChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAccessStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cdtWarrantyExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWarrantyExpires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWarrantyMonths.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWarrantyFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWarrantyFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWarranty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl17.SuspendLayout()
        CType(Me.cdtLeaseComplete.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLeaseComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLeaseMonths.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLeaseFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLeaseFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLease.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cbxCondition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUsed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPurchasePrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPurchaseDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPurchaseDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtMake.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCalendar.SuspendLayout()
        Me.tabInspection.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cdtInspLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInspLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInspFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkInsp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInspNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInspNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabService.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.cdtServLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtServLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxServFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkServ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtServNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtServNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabTest.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.cdtTestLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTestLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTestFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTestNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTestNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRisk.SuspendLayout()
        Me.tabActivity.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(762, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(671, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 546)
        Me.Panel1.Size = New System.Drawing.Size(859, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'ctMain
        '
        Me.ctMain.Location = New System.Drawing.Point(9, 53)
        Me.ctMain.Name = "ctMain"
        Me.ctMain.SelectedTabPage = Me.tabGeneral
        Me.ctMain.Size = New System.Drawing.Size(842, 492)
        Me.ctMain.TabIndex = 0
        Me.ctMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabGeneral, Me.tabCalendar, Me.tabInspection, Me.tabService, Me.tabTest, Me.tabRisk, Me.tabIncidents, Me.tabActivity})
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.Add(Me.GroupControl5)
        Me.tabGeneral.Controls.Add(Me.GroupControl4)
        Me.tabGeneral.Controls.Add(Me.GroupControl2)
        Me.tabGeneral.Controls.Add(Me.GroupBox3)
        Me.tabGeneral.Controls.Add(Me.GroupControl17)
        Me.tabGeneral.Controls.Add(Me.GroupControl3)
        Me.tabGeneral.Controls.Add(Me.GroupControl1)
        Me.tabGeneral.Controls.Add(Me.GroupBox1)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Size = New System.Drawing.Size(836, 464)
        Me.tabGeneral.Text = "General"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.txtLocation)
        Me.GroupControl5.Controls.Add(Me.CareLabel15)
        Me.GroupControl5.Controls.Add(Me.cbxSite)
        Me.GroupControl5.Controls.Add(Me.CareLabel18)
        Me.GroupControl5.Location = New System.Drawing.Point(6, 216)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(479, 85)
        Me.GroupControl5.TabIndex = 2
        Me.GroupControl5.Text = "Site and Location"
        '
        'txtLocation
        '
        Me.txtLocation.CharacterCasing = CharacterCasing.Normal
        Me.txtLocation.EnterMoveNextControl = True
        Me.txtLocation.Location = New System.Drawing.Point(91, 56)
        Me.txtLocation.MaxLength = 30
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.NumericAllowNegatives = False
        Me.txtLocation.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLocation.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLocation.Properties.AccessibleName = "Family"
        Me.txtLocation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLocation.Properties.Appearance.Options.UseFont = True
        Me.txtLocation.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLocation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLocation.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLocation.Properties.MaxLength = 30
        Me.txtLocation.Size = New System.Drawing.Size(378, 22)
        Me.txtLocation.TabIndex = 3
        Me.txtLocation.Tag = "AE"
        Me.txtLocation.TextAlign = HorizontalAlignment.Left
        Me.txtLocation.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel15.TabIndex = 2
        Me.CareLabel15.Text = "Location"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(91, 28)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(378, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel18.TabIndex = 0
        Me.CareLabel18.Text = "Site"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.chkAccessChild)
        Me.GroupControl4.Controls.Add(Me.CareLabel13)
        Me.GroupControl4.Controls.Add(Me.chkAccessStaff)
        Me.GroupControl4.Controls.Add(Me.CareLabel14)
        Me.GroupControl4.Location = New System.Drawing.Point(6, 400)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(479, 57)
        Me.GroupControl4.TabIndex = 4
        Me.GroupControl4.Text = "Usage"
        '
        'chkAccessChild
        '
        Me.chkAccessChild.EnterMoveNextControl = True
        Me.chkAccessChild.Location = New System.Drawing.Point(384, 29)
        Me.chkAccessChild.Name = "chkAccessChild"
        Me.chkAccessChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAccessChild.Properties.Appearance.Options.UseFont = True
        Me.chkAccessChild.Size = New System.Drawing.Size(20, 19)
        Me.chkAccessChild.TabIndex = 3
        Me.chkAccessChild.Tag = "AE"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(288, 31)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel13.TabIndex = 2
        Me.CareLabel13.Text = "Used by Children"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAccessStaff
        '
        Me.chkAccessStaff.EnterMoveNextControl = True
        Me.chkAccessStaff.Location = New System.Drawing.Point(91, 29)
        Me.chkAccessStaff.Name = "chkAccessStaff"
        Me.chkAccessStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAccessStaff.Properties.Appearance.Options.UseFont = True
        Me.chkAccessStaff.Size = New System.Drawing.Size(20, 19)
        Me.chkAccessStaff.TabIndex = 1
        Me.chkAccessStaff.Tag = "AE"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(69, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Used by Staff"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.cdtWarrantyExpires)
        Me.GroupControl2.Controls.Add(Me.CareLabel10)
        Me.GroupControl2.Controls.Add(Me.txtWarrantyMonths)
        Me.GroupControl2.Controls.Add(Me.cdtWarrantyFrom)
        Me.GroupControl2.Controls.Add(Me.CareLabel11)
        Me.GroupControl2.Controls.Add(Me.CareLabel12)
        Me.GroupControl2.Controls.Add(Me.chkWarranty)
        Me.GroupControl2.Location = New System.Drawing.Point(491, 371)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(339, 86)
        Me.GroupControl2.TabIndex = 7
        Me.GroupControl2.Text = "Warranty Information"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(218, 60)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(25, 15)
        Me.CareLabel8.TabIndex = 6
        Me.CareLabel8.Text = "Ends"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtWarrantyExpires
        '
        Me.cdtWarrantyExpires.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtWarrantyExpires.EnterMoveNextControl = True
        Me.cdtWarrantyExpires.Location = New System.Drawing.Point(249, 57)
        Me.cdtWarrantyExpires.Name = "cdtWarrantyExpires"
        Me.cdtWarrantyExpires.Properties.AccessibleName = "Date of Birth"
        Me.cdtWarrantyExpires.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWarrantyExpires.Properties.Appearance.Options.UseFont = True
        Me.cdtWarrantyExpires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWarrantyExpires.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWarrantyExpires.Size = New System.Drawing.Size(85, 22)
        Me.cdtWarrantyExpires.TabIndex = 7
        Me.cdtWarrantyExpires.Tag = "AE"
        Me.cdtWarrantyExpires.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(145, 32)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel10.TabIndex = 2
        Me.CareLabel10.Text = "Duration (months)"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWarrantyMonths
        '
        Me.txtWarrantyMonths.CharacterCasing = CharacterCasing.Normal
        Me.txtWarrantyMonths.EnterMoveNextControl = True
        Me.txtWarrantyMonths.Location = New System.Drawing.Point(249, 29)
        Me.txtWarrantyMonths.MaxLength = 10
        Me.txtWarrantyMonths.Name = "txtWarrantyMonths"
        Me.txtWarrantyMonths.NumericAllowNegatives = True
        Me.txtWarrantyMonths.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtWarrantyMonths.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWarrantyMonths.Properties.AccessibleName = "Family"
        Me.txtWarrantyMonths.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWarrantyMonths.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtWarrantyMonths.Properties.Appearance.Options.UseFont = True
        Me.txtWarrantyMonths.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWarrantyMonths.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtWarrantyMonths.Properties.Mask.EditMask = "##########"
        Me.txtWarrantyMonths.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtWarrantyMonths.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWarrantyMonths.Properties.MaxLength = 10
        Me.txtWarrantyMonths.Size = New System.Drawing.Size(85, 22)
        Me.txtWarrantyMonths.TabIndex = 3
        Me.txtWarrantyMonths.Tag = "AE"
        Me.txtWarrantyMonths.TextAlign = HorizontalAlignment.Left
        Me.txtWarrantyMonths.ToolTipText = ""
        '
        'cdtWarrantyFrom
        '
        Me.cdtWarrantyFrom.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtWarrantyFrom.EnterMoveNextControl = True
        Me.cdtWarrantyFrom.Location = New System.Drawing.Point(91, 57)
        Me.cdtWarrantyFrom.Name = "cdtWarrantyFrom"
        Me.cdtWarrantyFrom.Properties.AccessibleName = "Date of Birth"
        Me.cdtWarrantyFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWarrantyFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtWarrantyFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWarrantyFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWarrantyFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtWarrantyFrom.TabIndex = 5
        Me.cdtWarrantyFrom.Tag = "AE"
        Me.cdtWarrantyFrom.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(8, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Has Warranty"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(8, 60)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel12.TabIndex = 4
        Me.CareLabel12.Text = "Warranty from"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkWarranty
        '
        Me.chkWarranty.EnterMoveNextControl = True
        Me.chkWarranty.Location = New System.Drawing.Point(91, 30)
        Me.chkWarranty.Name = "chkWarranty"
        Me.chkWarranty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWarranty.Properties.Appearance.Options.UseFont = True
        Me.chkWarranty.Size = New System.Drawing.Size(20, 19)
        Me.chkWarranty.TabIndex = 1
        Me.chkWarranty.Tag = "AE"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.PictureBox)
        Me.GroupBox3.Location = New System.Drawing.Point(491, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(339, 267)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.Text = "Photo (Right Click to Amend Photo)"
        '
        'PictureBox
        '
        Me.PictureBox.Dock = DockStyle.Fill
        Me.PictureBox.DocumentID = Nothing
        Me.PictureBox.Image = Nothing
        Me.PictureBox.Location = New System.Drawing.Point(2, 20)
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Padding = New Padding(1, 1, 0, 0)
        Me.PictureBox.Size = New System.Drawing.Size(335, 245)
        Me.PictureBox.TabIndex = 0
        '
        'GroupControl17
        '
        Me.GroupControl17.Controls.Add(Me.CareLabel7)
        Me.GroupControl17.Controls.Add(Me.cdtLeaseComplete)
        Me.GroupControl17.Controls.Add(Me.CareLabel6)
        Me.GroupControl17.Controls.Add(Me.txtLeaseMonths)
        Me.GroupControl17.Controls.Add(Me.cdtLeaseFrom)
        Me.GroupControl17.Controls.Add(Me.CareLabel3)
        Me.GroupControl17.Controls.Add(Me.CareLabel4)
        Me.GroupControl17.Controls.Add(Me.chkLease)
        Me.GroupControl17.Location = New System.Drawing.Point(491, 279)
        Me.GroupControl17.Name = "GroupControl17"
        Me.GroupControl17.Size = New System.Drawing.Size(339, 86)
        Me.GroupControl17.TabIndex = 6
        Me.GroupControl17.Text = "Lease Information"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(218, 60)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(25, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Ends"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtLeaseComplete
        '
        Me.cdtLeaseComplete.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtLeaseComplete.EnterMoveNextControl = True
        Me.cdtLeaseComplete.Location = New System.Drawing.Point(249, 57)
        Me.cdtLeaseComplete.Name = "cdtLeaseComplete"
        Me.cdtLeaseComplete.Properties.AccessibleName = "Date of Birth"
        Me.cdtLeaseComplete.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtLeaseComplete.Properties.Appearance.Options.UseFont = True
        Me.cdtLeaseComplete.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtLeaseComplete.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtLeaseComplete.Size = New System.Drawing.Size(85, 22)
        Me.cdtLeaseComplete.TabIndex = 7
        Me.cdtLeaseComplete.Tag = "AE"
        Me.cdtLeaseComplete.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(145, 32)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Duration (months)"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLeaseMonths
        '
        Me.txtLeaseMonths.CharacterCasing = CharacterCasing.Normal
        Me.txtLeaseMonths.EnterMoveNextControl = True
        Me.txtLeaseMonths.Location = New System.Drawing.Point(249, 29)
        Me.txtLeaseMonths.MaxLength = 10
        Me.txtLeaseMonths.Name = "txtLeaseMonths"
        Me.txtLeaseMonths.NumericAllowNegatives = True
        Me.txtLeaseMonths.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtLeaseMonths.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLeaseMonths.Properties.AccessibleName = "Family"
        Me.txtLeaseMonths.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLeaseMonths.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLeaseMonths.Properties.Appearance.Options.UseFont = True
        Me.txtLeaseMonths.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLeaseMonths.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLeaseMonths.Properties.Mask.EditMask = "##########"
        Me.txtLeaseMonths.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLeaseMonths.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLeaseMonths.Properties.MaxLength = 10
        Me.txtLeaseMonths.Size = New System.Drawing.Size(85, 22)
        Me.txtLeaseMonths.TabIndex = 3
        Me.txtLeaseMonths.Tag = "AE"
        Me.txtLeaseMonths.TextAlign = HorizontalAlignment.Left
        Me.txtLeaseMonths.ToolTipText = ""
        '
        'cdtLeaseFrom
        '
        Me.cdtLeaseFrom.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtLeaseFrom.EnterMoveNextControl = True
        Me.cdtLeaseFrom.Location = New System.Drawing.Point(91, 56)
        Me.cdtLeaseFrom.Name = "cdtLeaseFrom"
        Me.cdtLeaseFrom.Properties.AccessibleName = "Date of Birth"
        Me.cdtLeaseFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtLeaseFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtLeaseFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtLeaseFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtLeaseFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtLeaseFrom.TabIndex = 5
        Me.cdtLeaseFrom.Tag = "AE"
        Me.cdtLeaseFrom.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 32)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Leased"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 60)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Leased from"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLease
        '
        Me.chkLease.EnterMoveNextControl = True
        Me.chkLease.Location = New System.Drawing.Point(91, 30)
        Me.chkLease.Name = "chkLease"
        Me.chkLease.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLease.Properties.Appearance.Options.UseFont = True
        Me.chkLease.Size = New System.Drawing.Size(20, 19)
        Me.chkLease.TabIndex = 1
        Me.chkLease.Tag = "AE"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel17)
        Me.GroupControl3.Controls.Add(Me.CareLabel16)
        Me.GroupControl3.Controls.Add(Me.cbxCondition)
        Me.GroupControl3.Controls.Add(Me.chkUsed)
        Me.GroupControl3.Controls.Add(Me.txtPurchasePrice)
        Me.GroupControl3.Controls.Add(Me.CareLabel1)
        Me.GroupControl3.Controls.Add(Me.CareLabel2)
        Me.GroupControl3.Controls.Add(Me.cdtPurchaseDate)
        Me.GroupControl3.Location = New System.Drawing.Point(6, 307)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(479, 87)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "Purchase and Condition"
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(288, 59)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel17.TabIndex = 6
        Me.CareLabel17.Text = "Purchase Price"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(288, 31)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel16.TabIndex = 2
        Me.CareLabel16.Text = "Condition Rating"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxCondition
        '
        Me.cbxCondition.AllowBlank = False
        Me.cbxCondition.DataSource = Nothing
        Me.cbxCondition.DisplayMember = Nothing
        Me.cbxCondition.EnterMoveNextControl = True
        Me.cbxCondition.Location = New System.Drawing.Point(384, 28)
        Me.cbxCondition.Name = "cbxCondition"
        Me.cbxCondition.Properties.AccessibleName = "Classification"
        Me.cbxCondition.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCondition.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCondition.Properties.Appearance.Options.UseFont = True
        Me.cbxCondition.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCondition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCondition.SelectedValue = Nothing
        Me.cbxCondition.Size = New System.Drawing.Size(85, 22)
        Me.cbxCondition.TabIndex = 3
        Me.cbxCondition.Tag = "AE"
        Me.cbxCondition.ValueMember = Nothing
        '
        'chkUsed
        '
        Me.chkUsed.EnterMoveNextControl = True
        Me.chkUsed.Location = New System.Drawing.Point(91, 29)
        Me.chkUsed.Name = "chkUsed"
        Me.chkUsed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUsed.Properties.Appearance.Options.UseFont = True
        Me.chkUsed.Size = New System.Drawing.Size(20, 19)
        Me.chkUsed.TabIndex = 1
        Me.chkUsed.Tag = "AE"
        '
        'txtPurchasePrice
        '
        Me.txtPurchasePrice.CharacterCasing = CharacterCasing.Normal
        Me.txtPurchasePrice.EnterMoveNextControl = True
        Me.txtPurchasePrice.Location = New System.Drawing.Point(384, 56)
        Me.txtPurchasePrice.MaxLength = 9
        Me.txtPurchasePrice.Name = "txtPurchasePrice"
        Me.txtPurchasePrice.NumericAllowNegatives = False
        Me.txtPurchasePrice.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtPurchasePrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPurchasePrice.Properties.AccessibleName = "Family"
        Me.txtPurchasePrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPurchasePrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPurchasePrice.Properties.Appearance.Options.UseFont = True
        Me.txtPurchasePrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPurchasePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPurchasePrice.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtPurchasePrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPurchasePrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPurchasePrice.Properties.MaxLength = 9
        Me.txtPurchasePrice.Size = New System.Drawing.Size(85, 22)
        Me.txtPurchasePrice.TabIndex = 7
        Me.txtPurchasePrice.Tag = "AE"
        Me.txtPurchasePrice.TextAlign = HorizontalAlignment.Left
        Me.txtPurchasePrice.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Purchase Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Used"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtPurchaseDate
        '
        Me.cdtPurchaseDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtPurchaseDate.EnterMoveNextControl = True
        Me.cdtPurchaseDate.Location = New System.Drawing.Point(91, 56)
        Me.cdtPurchaseDate.Name = "cdtPurchaseDate"
        Me.cdtPurchaseDate.Properties.AccessibleName = "Date of Birth"
        Me.cdtPurchaseDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtPurchaseDate.Properties.Appearance.Options.UseFont = True
        Me.cdtPurchaseDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPurchaseDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtPurchaseDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtPurchaseDate.TabIndex = 5
        Me.cdtPurchaseDate.Tag = "AE"
        Me.cdtPurchaseDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtMake)
        Me.GroupControl1.Controls.Add(Me.txtModel)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Location = New System.Drawing.Point(6, 125)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(479, 85)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Make and Model"
        '
        'txtMake
        '
        Me.txtMake.CharacterCasing = CharacterCasing.Normal
        Me.txtMake.EnterMoveNextControl = True
        Me.txtMake.Location = New System.Drawing.Point(91, 28)
        Me.txtMake.MaxLength = 30
        Me.txtMake.Name = "txtMake"
        Me.txtMake.NumericAllowNegatives = False
        Me.txtMake.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMake.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMake.Properties.AccessibleName = "Family"
        Me.txtMake.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMake.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMake.Properties.Appearance.Options.UseFont = True
        Me.txtMake.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMake.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMake.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMake.Properties.MaxLength = 30
        Me.txtMake.Size = New System.Drawing.Size(378, 22)
        Me.txtMake.TabIndex = 1
        Me.txtMake.Tag = "AE"
        Me.txtMake.TextAlign = HorizontalAlignment.Left
        Me.txtMake.ToolTipText = ""
        '
        'txtModel
        '
        Me.txtModel.CharacterCasing = CharacterCasing.Normal
        Me.txtModel.EnterMoveNextControl = True
        Me.txtModel.Location = New System.Drawing.Point(91, 56)
        Me.txtModel.MaxLength = 30
        Me.txtModel.Name = "txtModel"
        Me.txtModel.NumericAllowNegatives = False
        Me.txtModel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtModel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtModel.Properties.AccessibleName = "Family"
        Me.txtModel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtModel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtModel.Properties.Appearance.Options.UseFont = True
        Me.txtModel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtModel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtModel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtModel.Properties.MaxLength = 30
        Me.txtModel.Size = New System.Drawing.Size(378, 22)
        Me.txtModel.TabIndex = 3
        Me.txtModel.Tag = "AE"
        Me.txtModel.TextAlign = HorizontalAlignment.Left
        Me.txtModel.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Model"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Make"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblClass)
        Me.GroupBox1.Controls.Add(Me.cbxStatus)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cbxCategory)
        Me.GroupBox1.Controls.Add(Me.lblFamily)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(479, 113)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "General Details"
        '
        'lblClass
        '
        Me.lblClass.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblClass.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblClass.Location = New System.Drawing.Point(11, 59)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(48, 15)
        Me.lblClass.TabIndex = 2
        Me.lblClass.Text = "Category"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(91, 84)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Status"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(378, 22)
        Me.cbxStatus.TabIndex = 5
        Me.cbxStatus.Tag = "AEM"
        Me.cbxStatus.ValueMember = Nothing
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(11, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Status"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(91, 56)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AccessibleName = "Category"
        Me.cbxCategory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(378, 22)
        Me.cbxCategory.TabIndex = 3
        Me.cbxCategory.Tag = "AEM"
        Me.cbxCategory.ValueMember = Nothing
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(11, 31)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(32, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Name"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(91, 28)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.Size = New System.Drawing.Size(378, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'tabCalendar
        '
        Me.tabCalendar.Controls.Add(Me.YearView1)
        Me.tabCalendar.Name = "tabCalendar"
        Me.tabCalendar.Size = New System.Drawing.Size(836, 464)
        Me.tabCalendar.Text = "Calendar"
        '
        'YearView1
        '
        Me.YearView1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.YearView1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YearView1.Location = New System.Drawing.Point(3, 3)
        Me.YearView1.Name = "YearView1"
        Me.YearView1.Size = New System.Drawing.Size(830, 458)
        Me.YearView1.TabIndex = 0
        '
        'tabInspection
        '
        Me.tabInspection.Controls.Add(Me.cgInsp)
        Me.tabInspection.Controls.Add(Me.GroupControl6)
        Me.tabInspection.Name = "tabInspection"
        Me.tabInspection.Size = New System.Drawing.Size(836, 464)
        Me.tabInspection.Text = "Inspections"
        '
        'cgInsp
        '
        Me.cgInsp.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgInsp.ButtonsEnabled = False
        Me.cgInsp.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgInsp.HideFirstColumn = False
        Me.cgInsp.Location = New System.Drawing.Point(6, 99)
        Me.cgInsp.Name = "cgInsp"
        Me.cgInsp.PreviewColumn = ""
        Me.cgInsp.Size = New System.Drawing.Size(824, 359)
        Me.cgInsp.TabIndex = 1
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.cdtInspLast)
        Me.GroupControl6.Controls.Add(Me.CareLabel19)
        Me.GroupControl6.Controls.Add(Me.CareLabel20)
        Me.GroupControl6.Controls.Add(Me.cbxInspFreq)
        Me.GroupControl6.Controls.Add(Me.chkInsp)
        Me.GroupControl6.Controls.Add(Me.CareLabel21)
        Me.GroupControl6.Controls.Add(Me.CareLabel22)
        Me.GroupControl6.Controls.Add(Me.cdtInspNext)
        Me.GroupControl6.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(824, 87)
        Me.GroupControl6.TabIndex = 0
        Me.GroupControl6.Text = "Inspection Configuration"
        '
        'cdtInspLast
        '
        Me.cdtInspLast.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtInspLast.EnterMoveNextControl = True
        Me.cdtInspLast.Location = New System.Drawing.Point(407, 56)
        Me.cdtInspLast.Name = "cdtInspLast"
        Me.cdtInspLast.Properties.AccessibleName = "Date of Birth"
        Me.cdtInspLast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInspLast.Properties.Appearance.Options.UseFont = True
        Me.cdtInspLast.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInspLast.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInspLast.Size = New System.Drawing.Size(85, 22)
        Me.cdtInspLast.TabIndex = 7
        Me.cdtInspLast.Tag = "AE"
        Me.cdtInspLast.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(288, 59)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel19.TabIndex = 6
        Me.CareLabel19.Text = "Last Inspection"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(288, 31)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(113, 15)
        Me.CareLabel20.TabIndex = 2
        Me.CareLabel20.Text = "Inspection Frequency"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxInspFreq
        '
        Me.cbxInspFreq.AllowBlank = False
        Me.cbxInspFreq.DataSource = Nothing
        Me.cbxInspFreq.DisplayMember = Nothing
        Me.cbxInspFreq.EnterMoveNextControl = True
        Me.cbxInspFreq.Location = New System.Drawing.Point(407, 28)
        Me.cbxInspFreq.Name = "cbxInspFreq"
        Me.cbxInspFreq.Properties.AccessibleName = "Classification"
        Me.cbxInspFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInspFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInspFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxInspFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInspFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInspFreq.SelectedValue = Nothing
        Me.cbxInspFreq.Size = New System.Drawing.Size(250, 22)
        Me.cbxInspFreq.TabIndex = 3
        Me.cbxInspFreq.Tag = "AE"
        Me.cbxInspFreq.ValueMember = Nothing
        '
        'chkInsp
        '
        Me.chkInsp.EnterMoveNextControl = True
        Me.chkInsp.Location = New System.Drawing.Point(122, 29)
        Me.chkInsp.Name = "chkInsp"
        Me.chkInsp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInsp.Properties.Appearance.Options.UseFont = True
        Me.chkInsp.Size = New System.Drawing.Size(20, 19)
        Me.chkInsp.TabIndex = 1
        Me.chkInsp.Tag = "AE"
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(82, 15)
        Me.CareLabel21.TabIndex = 4
        Me.CareLabel21.Text = "Next Inspection"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel22.TabIndex = 0
        Me.CareLabel22.Text = "Inspection Required"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInspNext
        '
        Me.cdtInspNext.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtInspNext.EnterMoveNextControl = True
        Me.cdtInspNext.Location = New System.Drawing.Point(122, 56)
        Me.cdtInspNext.Name = "cdtInspNext"
        Me.cdtInspNext.Properties.AccessibleName = "Date of Birth"
        Me.cdtInspNext.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInspNext.Properties.Appearance.Options.UseFont = True
        Me.cdtInspNext.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInspNext.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInspNext.Size = New System.Drawing.Size(85, 22)
        Me.cdtInspNext.TabIndex = 5
        Me.cdtInspNext.Tag = "AE"
        Me.cdtInspNext.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'tabService
        '
        Me.tabService.Controls.Add(Me.GroupControl7)
        Me.tabService.Controls.Add(Me.cgServ)
        Me.tabService.Name = "tabService"
        Me.tabService.Size = New System.Drawing.Size(836, 464)
        Me.tabService.Text = "Servicing"
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl7.Controls.Add(Me.cdtServLast)
        Me.GroupControl7.Controls.Add(Me.CareLabel23)
        Me.GroupControl7.Controls.Add(Me.CareLabel24)
        Me.GroupControl7.Controls.Add(Me.cbxServFreq)
        Me.GroupControl7.Controls.Add(Me.chkServ)
        Me.GroupControl7.Controls.Add(Me.CareLabel25)
        Me.GroupControl7.Controls.Add(Me.CareLabel26)
        Me.GroupControl7.Controls.Add(Me.cdtServNext)
        Me.GroupControl7.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(824, 87)
        Me.GroupControl7.TabIndex = 0
        Me.GroupControl7.Text = "Servicing Configuration"
        '
        'cdtServLast
        '
        Me.cdtServLast.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtServLast.EnterMoveNextControl = True
        Me.cdtServLast.Location = New System.Drawing.Point(407, 56)
        Me.cdtServLast.Name = "cdtServLast"
        Me.cdtServLast.Properties.AccessibleName = "Date of Birth"
        Me.cdtServLast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtServLast.Properties.Appearance.Options.UseFont = True
        Me.cdtServLast.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtServLast.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtServLast.Size = New System.Drawing.Size(85, 22)
        Me.cdtServLast.TabIndex = 7
        Me.cdtServLast.Tag = "AE"
        Me.cdtServLast.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(288, 59)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel23.TabIndex = 6
        Me.CareLabel23.Text = "Last Service"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(288, 31)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(95, 15)
        Me.CareLabel24.TabIndex = 2
        Me.CareLabel24.Text = "Service Frequency"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxServFreq
        '
        Me.cbxServFreq.AllowBlank = False
        Me.cbxServFreq.DataSource = Nothing
        Me.cbxServFreq.DisplayMember = Nothing
        Me.cbxServFreq.EnterMoveNextControl = True
        Me.cbxServFreq.Location = New System.Drawing.Point(407, 28)
        Me.cbxServFreq.Name = "cbxServFreq"
        Me.cbxServFreq.Properties.AccessibleName = "Classification"
        Me.cbxServFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxServFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxServFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxServFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxServFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxServFreq.SelectedValue = Nothing
        Me.cbxServFreq.Size = New System.Drawing.Size(250, 22)
        Me.cbxServFreq.TabIndex = 3
        Me.cbxServFreq.Tag = "AE"
        Me.cbxServFreq.ValueMember = Nothing
        '
        'chkServ
        '
        Me.chkServ.EnterMoveNextControl = True
        Me.chkServ.Location = New System.Drawing.Point(122, 29)
        Me.chkServ.Name = "chkServ"
        Me.chkServ.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkServ.Properties.Appearance.Options.UseFont = True
        Me.chkServ.Size = New System.Drawing.Size(20, 19)
        Me.chkServ.TabIndex = 1
        Me.chkServ.Tag = "AE"
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel25.TabIndex = 4
        Me.CareLabel25.Text = "Next Service"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(87, 15)
        Me.CareLabel26.TabIndex = 0
        Me.CareLabel26.Text = "Service Required"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtServNext
        '
        Me.cdtServNext.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtServNext.EnterMoveNextControl = True
        Me.cdtServNext.Location = New System.Drawing.Point(122, 56)
        Me.cdtServNext.Name = "cdtServNext"
        Me.cdtServNext.Properties.AccessibleName = "Date of Birth"
        Me.cdtServNext.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtServNext.Properties.Appearance.Options.UseFont = True
        Me.cdtServNext.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtServNext.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtServNext.Size = New System.Drawing.Size(85, 22)
        Me.cdtServNext.TabIndex = 5
        Me.cdtServNext.Tag = "AE"
        Me.cdtServNext.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'cgServ
        '
        Me.cgServ.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgServ.ButtonsEnabled = False
        Me.cgServ.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgServ.HideFirstColumn = False
        Me.cgServ.Location = New System.Drawing.Point(6, 99)
        Me.cgServ.Name = "cgServ"
        Me.cgServ.PreviewColumn = ""
        Me.cgServ.Size = New System.Drawing.Size(824, 359)
        Me.cgServ.TabIndex = 1
        '
        'tabTest
        '
        Me.tabTest.Controls.Add(Me.cgActivity)
        Me.tabTest.Controls.Add(Me.GroupControl9)
        Me.tabTest.Name = "tabTest"
        Me.tabTest.Size = New System.Drawing.Size(836, 464)
        Me.tabTest.Text = "Testing"
        '
        'cgActivity
        '
        Me.cgActivity.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgActivity.ButtonsEnabled = False
        Me.cgActivity.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgActivity.HideFirstColumn = False
        Me.cgActivity.Location = New System.Drawing.Point(6, 99)
        Me.cgActivity.Name = "cgActivity"
        Me.cgActivity.PreviewColumn = ""
        Me.cgActivity.Size = New System.Drawing.Size(824, 359)
        Me.cgActivity.TabIndex = 1
        '
        'GroupControl9
        '
        Me.GroupControl9.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl9.Controls.Add(Me.cdtTestLast)
        Me.GroupControl9.Controls.Add(Me.CareLabel31)
        Me.GroupControl9.Controls.Add(Me.CareLabel32)
        Me.GroupControl9.Controls.Add(Me.cbxTestFreq)
        Me.GroupControl9.Controls.Add(Me.chkTest)
        Me.GroupControl9.Controls.Add(Me.CareLabel33)
        Me.GroupControl9.Controls.Add(Me.CareLabel34)
        Me.GroupControl9.Controls.Add(Me.cdtTestNext)
        Me.GroupControl9.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(824, 87)
        Me.GroupControl9.TabIndex = 0
        Me.GroupControl9.Text = "Testing Configuration"
        '
        'cdtTestLast
        '
        Me.cdtTestLast.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtTestLast.EnterMoveNextControl = True
        Me.cdtTestLast.Location = New System.Drawing.Point(407, 56)
        Me.cdtTestLast.Name = "cdtTestLast"
        Me.cdtTestLast.Properties.AccessibleName = "Date of Birth"
        Me.cdtTestLast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTestLast.Properties.Appearance.Options.UseFont = True
        Me.cdtTestLast.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTestLast.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTestLast.Size = New System.Drawing.Size(85, 22)
        Me.cdtTestLast.TabIndex = 7
        Me.cdtTestLast.Tag = "AE"
        Me.cdtTestLast.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(288, 59)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel31.TabIndex = 6
        Me.CareLabel31.Text = "Last Test"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(288, 31)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel32.TabIndex = 2
        Me.CareLabel32.Text = "Test Frequency"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTestFreq
        '
        Me.cbxTestFreq.AllowBlank = False
        Me.cbxTestFreq.DataSource = Nothing
        Me.cbxTestFreq.DisplayMember = Nothing
        Me.cbxTestFreq.EnterMoveNextControl = True
        Me.cbxTestFreq.Location = New System.Drawing.Point(407, 28)
        Me.cbxTestFreq.Name = "cbxTestFreq"
        Me.cbxTestFreq.Properties.AccessibleName = "Classification"
        Me.cbxTestFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxTestFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTestFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxTestFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTestFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxTestFreq.SelectedValue = Nothing
        Me.cbxTestFreq.Size = New System.Drawing.Size(250, 22)
        Me.cbxTestFreq.TabIndex = 3
        Me.cbxTestFreq.Tag = "AE"
        Me.cbxTestFreq.ValueMember = Nothing
        '
        'chkTest
        '
        Me.chkTest.EnterMoveNextControl = True
        Me.chkTest.Location = New System.Drawing.Point(122, 29)
        Me.chkTest.Name = "chkTest"
        Me.chkTest.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTest.Properties.Appearance.Options.UseFont = True
        Me.chkTest.Size = New System.Drawing.Size(20, 19)
        Me.chkTest.TabIndex = 1
        Me.chkTest.Tag = "AE"
        '
        'CareLabel33
        '
        Me.CareLabel33.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel33.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel33.Name = "CareLabel33"
        Me.CareLabel33.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel33.TabIndex = 4
        Me.CareLabel33.Text = "Next Test"
        Me.CareLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel34
        '
        Me.CareLabel34.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel34.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel34.Name = "CareLabel34"
        Me.CareLabel34.Size = New System.Drawing.Size(72, 15)
        Me.CareLabel34.TabIndex = 0
        Me.CareLabel34.Text = "Test Required"
        Me.CareLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTestNext
        '
        Me.cdtTestNext.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtTestNext.EnterMoveNextControl = True
        Me.cdtTestNext.Location = New System.Drawing.Point(122, 56)
        Me.cdtTestNext.Name = "cdtTestNext"
        Me.cdtTestNext.Properties.AccessibleName = "Date of Birth"
        Me.cdtTestNext.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTestNext.Properties.Appearance.Options.UseFont = True
        Me.cdtTestNext.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTestNext.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTestNext.Size = New System.Drawing.Size(85, 22)
        Me.cdtTestNext.TabIndex = 5
        Me.cdtTestNext.Tag = "AE"
        Me.cdtTestNext.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'tabRisk
        '
        Me.tabRisk.Controls.Add(Me.cgRisk)
        Me.tabRisk.Name = "tabRisk"
        Me.tabRisk.Size = New System.Drawing.Size(836, 464)
        Me.tabRisk.Text = "Associated Risk Assessments"
        '
        'cgRisk
        '
        Me.cgRisk.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRisk.ButtonsEnabled = False
        Me.cgRisk.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRisk.HideFirstColumn = False
        Me.cgRisk.Location = New System.Drawing.Point(6, 6)
        Me.cgRisk.Name = "cgRisk"
        Me.cgRisk.PreviewColumn = ""
        Me.cgRisk.Size = New System.Drawing.Size(824, 452)
        Me.cgRisk.TabIndex = 1
        '
        'tabIncidents
        '
        Me.tabIncidents.Name = "tabIncidents"
        Me.tabIncidents.Size = New System.Drawing.Size(836, 464)
        Me.tabIncidents.Text = "Associated Incidents"
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.CareGrid1)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(836, 464)
        Me.tabActivity.Text = "Activity"
        '
        'CareGrid1
        '
        Me.CareGrid1.AllowBuildColumns = True
        Me.CareGrid1.AllowEdit = False
        Me.CareGrid1.AllowHorizontalScroll = False
        Me.CareGrid1.AllowMultiSelect = False
        Me.CareGrid1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CareGrid1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareGrid1.Appearance.Options.UseFont = True
        Me.CareGrid1.AutoSizeByData = True
        Me.CareGrid1.DisableAutoSize = False
        Me.CareGrid1.DisableDataFormatting = False
        Me.CareGrid1.FocusedRowHandle = -2147483648
        Me.CareGrid1.HideFirstColumn = False
        Me.CareGrid1.Location = New System.Drawing.Point(6, 6)
        Me.CareGrid1.Name = "CareGrid1"
        Me.CareGrid1.PreviewColumn = ""
        Me.CareGrid1.QueryID = Nothing
        Me.CareGrid1.RowAutoHeight = False
        Me.CareGrid1.SearchAsYouType = True
        Me.CareGrid1.ShowAutoFilterRow = False
        Me.CareGrid1.ShowFindPanel = False
        Me.CareGrid1.ShowGroupByBox = True
        Me.CareGrid1.ShowLoadingPanel = False
        Me.CareGrid1.ShowNavigator = False
        Me.CareGrid1.Size = New System.Drawing.Size(825, 452)
        Me.CareGrid1.TabIndex = 0
        '
        'frmEquipment
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(859, 582)
        Me.Controls.Add(Me.ctMain)
        Me.Name = "frmEquipment"
        Me.Text = "frmEquipment"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.ctMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctMain.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.chkAccessChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAccessStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cdtWarrantyExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWarrantyExpires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWarrantyMonths.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWarrantyFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWarrantyFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWarranty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl17.ResumeLayout(False)
        Me.GroupControl17.PerformLayout()
        CType(Me.cdtLeaseComplete.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLeaseComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLeaseMonths.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLeaseFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLeaseFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLease.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cbxCondition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUsed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPurchasePrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPurchaseDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPurchaseDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtMake.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCalendar.ResumeLayout(False)
        Me.tabInspection.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cdtInspLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInspLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInspFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkInsp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInspNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInspNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabService.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.cdtServLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtServLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxServFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkServ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtServNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtServNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabTest.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.cdtTestLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTestLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTestFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTestNext.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTestNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRisk.ResumeLayout(False)
        Me.tabActivity.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctMain As Care.Controls.CareTab
    Friend WithEvents tabGeneral As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabInspection As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Private WithEvents cdtPurchaseDate As Care.Controls.CareDateTime
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtModel As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblClass As Care.Controls.CareLabel
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents GroupControl17 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents chkLease As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLocation As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkAccessChild As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents chkAccessStaff As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Private WithEvents cdtWarrantyExpires As Care.Controls.CareDateTime
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtWarrantyMonths As Care.Controls.CareTextBox
    Private WithEvents cdtWarrantyFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents chkWarranty As Care.Controls.CareCheckBox
    Friend WithEvents GroupBox3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PictureBox As Care.Controls.PhotoControl
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Private WithEvents cdtLeaseComplete As Care.Controls.CareDateTime
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtLeaseMonths As Care.Controls.CareTextBox
    Private WithEvents cdtLeaseFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents cbxCondition As Care.Controls.CareComboBox
    Friend WithEvents chkUsed As Care.Controls.CareCheckBox
    Friend WithEvents txtPurchasePrice As Care.Controls.CareTextBox
    Friend WithEvents txtMake As Care.Controls.CareTextBox
    Friend WithEvents tabCalendar As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabService As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabRisk As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabTest As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents YearView1 As Nursery.YearView
    Friend WithEvents cgInsp As Care.Controls.CareGridWithButtons
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Private WithEvents cdtInspLast As Care.Controls.CareDateTime
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents cbxInspFreq As Care.Controls.CareComboBox
    Friend WithEvents chkInsp As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Private WithEvents cdtInspNext As Care.Controls.CareDateTime
    Friend WithEvents cgServ As Care.Controls.CareGridWithButtons
    Friend WithEvents cgRisk As Care.Controls.CareGridWithButtons
    Friend WithEvents cgActivity As Care.Controls.CareGridWithButtons
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Private WithEvents cdtTestLast As Care.Controls.CareDateTime
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents cbxTestFreq As Care.Controls.CareComboBox
    Friend WithEvents chkTest As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel33 As Care.Controls.CareLabel
    Friend WithEvents CareLabel34 As Care.Controls.CareLabel
    Private WithEvents cdtTestNext As Care.Controls.CareDateTime
    Friend WithEvents CareGrid1 As Care.Controls.CareGrid
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Private WithEvents cdtServLast As Care.Controls.CareDateTime
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents cbxServFreq As Care.Controls.CareComboBox
    Friend WithEvents chkServ As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Private WithEvents cdtServNext As Care.Controls.CareDateTime
    Friend WithEvents tabIncidents As DevExpress.XtraTab.XtraTabPage
End Class
