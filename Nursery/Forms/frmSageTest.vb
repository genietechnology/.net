﻿Option Strict On

Imports Care.Global
Imports Care.Financials

Public Class frmSageTest

    Private m_Xero As Boolean = False
    Private m_System As String = ""
    Private m_DefaultNameFormat As String = ""
    'Private m_Records As New List(Of SyncRecord)
    Private m_Integration As Care.Financials.IntegrationDetail = Nothing
    Private m_Engine As Care.Financials.Engine = Nothing
    Private m_Connected As Boolean = False

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Session.SetProgressMessage("Connecting to Financial System...")

        'm_Integration = Financials.ReturnFinancialsIntegration
        If m_Integration IsNot Nothing Then
            m_Engine = New Care.Financials.Engine(Nothing)
            If m_Engine IsNot Nothing Then

                Dim _e As New Entity
                With _e
                    .OurAccountNumber = "TEST0001"
                    .ContactName = "Mr Test"
                End With

                Dim _r As Response = m_Engine.CreateEntity(_e)
                If _r.HasErrors Then
                    CareMessage(_r.ErrorText + vbCrLf + vbCrLf + _r.Exception.Message)
                End If

            End If
        End If

    End Sub
End Class