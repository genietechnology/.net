﻿Imports Care.Global
Imports Care.Data

Public Class frmGroups

    Dim m_Group As Business.Group

    Private Sub frmGroups_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.GridSQL = "select id, name as 'Name', age_min as 'Min Age', age_max as 'Max Age' from groups order by sequence"
        gbx.Hide()

    End Sub

    Protected Overrides Sub FormatGrid()
        'ug.DisplayLayout.Bands(0).Columns(0).Hidden = True
    End Sub

    Protected Overrides Sub SetBindings()

        m_Group = New Business.Group
        bs.DataSource = m_Group

        txtName.DataBindings.Add("Text", bs, "_name")
        txtAgeMin.DataBindings.Add("Text", bs, "_agemin")
        txtAgeMax.DataBindings.Add("Text", bs, "_agemax")
        txtSeq.DataBindings.Add("Text", bs, "_sequence")

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean

        If Grid.RecordCount <= 0 Then Return False
        If Grid.CurrentRow(0).ToString = "" Then Return False

        Dim _ID As String = Grid.CurrentRow(0).ToString
        Dim _SQL As String = ""
        Dim _Count As Integer = 0

        'check if there are any children associated with this group
        _SQL = "select count (*) from Children where group_id = '" + _ID + "'"
        _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            CareMessage("There are " + _Count.ToString + " children associated with this Group. Delete Cancelled.", MessageBoxIcon.Exclamation, "Child Check")
            Return False
        End If

        'check if there are any staff associated with this group
        _SQL = "select count (*) from Staff where group_id = '" + _ID + "'"
        _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            CareMessage("There are " + _Count.ToString + " staff associated with this Group. Delete Cancelled.", MessageBoxIcon.Exclamation, "Staff Check")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub CommitDelete(ID As Guid)
        Dim _SQL As String = "delete from Groups where id = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Protected Overrides Sub CommitUpdate()

        If txtAgeMin.Text = "" Then txtAgeMin.Text = "0"
        If txtAgeMax.Text = "" Then txtAgeMax.Text = "0"
        If txtSeq.Text = "" Then txtSeq.Text = "0"

        m_Group.Store()
        m_Group.UpdateGroupNames()

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_Group = Business.Group.RetreiveByID(ID)
        bs.DataSource = m_Group
    End Sub

End Class
