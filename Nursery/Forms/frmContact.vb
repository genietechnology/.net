﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared
Imports Care.Data

Public Class frmContact

    Dim m_Contact As Business.Contact
    Dim m_AddContact As Boolean
    Dim m_ID As Guid
    Dim m_Mode As String
    Dim m_Loading As Boolean

#Region "Properties"

    Public Property AddContact() As Boolean
        Get
            Return m_AddContact
        End Get
        Set(ByVal value As Boolean)
            m_AddContact = value
        End Set
    End Property

    Public Property ID() As Guid
        Get
            Return m_ID
        End Get
        Set(ByVal value As Guid)
            m_ID = value
        End Set
    End Property

#End Region

#Region "Overrides"

    Protected Overrides Sub KeyHandler(ByVal e As KeyEventArgs)
        MyBase.KeyHandler(e)
        If e.KeyCode = Keys.Escape Then Cancel()
    End Sub

#End Region

    Private Sub frmContact_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        m_Loading = True

        PictureBox.Enabled = False

        With cbxReportDetail
            .AddItem("Basic Report", "B")
            .AddItem("Standard Report", "S")
            .AddItem("High Detail Report", "H")
        End With

        cbxProvider.AllowBlank = True
        cbxProvider.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Voucher Providers"))
        cbxCommMethod.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Communication Preferences"))

        If m_AddContact Then
            m_Mode = "ADD"
            m_Contact = New Business.Contact
            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
            btnEdit.Enabled = False
            btnOK.Enabled = True
        Else
            m_Mode = ""
            m_Contact = Business.Contact.RetreiveByID(m_ID)
            MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
            btnEdit.Enabled = True
            btnOK.Enabled = False
        End If

        bs.DataSource = m_Contact
        SetBindings()

        If m_AddContact Then
            PictureBox.DocumentID = Nothing
            PictureBox.Clear()
        Else

            txtTel.Text = m_Contact._TelHome
            txtMobile.Text = m_Contact._TelMobile
            txtTelJob.Text = m_Contact._JobTel

            txtEmail.Text = m_Contact._Email
            txtEmailJob.Text = m_Contact._JobEmail

            PictureBox.Enabled = False
            If m_Contact._Photo.HasValue Then
                PictureBox.DocumentID = m_Contact._Photo
                PictureBox.Image = DocumentHandler.GetImageByID(m_Contact._Photo)
            Else
                PictureBox.DocumentID = Nothing
                PictureBox.Clear()
            End If

        End If

        With crm
            .CRMLinkType = Care.Shared.CRM.EnumLinkType.Family
            .CRMLinkID = m_Contact._FamilyId
            .CRMContactID = m_Contact._ID
            .CRMContactName = m_Contact._Fullname
            .CRMContactMobile = m_Contact._TelMobile
            .CRMContactEmail = m_Contact._Email
        End With

    End Sub

    Private Sub SetBindings()

        txtFullname.DataBindings.Add("Text", bs, "_Fullname")
        txtForename.DataBindings.Add("Text", bs, "_Forename")
        txtSurname.DataBindings.Add("Text", bs, "_Surname")
        txtRelationship.DataBindings.Add("Text", bs, "_Relationship")

        txtAddress.DataBindings.Add("Text", bs, "_Address")

        chkEmployed.DataBindings.Add("Checked", bs, "_Job")
        txtEmployer.DataBindings.Add("Text", bs, "_JobEmployer")
        txtJobTitle.DataBindings.Add("Text", bs, "_JobTitle")

        chkSMS.DataBindings.Add("Checked", bs, "_sms")

        cdtDOB.DataBindings.Add("Value", bs, "_Dob", True)
        txtNI.DataBindings.Add("Text", bs, "_Nino")

        chkPrimary.DataBindings.Add("Checked", bs, "_PrimaryCont")
        chkEmergency.DataBindings.Add("Checked", bs, "_EmerCont")
        chkCollect.DataBindings.Add("Checked", bs, "_Collect")
        chkParent.DataBindings.Add("Checked", bs, "_Parent")

        txtSecret.DataBindings.Add("Text", bs, "_Password")

        chkVoucher.DataBindings.Add("Checked", bs, "_Voucher")
        cbxProvider.DataBindings.Add("Text", bs, "_VoucherName")
        txtVouchRef.DataBindings.Add("Text", bs, "_VoucherRef")
        cdtVouchDate.DataBindings.Add("Value", bs, "_VoucherDate", True)
        txtVoucherAmount.DataBindings.Add("Text", bs, "_VoucherValue")

        chkReports.DataBindings.Add("Checked", bs, "_Report")
        cbxReportDetail.DataBindings.Add("SelectedValue", bs, "_ReportDetail")
        chkInvoiceEmail.DataBindings.Add("Checked", bs, "_InvoiceEmail")

        cbxCommMethod.DataBindings.Add("Text", bs, "_CommMethod")
        chkCommMarketing.DataBindings.Add("Checked", bs, "_CommMarketing")

    End Sub

    Private Sub CommitUpdate()

        bs.EndEdit()

        If m_Mode = "ADD" Then
            m_Contact._FamilyId = m_ID
        End If

        DocumentHandler.UpdatePhoto(m_Contact._Photo, m_Contact._ID, PictureBox, m_Contact._Fullname)

        If m_Contact._PrimaryCont Then
            Business.Contact.ResetPrimaryContacts(m_Contact._FamilyId.Value)
            m_Contact._EmerCont = True
            m_Contact._Collect = True
            m_Contact._Parent = True
        End If

        m_Contact._Address = txtAddress.Text

        If cbxProvider.SelectedValue Is Nothing Then
            m_Contact._VoucherId = Nothing
        Else
            If cbxProvider.SelectedValue.ToString <> "" Then
                m_Contact._VoucherId = New Guid(cbxProvider.SelectedValue.ToString)
            Else
                m_Contact._VoucherId = Nothing
            End If
        End If

        m_Contact._TelHome = txtTel.Text
        m_Contact._TelMobile = txtMobile.Text
        m_Contact._JobTel = txtTelJob.Text

        m_Contact._Email = txtEmail.Text
        m_Contact._JobEmail = txtEmailJob.Text

        Business.Contact.SaveContact(m_Contact)

    End Sub

    Private Sub Cancel()
        Me.Close()
    End Sub

#Region "Controls"

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        m_Mode = "EDIT"
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
        PictureBox.Enabled = True
        btnOK.Enabled = True
        Application.DoEvents()
        txtForename.Focus()
    End Sub

    Private Sub btnCopyAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyAddress.Click

        If m_Mode <> "" Then

            Dim _Family As Business.Family
            If m_AddContact Then
                _Family = Business.Family.RetreiveByID(m_ID)
            Else
                _Family = Business.Family.RetreiveByID(m_Contact._FamilyId.Value)
            End If

            If Not _Family Is Nothing Then
                m_Contact._Address = _Family._Address
                txtAddress.Text = _Family._Address
                _Family = Nothing
            End If
        Else

        End If

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        If MyControls.Validate(Me.Controls) Then

            Cursor.Current = Cursors.WaitCursor
            btnOK.Enabled = False
            btnCancel.Enabled = False

            bs.EndEdit()
            CommitUpdate()

            If m_Mode = "ADD" Then
                Me.Close()
            Else
                MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
                PictureBox.Enabled = False
            End If

            m_Mode = ""
            Cursor.Current = Cursors.Default

        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Cancel()
    End Sub

    Private Sub chkPrimary_Click(sender As System.Object, e As System.EventArgs) Handles chkPrimary.Click

        If chkPrimary.Checked Then

            CareMessage("This person is nominated as the Primary contact." & vbNewLine & vbNewLine & _
                        "A primary contact is:" & vbNewLine & _
                        "> Automatically selected as an emergency contact" & vbNewLine & _
                        "> Automatically selected as having parental responsibility" & vbNewLine & _
                        "> Automatically selected as being able to collect children" & vbNewLine & _
                        "> Used for all correspondence generated by Nursery Genie" & vbNewLine & vbNewLine & _
                        "There can only be one Primary contact per family.", MessageBoxIcon.Exclamation, "Primary Contact Note")

        End If

    End Sub

    Private Sub chkPrimary_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles chkPrimary.Validating
        If chkPrimary.Checked Then
            chkEmergency.Checked = True
            chkParent.Checked = True
            chkCollect.Checked = True
            chkInvoiceEmail.Checked = True
        Else
            If m_Contact._PrimaryCont Then
                CareMessage("A primary contact cannot be unset." & vbCrLf & _
                            "To change the primary contact - tick this box on another contact.", MessageBoxIcon.Exclamation, "Primary Contact")
                chkPrimary.Checked = True
            End If
        End If
    End Sub

    Private Sub frmContact_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        If m_Loading Then
            m_Loading = False
            txtForename.Focus()
        End If
    End Sub
#End Region

End Class
