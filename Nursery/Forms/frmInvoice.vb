﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms
Imports Care.Financials

Public Class frmInvoice

    Public Enum EnumMode
        AmendInvoice
        ViewInvoice
    End Enum

    Private m_Mode As EnumMode

    Private m_InvoiceID As Guid = Nothing
    Private m_Invoice As Business.Invoice = Nothing

    Private m_BatchID As Guid = Nothing
    Private m_SiteID As Guid? = Nothing

    Private m_ChildID As Guid? = Nothing
    Private m_Child As Business.Child = Nothing

    Private m_FamilyID As Guid = Nothing
    Private m_FamilyName As String
    Private m_FamilyFinancialsID As String

    Private m_MemoEdit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

    Public Sub New(ByVal Mode As EnumMode, ByVal InvoiceID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Mode = Mode

        m_InvoiceID = InvoiceID
        m_Invoice = Business.Invoice.RetreiveByID(m_InvoiceID)

        Dim _Batch As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(m_Invoice._BatchId.Value)

        'try and get the site from the invoice batch
        If _Batch._SiteId.HasValue Then m_SiteID = _Batch._SiteId

        Select Case m_Mode

            Case EnumMode.AmendInvoice
                Me.Text = "Amend Invoice"

            Case EnumMode.ViewInvoice
                Me.Text = "View Invoice"

        End Select

    End Sub

    Private Sub frmInvoice_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        btnFinancials.Enabled = False

        With cbxDiscount
            .AllowBlank = True
            .ValueMember = "ID"
            .DisplayMember = "name"
            .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, name from Discounts order by discount_value, name").Rows
        End With

        If m_Mode = EnumMode.AmendInvoice Then
            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls, ControlHandler.DebugMode.None)
        Else
            MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls, ControlHandler.DebugMode.None)
        End If

        DisplayInvoice()

        SetButtons()

    End Sub

    Private Sub DisplayFamilyAddress()

        Dim _Family As Business.Family = Business.Family.RetreiveByID(m_FamilyID)
        m_FamilyName = _Family._Surname
        m_FamilyFinancialsID = _Family._FinancialsId
        If txtAddress.Text = "" Then txtAddress.Text = _Family._Address
        _Family = Nothing

    End Sub

    Private Sub DisplayInvoice()

        m_BatchID = m_Invoice._BatchId.Value

        'we might not have a child, opening balance batches etc.
        If m_Invoice._ChildId.HasValue Then
            m_ChildID = m_Invoice._ChildId
            m_Child = Business.Child.RetreiveByID(m_ChildID.Value)
            If Not m_SiteID.HasValue Then
                m_SiteID = m_Child._SiteId
            End If
        End If

        btnSessions.Enabled = m_Invoice._ChildId.HasValue

        m_FamilyID = m_Invoice._FamilyId.Value
        DisplayFamilyAddress()

        txtInvoiceNo.Text = m_Invoice._InvoiceNo.ToString
        txtStatus.Text = m_Invoice._InvoiceStatus

        If m_Invoice._InvoiceDate.HasValue Then txtInvoiceDate.Text = Format(m_Invoice._InvoiceDate, "dd/MM/yyyy")
        If m_Invoice._InvoiceDueDate.HasValue Then txtInvoiceDueDate.Text = Format(m_Invoice._InvoiceDueDate, "dd/MM/yyyy")
        If m_Invoice._InvoicePeriod.HasValue Then txtInvoicePeriod.Text = Format(m_Invoice._InvoicePeriod, "dd/MM/yyyy")
        If m_Invoice._InvoicePeriodEnd.HasValue Then txtInvoicePeriodEnd.Text = Format(m_Invoice._InvoicePeriodEnd, "dd/MM/yyyy")

        txtChild.Text = m_Invoice._ChildName
        txtAddress.Text = m_Invoice._Address

        cbxDiscount.SelectedValue = m_Invoice._DiscountId

        txtSubTotal.Text = Format(m_Invoice._InvoiceSub, "0.00")
        txtDiscount.Text = Format(m_Invoice._InvoiceDiscount, "0.00")
        txtTOTAL.Text = Format(m_Invoice._InvoiceTotal, "0.00")

        If m_Invoice._InvoiceStatus = "Posted" AndAlso m_Invoice._FinancialsDocId <> "" Then
            btnFinancials.Enabled = True
            btnCredit.Enabled = True
        Else
            btnFinancials.Enabled = False
            btnCredit.Enabled = False
        End If

        If m_Invoice._ChangedBy.HasValue Then
            Dim _u As Care.Shared.Business.User = Care.Shared.Business.User.RetreiveByID(m_Invoice._ChangedBy.Value)
            If _u IsNot Nothing Then
                gbxHeader.Text += " - Last Modified by " + _u._Fullname + " @ " + Format(m_Invoice._ChangedStamp, "dd/MM/yyyy HH:mm").ToString
            Else
                gbxHeader.Text += " - Last Modified by Deleted User @ " + Format(m_Invoice._ChangedStamp, "dd/MM/yyyy HH:mm").ToString
            End If
        End If

        DisplayGrid()

    End Sub

    Private Sub DisplayGrid()

        Session.CursorWaiting()

        If radLines.Checked Then
            DisplayLines()
        Else
            DisplayDays()
        End If

        btnAdd.Visible = radLines.Checked
        btnEdit.Visible = radLines.Checked
        btnRemove.Visible = radLines.Checked

        chkHideColours.Visible = radLines.Checked
        chkHideNL.Visible = radLines.Checked
        chkHideIndicators.Visible = radLines.Checked

        Session.CursorDefault()

    End Sub

    Private Sub DisplayLines()

        Dim _SQL As String = ""
        _SQL += "select l.id, l.line_no, l.action_date, datename(weekday, l.action_date) as 'weekday',"
        _SQL += " l.description, l.nl_code, l.nl_tracking, l.section, l.summary_column, l.hours, l.value, t.colour"
        _SQL += " from InvoiceLines l"
        _SQL += " left join Tariffs t On cast(t.ID As varchar(100)) = l.ref_3"
        _SQL += " where l.invoice_id = '" & m_InvoiceID.ToString & "'"
        _SQL += " order by l.line_no"

        grdItems.Populate(Session.ConnectionString, _SQL)

        grdItems.UnderlyingGridControl.BeginUpdate()

        grdItems.Columns("id").Visible = False
        grdItems.Columns("line_no").Caption = "Line No"
        grdItems.Columns("action_date").Caption = "Date"
        grdItems.Columns("weekday").Caption = " "

        grdItems.Columns("description").Caption = "Description"
        grdItems.Columns("description").ColumnEdit = m_MemoEdit

        grdItems.Columns("section").Caption = "Section"
        grdItems.Columns("summary_column").Caption = "Column"
        grdItems.Columns("section").Visible = Not chkHideIndicators.Checked
        grdItems.Columns("summary_column").Visible = Not chkHideIndicators.Checked

        grdItems.Columns("nl_code").Caption = "NL Code"
        grdItems.Columns("nl_tracking").Caption = "NL Tracking"
        grdItems.Columns("nl_code").Visible = Not chkHideNL.Checked
        grdItems.Columns("nl_tracking").Visible = Not chkHideNL.Checked

        grdItems.Columns("hours").Caption = "Hours"
        grdItems.Columns("value").Caption = "Amount"
        grdItems.Columns("colour").Visible = False

        grdItems.AutoSizeColumns()

        grdItems.UnderlyingGridControl.EndUpdate()

    End Sub

    Private Sub DisplayDays()

        Dim _SQL As String = ""

        _SQL += "select invoice_date as 'Date', datename(weekday, invoice_date) as 'Day',"
        _SQL += " hours_non_funded as 'Non-Funded Hours', hours_funded as 'Funded Hours', hours_total as 'Total Hours',"
        _SQL += " value_non_funded as 'Non-Funded £', value_funded as 'Funded £', value_boltons as 'Bolt-Ons £',"
        _SQL += " value_gross as 'Gross £', value_net as 'Net £'"
        _SQL += " from InvoiceDays"
        _SQL += " where invoice_id = '" + m_InvoiceID.ToString + "'"
        _SQL += " order by invoice_date"

        grdItems.Populate(Session.ConnectionString, _SQL)
        grdItems.AutoSizeColumns()

    End Sub

    Private Sub ToggleItemButtons(ByVal Enabled As Boolean)

        If Enabled Then

            btnAdd.Enabled = True

            If grdItems.RecordCount > 0 Then
                btnEdit.Enabled = True
                btnRemove.Enabled = True
            Else
                btnEdit.Enabled = False
                btnRemove.Enabled = False
            End If

        Else
            btnAdd.Enabled = False
            btnEdit.Enabled = False
            btnRemove.Enabled = False
        End If

    End Sub

    Private Sub SetButtons()

        Select Case m_Mode

            Case EnumMode.AmendInvoice

                btnPrint.Enabled = True
                btnEmail.Enabled = True

                btnGenerate.Enabled = True

                btnSave.Enabled = True
                btnCancel.Enabled = True

                ToggleItemButtons(True)

            Case EnumMode.ViewInvoice

                btnPrint.Enabled = True
                btnEmail.Enabled = True

                btnGenerate.Enabled = False

                btnSave.Enabled = False
                btnCancel.Enabled = True

                ToggleItemButtons(False)

        End Select

    End Sub

    Private Sub CalculateTotals()

        Dim _SQL As String = "select isnull(SUM(value),0) as 'TOTAL', isnull(SUM(discountable),0) as 'DISCOUNTABLE'" & _
                             " from InvoiceLines" & _
                             " where invoice_id = '" & m_InvoiceID.ToString & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            m_Invoice._InvoiceSub = Decimal.Parse(_DR("TOTAL").ToString)
            m_Invoice._Discountable = Decimal.Parse(_DR("DISCOUNTABLE").ToString)
        Else
            m_Invoice._InvoiceSub = 0
            m_Invoice._Discountable = 0
        End If

        _DR = Nothing

        m_Invoice._InvoiceDiscount = 0

        If m_Invoice._InvoiceSub > 0 Then
            If cbxDiscount.SelectedValue IsNot Nothing Then
                Dim _D As Business.Discount = Business.Discount.RetreiveByID(New Guid(cbxDiscount.SelectedValue.ToString))
                If _D IsNot Nothing Then
                    If _D._DiscountType = "Percentage" Then
                        m_Invoice._InvoiceDiscount = m_Invoice._Discountable / 100 * _D._DiscountValue
                    Else
                        m_Invoice._InvoiceDiscount = _D._DiscountValue
                    End If
                End If
            End If
        End If

        m_Invoice._InvoiceTotal = m_Invoice._InvoiceSub - m_Invoice._InvoiceDiscount

        If m_Invoice._InvoiceDiscount > 0 Then
            m_Invoice._InvoiceDiscount = m_Invoice._InvoiceDiscount * -1
        End If

        m_Invoice._ChangedBy = Session.CurrentUser.ID
        m_Invoice._ChangedStamp = Now

        m_Invoice.Store()

        txtSubTotal.Text = Format(m_Invoice._InvoiceSub, "0.00")
        txtDiscount.Text = Format(m_Invoice._InvoiceDiscount, "0.00")
        txtTOTAL.Text = Format(m_Invoice._InvoiceTotal, "0.00")

    End Sub

    Private Sub SaveHeader()

        CalculateTotals()

        If cbxDiscount.SelectedValue Is Nothing Then
            m_Invoice._DiscountId = Nothing
            m_Invoice._DiscountName = Nothing
        Else
            m_Invoice._DiscountId = New Guid(cbxDiscount.SelectedValue.ToString)
            m_Invoice._DiscountName = cbxDiscount.Text
        End If

        m_Invoice._InvoiceSub = Decimal.Parse(txtSubTotal.Text)
        m_Invoice._InvoiceDiscount = Decimal.Parse(txtDiscount.Text)
        m_Invoice._InvoiceTotal = Decimal.Parse(txtTOTAL.Text)

        m_Invoice.Store()

    End Sub

    Private Sub Save()
        If Not MyControls.Validate(Me.Controls) Then Exit Sub
        SaveHeader()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Save()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click

        btnPrint.Enabled = False
        Session.CursorWaiting()

        If btnPrint.ShiftDown Then

            Dim _InvoiceFile As String = ""
            _InvoiceFile = m_Invoice._InvoiceLayout

            If _InvoiceFile = "" Then _InvoiceFile = ParameterHandler.ReturnString("INVOICEFILE")

            If _InvoiceFile = "" Then
                Session.SetProgressMessage("INVOICEFILE parameter does not exist or is blank.", 10)
                Exit Sub
            End If

            Select Case m_Invoice._GenerationMode

                Case 0
                    ReportHandler.DesignReport(Of Reports.InvoiceData)(_InvoiceFile, New List(Of Reports.InvoiceData))

                Case 1
                    ReportHandler.DesignReport(Of Reports.SummaryColumnInvoiceData)(_InvoiceFile, New List(Of Reports.SummaryColumnInvoiceData))

                Case 2
                    ReportHandler.DesignReport(Of Reports.KLNInvoiceData)(_InvoiceFile, New List(Of Reports.KLNInvoiceData))

            End Select

        Else
            Invoicing.PrintInvoice(m_InvoiceID)
        End If

        btnPrint.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ToggleLine()
    End Sub

    Private Sub EditLine()

        If radDays.Checked Then Exit Sub
        If m_Mode = EnumMode.ViewInvoice Then Exit Sub
        If grdItems.RecordCount < 1 Then Exit Sub

        If grdItems.CurrentRow(0).ToString <> "" Then
            Dim _id As New Guid(grdItems.CurrentRow(0).ToString)
            ToggleLine(_id)
        End If

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As System.EventArgs) Handles btnEdit.Click
        EditLine()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As System.EventArgs) Handles btnRemove.Click

        If CareMessage("Are you sure you want to Remove the selected Invoice Line?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Remove") = MsgBoxResult.Yes Then

            If grdItems.RecordCount < 1 Then Exit Sub
            If grdItems.CurrentRow(0).ToString <> "" Then

                Dim _id As New Guid(grdItems.CurrentRow(0).ToString)
                Dim _SQL As String = "delete from InvoiceLines where ID = '" & _id.ToString & "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                CalculateTotals()
                DisplayGrid()
                SetButtons()

            End If

        End If

    End Sub

    Private Sub ToggleLine(Optional LineID As Guid? = Nothing)

        Dim _frmLine As New frmInvoiceLine(m_Invoice, LineID)
        _frmLine.ShowDialog()

        If _frmLine.DialogResult = DialogResult.OK Then
            CalculateTotals()
            DisplayGrid()
            SetButtons()
        End If

    End Sub

    Private Sub cbxDiscount_Validated(sender As Object, e As System.EventArgs) Handles cbxDiscount.Validated
        CalculateTotals()
    End Sub

    Private Sub btnEmail_Click(sender As System.Object, e As System.EventArgs) Handles btnEmail.Click

        If txtStatus.Text = "Emailed" Then
            If CareMessage("This invoice has already been emailed. Are you sure you want to send another copy?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Email Invoice") = DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim _Invoices As New List(Of Guid)
        _Invoices.Add(m_InvoiceID)

        Invoicing.SendInvoices(_Invoices, True)

    End Sub

    Private Sub btnGenerate_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerate.Click
        RecalculateInvoice()
    End Sub

    Private Sub RecalculateInvoice()

        If m_ChildID.HasValue Then

            Save()

            Dim _Mess As String = "This process will re-calculate this invoice based upon the Session Data." & vbCrLf & vbCrLf & _
                                  "Are you sure you wish to Proceed?"

            If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Recalculate Invoice") = DialogResult.Yes Then

                ToggleAllButtons(False)
                Session.CursorWaiting()

                Invoicing.RecalculateInvoice(m_InvoiceID)
                m_Invoice = Business.Invoice.RetreiveByID(m_InvoiceID)

                ToggleAllButtons(True)
                Session.CursorDefault()

                DisplayInvoice()

            End If

        Else
            CareMessage("This type of Invoice cannot be re-calculated.", MessageBoxIcon.Exclamation, "Recalculate Invoice")
        End If

    End Sub

    Private Sub ToggleAllButtons(ByVal Enabled As Boolean)

        ToggleItemButtons(Enabled)

        btnPrint.Enabled = Enabled
        btnEmail.Enabled = Enabled
        btnGenerate.Enabled = Enabled
        btnFinancials.Enabled = Enabled

        btnSave.Enabled = Enabled
        btnCancel.Enabled = Enabled

    End Sub

    Private Sub grdItems_GridDoubleClick(sender As Object, e As EventArgs) Handles grdItems.GridDoubleClick
        EditLine()
    End Sub

    Private Sub btnFinancials_Click(sender As Object, e As EventArgs) Handles btnFinancials.Click
        If m_Invoice._InvoiceStatus = "Posted" AndAlso m_Invoice._FinancialsDocId <> "" Then
            Dim _E As New Care.Financials.Engine(FinanceShared.ReturnFinancialsIntegration(m_SiteID))
            If _E IsNot Nothing Then
                _E.OpenInvoice(m_Invoice._FinancialsDocId)
                _E = Nothing
            End If
        End If
    End Sub

    Private Sub btnSessions_Click(sender As Object, e As EventArgs) Handles btnSessions.Click

        If m_ChildID.HasValue Then

            btnSessions.Enabled = False
            Session.CursorWaiting()

            Business.Child.DrillDown(m_ChildID.Value, True)

            btnSessions.Enabled = True

        End If

    End Sub

    Private Sub grdItems_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles grdItems.RowCellStyle

        If radDays.Checked Then Exit Sub
        If chkHideColours.Checked Then Exit Sub

        Try

            Dim _ColourString As String = grdItems.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "colour")

            If _ColourString <> "" Then
                Dim _Colour As Drawing.Color = Nothing
                _Colour = Drawing.Color.FromName(_ColourString)
                e.Appearance.BackColor = _Colour
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkHideColours_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideColours.CheckedChanged
        grdItems.MoveFirst()
    End Sub

    Private Sub chkHideNL_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideNL.CheckedChanged
        DisplayGrid()
    End Sub

    Private Sub txtStatus_DoubleClick(sender As Object, e As EventArgs) Handles txtStatus.DoubleClick

        If txtStatus.Text = "Emailed" Then
            If CareMessage("Are you sure you want to mark this Invoice as generated?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Change Invoice Status") = DialogResult.Yes Then
                Invoicing.UpdateInvoiceStatus(m_InvoiceID.ToString, "Generated")
                txtStatus.Text = "Generated"
            End If
        Else
            CareMessage("Only Invoices marked as Emailed can be reset.", MessageBoxIcon.Exclamation, "Change Invoice Status")
        End If

    End Sub

    Private Sub btnFamily_Click(sender As Object, e As EventArgs) Handles btnFamily.Click
        Business.Family.DrillDown(m_FamilyID)
    End Sub

    Private Sub radLines_CheckedChanged(sender As Object, e As EventArgs) Handles radLines.CheckedChanged
        If radLines.Checked Then DisplayGrid()
    End Sub

    Private Sub radDays_CheckedChanged(sender As Object, e As EventArgs) Handles radDays.CheckedChanged
        If radDays.Checked Then DisplayGrid()
    End Sub

    Private Sub chkHideIndicators_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideIndicators.CheckedChanged
        DisplayGrid()
    End Sub

    Private Sub btnCredit_Click(sender As Object, e As EventArgs) Handles btnCredit.Click

        If CareMessage("Are you sure you want to Credit this Invoice?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Credit Invoice") = DialogResult.Yes Then

            btnCredit.Enabled = False
            Session.CursorWaiting()

            Dim _BatchNo As Integer = 0
            If Invoicing.CreditInvoice(m_InvoiceID, _BatchNo) Then
                CareMessage("Invoice has been credited. Batch " + _BatchNo.ToString + " is ready to post.", MessageBoxIcon.Information, "Invoice Credited")
            Else
                CareMessage("Unable to credit this invoice...", MessageBoxIcon.Exclamation, "Credit Invoice")
            End If

            Session.CursorDefault()

        End If

    End Sub

End Class
