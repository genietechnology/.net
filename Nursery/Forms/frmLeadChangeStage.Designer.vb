﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeadChangeStage
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtRecipient = New Care.Controls.CareTextBox(Me.components)
        Me.txtEmailBody = New DevExpress.XtraEditors.MemoEdit()
        Me.txtEmailSubject = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmail = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cdtActionDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxStage = New Care.Controls.CareComboBox(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtActionTime = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtContact = New Care.Controls.CareTextBox(Me.components)
        Me.txtStage = New Care.Controls.CareTextBox(Me.components)
        Me.txtChild = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtRecipient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmailBody.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmailSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cdtActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtActionDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtActionTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtRecipient)
        Me.GroupControl1.Controls.Add(Me.txtEmailBody)
        Me.GroupControl1.Controls.Add(Me.txtEmailSubject)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.chkEmail)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 168)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(872, 355)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Email Lead (Templates can be setup in Lead Stages)"
        '
        'txtRecipient
        '
        Me.txtRecipient.CharacterCasing = CharacterCasing.Normal
        Me.txtRecipient.EnterMoveNextControl = True
        Me.txtRecipient.Location = New System.Drawing.Point(112, 26)
        Me.txtRecipient.MaxLength = 1000
        Me.txtRecipient.Name = "txtRecipient"
        Me.txtRecipient.NumericAllowNegatives = False
        Me.txtRecipient.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRecipient.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRecipient.Properties.AccessibleName = "Family"
        Me.txtRecipient.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRecipient.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRecipient.Properties.Appearance.Options.UseFont = True
        Me.txtRecipient.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRecipient.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRecipient.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRecipient.Properties.MaxLength = 1000
        Me.txtRecipient.Size = New System.Drawing.Size(747, 22)
        Me.txtRecipient.TabIndex = 2
        Me.txtRecipient.Tag = ""
        Me.txtRecipient.TextAlign = HorizontalAlignment.Left
        Me.txtRecipient.ToolTipText = ""
        '
        'txtEmailBody
        '
        Me.txtEmailBody.Location = New System.Drawing.Point(88, 79)
        Me.txtEmailBody.Name = "txtEmailBody"
        Me.txtEmailBody.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailBody.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtEmailBody, True)
        Me.txtEmailBody.Size = New System.Drawing.Size(771, 270)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtEmailBody, OptionsSpelling1)
        Me.txtEmailBody.TabIndex = 6
        '
        'txtEmailSubject
        '
        Me.txtEmailSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtEmailSubject.EnterMoveNextControl = True
        Me.txtEmailSubject.Location = New System.Drawing.Point(88, 52)
        Me.txtEmailSubject.MaxLength = 1000
        Me.txtEmailSubject.Name = "txtEmailSubject"
        Me.txtEmailSubject.NumericAllowNegatives = False
        Me.txtEmailSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmailSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmailSubject.Properties.AccessibleName = "Family"
        Me.txtEmailSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmailSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEmailSubject.Properties.Appearance.Options.UseFont = True
        Me.txtEmailSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmailSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmailSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmailSubject.Properties.MaxLength = 1000
        Me.txtEmailSubject.Size = New System.Drawing.Size(771, 22)
        Me.txtEmailSubject.TabIndex = 4
        Me.txtEmailSubject.Tag = ""
        Me.txtEmailSubject.TextAlign = HorizontalAlignment.Left
        Me.txtEmailSubject.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 29)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(58, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Send Email"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmail
        '
        Me.chkEmail.EnterMoveNextControl = True
        Me.chkEmail.Location = New System.Drawing.Point(86, 27)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmail.Properties.Appearance.Options.UseFont = True
        Me.chkEmail.Size = New System.Drawing.Size(20, 19)
        Me.chkEmail.TabIndex = 1
        Me.chkEmail.Tag = "AE"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 54)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel2.TabIndex = 3
        Me.CareLabel2.Text = "Subject"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(11, 81)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(27, 15)
        Me.CareLabel3.TabIndex = 5
        Me.CareLabel3.Text = "Body"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.txtActionTime)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel4)
        Me.GroupControl2.Controls.Add(Me.cdtActionDate)
        Me.GroupControl2.Controls.Add(Me.cbxStage)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 102)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(872, 60)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.Text = "Change Stage"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(338, 31)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Change Stage to:"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Action Date"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtActionDate
        '
        Me.cdtActionDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtActionDate.EnterMoveNextControl = True
        Me.cdtActionDate.Location = New System.Drawing.Point(88, 28)
        Me.cdtActionDate.Name = "cdtActionDate"
        Me.cdtActionDate.Properties.AccessibleName = "Start Date"
        Me.cdtActionDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtActionDate.Properties.Appearance.Options.UseFont = True
        Me.cdtActionDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtActionDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtActionDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtActionDate.TabIndex = 1
        Me.cdtActionDate.Tag = ""
        Me.cdtActionDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'cbxStage
        '
        Me.cbxStage.AllowBlank = False
        Me.cbxStage.DataSource = Nothing
        Me.cbxStage.DisplayMember = Nothing
        Me.cbxStage.EnterMoveNextControl = True
        Me.cbxStage.Location = New System.Drawing.Point(434, 28)
        Me.cbxStage.Name = "cbxStage"
        Me.cbxStage.Properties.AccessibleName = "Gender"
        Me.cbxStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStage.Properties.Appearance.Options.UseFont = True
        Me.cbxStage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStage.SelectedValue = Nothing
        Me.cbxStage.Size = New System.Drawing.Size(425, 22)
        Me.cbxStage.TabIndex = 5
        Me.cbxStage.Tag = ""
        Me.cbxStage.ValueMember = Nothing
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(799, 529)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Tag = ""
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.CausesValidation = False
        Me.btnOK.Location = New System.Drawing.Point(708, 529)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(190, 31)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Action Time"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActionTime
        '
        Me.txtActionTime.CharacterCasing = CharacterCasing.Normal
        Me.txtActionTime.EnterMoveNextControl = True
        Me.txtActionTime.Location = New System.Drawing.Point(261, 28)
        Me.txtActionTime.MaxLength = 5
        Me.txtActionTime.Name = "txtActionTime"
        Me.txtActionTime.NumericAllowNegatives = False
        Me.txtActionTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtActionTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtActionTime.Properties.AccessibleName = "Family"
        Me.txtActionTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtActionTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtActionTime.Properties.Appearance.Options.UseFont = True
        Me.txtActionTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtActionTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtActionTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtActionTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtActionTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtActionTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtActionTime.Properties.MaxLength = 5
        Me.txtActionTime.Size = New System.Drawing.Size(61, 22)
        Me.txtActionTime.TabIndex = 3
        Me.txtActionTime.Tag = ""
        Me.txtActionTime.TextAlign = HorizontalAlignment.Left
        Me.txtActionTime.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.txtChild)
        Me.GroupControl3.Controls.Add(Me.CareLabel8)
        Me.GroupControl3.Controls.Add(Me.txtStage)
        Me.GroupControl3.Controls.Add(Me.txtContact)
        Me.GroupControl3.Controls.Add(Me.CareLabel7)
        Me.GroupControl3.Controls.Add(Me.CareLabel9)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 8)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(872, 88)
        Me.GroupControl3.TabIndex = 6
        Me.GroupControl3.Text = "Stage"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Contact"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Stage"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContact
        '
        Me.txtContact.CharacterCasing = CharacterCasing.Normal
        Me.txtContact.EnterMoveNextControl = True
        Me.txtContact.Location = New System.Drawing.Point(88, 28)
        Me.txtContact.MaxLength = 1000
        Me.txtContact.Name = "txtContact"
        Me.txtContact.NumericAllowNegatives = False
        Me.txtContact.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContact.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContact.Properties.AccessibleName = "Family"
        Me.txtContact.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContact.Properties.Appearance.Options.UseFont = True
        Me.txtContact.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContact.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContact.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContact.Properties.MaxLength = 1000
        Me.txtContact.Properties.ReadOnly = True
        Me.txtContact.Size = New System.Drawing.Size(303, 22)
        Me.txtContact.TabIndex = 8
        Me.txtContact.Tag = ""
        Me.txtContact.TextAlign = HorizontalAlignment.Left
        Me.txtContact.ToolTipText = ""
        '
        'txtStage
        '
        Me.txtStage.CharacterCasing = CharacterCasing.Normal
        Me.txtStage.EnterMoveNextControl = True
        Me.txtStage.Location = New System.Drawing.Point(88, 56)
        Me.txtStage.MaxLength = 1000
        Me.txtStage.Name = "txtStage"
        Me.txtStage.NumericAllowNegatives = False
        Me.txtStage.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStage.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStage.Properties.AccessibleName = "Family"
        Me.txtStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStage.Properties.Appearance.Options.UseFont = True
        Me.txtStage.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStage.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStage.Properties.MaxLength = 1000
        Me.txtStage.Properties.ReadOnly = True
        Me.txtStage.Size = New System.Drawing.Size(771, 22)
        Me.txtStage.TabIndex = 9
        Me.txtStage.Tag = ""
        Me.txtStage.TextAlign = HorizontalAlignment.Left
        Me.txtStage.ToolTipText = ""
        '
        'txtChild
        '
        Me.txtChild.CharacterCasing = CharacterCasing.Normal
        Me.txtChild.EnterMoveNextControl = True
        Me.txtChild.Location = New System.Drawing.Point(556, 28)
        Me.txtChild.MaxLength = 1000
        Me.txtChild.Name = "txtChild"
        Me.txtChild.NumericAllowNegatives = False
        Me.txtChild.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChild.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChild.Properties.AccessibleName = "Family"
        Me.txtChild.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChild.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChild.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChild.Properties.MaxLength = 1000
        Me.txtChild.Properties.ReadOnly = True
        Me.txtChild.Size = New System.Drawing.Size(303, 22)
        Me.txtChild.TabIndex = 11
        Me.txtChild.Tag = ""
        Me.txtChild.TextAlign = HorizontalAlignment.Left
        Me.txtChild.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(522, 31)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel8.TabIndex = 10
        Me.CareLabel8.Text = "Child"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmLeadChangeStage
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(896, 562)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeadChangeStage"
        Me.StartPosition = FormStartPosition.CenterScreen
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtRecipient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmailBody.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmailSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cdtActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtActionDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtActionTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtEmailBody As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtEmailSubject As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkEmail As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxStage As Care.Controls.CareComboBox
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents txtRecipient As Care.Controls.CareTextBox
    Private WithEvents cdtActionDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtActionTime As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtStage As Care.Controls.CareTextBox
    Friend WithEvents txtContact As Care.Controls.CareTextBox
    Friend WithEvents txtChild As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
End Class
