﻿Imports Care.Global

Public Class frmCDAPDetail

    Private m_CDAPID As Guid

    Private m_CDAPRecord As Business.ChildCDAP = Nothing
    Private m_CDAPDef As Business.CDAPDefinition = Nothing

    Public Sub New(ByVal CDAPID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_CDAPID = CDAPID

    End Sub

    Private Sub frmCDAPDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DisplayRecord()
        txtComments.BackColor = Session.ChangeColour
    End Sub

    Private Sub DisplayRecord()

        m_CDAPRecord = Business.ChildCDAP.RetreiveByID(m_CDAPID)
        m_CDAPDef = Business.CDAPDefinition.RetreiveByID(m_CDAPRecord._CdapId.Value)

        txtArea.Text = m_CDAPDef._Area
        txtStep.Text = m_CDAPDef._Step.ToString
        txtLetter.Text = m_CDAPDef._Letter

        txtName.Text = m_CDAPDef._Name
        txtStamp.Text = m_CDAPRecord._Stamp.ToString

        txtDescription.Text = m_CDAPDef._Description
        txtComments.Text = m_CDAPRecord._Comments

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        m_CDAPRecord._Comments = txtComments.Text
        m_CDAPRecord.Store()

        Me.Close()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmCDAPDetail_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtComments.Focus()
    End Sub
End Class