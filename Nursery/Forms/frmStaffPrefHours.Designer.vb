﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffPrefHours
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling8 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling9 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling10 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling11 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling12 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling13 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling14 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling15 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling16 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling17 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling18 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling19 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling20 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling21 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling22 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling23 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.gbx = New Care.Controls.CareFrame()
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDate = New Care.Controls.CareDateTime(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtSunH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtSatH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txtSunS = New DevExpress.XtraEditors.TextEdit()
        Me.txtSunF = New DevExpress.XtraEditors.TextEdit()
        Me.txtSatS = New DevExpress.XtraEditors.TextEdit()
        Me.txtSatF = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.txtWeekCost = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.txtWeekH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtFriH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtThuH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtWedH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtTueH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtMonH = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.txtFriS = New DevExpress.XtraEditors.TextEdit()
        Me.txtFriF = New DevExpress.XtraEditors.TextEdit()
        Me.txtThuS = New DevExpress.XtraEditors.TextEdit()
        Me.txtThuF = New DevExpress.XtraEditors.TextEdit()
        Me.txtWedS = New DevExpress.XtraEditors.TextEdit()
        Me.txtWedF = New DevExpress.XtraEditors.TextEdit()
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel27 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel28 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.txtTueS = New DevExpress.XtraEditors.TextEdit()
        Me.txtTueF = New DevExpress.XtraEditors.TextEdit()
        Me.txtMonS = New DevExpress.XtraEditors.TextEdit()
        Me.txtMonF = New DevExpress.XtraEditors.TextEdit()
        CType(Me.gbx, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbx.SuspendLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtSunH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSatH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSunS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSunF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSatS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSatF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeekCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeekH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFriH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtThuH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWedH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTueH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFriS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFriF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtThuS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtThuF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWedS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWedF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTueS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTueF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(147, 349)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(238, 349)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.CareLabel11)
        Me.gbx.Controls.Add(Me.cdtDate)
        Me.gbx.Location = New System.Drawing.Point(12, 12)
        Me.gbx.Name = "gbx"
        Me.gbx.ShowCaption = False
        Me.gbx.Size = New System.Drawing.Size(311, 42)
        Me.gbx.TabIndex = 0
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(12, 14)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Week Commencing"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDate
        '
        Me.cdtDate.EditValue = Nothing
        Me.cdtDate.EnterMoveNextControl = True
        Me.cdtDate.Location = New System.Drawing.Point(123, 11)
        Me.cdtDate.Name = "cdtDate"
        Me.cdtDate.Properties.AccessibleDescription = ""
        Me.cdtDate.Properties.AccessibleName = "Date"
        Me.cdtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtDate.TabIndex = 1
        Me.cdtDate.Tag = ""
        Me.cdtDate.Value = Nothing
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.txtSunH)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Controls.Add(Me.txtSatH)
        Me.GroupControl1.Controls.Add(Me.CareLabel16)
        Me.GroupControl1.Controls.Add(Me.CareLabel17)
        Me.GroupControl1.Controls.Add(Me.CareLabel23)
        Me.GroupControl1.Controls.Add(Me.txtSunS)
        Me.GroupControl1.Controls.Add(Me.txtSunF)
        Me.GroupControl1.Controls.Add(Me.txtSatS)
        Me.GroupControl1.Controls.Add(Me.txtSatF)
        Me.GroupControl1.Controls.Add(Me.CareLabel24)
        Me.GroupControl1.Controls.Add(Me.txtWeekCost)
        Me.GroupControl1.Controls.Add(Me.CareLabel32)
        Me.GroupControl1.Controls.Add(Me.CareLabel31)
        Me.GroupControl1.Controls.Add(Me.txtWeekH)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtFriH)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.txtThuH)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtWedH)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtTueH)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtMonH)
        Me.GroupControl1.Controls.Add(Me.CareLabel18)
        Me.GroupControl1.Controls.Add(Me.CareLabel19)
        Me.GroupControl1.Controls.Add(Me.CareLabel20)
        Me.GroupControl1.Controls.Add(Me.CareLabel21)
        Me.GroupControl1.Controls.Add(Me.CareLabel22)
        Me.GroupControl1.Controls.Add(Me.CareLabel25)
        Me.GroupControl1.Controls.Add(Me.txtFriS)
        Me.GroupControl1.Controls.Add(Me.txtFriF)
        Me.GroupControl1.Controls.Add(Me.txtThuS)
        Me.GroupControl1.Controls.Add(Me.txtThuF)
        Me.GroupControl1.Controls.Add(Me.txtWedS)
        Me.GroupControl1.Controls.Add(Me.txtWedF)
        Me.GroupControl1.Controls.Add(Me.CareLabel26)
        Me.GroupControl1.Controls.Add(Me.CareLabel27)
        Me.GroupControl1.Controls.Add(Me.CareLabel28)
        Me.GroupControl1.Controls.Add(Me.CareLabel29)
        Me.GroupControl1.Controls.Add(Me.txtTueS)
        Me.GroupControl1.Controls.Add(Me.txtTueF)
        Me.GroupControl1.Controls.Add(Me.txtMonS)
        Me.GroupControl1.Controls.Add(Me.txtMonF)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 60)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(311, 283)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Desired Working Pattern"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(216, 199)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel8.TabIndex = 48
        Me.CareLabel8.Tag = ""
        Me.CareLabel8.Text = "="
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSunH
        '
        Me.txtSunH.EditValue = ""
        Me.txtSunH.Location = New System.Drawing.Point(228, 197)
        Me.txtSunH.Margin = New Padding(1)
        Me.txtSunH.Name = "txtSunH"
        Me.txtSunH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSunH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSunH.Properties.MaxLength = 6
        Me.txtSunH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSunH, True)
        Me.txtSunH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSunH, OptionsSpelling1)
        Me.txtSunH.TabIndex = 49
        Me.txtSunH.TabStop = False
        Me.txtSunH.Tag = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(216, 171)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel9.TabIndex = 41
        Me.CareLabel9.Tag = ""
        Me.CareLabel9.Text = "="
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSatH
        '
        Me.txtSatH.EditValue = ""
        Me.txtSatH.Location = New System.Drawing.Point(228, 169)
        Me.txtSatH.Margin = New Padding(1)
        Me.txtSatH.Name = "txtSatH"
        Me.txtSatH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSatH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSatH.Properties.MaxLength = 6
        Me.txtSatH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSatH, True)
        Me.txtSatH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSatH, OptionsSpelling2)
        Me.txtSatH.TabIndex = 42
        Me.txtSatH.TabStop = False
        Me.txtSatH.Tag = ""
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(165, 199)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel16.TabIndex = 46
        Me.CareLabel16.Tag = ""
        Me.CareLabel16.Text = "-"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(165, 171)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel17.TabIndex = 38
        Me.CareLabel17.Tag = ""
        Me.CareLabel17.Text = "-"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(12, 199)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel23.TabIndex = 44
        Me.CareLabel23.Text = "Sunday"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSunS
        '
        Me.txtSunS.EditValue = ""
        Me.txtSunS.Location = New System.Drawing.Point(123, 197)
        Me.txtSunS.Margin = New Padding(1)
        Me.txtSunS.Name = "txtSunS"
        Me.txtSunS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSunS, True)
        Me.txtSunS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSunS, OptionsSpelling3)
        Me.txtSunS.TabIndex = 45
        Me.txtSunS.Tag = ""
        '
        'txtSunF
        '
        Me.txtSunF.EditValue = ""
        Me.txtSunF.Location = New System.Drawing.Point(174, 197)
        Me.txtSunF.Margin = New Padding(1)
        Me.txtSunF.Name = "txtSunF"
        Me.txtSunF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSunF, True)
        Me.txtSunF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSunF, OptionsSpelling4)
        Me.txtSunF.TabIndex = 47
        Me.txtSunF.Tag = ""
        '
        'txtSatS
        '
        Me.txtSatS.EditValue = ""
        Me.txtSatS.Location = New System.Drawing.Point(123, 169)
        Me.txtSatS.Margin = New Padding(1)
        Me.txtSatS.Name = "txtSatS"
        Me.txtSatS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSatS, True)
        Me.txtSatS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSatS, OptionsSpelling5)
        Me.txtSatS.TabIndex = 37
        Me.txtSatS.Tag = ""
        '
        'txtSatF
        '
        Me.txtSatF.EditValue = ""
        Me.txtSatF.Location = New System.Drawing.Point(174, 169)
        Me.txtSatF.Margin = New Padding(1)
        Me.txtSatF.Name = "txtSatF"
        Me.txtSatF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSatF, True)
        Me.txtSatF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSatF, OptionsSpelling6)
        Me.txtSatF.TabIndex = 39
        Me.txtSatF.Tag = ""
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(12, 171)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel24.TabIndex = 36
        Me.CareLabel24.Text = "Saturday"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeekCost
        '
        Me.txtWeekCost.EditValue = "0"
        Me.txtWeekCost.Location = New System.Drawing.Point(228, 254)
        Me.txtWeekCost.Margin = New Padding(1)
        Me.txtWeekCost.Name = "txtWeekCost"
        Me.txtWeekCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWeekCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtWeekCost.Properties.MaxLength = 6
        Me.txtWeekCost.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtWeekCost, True)
        Me.txtWeekCost.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtWeekCost, OptionsSpelling7)
        Me.txtWeekCost.TabIndex = 1
        Me.txtWeekCost.TabStop = False
        Me.txtWeekCost.Tag = ""
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(144, 256)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel32.TabIndex = 0
        Me.CareLabel32.Text = "Estimated Cost"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(145, 228)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel31.TabIndex = 51
        Me.CareLabel31.Text = "Total this week"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeekH
        '
        Me.txtWeekH.EditValue = ""
        Me.txtWeekH.Location = New System.Drawing.Point(228, 226)
        Me.txtWeekH.Margin = New Padding(1)
        Me.txtWeekH.Name = "txtWeekH"
        Me.txtWeekH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWeekH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtWeekH.Properties.MaxLength = 6
        Me.txtWeekH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtWeekH, True)
        Me.txtWeekH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtWeekH, OptionsSpelling8)
        Me.txtWeekH.TabIndex = 52
        Me.txtWeekH.TabStop = False
        Me.txtWeekH.Tag = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(216, 143)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel5.TabIndex = 33
        Me.CareLabel5.Tag = ""
        Me.CareLabel5.Text = "="
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFriH
        '
        Me.txtFriH.EditValue = ""
        Me.txtFriH.Location = New System.Drawing.Point(228, 141)
        Me.txtFriH.Margin = New Padding(1)
        Me.txtFriH.Name = "txtFriH"
        Me.txtFriH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFriH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtFriH.Properties.MaxLength = 6
        Me.txtFriH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtFriH, True)
        Me.txtFriH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtFriH, OptionsSpelling9)
        Me.txtFriH.TabIndex = 34
        Me.txtFriH.TabStop = False
        Me.txtFriH.Tag = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(216, 115)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel4.TabIndex = 26
        Me.CareLabel4.Tag = ""
        Me.CareLabel4.Text = "="
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtThuH
        '
        Me.txtThuH.EditValue = ""
        Me.txtThuH.Location = New System.Drawing.Point(228, 113)
        Me.txtThuH.Margin = New Padding(1)
        Me.txtThuH.Name = "txtThuH"
        Me.txtThuH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtThuH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtThuH.Properties.MaxLength = 6
        Me.txtThuH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtThuH, True)
        Me.txtThuH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtThuH, OptionsSpelling10)
        Me.txtThuH.TabIndex = 27
        Me.txtThuH.TabStop = False
        Me.txtThuH.Tag = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(216, 87)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel3.TabIndex = 19
        Me.CareLabel3.Tag = ""
        Me.CareLabel3.Text = "="
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWedH
        '
        Me.txtWedH.EditValue = ""
        Me.txtWedH.Location = New System.Drawing.Point(228, 85)
        Me.txtWedH.Margin = New Padding(1)
        Me.txtWedH.Name = "txtWedH"
        Me.txtWedH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWedH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtWedH.Properties.MaxLength = 6
        Me.txtWedH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtWedH, True)
        Me.txtWedH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtWedH, OptionsSpelling11)
        Me.txtWedH.TabIndex = 20
        Me.txtWedH.TabStop = False
        Me.txtWedH.Tag = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(216, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel2.TabIndex = 12
        Me.CareLabel2.Tag = ""
        Me.CareLabel2.Text = "="
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTueH
        '
        Me.txtTueH.EditValue = ""
        Me.txtTueH.Location = New System.Drawing.Point(228, 57)
        Me.txtTueH.Margin = New Padding(1)
        Me.txtTueH.Name = "txtTueH"
        Me.txtTueH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTueH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTueH.Properties.MaxLength = 6
        Me.txtTueH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtTueH, True)
        Me.txtTueH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtTueH, OptionsSpelling12)
        Me.txtTueH.TabIndex = 13
        Me.txtTueH.TabStop = False
        Me.txtTueH.Tag = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(216, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel1.TabIndex = 5
        Me.CareLabel1.Tag = ""
        Me.CareLabel1.Text = "="
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMonH
        '
        Me.txtMonH.EditValue = ""
        Me.txtMonH.Location = New System.Drawing.Point(228, 29)
        Me.txtMonH.Margin = New Padding(1)
        Me.txtMonH.Name = "txtMonH"
        Me.txtMonH.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMonH.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMonH.Properties.MaxLength = 6
        Me.txtMonH.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMonH, True)
        Me.txtMonH.Size = New System.Drawing.Size(72, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMonH, OptionsSpelling13)
        Me.txtMonH.TabIndex = 6
        Me.txtMonH.TabStop = False
        Me.txtMonH.Tag = ""
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(165, 143)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel18.TabIndex = 31
        Me.CareLabel18.Tag = ""
        Me.CareLabel18.Text = "-"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(165, 115)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel19.TabIndex = 24
        Me.CareLabel19.Tag = ""
        Me.CareLabel19.Text = "-"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(165, 87)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel20.TabIndex = 17
        Me.CareLabel20.Tag = ""
        Me.CareLabel20.Text = "-"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(165, 59)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel21.TabIndex = 10
        Me.CareLabel21.Tag = ""
        Me.CareLabel21.Text = "-"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(165, 31)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel22.TabIndex = 3
        Me.CareLabel22.Tag = ""
        Me.CareLabel22.Text = "-"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(12, 143)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel25.TabIndex = 29
        Me.CareLabel25.Text = "Friday"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFriS
        '
        Me.txtFriS.EditValue = ""
        Me.txtFriS.Location = New System.Drawing.Point(123, 141)
        Me.txtFriS.Margin = New Padding(1)
        Me.txtFriS.Name = "txtFriS"
        Me.txtFriS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtFriS, True)
        Me.txtFriS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtFriS, OptionsSpelling14)
        Me.txtFriS.TabIndex = 30
        Me.txtFriS.Tag = ""
        '
        'txtFriF
        '
        Me.txtFriF.EditValue = ""
        Me.txtFriF.Location = New System.Drawing.Point(174, 141)
        Me.txtFriF.Margin = New Padding(1)
        Me.txtFriF.Name = "txtFriF"
        Me.txtFriF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtFriF, True)
        Me.txtFriF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtFriF, OptionsSpelling15)
        Me.txtFriF.TabIndex = 32
        Me.txtFriF.Tag = ""
        '
        'txtThuS
        '
        Me.txtThuS.EditValue = ""
        Me.txtThuS.Location = New System.Drawing.Point(123, 113)
        Me.txtThuS.Margin = New Padding(1)
        Me.txtThuS.Name = "txtThuS"
        Me.txtThuS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtThuS, True)
        Me.txtThuS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtThuS, OptionsSpelling16)
        Me.txtThuS.TabIndex = 23
        Me.txtThuS.Tag = ""
        '
        'txtThuF
        '
        Me.txtThuF.EditValue = ""
        Me.txtThuF.Location = New System.Drawing.Point(174, 113)
        Me.txtThuF.Margin = New Padding(1)
        Me.txtThuF.Name = "txtThuF"
        Me.txtThuF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtThuF, True)
        Me.txtThuF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtThuF, OptionsSpelling17)
        Me.txtThuF.TabIndex = 25
        Me.txtThuF.Tag = ""
        '
        'txtWedS
        '
        Me.txtWedS.EditValue = ""
        Me.txtWedS.Location = New System.Drawing.Point(123, 85)
        Me.txtWedS.Margin = New Padding(1)
        Me.txtWedS.Name = "txtWedS"
        Me.txtWedS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtWedS, True)
        Me.txtWedS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtWedS, OptionsSpelling18)
        Me.txtWedS.TabIndex = 16
        Me.txtWedS.Tag = ""
        '
        'txtWedF
        '
        Me.txtWedF.EditValue = ""
        Me.txtWedF.Location = New System.Drawing.Point(174, 85)
        Me.txtWedF.Margin = New Padding(1)
        Me.txtWedF.Name = "txtWedF"
        Me.txtWedF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtWedF, True)
        Me.txtWedF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtWedF, OptionsSpelling19)
        Me.txtWedF.TabIndex = 18
        Me.txtWedF.Tag = ""
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(12, 115)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel26.TabIndex = 22
        Me.CareLabel26.Text = "Thursday"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel27
        '
        Me.CareLabel27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel27.Location = New System.Drawing.Point(12, 87)
        Me.CareLabel27.Name = "CareLabel27"
        Me.CareLabel27.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel27.TabIndex = 15
        Me.CareLabel27.Text = "Wednesday"
        Me.CareLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel28
        '
        Me.CareLabel28.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel28.Location = New System.Drawing.Point(12, 59)
        Me.CareLabel28.Name = "CareLabel28"
        Me.CareLabel28.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel28.TabIndex = 8
        Me.CareLabel28.Text = "Tuesday"
        Me.CareLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel29.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel29.TabIndex = 1
        Me.CareLabel29.Text = "Monday"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTueS
        '
        Me.txtTueS.EditValue = ""
        Me.txtTueS.Location = New System.Drawing.Point(123, 57)
        Me.txtTueS.Margin = New Padding(1)
        Me.txtTueS.Name = "txtTueS"
        Me.txtTueS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtTueS, True)
        Me.txtTueS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtTueS, OptionsSpelling20)
        Me.txtTueS.TabIndex = 9
        Me.txtTueS.Tag = ""
        '
        'txtTueF
        '
        Me.txtTueF.EditValue = ""
        Me.txtTueF.Location = New System.Drawing.Point(174, 57)
        Me.txtTueF.Margin = New Padding(1)
        Me.txtTueF.Name = "txtTueF"
        Me.txtTueF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtTueF, True)
        Me.txtTueF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtTueF, OptionsSpelling21)
        Me.txtTueF.TabIndex = 11
        Me.txtTueF.Tag = ""
        '
        'txtMonS
        '
        Me.txtMonS.EditValue = ""
        Me.txtMonS.Location = New System.Drawing.Point(123, 29)
        Me.txtMonS.Margin = New Padding(1)
        Me.txtMonS.Name = "txtMonS"
        Me.txtMonS.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMonS, True)
        Me.txtMonS.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMonS, OptionsSpelling22)
        Me.txtMonS.TabIndex = 2
        Me.txtMonS.Tag = ""
        '
        'txtMonF
        '
        Me.txtMonF.EditValue = ""
        Me.txtMonF.Location = New System.Drawing.Point(174, 29)
        Me.txtMonF.Margin = New Padding(1)
        Me.txtMonF.Name = "txtMonF"
        Me.txtMonF.Properties.MaxLength = 6
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMonF, True)
        Me.txtMonF.Size = New System.Drawing.Size(38, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMonF, OptionsSpelling23)
        Me.txtMonF.TabIndex = 4
        Me.txtMonF.Tag = ""
        '
        'frmStaffPrefHours
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 383)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbx)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStaffPrefHours"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmStaffPrefHours"
        CType(Me.gbx, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtSunH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSatH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSunS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSunF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSatS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSatF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeekCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeekH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFriH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtThuH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWedH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTueH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFriS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFriF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtThuS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtThuF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWedS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWedF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTueS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTueF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents gbx As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtDate As Care.Controls.CareDateTime
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtWeekCost As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents txtWeekH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtFriH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtThuH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtWedH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtTueH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtMonH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents txtFriS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtFriF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtThuS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtThuF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWedS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWedF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents CareLabel27 As Care.Controls.CareLabel
    Friend WithEvents CareLabel28 As Care.Controls.CareLabel
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents txtTueS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTueF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMonS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMonF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtSunH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtSatH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txtSunS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSunF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSatS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSatF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
End Class
