﻿Imports Care.Data
Imports Care.Global

Public Class frmCDAPSteps

    Dim m_Step As Business.CDAPStep

    Private Sub frmCDAPSteps_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, step as 'Step', age_min as 'Min Age', age_max as 'Max Age'" & _
                     " from CDAPSteps" & _
                     " order by step"

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_Step = New Business.CDAPStep
        bs.DataSource = m_Step

        txtStep.DataBindings.Add("Text", bs, "_Step")
        txtMin.DataBindings.Add("Text", bs, "_AgeMin")
        txtMax.DataBindings.Add("Text", bs, "_AgeMax")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_Step = Business.CDAPStep.RetreiveByID(ID)
        bs.DataSource = m_Step
    End Sub

    Protected Overrides Sub CommitUpdate()
        Business.CDAPStep.SaveRecord(CType(bs.Item(bs.Position), Business.CDAPStep))
    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        Dim _SQL As String = "delete from CDAPSteps where id = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Protected Overrides Sub AfterAdd()
        MyBase.AfterAdd()
        txtStep.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()
        MyBase.AfterEdit()
        txtStep.Focus()
    End Sub

End Class
