﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Public Class frmReportCRB

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click

        Cursor.Current = Cursors.WaitCursor
        btnRun.Enabled = False
        btnClose.Enabled = False

        Dim _SQL As String = "select fullname, crb_due, DATEDIFF(day, getdate(), crb_due) as 'crb_days'" & _
                             " from Staff" & _
                             " where status = 'P'" & _
                             " and (crb_due is null or DATEDIFF(day, getdate(), crb_due) <= 30)" & _
                             " order by crb_due, surname, forename"

        Dim _Rpt As New ReportHandler
        With _Rpt
            .ReportOutput = ReportHandler.OutputMode.Preview
            .ReportName = "CRB Report"
            .ReportFile = "crb.rpt"
            .ReportSQL = _SQL
            .RunReport(True)
        End With

        _Rpt = Nothing

        btnRun.Enabled = True
        btnClose.Enabled = True
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

End Class