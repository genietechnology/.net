﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTabletParams
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxParameter = New Care.Controls.CareFrame()
        Me.btnAmendCancel = New Care.Controls.CareButton(Me.components)
        Me.btnAmendOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtParent = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtDescription = New Care.Controls.CareTextBox(Me.components)
        Me.radScopeSpecific = New Care.Controls.CareRadioButton(Me.components)
        Me.radScopeMaster = New Care.Controls.CareRadioButton(Me.components)
        Me.chkValue = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.lblValue = New Care.Controls.CareLabel(Me.components)
        Me.txtEndpointName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtParamID = New Care.Controls.CareTextBox(Me.components)
        Me.txtValue = New Care.Controls.CareTextBox(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.gbxParameter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxParameter.SuspendLayout()
        CType(Me.txtParent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radScopeSpecific.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radScopeMaster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEndpointName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtParamID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = AnchorStyles.None
        Me.gbx.Controls.Add(Me.CareLabel6)
        Me.gbx.Controls.Add(Me.txtParent)
        Me.gbx.Controls.Add(Me.CareLabel5)
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.txtDescription)
        Me.gbx.Controls.Add(Me.radScopeSpecific)
        Me.gbx.Controls.Add(Me.radScopeMaster)
        Me.gbx.Controls.Add(Me.chkValue)
        Me.gbx.Controls.Add(Me.cbxType)
        Me.gbx.Controls.Add(Me.lblValue)
        Me.gbx.Controls.Add(Me.txtEndpointName)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Controls.Add(Me.txtParamID)
        Me.gbx.Controls.Add(Me.txtValue)
        Me.gbx.Location = New System.Drawing.Point(39, 75)
        Me.gbx.Size = New System.Drawing.Size(657, 244)
        Me.gbx.TabIndex = 2
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtValue, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtParamID, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtEndpointName, 0)
        Me.gbx.Controls.SetChildIndex(Me.lblValue, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxType, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkValue, 0)
        Me.gbx.Controls.SetChildIndex(Me.radScopeMaster, 0)
        Me.gbx.Controls.SetChildIndex(Me.radScopeSpecific, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDescription, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel5, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtParent, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel6, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 213)
        Me.Panel2.Size = New System.Drawing.Size(651, 28)
        Me.Panel2.TabIndex = 16
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(466, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(557, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Location = New System.Drawing.Point(14, 12)
        Me.Grid.Size = New System.Drawing.Size(700, 360)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 378)
        Me.Panel1.Size = New System.Drawing.Size(728, 35)
        Me.Panel1.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(634, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxParameter
        '
        Me.gbxParameter.Anchor = AnchorStyles.None
        Me.gbxParameter.Controls.Add(Me.btnAmendCancel)
        Me.gbxParameter.Controls.Add(Me.btnAmendOK)
        Me.gbxParameter.Location = New System.Drawing.Point(145, 90)
        Me.gbxParameter.Name = "gbxParameter"
        Me.gbxParameter.Size = New System.Drawing.Size(438, 258)
        Me.gbxParameter.TabIndex = 3
        '
        'btnAmendCancel
        '
        Me.btnAmendCancel.Location = New System.Drawing.Point(353, 227)
        Me.btnAmendCancel.Name = "btnAmendCancel"
        Me.btnAmendCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnAmendCancel.TabIndex = 5
        Me.btnAmendCancel.Text = "Cancel"
        '
        'btnAmendOK
        '
        Me.btnAmendOK.Location = New System.Drawing.Point(272, 227)
        Me.btnAmendOK.Name = "btnAmendOK"
        Me.btnAmendOK.Size = New System.Drawing.Size(75, 23)
        Me.btnAmendOK.TabIndex = 4
        Me.btnAmendOK.Text = "OK"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 75)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel6.TabIndex = 5
        Me.CareLabel6.Text = "Parent"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtParent
        '
        Me.txtParent.CharacterCasing = CharacterCasing.Normal
        Me.txtParent.EnterMoveNextControl = True
        Me.txtParent.Location = New System.Drawing.Point(111, 72)
        Me.txtParent.MaxLength = 40
        Me.txtParent.Name = "txtParent"
        Me.txtParent.NumericAllowNegatives = False
        Me.txtParent.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtParent.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtParent.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtParent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtParent.Properties.Appearance.Options.UseFont = True
        Me.txtParent.Properties.Appearance.Options.UseTextOptions = True
        Me.txtParent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtParent.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtParent.Properties.MaxLength = 40
        Me.txtParent.Size = New System.Drawing.Size(534, 22)
        Me.txtParent.TabIndex = 6
        Me.txtParent.Tag = "AE"
        Me.txtParent.TextAlign = HorizontalAlignment.Left
        Me.txtParent.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 187)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel5.TabIndex = 13
        Me.CareLabel5.Text = "Value"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 159)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel4.TabIndex = 11
        Me.CareLabel4.Text = "Parameter Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 131)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel3.TabIndex = 9
        Me.CareLabel3.Text = "Description"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 103)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(68, 15)
        Me.CareLabel2.TabIndex = 7
        Me.CareLabel2.Text = "Parameter ID"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = CharacterCasing.Normal
        Me.txtDescription.EnterMoveNextControl = True
        Me.txtDescription.Location = New System.Drawing.Point(111, 128)
        Me.txtDescription.MaxLength = 100
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.NumericAllowNegatives = False
        Me.txtDescription.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDescription.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDescription.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDescription.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDescription.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDescription.Properties.MaxLength = 100
        Me.txtDescription.Size = New System.Drawing.Size(534, 22)
        Me.txtDescription.TabIndex = 10
        Me.txtDescription.Tag = "AE"
        Me.txtDescription.TextAlign = HorizontalAlignment.Left
        Me.txtDescription.ToolTipText = ""
        '
        'radScopeSpecific
        '
        Me.radScopeSpecific.Location = New System.Drawing.Point(208, 17)
        Me.radScopeSpecific.Name = "radScopeSpecific"
        Me.radScopeSpecific.Properties.AutoWidth = True
        Me.radScopeSpecific.Properties.Caption = "Specific Endpoint"
        Me.radScopeSpecific.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radScopeSpecific.Properties.RadioGroupIndex = 0
        Me.radScopeSpecific.Size = New System.Drawing.Size(103, 19)
        Me.radScopeSpecific.TabIndex = 2
        Me.radScopeSpecific.TabStop = False
        '
        'radScopeMaster
        '
        Me.radScopeMaster.EditValue = True
        Me.radScopeMaster.Location = New System.Drawing.Point(111, 17)
        Me.radScopeMaster.Name = "radScopeMaster"
        Me.radScopeMaster.Properties.AutoWidth = True
        Me.radScopeMaster.Properties.Caption = "Master"
        Me.radScopeMaster.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radScopeMaster.Properties.RadioGroupIndex = 0
        Me.radScopeMaster.Size = New System.Drawing.Size(55, 19)
        Me.radScopeMaster.TabIndex = 1
        '
        'chkValue
        '
        Me.chkValue.EnterMoveNextControl = True
        Me.chkValue.Location = New System.Drawing.Point(111, 185)
        Me.chkValue.Name = "chkValue"
        Me.chkValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkValue.Properties.Appearance.Options.UseFont = True
        Me.chkValue.Size = New System.Drawing.Size(20, 19)
        Me.chkValue.TabIndex = 15
        Me.chkValue.Tag = "AE"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(111, 156)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AccessibleName = "Status"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(534, 22)
        Me.cbxType.TabIndex = 12
        Me.cbxType.Tag = "AE"
        Me.cbxType.ValueMember = Nothing
        '
        'lblValue
        '
        Me.lblValue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblValue.Location = New System.Drawing.Point(10, 47)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(83, 15)
        Me.lblValue.TabIndex = 3
        Me.lblValue.Text = "Endpoint Name"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEndpointName
        '
        Me.txtEndpointName.CharacterCasing = CharacterCasing.Normal
        Me.txtEndpointName.EnterMoveNextControl = True
        Me.txtEndpointName.Location = New System.Drawing.Point(111, 44)
        Me.txtEndpointName.MaxLength = 40
        Me.txtEndpointName.Name = "txtEndpointName"
        Me.txtEndpointName.NumericAllowNegatives = False
        Me.txtEndpointName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEndpointName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEndpointName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEndpointName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEndpointName.Properties.Appearance.Options.UseFont = True
        Me.txtEndpointName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEndpointName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEndpointName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEndpointName.Properties.MaxLength = 40
        Me.txtEndpointName.Size = New System.Drawing.Size(534, 22)
        Me.txtEndpointName.TabIndex = 4
        Me.txtEndpointName.Tag = "AE"
        Me.txtEndpointName.TextAlign = HorizontalAlignment.Left
        Me.txtEndpointName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 19)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Scope"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtParamID
        '
        Me.txtParamID.CharacterCasing = CharacterCasing.Upper
        Me.txtParamID.EnterMoveNextControl = True
        Me.txtParamID.Location = New System.Drawing.Point(111, 100)
        Me.txtParamID.MaxLength = 40
        Me.txtParamID.Name = "txtParamID"
        Me.txtParamID.NumericAllowNegatives = False
        Me.txtParamID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtParamID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtParamID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtParamID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtParamID.Properties.Appearance.Options.UseFont = True
        Me.txtParamID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtParamID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtParamID.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtParamID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtParamID.Properties.MaxLength = 40
        Me.txtParamID.Size = New System.Drawing.Size(534, 22)
        Me.txtParamID.TabIndex = 8
        Me.txtParamID.Tag = "AE"
        Me.txtParamID.TextAlign = HorizontalAlignment.Left
        Me.txtParamID.ToolTipText = ""
        '
        'txtValue
        '
        Me.txtValue.CharacterCasing = CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(111, 184)
        Me.txtValue.MaxLength = 100
        Me.txtValue.Name = "txtValue"
        Me.txtValue.NumericAllowNegatives = False
        Me.txtValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValue.Properties.MaxLength = 100
        Me.txtValue.Size = New System.Drawing.Size(534, 22)
        Me.txtValue.TabIndex = 14
        Me.txtValue.Tag = "AE"
        Me.txtValue.TextAlign = HorizontalAlignment.Left
        Me.txtValue.ToolTipText = ""
        '
        'frmTabletParams
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(728, 413)
        Me.Controls.Add(Me.gbxParameter)
        Me.LoadMaximised = True
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MaximizeBox = True
        Me.Name = "frmTabletParams"
        Me.Text = "frmTabletParams"
        Me.WindowState = FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.gbxParameter, 0)
        Me.Controls.SetChildIndex(Me.Grid, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.gbx, 0)
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.gbxParameter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxParameter.ResumeLayout(False)
        CType(Me.txtParent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radScopeSpecific.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radScopeMaster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEndpointName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtParamID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxParameter As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnAmendCancel As Care.Controls.CareButton
    Friend WithEvents btnAmendOK As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtParent As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtDescription As Care.Controls.CareTextBox
    Friend WithEvents radScopeSpecific As Care.Controls.CareRadioButton
    Friend WithEvents radScopeMaster As Care.Controls.CareRadioButton
    Friend WithEvents chkValue As Care.Controls.CareCheckBox
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents lblValue As Care.Controls.CareLabel
    Friend WithEvents txtEndpointName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtParamID As Care.Controls.CareTextBox
    Friend WithEvents txtValue As Care.Controls.CareTextBox
End Class
