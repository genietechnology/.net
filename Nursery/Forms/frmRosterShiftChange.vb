﻿Imports Care.Controls
Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports DevExpress.XtraEditors.Controls

Public Class frmRosterShiftChange

    Private m_RotaStaffID As Guid?

    'set in displayrecord
    '****************************
    Private m_RotaID As Guid
    Private m_RotaDate As Date
    '****************************

    Private m_MemoEdit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

    Public Sub New(ByVal RotaStaffID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RotaStaffID = RotaStaffID

    End Sub

    Private Sub frmRosterSwap_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtShiftStart.BackColor = Session.ChangeColour
        txtShiftEnd.BackColor = Session.ChangeColour

        ToggleSwap(False)
        ToggleShiftAmend(False)

        DisplayRecord()
        ToggleBottomButtons(True)

        btnCancel.Enabled = True

    End Sub

    Private Sub ToggleBottomButtons(ByVal Enabled As Boolean)
        btnDelete.Enabled = Enabled
        btnSwap.Enabled = Enabled
        btnTimes.Enabled = Enabled
        btnSwapComplete.Enabled = Enabled
        btnCancel.Enabled = Enabled
    End Sub

    Private Sub ToggleSwap(ByVal Visible As Boolean)

        gbxSwapHeader.Visible = Visible
        gbxSwapSlots.Visible = Visible
        btnSwapComplete.Visible = Visible

        If Visible Then
            Me.Width = 1016
            btnSwapComplete.Enabled = True
        Else
            Me.Width = 524
            btnSwapComplete.Enabled = False
        End If

    End Sub

    Private Sub DisplayRecord()

        Dim _R As Business.RotaStaff = Business.RotaStaff.RetreiveByID(m_RotaStaffID.Value)
        If _R IsNot Nothing Then

            m_RotaID = _R._RsRotaId.Value
            m_RotaDate = _R._RsDate.Value

            txtAStaff.Tag = _R._RsStaffId.ToString
            txtAStaff.Text = _R._RsStaffName
            txtARoom.Text = _R._RsRoom
            txtAFrom.Text = _R._RsStart.ToString
            txtATo.Text = _R._RsFinish.ToString
            txtADate.Text = Format(_R._RsDate, "dddd d MMMM yyyy")
            txtAHours.Text = Format(_R._RsHours, "0.00")
            txtACost.Tag = Format(_R._RsStaffRate, "0.00")
            txtACost.Text = Format(_R._RsCost, "0.00")

            DisplaySlots(cgASlots, _R._RsStaffId.Value)

        End If

    End Sub

    Private Sub DisplaySlots(ByRef GridControl As CareGrid, ByVal StaffID As Guid)

        Dim _SQL As String = ""

        _SQL += "select rss_start As 'Slot Start', rss_finish as 'Slot Finish', child_count as 'Children', staff_rostered as 'Staff',"
        _SQL += " staff_rostered_list as 'Rostered', staff_spare_list as 'Spare'"
        _SQL += " from RotaSlotStaff"
        _SQL += " left join RotaSlot on RotaSlot.rota_id = RotaSlotStaff.rss_rota_id and RotaSlot.ID = RotaSlotStaff.rss_slot_id"
        _SQL += " where rss_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " and rss_staff_id = '" + StaffID.ToString + "'"
        _SQL += " and rss_date = " + ValueHandler.SQLDate(m_RotaDate, True)
        _SQL += " order by rss_start"

        GridControl.Populate(Session.ConnectionString, _SQL)

        If GridControl.RecordCount > 0 Then
            GridControl.Columns("Rostered").ColumnEdit = m_MemoEdit
            GridControl.Columns("Spare").ColumnEdit = m_MemoEdit
        End If

    End Sub

    Private Sub btnSwapComplete_Click(sender As Object, e As EventArgs) Handles btnSwapComplete.Click

        'remove all the slots allocated for the staff for the selected date







        're-allocate the slots based upon the new date, start and end time





        're-build RotaSlot table based upon RotaSlotStaff








    End Sub
    Private Sub btnSwap_Click(sender As Object, e As EventArgs) Handles btnSwap.Click
        ToggleSwap(True)
    End Sub
    Private Sub ToggleShiftAmend(ByVal Visible As Boolean)
        gbxShift.Visible = Visible
        If Visible Then
            ToggleBottomButtons(False)
            txtShiftStart.Focus()
        Else
            ToggleBottomButtons(True)
        End If
    End Sub
    Private Sub btnTimes_Click(sender As Object, e As EventArgs) Handles btnTimes.Click
        ToggleShiftAmend(True)
    End Sub

    Private Sub btnShiftCancel_Click(sender As Object, e As EventArgs) Handles btnShiftCancel.Click
        ToggleShiftAmend(False)
    End Sub

    Private Sub btnShiftOK_Click(sender As Object, e As EventArgs) Handles btnShiftOK.Click
        ToggleShiftAmend(False)
    End Sub

    Private Function GetID(ByRef GridControl As Care.Controls.CareGrid) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item(0).ToString

        Return New Guid(_ID)

    End Function

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        If CareMessage("Are you sure you want to Delete this Shift?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Shift") = DialogResult.Yes Then

            Dim _SQL As String = ""

            _SQL += "delete from RotaSlotStaff"
            _SQL += " where rss_rota_id = '" + m_RotaID.ToString + "'"
            _SQL += " and rss_date = " + ValueHandler.SQLDate(m_RotaDate, True)
            _SQL += " and rss_staff_id = '" + txtAStaff.Tag.ToString + "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = ""
            _SQL += "delete from RotaStaff"
            _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
            _SQL += " and rs_date = " + ValueHandler.SQLDate(m_RotaDate, True)
            _SQL += " and rs_staff_id = '" + txtAStaff.Tag.ToString + "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            're-build RotaSlot table based upon RotaStaff and RotaChildren
            RebuildRotaSlotTable(m_RotaID, m_RotaDate)

        End If

    End Sub

    Private Sub RebuildRotaSlotTable(ByVal RotaID As Guid, ByVal RotaDate As Date)

        Dim _SS As List(Of Business.RotaSlotStaff) = Business.RotaSlotStaff.RetreiveByDate(RotaID, RotaDate)

        Dim _Slots As List(Of Business.RotaSlot) = Business.RotaSlot.RetreiveByDate(RotaID, RotaDate)
        For Each _S In _Slots
            SetRostered(_SS, _S._SlotId.Value, _S._StaffRostered, _S._StaffRosteredList)
            _S._StaffShort = _S._StaffRequired - _S._StaffRostered
            _S.Store()
        Next

    End Sub

    Private Sub SetRostered(ByRef StaffSlotList As List(Of Business.RotaSlotStaff), ByVal SlotID As Guid, ByRef RosteredCount As Integer, ByRef Rostered As String)

        RosteredCount = 0
        Rostered = ""

        Dim _Q = From _s As Business.RotaSlotStaff In StaffSlotList
                 Where _s._RssSlotId = SlotID

        If _Q IsNot Nothing And _Q.Count > 0 Then

            For Each _rss As Business.RotaSlotStaff In _Q

                RosteredCount += 1

                If Rostered = "" Then
                    Rostered += _rss._RssStaffName
                Else
                    Rostered += vbCrLf + _rss._RssStaffName
                End If

            Next

        End If

    End Sub

    Private Sub btnBSelect_Click(sender As Object, e As EventArgs) Handles btnBSelect.Click

        Dim _SQL As String = ""

        _SQL += "select rs_date As 'Date', datename(weekday, rs_date) as 'Day', rs_staff_name as 'Name' from RotaStaff"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Staff"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by rs_staff_name, rs_date"
            .ReturnField = "ID"
            .GridWhereClause = " and rs_rota_id = '" + m_RotaID.ToString + "' and rs_staff_id <> '" + txtAStaff.Tag.ToString + "'"
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then

            Dim _R As Business.RotaStaff = Business.RotaStaff.RetreiveByID(New Guid(_ReturnValue))
            If _R IsNot Nothing Then

                txtBStaff.Tag = _R._RsStaffId.ToString
                txtBStaff.Text = _R._RsStaffName
                txtBRoom.Text = _R._RsRoom
                txtBFrom.Text = _R._RsStart.ToString
                txtBTo.Text = _R._RsFinish.ToString
                txtBDate.Text = Format(_R._RsDate, "dddd d MMMM yyyy")
                txtBHours.Text = Format(_R._RsHours, "0.00")
                txtBCost.Text = Format(_R._RsCost, "0.00")

                DisplaySlots(cgBSlots, _R._RsStaffId.Value)

            End If

        End If

    End Sub

    'Private Sub SetControl(ByRef ControlIn As TextEdit)
    '    ControlIn.BackColor = Session.ChangeColour
    '    ControlIn.Properties.MaxLength = 5
    '    ControlIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
    '    ControlIn.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
    'End Sub

    Private Sub txtShiftStart_InvalidValue(sender As Object, e As InvalidValueExceptionEventArgs) Handles txtShiftStart.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtShiftEnd_InvalidValue(sender As Object, e As InvalidValueExceptionEventArgs) Handles txtShiftEnd.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtShiftStart_LostFocus(sender As Object, e As EventArgs) Handles txtShiftStart.LostFocus
        txtShiftHours.Text = CalculateHours(txtShiftStart.Text, txtShiftEnd.Text)
        CalculateCost(CalculateMins(txtShiftStart.Text, txtShiftEnd.Text), CDec(txtACost.Tag))
    End Sub

    Private Sub txtShiftEnd_LostFocus(sender As Object, e As EventArgs) Handles txtShiftEnd.LostFocus
        txtShiftHours.Text = CalculateHours(txtShiftStart.Text, txtShiftEnd.Text)
        CalculateCost(CalculateMins(txtShiftStart.Text, txtShiftEnd.Text), CDec(txtACost.Tag))
    End Sub

    Private Sub CalculateCost(ByVal Minutes As Double, ByVal Rate As Double)
        If Minutes > 0 Then
            Dim _Hours As Double = Minutes / 60
            txtShiftCost.Text = Format(_Hours * Rate, "0.00")
        Else
            txtShiftCost.Text = "0.00"
        End If
    End Sub

    Private Function CalculateHours(ByVal StartTime As String, ByVal FinishTime As String) As String
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return "0.00"
        Else
            Dim _Hours As Double = _Mins / 60
            Return Format(_Hours, "0.00")
        End If
    End Function

    Private Function CalculateHours(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return 0
        Else
            Return CDec(_Mins / 60)
        End If
    End Function

    Private Function CalculateMins(ByVal StartTime As String, ByVal FinishTime As String) As Double

        If StartTime = "" OrElse FinishTime = "" Then Return 0

        Dim _s As TimeSpan? = HHMMToTimeSpan(StartTime)
        Dim _f As TimeSpan? = HHMMToTimeSpan(FinishTime)

        If Not _s.HasValue OrElse Not _f.HasValue Then Return 0

        Return CalculateMins(_s.Value, _f.Value)

    End Function

    Private Function CalculateMins(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        If Not StartTime.HasValue OrElse Not FinishTime.HasValue Then Return 0
        Dim _Diff As TimeSpan = FinishTime.Value - StartTime.Value
        Return _Diff.TotalMinutes
    End Function

    Private Function HHMMToTimeSpan(ByVal HHMM As String) As TimeSpan?

        If HHMM = "" Then Return Nothing

        If HHMM.Contains(":") Then
            Dim _HS As String = HHMM.Substring(0, HHMM.IndexOf(":"))
            Dim _MS As String = HHMM.Substring(HHMM.IndexOf(":") + 1)
            Dim _TS As New TimeSpan(CInt(_HS), CInt(_MS), 0)
            Return _TS
        Else
            Return Nothing
        End If

    End Function

    Private Sub txtShiftStart_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtShiftStart.Validating

    End Sub
End Class
