﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDAPDetail
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtStamp = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtLetter = New Care.Controls.CareTextBox(Me.components)
        Me.txtStep = New Care.Controls.CareTextBox(Me.components)
        Me.txtArea = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtStaff = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox8 = New Care.Controls.CareFrame()
        Me.txtDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareComboBox1 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareComboBox2 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLetter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CareComboBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareComboBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtStamp)
        Me.GroupBox1.Controls.Add(Me.CareLabel5)
        Me.GroupBox1.Controls.Add(Me.txtLetter)
        Me.GroupBox1.Controls.Add(Me.txtStep)
        Me.GroupBox1.Controls.Add(Me.txtArea)
        Me.GroupBox1.Controls.Add(Me.CareLabel4)
        Me.GroupBox1.Controls.Add(Me.txtStaff)
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.CareLabel2)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(672, 118)
        Me.GroupBox1.TabIndex = 0
        '
        'txtStamp
        '
        Me.txtStamp.CharacterCasing = CharacterCasing.Normal
        Me.txtStamp.EnterMoveNextControl = True
        Me.txtStamp.Location = New System.Drawing.Point(484, 86)
        Me.txtStamp.MaxLength = 100
        Me.txtStamp.Name = "txtStamp"
        Me.txtStamp.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStamp.Properties.AccessibleName = "Name"
        Me.txtStamp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStamp.Properties.Appearance.Options.UseFont = True
        Me.txtStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStamp.Properties.MaxLength = 100
        Me.txtStamp.Properties.ReadOnly = True
        Me.txtStamp.ReadOnly = True
        Me.txtStamp.Size = New System.Drawing.Size(172, 22)
        Me.txtStamp.TabIndex = 5
        Me.txtStamp.Tag = ""
        Me.txtStamp.TextAlign = HorizontalAlignment.Left
        Me.txtStamp.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(455, 89)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel5.TabIndex = 31
        Me.CareLabel5.Text = "Date"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLetter
        '
        Me.txtLetter.CharacterCasing = CharacterCasing.Normal
        Me.txtLetter.EnterMoveNextControl = True
        Me.txtLetter.Location = New System.Drawing.Point(601, 30)
        Me.txtLetter.MaxLength = 100
        Me.txtLetter.Name = "txtLetter"
        Me.txtLetter.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLetter.Properties.AccessibleName = "Name"
        Me.txtLetter.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLetter.Properties.Appearance.Options.UseFont = True
        Me.txtLetter.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLetter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLetter.Properties.MaxLength = 100
        Me.txtLetter.Properties.ReadOnly = True
        Me.txtLetter.ReadOnly = True
        Me.txtLetter.Size = New System.Drawing.Size(55, 22)
        Me.txtLetter.TabIndex = 2
        Me.txtLetter.Tag = ""
        Me.txtLetter.TextAlign = HorizontalAlignment.Left
        Me.txtLetter.ToolTipText = ""
        '
        'txtStep
        '
        Me.txtStep.CharacterCasing = CharacterCasing.Normal
        Me.txtStep.EnterMoveNextControl = True
        Me.txtStep.Location = New System.Drawing.Point(484, 30)
        Me.txtStep.MaxLength = 100
        Me.txtStep.Name = "txtStep"
        Me.txtStep.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStep.Properties.AccessibleName = "Name"
        Me.txtStep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStep.Properties.Appearance.Options.UseFont = True
        Me.txtStep.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStep.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStep.Properties.MaxLength = 100
        Me.txtStep.Properties.ReadOnly = True
        Me.txtStep.ReadOnly = True
        Me.txtStep.Size = New System.Drawing.Size(65, 22)
        Me.txtStep.TabIndex = 1
        Me.txtStep.Tag = ""
        Me.txtStep.TextAlign = HorizontalAlignment.Left
        Me.txtStep.ToolTipText = ""
        '
        'txtArea
        '
        Me.txtArea.CharacterCasing = CharacterCasing.Normal
        Me.txtArea.EnterMoveNextControl = True
        Me.txtArea.Location = New System.Drawing.Point(126, 30)
        Me.txtArea.MaxLength = 100
        Me.txtArea.Name = "txtArea"
        Me.txtArea.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtArea.Properties.AccessibleName = "Name"
        Me.txtArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtArea.Properties.Appearance.Options.UseFont = True
        Me.txtArea.Properties.Appearance.Options.UseTextOptions = True
        Me.txtArea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtArea.Properties.MaxLength = 100
        Me.txtArea.Properties.ReadOnly = True
        Me.txtArea.ReadOnly = True
        Me.txtArea.Size = New System.Drawing.Size(306, 22)
        Me.txtArea.TabIndex = 0
        Me.txtArea.Tag = ""
        Me.txtArea.TextAlign = HorizontalAlignment.Left
        Me.txtArea.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(13, 89)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel4.TabIndex = 27
        Me.CareLabel4.Text = "Staff"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStaff
        '
        Me.txtStaff.CharacterCasing = CharacterCasing.Normal
        Me.txtStaff.EnterMoveNextControl = True
        Me.txtStaff.Location = New System.Drawing.Point(126, 86)
        Me.txtStaff.MaxLength = 100
        Me.txtStaff.Name = "txtStaff"
        Me.txtStaff.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStaff.Properties.AccessibleName = "Name"
        Me.txtStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStaff.Properties.Appearance.Options.UseFont = True
        Me.txtStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStaff.Properties.MaxLength = 100
        Me.txtStaff.Properties.ReadOnly = True
        Me.txtStaff.ReadOnly = True
        Me.txtStaff.Size = New System.Drawing.Size(306, 22)
        Me.txtStaff.TabIndex = 4
        Me.txtStaff.Tag = ""
        Me.txtStaff.TextAlign = HorizontalAlignment.Left
        Me.txtStaff.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(565, 33)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel3.TabIndex = 25
        Me.CareLabel3.Text = "Letter"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(13, 61)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 23
        Me.CareLabel2.Text = "Name"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(126, 58)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.Properties.ReadOnly = True
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(530, 22)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = ""
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(455, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel1.TabIndex = 21
        Me.CareLabel1.Text = "Step"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(6, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(13, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 15)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Developmental Area"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.txtDescription)
        Me.GroupBox8.Location = New System.Drawing.Point(12, 136)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(672, 160)
        Me.GroupBox8.TabIndex = 1
        Me.GroupBox8.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtDescription.Location = New System.Drawing.Point(12, 28)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtDescription, True)
        Me.txtDescription.Size = New System.Drawing.Size(648, 123)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtDescription, OptionsSpelling1)
        Me.txtDescription.TabIndex = 0
        Me.txtDescription.Tag = ""
        Me.txtDescription.UseOptimizedRendering = True
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtComments)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 302)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(672, 132)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(12, 28)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(648, 94)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling2)
        Me.txtComments.TabIndex = 0
        Me.txtComments.Tag = "AE"
        Me.txtComments.UseOptimizedRendering = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(809, 443)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(80, 25)
        Me.btnOK.TabIndex = 4
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(895, 443)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(80, 25)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Tag = "Y"
        Me.btnCancel.Text = "Cancel"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.CareComboBox1)
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.CareComboBox2)
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Location = New System.Drawing.Point(690, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(285, 422)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Photo"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(565, 33)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel6.TabIndex = 25
        Me.CareLabel6.Text = "Letter"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareComboBox1
        '
        Me.CareComboBox1.AllowBlank = False
        Me.CareComboBox1.DataSource = Nothing
        Me.CareComboBox1.DisplayMember = Nothing
        Me.CareComboBox1.EnterMoveNextControl = True
        Me.CareComboBox1.Location = New System.Drawing.Point(601, 30)
        Me.CareComboBox1.Name = "CareComboBox1"
        Me.CareComboBox1.Properties.AccessibleName = "Letter"
        Me.CareComboBox1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.CareComboBox1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareComboBox1.Properties.Appearance.Options.UseFont = True
        Me.CareComboBox1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CareComboBox1.Properties.ReadOnly = True
        Me.CareComboBox1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.CareComboBox1.ReadOnly = True
        Me.CareComboBox1.SelectedValue = Nothing
        Me.CareComboBox1.Size = New System.Drawing.Size(55, 22)
        Me.CareComboBox1.TabIndex = 2
        Me.CareComboBox1.Tag = ""
        Me.CareComboBox1.ValueMember = Nothing
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(455, 33)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel8.TabIndex = 21
        Me.CareLabel8.Text = "Step"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareComboBox2
        '
        Me.CareComboBox2.AllowBlank = False
        Me.CareComboBox2.DataSource = Nothing
        Me.CareComboBox2.DisplayMember = Nothing
        Me.CareComboBox2.EnterMoveNextControl = True
        Me.CareComboBox2.Location = New System.Drawing.Point(484, 30)
        Me.CareComboBox2.Name = "CareComboBox2"
        Me.CareComboBox2.Properties.AccessibleName = "Step"
        Me.CareComboBox2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.CareComboBox2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareComboBox2.Properties.Appearance.Options.UseFont = True
        Me.CareComboBox2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CareComboBox2.Properties.ReadOnly = True
        Me.CareComboBox2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.CareComboBox2.ReadOnly = True
        Me.CareComboBox2.SelectedValue = Nothing
        Me.CareComboBox2.Size = New System.Drawing.Size(55, 22)
        Me.CareComboBox2.TabIndex = 1
        Me.CareComboBox2.Tag = ""
        Me.CareComboBox2.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(6, 131)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(0, 15)
        Me.CareLabel9.TabIndex = 4
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCDAPDetail
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(987, 476)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox8)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCDAPDetail"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Observation Detail"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLetter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.CareComboBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareComboBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtStaff As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents txtDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareComboBox1 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareComboBox2 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtLetter As Care.Controls.CareTextBox
    Friend WithEvents txtStep As Care.Controls.CareTextBox
    Friend WithEvents txtArea As Care.Controls.CareTextBox
    Friend WithEvents txtStamp As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
End Class
