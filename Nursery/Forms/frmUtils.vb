﻿'OPTION STRICT OFF - ADDED BY JAMES AS UTILITIES ARE ALLOWED TO BE A BIT DIRTY
'OPTION STRICT SHOULD BE ON AT A PROJECT LEVEL
Option Strict Off

Imports Care.Global
Imports Care.Data

Public Class frmUtils

    Private Enum EnumMealType
        Snack
        Lunch
        Tea
    End Enum

    Private Function Prompt() As Boolean
        If InputBox("Enter Password:", "Run Utility") = "care122126" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        If Not Prompt() Then Exit Sub

        Dim _Records As List(Of Business.Invoice) = Business.Invoice.RetreiveAll
        For Each _r As Business.Invoice In _Records

            Dim _Block As String = ""
            If _r._Add1.Trim <> "" Then _Block += _r._Add1.Trim + vbNewLine
            If _r._Add2.Trim <> "" Then _Block += _r._Add2.Trim + vbNewLine
            If _r._Add3.Trim <> "" Then _Block += _r._Add3.Trim + vbNewLine
            If _r._Add4.Trim <> "" Then _Block += _r._Add4.Trim + vbNewLine
            If _r._Postcode.Trim <> "" Then _Block += _r._Postcode.Trim

            _Block = _Block.Replace(",", " ")
            _Block = _Block.Replace("  ", " ")

            _r._Address = _Block
            _r.Store()

        Next

        CareMessage("Done!")

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click

        If Not Prompt() Then Exit Sub

        Dim _Records As List(Of Business.Contact) = Business.Contact.RetreiveAll
        For Each _r As Business.Contact In _Records

            Dim _Block As String = ""
            If _r._Add1.Trim <> "" Then _Block += _r._Add1.Trim + vbNewLine
            If _r._Add2.Trim <> "" Then _Block += _r._Add2.Trim + vbNewLine
            If _r._Add3.Trim <> "" Then _Block += _r._Add3.Trim + vbNewLine
            If _r._Add4.Trim <> "" Then _Block += _r._Add4.Trim + vbNewLine
            If _r._Postcode.Trim <> "" Then _Block += _r._Postcode.Trim

            _Block = _Block.Replace(",", " ")
            _Block = _Block.Replace("  ", " ")

            _r._Address = _Block
            _r.Store()

        Next

        CareMessage("Done!")

    End Sub

    Private Sub CareButton1_Click(sender As System.Object, e As System.EventArgs) Handles CareButton1.Click

        If Not Prompt() Then Exit Sub

        Dim _Records As List(Of Business.Staff) = Business.Staff.RetreiveAll
        For Each _r As Business.Staff In _Records

            Dim _Block As String = ""
            If _r._Add1.Trim <> "" Then _Block += _r._Add1.Trim + vbNewLine
            If _r._Add2.Trim <> "" Then _Block += _r._Add2.Trim + vbNewLine
            If _r._Add3.Trim <> "" Then _Block += _r._Add3.Trim + vbNewLine
            If _r._Add4.Trim <> "" Then _Block += _r._Add4.Trim + vbNewLine
            If _r._Postcode.Trim <> "" Then _Block += _r._Postcode.Trim

            _Block = _Block.Replace(",", " ")
            _Block = _Block.Replace("  ", " ")

            _r._Address = _Block
            _r.Store()

        Next

        CareMessage("Done!")

    End Sub

    Private Sub CareButton5_Click(sender As Object, e As EventArgs) Handles CareButton5.Click

        If Not Prompt() Then Exit Sub

        FixToiletRecords("WET")
        FixToiletRecords("WETNAP")
        FixToiletRecords("SOIL")
        FixToiletRecords("SOILNAP")

        Session.HideProgressBar()

    End Sub

    Private Sub FixToiletRecords(ByVal ActivityType As String)

        Dim _SQL As String = "select id from Activity where type = '" & ActivityType & "'"
        Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _dt IsNot Nothing Then

            Session.SetupProgressBar("Correcting " + ActivityType + "...", _dt.Rows.Count)

            Dim _Desc As String = ""
            Dim _Time As String = ""

            If ActivityType = "WET" Then _Desc = "Wet"
            If ActivityType = "WETNAP" Then _Desc = "Wet"
            If ActivityType = "SOIL" Then _Desc = "Soiled"
            If ActivityType = "SOILNAP" Then _Desc = "Soiled"

            For Each _dr As DataRow In _dt.Rows

                Dim _A As Business.Activity = Business.Activity.RetreiveByID(New Guid(_dr.Item("id").ToString))

                _Time = Format(_A._Stamp, "HH:mm")

                _A._Type = "TOILET"
                _A._Value1 = ActivityType.Replace("NAP", "")

                If ActivityType.Contains("NAP") Then
                    _A._Value2 = "NAP"
                Else
                    _A._Value2 = ""
                End If

                _A._Description = _Desc + " @ " + _Time + " by " + _A._StaffName

                _A.Store()
                Session.StepProgressBar()

            Next

        End If

    End Sub

    Private Sub AddDrinkToMeals(ByVal MealType As EnumMealType)

        Dim _SQL As String = ""
        Dim _DrinkToAdd As String = "Juice / Water"
        Dim _DrinkID As String = ""

        'find the drink
        _SQL = "select ID from Food where name = '" + _DrinkToAdd + "'"
        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then

            _DrinkID = _DR.Item("ID").ToString

            Dim _MealField As String = ""
            If MealType = EnumMealType.Snack Then _MealField = "snack"
            If MealType = EnumMealType.Lunch Then _MealField = "lunch"
            If MealType = EnumMealType.Tea Then _MealField = "tea"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id from Meals where " + _MealField + " = 1")
            If _DT IsNot Nothing Then

                For Each _MealRow As DataRow In _DT.Rows

                    Dim _Meal As Business.Meal = Business.Meal.RetreiveByID(New Guid(_MealRow.Item("id").ToString))
                    If _Meal IsNot Nothing Then

                        'ensure the drink has not been added already
                        If Not _Meal.ContainsComponent(New Guid(_DrinkID)) Then

                            Dim _Component As New Business.MealComponent
                            _Component._ID = Guid.NewGuid
                            _Component._MealId = _Meal._ID
                            _Component._FoodId = New Guid(_DrinkID)
                            _Component._FoodName = _DrinkToAdd
                            _Component._FoodQty = 1

                            Business.MealComponent.SaveMealComponent(_Component)

                        End If

                    End If

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End If

        CareMessage("DONE")

    End Sub

    Private Sub CareButton2_Click(sender As Object, e As EventArgs) Handles CareButton2.Click
        If Not Prompt() Then Exit Sub
        AddDrinkToMeals(EnumMealType.Snack)
    End Sub

    Private Sub CareButton3_Click(sender As Object, e As EventArgs) Handles CareButton3.Click
        If Not Prompt() Then Exit Sub
        AddDrinkToMeals(EnumMealType.Lunch)
    End Sub

    Private Sub CareButton4_Click(sender As Object, e As EventArgs) Handles CareButton4.Click
        If Not Prompt() Then Exit Sub
        AddDrinkToMeals(EnumMealType.Tea)
    End Sub

    Private Sub CareButton6_Click(sender As Object, e As EventArgs) Handles CareButton6.Click
        If Not Prompt() Then Exit Sub
        FixSessions()
    End Sub

    Private Sub FixSessions()

        Dim _Sessions As List(Of Business.ChildAdvanceSession) = Business.ChildAdvanceSession.RetreiveAll

        For Each _S In _Sessions

            _S._Mode = "Weekly"

            _S._MondayDesc = TariffDesc(_S._Monday)
            _S._Monday1 = 0
            _S._Monday2 = 0

            _S._TuesdayDesc = TariffDesc(_S._Tuesday)
            _S._Tuesday1 = 0
            _S._Tuesday2 = 0

            _S._WednesdayDesc = TariffDesc(_S._Wednesday)
            _S._Wednesday1 = 0
            _S._Wednesday2 = 0

            _S._ThursdayDesc = TariffDesc(_S._Thursday)
            _S._Thursday1 = 0
            _S._Thursday2 = 0

            _S._FridayDesc = TariffDesc(_S._Friday)
            _S._Friday1 = 0
            _S._Friday2 = 0

            _S._SaturdayDesc = TariffDesc(_S._Saturday)
            _S._Saturday1 = 0
            _S._Saturday2 = 0

            _S._SundayDesc = TariffDesc(_S._Sunday)
            _S._Sunday1 = 0
            _S._Sunday2 = 0

            _S.Store()

        Next

        _Sessions = Nothing

        CareMessage("DONE!")

    End Sub

    Private Function TariffDesc(ByVal ID As Guid?) As String

        If Not ID.HasValue Then Return ""

        Dim _T As Business.Tariff = Business.Tariff.RetreiveByID(ID.Value)
        If _T IsNot Nothing Then
            Return _T._Name
        Else
            Return ""
        End If

    End Function

    'Private Function ReturnDailySession(ByVal ChildID As Guid, ByVal WeekDay As Business.ChildAdvanceSession.EnumWeekDay, ByVal FromDate As Date) As Boolean
    '    Dim _S As Business.ChildAdvanceSession = Business.ChildAdvanceSession.RetrieveRecurringBooking(ChildID.ToString, FromDate, Business.ChildAdvanceSession.EnumMode.Daily, WeekDay)
    '    If _S Is Nothing Then
    '        Return False
    '    Else
    '        _S = Nothing
    '        Return True
    '    End If
    'End Function

    Private Sub ImportConnect()

        Dim _ExtConn As String = "Data Source=LOCALHOST;Initial Catalog=CONNECT-WOODPECKERS;User Id=careuser;Password=careuser;"

        ''If InputBox("THIS WILL DELETE ALL DATA! - ENTER PASSWORD:", "Import External DB") <> "care122126" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "DELETE FROM CHILDREN"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "DELETE FROM CONTACTS"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "DELETE FROM FAMILY"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "SELECT * FROM ChildList order by surname, address1"
        Dim _Children As DataTable = DAL.GetDataTablefromSQL(_ExtConn, _SQL)

        If _Children IsNot Nothing Then

            Session.SetupProgressBar("Importing Records", _Children.Rows.Count)

            For Each _C As DataRow In _Children.Rows

                'check if we already have a family created with this surname and address
                Dim _FamilyID As String = CheckFamily(_C.Item("surname").ToString.Trim, _C.Item("address1").ToString.Trim)

                If _FamilyID = "" Then
                    Dim _F As New Business.Family
                    _F._Surname = _C.Item("surname").ToString.Trim
                    _F._LetterName = _C.Item("address1").ToString.Trim
                    _F.Store()
                    _FamilyID = _F._ID.ToString
                End If

                'import this child
                Dim _Child As New Business.Child
                With _Child

                    ._Status = "On Waiting List"
                    If _C.Item("childstatus").ToString.Trim = "Inactive" Then ._Status = "Archived"
                    ._Notes = _C.Item("ChildReference").ToString.Trim

                    ._Forename = _C.Item("forename").ToString.Trim
                    ._Surname = _C.Item("surname").ToString.Trim
                    ._Fullname = ._Forename + " " + ._Surname
                    ._Dob = ValueHandler.ConvertDate(_C.Item("dob"))
                    ._Gender = _C.Item("gender").ToString.First.ToString
                    ._FamilyId = New Guid(_FamilyID)
                    ._Surname = _C.Item("surname").ToString.Trim

                    ._Started = ValueHandler.ConvertDate(_C.Item("startdate"))
                    ._GroupStarted = ValueHandler.ConvertDate(_C.Item("startdate"))

                    ._KeyworkerId = New Guid("68A7973B-5093-48D4-8175-A36070D9CF51")
                    ._KeyworkerName = "No Keyworker"

                    Select Case _C.Item("site").ToString.Trim

                        Case "Woodpeckers HPS"
                            ._GroupId = New Guid("613CB37A-4EF6-4026-A9DA-A2CBA80D71AA")
                            ._GroupName = "Holiday Play Scheme"

                        Case "Gomer Schools BASC (GOMER)"
                            ._GroupId = New Guid("9A2D34BB-01D3-41CF-A73D-C64A54C9E7D7")
                            ._GroupName = "Gomer"

                        Case "Elson Schools BASC (ELSON)"
                            ._GroupId = New Guid("CA71DB21-6716-49DE-B19F-56B4103440B6")
                            ._GroupName = "Elson"

                        Case "Lee-on-the-Solent BASC (LoS)"
                            ._GroupId = New Guid("6D20F933-72AA-49F3-BE3B-35DF1F92AC14")
                            ._GroupName = "Lee-on-the-Solent"

                        Case "Leesland Schools BASC (Leesland)"
                            ._GroupId = New Guid("B5FF26BA-D13C-48BC-A66C-E7E7C76BA068")
                            ._GroupName = "Leesland"

                    End Select

                    ._MedAllergies = _C.Item("Details of any special dietary requirements, allergies and or si").ToString.Trim
                    If ._MedAllergies.ToLower = "no" Then ._MedAllergies = ""
                    If ._MedAllergies.ToLower = "none" Then ._MedAllergies = ""
                    If ._MedAllergies <> "" Then ._AllergyRating = "Moderate"

                    ._MedMedication = _C.Item("Details of any significant health issues").ToString.Trim
                    If ._MedMedication.ToLower = "no" Then ._MedMedication = ""
                    If ._MedMedication.ToLower = "none" Then ._MedMedication = ""

                    ._MedNotes = _C.Item("Record of immunisations").ToString.Trim

                End With

                'import the contacts
                ImportContacts(_ExtConn, _C.Item("ChildReference").ToString.Trim, _FamilyID)

                Dim _FamilySurname As String = ""
                SetFamily(_ExtConn, _FamilyID, _FamilySurname)

                _Child._FamilyName = _FamilySurname
                _Child.Store()

                Session.StepProgressBar()

            Next

        End If

    End Sub

    Private Sub CareButton7_Click(sender As Object, e As EventArgs) Handles CareButton7.Click

        If Not Prompt() Then Exit Sub

        ImportLibraCura()

    End Sub

    Private Sub SetFamily(ByVal ExternalConn As String, ByVal FamilyID As String, ByRef FamilySurname As String)

        Dim _Contacts As List(Of Business.Contact) = Business.Contact.RetreiveByFamilyID(FamilyID)
        For Each _c In _Contacts

            Dim _GotBillPayer As Boolean = False

            Dim _DR As DataRow = Nothing

            'try email address
            _GotBillPayer = FetchBillPayer(_DR, ExternalConn, "Bill Email", _c._Email)

            If Not _GotBillPayer Then
                'ok, try mobile number
                _GotBillPayer = FetchBillPayer(_DR, ExternalConn, "Bill Mob#", _c._TelMobile)
            End If

            If Not _GotBillPayer Then
                'ok, try home number
                _GotBillPayer = FetchBillPayer(_DR, ExternalConn, "Bill Tel#", _c._TelHome)
            End If

            If Not _GotBillPayer Then
                'ok, try work number
                _GotBillPayer = FetchBillPayer(_DR, ExternalConn, "Bill Work", _c._JobTel)
            End If

            If _GotBillPayer Then

                Dim _F As Business.Family = Business.Family.RetreiveByID(New Guid(FamilyID))
                With _F
                    ._LetterName = _DR.Item("Bill Name").ToString
                    ._Surname = _DR.Item("Surname").ToString
                    ._Address = _DR.Item("Bill Address").ToString
                    ._Postcode = _DR.Item("Bill Postcode").ToString
                    ._eInvoicing = True
                    .Store()
                End With

                _c._PrimaryCont = True
                _c._Forename = _DR.Item("Forename").ToString
                _c._Surname = _DR.Item("Surname").ToString
                _c.Store()

                FamilySurname = _c._Surname

            End If

        Next

    End Sub

    Private Function FetchBillPayer(ByRef DataRowReturned As DataRow, ByVal ExtConn As String, ByVal FieldName As String, ByVal FieldValue As String) As Boolean

        If FieldValue = "" Then Return False

        Dim _SQL As String = "select * from BillPayers where lower(replace([" + FieldName + "],' ','')) = '" + FieldValue.ToLower.Replace(" ", "") + "'"
        DataRowReturned = DAL.GetRowfromSQL(ExtConn, _SQL)
        If DataRowReturned Is Nothing Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub ImportContacts(ByVal ExtConn As String, ByVal ExtRef As String, ByVal FamilyID As String)

        Dim _SQL As String = "select * from ChildContacts where rtrim(childRef) = '" + ExtRef + "'"

        Dim _Contacts As DataTable = DAL.GetDataTablefromSQL(ExtConn, _SQL)
        If _Contacts IsNot Nothing Then

            For Each _C As DataRow In _Contacts.Rows

                Dim _Contact As New Business.Contact
                With _Contact

                    ._FamilyId = New Guid(FamilyID)

                    ._Fullname = _C.Item("contactname").ToString
                    ._Dob = ValueHandler.ConvertDate(_C.Item("dob"))
                    ._Relationship = _C.Item("contacttype").ToString

                    ._TelHome = _C.Item("contacttelhome").ToString
                    ._TelMobile = _C.Item("contacttelmob").ToString
                    ._Email = _C.Item("contactemail").ToString

                    ._JobEmployer = _C.Item("contactplaceofwork").ToString
                    ._JobTel = _C.Item("contacttelwork").ToString

                    ._Add1 = _C.Item("contactadd1").ToString
                    ._Add2 = _C.Item("contactadd2").ToString
                    ._Add3 = _C.Item("contactadd3").ToString
                    ._Add4 = _C.Item("contactadd4").ToString
                    ._Postcode = _C.Item("contactpostcode").ToString

                    .Store()

                End With

            Next

        End If

    End Sub

    Private Function CheckFamily(ByVal Surname As String, ByVal Address1 As String) As String

        Dim _SQL As String = "select ID from Family"
        _SQL += " where " + AppendField("surname", Surname)
        _SQL += " and " + AppendField("letter_name", Address1)

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

        If _DR IsNot Nothing Then
            Return _DR.Item("ID").ToString
        Else
            Return ""
        End If

    End Function

    Private Function AppendField(ByVal FieldName As String, FieldValue As String) As String
        Dim _Quote As String = "'"
        If FieldValue.Contains("'") Then FieldValue = FieldValue.Replace("'", "''")
        Return FieldName + " = '" + FieldValue + "'"
    End Function

    Private Sub CareButton8_Click(sender As Object, e As EventArgs) Handles CareButton8.Click

        If Not Prompt() Then Exit Sub

        Dim _Count As Integer = 0
        For Each _C As Business.ChildCalculation In Business.ChildCalculation.RetreiveAll
            _C.BuildInvoiceText(Invoicing.ReturnSplitInto(_C._SplitType))
            _C.Store()
            _Count += 1
        Next

        CareMessage("Processed " + _Count.ToString + " records.", MessageBoxIcon.Information, "")

    End Sub

    Private Sub CareButton9_Click(sender As Object, e As EventArgs)

        If Not Prompt() Then Exit Sub

        Dim _SQL As String = "delete from ChildCalculation"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        For Each _c As Business.ChildAdvanceSession In Business.ChildAdvanceSession.RetreiveAll

            Dim _Rate1 As Decimal = 0
            Dim _Rate2 As Decimal = 0
            Dim _Rate3 As Decimal = 0
            Dim _Rate4 As Decimal = 0
            Dim _Rate5 As Decimal = 0

            Dim _Count1 As Integer = 0
            Dim _Count2 As Integer = 0
            Dim _Count3 As Integer = 0
            Dim _Count4 As Integer = 0
            Dim _Count5 As Integer = 0

            If _c._Monday.HasValue Then
                _Rate1 = GetRate(_c._Monday.Value)
                _Count1 = 1
            End If

            If _c._Tuesday.HasValue Then
                Dim _r As Decimal = GetRate(_c._Tuesday.Value)
                If _r = _Rate1 Then
                    _Count1 += 1
                Else
                    _Count2 += 1
                End If
            End If

            If _c._Wednesday.HasValue Then
                Dim _r As Decimal = GetRate(_c._Wednesday.Value)
                If _r = _Rate1 Then
                    _Count1 += 1
                Else
                    If _r = _Rate2 Then
                        _Count2 += 1
                    Else
                        _Count3 += 1
                    End If
                End If
            End If

            If _c._Thursday.HasValue Then
                Dim _r As Decimal = GetRate(_c._Thursday.Value)
                If _r = _Rate1 Then
                    _Count1 += 1
                Else
                    If _r = _Rate2 Then
                        _Count2 += 1
                    Else
                        If _r = _Rate3 Then
                            _Count3 += 1
                        Else
                            _Count4 += 1
                        End If
                    End If
                End If
            End If

            If _c._Friday.HasValue Then
                Dim _r As Decimal = GetRate(_c._Thursday)
                If _r = _Rate1 Then
                    _Count1 += 1
                Else
                    If _r = _Rate2 Then
                        _Count2 += 1
                    Else
                        If _r = _Rate3 Then
                            _Count3 += 1
                        Else
                            If _r = _Rate4 Then
                                _Count4 += 1
                            Else
                                _Count5 += 1
                            End If
                        End If
                    End If
                End If
            End If

        Next

    End Sub

    Private Function GetRate(ByVal TariffID As Guid?) As Decimal
        Return 0
    End Function

    Private Sub CareButton9_Click_1(sender As Object, e As EventArgs) Handles CareButton9.Click

        If Not Prompt() Then Exit Sub

        Dim _OffSet As Integer = 0
        For Each _g In Business.Group.RetreiveAllByMinimumAge

            Dim _DOB As Date
            Dim _Months As Integer = _g._AgeMin
            If _Months = 0 Then _Months = _g._AgeMax

            _Months -= 12
            _Months = _Months * -1

            _DOB = DateAdd(DateInterval.Month, _Months, Today)
            _DOB = _DOB.AddDays(_OffSet)

            For Each _c As Business.Child In Business.Child.RetreiveByGroupID(_g._ID.Value)

                _c._Dob = _DOB
                _c._Started = _DOB.AddDays(28)
                _c.Store()

                _DOB = _DOB.AddDays(14)

            Next

            _OffSet = 7

        Next

        CareMessage("Done")

    End Sub

    Private Sub CareButton10_Click(sender As Object, e As EventArgs) Handles CareButton10.Click

        If Not Prompt() Then Exit Sub

        Dim _S As New Business.StartOfDay
        _S.Run()

        CareMessage("Done")

    End Sub

    Private Sub CareButton11_Click(sender As Object, e As EventArgs) Handles CareButton11.Click

        'If Not Prompt Then Exit Sub

        'Dim _SQL As String = ""

        '_SQL += "select r.date, r.type, r.person_id, r.name,"
        '_SQL += " (select top 1 stamp_in from Register i where i.date = r.date and i.person_id = r.person_id and i.in_out = 'I' order by i.stamp_in) as 'in',"
        '_SQL += " (select top 1 stamp_out from Register o where o.date = r.date and o.person_id = r.person_id and o.in_out = 'O' order by o.stamp_out desc) as 'out'"
        '_SQL += " from Register r"
        '_SQL += " where r.date not in (select rs.date from RegisterSummary rs where rs.date = r.date and rs.person_id = r.person_id)"
        '_SQL += " group by r.date, r.type, r.person_id, r.name"
        '_SQL += " order by r.date"

        'Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DT IsNot Nothing Then

        '    Session.SetupProgressBar("Processing...", _DT.Rows.Count)

        '    For Each _DR As DataRow In _DT.Rows

        '        Dim _Date As Date = ValueHandler.ConvertDate(_DR.Item("date"))

        '        Dim _D As Business.Day = Business.Day.RetreiveByDate(_Date)
        '        If _D IsNot Nothing Then

        '            Dim _RS As New Business.RegisterSummary
        '            With _RS

        '                ._DayId = _D._ID
        '                ._Date = _Date

        '                ._PersonId = New Guid(_DR.Item("person_id").ToString)
        '                ._PersonName = _DR.Item("name").ToString
        '                ._PersonType = _DR.Item("type").ToString

        '                Dim _In As Date? = ValueHandler.ConvertDate(_DR.Item("in"))
        '                Dim _Out As Date? = ValueHandler.ConvertDate(_DR.Item("out"))

        '                If _In.HasValue AndAlso _Out.HasValue Then
        '                    ._InOut = "O"
        '                    ._Duration = DateDiff(DateInterval.Minute, _In.Value, _Out.Value)
        '                Else
        '                    ._InOut = "I"
        '                    ._Duration = 0
        '                End If

        '                ._FirstStamp = _In
        '                ._LastStamp = _Out

        '                ._Location = "*UTIL*"

        '                .Store()

        '            End With

        '        End If

        '        Session.StepProgressBar()

        '    Next

        '    Session.HideProgressBar()

        '    _DT.Dispose()
        '    _DT = Nothing

        '    CareMessage("Done")

        'End If

    End Sub

    Private Sub CareButton12_Click(sender As Object, e As EventArgs) Handles CareButton12.Click

        If Not Prompt() Then Exit Sub

        Dim _Children As List(Of Business.Child) = Business.Child.RetreiveAll

        Session.SetupProgressBar("Processing Children", _Children.Count)

        For Each _C As Business.Child In _Children

            If _C._ConsCalpol Then CreateConsent(_C._ID, "Calpol")
            If _C._ConsOffsite Then CreateConsent(_C._ID, "Outings")
            If _C._ConsOutdoor Then CreateConsent(_C._ID, "Outdoor Play")
            If _C._ConsPhoto Then CreateConsent(_C._ID, "Photographs")
            If _C._ConsPlasters Then CreateConsent(_C._ID, "Plasters")
            If _C._ConsSuncream Then CreateConsent(_C._ID, "Sun Cream")

            Session.StepProgressBar()

        Next

        Session.HideProgressBar()

        CareMessage("Done")

    End Sub

    Private Sub CreateConsent(ByVal ChildID As Guid?, ByVal Consent As String)

        Dim _CC As New Business.ChildConsent
        _CC._ChildId = ChildID
        _CC._Consent = Consent
        _CC._Given = True
        _CC.Store()

    End Sub

    Private Sub CareButton13_Click(sender As Object, e As EventArgs) Handles CareButton13.Click

        If Not Prompt() Then Exit Sub

        Dim _Batches As List(Of Business.InvoiceBatch) = Business.InvoiceBatch.RetreiveAll

        Session.SetupProgressBar("Processing Batches", _Batches.Count)

        For Each _B As Business.InvoiceBatch In _Batches

            Dim _SQL As String = "update Invoices set additional_sessions = " + _B._AdditionalSessions.ToString + " where batch_id = '" + _B._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            Session.StepProgressBar()

        Next

        Session.HideProgressBar()

        CareMessage("Done")

    End Sub

    Private Sub FixActivity()

        If Not Prompt() Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select a.ID, g.ID as 'g_id', g.name as 'g_name', r.ID as 'r_id', r.room_name as 'r_name' from DayActivity a"
        _SQL += " left join Groups g on g.ID = a.group_id"
        _SQL += " left join SiteRooms r on r.room_name = g.name"
        _SQL += " where r.room_name <> ''"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Processing Activity", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _NewGroupID As String = _DR.Item("r_id").ToString

                Dim _x As String = "update DayActivity set group_id = '" + _NewGroupID + "' where ID = '" + _ID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _x)

                Session.StepProgressBar()

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Session.HideProgressBar()

        CareMessage("Done")

    End Sub

    Private Sub frmUtils_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub CreateMonthlyMatrixFromPatterns()

        Dim _SQL As String = "delete from ChildWeekMonth"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Dim _Children As List(Of Business.Child) = Business.Child.RetrieveLiveChildren

        'loop through each child
        For Each _C As Business.Child In _Children

            If _C._TermOnly = False Then

                'fetch the "current" booking pattern for this child
                _SQL = ""

                _SQL += "select top 1 * from ChildAdvanceSessions"
                _SQL += " where child_id = '" + _C._ID.ToString + "'"
                _SQL += " and date_from <= " + ValueHandler.SQLDate(Today, True)
                _SQL += " and mode = 'Weekly'"
                _SQL += " and recurring_scope = 1"
                _SQL += " order by date_from desc"

                Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                If _DR IsNot Nothing Then

                    Dim _TariffUsed As String = ""
                    Dim _Days As Integer = 0
                    Dim _FundedHours As Boolean = False

                    If _DR.Item("monday_desc").ToString <> "" Then
                        If _TariffUsed = "" Then _TariffUsed = _DR.Item("monday_desc").ToString
                        _Days += 1
                        If ValueHandler.ConvertDecimal(_DR.Item("monday_fund")) > 0 Then _FundedHours = True
                    End If

                    If _DR.Item("tuesday_desc").ToString <> "" Then
                        If _TariffUsed = "" Then _TariffUsed = _DR.Item("tuesday_desc").ToString
                        _Days += 1
                        If ValueHandler.ConvertDecimal(_DR.Item("tuesday_fund")) > 0 Then _FundedHours = True
                    End If

                    If _DR.Item("wednesday_desc").ToString <> "" Then
                        If _TariffUsed = "" Then _TariffUsed = _DR.Item("wednesday_desc").ToString
                        _Days += 1
                        If ValueHandler.ConvertDecimal(_DR.Item("wednesday_fund")) > 0 Then _FundedHours = True
                    End If

                    If _DR.Item("thursday_desc").ToString <> "" Then
                        If _TariffUsed = "" Then _TariffUsed = _DR.Item("thursday_desc").ToString
                        _Days += 1
                        If ValueHandler.ConvertDecimal(_DR.Item("thursday_fund")) > 0 Then _FundedHours = True
                    End If

                    If _DR.Item("friday_desc").ToString <> "" Then
                        If _TariffUsed = "" Then _TariffUsed = _DR.Item("friday_desc").ToString
                        _Days += 1
                        If ValueHandler.ConvertDecimal(_DR.Item("friday_fund")) > 0 Then _FundedHours = True
                    End If

                    If _Days > 0 Then

                        'check if the word short is used, if not we assume full day
                        Dim _TariffID As Guid? = Nothing
                        If _TariffUsed.ToUpper.Contains("SHORT") Then
                            _TariffID = GetMonthlyEquivalent(_C._SiteId, "SHORT")
                        Else
                            _TariffID = GetMonthlyEquivalent(_C._SiteId, "FULL")
                        End If

                        If _TariffID.HasValue Then

                            Dim _M As New Business.ChildWeekMonth
                            With _M

                                ._ChildId = _C._ID
                                ._DateFrom = ValueHandler.ConvertDate(_DR.Item("date_from"))
                                ._TariffId = _TariffID
                                ._Frequency = "Monthly"
                                ._DaysPerWeek = _Days

                                If _FundedHours Then
                                    ._AgeMode = "Specific Band"
                                    '._AgeId = _FundedBand
                                Else
                                    ._AgeMode = "Use DOB"
                                End If

                                .Store()

                            End With

                        End If
                    End If
                End If
            End If
        Next

    End Sub

    Private Function GetMonthlyEquivalent(ByVal SiteID As Guid?, ByVal TariffSearch As String) As Guid?

        Dim _SQL As String = ""

        _SQL += "select ID from Tariffs"
        _SQL += " where site_id = '" + SiteID.ToString + "'"
        _SQL += " and type = 'Month Matrix'"
        _SQL += " and name like '%" + TariffSearch + "%'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            Return New Guid(_DR.Item("ID").ToString)
        Else
            Return Nothing
        End If

    End Function

    Private Function GetFundedBand(ByVal SearchText As String) As Guid?

        Dim _SQL As String = "select ID from TariffAgeBands where description = '" + SearchText + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            Return New Guid(_DR.Item("ID").ToString)
        Else
            Return Nothing
        End If

    End Function

    Private Sub CareButton14_Click_1(sender As Object, e As EventArgs) Handles CareButton14.Click

    End Sub

    Private Sub ImportLibraCura()

        Dim _ExtConn As String = "Data Source=192.168.254.1;Initial Catalog=libacura;User Id=careuser;Password=careuser;"

        ''If InputBox("THIS WILL DELETE ALL DATA! - ENTER PASSWORD:", "Import External DB") <> "care122126" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "DELETE FROM CHILDREN WHERE CONS_OFFSITE = 1"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "DELETE FROM CONTACTS WHERE SMS_NAME = 'IMPORT'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "DELETE FROM FAMILY WHERE BALANCE = 999"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        'Account, Account ref, Name, Address Line 1, Address Line 2, Address Line 3, Address Line 4, Postcode
        _SQL = "SELECT * FROM adult_addresses order by account"
        Dim _Accounts As DataTable = DAL.GetDataTablefromSQL(_ExtConn, _SQL)

        If _Accounts IsNot Nothing Then

            Session.SetupProgressBar("Importing Records", _Accounts.Rows.Count)

            For Each _A As DataRow In _Accounts.Rows

                Dim _F As New Business.Family
                _F._AccountNo = _A.Item("Account").ToString
                _F._Surname = ReturnSurnameFromRef(_A.Item("Account ref").ToString)
                _F._LetterName = _A.Item("Name").ToString

                _F._Address = BuildAddress(_A.Item("Address Line 1").ToString, _A.Item("Address Line 2").ToString, _A.Item("Address Line 3").ToString, _A.Item("Address Line 4").ToString, _A.Item("Postcode").ToString)
                _F._Postcode = _A.Item("Postcode").ToString

                _F._Balance = 999 'SO WE CAN IDENTIFY OUR IMPORT

                _F._InvoiceFreq = "Monthly"
                _F._InvoiceDue = "x days from invoice date"
                _F._eInvoicing = True

                ProcessChildren(_F, _ExtConn)
                ProcessContacts(_F, _ExtConn)

                _F.Store()

                Session.StepProgressBar()

            Next

        End If

    End Sub

    Private Function BuildAddress(ByVal Add1 As String, ByVal Add2 As String, ByVal Add3 As String, ByVal Add4 As String, ByVal PostCode As String) As String

        Dim _Return As String = ""

        If Add1.Trim <> "" Then _Return += Add1.Trim

        If Add2.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add2.Trim
        End If

        If Add3.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add3.Trim
        End If

        If Add4.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add4.Trim
        End If

        If PostCode.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += PostCode.Trim
        End If

        Return _Return

    End Function

    Private Function ReturnSurnameFromRef(ByVal Ref As String) As String
        If Ref = "" Then Return ""
        If Ref.IndexOf("/") > 0 Then
            Return Ref.Substring(0, Ref.IndexOf("/"))
        Else
            Return ""
        End If
    End Function

    Private Sub ProcessChildren(ByRef Family As Business.Family, ByVal ExtConn As String)

        Dim _KeyworkerID As Guid = New Guid("C5519F17-A535-4142-80BF-65FFBEC03313")
        Dim _KeyworkerName As String = "No Keyworker"

        Dim _GroupID As Guid?
        Dim _GroupName As String = ""

        'CONS_OFFSITE

        Dim _SQL As String = ""
        _SQL += "select r.*, c.[Class collect from], b.[Birth date], b.Gender from children_registrations r"
        _SQL += " left join children_class c on c.Child = r.Child"
        _SQL += " left join children_bdaymonth b on b.Child = r.Child"
        _SQL += " where r.Account = '" + Family._AccountNo + "'"

        Dim _Children As DataTable = DAL.GetDataTablefromSQL(ExtConn, _SQL)

        If _Children IsNot Nothing Then

            For Each _DR As DataRow In _Children.Rows

                Dim _C As New Business.Child
                _C._ConsOffsite = True 'marker for import

                _C._FamilyId = Family._ID
                _C._FamilyName = Family._Surname

                _C._Forename = _DR.Item("Forename").ToString
                _C._Surname = _DR.Item("Surname").ToString

                _C._Status = "Current"
                _C._Started = DateSerial(2017, 1, 1)

                _C._KeyworkerId = _KeyworkerID
                _C._KeyworkerName = _KeyworkerName

                If _DR.Item("After School Club").ToString = "True" OrElse _DR.Item("Breakfast Club").ToString = "True" Then
                    _C._SiteName = "St Johns School"
                    _GroupID = New Guid("E94D6DD1-FCC6-49BD-9097-386F98158968")
                    _GroupName = "Club Room"
                Else
                    If _DR.Item("After School Club HW").ToString = "True" OrElse _DR.Item("Breakfast Club HW").ToString = "True" Then
                        _C._SiteName = "Heron Way School"
                        _GroupID = New Guid("0382AA3D-8AC1-4FA9-B8F7-EBC31E163B43")
                        _GroupName = "Club Room"
                    Else
                        _C._SiteName = "Holiday Club"
                        _GroupID = New Guid("4585CE41-FD85-4DEA-8CAF-D49268370651")
                        _GroupName = "Club Room"
                    End If
                End If

                _C._GroupId = _GroupID
                _C._GroupName = _GroupName

                _C._SchoolRef1 = _DR.Item("Class collect from").ToString

                Dim _DOB As String = _DR.Item("Birth date").ToString
                If _DOB IsNot Nothing Then
                    Dim _DOBDate As Date? = ValueHandler.ConvertDate(_DOB)
                    _C._Dob = _DOBDate
                End If

                _C._Gender = "U"
                If _DR.Item("Gender").ToString = "Boy" Then _C._Gender = "M"
                If _DR.Item("Gender").ToString = "Girl" Then _C._Gender = "F"

                _C.Store()

                Family._SiteId = _C._SiteId
                Family._SiteName = _C._SiteName

            Next

        End If



    End Sub

    Private Sub SplitForenameSurname(ByVal FullName As String, ByRef Forename As String, ByRef Surname As String)

        Dim _i As Integer = 1
        Dim _Names As String() = FullName.Split(CType(" ", Char()))
        For Each _Name In _Names
            If _i = 1 Then
                Forename = _Name
            Else
                If Surname = "" Then
                    Surname = _Name
                Else
                    Surname += " " + _Name
                End If
            End If
            _i += 1
        Next

    End Sub

    Private Sub ProcessContacts(ByRef Family As Business.Family, ByVal ExtConn As String)

        'store primary contact
        Dim _Primary As Business.Contact = ProcessPrimary(Family, ExtConn)

        'process all contacts

        'Account, Account ref, Order, Emergency contact, Relation to child, Telephone, Notes

        Dim _SQL As String = ""
        _SQL = "select * from accounts_emergency where Account = '" + Family._AccountNo + "' order by [Order]"
        Dim _Contacts As DataTable = DAL.GetDataTablefromSQL(ExtConn, _SQL)

        If _Contacts IsNot Nothing Then

            For Each _DR As DataRow In _Contacts.Rows

                If _Primary IsNot Nothing AndAlso _DR.Item("Emergency contact").ToString = _Primary._Fullname Then
                    _Primary._Relationship = _DR.Item("Relation to child").ToString
                    _Primary.Store()
                Else

                    Dim _C As New Business.Contact
                    _C._FamilyId = Family._ID
                    _C._SmsName = "IMPORT" 'marker for import

                    _C._Fullname = _DR.Item("Emergency contact").ToString
                    SplitForenameSurname(_C._Fullname, _C._Forename, _C._Surname)

                    _C._Address = Family._Address
                    _C._Postcode = Family._Postcode

                    _C._Relationship = _DR.Item("Relation to child").ToString
                    _C._EmerCont = True

                    If _DR.Item("Telephone").ToString.StartsWith("07") Then
                        _C._TelMobile = _DR.Item("Telephone").ToString
                    Else
                        _C._TelHome = _DR.Item("Telephone").ToString
                    End If

                    _C.Store()

                End If

            Next

        End If

    End Sub

    Private Function ProcessPrimary(ByRef Family As Business.Family, ByVal ExtConn As String) As Business.Contact

        Dim _SQL As String = ""

        'Account, Account ref, Name, Telephone, Email, Contact preference
        _SQL = "SELECT * FROM contact_details where account = '" + Family._AccountNo + "'"
        Dim _Contacts As DataTable = DAL.GetDataTablefromSQL(ExtConn, _SQL)

        If _Contacts IsNot Nothing Then

            Dim _DR As DataRow = _Contacts.Rows(0)

            Dim _Contact As New Business.Contact
            _Contact._FamilyId = Family._ID
            _Contact._Fullname = _DR.Item("Name").ToString

            SplitForenameSurname(_Contact._Fullname, _Contact._Forename, _Contact._Surname)

            _Contact._SmsName = "IMPORT" 'marker for import
            _Contact._PrimaryCont = True
            _Contact._Collect = True
            _Contact._EmerCont = True
            _Contact._Parent = True
            _Contact._Email = _DR.Item("Email").ToString

            'telephone numbers are in a block
            '07801 286306
            '01403 250325
            Dim _Tel As String = _DR.Item("Telephone").ToString

            'split by CRLF
            Dim _Nos As String() = _Tel.Split(Chr(10))
            Dim _i As Integer = 1
            For Each _No In _Nos
                If _No.StartsWith("07") OrElse _No.StartsWith("7") Then
                    _Contact._TelMobile = _No
                Else
                    If _i = 1 Then _Contact._TelHome = _No
                    If _i = 2 Then _Contact._JobTel = _No
                    _i += 1
                End If
            Next

            _Contact.Store()

            Return _Contact

        End If

        Return Nothing

    End Function

    Private Sub CareButton16_Click(sender As Object, e As EventArgs) Handles CareButton16.Click

        'fetch all current children
        Dim _Children As List(Of Business.Child) = Business.Child.RetrieveLiveChildren

        Session.SetupProgressBar("Processing...", _Children.Count)

        For Each _C In _Children

            If _C._Dob.HasValue Then

                Dim _AnnualFrom As Date = DateSerial(2018, 4, 1)
                Dim _AnnualTo As Date = DateSerial(2019, 3, 31)
                Dim _LastDate As Date = _AnnualTo

                Dim _DOB As Date = _C._Dob.Value
                Dim _Leaving As Boolean = False
                Dim _NextBirthday As Date = Nothing
                Dim _AgeNextBirthday As Integer

                'check if child has a birthday within the annualisation period
                Dim _HasBirthday As Boolean = False

                Dim _M As Date = _AnnualFrom
                While _M < _AnnualTo

                    If _DOB.Month = _M.Month Then
                        _HasBirthday = True
                        _NextBirthday = DateSerial(_M.Year, _M.Month, _DOB.Day)
                        _AgeNextBirthday = ValueHandler.ConvertInteger(DateDiff(DateInterval.Year, _DOB, _NextBirthday))
                        Exit While
                    End If

                    _M = _M.AddMonths(1)

                End While

                'check if the child is due to leave
                If _C._DateLeft.HasValue Then
                    If _C._DateLeft < _LastDate Then
                        _Leaving = True
                        _LastDate = _C._DateLeft.Value
                    End If
                End If

                'if they are leaving before their birthday, we just calculate up to their leaving date
                If _LastDate < _NextBirthday Then
                    Dim _MonthBefore As Date = EndOfPreviousMonth(_LastDate)
                    Dim _MonthsToSplit As Long = DateDiff(DateInterval.Month, _AnnualFrom, _MonthBefore) + 1
                    AnnualCalculation(_C._ID, _AnnualFrom, _AnnualFrom, _MonthBefore, _MonthsToSplit)
                Else

                    'if they have a birthday in the period and they are less than 4
                    'we need to create a calculation from the beginning of the period to the month before their birthday
                    'then another calculation from their birthday month to the end of the period (or leave date)
                    If _HasBirthday AndAlso _AgeNextBirthday < 4 Then

                        Dim _MonthBeforeBirthday As Date = EndOfPreviousMonth(_NextBirthday)
                        Dim _MonthsToBirthday As Long = DateDiff(DateInterval.Month, _AnnualFrom, _MonthBeforeBirthday) + 1

                        'create calculation
                        AnnualCalculation(_C._ID, _AnnualFrom, _AnnualFrom, _MonthBeforeBirthday, _MonthsToBirthday)

                        'then another calculation from their birthday month to the leaving date
                        Dim _1stBirthdayMonth As Date = DateSerial(_NextBirthday.Year, _NextBirthday.Month, 1)

                        If _Leaving Then
                            Dim _MonthBeforeLeaving As Date = EndOfPreviousMonth(_LastDate)
                            Dim _MonthsToLeaving As Long = DateDiff(DateInterval.Month, _1stBirthdayMonth, _MonthBeforeLeaving) + 1
                            AnnualCalculation(_C._ID, _AnnualFrom, _1stBirthdayMonth, _MonthBeforeLeaving, _MonthsToLeaving)
                        Else
                            Dim _MonthsToLeaving As Long = DateDiff(DateInterval.Month, _1stBirthdayMonth, _LastDate) + 1
                            AnnualCalculation(_C._ID, _AnnualFrom, _1stBirthdayMonth, _LastDate, _MonthsToLeaving)
                        End If

                    Else
                        If _Leaving Then
                            Dim _MonthBeforeLeaving As Date = EndOfPreviousMonth(_LastDate)
                            Dim _MonthsToLeaving As Long = DateDiff(DateInterval.Month, _AnnualFrom, _MonthBeforeLeaving) + 1
                            AnnualCalculation(_C._ID, _AnnualFrom, _AnnualFrom, _MonthBeforeLeaving, _MonthsToLeaving)
                        Else
                            Dim _MonthsToLeaving As Long = DateDiff(DateInterval.Month, _AnnualFrom, _LastDate) + 1
                            AnnualCalculation(_C._ID, _AnnualFrom, _AnnualFrom, _LastDate, _MonthsToLeaving)
                        End If
                    End If

                End If

            Else
                'blank dob
            End If

            Session.StepProgressBar()

        Next

        CareMessage("DONE!")
        Session.HideProgressBar()

    End Sub

    Private Function EndOfPreviousMonth(ByVal DateIn As Date) As Date
        'i.e. 10th October, means last date of 30/09
        Dim _MonthBefore As Date = DateIn.AddMonths(-1)
        Return DateSerial(_MonthBefore.Year, _MonthBefore.Month, DateTime.DaysInMonth(_MonthBefore.Year, _MonthBefore.Month))
    End Function

    Private Sub AnnualCalculation(ByVal ChildID As Guid?, ByVal InvoiceDate As Date, ByVal FromDate As Date, ByVal ToDate As Date, ByVal SplitMonths As Integer)

        Dim _A As New Business.ChildAnnual
        With _A

            ._ID = Guid.NewGuid
            ._ChildId = ChildID
            ._Active = True

            Dim _BatchNo As Integer = Invoicing.LastBatchNo + 1
            Dim _InvoiceNo As Integer = Invoicing.LastInvoiceNo + 1
            Dim _SplitType As String = SplitMonths.ToString + " months"
            If SplitMonths = 1 Then _SplitType = "1 month"

            If .Calculate(_A._ID.Value, _BatchNo, _InvoiceNo, InvoiceDate, InvoiceDate, FromDate, ToDate, _SplitType, SplitMonths) Then
                _A.CreateBatchHeader(_A._ID.Value, _BatchNo, "Annual Invoice", InvoiceDate, FromDate, ToDate, _InvoiceNo, _InvoiceNo)
            End If

        End With

    End Sub

    Private Sub CareButton17_Click(sender As Object, e As EventArgs) Handles CareButton17.Click

        If Not Prompt() Then Exit Sub

        Dim _Input As String = InputBox("Markup Percentage (numbers only)")
        If _Input <> "" Then

            Dim _Markup As Decimal = ValueHandler.ConvertDecimal(_Input)

            Dim _Tariffs As List(Of Business.Tariff) = Business.Tariff.RetreiveAll
            For Each _T In _Tariffs
                _T._Rate = ValueHandler.MarkUp2DP(_T._Rate, _Markup)
                _T.Store()
            Next

            Dim _VariableRates As List(Of Business.TariffVariable) = Business.TariffVariable.RetreiveAll
            For Each _V In _VariableRates
                _V._Rate = ValueHandler.MarkUp2DP(_V._Rate, _Markup)
                _V.Store()
            Next

            Dim _Matrix As List(Of Business.TariffMatrix) = Business.TariffMatrix.RetreiveAll
            For Each _M In _Matrix
                _M._Value = ValueHandler.MarkUp2DP(_M._Value, _Markup)
                _M.Store()
            Next

            CareMessage("DONE!")

        End If

    End Sub

    Private Sub CareButton18_Click(sender As Object, e As EventArgs) Handles CareButton18.Click

        If Not Prompt() Then Exit Sub

        Dim _SQL As String = "delete from ChildConsent"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Dim _ConsentItems As DataRowCollection = Care.Shared.ListHandler.ReturnItems("Consent")
        Dim _Children As List(Of Business.Child) = Business.Child.RetreiveAll

        Session.SetupProgressBar("Processing Children...", _Children.Count)

        For Each _C In _Children

            For Each _DR As DataRow In _ConsentItems

                Dim _CC As New Business.ChildConsent
                With _CC
                    ._ChildId = _C._ID
                    ._Consent = _DR.Item(1).ToString
                    ._ConsentDate = DateSerial(1970, 1, 1)
                    ._ConsentPerson = "Default"
                    ._Given = True
                    .Store()
                End With

            Next

            Session.StepProgressBar()

        Next

        CareMessage("DONE!")

    End Sub

    Private Sub CareButton19_Click(sender As Object, e As EventArgs) Handles CareButton19.Click

        If Not Prompt() Then Exit Sub

        CreateMonthlyMatrixFromPatterns()

        MsgBox("DONE!")

    End Sub

    Private Sub CareButton15_Click(sender As Object, e As EventArgs) Handles CareButton15.Click
        Dim x As Integer = 0
        Dim y As Integer = 1
        Dim z As Integer = y / x
    End Sub

    Private Sub CareButton20_Click(sender As Object, e As EventArgs) Handles CareButton20.Click
        Dim _fu As New Business.FinancialUtilities
        _fu.UpdateBalances()
    End Sub

    Private Sub CareButton21_Click(sender As Object, e As EventArgs) Handles CareButton21.Click

        Dim _ThumbnailSize As Integer = 325
        Dim _SQL As String = ""
        Dim _DB As String = Session.ConnectionString.Replace("dev", "buttercupcorner")

        'first time
        _SQL += "select ID from AppDocs"
        _SQL += " where file_ext in ('jpg','jpeg','png','tiff','bmp')"
        _SQL += " or subject like '%photo%'"
        _SQL += " order by stamp desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(_DB, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)

                'fetch the document
                Dim _SourceDoc As Care.Shared.Business.Document = Care.Shared.Business.Document.RetreiveByID(_DB, _ID)
                If _SourceDoc IsNot Nothing Then

                    Dim _SourceImage As System.Drawing.Image = Care.Shared.DocumentHandler.GetImagefromByteArray(_SourceDoc._Data)
                    If _SourceImage IsNot Nothing Then
                        If _SourceImage.Size.Height > _ThumbnailSize OrElse _SourceImage.Size.Width > _ThumbnailSize Then

                            'create a new document based upon the original
                            Care.Shared.DocumentHandler.InsertDocument(_DB, _SourceDoc._KeyId, Care.Shared.DocumentHandler.DocumentType.Application, _SourceDoc._Data, "Original " + _SourceDoc._Subject, "original", "")

                            'overwrite it with the thumbnail
                            _SourceDoc._Data = Care.Shared.DocumentHandler.ReturnThumbnail(_SourceDoc._Data, _ThumbnailSize)
                            _SourceDoc._FileSize = _SourceDoc._Data.Length
                            _SourceDoc._Tags = "thumb"

                            'save document
                            Care.Shared.Business.Document.SaveRecord(_DB, _SourceDoc)

                        End If
                    End If
                End If
            Next

        End If

        _DT.Dispose()
        _DT = Nothing

    End Sub
End Class