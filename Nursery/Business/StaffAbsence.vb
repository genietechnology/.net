﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class StaffAbsence

        Public Shared Function RetreiveByStaffID(ByVal StaffID As Guid) As List(Of Business.StaffAbsence)

            Dim _SQL As String = "select * from StaffAbsence" & _
                                 " where staff_id = '" & StaffID.ToString & "'"

            Dim _AbsenceList As List(Of Business.StaffAbsence)
            _AbsenceList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _AbsenceList

        End Function

        Public Shared Function RetreiveAbsenceBetweenDates(ByVal StaffID As Guid, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of Business.StaffAbsence)

            Dim _SQL As String = ""

            _SQL += "select * from StaffAbsence"
            _SQL += " where staff_id = '" & StaffID.ToString & "'"
            _SQL += " and abs_from between " + ValueHandler.SQLDate(DateFrom, True)
            _SQL += " and " + ValueHandler.SQLDate(DateTo, True)

            Dim _AbsenceList As List(Of Business.StaffAbsence)
            _AbsenceList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _AbsenceList

        End Function

        Public Shared Function RetreiveAbsenceFromDate(ByVal StaffID As Guid, ByVal DateFrom As Date) As List(Of Business.StaffAbsence)

            Dim _SQL As String = ""

            _SQL += "select * from StaffAbsence"
            _SQL += " where staff_id = '" & StaffID.ToString & "'"
            _SQL += " and abs_from > " + ValueHandler.SQLDate(DateFrom, True)

            Dim _AbsenceList As List(Of Business.StaffAbsence)
            _AbsenceList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _AbsenceList

        End Function

    End Class

End Namespace

