﻿Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Namespace Business

    Partial Class Staff

        Public Sub UpdateStatistics()

            'fetch absence records for last 52 weeks
            Dim _52WeeksAgo As Date = DateAdd(DateInterval.WeekOfYear, -52, Today)
            Dim _Absences As List(Of Business.StaffAbsence) = Business.StaffAbsence.RetreiveAbsenceFromDate(Me._ID.Value, _52WeeksAgo)

            Dim _BradInstances As Double = 0
            Dim _BradDays As Double = 0
            Dim _HolidaysFrom As Date = DateSerial(Today.Year, 1, 1)
            Dim _Holidays As Double = 0
            Dim _SickDays As Double = 0
            Dim _OtherDays As Double = 0

            'set the entitlement period for this employee
            Dim _EntitleFrom As Date? = Nothing
            Dim _EntitleTo As Date? = Nothing
            SetEntitlement(Me._HolsFrom, _EntitleFrom, _EntitleTo)

            'whip through the records, omitting any weekends (i.e. 1 month sick note)
            For Each _a In _Absences

                If _a._AbsFrom.HasValue AndAlso _a._AbsTo.HasValue Then

                    If _a._AbsType = "Sickness" Then

                        _BradInstances += 1

                        'if we are dealing with sickness calculation for bradford, its a rolling 52 weeks
                        'so we need to ensure we do not count absence into the future
                        'i.e. someone signed off on stress

                        If _a._AbsTo > Today Then
                            _BradDays += ValueHandler.WorkingDays(_a._AbsFrom.Value, Today)
                        Else
                            _BradDays += ValueHandler.WorkingDays(_a._AbsFrom.Value, _a._AbsTo.Value)
                        End If

                    End If

                    'now process the absence record based upon entitlement year

                    Dim _From As Date? = _a._AbsFrom
                    If _a._AbsFrom < _EntitleFrom Then _From = _EntitleFrom

                    Dim _To As Date? = _a._AbsTo
                    If _a._AbsTo > _EntitleTo Then _To = _EntitleTo

                    Dim _Days As Double = 0
                    If _From.HasValue AndAlso _To.HasValue Then
                        _Days = ValueHandler.WorkingDays(_From.Value, _To.Value)
                    End If

                    If _a._AbsType = "Annual Leave" Then
                        _Holidays += _Days
                    Else
                        If _a._AbsType = "Sickness" Then
                            _SickDays += _Days
                        Else
                            _OtherDays += _Days
                        End If
                    End If

                Else
                    'missing from/to date
                End If

            Next

            Dim _Score As Double = _BradInstances * _BradInstances * _BradDays

            Me._BradFrom = _52WeeksAgo
            Me._BradScore = CInt(_Score)

            Me._SickTaken = CInt(_SickDays)
            Me._HolsTaken = CInt(_Holidays)
            Me._AbsTaken = CInt(_OtherDays)

        End Sub

        Private Shared Sub SetEntitlement(ByVal HolidayYear As String, ByRef EntitleFrom As Date?, ByRef EntitleTo As Date?)

            If HolidayYear = "" Then HolidayYear = "January"

            Dim _Month As Integer = 0
            If HolidayYear = "January" Then _Month = 1
            If HolidayYear = "February" Then _Month = 2
            If HolidayYear = "March" Then _Month = 3
            If HolidayYear = "April" Then _Month = 4
            If HolidayYear = "May" Then _Month = 5
            If HolidayYear = "June" Then _Month = 6
            If HolidayYear = "July" Then _Month = 7
            If HolidayYear = "August" Then _Month = 8
            If HolidayYear = "September" Then _Month = 9
            If HolidayYear = "October" Then _Month = 10
            If HolidayYear = "November" Then _Month = 11
            If HolidayYear = "December" Then _Month = 12

            If _Month <= Today.Month Then
                EntitleFrom = DateSerial(Today.Year, _Month, 1)
            Else
                EntitleFrom = DateSerial(Today.Year - 1, _Month, 1)
            End If

            'add 12 months and take a day off
            EntitleTo = EntitleFrom.Value.AddYears(1)
            EntitleTo = EntitleTo.Value.AddDays(-1)

        End Sub

        Public Shared Sub DrillDown(ByVal StaffID As Guid)

            Dim _frm As New frmStaff(StaffID)
            With _frm
                .Text = "View Staff"
                .StartPosition = FormStartPosition.CenterScreen
                .ShowDialog()
            End With

        End Sub

        Public Shared Function RetreiveByClockNo(ByVal ClockNo As String) As Staff

            Dim _SQL As String = "select * from Staff where clock_no = '" + ClockNo + "'"

            Dim _Staff As Staff
            _Staff = PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))

            Return _Staff

        End Function

        Public Shared Function RetrieveAllBySiteName(SiteName As String) As List(Of Staff)

            Dim _SQL As String = "select * from Staff" &
                                 " where site_name = '" & SiteName & "'" &
                                 " and status = 'C'"

            Dim _StaffList As List(Of Staff)
            _StaffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _StaffList

        End Function

        Public Shared Function RetrieveLiveStaffBySite(SiteID As Guid) As List(Of Staff)

            Dim _SQL As String = "select * from Staff" &
                                 " where site_id = '" & SiteID.ToString & "'" &
                                 " and status = 'C'"

            Dim _StaffList As List(Of Staff)
            _StaffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _StaffList

        End Function

        Public Shared Function RetrieveLiveStaff() As List(Of Staff)

            Dim _SQL As String = "select * from Staff" &
                                 " where status = 'C'"

            Dim _StaffList As List(Of Staff)
            _StaffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _StaffList

        End Function

        Public Shared Function LineManagerSQL() As String
            Dim _SQL As String = "select id, fullname from Staff where status = 'C' and line_manager = 1 order by fullname"
            Return _SQL
        End Function

    End Class

End Namespace
