﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class TimeSlot

        Public Shared Function RetreiveAllByStartTime() As List(Of TimeSlot)
            Dim _TimeSlotList As List(Of TimeSlot)
            _TimeSlotList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Timeslots order by start_time"))
            Return _TimeSlotList
        End Function

    End Class

End Namespace

