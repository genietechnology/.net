﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class Medical

    Public Enum EnumLogOrRequest
        Log
        LogAll
        Request
    End Enum

    Public Shared Sub DesignReport(ByVal Mode As EnumLogOrRequest, ByVal ID As String)
        If Mode = EnumLogOrRequest.Request Then
            ReportHandler.DesignReport(Of MedicalReportData)("MedicineAudit.repx", New List(Of MedicalReportData))
        Else
            ReportHandler.DesignReport(Of MedicalReportData)("MedicineLog.repx", New List(Of MedicalReportData))
        End If
    End Sub

    Public Shared Sub PrintReport(ByVal Mode As EnumLogOrRequest, ByVal ID As String)
        If Mode = EnumLogOrRequest.Request Then
            PrintParentConsentForm(ID)
        Else
            PrintMedicineLog(Mode, ID)
        End If
    End Sub

    Public Shared Sub PrintParentConsentForm(ByVal ID As String)

        Dim _ReportData As New List(Of MedicalReportData)

        Dim _MedicalAuth As Business.MedicineAuth = Business.MedicineAuth.RetreiveByID(New Guid(ID))
        If _MedicalAuth IsNot Nothing Then

            Dim _SQL As String = "select MedicineAuth.id, MedicineAuth.child_name," &
                                 " MedicineAuth.illness, MedicineAuth.medicine, MedicineAuth.dosage, MedicineAuth.last_given," &
                                 " MedicineAuth.dosages_due," &
                                 " MedicineAuth.contact_name as 'auth_contact_name', MedicineAuth.contact_sig as 'auth_contact_sig'," &
                                 " MedicineAuth.staff_name as 'auth_staff_name', MedicineAuth.staff_sig as 'auth_staff_sig'," &
                                 " MedicineLog.ID as 'log_id', MedicineLog.actual," &
                                 " MedicineLog.staff_name as 'log_staff_name', MedicineLog.staff_sig as 'log_staff_sig'," &
                                 " MedicineLog.witness_name as 'log_witness_name', MedicineLog.witness_sig as 'log_witness_sig'" &
                                 " from MedicineAuth" &
                                 " left join MedicineLog on MedicineLog.auth_id = MedicineAuth.ID" &
                                 " where MedicineAuth.id = '" + ID + "'" &
                                 " and actual is not null" &
                                 " order by actual"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _R As New MedicalReportData
                    With _R

                        .ID = New Guid(_DR.Item("id").ToString)
                        .ChildName = _DR.Item("child_name").ToString
                        .Illness = _DR.Item("illness").ToString
                        .Medicine = _DR.Item("medicine").ToString
                        .Dosage = _DR.Item("dosage").ToString
                        .LastGiven = ValueHandler.ConvertDate(_DR.Item("last_given"))
                        .DosagesDue = _DR.Item("dosages_due").ToString

                        .AuthContactName = _DR.Item("auth_contact_name").ToString
                        SignatureHandler.WriteToBytes(.AuthContactPng, _DR.Item("auth_contact_sig").ToString, 750, 175)

                        .AuthStaffName = _DR.Item("auth_staff_name").ToString
                        SignatureHandler.WriteToBytes(.AuthStaffPng, _DR.Item("auth_staff_sig").ToString, 750, 175)

                        .LogID = New Guid(_DR.Item("log_id").ToString)
                        .LogActual = ValueHandler.ConvertDate(_DR.Item("actual"))

                        .LogStaffName = _DR.Item("log_staff_name").ToString
                        SignatureHandler.WriteToBytes(.LogStaffPng, _DR.Item("log_staff_sig").ToString, 750, 175)

                        .LogWitnessName = _DR.Item("log_witness_name").ToString
                        SignatureHandler.WriteToBytes(.LogWitnessPng, _DR.Item("log_witness_sig").ToString, 750, 175)

                    End With

                    _ReportData.Add(_R)

                Next

                If _ReportData.Count > 0 Then
                    ReportHandler.RunReport(Of MedicalReportData)("MedicineAudit.repx", _ReportData)
                End If

            Else
                'SQL statement failed
            End If

        Else
            'Getting Auth record failed
        End If

    End Sub

    Public Shared Sub PrintMedicineLog(ByVal Mode As EnumLogOrRequest, ByVal ID As String)

        Dim _ReportData As New List(Of MedicalReportData)

        Dim _SQL As String = "select" &
                             " MedicineLog.ID as 'log_id', MedicineLog.child_name, MedicineLog.actual," &
                             " MedicineLog.illness, MedicineLog.medicine, MedicineLog.dosage," &
                             " MedicineLog.staff_name as 'log_staff_name', MedicineLog.staff_sig as 'log_staff_sig'," &
                             " MedicineLog.witness_name as 'log_witness_name', MedicineLog.witness_sig as 'log_witness_sig'" &
                             " from MedicineLog" &
                             " where auth_id is null"

        If Mode = EnumLogOrRequest.LogAll Then
            _SQL += " and child_id = '" + ID + "'"
        Else
            _SQL += " and ID = '" + ID + "'"
        End If

        _SQL += " order by stamp desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _R As New MedicalReportData
                With _R

                    .ID = New Guid(_DR.Item("log_id").ToString)

                    .ChildName = _DR.Item("child_name").ToString
                    .Illness = _DR.Item("illness").ToString
                    .Medicine = _DR.Item("medicine").ToString
                    .Dosage = _DR.Item("dosage").ToString

                    .LastGiven = Nothing
                    .DosagesDue = ""
                    .AuthContactName = ""
                    .AuthStaffName = ""

                    .LogID = New Guid(_DR.Item("log_id").ToString)
                    .LogActual = ValueHandler.ConvertDate(_DR.Item("actual"))

                    .LogStaffName = _DR.Item("log_staff_name").ToString
                    SignatureHandler.WriteToBytes(.LogStaffPng, _DR.Item("log_staff_sig").ToString, 750, 175)

                    .LogWitnessName = _DR.Item("log_witness_name").ToString
                    SignatureHandler.WriteToBytes(.LogWitnessPng, _DR.Item("log_witness_sig").ToString, 750, 175)

                End With

                _ReportData.Add(_R)

            Next

            If _ReportData.Count > 0 Then
                ReportHandler.RunReport(Of MedicalReportData)("MedicineLog.repx", _ReportData)
            End If

        Else
            'SQL statement failed
        End If

    End Sub

    Private Class MedicalReportData

        Public Sub New()

        End Sub

        Public Property ID As Guid?
        Public Property ChildName As String
        Public Property Illness As String
        Public Property Medicine As String
        Public Property Dosage As String
        Public Property LastGiven As Date?
        Public Property DosagesDue As String
        Public Property AuthContactName As String
        Public Property AuthContactPng As Byte()
        Public Property AuthStaffName As String
        Public Property AuthStaffSig As String
        Public Property AuthStaffPng As Byte()
        Public Property LogID As Guid?
        Public Property LogActual As Date?
        Public Property LogStaffName As String
        Public Property LogStaffPng As Byte()
        Public Property LogWitnessName As String
        Public Property LogWitnessPng As Byte()

    End Class

End Class
