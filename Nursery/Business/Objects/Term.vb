﻿'*****************************************************
'Generated 24/09/2018 09:53:55 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Term
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._TermStart = Nothing
                ._TermEnd = Nothing
                ._HtStart = Nothing
                ._HtEnd = Nothing
                ._TermType = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._TermStart = Nothing
                ._TermEnd = Nothing
                ._HtStart = Nothing
                ._HtEnd = Nothing
                ._TermType = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@TermStart")>
        Public Property _TermStart() As Date?

        <StoredProcParameter("@TermEnd")>
        Public Property _TermEnd() As Date?

        <StoredProcParameter("@HtStart")>
        Public Property _HtStart() As Date?

        <StoredProcParameter("@HtEnd")>
        Public Property _HtEnd() As Date?

        <StoredProcParameter("@TermType")>
        Public Property _TermType() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Term
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTermbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Term
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTermbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Term)
            Dim _TermList As List(Of Term)
            _TermList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTermTable"))
            Return _TermList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Term)
            Dim _TermList As List(Of Term)
            _TermList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTermTable"))
            Return _TermList
        End Function

        Public Shared Sub SaveAll(ByVal TermList As List(Of Term))
            For Each _Term As Term In TermList
                SaveRecord(_Term)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TermList As List(Of Term))
            For Each _Term As Term In TermList
                SaveRecord(ConnectionString, _Term)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Term As Term) As Guid
            Dim _Current As Term = RetreiveByID(Term._ID.Value)
            Term.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Term, _Current, "upsertTerm")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Term As Term) As Guid
            Dim _Current As Term = RetreiveByID(ConnectionString, Term._ID.Value)
            Term.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Term, _Current, "upsertTerm")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Term = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTermbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Term = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTermbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Term = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTerm")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Term = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTerm")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Term

            Dim _T As Term = Nothing

            If DR IsNot Nothing Then
                _T = New Term()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Name = DR("name").ToString.Trim
                    _T._TermStart = GetDate(DR("term_start"))
                    _T._TermEnd = GetDate(DR("term_end"))
                    _T._HtStart = GetDate(DR("ht_start"))
                    _T._HtEnd = GetDate(DR("ht_end"))
                    _T._TermType = DR("term_type").ToString.Trim

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Term)

            Dim _TermList As New List(Of Term)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TermList.Add(PropertiesFromData(_DR))
            Next

            Return _TermList

        End Function


#End Region

    End Class

End Namespace
