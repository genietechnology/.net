﻿'*****************************************************
'Generated 14/03/2016 00:09:43 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class MenuWeek
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SeasonId As Guid?
        Dim m_Week As Byte

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SeasonId = Nothing
                ._Week = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SeasonId = Nothing
                ._Week = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SeasonId")> _
        Public Property _SeasonId() As Guid?
            Get
                Return m_SeasonId
            End Get
            Set(ByVal value As Guid?)
                m_SeasonId = value
            End Set
        End Property

        <StoredProcParameter("@Week")> _
        Public Property _Week() As Byte
            Get
                Return m_Week
            End Get
            Set(ByVal value As Byte)
                m_Week = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As MenuWeek

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMenuWeekbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of MenuWeek)

            Dim _MenuWeekList As List(Of MenuWeek)
            _MenuWeekList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMenuWeekTable"))
            Return _MenuWeekList

        End Function

        Public Shared Sub SaveAll(ByVal MenuWeekList As List(Of MenuWeek))

            For Each _MenuWeek As MenuWeek In MenuWeekList
                SaveRecord(_MenuWeek)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal MenuWeek As MenuWeek) As Guid
            MenuWeek.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, MenuWeek, "upsertMenuWeek")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMenuWeekbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMenuWeek")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As MenuWeek

            Dim _M As MenuWeek = Nothing

            If DR IsNot Nothing Then
                _M = New MenuWeek()
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._SeasonId = GetGUID(DR("season_id"))
                    _M._Week = GetByte(DR("week"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of MenuWeek)

            Dim _MenuWeekList As New List(Of MenuWeek)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MenuWeekList.Add(PropertiesFromData(_DR))
            Next

            Return _MenuWeekList

        End Function


#End Region

    End Class

End Namespace
