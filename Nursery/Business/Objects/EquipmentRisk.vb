﻿'*****************************************************
'Generated 07/06/2017 17:16:45 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class EquipmentRisk
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_EquipId As Guid?
        Dim m_RiskId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._EquipId = Nothing
                ._RiskId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._EquipId = Nothing
                ._RiskId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@EquipId")>
        Public Property _EquipId() As Guid?
            Get
                Return m_EquipId
            End Get
            Set(ByVal value As Guid?)
                m_EquipId = value
            End Set
        End Property

        <StoredProcParameter("@RiskId")>
        Public Property _RiskId() As Guid?
            Get
                Return m_RiskId
            End Get
            Set(ByVal value As Guid?)
                m_RiskId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As EquipmentRisk

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEquipmentRiskbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As EquipmentRisk

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getEquipmentRiskbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of EquipmentRisk)

            Dim _EquipmentRiskList As List(Of EquipmentRisk)
            _EquipmentRiskList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEquipmentRiskTable"))
            Return _EquipmentRiskList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of EquipmentRisk)

            Dim _EquipmentRiskList As List(Of EquipmentRisk)
            _EquipmentRiskList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getEquipmentRiskTable"))
            Return _EquipmentRiskList

        End Function

        Public Shared Sub SaveAll(ByVal EquipmentRiskList As List(Of EquipmentRisk))

            For Each _EquipmentRisk As EquipmentRisk In EquipmentRiskList
                SaveRecord(_EquipmentRisk)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal EquipmentRiskList As List(Of EquipmentRisk))

            For Each _EquipmentRisk As EquipmentRisk In EquipmentRiskList
                SaveRecord(ConnectionString, _EquipmentRisk)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal EquipmentRisk As EquipmentRisk) As Guid
            EquipmentRisk.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, EquipmentRisk, "upsertEquipmentRisk")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal EquipmentRisk As EquipmentRisk) As Guid
            EquipmentRisk.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, EquipmentRisk, "upsertEquipmentRisk")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEquipmentRiskbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteEquipmentRiskbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertEquipmentRisk")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertEquipmentRisk")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As EquipmentRisk

            Dim _E As EquipmentRisk = Nothing

            If DR IsNot Nothing Then
                _E = New EquipmentRisk()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._EquipId = GetGUID(DR("equip_id"))
                    _E._RiskId = GetGUID(DR("risk_id"))

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of EquipmentRisk)

            Dim _EquipmentRiskList As New List(Of EquipmentRisk)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EquipmentRiskList.Add(PropertiesFromData(_DR))
            Next

            Return _EquipmentRiskList

        End Function


#End Region

    End Class

End Namespace
