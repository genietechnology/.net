﻿Imports Care.Global
Imports Care.Data

Namespace Business

    <Obsolete("Use SiteRoom instead.", False)> _
    Public Class Room
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_MinAge As Integer
        Dim m_MaxAge As Integer
        Dim m_Capacity As Integer
        Dim m_StaffRatio As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._MinAge = 0
                ._MaxAge = 0
                ._Capacity = 0
                ._StaffRatio = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._MinAge = 0
                ._MaxAge = 0
                ._Capacity = 0
                ._StaffRatio = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@MinAge")> _
        Public Property _MinAge() As Integer
            Get
                Return m_MinAge
            End Get
            Set(ByVal value As Integer)
                m_MinAge = value
            End Set
        End Property

        <StoredProcParameter("@MaxAge")> _
        Public Property _MaxAge() As Integer
            Get
                Return m_MaxAge
            End Get
            Set(ByVal value As Integer)
                m_MaxAge = value
            End Set
        End Property

        <StoredProcParameter("@Capacity")> _
        Public Property _Capacity() As Integer
            Get
                Return m_Capacity
            End Get
            Set(ByVal value As Integer)
                m_Capacity = value
            End Set
        End Property

        <StoredProcParameter("@StaffRatio")> _
        Public Property _StaffRatio() As Integer
            Get
                Return m_StaffRatio
            End Get
            Set(ByVal value As Integer)
                m_StaffRatio = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Room

            Dim _Room As Room
            _Room = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRoombyID", ID))
            Return _Room

        End Function

        Public Shared Function RetreiveAll() As List(Of Room)

            Dim _RoomList As List(Of Room)
            _RoomList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRoomTable"))
            Return _RoomList

        End Function

        Public Shared Sub SaveAll(ByVal RoomList As List(Of Room))

            For Each _Room As Room In RoomList
                SaveRecord(_Room)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Room As Room) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Room, "upsertRoom")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRoombyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRoom")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Room

            Dim _R As New Room()

            If DR IsNot Nothing Then
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._Name = DR("name").ToString.Trim
                    _R._MinAge = GetInteger(DR("min_age"))
                    _R._MaxAge = GetInteger(DR("max_age"))
                    _R._Capacity = GetInteger(DR("capacity"))
                    _R._StaffRatio = GetInteger(DR("staff_ratio"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Room)

            Dim _RoomList As New List(Of Room)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RoomList.Add(PropertiesFromData(_DR))
            Next

            Return _RoomList

        End Function


#End Region

    End Class

End Namespace
