﻿'*****************************************************
'Generated 16/05/2017 18:43:35 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Rota
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_Wc As Date?
        Dim m_Status As String
        Dim m_Notes As String
        Dim m_CreateUser As String
        Dim m_CreateStamp As DateTime?
        Dim m_CommitUser As String
        Dim m_CommitStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._SiteName = ""
                ._Wc = Nothing
                ._Status = ""
                ._Notes = ""
                ._CreateUser = ""
                ._CreateStamp = Nothing
                ._CommitUser = ""
                ._CommitStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._SiteName = ""
                ._Wc = Nothing
                ._Status = ""
                ._Notes = ""
                ._CreateUser = ""
                ._CreateStamp = Nothing
                ._CommitUser = ""
                ._CommitStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@Wc")>
        Public Property _Wc() As Date?
            Get
                Return m_Wc
            End Get
            Set(ByVal value As Date?)
                m_Wc = value
            End Set
        End Property

        <StoredProcParameter("@Status")>
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@Notes")>
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property

        <StoredProcParameter("@CreateUser")>
        Public Property _CreateUser() As String
            Get
                Return m_CreateUser
            End Get
            Set(ByVal value As String)
                m_CreateUser = value
            End Set
        End Property

        <StoredProcParameter("@CreateStamp")>
        Public Property _CreateStamp() As DateTime?
            Get
                Return m_CreateStamp
            End Get
            Set(ByVal value As DateTime?)
                m_CreateStamp = value
            End Set
        End Property

        <StoredProcParameter("@CommitUser")>
        Public Property _CommitUser() As String
            Get
                Return m_CommitUser
            End Get
            Set(ByVal value As String)
                m_CommitUser = value
            End Set
        End Property

        <StoredProcParameter("@CommitStamp")>
        Public Property _CommitStamp() As DateTime?
            Get
                Return m_CommitStamp
            End Get
            Set(ByVal value As DateTime?)
                m_CommitStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Rota

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRotabyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Rota

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRotabyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Rota)

            Dim _RotaList As List(Of Rota)
            _RotaList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRotaTable"))
            Return _RotaList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Rota)

            Dim _RotaList As List(Of Rota)
            _RotaList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRotaTable"))
            Return _RotaList

        End Function

        Public Shared Sub SaveAll(ByVal RotaList As List(Of Rota))

            For Each _Rota As Rota In RotaList
                SaveRecord(_Rota)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RotaList As List(Of Rota))

            For Each _Rota As Rota In RotaList
                SaveRecord(ConnectionString, _Rota)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Rota As Rota) As Guid
            Rota.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Rota, "upsertRota")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Rota As Rota) As Guid
            Rota.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Rota, "upsertRota")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRotabyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRotabyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRota")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRota")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Rota

            Dim _R As Rota = Nothing

            If DR IsNot Nothing Then
                _R = New Rota()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._SiteId = GetGUID(DR("site_id"))
                    _R._SiteName = DR("site_name").ToString.Trim
                    _R._Wc = GetDate(DR("wc"))
                    _R._Status = DR("status").ToString.Trim
                    _R._Notes = DR("notes").ToString.Trim
                    _R._CreateUser = DR("create_user").ToString.Trim
                    _R._CreateStamp = GetDateTime(DR("create_stamp"))
                    _R._CommitUser = DR("commit_user").ToString.Trim
                    _R._CommitStamp = GetDateTime(DR("commit_stamp"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Rota)

            Dim _RotaList As New List(Of Rota)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RotaList.Add(PropertiesFromData(_DR))
            Next

            Return _RotaList

        End Function


#End Region

    End Class

End Namespace
