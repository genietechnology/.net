﻿'*****************************************************
'Generated 24/09/2018 09:46:47 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildAdvanceSession
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._Mode = ""
                ._DateFrom = Nothing
                ._Monday = Nothing
                ._MondayDesc = ""
                ._Monday1 = 0
                ._Monday2 = 0
                ._Tuesday = Nothing
                ._TuesdayDesc = ""
                ._Tuesday1 = 0
                ._Tuesday2 = 0
                ._Wednesday = Nothing
                ._WednesdayDesc = ""
                ._Wednesday1 = 0
                ._Wednesday2 = 0
                ._Thursday = Nothing
                ._ThursdayDesc = ""
                ._Thursday1 = 0
                ._Thursday2 = 0
                ._Friday = Nothing
                ._FridayDesc = ""
                ._Friday1 = 0
                ._Friday2 = 0
                ._Saturday = Nothing
                ._SaturdayDesc = ""
                ._Saturday1 = 0
                ._Saturday2 = 0
                ._Sunday = Nothing
                ._SundayDesc = ""
                ._Sunday1 = 0
                ._Sunday2 = 0
                ._Activated = False
                ._NoRepeat = False
                ._Avg = False
                ._MondayFund = 0
                ._TuesdayFund = 0
                ._WednesdayFund = 0
                ._ThursdayFund = 0
                ._FridayFund = 0
                ._SaturdayFund = 0
                ._SundayFund = 0
                ._PatternType = ""
                ._MondayBoid = ""
                ._MondayBo = ""
                ._TuesdayBoid = ""
                ._TuesdayBo = ""
                ._WednesdayBoid = ""
                ._WednesdayBo = ""
                ._ThursdayBoid = ""
                ._ThursdayBo = ""
                ._FridayBoid = ""
                ._FridayBo = ""
                ._SaturdayBoid = ""
                ._SaturdayBo = ""
                ._SundayBoid = ""
                ._SundayBo = ""
                ._RecurringScope = Nothing
                ._RecurringScopeDesc = ""
                ._Comments = ""
                ._BookingStatus = ""
                ._CreatedBy = ""
                ._CreatedStamp = Nothing
                ._ModifiedBy = ""
                ._ModifiedStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._Mode = ""
                ._DateFrom = Nothing
                ._Monday = Nothing
                ._MondayDesc = ""
                ._Monday1 = 0
                ._Monday2 = 0
                ._Tuesday = Nothing
                ._TuesdayDesc = ""
                ._Tuesday1 = 0
                ._Tuesday2 = 0
                ._Wednesday = Nothing
                ._WednesdayDesc = ""
                ._Wednesday1 = 0
                ._Wednesday2 = 0
                ._Thursday = Nothing
                ._ThursdayDesc = ""
                ._Thursday1 = 0
                ._Thursday2 = 0
                ._Friday = Nothing
                ._FridayDesc = ""
                ._Friday1 = 0
                ._Friday2 = 0
                ._Saturday = Nothing
                ._SaturdayDesc = ""
                ._Saturday1 = 0
                ._Saturday2 = 0
                ._Sunday = Nothing
                ._SundayDesc = ""
                ._Sunday1 = 0
                ._Sunday2 = 0
                ._Activated = False
                ._NoRepeat = False
                ._Avg = False
                ._MondayFund = 0
                ._TuesdayFund = 0
                ._WednesdayFund = 0
                ._ThursdayFund = 0
                ._FridayFund = 0
                ._SaturdayFund = 0
                ._SundayFund = 0
                ._PatternType = ""
                ._MondayBoid = ""
                ._MondayBo = ""
                ._TuesdayBoid = ""
                ._TuesdayBo = ""
                ._WednesdayBoid = ""
                ._WednesdayBo = ""
                ._ThursdayBoid = ""
                ._ThursdayBo = ""
                ._FridayBoid = ""
                ._FridayBo = ""
                ._SaturdayBoid = ""
                ._SaturdayBo = ""
                ._SundayBoid = ""
                ._SundayBo = ""
                ._RecurringScope = Nothing
                ._RecurringScopeDesc = ""
                ._Comments = ""
                ._BookingStatus = ""
                ._CreatedBy = ""
                ._CreatedStamp = Nothing
                ._ModifiedBy = ""
                ._ModifiedStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?

        <StoredProcParameter("@Mode")>
        Public Property _Mode() As String

        <StoredProcParameter("@DateFrom")>
        Public Property _DateFrom() As Date?

        <StoredProcParameter("@Monday")>
        Public Property _Monday() As Guid?

        <StoredProcParameter("@MondayDesc")>
        Public Property _MondayDesc() As String

        <StoredProcParameter("@Monday1")>
        Public Property _Monday1() As Decimal

        <StoredProcParameter("@Monday2")>
        Public Property _Monday2() As Decimal

        <StoredProcParameter("@Tuesday")>
        Public Property _Tuesday() As Guid?

        <StoredProcParameter("@TuesdayDesc")>
        Public Property _TuesdayDesc() As String

        <StoredProcParameter("@Tuesday1")>
        Public Property _Tuesday1() As Decimal

        <StoredProcParameter("@Tuesday2")>
        Public Property _Tuesday2() As Decimal

        <StoredProcParameter("@Wednesday")>
        Public Property _Wednesday() As Guid?

        <StoredProcParameter("@WednesdayDesc")>
        Public Property _WednesdayDesc() As String

        <StoredProcParameter("@Wednesday1")>
        Public Property _Wednesday1() As Decimal

        <StoredProcParameter("@Wednesday2")>
        Public Property _Wednesday2() As Decimal

        <StoredProcParameter("@Thursday")>
        Public Property _Thursday() As Guid?

        <StoredProcParameter("@ThursdayDesc")>
        Public Property _ThursdayDesc() As String

        <StoredProcParameter("@Thursday1")>
        Public Property _Thursday1() As Decimal

        <StoredProcParameter("@Thursday2")>
        Public Property _Thursday2() As Decimal

        <StoredProcParameter("@Friday")>
        Public Property _Friday() As Guid?

        <StoredProcParameter("@FridayDesc")>
        Public Property _FridayDesc() As String

        <StoredProcParameter("@Friday1")>
        Public Property _Friday1() As Decimal

        <StoredProcParameter("@Friday2")>
        Public Property _Friday2() As Decimal

        <StoredProcParameter("@Saturday")>
        Public Property _Saturday() As Guid?

        <StoredProcParameter("@SaturdayDesc")>
        Public Property _SaturdayDesc() As String

        <StoredProcParameter("@Saturday1")>
        Public Property _Saturday1() As Decimal

        <StoredProcParameter("@Saturday2")>
        Public Property _Saturday2() As Decimal

        <StoredProcParameter("@Sunday")>
        Public Property _Sunday() As Guid?

        <StoredProcParameter("@SundayDesc")>
        Public Property _SundayDesc() As String

        <StoredProcParameter("@Sunday1")>
        Public Property _Sunday1() As Decimal

        <StoredProcParameter("@Sunday2")>
        Public Property _Sunday2() As Decimal

        <StoredProcParameter("@Activated")>
        Public Property _Activated() As Boolean

        <StoredProcParameter("@NoRepeat")>
        Public Property _NoRepeat() As Boolean

        <StoredProcParameter("@Avg")>
        Public Property _Avg() As Boolean

        <StoredProcParameter("@MondayFund")>
        Public Property _MondayFund() As Decimal

        <StoredProcParameter("@TuesdayFund")>
        Public Property _TuesdayFund() As Decimal

        <StoredProcParameter("@WednesdayFund")>
        Public Property _WednesdayFund() As Decimal

        <StoredProcParameter("@ThursdayFund")>
        Public Property _ThursdayFund() As Decimal

        <StoredProcParameter("@FridayFund")>
        Public Property _FridayFund() As Decimal

        <StoredProcParameter("@SaturdayFund")>
        Public Property _SaturdayFund() As Decimal

        <StoredProcParameter("@SundayFund")>
        Public Property _SundayFund() As Decimal

        <StoredProcParameter("@PatternType")>
        Public Property _PatternType() As String

        <StoredProcParameter("@MondayBoid")>
        Public Property _MondayBoid() As String

        <StoredProcParameter("@MondayBo")>
        Public Property _MondayBo() As String

        <StoredProcParameter("@TuesdayBoid")>
        Public Property _TuesdayBoid() As String

        <StoredProcParameter("@TuesdayBo")>
        Public Property _TuesdayBo() As String

        <StoredProcParameter("@WednesdayBoid")>
        Public Property _WednesdayBoid() As String

        <StoredProcParameter("@WednesdayBo")>
        Public Property _WednesdayBo() As String

        <StoredProcParameter("@ThursdayBoid")>
        Public Property _ThursdayBoid() As String

        <StoredProcParameter("@ThursdayBo")>
        Public Property _ThursdayBo() As String

        <StoredProcParameter("@FridayBoid")>
        Public Property _FridayBoid() As String

        <StoredProcParameter("@FridayBo")>
        Public Property _FridayBo() As String

        <StoredProcParameter("@SaturdayBoid")>
        Public Property _SaturdayBoid() As String

        <StoredProcParameter("@SaturdayBo")>
        Public Property _SaturdayBo() As String

        <StoredProcParameter("@SundayBoid")>
        Public Property _SundayBoid() As String

        <StoredProcParameter("@SundayBo")>
        Public Property _SundayBo() As String

        <StoredProcParameter("@RecurringScope")>
        Public Property _RecurringScope() As Byte

        <StoredProcParameter("@RecurringScopeDesc")>
        Public Property _RecurringScopeDesc() As String

        <StoredProcParameter("@Comments")>
        Public Property _Comments() As String

        <StoredProcParameter("@BookingStatus")>
        Public Property _BookingStatus() As String

        <StoredProcParameter("@CreatedBy")>
        Public Property _CreatedBy() As String

        <StoredProcParameter("@CreatedStamp")>
        Public Property _CreatedStamp() As DateTime?

        <StoredProcParameter("@ModifiedBy")>
        Public Property _ModifiedBy() As String

        <StoredProcParameter("@ModifiedStamp")>
        Public Property _ModifiedStamp() As DateTime?


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildAdvanceSession
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildAdvanceSessionbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildAdvanceSession
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildAdvanceSessionbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of ChildAdvanceSession)
            Dim _ChildAdvanceSessionList As List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildAdvanceSessionTable"))
            Return _ChildAdvanceSessionList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildAdvanceSession)
            Dim _ChildAdvanceSessionList As List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildAdvanceSessionTable"))
            Return _ChildAdvanceSessionList
        End Function

        Public Shared Sub SaveAll(ByVal ChildAdvanceSessionList As List(Of ChildAdvanceSession))
            For Each _ChildAdvanceSession As ChildAdvanceSession In ChildAdvanceSessionList
                SaveRecord(_ChildAdvanceSession)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildAdvanceSessionList As List(Of ChildAdvanceSession))
            For Each _ChildAdvanceSession As ChildAdvanceSession In ChildAdvanceSessionList
                SaveRecord(ConnectionString, _ChildAdvanceSession)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal ChildAdvanceSession As ChildAdvanceSession) As Guid
            Dim _Current As ChildAdvanceSession = RetreiveByID(ChildAdvanceSession._ID.Value)
            ChildAdvanceSession.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildAdvanceSession, _Current, "upsertChildAdvanceSession")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildAdvanceSession As ChildAdvanceSession) As Guid
            Dim _Current As ChildAdvanceSession = RetreiveByID(ConnectionString, ChildAdvanceSession._ID.Value)
            ChildAdvanceSession.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildAdvanceSession, _Current, "upsertChildAdvanceSession")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As ChildAdvanceSession = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildAdvanceSessionbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As ChildAdvanceSession = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildAdvanceSessionbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As ChildAdvanceSession = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildAdvanceSession")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As ChildAdvanceSession = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildAdvanceSession")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildAdvanceSession

            Dim _C As ChildAdvanceSession = Nothing

            If DR IsNot Nothing Then
                _C = New ChildAdvanceSession()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._Mode = DR("mode").ToString.Trim
                    _C._DateFrom = GetDate(DR("date_from"))
                    _C._Monday = GetGUID(DR("monday"))
                    _C._MondayDesc = DR("monday_desc").ToString.Trim
                    _C._Monday1 = GetDecimal(DR("monday_1"))
                    _C._Monday2 = GetDecimal(DR("monday_2"))
                    _C._Tuesday = GetGUID(DR("tuesday"))
                    _C._TuesdayDesc = DR("tuesday_desc").ToString.Trim
                    _C._Tuesday1 = GetDecimal(DR("tuesday_1"))
                    _C._Tuesday2 = GetDecimal(DR("tuesday_2"))
                    _C._Wednesday = GetGUID(DR("wednesday"))
                    _C._WednesdayDesc = DR("wednesday_desc").ToString.Trim
                    _C._Wednesday1 = GetDecimal(DR("wednesday_1"))
                    _C._Wednesday2 = GetDecimal(DR("wednesday_2"))
                    _C._Thursday = GetGUID(DR("thursday"))
                    _C._ThursdayDesc = DR("thursday_desc").ToString.Trim
                    _C._Thursday1 = GetDecimal(DR("thursday_1"))
                    _C._Thursday2 = GetDecimal(DR("thursday_2"))
                    _C._Friday = GetGUID(DR("friday"))
                    _C._FridayDesc = DR("friday_desc").ToString.Trim
                    _C._Friday1 = GetDecimal(DR("friday_1"))
                    _C._Friday2 = GetDecimal(DR("friday_2"))
                    _C._Saturday = GetGUID(DR("saturday"))
                    _C._SaturdayDesc = DR("saturday_desc").ToString.Trim
                    _C._Saturday1 = GetDecimal(DR("saturday_1"))
                    _C._Saturday2 = GetDecimal(DR("saturday_2"))
                    _C._Sunday = GetGUID(DR("sunday"))
                    _C._SundayDesc = DR("sunday_desc").ToString.Trim
                    _C._Sunday1 = GetDecimal(DR("sunday_1"))
                    _C._Sunday2 = GetDecimal(DR("sunday_2"))
                    _C._Activated = GetBoolean(DR("activated"))
                    _C._NoRepeat = GetBoolean(DR("no_repeat"))
                    _C._Avg = GetBoolean(DR("avg"))
                    _C._MondayFund = GetDecimal(DR("monday_fund"))
                    _C._TuesdayFund = GetDecimal(DR("tuesday_fund"))
                    _C._WednesdayFund = GetDecimal(DR("wednesday_fund"))
                    _C._ThursdayFund = GetDecimal(DR("thursday_fund"))
                    _C._FridayFund = GetDecimal(DR("friday_fund"))
                    _C._SaturdayFund = GetDecimal(DR("saturday_fund"))
                    _C._SundayFund = GetDecimal(DR("sunday_fund"))
                    _C._PatternType = DR("pattern_type").ToString.Trim
                    _C._MondayBoid = DR("monday_boid").ToString.Trim
                    _C._MondayBo = DR("monday_bo").ToString.Trim
                    _C._TuesdayBoid = DR("tuesday_boid").ToString.Trim
                    _C._TuesdayBo = DR("tuesday_bo").ToString.Trim
                    _C._WednesdayBoid = DR("wednesday_boid").ToString.Trim
                    _C._WednesdayBo = DR("wednesday_bo").ToString.Trim
                    _C._ThursdayBoid = DR("thursday_boid").ToString.Trim
                    _C._ThursdayBo = DR("thursday_bo").ToString.Trim
                    _C._FridayBoid = DR("friday_boid").ToString.Trim
                    _C._FridayBo = DR("friday_bo").ToString.Trim
                    _C._SaturdayBoid = DR("saturday_boid").ToString.Trim
                    _C._SaturdayBo = DR("saturday_bo").ToString.Trim
                    _C._SundayBoid = DR("sunday_boid").ToString.Trim
                    _C._SundayBo = DR("sunday_bo").ToString.Trim
                    _C._RecurringScope = GetByte(DR("recurring_scope"))
                    _C._RecurringScopeDesc = DR("recurring_scope_desc").ToString.Trim
                    _C._Comments = DR("comments").ToString.Trim
                    _C._BookingStatus = DR("booking_status").ToString.Trim
                    _C._CreatedBy = DR("created_by").ToString.Trim
                    _C._CreatedStamp = GetDateTime(DR("created_stamp"))
                    _C._ModifiedBy = DR("modified_by").ToString.Trim
                    _C._ModifiedStamp = GetDateTime(DR("modified_stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildAdvanceSession)

            Dim _ChildAdvanceSessionList As New List(Of ChildAdvanceSession)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildAdvanceSessionList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildAdvanceSessionList

        End Function


#End Region

    End Class

End Namespace
