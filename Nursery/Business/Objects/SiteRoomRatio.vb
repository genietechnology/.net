﻿'*****************************************************
'Generated 18/09/2016 12:35:04 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class SiteRoomRatio
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RoomId As Guid?
        Dim m_AgeFrom As Integer
        Dim m_AgeTo As Integer
        Dim m_Capacity As Integer
        Dim m_Ratio As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RoomId = Nothing
                ._AgeFrom = 0
                ._AgeTo = 0
                ._Capacity = 0
                ._Ratio = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RoomId = Nothing
                ._AgeFrom = 0
                ._AgeTo = 0
                ._Capacity = 0
                ._Ratio = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@AgeFrom")> _
        Public Property _AgeFrom() As Integer
            Get
                Return m_AgeFrom
            End Get
            Set(ByVal value As Integer)
                m_AgeFrom = value
            End Set
        End Property

        <StoredProcParameter("@AgeTo")> _
        Public Property _AgeTo() As Integer
            Get
                Return m_AgeTo
            End Get
            Set(ByVal value As Integer)
                m_AgeTo = value
            End Set
        End Property

        <StoredProcParameter("@Capacity")> _
        Public Property _Capacity() As Integer
            Get
                Return m_Capacity
            End Get
            Set(ByVal value As Integer)
                m_Capacity = value
            End Set
        End Property

        <StoredProcParameter("@Ratio")> _
        Public Property _Ratio() As Integer
            Get
                Return m_Ratio
            End Get
            Set(ByVal value As Integer)
                m_Ratio = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As SiteRoomRatio

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSiteRoomRatiobyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of SiteRoomRatio)

            Dim _SiteRoomRatioList As List(Of SiteRoomRatio)
            _SiteRoomRatioList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSiteRoomRatioTable"))
            Return _SiteRoomRatioList

        End Function

        Public Shared Sub SaveAll(ByVal SiteRoomRatioList As List(Of SiteRoomRatio))

            For Each _SiteRoomRatio As SiteRoomRatio In SiteRoomRatioList
                SaveRecord(_SiteRoomRatio)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal SiteRoomRatio As SiteRoomRatio) As Guid
            SiteRoomRatio.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, SiteRoomRatio, "upsertSiteRoomRatio")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSiteRoomRatiobyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSiteRoomRatio")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As SiteRoomRatio

            Dim _S As SiteRoomRatio = Nothing

            If DR IsNot Nothing Then
                _S = New SiteRoomRatio()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._RoomId = GetGUID(DR("room_id"))
                    _S._AgeFrom = GetInteger(DR("age_from"))
                    _S._AgeTo = GetInteger(DR("age_to"))
                    _S._Capacity = GetInteger(DR("capacity"))
                    _S._Ratio = GetInteger(DR("ratio"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of SiteRoomRatio)

            Dim _SiteRoomRatioList As New List(Of SiteRoomRatio)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SiteRoomRatioList.Add(PropertiesFromData(_DR))
            Next

            Return _SiteRoomRatioList

        End Function


#End Region

    End Class

End Namespace
