﻿'*****************************************************
'Generated 04/10/2015 16:02:00 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class School
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Address As String
        Dim m_Poscode As String
        Dim m_Tel As String
        Dim m_PickupCharge As Decimal
        Dim m_DropoffCharge As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Address = ""
                ._Poscode = ""
                ._Tel = ""
                ._PickupCharge = 0
                ._DropoffCharge = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Address = ""
                ._Poscode = ""
                ._Tel = ""
                ._PickupCharge = 0
                ._DropoffCharge = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Address")> _
        Public Property _Address() As String
            Get
                Return m_Address
            End Get
            Set(ByVal value As String)
                m_Address = value
            End Set
        End Property

        <StoredProcParameter("@Poscode")> _
        Public Property _Poscode() As String
            Get
                Return m_Poscode
            End Get
            Set(ByVal value As String)
                m_Poscode = value
            End Set
        End Property

        <StoredProcParameter("@Tel")> _
        Public Property _Tel() As String
            Get
                Return m_Tel
            End Get
            Set(ByVal value As String)
                m_Tel = value
            End Set
        End Property

        <StoredProcParameter("@PickupCharge")> _
        Public Property _PickupCharge() As Decimal
            Get
                Return m_PickupCharge
            End Get
            Set(ByVal value As Decimal)
                m_PickupCharge = value
            End Set
        End Property

        <StoredProcParameter("@DropoffCharge")> _
        Public Property _DropoffCharge() As Decimal
            Get
                Return m_DropoffCharge
            End Get
            Set(ByVal value As Decimal)
                m_DropoffCharge = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As School

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSchoolbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of School)

            Dim _SchoolList As List(Of School)
            _SchoolList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSchoolTable"))
            Return _SchoolList

        End Function

        Public Shared Sub SaveAll(ByVal SchoolList As List(Of School))

            For Each _School As School In SchoolList
                SaveRecord(_School)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal School As School) As Guid
            School.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, School, "upsertSchool")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSchoolbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSchool")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As School

            Dim _S As School = Nothing

            If DR IsNot Nothing Then
                _S = New School()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._Name = DR("name").ToString.Trim
                    _S._Address = DR("address").ToString.Trim
                    _S._Poscode = DR("poscode").ToString.Trim
                    _S._Tel = DR("tel").ToString.Trim
                    _S._PickupCharge = GetDecimal(DR("pickup_charge"))
                    _S._DropoffCharge = GetDecimal(DR("dropoff_charge"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of School)

            Dim _SchoolList As New List(Of School)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SchoolList.Add(PropertiesFromData(_DR))
            Next

            Return _SchoolList

        End Function


#End Region

    End Class

End Namespace
