﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class MedicineAuth
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_ContactId As Guid?
        Dim m_ContactName As String
        Dim m_ContactSig As String
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_StaffSig As String
        Dim m_Illness As String
        Dim m_Medicine As String
        Dim m_Dosage As String
        Dim m_LastGiven As DateTime?
        Dim m_Frequency As String
        Dim m_DosagesDue As String
        Dim m_CollectId As Guid?
        Dim m_CollectName As String
        Dim m_CollectSig As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._ContactId = Nothing
                ._ContactName = ""
                ._ContactSig = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._StaffSig = ""
                ._Illness = ""
                ._Medicine = ""
                ._Dosage = ""
                ._LastGiven = Nothing
                ._Frequency = ""
                ._DosagesDue = ""
                ._CollectId = Nothing
                ._CollectName = ""
                ._CollectSig = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._ContactId = Nothing
                ._ContactName = ""
                ._ContactSig = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._StaffSig = ""
                ._Illness = ""
                ._Medicine = ""
                ._Dosage = ""
                ._LastGiven = Nothing
                ._Frequency = ""
                ._DosagesDue = ""
                ._CollectId = Nothing
                ._CollectName = ""
                ._CollectSig = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@ContactId")> _
        Public Property _ContactId() As Guid?
            Get
                Return m_ContactId
            End Get
            Set(ByVal value As Guid?)
                m_ContactId = value
            End Set
        End Property

        <StoredProcParameter("@ContactName")> _
        Public Property _ContactName() As String
            Get
                Return m_ContactName
            End Get
            Set(ByVal value As String)
                m_ContactName = value
            End Set
        End Property

        <StoredProcParameter("@ContactSig")> _
        Public Property _ContactSig() As String
            Get
                Return m_ContactSig
            End Get
            Set(ByVal value As String)
                m_ContactSig = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@StaffSig")> _
        Public Property _StaffSig() As String
            Get
                Return m_StaffSig
            End Get
            Set(ByVal value As String)
                m_StaffSig = value
            End Set
        End Property

        <StoredProcParameter("@Illness")> _
        Public Property _Illness() As String
            Get
                Return m_Illness
            End Get
            Set(ByVal value As String)
                m_Illness = value
            End Set
        End Property

        <StoredProcParameter("@Medicine")> _
        Public Property _Medicine() As String
            Get
                Return m_Medicine
            End Get
            Set(ByVal value As String)
                m_Medicine = value
            End Set
        End Property

        <StoredProcParameter("@Dosage")> _
        Public Property _Dosage() As String
            Get
                Return m_Dosage
            End Get
            Set(ByVal value As String)
                m_Dosage = value
            End Set
        End Property

        <StoredProcParameter("@LastGiven")> _
        Public Property _LastGiven() As DateTime?
            Get
                Return m_LastGiven
            End Get
            Set(ByVal value As DateTime?)
                m_LastGiven = value
            End Set
        End Property

        <StoredProcParameter("@Frequency")> _
        Public Property _Frequency() As String
            Get
                Return m_Frequency
            End Get
            Set(ByVal value As String)
                m_Frequency = value
            End Set
        End Property

        <StoredProcParameter("@DosagesDue")> _
        Public Property _DosagesDue() As String
            Get
                Return m_DosagesDue
            End Get
            Set(ByVal value As String)
                m_DosagesDue = value
            End Set
        End Property

        <StoredProcParameter("@CollectId")> _
        Public Property _CollectId() As Guid?
            Get
                Return m_CollectId
            End Get
            Set(ByVal value As Guid?)
                m_CollectId = value
            End Set
        End Property

        <StoredProcParameter("@CollectName")> _
        Public Property _CollectName() As String
            Get
                Return m_CollectName
            End Get
            Set(ByVal value As String)
                m_CollectName = value
            End Set
        End Property

        <StoredProcParameter("@CollectSig")> _
        Public Property _CollectSig() As String
            Get
                Return m_CollectSig
            End Get
            Set(ByVal value As String)
                m_CollectSig = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As MedicineAuth

            Dim _MedicineAuth As MedicineAuth
            _MedicineAuth = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMedicineAuthbyID", ID))
            Return _MedicineAuth

        End Function

        Public Shared Function RetreiveAll() As List(Of MedicineAuth)

            Dim _MedicineAuthList As List(Of MedicineAuth)
            _MedicineAuthList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMedicineAuthTable"))
            Return _MedicineAuthList

        End Function

        Public Shared Sub SaveAll(ByVal MedicineAuthList As List(Of MedicineAuth))

            For Each _MedicineAuth As MedicineAuth In MedicineAuthList
                SaveRecord(_MedicineAuth)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal MedicineAuth As MedicineAuth) As Guid
            DAL.SaveRecord(Session.ConnectionManager, MedicineAuth, "upsertMedicineAuth")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMedicineAuthbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMedicineAuth")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As MedicineAuth

            Dim _M As New MedicineAuth()

            If DR IsNot Nothing Then
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._DayId = GetGUID(DR("day_id"))
                    _M._ChildId = GetGUID(DR("child_id"))
                    _M._ChildName = DR("child_name").ToString.Trim
                    _M._ContactId = GetGUID(DR("contact_id"))
                    _M._ContactName = DR("contact_name").ToString.Trim
                    _M._ContactSig = GetXML(DR("contact_sig"))
                    _M._StaffId = GetGUID(DR("staff_id"))
                    _M._StaffName = DR("staff_name").ToString.Trim
                    _M._StaffSig = GetXML(DR("staff_sig"))
                    _M._Illness = DR("illness").ToString.Trim
                    _M._Medicine = DR("medicine").ToString.Trim
                    _M._Dosage = DR("dosage").ToString.Trim
                    _M._LastGiven = GetDateTime(DR("last_given"))
                    _M._Frequency = DR("frequency").ToString.Trim
                    _M._DosagesDue = DR("dosages_due").ToString.Trim
                    _M._CollectId = GetGUID(DR("collect_id"))
                    _M._CollectName = DR("collect_name").ToString.Trim
                    _M._CollectSig = GetXML(DR("collect_sig"))
                    _M._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of MedicineAuth)

            Dim _MedicineAuthList As New List(Of MedicineAuth)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MedicineAuthList.Add(PropertiesFromData(_DR))
            Next

            Return _MedicineAuthList

        End Function


#End Region

    End Class

End Namespace
