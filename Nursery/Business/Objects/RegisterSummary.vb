﻿'*****************************************************
'Generated 23/03/2016 20:47:27 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RegisterSummary
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_Date As Date?
        Dim m_PersonId As Guid?
        Dim m_PersonName As String
        Dim m_PersonType As String
        Dim m_InOut As String
        Dim m_Location As String
        Dim m_FirstStamp As DateTime?
        Dim m_LastStamp As DateTime?
        Dim m_Duration As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._Date = Nothing
                ._PersonId = Nothing
                ._PersonName = ""
                ._PersonType = ""
                ._InOut = ""
                ._Location = ""
                ._FirstStamp = Nothing
                ._LastStamp = Nothing
                ._Duration = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._Date = Nothing
                ._PersonId = Nothing
                ._PersonName = ""
                ._PersonType = ""
                ._InOut = ""
                ._Location = ""
                ._FirstStamp = Nothing
                ._LastStamp = Nothing
                ._Duration = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As Date?
            Get
                Return m_Date
            End Get
            Set(ByVal value As Date?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@PersonId")> _
        Public Property _PersonId() As Guid?
            Get
                Return m_PersonId
            End Get
            Set(ByVal value As Guid?)
                m_PersonId = value
            End Set
        End Property

        <StoredProcParameter("@PersonName")> _
        Public Property _PersonName() As String
            Get
                Return m_PersonName
            End Get
            Set(ByVal value As String)
                m_PersonName = value
            End Set
        End Property

        <StoredProcParameter("@PersonType")> _
        Public Property _PersonType() As String
            Get
                Return m_PersonType
            End Get
            Set(ByVal value As String)
                m_PersonType = value
            End Set
        End Property

        <StoredProcParameter("@InOut")> _
        Public Property _InOut() As String
            Get
                Return m_InOut
            End Get
            Set(ByVal value As String)
                m_InOut = value
            End Set
        End Property

        <StoredProcParameter("@Location")> _
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@FirstStamp")> _
        Public Property _FirstStamp() As DateTime?
            Get
                Return m_FirstStamp
            End Get
            Set(ByVal value As DateTime?)
                m_FirstStamp = value
            End Set
        End Property

        <StoredProcParameter("@LastStamp")> _
        Public Property _LastStamp() As DateTime?
            Get
                Return m_LastStamp
            End Get
            Set(ByVal value As DateTime?)
                m_LastStamp = value
            End Set
        End Property

        <StoredProcParameter("@Duration")> _
        Public Property _Duration() As Integer
            Get
                Return m_Duration
            End Get
            Set(ByVal value As Integer)
                m_Duration = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RegisterSummary

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRegisterSummarybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RegisterSummary)

            Dim _RegisterSummaryList As List(Of RegisterSummary)
            _RegisterSummaryList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRegisterSummaryTable"))
            Return _RegisterSummaryList

        End Function

        Public Shared Sub SaveAll(ByVal RegisterSummaryList As List(Of RegisterSummary))

            For Each _RegisterSummary As RegisterSummary In RegisterSummaryList
                SaveRecord(_RegisterSummary)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RegisterSummary As RegisterSummary) As Guid
            RegisterSummary.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RegisterSummary, "upsertRegisterSummary")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRegisterSummarybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRegisterSummary")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RegisterSummary

            Dim _R As RegisterSummary = Nothing

            If DR IsNot Nothing Then
                _R = New RegisterSummary()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._DayId = GetGUID(DR("day_id"))
                    _R._Date = GetDate(DR("date"))
                    _R._PersonId = GetGUID(DR("person_id"))
                    _R._PersonName = DR("person_name").ToString.Trim
                    _R._PersonType = DR("person_type").ToString.Trim
                    _R._InOut = DR("in_out").ToString.Trim
                    _R._Location = DR("location").ToString.Trim
                    _R._FirstStamp = GetDateTime(DR("first_stamp"))
                    _R._LastStamp = GetDateTime(DR("last_stamp"))
                    _R._Duration = GetInteger(DR("duration"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RegisterSummary)

            Dim _RegisterSummaryList As New List(Of RegisterSummary)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RegisterSummaryList.Add(PropertiesFromData(_DR))
            Next

            Return _RegisterSummaryList

        End Function


#End Region

    End Class

End Namespace
