﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class MedicineLog
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_AuthId As Guid?
        Dim m_Due As DateTime?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_Illness As String
        Dim m_Medicine As String
        Dim m_Dosage As String
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_StaffSig As String
        Dim m_WitnessId As Guid?
        Dim m_WitnessName As String
        Dim m_WitnessSig As String
        Dim m_Actual As DateTime?
        Dim m_Button As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._AuthId = Nothing
                ._Due = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._Illness = ""
                ._Medicine = ""
                ._Dosage = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._StaffSig = ""
                ._WitnessId = Nothing
                ._WitnessName = ""
                ._WitnessSig = ""
                ._Actual = Nothing
                ._Button = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._AuthId = Nothing
                ._Due = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._Illness = ""
                ._Medicine = ""
                ._Dosage = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._StaffSig = ""
                ._WitnessId = Nothing
                ._WitnessName = ""
                ._WitnessSig = ""
                ._Actual = Nothing
                ._Button = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@AuthId")> _
        Public Property _AuthId() As Guid?
            Get
                Return m_AuthId
            End Get
            Set(ByVal value As Guid?)
                m_AuthId = value
            End Set
        End Property

        <StoredProcParameter("@Due")> _
        Public Property _Due() As DateTime?
            Get
                Return m_Due
            End Get
            Set(ByVal value As DateTime?)
                m_Due = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@Illness")> _
        Public Property _Illness() As String
            Get
                Return m_Illness
            End Get
            Set(ByVal value As String)
                m_Illness = value
            End Set
        End Property

        <StoredProcParameter("@Medicine")> _
        Public Property _Medicine() As String
            Get
                Return m_Medicine
            End Get
            Set(ByVal value As String)
                m_Medicine = value
            End Set
        End Property

        <StoredProcParameter("@Dosage")> _
        Public Property _Dosage() As String
            Get
                Return m_Dosage
            End Get
            Set(ByVal value As String)
                m_Dosage = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@StaffSig")> _
        Public Property _StaffSig() As String
            Get
                Return m_StaffSig
            End Get
            Set(ByVal value As String)
                m_StaffSig = value
            End Set
        End Property

        <StoredProcParameter("@WitnessId")> _
        Public Property _WitnessId() As Guid?
            Get
                Return m_WitnessId
            End Get
            Set(ByVal value As Guid?)
                m_WitnessId = value
            End Set
        End Property

        <StoredProcParameter("@WitnessName")> _
        Public Property _WitnessName() As String
            Get
                Return m_WitnessName
            End Get
            Set(ByVal value As String)
                m_WitnessName = value
            End Set
        End Property

        <StoredProcParameter("@WitnessSig")> _
        Public Property _WitnessSig() As String
            Get
                Return m_WitnessSig
            End Get
            Set(ByVal value As String)
                m_WitnessSig = value
            End Set
        End Property

        <StoredProcParameter("@Actual")> _
        Public Property _Actual() As DateTime?
            Get
                Return m_Actual
            End Get
            Set(ByVal value As DateTime?)
                m_Actual = value
            End Set
        End Property

        <StoredProcParameter("@Button")> _
        Public Property _Button() As String
            Get
                Return m_Button
            End Get
            Set(ByVal value As String)
                m_Button = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As MedicineLog

            Dim _MedicineLog As MedicineLog
            _MedicineLog = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMedicineLogbyID", ID))
            Return _MedicineLog

        End Function

        Public Shared Function RetreiveAll() As List(Of MedicineLog)

            Dim _MedicineLogList As List(Of MedicineLog)
            _MedicineLogList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMedicineLogTable"))
            Return _MedicineLogList

        End Function

        Public Shared Sub SaveAll(ByVal MedicineLogList As List(Of MedicineLog))

            For Each _MedicineLog As MedicineLog In MedicineLogList
                SaveRecord(_MedicineLog)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal MedicineLog As MedicineLog) As Guid
            DAL.SaveRecord(Session.ConnectionManager, MedicineLog, "upsertMedicineLog")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMedicineLogbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMedicineLog")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As MedicineLog

            Dim _M As New MedicineLog()

            If DR IsNot Nothing Then
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._DayId = GetGUID(DR("day_id"))
                    _M._AuthId = GetGUID(DR("auth_id"))
                    _M._Due = GetDateTime(DR("due"))
                    _M._ChildId = GetGUID(DR("child_id"))
                    _M._ChildName = DR("child_name").ToString.Trim
                    _M._Illness = DR("illness").ToString.Trim
                    _M._Medicine = DR("medicine").ToString.Trim
                    _M._Dosage = DR("dosage").ToString.Trim
                    _M._StaffId = GetGUID(DR("staff_id"))
                    _M._StaffName = DR("staff_name").ToString.Trim
                    _M._StaffSig = GetXML(DR("staff_sig"))
                    _M._WitnessId = GetGUID(DR("witness_id"))
                    _M._WitnessName = DR("witness_name").ToString.Trim
                    _M._WitnessSig = GetXML(DR("witness_sig"))
                    _M._Actual = GetDateTime(DR("actual"))
                    _M._Button = DR("button").ToString.Trim
                    _M._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of MedicineLog)

            Dim _MedicineLogList As New List(Of MedicineLog)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MedicineLogList.Add(PropertiesFromData(_DR))
            Next

            Return _MedicineLogList

        End Function


#End Region

    End Class

End Namespace
