﻿'*****************************************************
'Generated 20/09/2018 22:34:50 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Media
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_KeyType As String
        Dim m_KeyId As Guid?
        Dim m_KeyName As String
        Dim m_Data As Byte()
        Dim m_DataSize As Integer
        Dim m_DeviceName As String
        Dim m_UploadStamp As DateTime?
        Dim m_ProcessedStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._KeyType = ""
                ._KeyId = Nothing
                ._KeyName = ""
                ._Data = Nothing
                ._DataSize = 0
                ._DeviceName = ""
                ._UploadStamp = Nothing
                ._ProcessedStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._KeyType = ""
                ._KeyId = Nothing
                ._KeyName = ""
                ._Data = Nothing
                ._DataSize = 0
                ._DeviceName = ""
                ._UploadStamp = Nothing
                ._ProcessedStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")>
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@KeyType")>
        Public Property _KeyType() As String
            Get
                Return m_KeyType
            End Get
            Set(ByVal value As String)
                m_KeyType = value
            End Set
        End Property

        <StoredProcParameter("@KeyId")>
        Public Property _KeyId() As Guid?
            Get
                Return m_KeyId
            End Get
            Set(ByVal value As Guid?)
                m_KeyId = value
            End Set
        End Property

        <StoredProcParameter("@KeyName")>
        Public Property _KeyName() As String
            Get
                Return m_KeyName
            End Get
            Set(ByVal value As String)
                m_KeyName = value
            End Set
        End Property

        <StoredProcParameter("@Data")>
        Public Property _Data() As Byte()
            Get
                Return m_Data
            End Get
            Set(ByVal value As Byte())
                m_Data = value
            End Set
        End Property

        <StoredProcParameter("@DataSize")>
        Public Property _DataSize() As Integer
            Get
                Return m_DataSize
            End Get
            Set(ByVal value As Integer)
                m_DataSize = value
            End Set
        End Property

        <StoredProcParameter("@DeviceName")>
        Public Property _DeviceName() As String
            Get
                Return m_DeviceName
            End Get
            Set(ByVal value As String)
                m_DeviceName = value
            End Set
        End Property

        <StoredProcParameter("@UploadStamp")>
        Public Property _UploadStamp() As DateTime?
            Get
                Return m_UploadStamp
            End Get
            Set(ByVal value As DateTime?)
                m_UploadStamp = value
            End Set
        End Property

        <StoredProcParameter("@ProcessedStamp")>
        Public Property _ProcessedStamp() As DateTime?
            Get
                Return m_ProcessedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_ProcessedStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Media

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMediabyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Media

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getMediabyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Media)

            Dim _MediaList As List(Of Media)
            _MediaList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMediaTable"))
            Return _MediaList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Media)

            Dim _MediaList As List(Of Media)
            _MediaList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getMediaTable"))
            Return _MediaList

        End Function

        Public Shared Sub SaveAll(ByVal MediaList As List(Of Media))

            For Each _Media As Media In MediaList
                SaveRecord(_Media)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal MediaList As List(Of Media))

            For Each _Media As Media In MediaList
                SaveRecord(ConnectionString, _Media)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Media As Media) As Guid
            Media.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Media, "upsertMedia")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Media As Media) As Guid
            Media.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Media, "upsertMedia")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMediabyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteMediabyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMedia")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMedia")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Media

            Dim _M As Media = Nothing

            If DR IsNot Nothing Then
                _M = New Media()
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._DayId = GetGUID(DR("day_id"))
                    _M._KeyType = DR("key_type").ToString.Trim
                    _M._KeyId = GetGUID(DR("key_id"))
                    _M._KeyName = DR("key_name").ToString.Trim
                    _M._Data = GetImage(DR("data"))
                    _M._DataSize = GetInteger(DR("data_size"))
                    _M._DeviceName = DR("device_name").ToString.Trim
                    _M._UploadStamp = GetDateTime(DR("upload_stamp"))
                    _M._ProcessedStamp = GetDateTime(DR("processed_stamp"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Media)

            Dim _MediaList As New List(Of Media)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MediaList.Add(PropertiesFromData(_DR))
            Next

            Return _MediaList

        End Function


#End Region

    End Class

End Namespace
