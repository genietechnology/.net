﻿'*****************************************************
'Generated 30/09/2016 22:40:36 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Booking
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_BookingDate As Date?
        Dim m_BookingFrom As TimeSpan?
        Dim m_BookingTo As TimeSpan?
        Dim m_BookingStatus As String
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_ChildDob As Date?
        Dim m_ChildAge As Byte
        Dim m_Points As Byte
        Dim m_RatioId As Guid?
        Dim m_SessionCount As Byte
        Dim m_Hours As Decimal
        Dim m_FundedHours As Decimal
        Dim m_Am As Integer
        Dim m_Pm As Integer
        Dim m_Breakfast As Integer
        Dim m_Lunch As Integer
        Dim m_Tea As Integer
        Dim m_Snacks As Integer
        Dim m_FullTime As Integer
        Dim m_TariffId As Guid?
        Dim m_TariffName As String
        Dim m_TariffRate As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._SiteName = ""
                ._BookingDate = Nothing
                ._BookingFrom = Nothing
                ._BookingTo = Nothing
                ._BookingStatus = ""
                ._ChildId = Nothing
                ._ChildName = ""
                ._ChildDob = Nothing
                ._ChildAge = Nothing
                ._Points = Nothing
                ._RatioId = Nothing
                ._SessionCount = Nothing
                ._Hours = 0
                ._FundedHours = 0
                ._Am = 0
                ._Pm = 0
                ._Breakfast = 0
                ._Lunch = 0
                ._Tea = 0
                ._Snacks = 0
                ._FullTime = 0
                ._TariffId = Nothing
                ._TariffName = ""
                ._TariffRate = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._SiteName = ""
                ._BookingDate = Nothing
                ._BookingFrom = Nothing
                ._BookingTo = Nothing
                ._BookingStatus = ""
                ._ChildId = Nothing
                ._ChildName = ""
                ._ChildDob = Nothing
                ._ChildAge = Nothing
                ._Points = Nothing
                ._RatioId = Nothing
                ._SessionCount = Nothing
                ._Hours = 0
                ._FundedHours = 0
                ._Am = 0
                ._Pm = 0
                ._Breakfast = 0
                ._Lunch = 0
                ._Tea = 0
                ._Snacks = 0
                ._FullTime = 0
                ._TariffId = Nothing
                ._TariffName = ""
                ._TariffRate = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")> _
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@BookingDate")> _
        Public Property _BookingDate() As Date?
            Get
                Return m_BookingDate
            End Get
            Set(ByVal value As Date?)
                m_BookingDate = value
            End Set
        End Property

        <StoredProcParameter("@BookingFrom")> _
        Public Property _BookingFrom() As TimeSpan?
            Get
                Return m_BookingFrom
            End Get
            Set(ByVal value As TimeSpan?)
                m_BookingFrom = value
            End Set
        End Property

        <StoredProcParameter("@BookingTo")> _
        Public Property _BookingTo() As TimeSpan?
            Get
                Return m_BookingTo
            End Get
            Set(ByVal value As TimeSpan?)
                m_BookingTo = value
            End Set
        End Property

        <StoredProcParameter("@BookingStatus")> _
        Public Property _BookingStatus() As String
            Get
                Return m_BookingStatus
            End Get
            Set(ByVal value As String)
                m_BookingStatus = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@ChildDob")> _
        Public Property _ChildDob() As Date?
            Get
                Return m_ChildDob
            End Get
            Set(ByVal value As Date?)
                m_ChildDob = value
            End Set
        End Property

        <StoredProcParameter("@ChildAge")> _
        Public Property _ChildAge() As Byte
            Get
                Return m_ChildAge
            End Get
            Set(ByVal value As Byte)
                m_ChildAge = value
            End Set
        End Property

        <StoredProcParameter("@Points")> _
        Public Property _Points() As Byte
            Get
                Return m_Points
            End Get
            Set(ByVal value As Byte)
                m_Points = value
            End Set
        End Property

        <StoredProcParameter("@RatioId")> _
        Public Property _RatioId() As Guid?
            Get
                Return m_RatioId
            End Get
            Set(ByVal value As Guid?)
                m_RatioId = value
            End Set
        End Property

        <StoredProcParameter("@SessionCount")> _
        Public Property _SessionCount() As Byte
            Get
                Return m_SessionCount
            End Get
            Set(ByVal value As Byte)
                m_SessionCount = value
            End Set
        End Property

        <StoredProcParameter("@Hours")> _
        Public Property _Hours() As Decimal
            Get
                Return m_Hours
            End Get
            Set(ByVal value As Decimal)
                m_Hours = value
            End Set
        End Property

        <StoredProcParameter("@FundedHours")> _
        Public Property _FundedHours() As Decimal
            Get
                Return m_FundedHours
            End Get
            Set(ByVal value As Decimal)
                m_FundedHours = value
            End Set
        End Property

        <StoredProcParameter("@Am")> _
        Public Property _Am() As Integer
            Get
                Return m_Am
            End Get
            Set(ByVal value As Integer)
                m_Am = value
            End Set
        End Property

        <StoredProcParameter("@Pm")> _
        Public Property _Pm() As Integer
            Get
                Return m_Pm
            End Get
            Set(ByVal value As Integer)
                m_Pm = value
            End Set
        End Property

        <StoredProcParameter("@Breakfast")> _
        Public Property _Breakfast() As Integer
            Get
                Return m_Breakfast
            End Get
            Set(ByVal value As Integer)
                m_Breakfast = value
            End Set
        End Property

        <StoredProcParameter("@Lunch")> _
        Public Property _Lunch() As Integer
            Get
                Return m_Lunch
            End Get
            Set(ByVal value As Integer)
                m_Lunch = value
            End Set
        End Property

        <StoredProcParameter("@Tea")> _
        Public Property _Tea() As Integer
            Get
                Return m_Tea
            End Get
            Set(ByVal value As Integer)
                m_Tea = value
            End Set
        End Property

        <StoredProcParameter("@Snacks")> _
        Public Property _Snacks() As Integer
            Get
                Return m_Snacks
            End Get
            Set(ByVal value As Integer)
                m_Snacks = value
            End Set
        End Property

        <StoredProcParameter("@FullTime")> _
        Public Property _FullTime() As Integer
            Get
                Return m_FullTime
            End Get
            Set(ByVal value As Integer)
                m_FullTime = value
            End Set
        End Property

        <StoredProcParameter("@TariffId")> _
        Public Property _TariffId() As Guid?
            Get
                Return m_TariffId
            End Get
            Set(ByVal value As Guid?)
                m_TariffId = value
            End Set
        End Property

        <StoredProcParameter("@TariffName")> _
        Public Property _TariffName() As String
            Get
                Return m_TariffName
            End Get
            Set(ByVal value As String)
                m_TariffName = value
            End Set
        End Property

        <StoredProcParameter("@TariffRate")> _
        Public Property _TariffRate() As Decimal
            Get
                Return m_TariffRate
            End Get
            Set(ByVal value As Decimal)
                m_TariffRate = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Booking

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getBookingbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Booking)

            Dim _BookingList As List(Of Booking)
            _BookingList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getBookingTable"))
            Return _BookingList

        End Function

        Public Shared Sub SaveAll(ByVal BookingList As List(Of Booking))

            For Each _Booking As Booking In BookingList
                SaveRecord(_Booking)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Booking As Booking) As Guid
            Booking.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Booking, "upsertBooking")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteBookingbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertBooking")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Booking

            Dim _B As Booking = Nothing

            If DR IsNot Nothing Then
                _B = New Booking()
                With DR
                    _B.IsNew = False
                    _B.IsDeleted = False
                    _B._ID = GetGUID(DR("ID"))
                    _B._SiteId = GetGUID(DR("site_id"))
                    _B._SiteName = DR("site_name").ToString.Trim
                    _B._BookingDate = GetDate(DR("booking_date"))
                    _B._BookingFrom = GetTimeSpan(DR("booking_from"))
                    _B._BookingTo = GetTimeSpan(DR("booking_to"))
                    _B._BookingStatus = DR("booking_status").ToString.Trim
                    _B._ChildId = GetGUID(DR("child_id"))
                    _B._ChildName = DR("child_name").ToString.Trim
                    _B._ChildDob = GetDate(DR("child_dob"))
                    _B._ChildAge = GetByte(DR("child_age"))
                    _B._Points = GetByte(DR("points"))
                    _B._RatioId = GetGUID(DR("ratio_id"))
                    _B._SessionCount = GetByte(DR("session_count"))
                    _B._Hours = GetDecimal(DR("hours"))
                    _B._FundedHours = GetDecimal(DR("funded_hours"))
                    _B._Am = GetInteger(DR("am"))
                    _B._Pm = GetInteger(DR("pm"))
                    _B._Breakfast = GetInteger(DR("breakfast"))
                    _B._Lunch = GetInteger(DR("lunch"))
                    _B._Tea = GetInteger(DR("tea"))
                    _B._Snacks = GetInteger(DR("snacks"))
                    _B._FullTime = GetInteger(DR("full_time"))
                    _B._TariffId = GetGUID(DR("tariff_id"))
                    _B._TariffName = DR("tariff_name").ToString.Trim
                    _B._TariffRate = GetDecimal(DR("tariff_rate"))

                End With
            End If

            Return _B

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Booking)

            Dim _BookingList As New List(Of Booking)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _BookingList.Add(PropertiesFromData(_DR))
            Next

            Return _BookingList

        End Function


#End Region

    End Class

End Namespace
