﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CDAPDefinition
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Area As String
        Dim m_Step As Integer
        Dim m_Letter As String
        Dim m_Stepletter As String
        Dim m_Name As String
        Dim m_Description As String
        Dim m_How As String
        Dim m_Examples As String
        Dim m_ProgOutcomes As String
        Dim m_Creativity As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Area = ""
                ._Step = 0
                ._Letter = ""
                ._Stepletter = ""
                ._Name = ""
                ._Description = ""
                ._How = ""
                ._Examples = ""
                ._ProgOutcomes = ""
                ._Creativity = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Area = ""
                ._Step = 0
                ._Letter = ""
                ._Stepletter = ""
                ._Name = ""
                ._Description = ""
                ._How = ""
                ._Examples = ""
                ._ProgOutcomes = ""
                ._Creativity = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Area")> _
        Public Property _Area() As String
            Get
                Return m_Area
            End Get
            Set(ByVal value As String)
                m_Area = value
            End Set
        End Property

        <StoredProcParameter("@Step")> _
        Public Property _Step() As Integer
            Get
                Return m_Step
            End Get
            Set(ByVal value As Integer)
                m_Step = value
            End Set
        End Property

        <StoredProcParameter("@Letter")> _
        Public Property _Letter() As String
            Get
                Return m_Letter
            End Get
            Set(ByVal value As String)
                m_Letter = value
            End Set
        End Property

        <StoredProcParameter("@Stepletter")> _
        Public Property _Stepletter() As String
            Get
                Return m_Stepletter
            End Get
            Set(ByVal value As String)
                m_Stepletter = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@How")> _
        Public Property _How() As String
            Get
                Return m_How
            End Get
            Set(ByVal value As String)
                m_How = value
            End Set
        End Property

        <StoredProcParameter("@Examples")> _
        Public Property _Examples() As String
            Get
                Return m_Examples
            End Get
            Set(ByVal value As String)
                m_Examples = value
            End Set
        End Property

        <StoredProcParameter("@ProgOutcomes")> _
        Public Property _ProgOutcomes() As String
            Get
                Return m_ProgOutcomes
            End Get
            Set(ByVal value As String)
                m_ProgOutcomes = value
            End Set
        End Property

        <StoredProcParameter("@Creativity")> _
        Public Property _Creativity() As String
            Get
                Return m_Creativity
            End Get
            Set(ByVal value As String)
                m_Creativity = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CDAPDefinition

            Dim _CDAPDefinition As CDAPDefinition
            _CDAPDefinition = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCDAPDefinitionbyID", ID))
            Return _CDAPDefinition

        End Function

        Public Shared Function RetreiveAll() As List(Of CDAPDefinition)

            Dim _CDAPDefinitionList As List(Of CDAPDefinition)
            _CDAPDefinitionList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCDAPDefinitionTable"))
            Return _CDAPDefinitionList

        End Function

        Public Shared Sub SaveAll(ByVal CDAPDefinitionList As List(Of CDAPDefinition))

            For Each _CDAPDefinition As CDAPDefinition In CDAPDefinitionList
                SaveRecord(_CDAPDefinition)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CDAPDefinition As CDAPDefinition) As Guid
            DAL.SaveRecord(Session.ConnectionManager, CDAPDefinition, "upsertCDAPDefinition")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCDAPDefinitionbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCDAPDefinition")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CDAPDefinition

            Dim _C As New CDAPDefinition()

            If DR IsNot Nothing Then
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Area = DR("area").ToString.Trim
                    _C._Step = GetInteger(DR("step"))
                    _C._Letter = DR("letter").ToString.Trim
                    _C._Stepletter = DR("stepletter").ToString.Trim
                    _C._Name = DR("name").ToString.Trim
                    _C._Description = DR("description").ToString.Trim
                    _C._How = DR("how").ToString.Trim
                    _C._Examples = DR("examples").ToString.Trim
                    _C._ProgOutcomes = DR("prog_outcomes").ToString.Trim
                    _C._Creativity = DR("creativity").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CDAPDefinition)

            Dim _CDAPDefinitionList As New List(Of CDAPDefinition)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CDAPDefinitionList.Add(PropertiesFromData(_DR))
            Next

            Return _CDAPDefinitionList

        End Function


#End Region

    End Class

End Namespace
