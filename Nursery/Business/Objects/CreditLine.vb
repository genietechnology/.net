﻿'*****************************************************
'Generated 09/01/2018 19:56:52 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CreditLine
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_CreditId As Guid?
        Dim m_LineNo As Long
        Dim m_ActionDate As Date?
        Dim m_Description As String
        Dim m_Ref1 As String
        Dim m_Ref2 As String
        Dim m_Ref3 As String
        Dim m_Value As Decimal
        Dim m_Hours As Decimal
        Dim m_FundedValue As Decimal
        Dim m_FundedHours As Decimal
        Dim m_NlCode As String
        Dim m_NlTracking As String
        Dim m_ChangedBy As Guid?
        Dim m_ChangedStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._CreditId = Nothing
                ._LineNo = 0
                ._ActionDate = Nothing
                ._Description = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Value = 0
                ._Hours = 0
                ._FundedValue = 0
                ._FundedHours = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._CreditId = Nothing
                ._LineNo = 0
                ._ActionDate = Nothing
                ._Description = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Value = 0
                ._Hours = 0
                ._FundedValue = 0
                ._FundedHours = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@CreditId")>
        Public Property _CreditId() As Guid?
            Get
                Return m_CreditId
            End Get
            Set(ByVal value As Guid?)
                m_CreditId = value
            End Set
        End Property

        <StoredProcParameter("@LineNo")>
        Public Property _LineNo() As Long
            Get
                Return m_LineNo
            End Get
            Set(ByVal value As Long)
                m_LineNo = value
            End Set
        End Property

        <StoredProcParameter("@ActionDate")>
        Public Property _ActionDate() As Date?
            Get
                Return m_ActionDate
            End Get
            Set(ByVal value As Date?)
                m_ActionDate = value
            End Set
        End Property

        <StoredProcParameter("@Description")>
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@Ref1")>
        Public Property _Ref1() As String
            Get
                Return m_Ref1
            End Get
            Set(ByVal value As String)
                m_Ref1 = value
            End Set
        End Property

        <StoredProcParameter("@Ref2")>
        Public Property _Ref2() As String
            Get
                Return m_Ref2
            End Get
            Set(ByVal value As String)
                m_Ref2 = value
            End Set
        End Property

        <StoredProcParameter("@Ref3")>
        Public Property _Ref3() As String
            Get
                Return m_Ref3
            End Get
            Set(ByVal value As String)
                m_Ref3 = value
            End Set
        End Property

        <StoredProcParameter("@Value")>
        Public Property _Value() As Decimal
            Get
                Return m_Value
            End Get
            Set(ByVal value As Decimal)
                m_Value = value
            End Set
        End Property

        <StoredProcParameter("@Hours")>
        Public Property _Hours() As Decimal
            Get
                Return m_Hours
            End Get
            Set(ByVal value As Decimal)
                m_Hours = value
            End Set
        End Property

        <StoredProcParameter("@FundedValue")>
        Public Property _FundedValue() As Decimal
            Get
                Return m_FundedValue
            End Get
            Set(ByVal value As Decimal)
                m_FundedValue = value
            End Set
        End Property

        <StoredProcParameter("@FundedHours")>
        Public Property _FundedHours() As Decimal
            Get
                Return m_FundedHours
            End Get
            Set(ByVal value As Decimal)
                m_FundedHours = value
            End Set
        End Property

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String
            Get
                Return m_NlCode
            End Get
            Set(ByVal value As String)
                m_NlCode = value
            End Set
        End Property

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String
            Get
                Return m_NlTracking
            End Get
            Set(ByVal value As String)
                m_NlTracking = value
            End Set
        End Property

        <StoredProcParameter("@ChangedBy")>
        Public Property _ChangedBy() As Guid?
            Get
                Return m_ChangedBy
            End Get
            Set(ByVal value As Guid?)
                m_ChangedBy = value
            End Set
        End Property

        <StoredProcParameter("@ChangedStamp")>
        Public Property _ChangedStamp() As DateTime?
            Get
                Return m_ChangedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_ChangedStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CreditLine

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCreditLinebyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CreditLine

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCreditLinebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CreditLine)

            Dim _CreditLineList As List(Of CreditLine)
            _CreditLineList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCreditLineTable"))
            Return _CreditLineList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CreditLine)

            Dim _CreditLineList As List(Of CreditLine)
            _CreditLineList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCreditLineTable"))
            Return _CreditLineList

        End Function

        Public Shared Sub SaveAll(ByVal CreditLineList As List(Of CreditLine))

            For Each _CreditLine As CreditLine In CreditLineList
                SaveRecord(_CreditLine)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CreditLineList As List(Of CreditLine))

            For Each _CreditLine As CreditLine In CreditLineList
                SaveRecord(ConnectionString, _CreditLine)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CreditLine As CreditLine) As Guid
            CreditLine.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CreditLine, "upsertCreditLine")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CreditLine As CreditLine) As Guid
            CreditLine.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CreditLine, "upsertCreditLine")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCreditLinebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCreditLinebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCreditLine")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCreditLine")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CreditLine

            Dim _C As CreditLine = Nothing

            If DR IsNot Nothing Then
                _C = New CreditLine()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._CreditId = GetGUID(DR("credit_id"))
                    _C._LineNo = GetLong(DR("line_no"))
                    _C._ActionDate = GetDate(DR("action_date"))
                    _C._Description = DR("description").ToString.Trim
                    _C._Ref1 = DR("ref_1").ToString.Trim
                    _C._Ref2 = DR("ref_2").ToString.Trim
                    _C._Ref3 = DR("ref_3").ToString.Trim
                    _C._Value = GetDecimal(DR("value"))
                    _C._Hours = GetDecimal(DR("hours"))
                    _C._FundedValue = GetDecimal(DR("funded_value"))
                    _C._FundedHours = GetDecimal(DR("funded_hours"))
                    _C._NlCode = DR("nl_code").ToString.Trim
                    _C._NlTracking = DR("nl_tracking").ToString.Trim
                    _C._ChangedBy = GetGUID(DR("changed_by"))
                    _C._ChangedStamp = GetDateTime(DR("changed_stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CreditLine)

            Dim _CreditLineList As New List(Of CreditLine)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CreditLineList.Add(PropertiesFromData(_DR))
            Next

            Return _CreditLineList

        End Function


#End Region

    End Class

End Namespace
