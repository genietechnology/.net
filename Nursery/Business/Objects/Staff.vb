﻿'*****************************************************
'Generated 24/09/2018 09:47:30 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Staff
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Status = ""
                ._Fullname = ""
                ._Forename = ""
                ._Surname = ""
                ._DateStarted = Nothing
                ._DateLeft = Nothing
                ._Dob = Nothing
                ._Nino = ""
                ._WelshSpeaker = False
                ._CrbDue = Nothing
                ._CrbLast = Nothing
                ._CarDriver = False
                ._CarOwner = False
                ._CarReg = ""
                ._CarDetails = ""
                ._CarInsChecked = Nothing
                ._Keyworker = False
                ._JobTitle = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._Address = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._IceName = ""
                ._IceRel = ""
                ._IceTel = ""
                ._IceMobile = ""
                ._Email = ""
                ._ContractType = ""
                ._ClockNo = ""
                ._ClockName = ""
                ._ClockPerms = ""
                ._ClockGroup = ""
                ._PayrollId = ""
                ._TaxCode = ""
                ._NiCat = ""
                ._SalaryUnit = ""
                ._SalaryValue = 0
                ._SalaryHour = 0
                ._HoursFte = 0
                ._HoursContracted = 0
                ._Photo = Nothing
                ._TouchPin = ""
                ._CrbRef = ""
                ._Religion = ""
                ._Ethnicity = ""
                ._Nationality = ""
                ._Language = ""
                ._Fa = False
                ._FaPaed = False
                ._FaForest = False
                ._Forest = False
                ._FoodHygiene = False
                ._Senco = False
                ._Enco = False
                ._Safeguard = False
                ._QualLevel = Nothing
                ._SiteId = Nothing
                ._SiteName = ""
                ._Coordinates = ""
                ._BradFrom = Nothing
                ._BradScore = 0
                ._LineManager = False
                ._Department = ""
                ._ReportsTo = ""
                ._ApplicantId = Nothing
                ._HolsFrom = ""
                ._HolsEntitlement = 0
                ._HolsTaken = 0
                ._SickTaken = 0
                ._AbsTaken = 0
                ._DbsCheckType = ""
                ._DbsIssued = Nothing
                ._DbsPosition = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Status = ""
                ._Fullname = ""
                ._Forename = ""
                ._Surname = ""
                ._DateStarted = Nothing
                ._DateLeft = Nothing
                ._Dob = Nothing
                ._Nino = ""
                ._WelshSpeaker = False
                ._CrbDue = Nothing
                ._CrbLast = Nothing
                ._CarDriver = False
                ._CarOwner = False
                ._CarReg = ""
                ._CarDetails = ""
                ._CarInsChecked = Nothing
                ._Keyworker = False
                ._JobTitle = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._Address = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._IceName = ""
                ._IceRel = ""
                ._IceTel = ""
                ._IceMobile = ""
                ._Email = ""
                ._ContractType = ""
                ._ClockNo = ""
                ._ClockName = ""
                ._ClockPerms = ""
                ._ClockGroup = ""
                ._PayrollId = ""
                ._TaxCode = ""
                ._NiCat = ""
                ._SalaryUnit = ""
                ._SalaryValue = 0
                ._SalaryHour = 0
                ._HoursFte = 0
                ._HoursContracted = 0
                ._Photo = Nothing
                ._TouchPin = ""
                ._CrbRef = ""
                ._Religion = ""
                ._Ethnicity = ""
                ._Nationality = ""
                ._Language = ""
                ._Fa = False
                ._FaPaed = False
                ._FaForest = False
                ._Forest = False
                ._FoodHygiene = False
                ._Senco = False
                ._Enco = False
                ._Safeguard = False
                ._QualLevel = Nothing
                ._SiteId = Nothing
                ._SiteName = ""
                ._Coordinates = ""
                ._BradFrom = Nothing
                ._BradScore = 0
                ._LineManager = False
                ._Department = ""
                ._ReportsTo = ""
                ._ApplicantId = Nothing
                ._HolsFrom = ""
                ._HolsEntitlement = 0
                ._HolsTaken = 0
                ._SickTaken = 0
                ._AbsTaken = 0
                ._DbsCheckType = ""
                ._DbsIssued = Nothing
                ._DbsPosition = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Status")>
        Public Property _Status() As String

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String

        <StoredProcParameter("@DateStarted")>
        Public Property _DateStarted() As Date?

        <StoredProcParameter("@DateLeft")>
        Public Property _DateLeft() As Date?

        <StoredProcParameter("@Dob")>
        Public Property _Dob() As Date?

        <StoredProcParameter("@Nino")>
        Public Property _Nino() As String

        <StoredProcParameter("@WelshSpeaker")>
        Public Property _WelshSpeaker() As Boolean

        <StoredProcParameter("@CrbDue")>
        Public Property _CrbDue() As Date?

        <StoredProcParameter("@CrbLast")>
        Public Property _CrbLast() As Date?

        <StoredProcParameter("@CarDriver")>
        Public Property _CarDriver() As Boolean

        <StoredProcParameter("@CarOwner")>
        Public Property _CarOwner() As Boolean

        <StoredProcParameter("@CarReg")>
        Public Property _CarReg() As String

        <StoredProcParameter("@CarDetails")>
        Public Property _CarDetails() As String

        <StoredProcParameter("@CarInsChecked")>
        Public Property _CarInsChecked() As Date?

        <StoredProcParameter("@Keyworker")>
        Public Property _Keyworker() As Boolean

        <StoredProcParameter("@JobTitle")>
        Public Property _JobTitle() As String

        <StoredProcParameter("@GroupId")>
        Public Property _GroupId() As Guid?

        <StoredProcParameter("@GroupName")>
        Public Property _GroupName() As String

        <StoredProcParameter("@Add1")>
        Public Property _Add1() As String

        <StoredProcParameter("@Add2")>
        Public Property _Add2() As String

        <StoredProcParameter("@Add3")>
        Public Property _Add3() As String

        <StoredProcParameter("@Add4")>
        Public Property _Add4() As String

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String

        <StoredProcParameter("@Address")>
        Public Property _Address() As String

        <StoredProcParameter("@TelHome")>
        Public Property _TelHome() As String

        <StoredProcParameter("@TelMobile")>
        Public Property _TelMobile() As String

        <StoredProcParameter("@IceName")>
        Public Property _IceName() As String

        <StoredProcParameter("@IceRel")>
        Public Property _IceRel() As String

        <StoredProcParameter("@IceTel")>
        Public Property _IceTel() As String

        <StoredProcParameter("@IceMobile")>
        Public Property _IceMobile() As String

        <StoredProcParameter("@Email")>
        Public Property _Email() As String

        <StoredProcParameter("@ContractType")>
        Public Property _ContractType() As String

        <StoredProcParameter("@ClockNo")>
        Public Property _ClockNo() As String

        <StoredProcParameter("@ClockName")>
        Public Property _ClockName() As String

        <StoredProcParameter("@ClockPerms")>
        Public Property _ClockPerms() As String

        <StoredProcParameter("@ClockGroup")>
        Public Property _ClockGroup() As String

        <StoredProcParameter("@PayrollId")>
        Public Property _PayrollId() As String

        <StoredProcParameter("@TaxCode")>
        Public Property _TaxCode() As String

        <StoredProcParameter("@NiCat")>
        Public Property _NiCat() As String

        <StoredProcParameter("@SalaryUnit")>
        Public Property _SalaryUnit() As String

        <StoredProcParameter("@SalaryValue")>
        Public Property _SalaryValue() As Decimal

        <StoredProcParameter("@SalaryHour")>
        Public Property _SalaryHour() As Decimal

        <StoredProcParameter("@HoursFte")>
        Public Property _HoursFte() As Decimal

        <StoredProcParameter("@HoursContracted")>
        Public Property _HoursContracted() As Decimal

        <StoredProcParameter("@Photo")>
        Public Property _Photo() As Guid?

        <StoredProcParameter("@TouchPin")>
        Public Property _TouchPin() As String

        <StoredProcParameter("@CrbRef")>
        Public Property _CrbRef() As String

        <StoredProcParameter("@Religion")>
        Public Property _Religion() As String

        <StoredProcParameter("@Ethnicity")>
        Public Property _Ethnicity() As String

        <StoredProcParameter("@Nationality")>
        Public Property _Nationality() As String

        <StoredProcParameter("@Language")>
        Public Property _Language() As String

        <StoredProcParameter("@Fa")>
        Public Property _Fa() As Boolean

        <StoredProcParameter("@FaPaed")>
        Public Property _FaPaed() As Boolean

        <StoredProcParameter("@FaForest")>
        Public Property _FaForest() As Boolean

        <StoredProcParameter("@Forest")>
        Public Property _Forest() As Boolean

        <StoredProcParameter("@FoodHygiene")>
        Public Property _FoodHygiene() As Boolean

        <StoredProcParameter("@Senco")>
        Public Property _Senco() As Boolean

        <StoredProcParameter("@Enco")>
        Public Property _Enco() As Boolean

        <StoredProcParameter("@Safeguard")>
        Public Property _Safeguard() As Boolean

        <StoredProcParameter("@QualLevel")>
        Public Property _QualLevel() As Byte

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@Coordinates")>
        Public Property _Coordinates() As String

        <StoredProcParameter("@BradFrom")>
        Public Property _BradFrom() As Date?

        <StoredProcParameter("@BradScore")>
        Public Property _BradScore() As Integer

        <StoredProcParameter("@LineManager")>
        Public Property _LineManager() As Boolean

        <StoredProcParameter("@Department")>
        Public Property _Department() As String

        <StoredProcParameter("@ReportsTo")>
        Public Property _ReportsTo() As String

        <StoredProcParameter("@ApplicantId")>
        Public Property _ApplicantId() As Guid?

        <StoredProcParameter("@HolsFrom")>
        Public Property _HolsFrom() As String

        <StoredProcParameter("@HolsEntitlement")>
        Public Property _HolsEntitlement() As Decimal

        <StoredProcParameter("@HolsTaken")>
        Public Property _HolsTaken() As Decimal

        <StoredProcParameter("@SickTaken")>
        Public Property _SickTaken() As Decimal

        <StoredProcParameter("@AbsTaken")>
        Public Property _AbsTaken() As Decimal

        <StoredProcParameter("@DbsCheckType")>
        Public Property _DbsCheckType() As String

        <StoredProcParameter("@DbsIssued")>
        Public Property _DbsIssued() As Date?

        <StoredProcParameter("@DbsPosition")>
        Public Property _DbsPosition() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Staff
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Staff
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getStaffbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Staff)
            Dim _StaffList As List(Of Staff)
            _StaffList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffTable"))
            Return _StaffList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Staff)
            Dim _StaffList As List(Of Staff)
            _StaffList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getStaffTable"))
            Return _StaffList
        End Function

        Public Shared Sub SaveAll(ByVal StaffList As List(Of Staff))
            For Each _Staff As Staff In StaffList
                SaveRecord(_Staff)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal StaffList As List(Of Staff))
            For Each _Staff As Staff In StaffList
                SaveRecord(ConnectionString, _Staff)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Staff As Staff) As Guid
            Dim _Current As Staff = RetreiveByID(Staff._ID.Value)
            Staff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Staff, _Current, "upsertStaff")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Staff As Staff) As Guid
            Dim _Current As Staff = RetreiveByID(ConnectionString, Staff._ID.Value)
            Staff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Staff, _Current, "upsertStaff")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Staff = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteStaffbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Staff = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteStaffbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Staff = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertStaff")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Staff = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertStaff")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Staff

            Dim _S As Staff = Nothing

            If DR IsNot Nothing Then
                _S = New Staff()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._Status = DR("status").ToString.Trim
                    _S._Fullname = DR("fullname").ToString.Trim
                    _S._Forename = DR("forename").ToString.Trim
                    _S._Surname = DR("surname").ToString.Trim
                    _S._DateStarted = GetDate(DR("date_started"))
                    _S._DateLeft = GetDate(DR("date_left"))
                    _S._Dob = GetDate(DR("dob"))
                    _S._Nino = DR("nino").ToString.Trim
                    _S._WelshSpeaker = GetBoolean(DR("welsh_speaker"))
                    _S._CrbDue = GetDate(DR("crb_due"))
                    _S._CrbLast = GetDate(DR("crb_last"))
                    _S._CarDriver = GetBoolean(DR("car_driver"))
                    _S._CarOwner = GetBoolean(DR("car_owner"))
                    _S._CarReg = DR("car_reg").ToString.Trim
                    _S._CarDetails = DR("car_details").ToString.Trim
                    _S._CarInsChecked = GetDate(DR("car_ins_checked"))
                    _S._Keyworker = GetBoolean(DR("keyworker"))
                    _S._JobTitle = DR("job_title").ToString.Trim
                    _S._GroupId = GetGUID(DR("group_id"))
                    _S._GroupName = DR("group_name").ToString.Trim
                    _S._Add1 = DR("add1").ToString.Trim
                    _S._Add2 = DR("add2").ToString.Trim
                    _S._Add3 = DR("add3").ToString.Trim
                    _S._Add4 = DR("add4").ToString.Trim
                    _S._Postcode = DR("postcode").ToString.Trim
                    _S._Address = DR("address").ToString.Trim
                    _S._TelHome = DR("tel_home").ToString.Trim
                    _S._TelMobile = DR("tel_mobile").ToString.Trim
                    _S._IceName = DR("ice_name").ToString.Trim
                    _S._IceRel = DR("ice_rel").ToString.Trim
                    _S._IceTel = DR("ice_tel").ToString.Trim
                    _S._IceMobile = DR("ice_mobile").ToString.Trim
                    _S._Email = DR("email").ToString.Trim
                    _S._ContractType = DR("contract_type").ToString.Trim
                    _S._ClockNo = DR("clock_no").ToString.Trim
                    _S._ClockName = DR("clock_name").ToString.Trim
                    _S._ClockPerms = DR("clock_perms").ToString.Trim
                    _S._ClockGroup = DR("clock_group").ToString.Trim
                    _S._PayrollId = DR("payroll_id").ToString.Trim
                    _S._TaxCode = DR("tax_code").ToString.Trim
                    _S._NiCat = DR("ni_cat").ToString.Trim
                    _S._SalaryUnit = DR("salary_unit").ToString.Trim
                    _S._SalaryValue = GetDecimal(DR("salary_value"))
                    _S._SalaryHour = GetDecimal(DR("salary_hour"))
                    _S._HoursFte = GetDecimal(DR("hours_fte"))
                    _S._HoursContracted = GetDecimal(DR("hours_contracted"))
                    _S._Photo = GetGUID(DR("photo"))
                    _S._TouchPin = DR("touch_pin").ToString.Trim
                    _S._CrbRef = DR("crb_ref").ToString.Trim
                    _S._Religion = DR("religion").ToString.Trim
                    _S._Ethnicity = DR("ethnicity").ToString.Trim
                    _S._Nationality = DR("nationality").ToString.Trim
                    _S._Language = DR("language").ToString.Trim
                    _S._Fa = GetBoolean(DR("fa"))
                    _S._FaPaed = GetBoolean(DR("fa_paed"))
                    _S._FaForest = GetBoolean(DR("fa_forest"))
                    _S._Forest = GetBoolean(DR("forest"))
                    _S._FoodHygiene = GetBoolean(DR("food_hygiene"))
                    _S._Senco = GetBoolean(DR("senco"))
                    _S._Enco = GetBoolean(DR("enco"))
                    _S._Safeguard = GetBoolean(DR("safeguard"))
                    _S._QualLevel = GetByte(DR("qual_level"))
                    _S._SiteId = GetGUID(DR("site_id"))
                    _S._SiteName = DR("site_name").ToString.Trim
                    _S._Coordinates = DR("coordinates").ToString.Trim
                    _S._BradFrom = GetDate(DR("brad_from"))
                    _S._BradScore = GetInteger(DR("brad_score"))
                    _S._LineManager = GetBoolean(DR("line_manager"))
                    _S._Department = DR("department").ToString.Trim
                    _S._ReportsTo = DR("reports_to").ToString.Trim
                    _S._ApplicantId = GetGUID(DR("applicant_id"))
                    _S._HolsFrom = DR("hols_from").ToString.Trim
                    _S._HolsEntitlement = GetDecimal(DR("hols_entitlement"))
                    _S._HolsTaken = GetDecimal(DR("hols_taken"))
                    _S._SickTaken = GetDecimal(DR("sick_taken"))
                    _S._AbsTaken = GetDecimal(DR("abs_taken"))
                    _S._DbsCheckType = DR("dbs_check_type").ToString.Trim
                    _S._DbsIssued = GetDate(DR("dbs_issued"))
                    _S._DbsPosition = DR("dbs_position").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Staff)

            Dim _StaffList As New List(Of Staff)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffList

        End Function


#End Region

    End Class

End Namespace
