﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class InspGroup
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_MonthsMin As Integer
        Dim m_MonthsMax As Integer
        Dim m_ChildCapacity As Integer
        Dim m_StaffRatio As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._MonthsMin = 0
                ._MonthsMax = 0
                ._ChildCapacity = 0
                ._StaffRatio = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._MonthsMin = 0
                ._MonthsMax = 0
                ._ChildCapacity = 0
                ._StaffRatio = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@MonthsMin")> _
        Public Property _MonthsMin() As Integer
            Get
                Return m_MonthsMin
            End Get
            Set(ByVal value As Integer)
                m_MonthsMin = value
            End Set
        End Property

        <StoredProcParameter("@MonthsMax")> _
        Public Property _MonthsMax() As Integer
            Get
                Return m_MonthsMax
            End Get
            Set(ByVal value As Integer)
                m_MonthsMax = value
            End Set
        End Property

        <StoredProcParameter("@ChildCapacity")> _
        Public Property _ChildCapacity() As Integer
            Get
                Return m_ChildCapacity
            End Get
            Set(ByVal value As Integer)
                m_ChildCapacity = value
            End Set
        End Property

        <StoredProcParameter("@StaffRatio")> _
        Public Property _StaffRatio() As Integer
            Get
                Return m_StaffRatio
            End Get
            Set(ByVal value As Integer)
                m_StaffRatio = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As InspGroup

            Dim _InspGroup As InspGroup
            _InspGroup = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInspGroupbyID", ID))
            Return _InspGroup

        End Function

        Public Shared Function RetreiveAll() As List(Of InspGroup)

            Dim _InspGroupList As List(Of InspGroup)
            _InspGroupList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInspGroupTable"))
            Return _InspGroupList

        End Function

        Public Shared Sub SaveAll(ByVal InspGroupList As List(Of InspGroup))

            For Each _InspGroup As InspGroup In InspGroupList
                SaveRecord(_InspGroup)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal InspGroup As InspGroup) As Guid
            DAL.SaveRecord(Session.ConnectionManager, InspGroup, "upsertInspGroup")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteInspGroupbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertInspGroup")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As InspGroup

            Dim _I As New InspGroup()

            If DR IsNot Nothing Then
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._Name = DR("name").ToString.Trim
                    _I._MonthsMin = GetInteger(DR("months_min"))
                    _I._MonthsMax = GetInteger(DR("months_max"))
                    _I._ChildCapacity = GetInteger(DR("child_capacity"))
                    _I._StaffRatio = GetInteger(DR("staff_ratio"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of InspGroup)

            Dim _InspGroupList As New List(Of InspGroup)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InspGroupList.Add(PropertiesFromData(_DR))
            Next

            Return _InspGroupList

        End Function


#End Region

    End Class

End Namespace
