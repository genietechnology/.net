﻿'*****************************************************
'Generated 24/09/2018 09:50:19 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildCharge
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._ActionDate = Nothing
                ._Description = ""
                ._Reference = ""
                ._Value = 0
                ._InvoiceLine = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._ActionDate = Nothing
                ._Description = ""
                ._Reference = ""
                ._Value = 0
                ._InvoiceLine = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?

        <StoredProcParameter("@ActionDate")>
        Public Property _ActionDate() As Date?

        <StoredProcParameter("@Description")>
        Public Property _Description() As String

        <StoredProcParameter("@Reference")>
        Public Property _Reference() As String

        <StoredProcParameter("@Value")>
        Public Property _Value() As Decimal

        <StoredProcParameter("@InvoiceLine")>
        Public Property _InvoiceLine() As Guid?


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildCharge
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildChargebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildCharge
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildChargebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of ChildCharge)
            Dim _ChildChargeList As List(Of ChildCharge)
            _ChildChargeList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildChargeTable"))
            Return _ChildChargeList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildCharge)
            Dim _ChildChargeList As List(Of ChildCharge)
            _ChildChargeList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildChargeTable"))
            Return _ChildChargeList
        End Function

        Public Shared Sub SaveAll(ByVal ChildChargeList As List(Of ChildCharge))
            For Each _ChildCharge As ChildCharge In ChildChargeList
                SaveRecord(_ChildCharge)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildChargeList As List(Of ChildCharge))
            For Each _ChildCharge As ChildCharge In ChildChargeList
                SaveRecord(ConnectionString, _ChildCharge)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal ChildCharge As ChildCharge) As Guid
            Dim _Current As ChildCharge = RetreiveByID(ChildCharge._ID.Value)
            ChildCharge.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildCharge, _Current, "upsertChildCharge")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildCharge As ChildCharge) As Guid
            Dim _Current As ChildCharge = RetreiveByID(ConnectionString, ChildCharge._ID.Value)
            ChildCharge.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildCharge, _Current, "upsertChildCharge")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As ChildCharge = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildChargebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As ChildCharge = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildChargebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As ChildCharge = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildCharge")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As ChildCharge = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildCharge")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildCharge

            Dim _C As ChildCharge = Nothing

            If DR IsNot Nothing Then
                _C = New ChildCharge()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._ActionDate = GetDate(DR("action_date"))
                    _C._Description = DR("description").ToString.Trim
                    _C._Reference = DR("reference").ToString.Trim
                    _C._Value = GetDecimal(DR("value"))
                    _C._InvoiceLine = GetGUID(DR("invoice_line"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildCharge)

            Dim _ChildChargeList As New List(Of ChildCharge)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildChargeList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildChargeList

        End Function


#End Region

    End Class

End Namespace
