﻿'*****************************************************
'Generated 29/11/2015 22:13:02 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildWeekMonth
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_TariffId As Guid?
        Dim m_DateFrom As Date?
        Dim m_Frequency As String
        Dim m_DaysPerWeek As Integer
        Dim m_AgeMode As String
        Dim m_AgeId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._TariffId = Nothing
                ._DateFrom = Nothing
                ._Frequency = ""
                ._DaysPerWeek = 0
                ._AgeMode = ""
                ._AgeId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._TariffId = Nothing
                ._DateFrom = Nothing
                ._Frequency = ""
                ._DaysPerWeek = 0
                ._AgeMode = ""
                ._AgeId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@TariffId")> _
        Public Property _TariffId() As Guid?
            Get
                Return m_TariffId
            End Get
            Set(ByVal value As Guid?)
                m_TariffId = value
            End Set
        End Property

        <StoredProcParameter("@DateFrom")> _
        Public Property _DateFrom() As Date?
            Get
                Return m_DateFrom
            End Get
            Set(ByVal value As Date?)
                m_DateFrom = value
            End Set
        End Property

        <StoredProcParameter("@Frequency")> _
        Public Property _Frequency() As String
            Get
                Return m_Frequency
            End Get
            Set(ByVal value As String)
                m_Frequency = value
            End Set
        End Property

        <StoredProcParameter("@DaysPerWeek")> _
        Public Property _DaysPerWeek() As Integer
            Get
                Return m_DaysPerWeek
            End Get
            Set(ByVal value As Integer)
                m_DaysPerWeek = value
            End Set
        End Property

        <StoredProcParameter("@AgeMode")> _
        Public Property _AgeMode() As String
            Get
                Return m_AgeMode
            End Get
            Set(ByVal value As String)
                m_AgeMode = value
            End Set
        End Property

        <StoredProcParameter("@AgeId")> _
        Public Property _AgeId() As Guid?
            Get
                Return m_AgeId
            End Get
            Set(ByVal value As Guid?)
                m_AgeId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildWeekMonth

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildWeekMonthbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildWeekMonth)

            Dim _ChildWeekMonthList As List(Of ChildWeekMonth)
            _ChildWeekMonthList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildWeekMonthTable"))
            Return _ChildWeekMonthList

        End Function

        Public Shared Sub SaveAll(ByVal ChildWeekMonthList As List(Of ChildWeekMonth))

            For Each _ChildWeekMonth As ChildWeekMonth In ChildWeekMonthList
                SaveRecord(_ChildWeekMonth)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildWeekMonth As ChildWeekMonth) As Guid
            ChildWeekMonth.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildWeekMonth, "upsertChildWeekMonth")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildWeekMonthbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildWeekMonth")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildWeekMonth

            Dim _C As ChildWeekMonth = Nothing

            If DR IsNot Nothing Then
                _C = New ChildWeekMonth()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._TariffId = GetGUID(DR("tariff_id"))
                    _C._DateFrom = GetDate(DR("date_from"))
                    _C._Frequency = DR("frequency").ToString.Trim
                    _C._DaysPerWeek = GetInteger(DR("days_per_week"))
                    _C._AgeMode = DR("age_mode").ToString.Trim
                    _C._AgeId = GetGUID(DR("age_id"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildWeekMonth)

            Dim _ChildWeekMonthList As New List(Of ChildWeekMonth)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildWeekMonthList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildWeekMonthList

        End Function


#End Region

    End Class

End Namespace
