﻿'*****************************************************
'Generated 05/01/2017 21:48:58 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Incident
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_IncidentType As String
        Dim m_Location As String
        Dim m_Details As String
        Dim m_Injury As String
        Dim m_Treatment As String
        Dim m_Action As String
        Dim m_SigStaff As Guid?
        Dim m_SigWitness As Guid?
        Dim m_SigContact As Guid?
        Dim m_Photo As Byte()
        Dim m_BodyMap As Byte()
        Dim m_Stamp As DateTime?
        Dim m_SigManager As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._IncidentType = ""
                ._Location = ""
                ._Details = ""
                ._Injury = ""
                ._Treatment = ""
                ._Action = ""
                ._SigStaff = Nothing
                ._SigWitness = Nothing
                ._SigContact = Nothing
                ._Photo = Nothing
                ._BodyMap = Nothing
                ._Stamp = Nothing
                ._SigManager = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._IncidentType = ""
                ._Location = ""
                ._Details = ""
                ._Injury = ""
                ._Treatment = ""
                ._Action = ""
                ._SigStaff = Nothing
                ._SigWitness = Nothing
                ._SigContact = Nothing
                ._Photo = Nothing
                ._BodyMap = Nothing
                ._Stamp = Nothing
                ._SigManager = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@IncidentType")> _
        Public Property _IncidentType() As String
            Get
                Return m_IncidentType
            End Get
            Set(ByVal value As String)
                m_IncidentType = value
            End Set
        End Property

        <StoredProcParameter("@Location")> _
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@Details")> _
        Public Property _Details() As String
            Get
                Return m_Details
            End Get
            Set(ByVal value As String)
                m_Details = value
            End Set
        End Property

        <StoredProcParameter("@Injury")> _
        Public Property _Injury() As String
            Get
                Return m_Injury
            End Get
            Set(ByVal value As String)
                m_Injury = value
            End Set
        End Property

        <StoredProcParameter("@Treatment")> _
        Public Property _Treatment() As String
            Get
                Return m_Treatment
            End Get
            Set(ByVal value As String)
                m_Treatment = value
            End Set
        End Property

        <StoredProcParameter("@Action")> _
        Public Property _Action() As String
            Get
                Return m_Action
            End Get
            Set(ByVal value As String)
                m_Action = value
            End Set
        End Property

        <StoredProcParameter("@SigStaff")> _
        Public Property _SigStaff() As Guid?
            Get
                Return m_SigStaff
            End Get
            Set(ByVal value As Guid?)
                m_SigStaff = value
            End Set
        End Property

        <StoredProcParameter("@SigWitness")> _
        Public Property _SigWitness() As Guid?
            Get
                Return m_SigWitness
            End Get
            Set(ByVal value As Guid?)
                m_SigWitness = value
            End Set
        End Property

        <StoredProcParameter("@SigContact")> _
        Public Property _SigContact() As Guid?
            Get
                Return m_SigContact
            End Get
            Set(ByVal value As Guid?)
                m_SigContact = value
            End Set
        End Property

        <StoredProcParameter("@Photo")> _
        Public Property _Photo() As Byte()
            Get
                Return m_Photo
            End Get
            Set(ByVal value As Byte())
                m_Photo = value
            End Set
        End Property

        <StoredProcParameter("@BodyMap")> _
        Public Property _BodyMap() As Byte()
            Get
                Return m_BodyMap
            End Get
            Set(ByVal value As Byte())
                m_BodyMap = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@SigManager")> _
        Public Property _SigManager() As Guid?
            Get
                Return m_SigManager
            End Get
            Set(ByVal value As Guid?)
                m_SigManager = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Incident

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getIncidentbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Incident)

            Dim _IncidentList As List(Of Incident)
            _IncidentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getIncidentTable"))
            Return _IncidentList

        End Function

        Public Shared Sub SaveAll(ByVal IncidentList As List(Of Incident))

            For Each _Incident As Incident In IncidentList
                SaveRecord(_Incident)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Incident As Incident) As Guid
            Incident.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Incident, "upsertIncident")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteIncidentbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertIncident")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Incident

            Dim _I As Incident = Nothing

            If DR IsNot Nothing Then
                _I = New Incident()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._DayId = GetGUID(DR("day_id"))
                    _I._ChildId = GetGUID(DR("child_id"))
                    _I._ChildName = DR("child_name").ToString.Trim
                    _I._StaffId = GetGUID(DR("staff_id"))
                    _I._StaffName = DR("staff_name").ToString.Trim
                    _I._IncidentType = DR("incident_type").ToString.Trim
                    _I._Location = DR("location").ToString.Trim
                    _I._Details = DR("details").ToString.Trim
                    _I._Injury = DR("injury").ToString.Trim
                    _I._Treatment = DR("treatment").ToString.Trim
                    _I._Action = DR("action").ToString.Trim
                    _I._SigStaff = GetGUID(DR("sig_staff"))
                    _I._SigWitness = GetGUID(DR("sig_witness"))
                    _I._SigContact = GetGUID(DR("sig_contact"))
                    _I._Photo = GetImage(DR("photo"))
                    _I._BodyMap = GetImage(DR("body_map"))
                    _I._Stamp = GetDateTime(DR("stamp"))
                    _I._SigManager = GetGUID(DR("sig_manager"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Incident)

            Dim _IncidentList As New List(Of Incident)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _IncidentList.Add(PropertiesFromData(_DR))
            Next

            Return _IncidentList

        End Function


#End Region

    End Class

End Namespace
