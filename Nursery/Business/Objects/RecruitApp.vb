﻿'*****************************************************
'Generated 17/07/2018 21:07:55 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RecruitApp
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_DateApplied As Date?
        Dim m_Stage As String
        Dim m_Forename As String
        Dim m_Surname As String
        Dim m_Fullname As String
        Dim m_Gender As String
        Dim m_Dob As Date?
        Dim m_Address As String
        Dim m_Postcode As String
        Dim m_TelHome As String
        Dim m_TelMobile As String
        Dim m_Email As String
        Dim m_QualLevel As Byte
        Dim m_CurrentEmployer As String
        Dim m_CurrentPosition As String
        Dim m_CurrentSalary As Decimal
        Dim m_CurrentHours As Decimal
        Dim m_ChkIdWhen As Date?
        Dim m_ChkIdWho As String
        Dim m_ChkIdWhat As String
        Dim m_ChkQual As Boolean
        Dim m_ChkQualWhen As Date?
        Dim m_ChkQualWho As String
        Dim m_ChkQualWhat As String
        Dim m_ChkBarrWhen As Date?
        Dim m_ChkBarrWho As String
        Dim m_ChkDbsWhen As Date?
        Dim m_ChkDbsWho As String
        Dim m_ChkDbsIssued As Date?
        Dim m_ChkDbsRef As String
        Dim m_ChkOvs As Boolean
        Dim m_ChkOvsWhen As Date?
        Dim m_ChkOvsWho As String
        Dim m_ChkRukWhen As Date?
        Dim m_ChkRukWho As String
        Dim m_ChkRukWhat As String
        Dim m_ChkDisWhen As Date?
        Dim m_ChkDisWho As String
        Dim m_ChkRef1When As Date?
        Dim m_ChkRef1Who As String
        Dim m_ChkRef1What As String
        Dim m_ChkRef2When As Date?
        Dim m_ChkRef2Who As String
        Dim m_ChkRef2What As String
        Dim m_ChkDbsType As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._DateApplied = Nothing
                ._Stage = ""
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Gender = ""
                ._Dob = Nothing
                ._Address = ""
                ._Postcode = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._Email = ""
                ._QualLevel = Nothing
                ._CurrentEmployer = ""
                ._CurrentPosition = ""
                ._CurrentSalary = 0
                ._CurrentHours = 0
                ._ChkIdWhen = Nothing
                ._ChkIdWho = ""
                ._ChkIdWhat = ""
                ._ChkQual = False
                ._ChkQualWhen = Nothing
                ._ChkQualWho = ""
                ._ChkQualWhat = ""
                ._ChkBarrWhen = Nothing
                ._ChkBarrWho = ""
                ._ChkDbsWhen = Nothing
                ._ChkDbsWho = ""
                ._ChkDbsIssued = Nothing
                ._ChkDbsRef = ""
                ._ChkOvs = False
                ._ChkOvsWhen = Nothing
                ._ChkOvsWho = ""
                ._ChkRukWhen = Nothing
                ._ChkRukWho = ""
                ._ChkRukWhat = ""
                ._ChkDisWhen = Nothing
                ._ChkDisWho = ""
                ._ChkRef1When = Nothing
                ._ChkRef1Who = ""
                ._ChkRef1What = ""
                ._ChkRef2When = Nothing
                ._ChkRef2Who = ""
                ._ChkRef2What = ""
                ._ChkDbsType = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._DateApplied = Nothing
                ._Stage = ""
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Gender = ""
                ._Dob = Nothing
                ._Address = ""
                ._Postcode = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._Email = ""
                ._QualLevel = Nothing
                ._CurrentEmployer = ""
                ._CurrentPosition = ""
                ._CurrentSalary = 0
                ._CurrentHours = 0
                ._ChkIdWhen = Nothing
                ._ChkIdWho = ""
                ._ChkIdWhat = ""
                ._ChkQual = False
                ._ChkQualWhen = Nothing
                ._ChkQualWho = ""
                ._ChkQualWhat = ""
                ._ChkBarrWhen = Nothing
                ._ChkBarrWho = ""
                ._ChkDbsWhen = Nothing
                ._ChkDbsWho = ""
                ._ChkDbsIssued = Nothing
                ._ChkDbsRef = ""
                ._ChkOvs = False
                ._ChkOvsWhen = Nothing
                ._ChkOvsWho = ""
                ._ChkRukWhen = Nothing
                ._ChkRukWho = ""
                ._ChkRukWhat = ""
                ._ChkDisWhen = Nothing
                ._ChkDisWho = ""
                ._ChkRef1When = Nothing
                ._ChkRef1Who = ""
                ._ChkRef1What = ""
                ._ChkRef2When = Nothing
                ._ChkRef2Who = ""
                ._ChkRef2What = ""
                ._ChkDbsType = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@DateApplied")>
        Public Property _DateApplied() As Date?
            Get
                Return m_DateApplied
            End Get
            Set(ByVal value As Date?)
                m_DateApplied = value
            End Set
        End Property

        <StoredProcParameter("@Stage")>
        Public Property _Stage() As String
            Get
                Return m_Stage
            End Get
            Set(ByVal value As String)
                m_Stage = value
            End Set
        End Property

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String
            Get
                Return m_Forename
            End Get
            Set(ByVal value As String)
                m_Forename = value
            End Set
        End Property

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String
            Get
                Return m_Surname
            End Get
            Set(ByVal value As String)
                m_Surname = value
            End Set
        End Property

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String
            Get
                Return m_Fullname
            End Get
            Set(ByVal value As String)
                m_Fullname = value
            End Set
        End Property

        <StoredProcParameter("@Gender")>
        Public Property _Gender() As String
            Get
                Return m_Gender
            End Get
            Set(ByVal value As String)
                m_Gender = value
            End Set
        End Property

        <StoredProcParameter("@Dob")>
        Public Property _Dob() As Date?
            Get
                Return m_Dob
            End Get
            Set(ByVal value As Date?)
                m_Dob = value
            End Set
        End Property

        <StoredProcParameter("@Address")>
        Public Property _Address() As String
            Get
                Return m_Address
            End Get
            Set(ByVal value As String)
                m_Address = value
            End Set
        End Property

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String
            Get
                Return m_Postcode
            End Get
            Set(ByVal value As String)
                m_Postcode = value
            End Set
        End Property

        <StoredProcParameter("@TelHome")>
        Public Property _TelHome() As String
            Get
                Return m_TelHome
            End Get
            Set(ByVal value As String)
                m_TelHome = value
            End Set
        End Property

        <StoredProcParameter("@TelMobile")>
        Public Property _TelMobile() As String
            Get
                Return m_TelMobile
            End Get
            Set(ByVal value As String)
                m_TelMobile = value
            End Set
        End Property

        <StoredProcParameter("@Email")>
        Public Property _Email() As String
            Get
                Return m_Email
            End Get
            Set(ByVal value As String)
                m_Email = value
            End Set
        End Property

        <StoredProcParameter("@QualLevel")>
        Public Property _QualLevel() As Byte
            Get
                Return m_QualLevel
            End Get
            Set(ByVal value As Byte)
                m_QualLevel = value
            End Set
        End Property

        <StoredProcParameter("@CurrentEmployer")>
        Public Property _CurrentEmployer() As String
            Get
                Return m_CurrentEmployer
            End Get
            Set(ByVal value As String)
                m_CurrentEmployer = value
            End Set
        End Property

        <StoredProcParameter("@CurrentPosition")>
        Public Property _CurrentPosition() As String
            Get
                Return m_CurrentPosition
            End Get
            Set(ByVal value As String)
                m_CurrentPosition = value
            End Set
        End Property

        <StoredProcParameter("@CurrentSalary")>
        Public Property _CurrentSalary() As Decimal
            Get
                Return m_CurrentSalary
            End Get
            Set(ByVal value As Decimal)
                m_CurrentSalary = value
            End Set
        End Property

        <StoredProcParameter("@CurrentHours")>
        Public Property _CurrentHours() As Decimal
            Get
                Return m_CurrentHours
            End Get
            Set(ByVal value As Decimal)
                m_CurrentHours = value
            End Set
        End Property

        <StoredProcParameter("@ChkIdWhen")>
        Public Property _ChkIdWhen() As Date?
            Get
                Return m_ChkIdWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkIdWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkIdWho")>
        Public Property _ChkIdWho() As String
            Get
                Return m_ChkIdWho
            End Get
            Set(ByVal value As String)
                m_ChkIdWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkIdWhat")>
        Public Property _ChkIdWhat() As String
            Get
                Return m_ChkIdWhat
            End Get
            Set(ByVal value As String)
                m_ChkIdWhat = value
            End Set
        End Property

        <StoredProcParameter("@ChkQual")>
        Public Property _ChkQual() As Boolean
            Get
                Return m_ChkQual
            End Get
            Set(ByVal value As Boolean)
                m_ChkQual = value
            End Set
        End Property

        <StoredProcParameter("@ChkQualWhen")>
        Public Property _ChkQualWhen() As Date?
            Get
                Return m_ChkQualWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkQualWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkQualWho")>
        Public Property _ChkQualWho() As String
            Get
                Return m_ChkQualWho
            End Get
            Set(ByVal value As String)
                m_ChkQualWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkQualWhat")>
        Public Property _ChkQualWhat() As String
            Get
                Return m_ChkQualWhat
            End Get
            Set(ByVal value As String)
                m_ChkQualWhat = value
            End Set
        End Property

        <StoredProcParameter("@ChkBarrWhen")>
        Public Property _ChkBarrWhen() As Date?
            Get
                Return m_ChkBarrWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkBarrWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkBarrWho")>
        Public Property _ChkBarrWho() As String
            Get
                Return m_ChkBarrWho
            End Get
            Set(ByVal value As String)
                m_ChkBarrWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkDbsWhen")>
        Public Property _ChkDbsWhen() As Date?
            Get
                Return m_ChkDbsWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkDbsWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkDbsWho")>
        Public Property _ChkDbsWho() As String
            Get
                Return m_ChkDbsWho
            End Get
            Set(ByVal value As String)
                m_ChkDbsWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkDbsIssued")>
        Public Property _ChkDbsIssued() As Date?
            Get
                Return m_ChkDbsIssued
            End Get
            Set(ByVal value As Date?)
                m_ChkDbsIssued = value
            End Set
        End Property

        <StoredProcParameter("@ChkDbsRef")>
        Public Property _ChkDbsRef() As String
            Get
                Return m_ChkDbsRef
            End Get
            Set(ByVal value As String)
                m_ChkDbsRef = value
            End Set
        End Property

        <StoredProcParameter("@ChkOvs")>
        Public Property _ChkOvs() As Boolean
            Get
                Return m_ChkOvs
            End Get
            Set(ByVal value As Boolean)
                m_ChkOvs = value
            End Set
        End Property

        <StoredProcParameter("@ChkOvsWhen")>
        Public Property _ChkOvsWhen() As Date?
            Get
                Return m_ChkOvsWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkOvsWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkOvsWho")>
        Public Property _ChkOvsWho() As String
            Get
                Return m_ChkOvsWho
            End Get
            Set(ByVal value As String)
                m_ChkOvsWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkRukWhen")>
        Public Property _ChkRukWhen() As Date?
            Get
                Return m_ChkRukWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkRukWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkRukWho")>
        Public Property _ChkRukWho() As String
            Get
                Return m_ChkRukWho
            End Get
            Set(ByVal value As String)
                m_ChkRukWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkRukWhat")>
        Public Property _ChkRukWhat() As String
            Get
                Return m_ChkRukWhat
            End Get
            Set(ByVal value As String)
                m_ChkRukWhat = value
            End Set
        End Property

        <StoredProcParameter("@ChkDisWhen")>
        Public Property _ChkDisWhen() As Date?
            Get
                Return m_ChkDisWhen
            End Get
            Set(ByVal value As Date?)
                m_ChkDisWhen = value
            End Set
        End Property

        <StoredProcParameter("@ChkDisWho")>
        Public Property _ChkDisWho() As String
            Get
                Return m_ChkDisWho
            End Get
            Set(ByVal value As String)
                m_ChkDisWho = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef1When")>
        Public Property _ChkRef1When() As Date?
            Get
                Return m_ChkRef1When
            End Get
            Set(ByVal value As Date?)
                m_ChkRef1When = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef1Who")>
        Public Property _ChkRef1Who() As String
            Get
                Return m_ChkRef1Who
            End Get
            Set(ByVal value As String)
                m_ChkRef1Who = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef1What")>
        Public Property _ChkRef1What() As String
            Get
                Return m_ChkRef1What
            End Get
            Set(ByVal value As String)
                m_ChkRef1What = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef2When")>
        Public Property _ChkRef2When() As Date?
            Get
                Return m_ChkRef2When
            End Get
            Set(ByVal value As Date?)
                m_ChkRef2When = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef2Who")>
        Public Property _ChkRef2Who() As String
            Get
                Return m_ChkRef2Who
            End Get
            Set(ByVal value As String)
                m_ChkRef2Who = value
            End Set
        End Property

        <StoredProcParameter("@ChkRef2What")>
        Public Property _ChkRef2What() As String
            Get
                Return m_ChkRef2What
            End Get
            Set(ByVal value As String)
                m_ChkRef2What = value
            End Set
        End Property

        <StoredProcParameter("@ChkDbsType")>
        Public Property _ChkDbsType() As String
            Get
                Return m_ChkDbsType
            End Get
            Set(ByVal value As String)
                m_ChkDbsType = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RecruitApp

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRecruitAppbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RecruitApp

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRecruitAppbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RecruitApp)

            Dim _RecruitAppList As List(Of RecruitApp)
            _RecruitAppList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRecruitAppTable"))
            Return _RecruitAppList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RecruitApp)

            Dim _RecruitAppList As List(Of RecruitApp)
            _RecruitAppList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRecruitAppTable"))
            Return _RecruitAppList

        End Function

        Public Shared Sub SaveAll(ByVal RecruitAppList As List(Of RecruitApp))

            For Each _RecruitApp As RecruitApp In RecruitAppList
                SaveRecord(_RecruitApp)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RecruitAppList As List(Of RecruitApp))

            For Each _RecruitApp As RecruitApp In RecruitAppList
                SaveRecord(ConnectionString, _RecruitApp)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RecruitApp As RecruitApp) As Guid
            RecruitApp.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RecruitApp, "upsertRecruitApp")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RecruitApp As RecruitApp) As Guid
            RecruitApp.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RecruitApp, "upsertRecruitApp")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRecruitAppbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRecruitAppbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRecruitApp")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRecruitApp")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RecruitApp

            Dim _R As RecruitApp = Nothing

            If DR IsNot Nothing Then
                _R = New RecruitApp()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._JobId = GetGUID(DR("job_id"))
                    _R._DateApplied = GetDate(DR("date_applied"))
                    _R._Stage = DR("stage").ToString.Trim
                    _R._Forename = DR("forename").ToString.Trim
                    _R._Surname = DR("surname").ToString.Trim
                    _R._Fullname = DR("fullname").ToString.Trim
                    _R._Gender = DR("gender").ToString.Trim
                    _R._Dob = GetDate(DR("dob"))
                    _R._Address = DR("address").ToString.Trim
                    _R._Postcode = DR("postcode").ToString.Trim
                    _R._TelHome = DR("tel_home").ToString.Trim
                    _R._TelMobile = DR("tel_mobile").ToString.Trim
                    _R._Email = DR("email").ToString.Trim
                    _R._QualLevel = GetByte(DR("qual_level"))
                    _R._CurrentEmployer = DR("current_employer").ToString.Trim
                    _R._CurrentPosition = DR("current_position").ToString.Trim
                    _R._CurrentSalary = GetDecimal(DR("current_salary"))
                    _R._CurrentHours = GetDecimal(DR("current_hours"))
                    _R._ChkIdWhen = GetDate(DR("chk_id_when"))
                    _R._ChkIdWho = DR("chk_id_who").ToString.Trim
                    _R._ChkIdWhat = DR("chk_id_what").ToString.Trim
                    _R._ChkQual = GetBoolean(DR("chk_qual"))
                    _R._ChkQualWhen = GetDate(DR("chk_qual_when"))
                    _R._ChkQualWho = DR("chk_qual_who").ToString.Trim
                    _R._ChkQualWhat = DR("chk_qual_what").ToString.Trim
                    _R._ChkBarrWhen = GetDate(DR("chk_barr_when"))
                    _R._ChkBarrWho = DR("chk_barr_who").ToString.Trim
                    _R._ChkDbsWhen = GetDate(DR("chk_dbs_when"))
                    _R._ChkDbsWho = DR("chk_dbs_who").ToString.Trim
                    _R._ChkDbsIssued = GetDate(DR("chk_dbs_issued"))
                    _R._ChkDbsRef = DR("chk_dbs_ref").ToString.Trim
                    _R._ChkOvs = GetBoolean(DR("chk_ovs"))
                    _R._ChkOvsWhen = GetDate(DR("chk_ovs_when"))
                    _R._ChkOvsWho = DR("chk_ovs_who").ToString.Trim
                    _R._ChkRukWhen = GetDate(DR("chk_ruk_when"))
                    _R._ChkRukWho = DR("chk_ruk_who").ToString.Trim
                    _R._ChkRukWhat = DR("chk_ruk_what").ToString.Trim
                    _R._ChkDisWhen = GetDate(DR("chk_dis_when"))
                    _R._ChkDisWho = DR("chk_dis_who").ToString.Trim
                    _R._ChkRef1When = GetDate(DR("chk_ref1_when"))
                    _R._ChkRef1Who = DR("chk_ref1_who").ToString.Trim
                    _R._ChkRef1What = DR("chk_ref1_what").ToString.Trim
                    _R._ChkRef2When = GetDate(DR("chk_ref2_when"))
                    _R._ChkRef2Who = DR("chk_ref2_who").ToString.Trim
                    _R._ChkRef2What = DR("chk_ref2_what").ToString.Trim
                    _R._ChkDbsType = DR("chk_dbs_type").ToString.Trim

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RecruitApp)

            Dim _RecruitAppList As New List(Of RecruitApp)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RecruitAppList.Add(PropertiesFromData(_DR))
            Next

            Return _RecruitAppList

        End Function


#End Region

    End Class

End Namespace
