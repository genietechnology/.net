﻿'*****************************************************
'Generated 24/09/2018 09:51:01 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Day
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Date = Nothing
                ._BreakfastId = Nothing
                ._BreakfastName = ""
                ._SnackId = Nothing
                ._SnackName = ""
                ._LunchId = Nothing
                ._LunchName = ""
                ._LunchDesId = Nothing
                ._LunchDesName = ""
                ._TeaId = Nothing
                ._TeaName = ""
                ._TeaDesId = Nothing
                ._TeaDesName = ""
                ._Message = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._SnackPmId = Nothing
                ._SnackPmName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Date = Nothing
                ._BreakfastId = Nothing
                ._BreakfastName = ""
                ._SnackId = Nothing
                ._SnackName = ""
                ._LunchId = Nothing
                ._LunchName = ""
                ._LunchDesId = Nothing
                ._LunchDesName = ""
                ._TeaId = Nothing
                ._TeaName = ""
                ._TeaDesId = Nothing
                ._TeaDesName = ""
                ._Message = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._SnackPmId = Nothing
                ._SnackPmName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Date")>
        Public Property _Date() As Date?

        <StoredProcParameter("@BreakfastId")>
        Public Property _BreakfastId() As Guid?

        <StoredProcParameter("@BreakfastName")>
        Public Property _BreakfastName() As String

        <StoredProcParameter("@SnackId")>
        Public Property _SnackId() As Guid?

        <StoredProcParameter("@SnackName")>
        Public Property _SnackName() As String

        <StoredProcParameter("@LunchId")>
        Public Property _LunchId() As Guid?

        <StoredProcParameter("@LunchName")>
        Public Property _LunchName() As String

        <StoredProcParameter("@LunchDesId")>
        Public Property _LunchDesId() As Guid?

        <StoredProcParameter("@LunchDesName")>
        Public Property _LunchDesName() As String

        <StoredProcParameter("@TeaId")>
        Public Property _TeaId() As Guid?

        <StoredProcParameter("@TeaName")>
        Public Property _TeaName() As String

        <StoredProcParameter("@TeaDesId")>
        Public Property _TeaDesId() As Guid?

        <StoredProcParameter("@TeaDesName")>
        Public Property _TeaDesName() As String

        <StoredProcParameter("@Message")>
        Public Property _Message() As String

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@SnackPmId")>
        Public Property _SnackPmId() As Guid?

        <StoredProcParameter("@SnackPmName")>
        Public Property _SnackPmName() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Day
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDaybyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Day
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getDaybyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Day)
            Dim _DayList As List(Of Day)
            _DayList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDayTable"))
            Return _DayList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Day)
            Dim _DayList As List(Of Day)
            _DayList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getDayTable"))
            Return _DayList
        End Function

        Public Shared Sub SaveAll(ByVal DayList As List(Of Day))
            For Each _Day As Day In DayList
                SaveRecord(_Day)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal DayList As List(Of Day))
            For Each _Day As Day In DayList
                SaveRecord(ConnectionString, _Day)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Day As Day) As Guid
            Dim _Current As Day = RetreiveByID(Day._ID.Value)
            Day.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Day, _Current, "upsertDay")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Day As Day) As Guid
            Dim _Current As Day = RetreiveByID(ConnectionString, Day._ID.Value)
            Day.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Day, _Current, "upsertDay")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Day = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteDaybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Day = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteDaybyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Day = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertDay")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Day = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertDay")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Day

            Dim _D As Day = Nothing

            If DR IsNot Nothing Then
                _D = New Day()
                With DR
                    _D.IsNew = False
                    _D.IsDeleted = False
                    _D._ID = GetGUID(DR("ID"))
                    _D._Date = GetDate(DR("date"))
                    _D._BreakfastId = GetGUID(DR("breakfast_id"))
                    _D._BreakfastName = DR("breakfast_name").ToString.Trim
                    _D._SnackId = GetGUID(DR("snack_id"))
                    _D._SnackName = DR("snack_name").ToString.Trim
                    _D._LunchId = GetGUID(DR("lunch_id"))
                    _D._LunchName = DR("lunch_name").ToString.Trim
                    _D._LunchDesId = GetGUID(DR("lunch_des_id"))
                    _D._LunchDesName = DR("lunch_des_name").ToString.Trim
                    _D._TeaId = GetGUID(DR("tea_id"))
                    _D._TeaName = DR("tea_name").ToString.Trim
                    _D._TeaDesId = GetGUID(DR("tea_des_id"))
                    _D._TeaDesName = DR("tea_des_name").ToString.Trim
                    _D._Message = DR("message").ToString.Trim
                    _D._SiteId = GetGUID(DR("site_id"))
                    _D._SiteName = DR("site_name").ToString.Trim
                    _D._SnackPmId = GetGUID(DR("snack_pm_id"))
                    _D._SnackPmName = DR("snack_pm_name").ToString.Trim

                End With
            End If

            Return _D

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Day)

            Dim _DayList As New List(Of Day)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DayList.Add(PropertiesFromData(_DR))
            Next

            Return _DayList

        End Function


#End Region

    End Class

End Namespace
