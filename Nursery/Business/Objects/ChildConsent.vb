﻿'*****************************************************
'Generated 28/02/2016 22:07:02 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildConsent
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_Consent As String
        Dim m_ConsentDate As Date?
        Dim m_ConsentPerson As String
        Dim m_Given As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._Consent = ""
                ._ConsentDate = Nothing
                ._ConsentPerson = ""
                ._Given = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._Consent = ""
                ._ConsentDate = Nothing
                ._ConsentPerson = ""
                ._Given = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@Consent")> _
        Public Property _Consent() As String
            Get
                Return m_Consent
            End Get
            Set(ByVal value As String)
                m_Consent = value
            End Set
        End Property

        <StoredProcParameter("@ConsentDate")> _
        Public Property _ConsentDate() As Date?
            Get
                Return m_ConsentDate
            End Get
            Set(ByVal value As Date?)
                m_ConsentDate = value
            End Set
        End Property

        <StoredProcParameter("@ConsentPerson")> _
        Public Property _ConsentPerson() As String
            Get
                Return m_ConsentPerson
            End Get
            Set(ByVal value As String)
                m_ConsentPerson = value
            End Set
        End Property

        <StoredProcParameter("@Given")> _
        Public Property _Given() As Boolean
            Get
                Return m_Given
            End Get
            Set(ByVal value As Boolean)
                m_Given = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildConsent

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildConsentbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildConsent)

            Dim _ChildConsentList As List(Of ChildConsent)
            _ChildConsentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildConsentTable"))
            Return _ChildConsentList

        End Function

        Public Shared Sub SaveAll(ByVal ChildConsentList As List(Of ChildConsent))

            For Each _ChildConsent As ChildConsent In ChildConsentList
                SaveRecord(_ChildConsent)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildConsent As ChildConsent) As Guid
            ChildConsent.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildConsent, "upsertChildConsent")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildConsentbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildConsent")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildConsent

            Dim _C As ChildConsent = Nothing

            If DR IsNot Nothing Then
                _C = New ChildConsent()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._Consent = DR("consent").ToString.Trim
                    _C._ConsentDate = GetDate(DR("consent_date"))
                    _C._ConsentPerson = DR("consent_person").ToString.Trim
                    _C._Given = GetBoolean(DR("given"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildConsent)

            Dim _ChildConsentList As New List(Of ChildConsent)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildConsentList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildConsentList

        End Function


#End Region

    End Class

End Namespace
