﻿'*****************************************************
'Generated 24/09/2018 09:51:15 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class DayActivity
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._GroupId = Nothing
                ._ActivityText = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._GroupId = Nothing
                ._ActivityText = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@DayId")>
        Public Property _DayId() As Guid?

        <StoredProcParameter("@GroupId")>
        Public Property _GroupId() As Guid?

        <StoredProcParameter("@ActivityText")>
        Public Property _ActivityText() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As DayActivity
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDayActivitybyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As DayActivity
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getDayActivitybyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of DayActivity)
            Dim _DayActivityList As List(Of DayActivity)
            _DayActivityList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDayActivityTable"))
            Return _DayActivityList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of DayActivity)
            Dim _DayActivityList As List(Of DayActivity)
            _DayActivityList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getDayActivityTable"))
            Return _DayActivityList
        End Function

        Public Shared Sub SaveAll(ByVal DayActivityList As List(Of DayActivity))
            For Each _DayActivity As DayActivity In DayActivityList
                SaveRecord(_DayActivity)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal DayActivityList As List(Of DayActivity))
            For Each _DayActivity As DayActivity In DayActivityList
                SaveRecord(ConnectionString, _DayActivity)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal DayActivity As DayActivity) As Guid
            Dim _Current As DayActivity = RetreiveByID(DayActivity._ID.Value)
            DayActivity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, DayActivity, _Current, "upsertDayActivity")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal DayActivity As DayActivity) As Guid
            Dim _Current As DayActivity = RetreiveByID(ConnectionString, DayActivity._ID.Value)
            DayActivity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, DayActivity, _Current, "upsertDayActivity")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As DayActivity = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteDayActivitybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As DayActivity = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteDayActivitybyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As DayActivity = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertDayActivity")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As DayActivity = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertDayActivity")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As DayActivity

            Dim _D As DayActivity = Nothing

            If DR IsNot Nothing Then
                _D = New DayActivity()
                With DR
                    _D.IsNew = False
                    _D.IsDeleted = False
                    _D._ID = GetGUID(DR("ID"))
                    _D._DayId = GetGUID(DR("day_id"))
                    _D._GroupId = GetGUID(DR("group_id"))
                    _D._ActivityText = DR("activity_text").ToString.Trim

                End With
            End If

            Return _D

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of DayActivity)

            Dim _DayActivityList As New List(Of DayActivity)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DayActivityList.Add(PropertiesFromData(_DR))
            Next

            Return _DayActivityList

        End Function


#End Region

    End Class

End Namespace
