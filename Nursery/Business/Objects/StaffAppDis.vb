﻿'*****************************************************
'Generated 05/10/2015 05:43:52 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffAppDis
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_StaffId As Guid?
        Dim m_Type As String
        Dim m_ActionDate As Date?
        Dim m_Subject As String
        Dim m_Notes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._StaffId = Nothing
                ._Type = ""
                ._ActionDate = Nothing
                ._Subject = ""
                ._Notes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._StaffId = Nothing
                ._Type = ""
                ._ActionDate = Nothing
                ._Subject = ""
                ._Notes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@ActionDate")> _
        Public Property _ActionDate() As Date?
            Get
                Return m_ActionDate
            End Get
            Set(ByVal value As Date?)
                m_ActionDate = value
            End Set
        End Property

        <StoredProcParameter("@Subject")> _
        Public Property _Subject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property

        <StoredProcParameter("@Notes")> _
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffAppDis

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffAppDisbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffAppDis)

            Dim _StaffAppDisList As List(Of StaffAppDis)
            _StaffAppDisList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffAppDisTable"))
            Return _StaffAppDisList

        End Function

        Public Shared Sub SaveAll(ByVal StaffAppDisList As List(Of StaffAppDis))

            For Each _StaffAppDis As StaffAppDis In StaffAppDisList
                SaveRecord(_StaffAppDis)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffAppDis As StaffAppDis) As Guid
            StaffAppDis.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffAppDis, "upsertStaffAppDis")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffAppDisbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffAppDis")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffAppDis

            Dim _S As StaffAppDis = Nothing

            If DR IsNot Nothing Then
                _S = New StaffAppDis()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._StaffId = GetGUID(DR("staff_id"))
                    _S._Type = DR("type").ToString.Trim
                    _S._ActionDate = GetDate(DR("action_date"))
                    _S._Subject = DR("subject").ToString.Trim
                    _S._Notes = DR("notes").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffAppDis)

            Dim _StaffAppDisList As New List(Of StaffAppDis)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffAppDisList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffAppDisList

        End Function


#End Region

    End Class

End Namespace
