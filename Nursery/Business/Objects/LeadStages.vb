﻿'*****************************************************
'Generated 24/01/2016 19:43:01 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class LeadStage
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_PcntComplete As Byte
        Dim m_ActionStart As Boolean
        Dim m_ActionContact As Boolean
        Dim m_ActionPack As Boolean
        Dim m_ActionViewing As Boolean
        Dim m_ActionQuote As Boolean
        Dim m_ActionList As Boolean
        Dim m_ActionClose As Boolean
        Dim m_CloseReason As String
        Dim m_Email As Boolean
        Dim m_EmailSubject As String
        Dim m_EmailBody As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._PcntComplete = Nothing
                ._ActionStart = False
                ._ActionContact = False
                ._ActionPack = False
                ._ActionViewing = False
                ._ActionQuote = False
                ._ActionList = False
                ._ActionClose = False
                ._CloseReason = ""
                ._Email = False
                ._EmailSubject = ""
                ._EmailBody = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._PcntComplete = Nothing
                ._ActionStart = False
                ._ActionContact = False
                ._ActionPack = False
                ._ActionViewing = False
                ._ActionQuote = False
                ._ActionList = False
                ._ActionClose = False
                ._CloseReason = ""
                ._Email = False
                ._EmailSubject = ""
                ._EmailBody = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@PcntComplete")> _
        Public Property _PcntComplete() As Byte
            Get
                Return m_PcntComplete
            End Get
            Set(ByVal value As Byte)
                m_PcntComplete = value
            End Set
        End Property

        <StoredProcParameter("@ActionStart")> _
        Public Property _ActionStart() As Boolean
            Get
                Return m_ActionStart
            End Get
            Set(ByVal value As Boolean)
                m_ActionStart = value
            End Set
        End Property

        <StoredProcParameter("@ActionContact")> _
        Public Property _ActionContact() As Boolean
            Get
                Return m_ActionContact
            End Get
            Set(ByVal value As Boolean)
                m_ActionContact = value
            End Set
        End Property

        <StoredProcParameter("@ActionPack")> _
        Public Property _ActionPack() As Boolean
            Get
                Return m_ActionPack
            End Get
            Set(ByVal value As Boolean)
                m_ActionPack = value
            End Set
        End Property

        <StoredProcParameter("@ActionViewing")> _
        Public Property _ActionViewing() As Boolean
            Get
                Return m_ActionViewing
            End Get
            Set(ByVal value As Boolean)
                m_ActionViewing = value
            End Set
        End Property

        <StoredProcParameter("@ActionQuote")> _
        Public Property _ActionQuote() As Boolean
            Get
                Return m_ActionQuote
            End Get
            Set(ByVal value As Boolean)
                m_ActionQuote = value
            End Set
        End Property

        <StoredProcParameter("@ActionList")> _
        Public Property _ActionList() As Boolean
            Get
                Return m_ActionList
            End Get
            Set(ByVal value As Boolean)
                m_ActionList = value
            End Set
        End Property

        <StoredProcParameter("@ActionClose")> _
        Public Property _ActionClose() As Boolean
            Get
                Return m_ActionClose
            End Get
            Set(ByVal value As Boolean)
                m_ActionClose = value
            End Set
        End Property

        <StoredProcParameter("@CloseReason")> _
        Public Property _CloseReason() As String
            Get
                Return m_CloseReason
            End Get
            Set(ByVal value As String)
                m_CloseReason = value
            End Set
        End Property

        <StoredProcParameter("@Email")> _
        Public Property _Email() As Boolean
            Get
                Return m_Email
            End Get
            Set(ByVal value As Boolean)
                m_Email = value
            End Set
        End Property

        <StoredProcParameter("@EmailSubject")> _
        Public Property _EmailSubject() As String
            Get
                Return m_EmailSubject
            End Get
            Set(ByVal value As String)
                m_EmailSubject = value
            End Set
        End Property

        <StoredProcParameter("@EmailBody")> _
        Public Property _EmailBody() As String
            Get
                Return m_EmailBody
            End Get
            Set(ByVal value As String)
                m_EmailBody = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As LeadStage

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getLeadStagebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of LeadStage)

            Dim _LeadStageList As List(Of LeadStage)
            _LeadStageList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getLeadStageTable"))
            Return _LeadStageList

        End Function

        Public Shared Sub SaveAll(ByVal LeadStageList As List(Of LeadStage))

            For Each _LeadStage As LeadStage In LeadStageList
                SaveRecord(_LeadStage)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal LeadStage As LeadStage) As Guid
            LeadStage.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, LeadStage, "upsertLeadStage")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteLeadStagebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertLeadStage")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As LeadStage

            Dim _L As LeadStage = Nothing

            If DR IsNot Nothing Then
                _L = New LeadStage()
                With DR
                    _L.IsNew = False
                    _L.IsDeleted = False
                    _L._ID = GetGUID(DR("ID"))
                    _L._Name = DR("name").ToString.Trim
                    _L._PcntComplete = GetByte(DR("pcnt_complete"))
                    _L._ActionStart = GetBoolean(DR("action_start"))
                    _L._ActionContact = GetBoolean(DR("action_contact"))
                    _L._ActionPack = GetBoolean(DR("action_pack"))
                    _L._ActionViewing = GetBoolean(DR("action_viewing"))
                    _L._ActionQuote = GetBoolean(DR("action_quote"))
                    _L._ActionList = GetBoolean(DR("action_list"))
                    _L._ActionClose = GetBoolean(DR("action_close"))
                    _L._CloseReason = DR("close_reason").ToString.Trim
                    _L._Email = GetBoolean(DR("email"))
                    _L._EmailSubject = DR("email_subject").ToString.Trim
                    _L._EmailBody = DR("email_body").ToString.Trim

                End With
            End If

            Return _L

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of LeadStage)

            Dim _LeadStageList As New List(Of LeadStage)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _LeadStageList.Add(PropertiesFromData(_DR))
            Next

            Return _LeadStageList

        End Function


#End Region

    End Class

End Namespace
