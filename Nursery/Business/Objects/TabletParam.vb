﻿'*****************************************************
'Generated 16/05/2016 00:00:34 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TabletParam
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Device As String
        Dim m_Parent As String
        Dim m_ParamId As String
        Dim m_ParamType As String
        Dim m_ParamDesc As String
        Dim m_ParamValue As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Device = ""
                ._Parent = ""
                ._ParamId = ""
                ._ParamType = ""
                ._ParamDesc = ""
                ._ParamValue = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Device = ""
                ._Parent = ""
                ._ParamId = ""
                ._ParamType = ""
                ._ParamDesc = ""
                ._ParamValue = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Device")> _
        Public Property _Device() As String
            Get
                Return m_Device
            End Get
            Set(ByVal value As String)
                m_Device = value
            End Set
        End Property

        <StoredProcParameter("@Parent")> _
        Public Property _Parent() As String
            Get
                Return m_Parent
            End Get
            Set(ByVal value As String)
                m_Parent = value
            End Set
        End Property

        <StoredProcParameter("@ParamId")> _
        Public Property _ParamId() As String
            Get
                Return m_ParamId
            End Get
            Set(ByVal value As String)
                m_ParamId = value
            End Set
        End Property

        <StoredProcParameter("@ParamType")> _
        Public Property _ParamType() As String
            Get
                Return m_ParamType
            End Get
            Set(ByVal value As String)
                m_ParamType = value
            End Set
        End Property

        <StoredProcParameter("@ParamDesc")> _
        Public Property _ParamDesc() As String
            Get
                Return m_ParamDesc
            End Get
            Set(ByVal value As String)
                m_ParamDesc = value
            End Set
        End Property

        <StoredProcParameter("@ParamValue")> _
        Public Property _ParamValue() As String
            Get
                Return m_ParamValue
            End Get
            Set(ByVal value As String)
                m_ParamValue = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TabletParam

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTabletParambyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of TabletParam)

            Dim _TabletParamList As List(Of TabletParam)
            _TabletParamList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTabletParamTable"))
            Return _TabletParamList

        End Function

        Public Shared Sub SaveAll(ByVal TabletParamList As List(Of TabletParam))

            For Each _TabletParam As TabletParam In TabletParamList
                SaveRecord(_TabletParam)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal TabletParam As TabletParam) As Guid
            TabletParam.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, TabletParam, "upsertTabletParam")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTabletParambyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertTabletParam")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TabletParam

            Dim _T As TabletParam = Nothing

            If DR IsNot Nothing Then
                _T = New TabletParam()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Device = DR("device").ToString.Trim
                    _T._Parent = DR("parent").ToString.Trim
                    _T._ParamId = DR("param_id").ToString.Trim
                    _T._ParamType = DR("param_type").ToString.Trim
                    _T._ParamDesc = DR("param_desc").ToString.Trim
                    _T._ParamValue = DR("param_value").ToString.Trim

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TabletParam)

            Dim _TabletParamList As New List(Of TabletParam)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TabletParamList.Add(PropertiesFromData(_DR))
            Next

            Return _TabletParamList

        End Function


#End Region

    End Class

End Namespace
