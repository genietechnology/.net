﻿'*****************************************************
'Generated 01/07/2015 00:14:50 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class BookingSlot
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_BookingId As Guid?
        Dim m_BookingDate As Date?
        Dim m_ChildId As Guid?
        Dim m_SlotId As Guid?
        Dim m_SlotName As String
        Dim m_SlotStart As TimeSpan?
        Dim m_SlotEnd As TimeSpan?
        Dim m_Booked As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BookingId = Nothing
                ._BookingDate = Nothing
                ._ChildId = Nothing
                ._SlotId = Nothing
                ._SlotName = ""
                ._SlotStart = Nothing
                ._SlotEnd = Nothing
                ._Booked = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BookingId = Nothing
                ._BookingDate = Nothing
                ._ChildId = Nothing
                ._SlotId = Nothing
                ._SlotName = ""
                ._SlotStart = Nothing
                ._SlotEnd = Nothing
                ._Booked = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@BookingId")> _
        Public Property _BookingId() As Guid?
            Get
                Return m_BookingId
            End Get
            Set(ByVal value As Guid?)
                m_BookingId = value
            End Set
        End Property

        <StoredProcParameter("@BookingDate")> _
        Public Property _BookingDate() As Date?
            Get
                Return m_BookingDate
            End Get
            Set(ByVal value As Date?)
                m_BookingDate = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@SlotId")> _
        Public Property _SlotId() As Guid?
            Get
                Return m_SlotId
            End Get
            Set(ByVal value As Guid?)
                m_SlotId = value
            End Set
        End Property

        <StoredProcParameter("@SlotName")> _
        Public Property _SlotName() As String
            Get
                Return m_SlotName
            End Get
            Set(ByVal value As String)
                m_SlotName = value
            End Set
        End Property

        <StoredProcParameter("@SlotStart")> _
        Public Property _SlotStart() As TimeSpan?
            Get
                Return m_SlotStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_SlotStart = value
            End Set
        End Property

        <StoredProcParameter("@SlotEnd")> _
        Public Property _SlotEnd() As TimeSpan?
            Get
                Return m_SlotEnd
            End Get
            Set(ByVal value As TimeSpan?)
                m_SlotEnd = value
            End Set
        End Property

        <StoredProcParameter("@Booked")> _
        Public Property _Booked() As Boolean
            Get
                Return m_Booked
            End Get
            Set(ByVal value As Boolean)
                m_Booked = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As BookingSlot

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getBookingSlotbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of BookingSlot)

            Dim _BookingSlotList As List(Of BookingSlot)
            _BookingSlotList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getBookingSlotTable"))
            Return _BookingSlotList

        End Function

        Public Shared Sub SaveAll(ByVal BookingSlotList As List(Of BookingSlot))

            For Each _BookingSlot As BookingSlot In BookingSlotList
                SaveRecord(_BookingSlot)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal BookingSlot As BookingSlot) As Guid
            DAL.SaveRecord(Session.ConnectionManager, BookingSlot, "upsertBookingSlot")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteBookingSlotbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertBookingSlot")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As BookingSlot

            Dim _B As BookingSlot = Nothing

            If DR IsNot Nothing Then
                _B = New BookingSlot()
                With DR
                    _B.IsNew = False
                    _B.IsDeleted = False
                    _B._ID = GetGUID(DR("ID"))
                    _B._BookingId = GetGUID(DR("booking_id"))
                    _B._BookingDate = GetDate(DR("booking_date"))
                    _B._ChildId = GetGUID(DR("child_id"))
                    _B._SlotId = GetGUID(DR("slot_id"))
                    _B._SlotName = DR("slot_name").ToString.Trim
                    _B._SlotStart = GetTimeSpan(DR("slot_start"))
                    _B._SlotEnd = GetTimeSpan(DR("slot_end"))
                    _B._Booked = GetBoolean(DR("booked"))

                End With
            End If

            Return _B

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of BookingSlot)

            Dim _BookingSlotList As New List(Of BookingSlot)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _BookingSlotList.Add(PropertiesFromData(_DR))
            Next

            Return _BookingSlotList

        End Function


#End Region

    End Class

End Namespace
