﻿'*****************************************************
'Generated 24/09/2018 09:49:52 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildCalculation
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._DateFrom = Nothing
                ._Units = 0
                ._Multiplier = ""
                ._Rate = 0
                ._Per = ""
                ._Weeks = 0
                ._CalcDay = 0
                ._CalcWeek = 0
                ._CalcMonth = 0
                ._CalcAnnum = 0
                ._FundUnits = 0
                ._FundMultiplier = ""
                ._FundRate = 0
                ._FundPer = ""
                ._FundWeeks = 0
                ._FundCalcDay = 0
                ._FundCalcWeek = 0
                ._FundCalcMonth = 0
                ._FundCalcAnnum = 0
                ._TotalCalcMonth = 0
                ._TotalCalcAnnum = 0
                ._InvoiceText = ""
                ._Active = False
                ._DiscountMode = ""
                ._DiscountRate = 0
                ._DiscountAmount = 0
                ._C2Sign = ""
                ._C2Units = 0
                ._C2Multiplier = ""
                ._C2Rate = 0
                ._C2Per = ""
                ._C2Weeks = 0
                ._C2CalcDay = 0
                ._C2CalcWeek = 0
                ._C2CalcMonth = 0
                ._C2CalcAnnum = 0
                ._C3Sign = ""
                ._C3Units = 0
                ._C3Multiplier = ""
                ._C3Rate = 0
                ._C3Per = ""
                ._C3Weeks = 0
                ._C3CalcDay = 0
                ._C3CalcWeek = 0
                ._C3CalcMonth = 0
                ._C3CalcAnnum = 0
                ._C4Sign = ""
                ._C4Units = 0
                ._C4Multiplier = ""
                ._C4Rate = 0
                ._C4Per = ""
                ._C4Weeks = 0
                ._C4CalcDay = 0
                ._C4CalcWeek = 0
                ._C4CalcMonth = 0
                ._C4CalcAnnum = 0
                ._C5Sign = ""
                ._C5Units = 0
                ._C5Multiplier = ""
                ._C5Rate = 0
                ._C5Per = ""
                ._C5Weeks = 0
                ._C5CalcDay = 0
                ._C5CalcWeek = 0
                ._C5CalcMonth = 0
                ._C5CalcAnnum = 0
                ._SplitType = ""
                ._TotalCalcWeek = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._DateFrom = Nothing
                ._Units = 0
                ._Multiplier = ""
                ._Rate = 0
                ._Per = ""
                ._Weeks = 0
                ._CalcDay = 0
                ._CalcWeek = 0
                ._CalcMonth = 0
                ._CalcAnnum = 0
                ._FundUnits = 0
                ._FundMultiplier = ""
                ._FundRate = 0
                ._FundPer = ""
                ._FundWeeks = 0
                ._FundCalcDay = 0
                ._FundCalcWeek = 0
                ._FundCalcMonth = 0
                ._FundCalcAnnum = 0
                ._TotalCalcMonth = 0
                ._TotalCalcAnnum = 0
                ._InvoiceText = ""
                ._Active = False
                ._DiscountMode = ""
                ._DiscountRate = 0
                ._DiscountAmount = 0
                ._C2Sign = ""
                ._C2Units = 0
                ._C2Multiplier = ""
                ._C2Rate = 0
                ._C2Per = ""
                ._C2Weeks = 0
                ._C2CalcDay = 0
                ._C2CalcWeek = 0
                ._C2CalcMonth = 0
                ._C2CalcAnnum = 0
                ._C3Sign = ""
                ._C3Units = 0
                ._C3Multiplier = ""
                ._C3Rate = 0
                ._C3Per = ""
                ._C3Weeks = 0
                ._C3CalcDay = 0
                ._C3CalcWeek = 0
                ._C3CalcMonth = 0
                ._C3CalcAnnum = 0
                ._C4Sign = ""
                ._C4Units = 0
                ._C4Multiplier = ""
                ._C4Rate = 0
                ._C4Per = ""
                ._C4Weeks = 0
                ._C4CalcDay = 0
                ._C4CalcWeek = 0
                ._C4CalcMonth = 0
                ._C4CalcAnnum = 0
                ._C5Sign = ""
                ._C5Units = 0
                ._C5Multiplier = ""
                ._C5Rate = 0
                ._C5Per = ""
                ._C5Weeks = 0
                ._C5CalcDay = 0
                ._C5CalcWeek = 0
                ._C5CalcMonth = 0
                ._C5CalcAnnum = 0
                ._SplitType = ""
                ._TotalCalcWeek = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?

        <StoredProcParameter("@DateFrom")>
        Public Property _DateFrom() As Date?

        <StoredProcParameter("@Units")>
        Public Property _Units() As Decimal

        <StoredProcParameter("@Multiplier")>
        Public Property _Multiplier() As String

        <StoredProcParameter("@Rate")>
        Public Property _Rate() As Decimal

        <StoredProcParameter("@Per")>
        Public Property _Per() As String

        <StoredProcParameter("@Weeks")>
        Public Property _Weeks() As Decimal

        <StoredProcParameter("@CalcDay")>
        Public Property _CalcDay() As Decimal

        <StoredProcParameter("@CalcWeek")>
        Public Property _CalcWeek() As Decimal

        <StoredProcParameter("@CalcMonth")>
        Public Property _CalcMonth() As Decimal

        <StoredProcParameter("@CalcAnnum")>
        Public Property _CalcAnnum() As Decimal

        <StoredProcParameter("@FundUnits")>
        Public Property _FundUnits() As Decimal

        <StoredProcParameter("@FundMultiplier")>
        Public Property _FundMultiplier() As String

        <StoredProcParameter("@FundRate")>
        Public Property _FundRate() As Decimal

        <StoredProcParameter("@FundPer")>
        Public Property _FundPer() As String

        <StoredProcParameter("@FundWeeks")>
        Public Property _FundWeeks() As Decimal

        <StoredProcParameter("@FundCalcDay")>
        Public Property _FundCalcDay() As Decimal

        <StoredProcParameter("@FundCalcWeek")>
        Public Property _FundCalcWeek() As Decimal

        <StoredProcParameter("@FundCalcMonth")>
        Public Property _FundCalcMonth() As Decimal

        <StoredProcParameter("@FundCalcAnnum")>
        Public Property _FundCalcAnnum() As Decimal

        <StoredProcParameter("@TotalCalcMonth")>
        Public Property _TotalCalcMonth() As Decimal

        <StoredProcParameter("@TotalCalcAnnum")>
        Public Property _TotalCalcAnnum() As Decimal

        <StoredProcParameter("@InvoiceText")>
        Public Property _InvoiceText() As String

        <StoredProcParameter("@Active")>
        Public Property _Active() As Boolean

        <StoredProcParameter("@DiscountMode")>
        Public Property _DiscountMode() As String

        <StoredProcParameter("@DiscountRate")>
        Public Property _DiscountRate() As Decimal

        <StoredProcParameter("@DiscountAmount")>
        Public Property _DiscountAmount() As Decimal

        <StoredProcParameter("@C2Sign")>
        Public Property _C2Sign() As String

        <StoredProcParameter("@C2Units")>
        Public Property _C2Units() As Decimal

        <StoredProcParameter("@C2Multiplier")>
        Public Property _C2Multiplier() As String

        <StoredProcParameter("@C2Rate")>
        Public Property _C2Rate() As Decimal

        <StoredProcParameter("@C2Per")>
        Public Property _C2Per() As String

        <StoredProcParameter("@C2Weeks")>
        Public Property _C2Weeks() As Decimal

        <StoredProcParameter("@C2CalcDay")>
        Public Property _C2CalcDay() As Decimal

        <StoredProcParameter("@C2CalcWeek")>
        Public Property _C2CalcWeek() As Decimal

        <StoredProcParameter("@C2CalcMonth")>
        Public Property _C2CalcMonth() As Decimal

        <StoredProcParameter("@C2CalcAnnum")>
        Public Property _C2CalcAnnum() As Decimal

        <StoredProcParameter("@C3Sign")>
        Public Property _C3Sign() As String

        <StoredProcParameter("@C3Units")>
        Public Property _C3Units() As Decimal

        <StoredProcParameter("@C3Multiplier")>
        Public Property _C3Multiplier() As String

        <StoredProcParameter("@C3Rate")>
        Public Property _C3Rate() As Decimal

        <StoredProcParameter("@C3Per")>
        Public Property _C3Per() As String

        <StoredProcParameter("@C3Weeks")>
        Public Property _C3Weeks() As Decimal

        <StoredProcParameter("@C3CalcDay")>
        Public Property _C3CalcDay() As Decimal

        <StoredProcParameter("@C3CalcWeek")>
        Public Property _C3CalcWeek() As Decimal

        <StoredProcParameter("@C3CalcMonth")>
        Public Property _C3CalcMonth() As Decimal

        <StoredProcParameter("@C3CalcAnnum")>
        Public Property _C3CalcAnnum() As Decimal

        <StoredProcParameter("@C4Sign")>
        Public Property _C4Sign() As String

        <StoredProcParameter("@C4Units")>
        Public Property _C4Units() As Decimal

        <StoredProcParameter("@C4Multiplier")>
        Public Property _C4Multiplier() As String

        <StoredProcParameter("@C4Rate")>
        Public Property _C4Rate() As Decimal

        <StoredProcParameter("@C4Per")>
        Public Property _C4Per() As String

        <StoredProcParameter("@C4Weeks")>
        Public Property _C4Weeks() As Decimal

        <StoredProcParameter("@C4CalcDay")>
        Public Property _C4CalcDay() As Decimal

        <StoredProcParameter("@C4CalcWeek")>
        Public Property _C4CalcWeek() As Decimal

        <StoredProcParameter("@C4CalcMonth")>
        Public Property _C4CalcMonth() As Decimal

        <StoredProcParameter("@C4CalcAnnum")>
        Public Property _C4CalcAnnum() As Decimal

        <StoredProcParameter("@C5Sign")>
        Public Property _C5Sign() As String

        <StoredProcParameter("@C5Units")>
        Public Property _C5Units() As Decimal

        <StoredProcParameter("@C5Multiplier")>
        Public Property _C5Multiplier() As String

        <StoredProcParameter("@C5Rate")>
        Public Property _C5Rate() As Decimal

        <StoredProcParameter("@C5Per")>
        Public Property _C5Per() As String

        <StoredProcParameter("@C5Weeks")>
        Public Property _C5Weeks() As Decimal

        <StoredProcParameter("@C5CalcDay")>
        Public Property _C5CalcDay() As Decimal

        <StoredProcParameter("@C5CalcWeek")>
        Public Property _C5CalcWeek() As Decimal

        <StoredProcParameter("@C5CalcMonth")>
        Public Property _C5CalcMonth() As Decimal

        <StoredProcParameter("@C5CalcAnnum")>
        Public Property _C5CalcAnnum() As Decimal

        <StoredProcParameter("@SplitType")>
        Public Property _SplitType() As String

        <StoredProcParameter("@TotalCalcWeek")>
        Public Property _TotalCalcWeek() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildCalculation
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildCalculationbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildCalculation
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildCalculationbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of ChildCalculation)
            Dim _ChildCalculationList As List(Of ChildCalculation)
            _ChildCalculationList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildCalculationTable"))
            Return _ChildCalculationList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildCalculation)
            Dim _ChildCalculationList As List(Of ChildCalculation)
            _ChildCalculationList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildCalculationTable"))
            Return _ChildCalculationList
        End Function

        Public Shared Sub SaveAll(ByVal ChildCalculationList As List(Of ChildCalculation))
            For Each _ChildCalculation As ChildCalculation In ChildCalculationList
                SaveRecord(_ChildCalculation)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildCalculationList As List(Of ChildCalculation))
            For Each _ChildCalculation As ChildCalculation In ChildCalculationList
                SaveRecord(ConnectionString, _ChildCalculation)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal ChildCalculation As ChildCalculation) As Guid
            Dim _Current As ChildCalculation = RetreiveByID(ChildCalculation._ID.Value)
            ChildCalculation.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildCalculation, _Current, "upsertChildCalculation")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildCalculation As ChildCalculation) As Guid
            Dim _Current As ChildCalculation = RetreiveByID(ConnectionString, ChildCalculation._ID.Value)
            ChildCalculation.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildCalculation, _Current, "upsertChildCalculation")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As ChildCalculation = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildCalculationbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As ChildCalculation = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildCalculationbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As ChildCalculation = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildCalculation")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As ChildCalculation = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildCalculation")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildCalculation

            Dim _C As ChildCalculation = Nothing

            If DR IsNot Nothing Then
                _C = New ChildCalculation()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._DateFrom = GetDate(DR("date_from"))
                    _C._Units = GetDecimal(DR("units"))
                    _C._Multiplier = DR("multiplier").ToString.Trim
                    _C._Rate = GetDecimal(DR("rate"))
                    _C._Per = DR("per").ToString.Trim
                    _C._Weeks = GetDecimal(DR("weeks"))
                    _C._CalcDay = GetDecimal(DR("calc_day"))
                    _C._CalcWeek = GetDecimal(DR("calc_week"))
                    _C._CalcMonth = GetDecimal(DR("calc_month"))
                    _C._CalcAnnum = GetDecimal(DR("calc_annum"))
                    _C._FundUnits = GetDecimal(DR("fund_units"))
                    _C._FundMultiplier = DR("fund_multiplier").ToString.Trim
                    _C._FundRate = GetDecimal(DR("fund_rate"))
                    _C._FundPer = DR("fund_per").ToString.Trim
                    _C._FundWeeks = GetDecimal(DR("fund_weeks"))
                    _C._FundCalcDay = GetDecimal(DR("fund_calc_day"))
                    _C._FundCalcWeek = GetDecimal(DR("fund_calc_week"))
                    _C._FundCalcMonth = GetDecimal(DR("fund_calc_month"))
                    _C._FundCalcAnnum = GetDecimal(DR("fund_calc_annum"))
                    _C._TotalCalcMonth = GetDecimal(DR("total_calc_month"))
                    _C._TotalCalcAnnum = GetDecimal(DR("total_calc_annum"))
                    _C._InvoiceText = DR("invoice_text").ToString.Trim
                    _C._Active = GetBoolean(DR("active"))
                    _C._DiscountMode = DR("discount_mode").ToString.Trim
                    _C._DiscountRate = GetDecimal(DR("discount_rate"))
                    _C._DiscountAmount = GetDecimal(DR("discount_amount"))
                    _C._C2Sign = DR("c2_sign").ToString.Trim
                    _C._C2Units = GetDecimal(DR("c2_units"))
                    _C._C2Multiplier = DR("c2_multiplier").ToString.Trim
                    _C._C2Rate = GetDecimal(DR("c2_rate"))
                    _C._C2Per = DR("c2_per").ToString.Trim
                    _C._C2Weeks = GetDecimal(DR("c2_weeks"))
                    _C._C2CalcDay = GetDecimal(DR("c2_calc_day"))
                    _C._C2CalcWeek = GetDecimal(DR("c2_calc_week"))
                    _C._C2CalcMonth = GetDecimal(DR("c2_calc_month"))
                    _C._C2CalcAnnum = GetDecimal(DR("c2_calc_annum"))
                    _C._C3Sign = DR("c3_sign").ToString.Trim
                    _C._C3Units = GetDecimal(DR("c3_units"))
                    _C._C3Multiplier = DR("c3_multiplier").ToString.Trim
                    _C._C3Rate = GetDecimal(DR("c3_rate"))
                    _C._C3Per = DR("c3_per").ToString.Trim
                    _C._C3Weeks = GetDecimal(DR("c3_weeks"))
                    _C._C3CalcDay = GetDecimal(DR("c3_calc_day"))
                    _C._C3CalcWeek = GetDecimal(DR("c3_calc_week"))
                    _C._C3CalcMonth = GetDecimal(DR("c3_calc_month"))
                    _C._C3CalcAnnum = GetDecimal(DR("c3_calc_annum"))
                    _C._C4Sign = DR("c4_sign").ToString.Trim
                    _C._C4Units = GetDecimal(DR("c4_units"))
                    _C._C4Multiplier = DR("c4_multiplier").ToString.Trim
                    _C._C4Rate = GetDecimal(DR("c4_rate"))
                    _C._C4Per = DR("c4_per").ToString.Trim
                    _C._C4Weeks = GetDecimal(DR("c4_weeks"))
                    _C._C4CalcDay = GetDecimal(DR("c4_calc_day"))
                    _C._C4CalcWeek = GetDecimal(DR("c4_calc_week"))
                    _C._C4CalcMonth = GetDecimal(DR("c4_calc_month"))
                    _C._C4CalcAnnum = GetDecimal(DR("c4_calc_annum"))
                    _C._C5Sign = DR("c5_sign").ToString.Trim
                    _C._C5Units = GetDecimal(DR("c5_units"))
                    _C._C5Multiplier = DR("c5_multiplier").ToString.Trim
                    _C._C5Rate = GetDecimal(DR("c5_rate"))
                    _C._C5Per = DR("c5_per").ToString.Trim
                    _C._C5Weeks = GetDecimal(DR("c5_weeks"))
                    _C._C5CalcDay = GetDecimal(DR("c5_calc_day"))
                    _C._C5CalcWeek = GetDecimal(DR("c5_calc_week"))
                    _C._C5CalcMonth = GetDecimal(DR("c5_calc_month"))
                    _C._C5CalcAnnum = GetDecimal(DR("c5_calc_annum"))
                    _C._SplitType = DR("split_type").ToString.Trim
                    _C._TotalCalcWeek = GetDecimal(DR("total_calc_week"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildCalculation)

            Dim _ChildCalculationList As New List(Of ChildCalculation)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildCalculationList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildCalculationList

        End Function


#End Region

    End Class

End Namespace
