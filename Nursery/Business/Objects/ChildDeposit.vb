﻿'*****************************************************
'Generated 16/07/2018 19:28:56 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildDeposit
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_DepDate As Date?
        Dim m_DepType As String
        Dim m_DepValue As Decimal
        Dim m_DepRef As String
        Dim m_DepPost As Boolean
        Dim m_DepPostBatch As Guid?
        Dim m_DepPostDoc As Guid?
        Dim m_DepUser As Guid?
        Dim m_DepStamp As DateTime?
        Dim m_DepNotes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._DepDate = Nothing
                ._DepType = ""
                ._DepValue = 0
                ._DepRef = ""
                ._DepPost = False
                ._DepPostBatch = Nothing
                ._DepPostDoc = Nothing
                ._DepUser = Nothing
                ._DepStamp = Nothing
                ._DepNotes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._DepDate = Nothing
                ._DepType = ""
                ._DepValue = 0
                ._DepRef = ""
                ._DepPost = False
                ._DepPostBatch = Nothing
                ._DepPostDoc = Nothing
                ._DepUser = Nothing
                ._DepStamp = Nothing
                ._DepNotes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@DepDate")>
        Public Property _DepDate() As Date?
            Get
                Return m_DepDate
            End Get
            Set(ByVal value As Date?)
                m_DepDate = value
            End Set
        End Property

        <StoredProcParameter("@DepType")>
        Public Property _DepType() As String
            Get
                Return m_DepType
            End Get
            Set(ByVal value As String)
                m_DepType = value
            End Set
        End Property

        <StoredProcParameter("@DepValue")>
        Public Property _DepValue() As Decimal
            Get
                Return m_DepValue
            End Get
            Set(ByVal value As Decimal)
                m_DepValue = value
            End Set
        End Property

        <StoredProcParameter("@DepRef")>
        Public Property _DepRef() As String
            Get
                Return m_DepRef
            End Get
            Set(ByVal value As String)
                m_DepRef = value
            End Set
        End Property

        <StoredProcParameter("@DepPost")>
        Public Property _DepPost() As Boolean
            Get
                Return m_DepPost
            End Get
            Set(ByVal value As Boolean)
                m_DepPost = value
            End Set
        End Property

        <StoredProcParameter("@DepPostBatch")>
        Public Property _DepPostBatch() As Guid?
            Get
                Return m_DepPostBatch
            End Get
            Set(ByVal value As Guid?)
                m_DepPostBatch = value
            End Set
        End Property

        <StoredProcParameter("@DepPostDoc")>
        Public Property _DepPostDoc() As Guid?
            Get
                Return m_DepPostDoc
            End Get
            Set(ByVal value As Guid?)
                m_DepPostDoc = value
            End Set
        End Property

        <StoredProcParameter("@DepUser")>
        Public Property _DepUser() As Guid?
            Get
                Return m_DepUser
            End Get
            Set(ByVal value As Guid?)
                m_DepUser = value
            End Set
        End Property

        <StoredProcParameter("@DepStamp")>
        Public Property _DepStamp() As DateTime?
            Get
                Return m_DepStamp
            End Get
            Set(ByVal value As DateTime?)
                m_DepStamp = value
            End Set
        End Property

        <StoredProcParameter("@DepNotes")>
        Public Property _DepNotes() As String
            Get
                Return m_DepNotes
            End Get
            Set(ByVal value As String)
                m_DepNotes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildDeposit

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildDepositbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildDeposit

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildDepositbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildDeposit)

            Dim _ChildDepositList As List(Of ChildDeposit)
            _ChildDepositList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildDepositTable"))
            Return _ChildDepositList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildDeposit)

            Dim _ChildDepositList As List(Of ChildDeposit)
            _ChildDepositList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildDepositTable"))
            Return _ChildDepositList

        End Function

        Public Shared Sub SaveAll(ByVal ChildDepositList As List(Of ChildDeposit))

            For Each _ChildDeposit As ChildDeposit In ChildDepositList
                SaveRecord(_ChildDeposit)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildDepositList As List(Of ChildDeposit))

            For Each _ChildDeposit As ChildDeposit In ChildDepositList
                SaveRecord(ConnectionString, _ChildDeposit)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildDeposit As ChildDeposit) As Guid
            ChildDeposit.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildDeposit, "upsertChildDeposit")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildDeposit As ChildDeposit) As Guid
            ChildDeposit.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildDeposit, "upsertChildDeposit")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildDepositbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteChildDepositbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildDeposit")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildDeposit")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildDeposit

            Dim _C As ChildDeposit = Nothing

            If DR IsNot Nothing Then
                _C = New ChildDeposit()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._DepDate = GetDate(DR("dep_date"))
                    _C._DepType = DR("dep_type").ToString.Trim
                    _C._DepValue = GetDecimal(DR("dep_value"))
                    _C._DepRef = DR("dep_ref").ToString.Trim
                    _C._DepPost = GetBoolean(DR("dep_post"))
                    _C._DepPostBatch = GetGUID(DR("dep_post_batch"))
                    _C._DepPostDoc = GetGUID(DR("dep_post_doc"))
                    _C._DepUser = GetGUID(DR("dep_user"))
                    _C._DepStamp = GetDateTime(DR("dep_stamp"))
                    _C._DepNotes = DR("dep_notes").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildDeposit)

            Dim _ChildDepositList As New List(Of ChildDeposit)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildDepositList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildDepositList

        End Function


#End Region

    End Class

End Namespace
