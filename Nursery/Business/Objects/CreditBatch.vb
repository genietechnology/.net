﻿'*****************************************************
'Generated 18/07/2018 22:10:03 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CreditBatch
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_BatchNo As Long
        Dim m_BatchStatus As String
        Dim m_BatchDate As Date?
        Dim m_UserId As Guid?
        Dim m_Stamp As DateTime?
        Dim m_Comments As String
        Dim m_SiteId As Guid?
        Dim m_SiteName As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BatchNo = 0
                ._BatchStatus = ""
                ._BatchDate = Nothing
                ._UserId = Nothing
                ._Stamp = Nothing
                ._Comments = ""
                ._SiteId = Nothing
                ._SiteName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BatchNo = 0
                ._BatchStatus = ""
                ._BatchDate = Nothing
                ._UserId = Nothing
                ._Stamp = Nothing
                ._Comments = ""
                ._SiteId = Nothing
                ._SiteName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@BatchNo")>
        Public Property _BatchNo() As Long
            Get
                Return m_BatchNo
            End Get
            Set(ByVal value As Long)
                m_BatchNo = value
            End Set
        End Property

        <StoredProcParameter("@BatchStatus")>
        Public Property _BatchStatus() As String
            Get
                Return m_BatchStatus
            End Get
            Set(ByVal value As String)
                m_BatchStatus = value
            End Set
        End Property

        <StoredProcParameter("@BatchDate")>
        Public Property _BatchDate() As Date?
            Get
                Return m_BatchDate
            End Get
            Set(ByVal value As Date?)
                m_BatchDate = value
            End Set
        End Property

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@Comments")>
        Public Property _Comments() As String
            Get
                Return m_Comments
            End Get
            Set(ByVal value As String)
                m_Comments = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CreditBatch

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCreditBatchbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CreditBatch

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCreditBatchbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CreditBatch)

            Dim _CreditBatchList As List(Of CreditBatch)
            _CreditBatchList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCreditBatchTable"))
            Return _CreditBatchList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CreditBatch)

            Dim _CreditBatchList As List(Of CreditBatch)
            _CreditBatchList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCreditBatchTable"))
            Return _CreditBatchList

        End Function

        Public Shared Sub SaveAll(ByVal CreditBatchList As List(Of CreditBatch))

            For Each _CreditBatch As CreditBatch In CreditBatchList
                SaveRecord(_CreditBatch)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CreditBatchList As List(Of CreditBatch))

            For Each _CreditBatch As CreditBatch In CreditBatchList
                SaveRecord(ConnectionString, _CreditBatch)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CreditBatch As CreditBatch) As Guid
            CreditBatch.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CreditBatch, "upsertCreditBatch")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CreditBatch As CreditBatch) As Guid
            CreditBatch.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CreditBatch, "upsertCreditBatch")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCreditBatchbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCreditBatchbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCreditBatch")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCreditBatch")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CreditBatch

            Dim _C As CreditBatch = Nothing

            If DR IsNot Nothing Then
                _C = New CreditBatch()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._BatchNo = GetLong(DR("batch_no"))
                    _C._BatchStatus = DR("batch_status").ToString.Trim
                    _C._BatchDate = GetDate(DR("batch_date"))
                    _C._UserId = GetGUID(DR("user_id"))
                    _C._Stamp = GetDateTime(DR("stamp"))
                    _C._Comments = DR("comments").ToString.Trim
                    _C._SiteId = GetGUID(DR("site_id"))
                    _C._SiteName = DR("site_name").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CreditBatch)

            Dim _CreditBatchList As New List(Of CreditBatch)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CreditBatchList.Add(PropertiesFromData(_DR))
            Next

            Return _CreditBatchList

        End Function


#End Region

    End Class

End Namespace
