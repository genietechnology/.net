﻿'*****************************************************
'Generated 05/12/2017 18:12:52 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class FundDay
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_BatchId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_ChildDob As Date?
        Dim m_ChildMonths As Integer
        Dim m_ActionDate As Date?
        Dim m_FundHrs As Decimal
        Dim m_FundRate As Decimal
        Dim m_FundValue As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BatchId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._ChildDob = Nothing
                ._ChildMonths = 0
                ._ActionDate = Nothing
                ._FundHrs = 0
                ._FundRate = 0
                ._FundValue = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BatchId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._ChildDob = Nothing
                ._ChildMonths = 0
                ._ActionDate = Nothing
                ._FundHrs = 0
                ._FundRate = 0
                ._FundValue = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@BatchId")>
        Public Property _BatchId() As Guid?
            Get
                Return m_BatchId
            End Get
            Set(ByVal value As Guid?)
                m_BatchId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")>
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@ChildDob")>
        Public Property _ChildDob() As Date?
            Get
                Return m_ChildDob
            End Get
            Set(ByVal value As Date?)
                m_ChildDob = value
            End Set
        End Property

        <StoredProcParameter("@ChildMonths")>
        Public Property _ChildMonths() As Integer
            Get
                Return m_ChildMonths
            End Get
            Set(ByVal value As Integer)
                m_ChildMonths = value
            End Set
        End Property

        <StoredProcParameter("@ActionDate")>
        Public Property _ActionDate() As Date?
            Get
                Return m_ActionDate
            End Get
            Set(ByVal value As Date?)
                m_ActionDate = value
            End Set
        End Property

        <StoredProcParameter("@FundHrs")>
        Public Property _FundHrs() As Decimal
            Get
                Return m_FundHrs
            End Get
            Set(ByVal value As Decimal)
                m_FundHrs = value
            End Set
        End Property

        <StoredProcParameter("@FundRate")>
        Public Property _FundRate() As Decimal
            Get
                Return m_FundRate
            End Get
            Set(ByVal value As Decimal)
                m_FundRate = value
            End Set
        End Property

        <StoredProcParameter("@FundValue")>
        Public Property _FundValue() As Decimal
            Get
                Return m_FundValue
            End Get
            Set(ByVal value As Decimal)
                m_FundValue = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As FundDay

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getFundDaybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As FundDay

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getFundDaybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of FundDay)

            Dim _FundDayList As List(Of FundDay)
            _FundDayList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getFundDayTable"))
            Return _FundDayList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of FundDay)

            Dim _FundDayList As List(Of FundDay)
            _FundDayList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getFundDayTable"))
            Return _FundDayList

        End Function

        Public Shared Sub SaveAll(ByVal FundDayList As List(Of FundDay))

            For Each _FundDay As FundDay In FundDayList
                SaveRecord(_FundDay)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal FundDayList As List(Of FundDay))

            For Each _FundDay As FundDay In FundDayList
                SaveRecord(ConnectionString, _FundDay)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal FundDay As FundDay) As Guid
            FundDay.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, FundDay, "upsertFundDay")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal FundDay As FundDay) As Guid
            FundDay.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, FundDay, "upsertFundDay")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteFundDaybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteFundDaybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertFundDay")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertFundDay")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As FundDay

            Dim _F As FundDay = Nothing

            If DR IsNot Nothing Then
                _F = New FundDay()
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._BatchId = GetGUID(DR("batch_id"))
                    _F._ChildId = GetGUID(DR("child_id"))
                    _F._ChildName = DR("child_name").ToString.Trim
                    _F._ChildDob = GetDate(DR("child_dob"))
                    _F._ChildMonths = GetInteger(DR("child_months"))
                    _F._ActionDate = GetDate(DR("action_date"))
                    _F._FundHrs = GetDecimal(DR("fund_hrs"))
                    _F._FundRate = GetDecimal(DR("fund_rate"))
                    _F._FundValue = GetDecimal(DR("fund_value"))

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of FundDay)

            Dim _FundDayList As New List(Of FundDay)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _FundDayList.Add(PropertiesFromData(_DR))
            Next

            Return _FundDayList

        End Function


#End Region

    End Class

End Namespace
