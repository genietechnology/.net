﻿'*****************************************************
'Generated 25/05/2017 07:42:44 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CheckListItem
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ListId As Guid?
        Dim m_Description As String
        Dim m_ItemType As String
        Dim m_Mandatory As Boolean
        Dim m_Points As Integer
        Dim m_Sequence As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ListId = Nothing
                ._Description = ""
                ._ItemType = ""
                ._Mandatory = False
                ._Points = 0
                ._Sequence = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ListId = Nothing
                ._Description = ""
                ._ItemType = ""
                ._Mandatory = False
                ._Points = 0
                ._Sequence = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ListId")>
        Public Property _ListId() As Guid?
            Get
                Return m_ListId
            End Get
            Set(ByVal value As Guid?)
                m_ListId = value
            End Set
        End Property

        <StoredProcParameter("@Description")>
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@ItemType")>
        Public Property _ItemType() As String
            Get
                Return m_ItemType
            End Get
            Set(ByVal value As String)
                m_ItemType = value
            End Set
        End Property

        <StoredProcParameter("@Mandatory")>
        Public Property _Mandatory() As Boolean
            Get
                Return m_Mandatory
            End Get
            Set(ByVal value As Boolean)
                m_Mandatory = value
            End Set
        End Property

        <StoredProcParameter("@Points")>
        Public Property _Points() As Integer
            Get
                Return m_Points
            End Get
            Set(ByVal value As Integer)
                m_Points = value
            End Set
        End Property

        <StoredProcParameter("@Sequence")>
        Public Property _Sequence() As Integer
            Get
                Return m_Sequence
            End Get
            Set(ByVal value As Integer)
                m_Sequence = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CheckListItem

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCheckListItembyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CheckListItem

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCheckListItembyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CheckListItem)

            Dim _CheckListItemList As List(Of CheckListItem)
            _CheckListItemList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCheckListItemTable"))
            Return _CheckListItemList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CheckListItem)

            Dim _CheckListItemList As List(Of CheckListItem)
            _CheckListItemList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCheckListItemTable"))
            Return _CheckListItemList

        End Function

        Public Shared Sub SaveAll(ByVal CheckListItemList As List(Of CheckListItem))

            For Each _CheckListItem As CheckListItem In CheckListItemList
                SaveRecord(_CheckListItem)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CheckListItemList As List(Of CheckListItem))

            For Each _CheckListItem As CheckListItem In CheckListItemList
                SaveRecord(ConnectionString, _CheckListItem)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CheckListItem As CheckListItem) As Guid
            CheckListItem.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CheckListItem, "upsertCheckListItem")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CheckListItem As CheckListItem) As Guid
            CheckListItem.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CheckListItem, "upsertCheckListItem")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCheckListItembyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCheckListItembyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCheckListItem")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCheckListItem")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CheckListItem

            Dim _C As CheckListItem = Nothing

            If DR IsNot Nothing Then
                _C = New CheckListItem()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ListId = GetGUID(DR("list_id"))
                    _C._Description = DR("description").ToString.Trim
                    _C._ItemType = DR("item_type").ToString.Trim
                    _C._Mandatory = GetBoolean(DR("mandatory"))
                    _C._Points = GetInteger(DR("points"))
                    _C._Sequence = GetInteger(DR("sequence"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CheckListItem)

            Dim _CheckListItemList As New List(Of CheckListItem)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CheckListItemList.Add(PropertiesFromData(_DR))
            Next

            Return _CheckListItemList

        End Function


#End Region

    End Class

End Namespace
