﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CDAPStep
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Step As Integer
        Dim m_AgeMin As Integer
        Dim m_AgeMax As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Step = 0
                ._AgeMin = 0
                ._AgeMax = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Step = 0
                ._AgeMin = 0
                ._AgeMax = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Step")> _
        Public Property _Step() As Integer
            Get
                Return m_Step
            End Get
            Set(ByVal value As Integer)
                m_Step = value
            End Set
        End Property

        <StoredProcParameter("@AgeMin")> _
        Public Property _AgeMin() As Integer
            Get
                Return m_AgeMin
            End Get
            Set(ByVal value As Integer)
                m_AgeMin = value
            End Set
        End Property

        <StoredProcParameter("@AgeMax")> _
        Public Property _AgeMax() As Integer
            Get
                Return m_AgeMax
            End Get
            Set(ByVal value As Integer)
                m_AgeMax = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CDAPStep

            Dim _CDAPStep As CDAPStep
            _CDAPStep = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCDAPStepbyID", ID))
            Return _CDAPStep

        End Function

        Public Shared Function RetreiveAll() As List(Of CDAPStep)

            Dim _CDAPStepList As List(Of CDAPStep)
            _CDAPStepList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCDAPStepTable"))
            Return _CDAPStepList

        End Function

        Public Shared Sub SaveAll(ByVal CDAPStepList As List(Of CDAPStep))

            For Each _CDAPStep As CDAPStep In CDAPStepList
                SaveRecord(_CDAPStep)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CDAPStep As CDAPStep) As Guid
            DAL.SaveRecord(Session.ConnectionManager, CDAPStep, "upsertCDAPStep")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCDAPStepbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCDAPStep")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CDAPStep

            Dim _C As New CDAPStep()

            If DR IsNot Nothing Then
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Step = GetInteger(DR("step"))
                    _C._AgeMin = GetInteger(DR("age_min"))
                    _C._AgeMax = GetInteger(DR("age_max"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CDAPStep)

            Dim _CDAPStepList As New List(Of CDAPStep)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CDAPStepList.Add(PropertiesFromData(_DR))
            Next

            Return _CDAPStepList

        End Function


#End Region

    End Class

End Namespace
