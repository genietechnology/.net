﻿'*****************************************************
'Generated 24/09/2018 09:49:36 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildAnnual
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._DateInvoiced = Nothing
                ._DateDue = Nothing
                ._SplitType = ""
                ._SplitSegments = Nothing
                ._SplitTotal = 0
                ._SplitBatch = Nothing
                ._AnnualInvoice = Nothing
                ._AnnualTotal = 0
                ._Active = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._DateInvoiced = Nothing
                ._DateDue = Nothing
                ._SplitType = ""
                ._SplitSegments = Nothing
                ._SplitTotal = 0
                ._SplitBatch = Nothing
                ._AnnualInvoice = Nothing
                ._AnnualTotal = 0
                ._Active = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?

        <StoredProcParameter("@DateFrom")>
        Public Property _DateFrom() As Date?

        <StoredProcParameter("@DateTo")>
        Public Property _DateTo() As Date?

        <StoredProcParameter("@DateInvoiced")>
        Public Property _DateInvoiced() As Date?

        <StoredProcParameter("@DateDue")>
        Public Property _DateDue() As Date?

        <StoredProcParameter("@SplitType")>
        Public Property _SplitType() As String

        <StoredProcParameter("@SplitSegments")>
        Public Property _SplitSegments() As Byte

        <StoredProcParameter("@SplitTotal")>
        Public Property _SplitTotal() As Decimal

        <StoredProcParameter("@SplitBatch")>
        Public Property _SplitBatch() As Guid?

        <StoredProcParameter("@AnnualInvoice")>
        Public Property _AnnualInvoice() As Guid?

        <StoredProcParameter("@AnnualTotal")>
        Public Property _AnnualTotal() As Decimal

        <StoredProcParameter("@Active")>
        Public Property _Active() As Boolean


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildAnnual
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildAnnualbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildAnnual
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildAnnualbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of ChildAnnual)
            Dim _ChildAnnualList As List(Of ChildAnnual)
            _ChildAnnualList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildAnnualTable"))
            Return _ChildAnnualList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildAnnual)
            Dim _ChildAnnualList As List(Of ChildAnnual)
            _ChildAnnualList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildAnnualTable"))
            Return _ChildAnnualList
        End Function

        Public Shared Sub SaveAll(ByVal ChildAnnualList As List(Of ChildAnnual))
            For Each _ChildAnnual As ChildAnnual In ChildAnnualList
                SaveRecord(_ChildAnnual)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildAnnualList As List(Of ChildAnnual))
            For Each _ChildAnnual As ChildAnnual In ChildAnnualList
                SaveRecord(ConnectionString, _ChildAnnual)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal ChildAnnual As ChildAnnual) As Guid
            Dim _Current As ChildAnnual = RetreiveByID(ChildAnnual._ID.Value)
            ChildAnnual.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildAnnual, _Current, "upsertChildAnnual")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildAnnual As ChildAnnual) As Guid
            Dim _Current As ChildAnnual = RetreiveByID(ConnectionString, ChildAnnual._ID.Value)
            ChildAnnual.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, ChildAnnual, _Current, "upsertChildAnnual")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As ChildAnnual = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildAnnualbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As ChildAnnual = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildAnnualbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As ChildAnnual = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildAnnual")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As ChildAnnual = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChildAnnual")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildAnnual

            Dim _C As ChildAnnual = Nothing

            If DR IsNot Nothing Then
                _C = New ChildAnnual()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._DateFrom = GetDate(DR("date_from"))
                    _C._DateTo = GetDate(DR("date_to"))
                    _C._DateInvoiced = GetDate(DR("date_invoiced"))
                    _C._DateDue = GetDate(DR("date_due"))
                    _C._SplitType = DR("split_type").ToString.Trim
                    _C._SplitSegments = GetByte(DR("split_segments"))
                    _C._SplitTotal = GetDecimal(DR("split_total"))
                    _C._SplitBatch = GetGUID(DR("split_batch"))
                    _C._AnnualInvoice = GetGUID(DR("annual_invoice"))
                    _C._AnnualTotal = GetDecimal(DR("annual_total"))
                    _C._Active = GetBoolean(DR("active"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildAnnual)

            Dim _ChildAnnualList As New List(Of ChildAnnual)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildAnnualList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildAnnualList

        End Function


#End Region

    End Class

End Namespace
