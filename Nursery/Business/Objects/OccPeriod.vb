﻿'*****************************************************
'Generated 07/04/2016 13:29:03 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class OccPeriod
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_PeriodFrom As Date?
        Dim m_PeriodTo As Date?
        Dim m_RoomId As Guid?
        Dim m_RoomName As String
        Dim m_RoomCapacity As Integer
        Dim m_RoomRatio As Integer
        Dim m_FteHours As Decimal
        Dim m_TotCapacity As Integer
        Dim m_Occ As Decimal
        Dim m_AvgFte As Decimal
        Dim m_MaxFt As Decimal
        Dim m_MaxPt As Decimal
        Dim m_SumHours As Decimal
        Dim m_SumFunded As Decimal
        Dim m_SumRevenue As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._PeriodFrom = Nothing
                ._PeriodTo = Nothing
                ._RoomId = Nothing
                ._RoomName = ""
                ._RoomCapacity = 0
                ._RoomRatio = 0
                ._FteHours = 0
                ._TotCapacity = 0
                ._Occ = 0
                ._AvgFte = 0
                ._MaxFt = 0
                ._MaxPt = 0
                ._SumHours = 0
                ._SumFunded = 0
                ._SumRevenue = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._PeriodFrom = Nothing
                ._PeriodTo = Nothing
                ._RoomId = Nothing
                ._RoomName = ""
                ._RoomCapacity = 0
                ._RoomRatio = 0
                ._FteHours = 0
                ._TotCapacity = 0
                ._Occ = 0
                ._AvgFte = 0
                ._MaxFt = 0
                ._MaxPt = 0
                ._SumHours = 0
                ._SumFunded = 0
                ._SumRevenue = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@PeriodFrom")> _
        Public Property _PeriodFrom() As Date?
            Get
                Return m_PeriodFrom
            End Get
            Set(ByVal value As Date?)
                m_PeriodFrom = value
            End Set
        End Property

        <StoredProcParameter("@PeriodTo")> _
        Public Property _PeriodTo() As Date?
            Get
                Return m_PeriodTo
            End Get
            Set(ByVal value As Date?)
                m_PeriodTo = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@RoomName")> _
        Public Property _RoomName() As String
            Get
                Return m_RoomName
            End Get
            Set(ByVal value As String)
                m_RoomName = value
            End Set
        End Property

        <StoredProcParameter("@RoomCapacity")> _
        Public Property _RoomCapacity() As Integer
            Get
                Return m_RoomCapacity
            End Get
            Set(ByVal value As Integer)
                m_RoomCapacity = value
            End Set
        End Property

        <StoredProcParameter("@RoomRatio")> _
        Public Property _RoomRatio() As Integer
            Get
                Return m_RoomRatio
            End Get
            Set(ByVal value As Integer)
                m_RoomRatio = value
            End Set
        End Property

        <StoredProcParameter("@FteHours")> _
        Public Property _FteHours() As Decimal
            Get
                Return m_FteHours
            End Get
            Set(ByVal value As Decimal)
                m_FteHours = value
            End Set
        End Property

        <StoredProcParameter("@TotCapacity")> _
        Public Property _TotCapacity() As Integer
            Get
                Return m_TotCapacity
            End Get
            Set(ByVal value As Integer)
                m_TotCapacity = value
            End Set
        End Property

        <StoredProcParameter("@Occ")> _
        Public Property _Occ() As Decimal
            Get
                Return m_Occ
            End Get
            Set(ByVal value As Decimal)
                m_Occ = value
            End Set
        End Property

        <StoredProcParameter("@AvgFte")> _
        Public Property _AvgFte() As Decimal
            Get
                Return m_AvgFte
            End Get
            Set(ByVal value As Decimal)
                m_AvgFte = value
            End Set
        End Property

        <StoredProcParameter("@MaxFt")> _
        Public Property _MaxFt() As Decimal
            Get
                Return m_MaxFt
            End Get
            Set(ByVal value As Decimal)
                m_MaxFt = value
            End Set
        End Property

        <StoredProcParameter("@MaxPt")> _
        Public Property _MaxPt() As Decimal
            Get
                Return m_MaxPt
            End Get
            Set(ByVal value As Decimal)
                m_MaxPt = value
            End Set
        End Property

        <StoredProcParameter("@SumHours")> _
        Public Property _SumHours() As Decimal
            Get
                Return m_SumHours
            End Get
            Set(ByVal value As Decimal)
                m_SumHours = value
            End Set
        End Property

        <StoredProcParameter("@SumFunded")> _
        Public Property _SumFunded() As Decimal
            Get
                Return m_SumFunded
            End Get
            Set(ByVal value As Decimal)
                m_SumFunded = value
            End Set
        End Property

        <StoredProcParameter("@SumRevenue")> _
        Public Property _SumRevenue() As Decimal
            Get
                Return m_SumRevenue
            End Get
            Set(ByVal value As Decimal)
                m_SumRevenue = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As OccPeriod

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getOccPeriodbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of OccPeriod)

            Dim _OccPeriodList As List(Of OccPeriod)
            _OccPeriodList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getOccPeriodTable"))
            Return _OccPeriodList

        End Function

        Public Shared Sub SaveAll(ByVal OccPeriodList As List(Of OccPeriod))

            For Each _OccPeriod As OccPeriod In OccPeriodList
                SaveRecord(_OccPeriod)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal OccPeriod As OccPeriod) As Guid
            OccPeriod.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, OccPeriod, "upsertOccPeriod")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteOccPeriodbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertOccPeriod")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As OccPeriod

            Dim _O As OccPeriod = Nothing

            If DR IsNot Nothing Then
                _O = New OccPeriod()
                With DR
                    _O.IsNew = False
                    _O.IsDeleted = False
                    _O._ID = GetGUID(DR("ID"))
                    _O._PeriodFrom = GetDate(DR("period_from"))
                    _O._PeriodTo = GetDate(DR("period_to"))
                    _O._RoomId = GetGUID(DR("room_id"))
                    _O._RoomName = DR("room_name").ToString.Trim
                    _O._RoomCapacity = GetInteger(DR("room_capacity"))
                    _O._RoomRatio = GetInteger(DR("room_ratio"))
                    _O._FteHours = GetDecimal(DR("fte_hours"))
                    _O._TotCapacity = GetInteger(DR("tot_capacity"))
                    _O._Occ = GetDecimal(DR("occ"))
                    _O._AvgFte = GetDecimal(DR("avg_fte"))
                    _O._MaxFt = GetDecimal(DR("max_ft"))
                    _O._MaxPt = GetDecimal(DR("max_pt"))
                    _O._SumHours = GetDecimal(DR("sum_hours"))
                    _O._SumFunded = GetDecimal(DR("sum_funded"))
                    _O._SumRevenue = GetDecimal(DR("sum_revenue"))

                End With
            End If

            Return _O

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of OccPeriod)

            Dim _OccPeriodList As New List(Of OccPeriod)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _OccPeriodList.Add(PropertiesFromData(_DR))
            Next

            Return _OccPeriodList

        End Function


#End Region

    End Class

End Namespace
