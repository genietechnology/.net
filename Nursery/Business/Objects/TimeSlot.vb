﻿'*****************************************************
'Generated 30/09/2016 22:36:44 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TimeSlot
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Category As String
        Dim m_StartTime As TimeSpan?
        Dim m_EndTime As TimeSpan?
        Dim m_ShortName As String
        Dim m_Headcount As Boolean
        Dim m_Am As Boolean
        Dim m_Pm As Boolean
        Dim m_Breakfast As Boolean
        Dim m_Lunch As Boolean
        Dim m_Tea As Boolean
        Dim m_Snack As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Category = ""
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._ShortName = ""
                ._Headcount = False
                ._Am = False
                ._Pm = False
                ._Breakfast = False
                ._Lunch = False
                ._Tea = False
                ._Snack = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Category = ""
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._ShortName = ""
                ._Headcount = False
                ._Am = False
                ._Pm = False
                ._Breakfast = False
                ._Lunch = False
                ._Tea = False
                ._Snack = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Category")> _
        Public Property _Category() As String
            Get
                Return m_Category
            End Get
            Set(ByVal value As String)
                m_Category = value
            End Set
        End Property

        <StoredProcParameter("@StartTime")> _
        Public Property _StartTime() As TimeSpan?
            Get
                Return m_StartTime
            End Get
            Set(ByVal value As TimeSpan?)
                m_StartTime = value
            End Set
        End Property

        <StoredProcParameter("@EndTime")> _
        Public Property _EndTime() As TimeSpan?
            Get
                Return m_EndTime
            End Get
            Set(ByVal value As TimeSpan?)
                m_EndTime = value
            End Set
        End Property

        <StoredProcParameter("@ShortName")> _
        Public Property _ShortName() As String
            Get
                Return m_ShortName
            End Get
            Set(ByVal value As String)
                m_ShortName = value
            End Set
        End Property

        <StoredProcParameter("@Headcount")> _
        Public Property _Headcount() As Boolean
            Get
                Return m_Headcount
            End Get
            Set(ByVal value As Boolean)
                m_Headcount = value
            End Set
        End Property

        <StoredProcParameter("@Am")> _
        Public Property _Am() As Boolean
            Get
                Return m_Am
            End Get
            Set(ByVal value As Boolean)
                m_Am = value
            End Set
        End Property

        <StoredProcParameter("@Pm")> _
        Public Property _Pm() As Boolean
            Get
                Return m_Pm
            End Get
            Set(ByVal value As Boolean)
                m_Pm = value
            End Set
        End Property

        <StoredProcParameter("@Breakfast")> _
        Public Property _Breakfast() As Boolean
            Get
                Return m_Breakfast
            End Get
            Set(ByVal value As Boolean)
                m_Breakfast = value
            End Set
        End Property

        <StoredProcParameter("@Lunch")> _
        Public Property _Lunch() As Boolean
            Get
                Return m_Lunch
            End Get
            Set(ByVal value As Boolean)
                m_Lunch = value
            End Set
        End Property

        <StoredProcParameter("@Tea")> _
        Public Property _Tea() As Boolean
            Get
                Return m_Tea
            End Get
            Set(ByVal value As Boolean)
                m_Tea = value
            End Set
        End Property

        <StoredProcParameter("@Snack")> _
        Public Property _Snack() As Boolean
            Get
                Return m_Snack
            End Get
            Set(ByVal value As Boolean)
                m_Snack = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TimeSlot

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTimeSlotbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of TimeSlot)

            Dim _TimeSlotList As List(Of TimeSlot)
            _TimeSlotList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTimeSlotTable"))
            Return _TimeSlotList

        End Function

        Public Shared Sub SaveAll(ByVal TimeSlotList As List(Of TimeSlot))

            For Each _TimeSlot As TimeSlot In TimeSlotList
                SaveRecord(_TimeSlot)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal TimeSlot As TimeSlot) As Guid
            TimeSlot.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, TimeSlot, "upsertTimeSlot")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTimeSlotbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertTimeSlot")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TimeSlot

            Dim _T As TimeSlot = Nothing

            If DR IsNot Nothing Then
                _T = New TimeSlot()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Name = DR("name").ToString.Trim
                    _T._Category = DR("category").ToString.Trim
                    _T._StartTime = GetTimeSpan(DR("start_time"))
                    _T._EndTime = GetTimeSpan(DR("end_time"))
                    _T._ShortName = DR("short_name").ToString.Trim
                    _T._Headcount = GetBoolean(DR("headcount"))
                    _T._Am = GetBoolean(DR("am"))
                    _T._Pm = GetBoolean(DR("pm"))
                    _T._Breakfast = GetBoolean(DR("breakfast"))
                    _T._Lunch = GetBoolean(DR("lunch"))
                    _T._Tea = GetBoolean(DR("tea"))
                    _T._Snack = GetBoolean(DR("snack"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TimeSlot)

            Dim _TimeSlotList As New List(Of TimeSlot)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TimeSlotList.Add(PropertiesFromData(_DR))
            Next

            Return _TimeSlotList

        End Function


#End Region

    End Class

End Namespace
