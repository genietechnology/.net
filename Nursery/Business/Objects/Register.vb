﻿'*****************************************************
'Generated 22/05/2016 16:24:14 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Register
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Date As DateTime?
        Dim m_Type As String
        Dim m_InOut As String
        Dim m_PersonId As Guid?
        Dim m_Name As String
        Dim m_StampIn As DateTime?
        Dim m_StampOut As DateTime?
        Dim m_Location As String
        Dim m_Ref1 As String
        Dim m_Ref2 As String
        Dim m_Ref3 As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Date = Nothing
                ._Type = ""
                ._InOut = ""
                ._PersonId = Nothing
                ._Name = ""
                ._StampIn = Nothing
                ._StampOut = Nothing
                ._Location = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Date = Nothing
                ._Type = ""
                ._InOut = ""
                ._PersonId = Nothing
                ._Name = ""
                ._StampIn = Nothing
                ._StampOut = Nothing
                ._Location = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As DateTime?
            Get
                Return m_Date
            End Get
            Set(ByVal value As DateTime?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@InOut")> _
        Public Property _InOut() As String
            Get
                Return m_InOut
            End Get
            Set(ByVal value As String)
                m_InOut = value
            End Set
        End Property

        <StoredProcParameter("@PersonId")> _
        Public Property _PersonId() As Guid?
            Get
                Return m_PersonId
            End Get
            Set(ByVal value As Guid?)
                m_PersonId = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@StampIn")> _
        Public Property _StampIn() As DateTime?
            Get
                Return m_StampIn
            End Get
            Set(ByVal value As DateTime?)
                m_StampIn = value
            End Set
        End Property

        <StoredProcParameter("@StampOut")> _
        Public Property _StampOut() As DateTime?
            Get
                Return m_StampOut
            End Get
            Set(ByVal value As DateTime?)
                m_StampOut = value
            End Set
        End Property

        <StoredProcParameter("@Location")> _
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@Ref1")> _
        Public Property _Ref1() As String
            Get
                Return m_Ref1
            End Get
            Set(ByVal value As String)
                m_Ref1 = value
            End Set
        End Property

        <StoredProcParameter("@Ref2")> _
        Public Property _Ref2() As String
            Get
                Return m_Ref2
            End Get
            Set(ByVal value As String)
                m_Ref2 = value
            End Set
        End Property

        <StoredProcParameter("@Ref3")> _
        Public Property _Ref3() As String
            Get
                Return m_Ref3
            End Get
            Set(ByVal value As String)
                m_Ref3 = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Register

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRegisterbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Register)

            Dim _RegisterList As List(Of Register)
            _RegisterList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRegisterTable"))
            Return _RegisterList

        End Function

        Public Shared Sub SaveAll(ByVal RegisterList As List(Of Register))

            For Each _Register As Register In RegisterList
                SaveRecord(_Register)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Register As Register) As Guid
            Register.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Register, "upsertRegister")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRegisterbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRegister")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Register

            Dim _R As Register = Nothing

            If DR IsNot Nothing Then
                _R = New Register()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._Date = GetDateTime(DR("date"))
                    _R._Type = DR("type").ToString.Trim
                    _R._InOut = DR("in_out").ToString.Trim
                    _R._PersonId = GetGUID(DR("person_id"))
                    _R._Name = DR("name").ToString.Trim
                    _R._StampIn = GetDateTime(DR("stamp_in"))
                    _R._StampOut = GetDateTime(DR("stamp_out"))
                    _R._Location = DR("location").ToString.Trim
                    _R._Ref1 = DR("ref_1").ToString.Trim
                    _R._Ref2 = DR("ref_2").ToString.Trim
                    _R._Ref3 = DR("ref_3").ToString.Trim

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Register)

            Dim _RegisterList As New List(Of Register)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RegisterList.Add(PropertiesFromData(_DR))
            Next

            Return _RegisterList

        End Function


#End Region

    End Class

End Namespace
