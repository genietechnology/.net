﻿'*****************************************************
'Generated 24/09/2018 09:44:22 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Child
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Status = ""
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Dob = Nothing
                ._Gender = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._Started = Nothing
                ._DateLeft = Nothing
                ._KeyworkerId = Nothing
                ._KeyworkerName = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._GroupStarted = Nothing
                ._Nappies = False
                ._Milk = False
                ._BabyFood = False
                ._OffMenu = False
                ._PaperReport = False
                ._ReportDetail = ""
                ._MedAllergies = ""
                ._MedMedication = ""
                ._MedNotes = ""
                ._ConsOffsite = False
                ._ConsOutdoor = False
                ._ConsPhoto = False
                ._ConsCalpol = False
                ._ConsPlasters = False
                ._ConsSuncream = False
                ._SurgName = ""
                ._SurgDoc = ""
                ._SurgAdd1 = ""
                ._SurgAdd2 = ""
                ._SurgAdd3 = ""
                ._SurgAdd4 = ""
                ._SurgPostcode = ""
                ._SurgTel = ""
                ._Monday = False
                ._Tuesday = False
                ._Wednesday = False
                ._Thursday = False
                ._Friday = False
                ._Saturday = False
                ._Sunday = False
                ._SmsMilk = False
                ._SmsFood = False
                ._SmsToilet = False
                ._SmsSleep = False
                ._Notes = ""
                ._Photo = Nothing
                ._Discount = Nothing
                ._School = Nothing
                ._SchoolPickup = False
                ._SchoolDropoff = False
                ._Feee = False
                ._Feee2 = False
                ._FeeeCurn = ""
                ._FeeeIdaciRank = 0
                ._FeeeIdaciRate = 0
                ._Class = ""
                ._SmsMedical = False
                ._SmsObs = False
                ._TermOnly = False
                ._TermType = ""
                ._InvoiceFreq = ""
                ._AllergyRating = ""
                ._NlCode = ""
                ._NlTracking = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._DietRestrict = ""
                ._DietNotes = ""
                ._HvName = ""
                ._HvAddress = ""
                ._HvTel = ""
                ._HvMobile = ""
                ._HvEmail = ""
                ._CalcLeaveDate = Nothing
                ._CalcFundDate = Nothing
                ._Sen = False
                ._SenNotes = ""
                ._Religion = ""
                ._Ethnicity = ""
                ._Nationality = ""
                ._Language = ""
                ._LeadId = Nothing
                ._CreatedStamp = Nothing
                ._CreatedBy = Nothing
                ._Tags = ""
                ._SurnameForename = ""
                ._Knownas = ""
                ._InvoiceDue = ""
                ._PaymentMethod = ""
                ._PaymentRef = ""
                ._FundingType = ""
                ._FundingRef = ""
                ._FundingEnhancement = 0
                ._MoveMode = ""
                ._MoveDate = Nothing
                ._MoveRatioId = Nothing
                ._SchoolRef1 = ""
                ._SchoolRef2 = ""
                ._VoucherMode = ""
                ._VoucherProportion = 0
                ._VoucherSeq = 0
                ._FoodDetail = False
                ._FundingHrsOpen = 0
                ._FundingHrsCap = 0
                ._FundingHrsWeek = 0
                ._FundingHrsTermTd = 0
                ._FundingHrsYearTd = 0
                ._FundingRate = 0
                ._TodayNotes = ""
                ._PlacementForename = ""
                ._PlacementSurname = ""
                ._PlacementFamilyId = Nothing
                ._PlacementName = ""
                ._PlacementAgency = ""
                ._PlacementReferee = ""
                ._Pin = ""
                ._ExternalId = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Status = ""
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Dob = Nothing
                ._Gender = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._Started = Nothing
                ._DateLeft = Nothing
                ._KeyworkerId = Nothing
                ._KeyworkerName = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._GroupStarted = Nothing
                ._Nappies = False
                ._Milk = False
                ._BabyFood = False
                ._OffMenu = False
                ._PaperReport = False
                ._ReportDetail = ""
                ._MedAllergies = ""
                ._MedMedication = ""
                ._MedNotes = ""
                ._ConsOffsite = False
                ._ConsOutdoor = False
                ._ConsPhoto = False
                ._ConsCalpol = False
                ._ConsPlasters = False
                ._ConsSuncream = False
                ._SurgName = ""
                ._SurgDoc = ""
                ._SurgAdd1 = ""
                ._SurgAdd2 = ""
                ._SurgAdd3 = ""
                ._SurgAdd4 = ""
                ._SurgPostcode = ""
                ._SurgTel = ""
                ._Monday = False
                ._Tuesday = False
                ._Wednesday = False
                ._Thursday = False
                ._Friday = False
                ._Saturday = False
                ._Sunday = False
                ._SmsMilk = False
                ._SmsFood = False
                ._SmsToilet = False
                ._SmsSleep = False
                ._Notes = ""
                ._Photo = Nothing
                ._Discount = Nothing
                ._School = Nothing
                ._SchoolPickup = False
                ._SchoolDropoff = False
                ._Feee = False
                ._Feee2 = False
                ._FeeeCurn = ""
                ._FeeeIdaciRank = 0
                ._FeeeIdaciRate = 0
                ._Class = ""
                ._SmsMedical = False
                ._SmsObs = False
                ._TermOnly = False
                ._TermType = ""
                ._InvoiceFreq = ""
                ._AllergyRating = ""
                ._NlCode = ""
                ._NlTracking = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._DietRestrict = ""
                ._DietNotes = ""
                ._HvName = ""
                ._HvAddress = ""
                ._HvTel = ""
                ._HvMobile = ""
                ._HvEmail = ""
                ._CalcLeaveDate = Nothing
                ._CalcFundDate = Nothing
                ._Sen = False
                ._SenNotes = ""
                ._Religion = ""
                ._Ethnicity = ""
                ._Nationality = ""
                ._Language = ""
                ._LeadId = Nothing
                ._CreatedStamp = Nothing
                ._CreatedBy = Nothing
                ._Tags = ""
                ._SurnameForename = ""
                ._Knownas = ""
                ._InvoiceDue = ""
                ._PaymentMethod = ""
                ._PaymentRef = ""
                ._FundingType = ""
                ._FundingRef = ""
                ._FundingEnhancement = 0
                ._MoveMode = ""
                ._MoveDate = Nothing
                ._MoveRatioId = Nothing
                ._SchoolRef1 = ""
                ._SchoolRef2 = ""
                ._VoucherMode = ""
                ._VoucherProportion = 0
                ._VoucherSeq = 0
                ._FoodDetail = False
                ._FundingHrsOpen = 0
                ._FundingHrsCap = 0
                ._FundingHrsWeek = 0
                ._FundingHrsTermTd = 0
                ._FundingHrsYearTd = 0
                ._FundingRate = 0
                ._TodayNotes = ""
                ._PlacementForename = ""
                ._PlacementSurname = ""
                ._PlacementFamilyId = Nothing
                ._PlacementName = ""
                ._PlacementAgency = ""
                ._PlacementReferee = ""
                ._Pin = ""
                ._ExternalId = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Status")>
        Public Property _Status() As String

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String

        <StoredProcParameter("@Dob")>
        Public Property _Dob() As Date?

        <StoredProcParameter("@Gender")>
        Public Property _Gender() As String

        <StoredProcParameter("@FamilyId")>
        Public Property _FamilyId() As Guid?

        <StoredProcParameter("@FamilyName")>
        Public Property _FamilyName() As String

        <StoredProcParameter("@Started")>
        Public Property _Started() As Date?

        <StoredProcParameter("@DateLeft")>
        Public Property _DateLeft() As Date?

        <StoredProcParameter("@KeyworkerId")>
        Public Property _KeyworkerId() As Guid?

        <StoredProcParameter("@KeyworkerName")>
        Public Property _KeyworkerName() As String

        <StoredProcParameter("@GroupId")>
        Public Property _GroupId() As Guid?

        <StoredProcParameter("@GroupName")>
        Public Property _GroupName() As String

        <StoredProcParameter("@GroupStarted")>
        Public Property _GroupStarted() As Date?

        <StoredProcParameter("@Nappies")>
        Public Property _Nappies() As Boolean

        <StoredProcParameter("@Milk")>
        Public Property _Milk() As Boolean

        <StoredProcParameter("@BabyFood")>
        Public Property _BabyFood() As Boolean

        <StoredProcParameter("@OffMenu")>
        Public Property _OffMenu() As Boolean

        <StoredProcParameter("@PaperReport")>
        Public Property _PaperReport() As Boolean

        <StoredProcParameter("@ReportDetail")>
        Public Property _ReportDetail() As String

        <StoredProcParameter("@MedAllergies")>
        Public Property _MedAllergies() As String

        <StoredProcParameter("@MedMedication")>
        Public Property _MedMedication() As String

        <StoredProcParameter("@MedNotes")>
        Public Property _MedNotes() As String

        <StoredProcParameter("@ConsOffsite")>
        Public Property _ConsOffsite() As Boolean

        <StoredProcParameter("@ConsOutdoor")>
        Public Property _ConsOutdoor() As Boolean

        <StoredProcParameter("@ConsPhoto")>
        Public Property _ConsPhoto() As Boolean

        <StoredProcParameter("@ConsCalpol")>
        Public Property _ConsCalpol() As Boolean

        <StoredProcParameter("@ConsPlasters")>
        Public Property _ConsPlasters() As Boolean

        <StoredProcParameter("@ConsSuncream")>
        Public Property _ConsSuncream() As Boolean

        <StoredProcParameter("@SurgName")>
        Public Property _SurgName() As String

        <StoredProcParameter("@SurgDoc")>
        Public Property _SurgDoc() As String

        <StoredProcParameter("@SurgAdd1")>
        Public Property _SurgAdd1() As String

        <StoredProcParameter("@SurgAdd2")>
        Public Property _SurgAdd2() As String

        <StoredProcParameter("@SurgAdd3")>
        Public Property _SurgAdd3() As String

        <StoredProcParameter("@SurgAdd4")>
        Public Property _SurgAdd4() As String

        <StoredProcParameter("@SurgPostcode")>
        Public Property _SurgPostcode() As String

        <StoredProcParameter("@SurgTel")>
        Public Property _SurgTel() As String

        <StoredProcParameter("@Monday")>
        Public Property _Monday() As Boolean

        <StoredProcParameter("@Tuesday")>
        Public Property _Tuesday() As Boolean

        <StoredProcParameter("@Wednesday")>
        Public Property _Wednesday() As Boolean

        <StoredProcParameter("@Thursday")>
        Public Property _Thursday() As Boolean

        <StoredProcParameter("@Friday")>
        Public Property _Friday() As Boolean

        <StoredProcParameter("@Saturday")>
        Public Property _Saturday() As Boolean

        <StoredProcParameter("@Sunday")>
        Public Property _Sunday() As Boolean

        <StoredProcParameter("@SmsMilk")>
        Public Property _SmsMilk() As Boolean

        <StoredProcParameter("@SmsFood")>
        Public Property _SmsFood() As Boolean

        <StoredProcParameter("@SmsToilet")>
        Public Property _SmsToilet() As Boolean

        <StoredProcParameter("@SmsSleep")>
        Public Property _SmsSleep() As Boolean

        <StoredProcParameter("@Notes")>
        Public Property _Notes() As String

        <StoredProcParameter("@Photo")>
        Public Property _Photo() As Guid?

        <StoredProcParameter("@Discount")>
        Public Property _Discount() As Guid?

        <StoredProcParameter("@School")>
        Public Property _School() As Guid?

        <StoredProcParameter("@SchoolPickup")>
        Public Property _SchoolPickup() As Boolean

        <StoredProcParameter("@SchoolDropoff")>
        Public Property _SchoolDropoff() As Boolean

        <StoredProcParameter("@Feee")>
        Public Property _Feee() As Boolean

        <StoredProcParameter("@Feee2")>
        Public Property _Feee2() As Boolean

        <StoredProcParameter("@FeeeCurn")>
        Public Property _FeeeCurn() As String

        <StoredProcParameter("@FeeeIdaciRank")>
        Public Property _FeeeIdaciRank() As Integer

        <StoredProcParameter("@FeeeIdaciRate")>
        Public Property _FeeeIdaciRate() As Decimal

        <StoredProcParameter("@Class")>
        Public Property _Class() As String

        <StoredProcParameter("@SmsMedical")>
        Public Property _SmsMedical() As Boolean

        <StoredProcParameter("@SmsObs")>
        Public Property _SmsObs() As Boolean

        <StoredProcParameter("@TermOnly")>
        Public Property _TermOnly() As Boolean

        <StoredProcParameter("@TermType")>
        Public Property _TermType() As String

        <StoredProcParameter("@InvoiceFreq")>
        Public Property _InvoiceFreq() As String

        <StoredProcParameter("@AllergyRating")>
        Public Property _AllergyRating() As String

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@DietRestrict")>
        Public Property _DietRestrict() As String

        <StoredProcParameter("@DietNotes")>
        Public Property _DietNotes() As String

        <StoredProcParameter("@HvName")>
        Public Property _HvName() As String

        <StoredProcParameter("@HvAddress")>
        Public Property _HvAddress() As String

        <StoredProcParameter("@HvTel")>
        Public Property _HvTel() As String

        <StoredProcParameter("@HvMobile")>
        Public Property _HvMobile() As String

        <StoredProcParameter("@HvEmail")>
        Public Property _HvEmail() As String

        <StoredProcParameter("@CalcLeaveDate")>
        Public Property _CalcLeaveDate() As Date?

        <StoredProcParameter("@CalcFundDate")>
        Public Property _CalcFundDate() As Date?

        <StoredProcParameter("@Sen")>
        Public Property _Sen() As Boolean

        <StoredProcParameter("@SenNotes")>
        Public Property _SenNotes() As String

        <StoredProcParameter("@Religion")>
        Public Property _Religion() As String

        <StoredProcParameter("@Ethnicity")>
        Public Property _Ethnicity() As String

        <StoredProcParameter("@Nationality")>
        Public Property _Nationality() As String

        <StoredProcParameter("@Language")>
        Public Property _Language() As String

        <StoredProcParameter("@LeadId")>
        Public Property _LeadId() As Guid?

        <StoredProcParameter("@CreatedStamp")>
        Public Property _CreatedStamp() As DateTime?

        <StoredProcParameter("@CreatedBy")>
        Public Property _CreatedBy() As Guid?

        <StoredProcParameter("@Tags")>
        Public Property _Tags() As String

        <StoredProcParameter("@SurnameForename")>
        Public Property _SurnameForename() As String

        <StoredProcParameter("@Knownas")>
        Public Property _Knownas() As String

        <StoredProcParameter("@InvoiceDue")>
        Public Property _InvoiceDue() As String

        <StoredProcParameter("@PaymentMethod")>
        Public Property _PaymentMethod() As String

        <StoredProcParameter("@PaymentRef")>
        Public Property _PaymentRef() As String

        <StoredProcParameter("@FundingType")>
        Public Property _FundingType() As String

        <StoredProcParameter("@FundingRef")>
        Public Property _FundingRef() As String

        <StoredProcParameter("@FundingEnhancement")>
        Public Property _FundingEnhancement() As Decimal

        <StoredProcParameter("@MoveMode")>
        Public Property _MoveMode() As String

        <StoredProcParameter("@MoveDate")>
        Public Property _MoveDate() As Date?

        <StoredProcParameter("@MoveRatioId")>
        Public Property _MoveRatioId() As Guid?

        <StoredProcParameter("@SchoolRef1")>
        Public Property _SchoolRef1() As String

        <StoredProcParameter("@SchoolRef2")>
        Public Property _SchoolRef2() As String

        <StoredProcParameter("@VoucherMode")>
        Public Property _VoucherMode() As String

        <StoredProcParameter("@VoucherProportion")>
        Public Property _VoucherProportion() As Decimal

        <StoredProcParameter("@VoucherSeq")>
        Public Property _VoucherSeq() As Integer

        <StoredProcParameter("@FoodDetail")>
        Public Property _FoodDetail() As Boolean

        <StoredProcParameter("@FundingHrsOpen")>
        Public Property _FundingHrsOpen() As Decimal

        <StoredProcParameter("@FundingHrsCap")>
        Public Property _FundingHrsCap() As Decimal

        <StoredProcParameter("@FundingHrsWeek")>
        Public Property _FundingHrsWeek() As Decimal

        <StoredProcParameter("@FundingHrsTermTd")>
        Public Property _FundingHrsTermTd() As Decimal

        <StoredProcParameter("@FundingHrsYearTd")>
        Public Property _FundingHrsYearTd() As Decimal

        <StoredProcParameter("@FundingRate")>
        Public Property _FundingRate() As Decimal

        <StoredProcParameter("@TodayNotes")>
        Public Property _TodayNotes() As String

        <StoredProcParameter("@PlacementForename")>
        Public Property _PlacementForename() As String

        <StoredProcParameter("@PlacementSurname")>
        Public Property _PlacementSurname() As String

        <StoredProcParameter("@PlacementFamilyId")>
        Public Property _PlacementFamilyId() As Guid?

        <StoredProcParameter("@PlacementName")>
        Public Property _PlacementName() As String

        <StoredProcParameter("@PlacementAgency")>
        Public Property _PlacementAgency() As String

        <StoredProcParameter("@PlacementReferee")>
        Public Property _PlacementReferee() As String

        <StoredProcParameter("@Pin")>
        Public Property _Pin() As String

        <StoredProcParameter("@ExternalId")>
        Public Property _ExternalId() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Child
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Child
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Child)
            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildTable"))
            Return _ChildList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Child)
            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildTable"))
            Return _ChildList
        End Function

        Public Shared Sub SaveAll(ByVal ChildList As List(Of Child))
            For Each _Child As Child In ChildList
                SaveRecord(_Child)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildList As List(Of Child))
            For Each _Child As Child In ChildList
                SaveRecord(ConnectionString, _Child)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Child As Child) As Guid
            Dim _Current As Child = RetreiveByID(Child._ID.Value)
            Child.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Child, _Current, "upsertChild")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Child As Child) As Guid
            Dim _Current As Child = RetreiveByID(ConnectionString, Child._ID.Value)
            Child.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Child, _Current, "upsertChild")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Child = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Child = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteChildbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Child = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChild")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Child = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertChild")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Child

            Dim _C As Child = Nothing

            If DR IsNot Nothing Then
                _C = New Child()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Status = DR("status").ToString.Trim
                    _C._Forename = DR("forename").ToString.Trim
                    _C._Surname = DR("surname").ToString.Trim
                    _C._Fullname = DR("fullname").ToString.Trim
                    _C._Dob = GetDate(DR("dob"))
                    _C._Gender = DR("gender").ToString.Trim
                    _C._FamilyId = GetGUID(DR("family_id"))
                    _C._FamilyName = DR("family_name").ToString.Trim
                    _C._Started = GetDate(DR("started"))
                    _C._DateLeft = GetDate(DR("date_left"))
                    _C._KeyworkerId = GetGUID(DR("keyworker_id"))
                    _C._KeyworkerName = DR("keyworker_name").ToString.Trim
                    _C._GroupId = GetGUID(DR("group_id"))
                    _C._GroupName = DR("group_name").ToString.Trim
                    _C._GroupStarted = GetDate(DR("group_started"))
                    _C._Nappies = GetBoolean(DR("nappies"))
                    _C._Milk = GetBoolean(DR("milk"))
                    _C._BabyFood = GetBoolean(DR("baby_food"))
                    _C._OffMenu = GetBoolean(DR("off_menu"))
                    _C._PaperReport = GetBoolean(DR("paper_report"))
                    _C._ReportDetail = DR("report_detail").ToString.Trim
                    _C._MedAllergies = DR("med_allergies").ToString.Trim
                    _C._MedMedication = DR("med_medication").ToString.Trim
                    _C._MedNotes = DR("med_notes").ToString.Trim
                    _C._ConsOffsite = GetBoolean(DR("cons_offsite"))
                    _C._ConsOutdoor = GetBoolean(DR("cons_outdoor"))
                    _C._ConsPhoto = GetBoolean(DR("cons_photo"))
                    _C._ConsCalpol = GetBoolean(DR("cons_calpol"))
                    _C._ConsPlasters = GetBoolean(DR("cons_plasters"))
                    _C._ConsSuncream = GetBoolean(DR("cons_suncream"))
                    _C._SurgName = DR("surg_name").ToString.Trim
                    _C._SurgDoc = DR("surg_doc").ToString.Trim
                    _C._SurgAdd1 = DR("surg_add1").ToString.Trim
                    _C._SurgAdd2 = DR("surg_add2").ToString.Trim
                    _C._SurgAdd3 = DR("surg_add3").ToString.Trim
                    _C._SurgAdd4 = DR("surg_add4").ToString.Trim
                    _C._SurgPostcode = DR("surg_postcode").ToString.Trim
                    _C._SurgTel = DR("surg_tel").ToString.Trim
                    _C._Monday = GetBoolean(DR("monday"))
                    _C._Tuesday = GetBoolean(DR("tuesday"))
                    _C._Wednesday = GetBoolean(DR("wednesday"))
                    _C._Thursday = GetBoolean(DR("thursday"))
                    _C._Friday = GetBoolean(DR("friday"))
                    _C._Saturday = GetBoolean(DR("saturday"))
                    _C._Sunday = GetBoolean(DR("sunday"))
                    _C._SmsMilk = GetBoolean(DR("sms_milk"))
                    _C._SmsFood = GetBoolean(DR("sms_food"))
                    _C._SmsToilet = GetBoolean(DR("sms_toilet"))
                    _C._SmsSleep = GetBoolean(DR("sms_sleep"))
                    _C._Notes = DR("notes").ToString.Trim
                    _C._Photo = GetGUID(DR("photo"))
                    _C._Discount = GetGUID(DR("discount"))
                    _C._School = GetGUID(DR("school"))
                    _C._SchoolPickup = GetBoolean(DR("school_pickup"))
                    _C._SchoolDropoff = GetBoolean(DR("school_dropoff"))
                    _C._Feee = GetBoolean(DR("feee"))
                    _C._Feee2 = GetBoolean(DR("feee_2"))
                    _C._FeeeCurn = DR("feee_curn").ToString.Trim
                    _C._FeeeIdaciRank = GetInteger(DR("feee_idaci_rank"))
                    _C._FeeeIdaciRate = GetDecimal(DR("feee_idaci_rate"))
                    _C._Class = DR("class").ToString.Trim
                    _C._SmsMedical = GetBoolean(DR("sms_medical"))
                    _C._SmsObs = GetBoolean(DR("sms_obs"))
                    _C._TermOnly = GetBoolean(DR("term_only"))
                    _C._TermType = DR("term_type").ToString.Trim
                    _C._InvoiceFreq = DR("invoice_freq").ToString.Trim
                    _C._AllergyRating = DR("allergy_rating").ToString.Trim
                    _C._NlCode = DR("nl_code").ToString.Trim
                    _C._NlTracking = DR("nl_tracking").ToString.Trim
                    _C._SiteId = GetGUID(DR("site_id"))
                    _C._SiteName = DR("site_name").ToString.Trim
                    _C._DietRestrict = DR("diet_restrict").ToString.Trim
                    _C._DietNotes = DR("diet_notes").ToString.Trim
                    _C._HvName = DR("hv_name").ToString.Trim
                    _C._HvAddress = DR("hv_address").ToString.Trim
                    _C._HvTel = DR("hv_tel").ToString.Trim
                    _C._HvMobile = DR("hv_mobile").ToString.Trim
                    _C._HvEmail = DR("hv_email").ToString.Trim
                    _C._CalcLeaveDate = GetDate(DR("calc_leave_date"))
                    _C._CalcFundDate = GetDate(DR("calc_fund_date"))
                    _C._Sen = GetBoolean(DR("sen"))
                    _C._SenNotes = DR("sen_notes").ToString.Trim
                    _C._Religion = DR("religion").ToString.Trim
                    _C._Ethnicity = DR("ethnicity").ToString.Trim
                    _C._Nationality = DR("nationality").ToString.Trim
                    _C._Language = DR("language").ToString.Trim
                    _C._LeadId = GetGUID(DR("lead_id"))
                    _C._CreatedStamp = GetDateTime(DR("created_stamp"))
                    _C._CreatedBy = GetGUID(DR("created_by"))
                    _C._Tags = DR("tags").ToString.Trim
                    _C._SurnameForename = DR("surname_forename").ToString.Trim
                    _C._Knownas = DR("knownas").ToString.Trim
                    _C._InvoiceDue = DR("invoice_due").ToString.Trim
                    _C._PaymentMethod = DR("payment_method").ToString.Trim
                    _C._PaymentRef = DR("payment_ref").ToString.Trim
                    _C._FundingType = DR("funding_type").ToString.Trim
                    _C._FundingRef = DR("funding_ref").ToString.Trim
                    _C._FundingEnhancement = GetDecimal(DR("funding_enhancement"))
                    _C._MoveMode = DR("move_mode").ToString.Trim
                    _C._MoveDate = GetDate(DR("move_date"))
                    _C._MoveRatioId = GetGUID(DR("move_ratio_id"))
                    _C._SchoolRef1 = DR("school_ref_1").ToString.Trim
                    _C._SchoolRef2 = DR("school_ref_2").ToString.Trim
                    _C._VoucherMode = DR("voucher_mode").ToString.Trim
                    _C._VoucherProportion = GetDecimal(DR("voucher_proportion"))
                    _C._VoucherSeq = GetInteger(DR("voucher_seq"))
                    _C._FoodDetail = GetBoolean(DR("food_detail"))
                    _C._FundingHrsOpen = GetDecimal(DR("funding_hrs_open"))
                    _C._FundingHrsCap = GetDecimal(DR("funding_hrs_cap"))
                    _C._FundingHrsWeek = GetDecimal(DR("funding_hrs_week"))
                    _C._FundingHrsTermTd = GetDecimal(DR("funding_hrs_term_td"))
                    _C._FundingHrsYearTd = GetDecimal(DR("funding_hrs_year_td"))
                    _C._FundingRate = GetDecimal(DR("funding_rate"))
                    _C._TodayNotes = DR("today_notes").ToString.Trim
                    _C._PlacementForename = DR("placement_forename").ToString.Trim
                    _C._PlacementSurname = DR("placement_surname").ToString.Trim
                    _C._PlacementFamilyId = GetGUID(DR("placement_family_id"))
                    _C._PlacementName = DR("placement_name").ToString.Trim
                    _C._PlacementAgency = DR("placement_agency").ToString.Trim
                    _C._PlacementReferee = DR("placement_referee").ToString.Trim
                    _C._Pin = DR("pin").ToString.Trim
                    _C._ExternalId = DR("external_id").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Child)

            Dim _ChildList As New List(Of Child)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildList

        End Function


#End Region

    End Class

End Namespace
