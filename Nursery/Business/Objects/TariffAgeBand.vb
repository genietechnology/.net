﻿'*****************************************************
'Generated 24/09/2018 09:52:41 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TariffAgeBand
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Description = ""
                ._MinAge = 0
                ._MaxAge = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Description = ""
                ._MinAge = 0
                ._MaxAge = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Description")>
        Public Property _Description() As String

        <StoredProcParameter("@MinAge")>
        Public Property _MinAge() As Integer

        <StoredProcParameter("@MaxAge")>
        Public Property _MaxAge() As Integer


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TariffAgeBand
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffAgeBandbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TariffAgeBand
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffAgeBandbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of TariffAgeBand)
            Dim _TariffAgeBandList As List(Of TariffAgeBand)
            _TariffAgeBandList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffAgeBandTable"))
            Return _TariffAgeBandList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TariffAgeBand)
            Dim _TariffAgeBandList As List(Of TariffAgeBand)
            _TariffAgeBandList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffAgeBandTable"))
            Return _TariffAgeBandList
        End Function

        Public Shared Sub SaveAll(ByVal TariffAgeBandList As List(Of TariffAgeBand))
            For Each _TariffAgeBand As TariffAgeBand In TariffAgeBandList
                SaveRecord(_TariffAgeBand)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffAgeBandList As List(Of TariffAgeBand))
            For Each _TariffAgeBand As TariffAgeBand In TariffAgeBandList
                SaveRecord(ConnectionString, _TariffAgeBand)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal TariffAgeBand As TariffAgeBand) As Guid
            Dim _Current As TariffAgeBand = RetreiveByID(TariffAgeBand._ID.Value)
            TariffAgeBand.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffAgeBand, _Current, "upsertTariffAgeBand")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TariffAgeBand As TariffAgeBand) As Guid
            Dim _Current As TariffAgeBand = RetreiveByID(ConnectionString, TariffAgeBand._ID.Value)
            TariffAgeBand.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffAgeBand, _Current, "upsertTariffAgeBand")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As TariffAgeBand = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffAgeBandbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As TariffAgeBand = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffAgeBandbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As TariffAgeBand = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffAgeBand")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As TariffAgeBand = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffAgeBand")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TariffAgeBand

            Dim _T As TariffAgeBand = Nothing

            If DR IsNot Nothing Then
                _T = New TariffAgeBand()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Description = DR("description").ToString.Trim
                    _T._MinAge = GetInteger(DR("min_age"))
                    _T._MaxAge = GetInteger(DR("max_age"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TariffAgeBand)

            Dim _TariffAgeBandList As New List(Of TariffAgeBand)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffAgeBandList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffAgeBandList

        End Function


#End Region

    End Class

End Namespace
