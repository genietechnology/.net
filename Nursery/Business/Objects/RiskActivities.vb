﻿'*****************************************************
'Generated 27/06/2017 13:16:42 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RiskActivities
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RiskId As Guid?
        Dim m_Activity As String
        Dim m_HazardCodes As String
        Dim m_HazardDesc As String
        Dim m_RiskCodes As String
        Dim m_RiskDesc As String
        Dim m_PeopleCodes As String
        Dim m_PeopleDesc As String
        Dim m_Controls As String
        Dim m_UcLikely As Integer
        Dim m_UcSeverity As Integer
        Dim m_UcRating As Integer
        Dim m_cLikely As Integer
        Dim m_cSeverity As Integer
        Dim m_cRating As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RiskId = Nothing
                ._Activity = ""
                ._HazardCodes = ""
                ._HazardDesc = ""
                ._RiskCodes = ""
                ._RiskDesc = ""
                ._PeopleCodes = ""
                ._PeopleDesc = ""
                ._Controls = ""
                ._UcLikely = 0
                ._UcSeverity = 0
                ._UcRating = 0
                ._cLikely = 0
                ._cSeverity = 0
                ._cRating = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RiskId = Nothing
                ._Activity = ""
                ._HazardCodes = ""
                ._HazardDesc = ""
                ._RiskCodes = ""
                ._RiskDesc = ""
                ._PeopleCodes = ""
                ._PeopleDesc = ""
                ._Controls = ""
                ._UcLikely = 0
                ._UcSeverity = 0
                ._UcRating = 0
                ._cLikely = 0
                ._cSeverity = 0
                ._cRating = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RiskId")>
        Public Property _RiskId() As Guid?
            Get
                Return m_RiskId
            End Get
            Set(ByVal value As Guid?)
                m_RiskId = value
            End Set
        End Property

        <StoredProcParameter("@Activity")>
        Public Property _Activity() As String
            Get
                Return m_Activity
            End Get
            Set(ByVal value As String)
                m_Activity = value
            End Set
        End Property

        <StoredProcParameter("@HazardCodes")>
        Public Property _HazardCodes() As String
            Get
                Return m_HazardCodes
            End Get
            Set(ByVal value As String)
                m_HazardCodes = value
            End Set
        End Property

        <StoredProcParameter("@HazardDesc")>
        Public Property _HazardDesc() As String
            Get
                Return m_HazardDesc
            End Get
            Set(ByVal value As String)
                m_HazardDesc = value
            End Set
        End Property

        <StoredProcParameter("@RiskCodes")>
        Public Property _RiskCodes() As String
            Get
                Return m_RiskCodes
            End Get
            Set(ByVal value As String)
                m_RiskCodes = value
            End Set
        End Property

        <StoredProcParameter("@RiskDesc")>
        Public Property _RiskDesc() As String
            Get
                Return m_RiskDesc
            End Get
            Set(ByVal value As String)
                m_RiskDesc = value
            End Set
        End Property

        <StoredProcParameter("@PeopleCodes")>
        Public Property _PeopleCodes() As String
            Get
                Return m_PeopleCodes
            End Get
            Set(ByVal value As String)
                m_PeopleCodes = value
            End Set
        End Property

        <StoredProcParameter("@PeopleDesc")>
        Public Property _PeopleDesc() As String
            Get
                Return m_PeopleDesc
            End Get
            Set(ByVal value As String)
                m_PeopleDesc = value
            End Set
        End Property

        <StoredProcParameter("@Controls")>
        Public Property _Controls() As String
            Get
                Return m_Controls
            End Get
            Set(ByVal value As String)
                m_Controls = value
            End Set
        End Property

        <StoredProcParameter("@UcLikely")>
        Public Property _UcLikely() As Integer
            Get
                Return m_UcLikely
            End Get
            Set(ByVal value As Integer)
                m_UcLikely = value
            End Set
        End Property

        <StoredProcParameter("@UcSeverity")>
        Public Property _UcSeverity() As Integer
            Get
                Return m_UcSeverity
            End Get
            Set(ByVal value As Integer)
                m_UcSeverity = value
            End Set
        End Property

        <StoredProcParameter("@UcRating")>
        Public Property _UcRating() As Integer
            Get
                Return m_UcRating
            End Get
            Set(ByVal value As Integer)
                m_UcRating = value
            End Set
        End Property

        <StoredProcParameter("@cLikely")>
        Public Property _cLikely() As Integer
            Get
                Return m_cLikely
            End Get
            Set(ByVal value As Integer)
                m_cLikely = value
            End Set
        End Property

        <StoredProcParameter("@cSeverity")>
        Public Property _cSeverity() As Integer
            Get
                Return m_cSeverity
            End Get
            Set(ByVal value As Integer)
                m_cSeverity = value
            End Set
        End Property

        <StoredProcParameter("@cRating")>
        Public Property _cRating() As Integer
            Get
                Return m_cRating
            End Get
            Set(ByVal value As Integer)
                m_cRating = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RiskActivities

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRiskActivitiesbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RiskActivities

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRiskActivitiesbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RiskActivities)

            Dim _RiskActivitiesList As List(Of RiskActivities)
            _RiskActivitiesList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRiskActivitiesTable"))
            Return _RiskActivitiesList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RiskActivities)

            Dim _RiskActivitiesList As List(Of RiskActivities)
            _RiskActivitiesList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRiskActivitiesTable"))
            Return _RiskActivitiesList

        End Function

        Public Shared Sub SaveAll(ByVal RiskActivitiesList As List(Of RiskActivities))

            For Each _RiskActivities As RiskActivities In RiskActivitiesList
                SaveRecord(_RiskActivities)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RiskActivitiesList As List(Of RiskActivities))

            For Each _RiskActivities As RiskActivities In RiskActivitiesList
                SaveRecord(ConnectionString, _RiskActivities)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RiskActivities As RiskActivities) As Guid
            RiskActivities.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RiskActivities, "upsertRiskActivities")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RiskActivities As RiskActivities) As Guid
            RiskActivities.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RiskActivities, "upsertRiskActivities")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRiskActivitiesbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRiskActivitiesbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRiskActivities")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRiskActivities")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RiskActivities

            Dim _R As RiskActivities = Nothing

            If DR IsNot Nothing Then
                _R = New RiskActivities()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._RiskId = GetGUID(DR("risk_id"))
                    _R._Activity = DR("activity").ToString.Trim
                    _R._HazardCodes = DR("hazard_codes").ToString.Trim
                    _R._HazardDesc = DR("hazard_desc").ToString.Trim
                    _R._RiskCodes = DR("risk_codes").ToString.Trim
                    _R._RiskDesc = DR("risk_desc").ToString.Trim
                    _R._PeopleCodes = DR("people_codes").ToString.Trim
                    _R._PeopleDesc = DR("people_desc").ToString.Trim
                    _R._Controls = DR("controls").ToString.Trim
                    _R._UcLikely = GetInteger(DR("uc_likely"))
                    _R._UcSeverity = GetInteger(DR("uc_severity"))
                    _R._UcRating = GetInteger(DR("uc_rating"))
                    _R._cLikely = GetInteger(DR("c_likely"))
                    _R._cSeverity = GetInteger(DR("c_severity"))
                    _R._cRating = GetInteger(DR("c_rating"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RiskActivities)

            Dim _RiskActivitiesList As New List(Of RiskActivities)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RiskActivitiesList.Add(PropertiesFromData(_DR))
            Next

            Return _RiskActivitiesList

        End Function


#End Region

    End Class

End Namespace
