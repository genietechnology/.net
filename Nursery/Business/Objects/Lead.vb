﻿'*****************************************************
'Generated 24/06/2017 12:04:47 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Lead
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DateEntered As Date?
        Dim m_Stage As String
        Dim m_StageStart As Date?
        Dim m_FoundUs As String
        Dim m_Via As String
        Dim m_Rating As String
        Dim m_Closed As Boolean
        Dim m_ClosedDate As Date?
        Dim m_ContactForename As String
        Dim m_ContactSurname As String
        Dim m_ContactFullname As String
        Dim m_ContactRelationship As String
        Dim m_ContactAddress As String
        Dim m_ContactPostcode As String
        Dim m_ContactHome As String
        Dim m_ContactMobile As String
        Dim m_ContactEmail As String
        Dim m_ContactNotes As String
        Dim m_ChildClass As String
        Dim m_ChildForename As String
        Dim m_ChildSurname As String
        Dim m_ChildFullname As String
        Dim m_ChildGender As String
        Dim m_ChildDob As Date?
        Dim m_ChildStarting As Date?
        Dim m_ChildRoom As Guid?
        Dim m_ChildSchool As Guid?
        Dim m_ChildPickups As Boolean
        Dim m_ChildMonday As Boolean
        Dim m_ChildTuesday As Boolean
        Dim m_ChildWednesday As Boolean
        Dim m_ChildThursday As Boolean
        Dim m_ChildFriday As Boolean
        Dim m_ChildSaturday As Boolean
        Dim m_ChildSunday As Boolean
        Dim m_ChildNotes As String
        Dim m_StageId As Guid?
        Dim m_StagePcnt As Byte
        Dim m_OwnerId As Guid?
        Dim m_DateFirstContacted As DateTime?
        Dim m_DateLastContacted As DateTime?
        Dim m_DatePackSent As DateTime?
        Dim m_DateViewing As DateTime?
        Dim m_DateQuoted As DateTime?
        Dim m_DateListed As DateTime?
        Dim m_ViewOutcome As String
        Dim m_ViewComments As String
        Dim m_Coordinates As String
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_CommMethod As String
        Dim m_CommMarketing As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DateEntered = Nothing
                ._Stage = ""
                ._StageStart = Nothing
                ._FoundUs = ""
                ._Via = ""
                ._Rating = ""
                ._Closed = False
                ._ClosedDate = Nothing
                ._ContactForename = ""
                ._ContactSurname = ""
                ._ContactFullname = ""
                ._ContactRelationship = ""
                ._ContactAddress = ""
                ._ContactPostcode = ""
                ._ContactHome = ""
                ._ContactMobile = ""
                ._ContactEmail = ""
                ._ContactNotes = ""
                ._ChildClass = ""
                ._ChildForename = ""
                ._ChildSurname = ""
                ._ChildFullname = ""
                ._ChildGender = ""
                ._ChildDob = Nothing
                ._ChildStarting = Nothing
                ._ChildRoom = Nothing
                ._ChildSchool = Nothing
                ._ChildPickups = False
                ._ChildMonday = False
                ._ChildTuesday = False
                ._ChildWednesday = False
                ._ChildThursday = False
                ._ChildFriday = False
                ._ChildSaturday = False
                ._ChildSunday = False
                ._ChildNotes = ""
                ._StageId = Nothing
                ._StagePcnt = Nothing
                ._OwnerId = Nothing
                ._DateFirstContacted = Nothing
                ._DateLastContacted = Nothing
                ._DatePackSent = Nothing
                ._DateViewing = Nothing
                ._DateQuoted = Nothing
                ._DateListed = Nothing
                ._ViewOutcome = ""
                ._ViewComments = ""
                ._Coordinates = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._CommMethod = ""
                ._CommMarketing = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DateEntered = Nothing
                ._Stage = ""
                ._StageStart = Nothing
                ._FoundUs = ""
                ._Via = ""
                ._Rating = ""
                ._Closed = False
                ._ClosedDate = Nothing
                ._ContactForename = ""
                ._ContactSurname = ""
                ._ContactFullname = ""
                ._ContactRelationship = ""
                ._ContactAddress = ""
                ._ContactPostcode = ""
                ._ContactHome = ""
                ._ContactMobile = ""
                ._ContactEmail = ""
                ._ContactNotes = ""
                ._ChildClass = ""
                ._ChildForename = ""
                ._ChildSurname = ""
                ._ChildFullname = ""
                ._ChildGender = ""
                ._ChildDob = Nothing
                ._ChildStarting = Nothing
                ._ChildRoom = Nothing
                ._ChildSchool = Nothing
                ._ChildPickups = False
                ._ChildMonday = False
                ._ChildTuesday = False
                ._ChildWednesday = False
                ._ChildThursday = False
                ._ChildFriday = False
                ._ChildSaturday = False
                ._ChildSunday = False
                ._ChildNotes = ""
                ._StageId = Nothing
                ._StagePcnt = Nothing
                ._OwnerId = Nothing
                ._DateFirstContacted = Nothing
                ._DateLastContacted = Nothing
                ._DatePackSent = Nothing
                ._DateViewing = Nothing
                ._DateQuoted = Nothing
                ._DateListed = Nothing
                ._ViewOutcome = ""
                ._ViewComments = ""
                ._Coordinates = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._CommMethod = ""
                ._CommMarketing = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DateEntered")>
        Public Property _DateEntered() As Date?
            Get
                Return m_DateEntered
            End Get
            Set(ByVal value As Date?)
                m_DateEntered = value
            End Set
        End Property

        <StoredProcParameter("@Stage")>
        Public Property _Stage() As String
            Get
                Return m_Stage
            End Get
            Set(ByVal value As String)
                m_Stage = value
            End Set
        End Property

        <StoredProcParameter("@StageStart")>
        Public Property _StageStart() As Date?
            Get
                Return m_StageStart
            End Get
            Set(ByVal value As Date?)
                m_StageStart = value
            End Set
        End Property

        <StoredProcParameter("@FoundUs")>
        Public Property _FoundUs() As String
            Get
                Return m_FoundUs
            End Get
            Set(ByVal value As String)
                m_FoundUs = value
            End Set
        End Property

        <StoredProcParameter("@Via")>
        Public Property _Via() As String
            Get
                Return m_Via
            End Get
            Set(ByVal value As String)
                m_Via = value
            End Set
        End Property

        <StoredProcParameter("@Rating")>
        Public Property _Rating() As String
            Get
                Return m_Rating
            End Get
            Set(ByVal value As String)
                m_Rating = value
            End Set
        End Property

        <StoredProcParameter("@Closed")>
        Public Property _Closed() As Boolean
            Get
                Return m_Closed
            End Get
            Set(ByVal value As Boolean)
                m_Closed = value
            End Set
        End Property

        <StoredProcParameter("@ClosedDate")>
        Public Property _ClosedDate() As Date?
            Get
                Return m_ClosedDate
            End Get
            Set(ByVal value As Date?)
                m_ClosedDate = value
            End Set
        End Property

        <StoredProcParameter("@ContactForename")>
        Public Property _ContactForename() As String
            Get
                Return m_ContactForename
            End Get
            Set(ByVal value As String)
                m_ContactForename = value
            End Set
        End Property

        <StoredProcParameter("@ContactSurname")>
        Public Property _ContactSurname() As String
            Get
                Return m_ContactSurname
            End Get
            Set(ByVal value As String)
                m_ContactSurname = value
            End Set
        End Property

        <StoredProcParameter("@ContactFullname")>
        Public Property _ContactFullname() As String
            Get
                Return m_ContactFullname
            End Get
            Set(ByVal value As String)
                m_ContactFullname = value
            End Set
        End Property

        <StoredProcParameter("@ContactRelationship")>
        Public Property _ContactRelationship() As String
            Get
                Return m_ContactRelationship
            End Get
            Set(ByVal value As String)
                m_ContactRelationship = value
            End Set
        End Property

        <StoredProcParameter("@ContactAddress")>
        Public Property _ContactAddress() As String
            Get
                Return m_ContactAddress
            End Get
            Set(ByVal value As String)
                m_ContactAddress = value
            End Set
        End Property

        <StoredProcParameter("@ContactPostcode")>
        Public Property _ContactPostcode() As String
            Get
                Return m_ContactPostcode
            End Get
            Set(ByVal value As String)
                m_ContactPostcode = value
            End Set
        End Property

        <StoredProcParameter("@ContactHome")>
        Public Property _ContactHome() As String
            Get
                Return m_ContactHome
            End Get
            Set(ByVal value As String)
                m_ContactHome = value
            End Set
        End Property

        <StoredProcParameter("@ContactMobile")>
        Public Property _ContactMobile() As String
            Get
                Return m_ContactMobile
            End Get
            Set(ByVal value As String)
                m_ContactMobile = value
            End Set
        End Property

        <StoredProcParameter("@ContactEmail")>
        Public Property _ContactEmail() As String
            Get
                Return m_ContactEmail
            End Get
            Set(ByVal value As String)
                m_ContactEmail = value
            End Set
        End Property

        <StoredProcParameter("@ContactNotes")>
        Public Property _ContactNotes() As String
            Get
                Return m_ContactNotes
            End Get
            Set(ByVal value As String)
                m_ContactNotes = value
            End Set
        End Property

        <StoredProcParameter("@ChildClass")>
        Public Property _ChildClass() As String
            Get
                Return m_ChildClass
            End Get
            Set(ByVal value As String)
                m_ChildClass = value
            End Set
        End Property

        <StoredProcParameter("@ChildForename")>
        Public Property _ChildForename() As String
            Get
                Return m_ChildForename
            End Get
            Set(ByVal value As String)
                m_ChildForename = value
            End Set
        End Property

        <StoredProcParameter("@ChildSurname")>
        Public Property _ChildSurname() As String
            Get
                Return m_ChildSurname
            End Get
            Set(ByVal value As String)
                m_ChildSurname = value
            End Set
        End Property

        <StoredProcParameter("@ChildFullname")>
        Public Property _ChildFullname() As String
            Get
                Return m_ChildFullname
            End Get
            Set(ByVal value As String)
                m_ChildFullname = value
            End Set
        End Property

        <StoredProcParameter("@ChildGender")>
        Public Property _ChildGender() As String
            Get
                Return m_ChildGender
            End Get
            Set(ByVal value As String)
                m_ChildGender = value
            End Set
        End Property

        <StoredProcParameter("@ChildDob")>
        Public Property _ChildDob() As Date?
            Get
                Return m_ChildDob
            End Get
            Set(ByVal value As Date?)
                m_ChildDob = value
            End Set
        End Property

        <StoredProcParameter("@ChildStarting")>
        Public Property _ChildStarting() As Date?
            Get
                Return m_ChildStarting
            End Get
            Set(ByVal value As Date?)
                m_ChildStarting = value
            End Set
        End Property

        <StoredProcParameter("@ChildRoom")>
        Public Property _ChildRoom() As Guid?
            Get
                Return m_ChildRoom
            End Get
            Set(ByVal value As Guid?)
                m_ChildRoom = value
            End Set
        End Property

        <StoredProcParameter("@ChildSchool")>
        Public Property _ChildSchool() As Guid?
            Get
                Return m_ChildSchool
            End Get
            Set(ByVal value As Guid?)
                m_ChildSchool = value
            End Set
        End Property

        <StoredProcParameter("@ChildPickups")>
        Public Property _ChildPickups() As Boolean
            Get
                Return m_ChildPickups
            End Get
            Set(ByVal value As Boolean)
                m_ChildPickups = value
            End Set
        End Property

        <StoredProcParameter("@ChildMonday")>
        Public Property _ChildMonday() As Boolean
            Get
                Return m_ChildMonday
            End Get
            Set(ByVal value As Boolean)
                m_ChildMonday = value
            End Set
        End Property

        <StoredProcParameter("@ChildTuesday")>
        Public Property _ChildTuesday() As Boolean
            Get
                Return m_ChildTuesday
            End Get
            Set(ByVal value As Boolean)
                m_ChildTuesday = value
            End Set
        End Property

        <StoredProcParameter("@ChildWednesday")>
        Public Property _ChildWednesday() As Boolean
            Get
                Return m_ChildWednesday
            End Get
            Set(ByVal value As Boolean)
                m_ChildWednesday = value
            End Set
        End Property

        <StoredProcParameter("@ChildThursday")>
        Public Property _ChildThursday() As Boolean
            Get
                Return m_ChildThursday
            End Get
            Set(ByVal value As Boolean)
                m_ChildThursday = value
            End Set
        End Property

        <StoredProcParameter("@ChildFriday")>
        Public Property _ChildFriday() As Boolean
            Get
                Return m_ChildFriday
            End Get
            Set(ByVal value As Boolean)
                m_ChildFriday = value
            End Set
        End Property

        <StoredProcParameter("@ChildSaturday")>
        Public Property _ChildSaturday() As Boolean
            Get
                Return m_ChildSaturday
            End Get
            Set(ByVal value As Boolean)
                m_ChildSaturday = value
            End Set
        End Property

        <StoredProcParameter("@ChildSunday")>
        Public Property _ChildSunday() As Boolean
            Get
                Return m_ChildSunday
            End Get
            Set(ByVal value As Boolean)
                m_ChildSunday = value
            End Set
        End Property

        <StoredProcParameter("@ChildNotes")>
        Public Property _ChildNotes() As String
            Get
                Return m_ChildNotes
            End Get
            Set(ByVal value As String)
                m_ChildNotes = value
            End Set
        End Property

        <StoredProcParameter("@StageId")>
        Public Property _StageId() As Guid?
            Get
                Return m_StageId
            End Get
            Set(ByVal value As Guid?)
                m_StageId = value
            End Set
        End Property

        <StoredProcParameter("@StagePcnt")>
        Public Property _StagePcnt() As Byte
            Get
                Return m_StagePcnt
            End Get
            Set(ByVal value As Byte)
                m_StagePcnt = value
            End Set
        End Property

        <StoredProcParameter("@OwnerId")>
        Public Property _OwnerId() As Guid?
            Get
                Return m_OwnerId
            End Get
            Set(ByVal value As Guid?)
                m_OwnerId = value
            End Set
        End Property

        <StoredProcParameter("@DateFirstContacted")>
        Public Property _DateFirstContacted() As DateTime?
            Get
                Return m_DateFirstContacted
            End Get
            Set(ByVal value As DateTime?)
                m_DateFirstContacted = value
            End Set
        End Property

        <StoredProcParameter("@DateLastContacted")>
        Public Property _DateLastContacted() As DateTime?
            Get
                Return m_DateLastContacted
            End Get
            Set(ByVal value As DateTime?)
                m_DateLastContacted = value
            End Set
        End Property

        <StoredProcParameter("@DatePackSent")>
        Public Property _DatePackSent() As DateTime?
            Get
                Return m_DatePackSent
            End Get
            Set(ByVal value As DateTime?)
                m_DatePackSent = value
            End Set
        End Property

        <StoredProcParameter("@DateViewing")>
        Public Property _DateViewing() As DateTime?
            Get
                Return m_DateViewing
            End Get
            Set(ByVal value As DateTime?)
                m_DateViewing = value
            End Set
        End Property

        <StoredProcParameter("@DateQuoted")>
        Public Property _DateQuoted() As DateTime?
            Get
                Return m_DateQuoted
            End Get
            Set(ByVal value As DateTime?)
                m_DateQuoted = value
            End Set
        End Property

        <StoredProcParameter("@DateListed")>
        Public Property _DateListed() As DateTime?
            Get
                Return m_DateListed
            End Get
            Set(ByVal value As DateTime?)
                m_DateListed = value
            End Set
        End Property

        <StoredProcParameter("@ViewOutcome")>
        Public Property _ViewOutcome() As String
            Get
                Return m_ViewOutcome
            End Get
            Set(ByVal value As String)
                m_ViewOutcome = value
            End Set
        End Property

        <StoredProcParameter("@ViewComments")>
        Public Property _ViewComments() As String
            Get
                Return m_ViewComments
            End Get
            Set(ByVal value As String)
                m_ViewComments = value
            End Set
        End Property

        <StoredProcParameter("@Coordinates")>
        Public Property _Coordinates() As String
            Get
                Return m_Coordinates
            End Get
            Set(ByVal value As String)
                m_Coordinates = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@CommMethod")>
        Public Property _CommMethod() As String
            Get
                Return m_CommMethod
            End Get
            Set(ByVal value As String)
                m_CommMethod = value
            End Set
        End Property

        <StoredProcParameter("@CommMarketing")>
        Public Property _CommMarketing() As Boolean
            Get
                Return m_CommMarketing
            End Get
            Set(ByVal value As Boolean)
                m_CommMarketing = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Lead

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getLeadbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Lead

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getLeadbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Lead)

            Dim _LeadList As List(Of Lead)
            _LeadList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getLeadTable"))
            Return _LeadList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Lead)

            Dim _LeadList As List(Of Lead)
            _LeadList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getLeadTable"))
            Return _LeadList

        End Function

        Public Shared Sub SaveAll(ByVal LeadList As List(Of Lead))

            For Each _Lead As Lead In LeadList
                SaveRecord(_Lead)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal LeadList As List(Of Lead))

            For Each _Lead As Lead In LeadList
                SaveRecord(ConnectionString, _Lead)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Lead As Lead) As Guid
            Lead.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Lead, "upsertLead")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Lead As Lead) As Guid
            Lead.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Lead, "upsertLead")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteLeadbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteLeadbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertLead")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertLead")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Lead

            Dim _L As Lead = Nothing

            If DR IsNot Nothing Then
                _L = New Lead()
                With DR
                    _L.IsNew = False
                    _L.IsDeleted = False
                    _L._ID = GetGUID(DR("ID"))
                    _L._DateEntered = GetDate(DR("date_entered"))
                    _L._Stage = DR("stage").ToString.Trim
                    _L._StageStart = GetDate(DR("stage_start"))
                    _L._FoundUs = DR("found_us").ToString.Trim
                    _L._Via = DR("via").ToString.Trim
                    _L._Rating = DR("rating").ToString.Trim
                    _L._Closed = GetBoolean(DR("closed"))
                    _L._ClosedDate = GetDate(DR("closed_date"))
                    _L._ContactForename = DR("contact_forename").ToString.Trim
                    _L._ContactSurname = DR("contact_surname").ToString.Trim
                    _L._ContactFullname = DR("contact_fullname").ToString.Trim
                    _L._ContactRelationship = DR("contact_relationship").ToString.Trim
                    _L._ContactAddress = DR("contact_address").ToString.Trim
                    _L._ContactPostcode = DR("contact_postcode").ToString.Trim
                    _L._ContactHome = DR("contact_home").ToString.Trim
                    _L._ContactMobile = DR("contact_mobile").ToString.Trim
                    _L._ContactEmail = DR("contact_email").ToString.Trim
                    _L._ContactNotes = DR("contact_notes").ToString.Trim
                    _L._ChildClass = DR("child_class").ToString.Trim
                    _L._ChildForename = DR("child_forename").ToString.Trim
                    _L._ChildSurname = DR("child_surname").ToString.Trim
                    _L._ChildFullname = DR("child_fullname").ToString.Trim
                    _L._ChildGender = DR("child_gender").ToString.Trim
                    _L._ChildDob = GetDate(DR("child_dob"))
                    _L._ChildStarting = GetDate(DR("child_starting"))
                    _L._ChildRoom = GetGUID(DR("child_room"))
                    _L._ChildSchool = GetGUID(DR("child_school"))
                    _L._ChildPickups = GetBoolean(DR("child_pickups"))
                    _L._ChildMonday = GetBoolean(DR("child_monday"))
                    _L._ChildTuesday = GetBoolean(DR("child_tuesday"))
                    _L._ChildWednesday = GetBoolean(DR("child_wednesday"))
                    _L._ChildThursday = GetBoolean(DR("child_thursday"))
                    _L._ChildFriday = GetBoolean(DR("child_friday"))
                    _L._ChildSaturday = GetBoolean(DR("child_saturday"))
                    _L._ChildSunday = GetBoolean(DR("child_sunday"))
                    _L._ChildNotes = DR("child_notes").ToString.Trim
                    _L._StageId = GetGUID(DR("stage_id"))
                    _L._StagePcnt = GetByte(DR("stage_pcnt"))
                    _L._OwnerId = GetGUID(DR("owner_id"))
                    _L._DateFirstContacted = GetDateTime(DR("date_first_contacted"))
                    _L._DateLastContacted = GetDateTime(DR("date_last_contacted"))
                    _L._DatePackSent = GetDateTime(DR("date_pack_sent"))
                    _L._DateViewing = GetDateTime(DR("date_viewing"))
                    _L._DateQuoted = GetDateTime(DR("date_quoted"))
                    _L._DateListed = GetDateTime(DR("date_listed"))
                    _L._ViewOutcome = DR("view_outcome").ToString.Trim
                    _L._ViewComments = DR("view_comments").ToString.Trim
                    _L._Coordinates = DR("coordinates").ToString.Trim
                    _L._SiteId = GetGUID(DR("site_id"))
                    _L._SiteName = DR("site_name").ToString.Trim
                    _L._CommMethod = DR("comm_method").ToString.Trim
                    _L._CommMarketing = GetBoolean(DR("comm_marketing"))

                End With
            End If

            Return _L

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Lead)

            Dim _LeadList As New List(Of Lead)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _LeadList.Add(PropertiesFromData(_DR))
            Next

            Return _LeadList

        End Function


#End Region

    End Class

End Namespace
