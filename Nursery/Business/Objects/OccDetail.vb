﻿'*****************************************************
'Generated 07/04/2016 16:06:10 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class OccDetail
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_OccYear As Integer
        Dim m_OccMonth As Byte
        Dim m_OccWeek As Byte
        Dim m_OccDate As Date?
        Dim m_RoomId As Guid?
        Dim m_Am As Integer
        Dim m_Pm As Integer
        Dim m_Headcount As Integer
        Dim m_Occ As Decimal
        Dim m_Ft As Decimal
        Dim m_Pt As Decimal
        Dim m_Fte As Decimal
        Dim m_Hours As Decimal
        Dim m_Funded As Decimal
        Dim m_Revenue As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._OccYear = 0
                ._OccMonth = Nothing
                ._OccWeek = Nothing
                ._OccDate = Nothing
                ._RoomId = Nothing
                ._Am = 0
                ._Pm = 0
                ._Headcount = 0
                ._Occ = 0
                ._Ft = 0
                ._Pt = 0
                ._Fte = 0
                ._Hours = 0
                ._Funded = 0
                ._Revenue = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._OccYear = 0
                ._OccMonth = Nothing
                ._OccWeek = Nothing
                ._OccDate = Nothing
                ._RoomId = Nothing
                ._Am = 0
                ._Pm = 0
                ._Headcount = 0
                ._Occ = 0
                ._Ft = 0
                ._Pt = 0
                ._Fte = 0
                ._Hours = 0
                ._Funded = 0
                ._Revenue = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@OccYear")> _
        Public Property _OccYear() As Integer
            Get
                Return m_OccYear
            End Get
            Set(ByVal value As Integer)
                m_OccYear = value
            End Set
        End Property

        <StoredProcParameter("@OccMonth")> _
        Public Property _OccMonth() As Byte
            Get
                Return m_OccMonth
            End Get
            Set(ByVal value As Byte)
                m_OccMonth = value
            End Set
        End Property

        <StoredProcParameter("@OccWeek")> _
        Public Property _OccWeek() As Byte
            Get
                Return m_OccWeek
            End Get
            Set(ByVal value As Byte)
                m_OccWeek = value
            End Set
        End Property

        <StoredProcParameter("@OccDate")> _
        Public Property _OccDate() As Date?
            Get
                Return m_OccDate
            End Get
            Set(ByVal value As Date?)
                m_OccDate = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@Am")> _
        Public Property _Am() As Integer
            Get
                Return m_Am
            End Get
            Set(ByVal value As Integer)
                m_Am = value
            End Set
        End Property

        <StoredProcParameter("@Pm")> _
        Public Property _Pm() As Integer
            Get
                Return m_Pm
            End Get
            Set(ByVal value As Integer)
                m_Pm = value
            End Set
        End Property

        <StoredProcParameter("@Headcount")> _
        Public Property _Headcount() As Integer
            Get
                Return m_Headcount
            End Get
            Set(ByVal value As Integer)
                m_Headcount = value
            End Set
        End Property

        <StoredProcParameter("@Occ")> _
        Public Property _Occ() As Decimal
            Get
                Return m_Occ
            End Get
            Set(ByVal value As Decimal)
                m_Occ = value
            End Set
        End Property

        <StoredProcParameter("@Ft")> _
        Public Property _Ft() As Decimal
            Get
                Return m_Ft
            End Get
            Set(ByVal value As Decimal)
                m_Ft = value
            End Set
        End Property

        <StoredProcParameter("@Pt")> _
        Public Property _Pt() As Decimal
            Get
                Return m_Pt
            End Get
            Set(ByVal value As Decimal)
                m_Pt = value
            End Set
        End Property

        <StoredProcParameter("@Fte")> _
        Public Property _Fte() As Decimal
            Get
                Return m_Fte
            End Get
            Set(ByVal value As Decimal)
                m_Fte = value
            End Set
        End Property

        <StoredProcParameter("@Hours")> _
        Public Property _Hours() As Decimal
            Get
                Return m_Hours
            End Get
            Set(ByVal value As Decimal)
                m_Hours = value
            End Set
        End Property

        <StoredProcParameter("@Funded")> _
        Public Property _Funded() As Decimal
            Get
                Return m_Funded
            End Get
            Set(ByVal value As Decimal)
                m_Funded = value
            End Set
        End Property

        <StoredProcParameter("@Revenue")> _
        Public Property _Revenue() As Decimal
            Get
                Return m_Revenue
            End Get
            Set(ByVal value As Decimal)
                m_Revenue = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As OccDetail

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getOccDetailbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of OccDetail)

            Dim _OccDetailList As List(Of OccDetail)
            _OccDetailList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getOccDetailTable"))
            Return _OccDetailList

        End Function

        Public Shared Sub SaveAll(ByVal OccDetailList As List(Of OccDetail))

            For Each _OccDetail As OccDetail In OccDetailList
                SaveRecord(_OccDetail)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal OccDetail As OccDetail) As Guid
            OccDetail.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, OccDetail, "upsertOccDetail")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteOccDetailbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertOccDetail")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As OccDetail

            Dim _O As OccDetail = Nothing

            If DR IsNot Nothing Then
                _O = New OccDetail()
                With DR
                    _O.IsNew = False
                    _O.IsDeleted = False
                    _O._ID = GetGUID(DR("ID"))
                    _O._OccYear = GetInteger(DR("occ_year"))
                    _O._OccMonth = GetByte(DR("occ_month"))
                    _O._OccWeek = GetByte(DR("occ_week"))
                    _O._OccDate = GetDate(DR("occ_date"))
                    _O._RoomId = GetGUID(DR("room_id"))
                    _O._Am = GetInteger(DR("am"))
                    _O._Pm = GetInteger(DR("pm"))
                    _O._Headcount = GetInteger(DR("headcount"))
                    _O._Occ = GetDecimal(DR("occ"))
                    _O._Ft = GetDecimal(DR("ft"))
                    _O._Pt = GetDecimal(DR("pt"))
                    _O._Fte = GetDecimal(DR("fte"))
                    _O._Hours = GetDecimal(DR("hours"))
                    _O._Funded = GetDecimal(DR("funded"))
                    _O._Revenue = GetDecimal(DR("revenue"))

                End With
            End If

            Return _O

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of OccDetail)

            Dim _OccDetailList As New List(Of OccDetail)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _OccDetailList.Add(PropertiesFromData(_DR))
            Next

            Return _OccDetailList

        End Function


#End Region

    End Class

End Namespace
