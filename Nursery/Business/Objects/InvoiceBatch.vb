﻿'*****************************************************
'Generated 24/09/2018 09:51:37 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class InvoiceBatch
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BatchNo = 0
                ._BatchStatus = ""
                ._BatchDate = Nothing
                ._BatchPeriod = ""
                ._DateFirst = Nothing
                ._DateLast = Nothing
                ._InvFirst = 0
                ._InvLast = 0
                ._BatchClass = ""
                ._BatchFreq = ""
                ._UserId = Nothing
                ._Stamp = Nothing
                ._BatchGroup = ""
                ._BatchLayout = ""
                ._Xcheck = 0
                ._XcheckFrom = Nothing
                ._XcheckTo = Nothing
                ._XcheckThresh = 0
                ._XcheckPerHour = 0
                ._XcheckAdditional = False
                ._SiteId = Nothing
                ._SiteName = ""
                ._AdditionalSessions = Nothing
                ._DuplicateMode = Nothing
                ._TariffFilter = ""
                ._FundingType = ""
                ._TagFilter = ""
                ._Comments = ""
                ._PaymentMethod = ""
                ._AdditionalFrom = Nothing
                ._AdditionalTo = Nothing
                ._GenerationMode = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BatchNo = 0
                ._BatchStatus = ""
                ._BatchDate = Nothing
                ._BatchPeriod = ""
                ._DateFirst = Nothing
                ._DateLast = Nothing
                ._InvFirst = 0
                ._InvLast = 0
                ._BatchClass = ""
                ._BatchFreq = ""
                ._UserId = Nothing
                ._Stamp = Nothing
                ._BatchGroup = ""
                ._BatchLayout = ""
                ._Xcheck = 0
                ._XcheckFrom = Nothing
                ._XcheckTo = Nothing
                ._XcheckThresh = 0
                ._XcheckPerHour = 0
                ._XcheckAdditional = False
                ._SiteId = Nothing
                ._SiteName = ""
                ._AdditionalSessions = Nothing
                ._DuplicateMode = Nothing
                ._TariffFilter = ""
                ._FundingType = ""
                ._TagFilter = ""
                ._Comments = ""
                ._PaymentMethod = ""
                ._AdditionalFrom = Nothing
                ._AdditionalTo = Nothing
                ._GenerationMode = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@BatchNo")>
        Public Property _BatchNo() As Long

        <StoredProcParameter("@BatchStatus")>
        Public Property _BatchStatus() As String

        <StoredProcParameter("@BatchDate")>
        Public Property _BatchDate() As Date?

        <StoredProcParameter("@BatchPeriod")>
        Public Property _BatchPeriod() As String

        <StoredProcParameter("@DateFirst")>
        Public Property _DateFirst() As Date?

        <StoredProcParameter("@DateLast")>
        Public Property _DateLast() As Date?

        <StoredProcParameter("@InvFirst")>
        Public Property _InvFirst() As Long

        <StoredProcParameter("@InvLast")>
        Public Property _InvLast() As Long

        <StoredProcParameter("@BatchClass")>
        Public Property _BatchClass() As String

        <StoredProcParameter("@BatchFreq")>
        Public Property _BatchFreq() As String

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?

        <StoredProcParameter("@BatchGroup")>
        Public Property _BatchGroup() As String

        <StoredProcParameter("@BatchLayout")>
        Public Property _BatchLayout() As String

        <StoredProcParameter("@Xcheck")>
        Public Property _Xcheck() As Integer

        <StoredProcParameter("@XcheckFrom")>
        Public Property _XcheckFrom() As Date?

        <StoredProcParameter("@XcheckTo")>
        Public Property _XcheckTo() As Date?

        <StoredProcParameter("@XcheckThresh")>
        Public Property _XcheckThresh() As Decimal

        <StoredProcParameter("@XcheckPerHour")>
        Public Property _XcheckPerHour() As Decimal

        <StoredProcParameter("@XcheckAdditional")>
        Public Property _XcheckAdditional() As Boolean

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@AdditionalSessions")>
        Public Property _AdditionalSessions() As Byte

        <StoredProcParameter("@DuplicateMode")>
        Public Property _DuplicateMode() As Byte

        <StoredProcParameter("@TariffFilter")>
        Public Property _TariffFilter() As String

        <StoredProcParameter("@FundingType")>
        Public Property _FundingType() As String

        <StoredProcParameter("@TagFilter")>
        Public Property _TagFilter() As String

        <StoredProcParameter("@Comments")>
        Public Property _Comments() As String

        <StoredProcParameter("@PaymentMethod")>
        Public Property _PaymentMethod() As String

        <StoredProcParameter("@AdditionalFrom")>
        Public Property _AdditionalFrom() As Date?

        <StoredProcParameter("@AdditionalTo")>
        Public Property _AdditionalTo() As Date?

        <StoredProcParameter("@GenerationMode")>
        Public Property _GenerationMode() As Byte


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As InvoiceBatch
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInvoiceBatchbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As InvoiceBatch
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getInvoiceBatchbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of InvoiceBatch)
            Dim _InvoiceBatchList As List(Of InvoiceBatch)
            _InvoiceBatchList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInvoiceBatchTable"))
            Return _InvoiceBatchList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of InvoiceBatch)
            Dim _InvoiceBatchList As List(Of InvoiceBatch)
            _InvoiceBatchList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getInvoiceBatchTable"))
            Return _InvoiceBatchList
        End Function

        Public Shared Sub SaveAll(ByVal InvoiceBatchList As List(Of InvoiceBatch))
            For Each _InvoiceBatch As InvoiceBatch In InvoiceBatchList
                SaveRecord(_InvoiceBatch)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal InvoiceBatchList As List(Of InvoiceBatch))
            For Each _InvoiceBatch As InvoiceBatch In InvoiceBatchList
                SaveRecord(ConnectionString, _InvoiceBatch)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal InvoiceBatch As InvoiceBatch) As Guid
            Dim _Current As InvoiceBatch = RetreiveByID(InvoiceBatch._ID.Value)
            InvoiceBatch.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, InvoiceBatch, _Current, "upsertInvoiceBatch")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal InvoiceBatch As InvoiceBatch) As Guid
            Dim _Current As InvoiceBatch = RetreiveByID(ConnectionString, InvoiceBatch._ID.Value)
            InvoiceBatch.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, InvoiceBatch, _Current, "upsertInvoiceBatch")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As InvoiceBatch = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoiceBatchbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As InvoiceBatch = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoiceBatchbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As InvoiceBatch = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoiceBatch")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As InvoiceBatch = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoiceBatch")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As InvoiceBatch

            Dim _I As InvoiceBatch = Nothing

            If DR IsNot Nothing Then
                _I = New InvoiceBatch()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._BatchNo = GetLong(DR("batch_no"))
                    _I._BatchStatus = DR("batch_status").ToString.Trim
                    _I._BatchDate = GetDate(DR("batch_date"))
                    _I._BatchPeriod = DR("batch_period").ToString.Trim
                    _I._DateFirst = GetDate(DR("date_first"))
                    _I._DateLast = GetDate(DR("date_last"))
                    _I._InvFirst = GetLong(DR("inv_first"))
                    _I._InvLast = GetLong(DR("inv_last"))
                    _I._BatchClass = DR("batch_class").ToString.Trim
                    _I._BatchFreq = DR("batch_freq").ToString.Trim
                    _I._UserId = GetGUID(DR("user_id"))
                    _I._Stamp = GetDateTime(DR("stamp"))
                    _I._BatchGroup = DR("batch_group").ToString.Trim
                    _I._BatchLayout = DR("batch_layout").ToString.Trim
                    _I._Xcheck = GetInteger(DR("xcheck"))
                    _I._XcheckFrom = GetDate(DR("xcheck_from"))
                    _I._XcheckTo = GetDate(DR("xcheck_to"))
                    _I._XcheckThresh = GetDecimal(DR("xcheck_thresh"))
                    _I._XcheckPerHour = GetDecimal(DR("xcheck_per_hour"))
                    _I._XcheckAdditional = GetBoolean(DR("xcheck_additional"))
                    _I._SiteId = GetGUID(DR("site_id"))
                    _I._SiteName = DR("site_name").ToString.Trim
                    _I._AdditionalSessions = GetByte(DR("additional_sessions"))
                    _I._DuplicateMode = GetByte(DR("duplicate_mode"))
                    _I._TariffFilter = DR("tariff_filter").ToString.Trim
                    _I._FundingType = DR("funding_type").ToString.Trim
                    _I._TagFilter = DR("tag_filter").ToString.Trim
                    _I._Comments = DR("comments").ToString.Trim
                    _I._PaymentMethod = DR("payment_method").ToString.Trim
                    _I._AdditionalFrom = GetDate(DR("additional_from"))
                    _I._AdditionalTo = GetDate(DR("additional_to"))
                    _I._GenerationMode = GetByte(DR("generation_mode"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of InvoiceBatch)

            Dim _InvoiceBatchList As New List(Of InvoiceBatch)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InvoiceBatchList.Add(PropertiesFromData(_DR))
            Next

            Return _InvoiceBatchList

        End Function


#End Region

    End Class

End Namespace
