﻿'*****************************************************
'Generated 29/05/2016 08:34:56 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AgePoint
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_AgeMin As Integer
        Dim m_AgeMax As Integer
        Dim m_Points As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._AgeMin = 0
                ._AgeMax = 0
                ._Points = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._AgeMin = 0
                ._AgeMax = 0
                ._Points = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@AgeMin")> _
        Public Property _AgeMin() As Integer
            Get
                Return m_AgeMin
            End Get
            Set(ByVal value As Integer)
                m_AgeMin = value
            End Set
        End Property

        <StoredProcParameter("@AgeMax")> _
        Public Property _AgeMax() As Integer
            Get
                Return m_AgeMax
            End Get
            Set(ByVal value As Integer)
                m_AgeMax = value
            End Set
        End Property

        <StoredProcParameter("@Points")> _
        Public Property _Points() As Integer
            Get
                Return m_Points
            End Get
            Set(ByVal value As Integer)
                m_Points = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AgePoint

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAgePointbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AgePoint)

            Dim _AgePointList As List(Of AgePoint)
            _AgePointList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAgePointTable"))
            Return _AgePointList

        End Function

        Public Shared Sub SaveAll(ByVal AgePointList As List(Of AgePoint))

            For Each _AgePoint As AgePoint In AgePointList
                SaveRecord(_AgePoint)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AgePoint As AgePoint) As Guid
            AgePoint.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AgePoint, "upsertAgePoint")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAgePointbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAgePoint")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AgePoint

            Dim _A As AgePoint = Nothing

            If DR IsNot Nothing Then
                _A = New AgePoint()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._AgeMin = GetInteger(DR("age_min"))
                    _A._AgeMax = GetInteger(DR("age_max"))
                    _A._Points = GetInteger(DR("points"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AgePoint)

            Dim _AgePointList As New List(Of AgePoint)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AgePointList.Add(PropertiesFromData(_DR))
            Next

            Return _AgePointList

        End Function


#End Region

    End Class

End Namespace
