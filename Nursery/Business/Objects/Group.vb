﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Group
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_AgeMin As Integer
        Dim m_AgeMax As Integer
        Dim m_Sequence As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._AgeMin = 0
                ._AgeMax = 0
                ._Sequence = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._AgeMin = 0
                ._AgeMax = 0
                ._Sequence = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@AgeMin")> _
        Public Property _AgeMin() As Integer
            Get
                Return m_AgeMin
            End Get
            Set(ByVal value As Integer)
                m_AgeMin = value
            End Set
        End Property

        <StoredProcParameter("@AgeMax")> _
        Public Property _AgeMax() As Integer
            Get
                Return m_AgeMax
            End Get
            Set(ByVal value As Integer)
                m_AgeMax = value
            End Set
        End Property

        <StoredProcParameter("@Sequence")> _
        Public Property _Sequence() As Integer
            Get
                Return m_Sequence
            End Get
            Set(ByVal value As Integer)
                m_Sequence = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Group

            Dim _Group As Group
            _Group = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getGroupbyID", ID))
            Return _Group

        End Function

        Public Shared Function RetreiveAll() As List(Of Group)

            Dim _GroupList As List(Of Group)
            _GroupList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getGroupTable"))
            Return _GroupList

        End Function

        Public Shared Sub SaveAll(ByVal GroupList As List(Of Group))

            For Each _Group As Group In GroupList
                SaveRecord(_Group)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Group As Group) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Group, "upsertGroup")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteGroupbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertGroup")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Group

            Dim _G As New Group()

            If DR IsNot Nothing Then
                With DR
                    _G.IsNew = False
                    _G.IsDeleted = False
                    _G._ID = GetGUID(DR("ID"))
                    _G._Name = DR("name").ToString.Trim
                    _G._AgeMin = GetInteger(DR("age_min"))
                    _G._AgeMax = GetInteger(DR("age_max"))
                    _G._Sequence = GetInteger(DR("sequence"))

                End With
            End If

            Return _G

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Group)

            Dim _GroupList As New List(Of Group)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _GroupList.Add(PropertiesFromData(_DR))
            Next

            Return _GroupList

        End Function


#End Region

    End Class

End Namespace
