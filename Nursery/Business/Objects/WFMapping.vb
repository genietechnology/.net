﻿'*****************************************************
'Generated 24/09/2018 09:54:28 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class WFMapping
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FormId = Nothing
                ._ParentField = ""
                ._FormField = ""
                ._FormLabel = ""
                ._ValueObject = ""
                ._ValueProperty = ""
                ._ValueOverride = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FormId = Nothing
                ._ParentField = ""
                ._FormField = ""
                ._FormLabel = ""
                ._ValueObject = ""
                ._ValueProperty = ""
                ._ValueOverride = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@FormId")>
        Public Property _FormId() As Guid?

        <StoredProcParameter("@ParentField")>
        Public Property _ParentField() As String

        <StoredProcParameter("@FormField")>
        Public Property _FormField() As String

        <StoredProcParameter("@FormLabel")>
        Public Property _FormLabel() As String

        <StoredProcParameter("@ValueObject")>
        Public Property _ValueObject() As String

        <StoredProcParameter("@ValueProperty")>
        Public Property _ValueProperty() As String

        <StoredProcParameter("@ValueOverride")>
        Public Property _ValueOverride() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As WFMapping
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getWFMappingbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As WFMapping
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getWFMappingbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of WFMapping)
            Dim _WFMappingList As List(Of WFMapping)
            _WFMappingList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getWFMappingTable"))
            Return _WFMappingList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of WFMapping)
            Dim _WFMappingList As List(Of WFMapping)
            _WFMappingList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getWFMappingTable"))
            Return _WFMappingList
        End Function

        Public Shared Sub SaveAll(ByVal WFMappingList As List(Of WFMapping))
            For Each _WFMapping As WFMapping In WFMappingList
                SaveRecord(_WFMapping)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal WFMappingList As List(Of WFMapping))
            For Each _WFMapping As WFMapping In WFMappingList
                SaveRecord(ConnectionString, _WFMapping)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal WFMapping As WFMapping) As Guid
            Dim _Current As WFMapping = RetreiveByID(WFMapping._ID.Value)
            WFMapping.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, WFMapping, _Current, "upsertWFMapping")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal WFMapping As WFMapping) As Guid
            Dim _Current As WFMapping = RetreiveByID(ConnectionString, WFMapping._ID.Value)
            WFMapping.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, WFMapping, _Current, "upsertWFMapping")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As WFMapping = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteWFMappingbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As WFMapping = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteWFMappingbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As WFMapping = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertWFMapping")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As WFMapping = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertWFMapping")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As WFMapping

            Dim _W As WFMapping = Nothing

            If DR IsNot Nothing Then
                _W = New WFMapping()
                With DR
                    _W.IsNew = False
                    _W.IsDeleted = False
                    _W._ID = GetGUID(DR("ID"))
                    _W._FormId = GetGUID(DR("form_id"))
                    _W._ParentField = DR("parent_field").ToString.Trim
                    _W._FormField = DR("form_field").ToString.Trim
                    _W._FormLabel = DR("form_label").ToString.Trim
                    _W._ValueObject = DR("value_object").ToString.Trim
                    _W._ValueProperty = DR("value_property").ToString.Trim
                    _W._ValueOverride = DR("value_override").ToString.Trim

                End With
            End If

            Return _W

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of WFMapping)

            Dim _WFMappingList As New List(Of WFMapping)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _WFMappingList.Add(PropertiesFromData(_DR))
            Next

            Return _WFMappingList

        End Function


#End Region

    End Class

End Namespace
