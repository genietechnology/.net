﻿Public Class AllergyMatrix

    Public Sub New(ByVal Desc As String, ByVal Ticked As Boolean)
        Description = Desc
        Selected = Ticked
    End Sub

    Public Property Description As String
    Public Property Selected As Boolean

End Class
