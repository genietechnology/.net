﻿'*****************************************************
'Generated 05/10/2015 05:45:00 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffQual
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_StaffId As Guid?
        Dim m_QualDate As Date?
        Dim m_QualName As String
        Dim m_QualExpires As Date?
        Dim m_QualScore As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._StaffId = Nothing
                ._QualDate = Nothing
                ._QualName = ""
                ._QualExpires = Nothing
                ._QualScore = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._StaffId = Nothing
                ._QualDate = Nothing
                ._QualName = ""
                ._QualExpires = Nothing
                ._QualScore = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@QualDate")> _
        Public Property _QualDate() As Date?
            Get
                Return m_QualDate
            End Get
            Set(ByVal value As Date?)
                m_QualDate = value
            End Set
        End Property

        <StoredProcParameter("@QualName")> _
        Public Property _QualName() As String
            Get
                Return m_QualName
            End Get
            Set(ByVal value As String)
                m_QualName = value
            End Set
        End Property

        <StoredProcParameter("@QualExpires")> _
        Public Property _QualExpires() As Date?
            Get
                Return m_QualExpires
            End Get
            Set(ByVal value As Date?)
                m_QualExpires = value
            End Set
        End Property

        <StoredProcParameter("@QualScore")> _
        Public Property _QualScore() As Decimal
            Get
                Return m_QualScore
            End Get
            Set(ByVal value As Decimal)
                m_QualScore = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffQual

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffQualbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffQual)

            Dim _StaffQualList As List(Of StaffQual)
            _StaffQualList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffQualTable"))
            Return _StaffQualList

        End Function

        Public Shared Sub SaveAll(ByVal StaffQualList As List(Of StaffQual))

            For Each _StaffQual As StaffQual In StaffQualList
                SaveRecord(_StaffQual)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffQual As StaffQual) As Guid
            StaffQual.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffQual, "upsertStaffQual")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffQualbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffQual")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffQual

            Dim _S As StaffQual = Nothing

            If DR IsNot Nothing Then
                _S = New StaffQual()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._StaffId = GetGUID(DR("staff_id"))
                    _S._QualDate = GetDate(DR("qual_date"))
                    _S._QualName = DR("qual_name").ToString.Trim
                    _S._QualExpires = GetDate(DR("qual_expires"))
                    _S._QualScore = GetDecimal(DR("qual_score"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffQual)

            Dim _StaffQualList As New List(Of StaffQual)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffQualList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffQualList

        End Function


#End Region

    End Class

End Namespace
