﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Request
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Description As String
        Dim m_ReportText As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Description = ""
                ._ReportText = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Description = ""
                ._ReportText = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@ReportText")> _
        Public Property _ReportText() As String
            Get
                Return m_ReportText
            End Get
            Set(ByVal value As String)
                m_ReportText = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Request

            Dim _Request As Request
            _Request = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRequestbyID", ID))
            Return _Request

        End Function

        Public Shared Function RetreiveAll() As List(Of Request)

            Dim _RequestList As List(Of Request)
            _RequestList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRequestTable"))
            Return _RequestList

        End Function

        Public Shared Sub SaveAll(ByVal RequestList As List(Of Request))

            For Each _Request As Request In RequestList
                SaveRecord(_Request)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Request As Request) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Request, "upsertRequest")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRequestbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRequest")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Request

            Dim _R As New Request()

            If DR IsNot Nothing Then
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._Description = DR("description").ToString.Trim
                    _R._ReportText = DR("report_text").ToString.Trim

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Request)

            Dim _RequestList As New List(Of Request)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RequestList.Add(PropertiesFromData(_DR))
            Next

            Return _RequestList

        End Function


#End Region

    End Class

End Namespace
