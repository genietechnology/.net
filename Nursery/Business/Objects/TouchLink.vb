﻿'*****************************************************
'Generated 28/05/2015 20:34:15 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TouchLink
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Area As String
        Dim m_Type As String
        Dim m_Name As String
        Dim m_Path As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Area = ""
                ._Type = ""
                ._Name = ""
                ._Path = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Area = ""
                ._Type = ""
                ._Name = ""
                ._Path = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Area")> _
        Public Property _Area() As String
            Get
                Return m_Area
            End Get
            Set(ByVal value As String)
                m_Area = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Path")> _
        Public Property _Path() As String
            Get
                Return m_Path
            End Get
            Set(ByVal value As String)
                m_Path = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TouchLink

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTouchLinkbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of TouchLink)

            Dim _TouchLinkList As List(Of TouchLink)
            _TouchLinkList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTouchLinkTable"))
            Return _TouchLinkList

        End Function

        Public Shared Sub SaveAll(ByVal TouchLinkList As List(Of TouchLink))

            For Each _TouchLink As TouchLink In TouchLinkList
                SaveRecord(_TouchLink)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal TouchLink As TouchLink) As Guid
            DAL.SaveRecord(Session.ConnectionManager, TouchLink, "upsertTouchLink")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTouchLinkbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertTouchLink")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TouchLink

            Dim _T As TouchLink = Nothing

            If DR IsNot Nothing Then
                _T = New TouchLink()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Area = DR("area").ToString.Trim
                    _T._Type = DR("type").ToString.Trim
                    _T._Name = DR("name").ToString.Trim
                    _T._Path = DR("path").ToString.Trim

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TouchLink)

            Dim _TouchLinkList As New List(Of TouchLink)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TouchLinkList.Add(PropertiesFromData(_DR))
            Next

            Return _TouchLinkList

        End Function


#End Region

    End Class

End Namespace
