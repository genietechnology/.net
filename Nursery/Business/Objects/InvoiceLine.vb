﻿'*****************************************************
'Generated 24/09/2018 09:49:08 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class InvoiceLine
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._InvoiceId = Nothing
                ._LineNo = 0
                ._ActionDate = Nothing
                ._Description = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Value = 0
                ._Section = ""
                ._Hours = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing
                ._XcheckOrigHours = 0
                ._Discountable = 0
                ._PhNonfunded = 0
                ._PhFunded = 0
                ._ValueFunded = 0
                ._SummaryColumn = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._InvoiceId = Nothing
                ._LineNo = 0
                ._ActionDate = Nothing
                ._Description = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Value = 0
                ._Section = ""
                ._Hours = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing
                ._XcheckOrigHours = 0
                ._Discountable = 0
                ._PhNonfunded = 0
                ._PhFunded = 0
                ._ValueFunded = 0
                ._SummaryColumn = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@InvoiceId")>
        Public Property _InvoiceId() As Guid?

        <StoredProcParameter("@LineNo")>
        Public Property _LineNo() As Long

        <StoredProcParameter("@ActionDate")>
        Public Property _ActionDate() As Date?

        <StoredProcParameter("@Description")>
        Public Property _Description() As String

        <StoredProcParameter("@Ref1")>
        Public Property _Ref1() As String

        <StoredProcParameter("@Ref2")>
        Public Property _Ref2() As String

        <StoredProcParameter("@Ref3")>
        Public Property _Ref3() As String

        <StoredProcParameter("@Value")>
        Public Property _Value() As Decimal

        <StoredProcParameter("@Section")>
        Public Property _Section() As String

        <StoredProcParameter("@Hours")>
        Public Property _Hours() As Decimal

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String

        <StoredProcParameter("@ChangedBy")>
        Public Property _ChangedBy() As Guid?

        <StoredProcParameter("@ChangedStamp")>
        Public Property _ChangedStamp() As DateTime?

        <StoredProcParameter("@XcheckOrigHours")>
        Public Property _XcheckOrigHours() As Decimal

        <StoredProcParameter("@Discountable")>
        Public Property _Discountable() As Decimal

        <StoredProcParameter("@PhNonfunded")>
        Public Property _PhNonfunded() As Decimal

        <StoredProcParameter("@PhFunded")>
        Public Property _PhFunded() As Decimal

        <StoredProcParameter("@ValueFunded")>
        Public Property _ValueFunded() As Decimal

        <StoredProcParameter("@SummaryColumn")>
        Public Property _SummaryColumn() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As InvoiceLine
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInvoiceLinebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As InvoiceLine
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getInvoiceLinebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of InvoiceLine)
            Dim _InvoiceLineList As List(Of InvoiceLine)
            _InvoiceLineList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInvoiceLineTable"))
            Return _InvoiceLineList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of InvoiceLine)
            Dim _InvoiceLineList As List(Of InvoiceLine)
            _InvoiceLineList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getInvoiceLineTable"))
            Return _InvoiceLineList
        End Function

        Public Shared Sub SaveAll(ByVal InvoiceLineList As List(Of InvoiceLine))
            For Each _InvoiceLine As InvoiceLine In InvoiceLineList
                SaveRecord(_InvoiceLine)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal InvoiceLineList As List(Of InvoiceLine))
            For Each _InvoiceLine As InvoiceLine In InvoiceLineList
                SaveRecord(ConnectionString, _InvoiceLine)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal InvoiceLine As InvoiceLine) As Guid
            Dim _Current As InvoiceLine = RetreiveByID(InvoiceLine._ID.Value)
            InvoiceLine.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, InvoiceLine, _Current, "upsertInvoiceLine")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal InvoiceLine As InvoiceLine) As Guid
            Dim _Current As InvoiceLine = RetreiveByID(ConnectionString, InvoiceLine._ID.Value)
            InvoiceLine.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, InvoiceLine, _Current, "upsertInvoiceLine")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As InvoiceLine = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoiceLinebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As InvoiceLine = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoiceLinebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As InvoiceLine = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoiceLine")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As InvoiceLine = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoiceLine")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As InvoiceLine

            Dim _I As InvoiceLine = Nothing

            If DR IsNot Nothing Then
                _I = New InvoiceLine()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._InvoiceId = GetGUID(DR("invoice_id"))
                    _I._LineNo = GetLong(DR("line_no"))
                    _I._ActionDate = GetDate(DR("action_date"))
                    _I._Description = DR("description").ToString.Trim
                    _I._Ref1 = DR("ref_1").ToString.Trim
                    _I._Ref2 = DR("ref_2").ToString.Trim
                    _I._Ref3 = DR("ref_3").ToString.Trim
                    _I._Value = GetDecimal(DR("value"))
                    _I._Section = DR("section").ToString.Trim
                    _I._Hours = GetDecimal(DR("hours"))
                    _I._NlCode = DR("nl_code").ToString.Trim
                    _I._NlTracking = DR("nl_tracking").ToString.Trim
                    _I._ChangedBy = GetGUID(DR("changed_by"))
                    _I._ChangedStamp = GetDateTime(DR("changed_stamp"))
                    _I._XcheckOrigHours = GetDecimal(DR("xcheck_orig_hours"))
                    _I._Discountable = GetDecimal(DR("discountable"))
                    _I._PhNonfunded = GetDecimal(DR("ph_nonfunded"))
                    _I._PhFunded = GetDecimal(DR("ph_funded"))
                    _I._ValueFunded = GetDecimal(DR("value_funded"))
                    _I._SummaryColumn = DR("summary_column").ToString.Trim

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of InvoiceLine)

            Dim _InvoiceLineList As New List(Of InvoiceLine)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InvoiceLineList.Add(PropertiesFromData(_DR))
            Next

            Return _InvoiceLineList

        End Function


#End Region

    End Class

End Namespace
