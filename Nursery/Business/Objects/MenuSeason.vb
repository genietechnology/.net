﻿'*****************************************************
'Generated 13/03/2016 18:40:49 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class MenuSeason
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_DateFrom As Date?
        Dim m_Weeks As Byte

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._DateFrom = Nothing
                ._Weeks = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._DateFrom = Nothing
                ._Weeks = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@DateFrom")> _
        Public Property _DateFrom() As Date?
            Get
                Return m_DateFrom
            End Get
            Set(ByVal value As Date?)
                m_DateFrom = value
            End Set
        End Property

        <StoredProcParameter("@Weeks")> _
        Public Property _Weeks() As Byte
            Get
                Return m_Weeks
            End Get
            Set(ByVal value As Byte)
                m_Weeks = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As MenuSeason

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMenuSeasonbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of MenuSeason)

            Dim _MenuSeasonList As List(Of MenuSeason)
            _MenuSeasonList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMenuSeasonTable"))
            Return _MenuSeasonList

        End Function

        Public Shared Sub SaveAll(ByVal MenuSeasonList As List(Of MenuSeason))

            For Each _MenuSeason As MenuSeason In MenuSeasonList
                SaveRecord(_MenuSeason)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal MenuSeason As MenuSeason) As Guid
            MenuSeason.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, MenuSeason, "upsertMenuSeason")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMenuSeasonbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMenuSeason")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As MenuSeason

            Dim _M As MenuSeason = Nothing

            If DR IsNot Nothing Then
                _M = New MenuSeason()
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._Name = DR("name").ToString.Trim
                    _M._DateFrom = GetDate(DR("date_from"))
                    _M._Weeks = GetByte(DR("weeks"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of MenuSeason)

            Dim _MenuSeasonList As New List(Of MenuSeason)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MenuSeasonList.Add(PropertiesFromData(_DR))
            Next

            Return _MenuSeasonList

        End Function


#End Region

    End Class

End Namespace
