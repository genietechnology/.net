﻿'*****************************************************
'Generated 10/04/2017 16:33:40 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildHoliday
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_FromDate As Date?
        Dim m_ToDate As Date?
        Dim m_Discount As Decimal
        Dim m_FromHalf As Boolean
        Dim m_ToHalf As Boolean
        Dim m_Notes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._FromDate = Nothing
                ._ToDate = Nothing
                ._Discount = 0
                ._FromHalf = False
                ._ToHalf = False
                ._Notes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._FromDate = Nothing
                ._ToDate = Nothing
                ._Discount = 0
                ._FromHalf = False
                ._ToHalf = False
                ._Notes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@FromDate")> _
        Public Property _FromDate() As Date?
            Get
                Return m_FromDate
            End Get
            Set(ByVal value As Date?)
                m_FromDate = value
            End Set
        End Property

        <StoredProcParameter("@ToDate")> _
        Public Property _ToDate() As Date?
            Get
                Return m_ToDate
            End Get
            Set(ByVal value As Date?)
                m_ToDate = value
            End Set
        End Property

        <StoredProcParameter("@Discount")> _
        Public Property _Discount() As Decimal
            Get
                Return m_Discount
            End Get
            Set(ByVal value As Decimal)
                m_Discount = value
            End Set
        End Property

        <StoredProcParameter("@FromHalf")> _
        Public Property _FromHalf() As Boolean
            Get
                Return m_FromHalf
            End Get
            Set(ByVal value As Boolean)
                m_FromHalf = value
            End Set
        End Property

        <StoredProcParameter("@ToHalf")> _
        Public Property _ToHalf() As Boolean
            Get
                Return m_ToHalf
            End Get
            Set(ByVal value As Boolean)
                m_ToHalf = value
            End Set
        End Property

        <StoredProcParameter("@Notes")> _
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildHoliday

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildHolidaybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildHoliday

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildHolidaybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildHoliday)

            Dim _ChildHolidayList As List(Of ChildHoliday)
            _ChildHolidayList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildHolidayTable"))
            Return _ChildHolidayList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildHoliday)

            Dim _ChildHolidayList As List(Of ChildHoliday)
            _ChildHolidayList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildHolidayTable"))
            Return _ChildHolidayList

        End Function

        Public Shared Sub SaveAll(ByVal ChildHolidayList As List(Of ChildHoliday))

            For Each _ChildHoliday As ChildHoliday In ChildHolidayList
                SaveRecord(_ChildHoliday)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildHolidayList As List(Of ChildHoliday))

            For Each _ChildHoliday As ChildHoliday In ChildHolidayList
                SaveRecord(ConnectionString, _ChildHoliday)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildHoliday As ChildHoliday) As Guid
            ChildHoliday.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildHoliday, "upsertChildHoliday")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildHoliday As ChildHoliday) As Guid
            ChildHoliday.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildHoliday, "upsertChildHoliday")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildHolidaybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteChildHolidaybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildHoliday")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildHoliday")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildHoliday

            Dim _C As ChildHoliday = Nothing

            If DR IsNot Nothing Then
                _C = New ChildHoliday()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._FromDate = GetDate(DR("from_date"))
                    _C._ToDate = GetDate(DR("to_date"))
                    _C._Discount = GetDecimal(DR("discount"))
                    _C._FromHalf = GetBoolean(DR("from_half"))
                    _C._ToHalf = GetBoolean(DR("to_half"))
                    _C._Notes = DR("notes").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildHoliday)

            Dim _ChildHolidayList As New List(Of ChildHoliday)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildHolidayList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildHolidayList

        End Function


#End Region

    End Class

End Namespace
