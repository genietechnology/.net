﻿'*****************************************************
'Generated 29/04/2017 11:04:25 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Risk
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_Title As String
        Dim m_Area As String
        Dim m_Status As String
        Dim m_Rev As String
        Dim m_Created As DateTime?
        Dim m_CreatedBy As String
        Dim m_AssFreq As String
        Dim m_AssFreqDays As Integer
        Dim m_AssLastId As Guid?
        Dim m_AssLast As DateTime?
        Dim m_AssLastBy As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._SiteName = ""
                ._Title = ""
                ._Area = ""
                ._Status = ""
                ._Rev = ""
                ._Created = Nothing
                ._CreatedBy = ""
                ._AssFreq = ""
                ._AssFreqDays = 0
                ._AssLastId = Nothing
                ._AssLast = Nothing
                ._AssLastBy = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._SiteName = ""
                ._Title = ""
                ._Area = ""
                ._Status = ""
                ._Rev = ""
                ._Created = Nothing
                ._CreatedBy = ""
                ._AssFreq = ""
                ._AssFreqDays = 0
                ._AssLastId = Nothing
                ._AssLast = Nothing
                ._AssLastBy = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")> _
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@Title")> _
        Public Property _Title() As String
            Get
                Return m_Title
            End Get
            Set(ByVal value As String)
                m_Title = value
            End Set
        End Property

        <StoredProcParameter("@Area")> _
        Public Property _Area() As String
            Get
                Return m_Area
            End Get
            Set(ByVal value As String)
                m_Area = value
            End Set
        End Property

        <StoredProcParameter("@Status")> _
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@Rev")> _
        Public Property _Rev() As String
            Get
                Return m_Rev
            End Get
            Set(ByVal value As String)
                m_Rev = value
            End Set
        End Property

        <StoredProcParameter("@Created")> _
        Public Property _Created() As DateTime?
            Get
                Return m_Created
            End Get
            Set(ByVal value As DateTime?)
                m_Created = value
            End Set
        End Property

        <StoredProcParameter("@CreatedBy")> _
        Public Property _CreatedBy() As String
            Get
                Return m_CreatedBy
            End Get
            Set(ByVal value As String)
                m_CreatedBy = value
            End Set
        End Property

        <StoredProcParameter("@AssFreq")> _
        Public Property _AssFreq() As String
            Get
                Return m_AssFreq
            End Get
            Set(ByVal value As String)
                m_AssFreq = value
            End Set
        End Property

        <StoredProcParameter("@AssFreqDays")> _
        Public Property _AssFreqDays() As Integer
            Get
                Return m_AssFreqDays
            End Get
            Set(ByVal value As Integer)
                m_AssFreqDays = value
            End Set
        End Property

        <StoredProcParameter("@AssLastId")> _
        Public Property _AssLastId() As Guid?
            Get
                Return m_AssLastId
            End Get
            Set(ByVal value As Guid?)
                m_AssLastId = value
            End Set
        End Property

        <StoredProcParameter("@AssLast")> _
        Public Property _AssLast() As DateTime?
            Get
                Return m_AssLast
            End Get
            Set(ByVal value As DateTime?)
                m_AssLast = value
            End Set
        End Property

        <StoredProcParameter("@AssLastBy")> _
        Public Property _AssLastBy() As String
            Get
                Return m_AssLastBy
            End Get
            Set(ByVal value As String)
                m_AssLastBy = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Risk

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRiskbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Risk

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRiskbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Risk)

            Dim _RiskList As List(Of Risk)
            _RiskList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRiskTable"))
            Return _RiskList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Risk)

            Dim _RiskList As List(Of Risk)
            _RiskList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRiskTable"))
            Return _RiskList

        End Function

        Public Shared Sub SaveAll(ByVal RiskList As List(Of Risk))

            For Each _Risk As Risk In RiskList
                SaveRecord(_Risk)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RiskList As List(Of Risk))

            For Each _Risk As Risk In RiskList
                SaveRecord(ConnectionString, _Risk)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Risk As Risk) As Guid
            Risk.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Risk, "upsertRisk")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Risk As Risk) As Guid
            Risk.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Risk, "upsertRisk")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRiskbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRiskbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRisk")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRisk")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Risk

            Dim _R As Risk = Nothing

            If DR IsNot Nothing Then
                _R = New Risk()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._SiteId = GetGUID(DR("site_id"))
                    _R._SiteName = DR("site_name").ToString.Trim
                    _R._Title = DR("title").ToString.Trim
                    _R._Area = DR("area").ToString.Trim
                    _R._Status = DR("status").ToString.Trim
                    _R._Rev = DR("rev").ToString.Trim
                    _R._Created = GetDateTime(DR("created"))
                    _R._CreatedBy = DR("created_by").ToString.Trim
                    _R._AssFreq = DR("ass_freq").ToString.Trim
                    _R._AssFreqDays = GetInteger(DR("ass_freq_days"))
                    _R._AssLastId = GetGUID(DR("ass_last_id"))
                    _R._AssLast = GetDateTime(DR("ass_last"))
                    _R._AssLastBy = DR("ass_last_by").ToString.Trim

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Risk)

            Dim _RiskList As New List(Of Risk)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RiskList.Add(PropertiesFromData(_DR))
            Next

            Return _RiskList

        End Function


#End Region

    End Class

End Namespace
