﻿'*****************************************************
'Generated 17/12/2015 15:34:00 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class EYStatement
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_AreaId As Guid?
        Dim m_AgeId As Guid?
        Dim m_Statement As String
        Dim m_DisplaySeq As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._AreaId = Nothing
                ._AgeId = Nothing
                ._Statement = ""
                ._DisplaySeq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._AreaId = Nothing
                ._AgeId = Nothing
                ._Statement = ""
                ._DisplaySeq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@AreaId")> _
        Public Property _AreaId() As Guid?
            Get
                Return m_AreaId
            End Get
            Set(ByVal value As Guid?)
                m_AreaId = value
            End Set
        End Property

        <StoredProcParameter("@AgeId")> _
        Public Property _AgeId() As Guid?
            Get
                Return m_AgeId
            End Get
            Set(ByVal value As Guid?)
                m_AgeId = value
            End Set
        End Property

        <StoredProcParameter("@Statement")> _
        Public Property _Statement() As String
            Get
                Return m_Statement
            End Get
            Set(ByVal value As String)
                m_Statement = value
            End Set
        End Property

        <StoredProcParameter("@DisplaySeq")> _
        Public Property _DisplaySeq() As Integer
            Get
                Return m_DisplaySeq
            End Get
            Set(ByVal value As Integer)
                m_DisplaySeq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As EYStatement

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEYStatementbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of EYStatement)

            Dim _EYStatementList As List(Of EYStatement)
            _EYStatementList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEYStatementTable"))
            Return _EYStatementList

        End Function

        Public Shared Sub SaveAll(ByVal EYStatementList As List(Of EYStatement))

            For Each _EYStatement As EYStatement In EYStatementList
                SaveRecord(_EYStatement)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal EYStatement As EYStatement) As Guid
            EYStatement.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, EYStatement, "upsertEYStatement")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEYStatementbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertEYStatement")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As EYStatement

            Dim _E As EYStatement = Nothing

            If DR IsNot Nothing Then
                _E = New EYStatement()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._AreaId = GetGUID(DR("area_id"))
                    _E._AgeId = GetGUID(DR("age_id"))
                    _E._Statement = DR("statement").ToString.Trim
                    _E._DisplaySeq = GetInteger(DR("display_seq"))

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of EYStatement)

            Dim _EYStatementList As New List(Of EYStatement)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EYStatementList.Add(PropertiesFromData(_DR))
            Next

            Return _EYStatementList

        End Function


#End Region

    End Class

End Namespace
