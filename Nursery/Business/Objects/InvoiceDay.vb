﻿'*****************************************************
'Generated 28/08/2016 17:42:13 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class InvoiceDay
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_InvoiceBatch As Guid?
        Dim m_InvoiceId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildForename As String
        Dim m_ChildSurname As String
        Dim m_InvoiceDate As Date?
        Dim m_HoursNonFunded As Decimal
        Dim m_HoursFunded As Decimal
        Dim m_HoursTotal As Decimal
        Dim m_ValueNonFunded As Decimal
        Dim m_ValueFunded As Decimal
        Dim m_ValueBoltons As Decimal
        Dim m_ValueGross As Decimal
        Dim m_ValueNet As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._InvoiceBatch = Nothing
                ._InvoiceId = Nothing
                ._ChildId = Nothing
                ._ChildForename = ""
                ._ChildSurname = ""
                ._InvoiceDate = Nothing
                ._HoursNonFunded = 0
                ._HoursFunded = 0
                ._HoursTotal = 0
                ._ValueNonFunded = 0
                ._ValueFunded = 0
                ._ValueBoltons = 0
                ._ValueGross = 0
                ._ValueNet = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._InvoiceBatch = Nothing
                ._InvoiceId = Nothing
                ._ChildId = Nothing
                ._ChildForename = ""
                ._ChildSurname = ""
                ._InvoiceDate = Nothing
                ._HoursNonFunded = 0
                ._HoursFunded = 0
                ._HoursTotal = 0
                ._ValueNonFunded = 0
                ._ValueFunded = 0
                ._ValueBoltons = 0
                ._ValueGross = 0
                ._ValueNet = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceBatch")> _
        Public Property _InvoiceBatch() As Guid?
            Get
                Return m_InvoiceBatch
            End Get
            Set(ByVal value As Guid?)
                m_InvoiceBatch = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceId")> _
        Public Property _InvoiceId() As Guid?
            Get
                Return m_InvoiceId
            End Get
            Set(ByVal value As Guid?)
                m_InvoiceId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildForename")> _
        Public Property _ChildForename() As String
            Get
                Return m_ChildForename
            End Get
            Set(ByVal value As String)
                m_ChildForename = value
            End Set
        End Property

        <StoredProcParameter("@ChildSurname")> _
        Public Property _ChildSurname() As String
            Get
                Return m_ChildSurname
            End Get
            Set(ByVal value As String)
                m_ChildSurname = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceDate")> _
        Public Property _InvoiceDate() As Date?
            Get
                Return m_InvoiceDate
            End Get
            Set(ByVal value As Date?)
                m_InvoiceDate = value
            End Set
        End Property

        <StoredProcParameter("@HoursNonFunded")> _
        Public Property _HoursNonFunded() As Decimal
            Get
                Return m_HoursNonFunded
            End Get
            Set(ByVal value As Decimal)
                m_HoursNonFunded = value
            End Set
        End Property

        <StoredProcParameter("@HoursFunded")> _
        Public Property _HoursFunded() As Decimal
            Get
                Return m_HoursFunded
            End Get
            Set(ByVal value As Decimal)
                m_HoursFunded = value
            End Set
        End Property

        <StoredProcParameter("@HoursTotal")> _
        Public Property _HoursTotal() As Decimal
            Get
                Return m_HoursTotal
            End Get
            Set(ByVal value As Decimal)
                m_HoursTotal = value
            End Set
        End Property

        <StoredProcParameter("@ValueNonFunded")> _
        Public Property _ValueNonFunded() As Decimal
            Get
                Return m_ValueNonFunded
            End Get
            Set(ByVal value As Decimal)
                m_ValueNonFunded = value
            End Set
        End Property

        <StoredProcParameter("@ValueFunded")> _
        Public Property _ValueFunded() As Decimal
            Get
                Return m_ValueFunded
            End Get
            Set(ByVal value As Decimal)
                m_ValueFunded = value
            End Set
        End Property

        <StoredProcParameter("@ValueBoltons")> _
        Public Property _ValueBoltons() As Decimal
            Get
                Return m_ValueBoltons
            End Get
            Set(ByVal value As Decimal)
                m_ValueBoltons = value
            End Set
        End Property

        <StoredProcParameter("@ValueGross")> _
        Public Property _ValueGross() As Decimal
            Get
                Return m_ValueGross
            End Get
            Set(ByVal value As Decimal)
                m_ValueGross = value
            End Set
        End Property

        <StoredProcParameter("@ValueNet")> _
        Public Property _ValueNet() As Decimal
            Get
                Return m_ValueNet
            End Get
            Set(ByVal value As Decimal)
                m_ValueNet = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As InvoiceDay

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInvoiceDaybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of InvoiceDay)

            Dim _InvoiceDayList As List(Of InvoiceDay)
            _InvoiceDayList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInvoiceDayTable"))
            Return _InvoiceDayList

        End Function

        Public Shared Sub SaveAll(ByVal InvoiceDayList As List(Of InvoiceDay))

            For Each _InvoiceDay As InvoiceDay In InvoiceDayList
                SaveRecord(_InvoiceDay)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal InvoiceDay As InvoiceDay) As Guid
            InvoiceDay.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, InvoiceDay, "upsertInvoiceDay")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteInvoiceDaybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertInvoiceDay")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As InvoiceDay

            Dim _I As InvoiceDay = Nothing

            If DR IsNot Nothing Then
                _I = New InvoiceDay()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._InvoiceBatch = GetGUID(DR("invoice_batch"))
                    _I._InvoiceId = GetGUID(DR("invoice_id"))
                    _I._ChildId = GetGUID(DR("child_id"))
                    _I._ChildForename = DR("child_forename").ToString.Trim
                    _I._ChildSurname = DR("child_surname").ToString.Trim
                    _I._InvoiceDate = GetDate(DR("invoice_date"))
                    _I._HoursNonFunded = GetDecimal(DR("hours_non_funded"))
                    _I._HoursFunded = GetDecimal(DR("hours_funded"))
                    _I._HoursTotal = GetDecimal(DR("hours_total"))
                    _I._ValueNonFunded = GetDecimal(DR("value_non_funded"))
                    _I._ValueFunded = GetDecimal(DR("value_funded"))
                    _I._ValueBoltons = GetDecimal(DR("value_boltons"))
                    _I._ValueGross = GetDecimal(DR("value_gross"))
                    _I._ValueNet = GetDecimal(DR("value_net"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of InvoiceDay)

            Dim _InvoiceDayList As New List(Of InvoiceDay)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InvoiceDayList.Add(PropertiesFromData(_DR))
            Next

            Return _InvoiceDayList

        End Function


#End Region

    End Class

End Namespace
