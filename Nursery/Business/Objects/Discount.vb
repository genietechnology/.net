﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Discount
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_DiscountType As String
        Dim m_DiscountValue As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._DiscountType = ""
                ._DiscountValue = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._DiscountType = ""
                ._DiscountValue = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@DiscountType")> _
        Public Property _DiscountType() As String
            Get
                Return m_DiscountType
            End Get
            Set(ByVal value As String)
                m_DiscountType = value
            End Set
        End Property

        <StoredProcParameter("@DiscountValue")> _
        Public Property _DiscountValue() As Decimal
            Get
                Return m_DiscountValue
            End Get
            Set(ByVal value As Decimal)
                m_DiscountValue = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Discount

            Dim _Discount As Discount
            _Discount = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDiscountbyID", ID))
            Return _Discount

        End Function

        Public Shared Function RetreiveAll() As List(Of Discount)

            Dim _DiscountList As List(Of Discount)
            _DiscountList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDiscountTable"))
            Return _DiscountList

        End Function

        Public Shared Sub SaveAll(ByVal DiscountList As List(Of Discount))

            For Each _Discount As Discount In DiscountList
                SaveRecord(_Discount)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Discount As Discount) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Discount, "upsertDiscount")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteDiscountbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertDiscount")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Discount

            Dim _D As New Discount()

            If DR IsNot Nothing Then
                With DR
                    _D.IsNew = False
                    _D.IsDeleted = False
                    _D._ID = GetGUID(DR("ID"))
                    _D._Name = DR("name").ToString.Trim
                    _D._DiscountType = DR("discount_type").ToString.Trim
                    _D._DiscountValue = GetDecimal(DR("discount_value"))

                End With
            End If

            Return _D

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Discount)

            Dim _DiscountList As New List(Of Discount)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DiscountList.Add(PropertiesFromData(_DR))
            Next

            Return _DiscountList

        End Function


#End Region

    End Class

End Namespace
