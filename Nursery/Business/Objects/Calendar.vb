﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Calendar
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Date As Date?
        Dim m_Dayofweek As String
        Dim m_Weekend As Boolean
        Dim m_Holiday As Boolean
        Dim m_HolidayName As String
        Dim m_Closed As Boolean
        Dim m_NoCharge As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Date = Nothing
                ._Dayofweek = ""
                ._Weekend = False
                ._Holiday = False
                ._HolidayName = ""
                ._Closed = False
                ._NoCharge = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Date = Nothing
                ._Dayofweek = ""
                ._Weekend = False
                ._Holiday = False
                ._HolidayName = ""
                ._Closed = False
                ._NoCharge = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As Date?
            Get
                Return m_Date
            End Get
            Set(ByVal value As Date?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@Dayofweek")> _
        Public Property _Dayofweek() As String
            Get
                Return m_Dayofweek
            End Get
            Set(ByVal value As String)
                m_Dayofweek = value
            End Set
        End Property

        <StoredProcParameter("@Weekend")> _
        Public Property _Weekend() As Boolean
            Get
                Return m_Weekend
            End Get
            Set(ByVal value As Boolean)
                m_Weekend = value
            End Set
        End Property

        <StoredProcParameter("@Holiday")> _
        Public Property _Holiday() As Boolean
            Get
                Return m_Holiday
            End Get
            Set(ByVal value As Boolean)
                m_Holiday = value
            End Set
        End Property

        <StoredProcParameter("@HolidayName")> _
        Public Property _HolidayName() As String
            Get
                Return m_HolidayName
            End Get
            Set(ByVal value As String)
                m_HolidayName = value
            End Set
        End Property

        <StoredProcParameter("@Closed")> _
        Public Property _Closed() As Boolean
            Get
                Return m_Closed
            End Get
            Set(ByVal value As Boolean)
                m_Closed = value
            End Set
        End Property

        <StoredProcParameter("@NoCharge")> _
        Public Property _NoCharge() As Boolean
            Get
                Return m_NoCharge
            End Get
            Set(ByVal value As Boolean)
                m_NoCharge = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Calendar

            Dim _Calendar As Calendar
            _Calendar = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCalendarbyID", ID))
            Return _Calendar

        End Function

        Public Shared Function RetreiveAll() As List(Of Calendar)

            Dim _CalendarList As List(Of Calendar)
            _CalendarList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCalendarTable"))
            Return _CalendarList

        End Function

        Public Shared Sub SaveAll(ByVal CalendarList As List(Of Calendar))

            For Each _Calendar As Calendar In CalendarList
                SaveRecord(_Calendar)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Calendar As Calendar) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Calendar, "upsertCalendar")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCalendarbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCalendar")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Calendar

            Dim _C As New Calendar()

            If DR IsNot Nothing Then
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Date = GetDate(DR("date"))
                    _C._Dayofweek = DR("dayofweek").ToString.Trim
                    _C._Weekend = GetBoolean(DR("weekend"))
                    _C._Holiday = GetBoolean(DR("holiday"))
                    _C._HolidayName = DR("holiday_name").ToString.Trim
                    _C._Closed = GetBoolean(DR("closed"))
                    _C._NoCharge = GetBoolean(DR("no_charge"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Calendar)

            Dim _CalendarList As New List(Of Calendar)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CalendarList.Add(PropertiesFromData(_DR))
            Next

            Return _CalendarList

        End Function


#End Region

    End Class

End Namespace
