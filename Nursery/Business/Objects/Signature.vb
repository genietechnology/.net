﻿'*****************************************************
'Generated 05/01/2017 21:54:48 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Signature
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_PersonId As Guid?
        Dim m_PersonType As String
        Dim m_PersonName As String
        Dim m_Signature As Byte()
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._PersonId = Nothing
                ._PersonType = ""
                ._PersonName = ""
                ._Signature = Nothing
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._PersonId = Nothing
                ._PersonType = ""
                ._PersonName = ""
                ._Signature = Nothing
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@PersonId")> _
        Public Property _PersonId() As Guid?
            Get
                Return m_PersonId
            End Get
            Set(ByVal value As Guid?)
                m_PersonId = value
            End Set
        End Property

        <StoredProcParameter("@PersonType")> _
        Public Property _PersonType() As String
            Get
                Return m_PersonType
            End Get
            Set(ByVal value As String)
                m_PersonType = value
            End Set
        End Property

        <StoredProcParameter("@PersonName")> _
        Public Property _PersonName() As String
            Get
                Return m_PersonName
            End Get
            Set(ByVal value As String)
                m_PersonName = value
            End Set
        End Property

        <StoredProcParameter("@Signature")> _
        Public Property _Signature() As Byte()
            Get
                Return m_Signature
            End Get
            Set(ByVal value As Byte())
                m_Signature = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Signature

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSignaturebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Signature)

            Dim _SignatureList As List(Of Signature)
            _SignatureList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSignatureTable"))
            Return _SignatureList

        End Function

        Public Shared Sub SaveAll(ByVal SignatureList As List(Of Signature))

            For Each _Signature As Signature In SignatureList
                SaveRecord(_Signature)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Signature As Signature) As Guid
            Signature.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Signature, "upsertSignature")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSignaturebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSignature")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Signature

            Dim _S As Signature = Nothing

            If DR IsNot Nothing Then
                _S = New Signature()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._DayId = GetGUID(DR("day_id"))
                    _S._PersonId = GetGUID(DR("person_id"))
                    _S._PersonType = DR("person_type").ToString.Trim
                    _S._PersonName = DR("person_name").ToString.Trim
                    _S._Signature = GetImage(DR("signature"))
                    _S._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Signature)

            Dim _SignatureList As New List(Of Signature)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SignatureList.Add(PropertiesFromData(_DR))
            Next

            Return _SignatureList

        End Function


#End Region

    End Class

End Namespace
