﻿'*****************************************************
'Generated 27/09/2018 17:41:13 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Contact
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FamilyId = Nothing
                ._Surname = ""
                ._Forename = ""
                ._Fullname = ""
                ._Relationship = ""
                ._Dob = Nothing
                ._PrimaryCont = False
                ._EmerCont = False
                ._Sms = False
                ._SmsName = ""
                ._Password = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._Address = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._Email = ""
                ._Job = False
                ._JobTitle = ""
                ._JobEmployer = ""
                ._JobAddress = ""
                ._JobTel = ""
                ._JobEmail = ""
                ._Voucher = False
                ._VoucherId = Nothing
                ._VoucherName = ""
                ._VoucherRef = ""
                ._VoucherDate = Nothing
                ._VoucherValue = 0
                ._Report = False
                ._ReportDetail = ""
                ._ReportMethod = ""
                ._ReportEmail = ""
                ._Photo = Nothing
                ._Parent = False
                ._Collect = False
                ._InvoiceEmail = False
                ._Nino = ""
                ._CommMethod = ""
                ._CommMarketing = False
                ._TouchPin = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FamilyId = Nothing
                ._Surname = ""
                ._Forename = ""
                ._Fullname = ""
                ._Relationship = ""
                ._Dob = Nothing
                ._PrimaryCont = False
                ._EmerCont = False
                ._Sms = False
                ._SmsName = ""
                ._Password = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._Address = ""
                ._TelHome = ""
                ._TelMobile = ""
                ._Email = ""
                ._Job = False
                ._JobTitle = ""
                ._JobEmployer = ""
                ._JobAddress = ""
                ._JobTel = ""
                ._JobEmail = ""
                ._Voucher = False
                ._VoucherId = Nothing
                ._VoucherName = ""
                ._VoucherRef = ""
                ._VoucherDate = Nothing
                ._VoucherValue = 0
                ._Report = False
                ._ReportDetail = ""
                ._ReportMethod = ""
                ._ReportEmail = ""
                ._Photo = Nothing
                ._Parent = False
                ._Collect = False
                ._InvoiceEmail = False
                ._Nino = ""
                ._CommMethod = ""
                ._CommMarketing = False
                ._TouchPin = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@FamilyId")>
        Public Property _FamilyId() As Guid?

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String

        <StoredProcParameter("@Relationship")>
        Public Property _Relationship() As String

        <StoredProcParameter("@Dob")>
        Public Property _Dob() As Date?

        <StoredProcParameter("@PrimaryCont")>
        Public Property _PrimaryCont() As Boolean

        <StoredProcParameter("@EmerCont")>
        Public Property _EmerCont() As Boolean

        <StoredProcParameter("@Sms")>
        Public Property _Sms() As Boolean

        <StoredProcParameter("@SmsName")>
        Public Property _SmsName() As String

        <StoredProcParameter("@Password")>
        Public Property _Password() As String

        <StoredProcParameter("@Add1")>
        Public Property _Add1() As String

        <StoredProcParameter("@Add2")>
        Public Property _Add2() As String

        <StoredProcParameter("@Add3")>
        Public Property _Add3() As String

        <StoredProcParameter("@Add4")>
        Public Property _Add4() As String

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String

        <StoredProcParameter("@Address")>
        Public Property _Address() As String

        <StoredProcParameter("@TelHome")>
        Public Property _TelHome() As String

        <StoredProcParameter("@TelMobile")>
        Public Property _TelMobile() As String

        <StoredProcParameter("@Email")>
        Public Property _Email() As String

        <StoredProcParameter("@Job")>
        Public Property _Job() As Boolean

        <StoredProcParameter("@JobTitle")>
        Public Property _JobTitle() As String

        <StoredProcParameter("@JobEmployer")>
        Public Property _JobEmployer() As String

        <StoredProcParameter("@JobAddress")>
        Public Property _JobAddress() As String

        <StoredProcParameter("@JobTel")>
        Public Property _JobTel() As String

        <StoredProcParameter("@JobEmail")>
        Public Property _JobEmail() As String

        <StoredProcParameter("@Voucher")>
        Public Property _Voucher() As Boolean

        <StoredProcParameter("@VoucherId")>
        Public Property _VoucherId() As Guid?

        <StoredProcParameter("@VoucherName")>
        Public Property _VoucherName() As String

        <StoredProcParameter("@VoucherRef")>
        Public Property _VoucherRef() As String

        <StoredProcParameter("@VoucherDate")>
        Public Property _VoucherDate() As Date?

        <StoredProcParameter("@VoucherValue")>
        Public Property _VoucherValue() As Decimal

        <StoredProcParameter("@Report")>
        Public Property _Report() As Boolean

        <StoredProcParameter("@ReportDetail")>
        Public Property _ReportDetail() As String

        <StoredProcParameter("@ReportMethod")>
        Public Property _ReportMethod() As String

        <StoredProcParameter("@ReportEmail")>
        Public Property _ReportEmail() As String

        <StoredProcParameter("@Photo")>
        Public Property _Photo() As Guid?

        <StoredProcParameter("@Parent")>
        Public Property _Parent() As Boolean

        <StoredProcParameter("@Collect")>
        Public Property _Collect() As Boolean

        <StoredProcParameter("@InvoiceEmail")>
        Public Property _InvoiceEmail() As Boolean

        <StoredProcParameter("@Nino")>
        Public Property _Nino() As String

        <StoredProcParameter("@CommMethod")>
        Public Property _CommMethod() As String

        <StoredProcParameter("@CommMarketing")>
        Public Property _CommMarketing() As Boolean

        <StoredProcParameter("@TouchPin")>
        Public Property _TouchPin() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Contact
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getContactbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Contact
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getContactbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Contact)
            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getContactTable"))
            Return _ContactList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Contact)
            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getContactTable"))
            Return _ContactList
        End Function

        Public Shared Sub SaveAll(ByVal ContactList As List(Of Contact))
            For Each _Contact As Contact In ContactList
                SaveRecord(_Contact)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ContactList As List(Of Contact))
            For Each _Contact As Contact In ContactList
                SaveRecord(ConnectionString, _Contact)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Contact As Contact) As Guid
            Dim _Current As Contact = RetreiveByID(Contact._ID.Value)
            Contact.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Contact, _Current, "upsertContact")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Contact As Contact) As Guid
            Dim _Current As Contact = RetreiveByID(ConnectionString, Contact._ID.Value)
            Contact.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Contact, _Current, "upsertContact")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Contact = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteContactbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Contact = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteContactbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Contact = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertContact")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Contact = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertContact")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Contact

            Dim _C As Contact = Nothing

            If DR IsNot Nothing Then
                _C = New Contact()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._FamilyId = GetGUID(DR("Family_ID"))
                    _C._Surname = DR("surname").ToString.Trim
                    _C._Forename = DR("forename").ToString.Trim
                    _C._Fullname = DR("fullname").ToString.Trim
                    _C._Relationship = DR("relationship").ToString.Trim
                    _C._Dob = GetDate(DR("dob"))
                    _C._PrimaryCont = GetBoolean(DR("primary_cont"))
                    _C._EmerCont = GetBoolean(DR("emer_cont"))
                    _C._Sms = GetBoolean(DR("sms"))
                    _C._SmsName = DR("sms_name").ToString.Trim
                    _C._Password = DR("password").ToString.Trim
                    _C._Add1 = DR("add1").ToString.Trim
                    _C._Add2 = DR("add2").ToString.Trim
                    _C._Add3 = DR("add3").ToString.Trim
                    _C._Add4 = DR("add4").ToString.Trim
                    _C._Postcode = DR("postcode").ToString.Trim
                    _C._Address = DR("address").ToString.Trim
                    _C._TelHome = DR("tel_home").ToString.Trim
                    _C._TelMobile = DR("tel_mobile").ToString.Trim
                    _C._Email = DR("email").ToString.Trim
                    _C._Job = GetBoolean(DR("job"))
                    _C._JobTitle = DR("job_title").ToString.Trim
                    _C._JobEmployer = DR("job_employer").ToString.Trim
                    _C._JobAddress = DR("job_address").ToString.Trim
                    _C._JobTel = DR("job_tel").ToString.Trim
                    _C._JobEmail = DR("job_email").ToString.Trim
                    _C._Voucher = GetBoolean(DR("voucher"))
                    _C._VoucherId = GetGUID(DR("voucher_id"))
                    _C._VoucherName = DR("voucher_name").ToString.Trim
                    _C._VoucherRef = DR("voucher_ref").ToString.Trim
                    _C._VoucherDate = GetDate(DR("voucher_date"))
                    _C._VoucherValue = GetDecimal(DR("voucher_value"))
                    _C._Report = GetBoolean(DR("report"))
                    _C._ReportDetail = DR("report_detail").ToString.Trim
                    _C._ReportMethod = DR("report_method").ToString.Trim
                    _C._ReportEmail = DR("report_email").ToString.Trim
                    _C._Photo = GetGUID(DR("photo"))
                    _C._Parent = GetBoolean(DR("parent"))
                    _C._Collect = GetBoolean(DR("collect"))
                    _C._InvoiceEmail = GetBoolean(DR("invoice_email"))
                    _C._Nino = DR("nino").ToString.Trim
                    _C._CommMethod = DR("comm_method").ToString.Trim
                    _C._CommMarketing = GetBoolean(DR("comm_marketing"))
                    _C._TouchPin = DR("touch_pin").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Contact)

            Dim _ContactList As New List(Of Contact)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ContactList.Add(PropertiesFromData(_DR))
            Next

            Return _ContactList

        End Function


#End Region

    End Class

End Namespace
