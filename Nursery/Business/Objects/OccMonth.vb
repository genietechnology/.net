﻿'*****************************************************
'Generated 09/04/2016 21:55:05 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class OccMonth
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_PeriodYear As Integer
        Dim m_PeriodMonth As Byte
        Dim m_PeriodFrom As Date?
        Dim m_PeriodTo As Date?
        Dim m_RoomId As Guid?
        Dim m_RoomName As String
        Dim m_RoomCapacity As Integer
        Dim m_RoomRatio As Integer
        Dim m_Mondays As Integer
        Dim m_Tuesdays As Integer
        Dim m_Wednesdays As Integer
        Dim m_Thursdays As Integer
        Dim m_Fridays As Integer
        Dim m_MonAm As Integer
        Dim m_MonPm As Integer
        Dim m_MonHc As Integer
        Dim m_MonOcc As Decimal
        Dim m_TueAm As Integer
        Dim m_TuePm As Integer
        Dim m_TueHc As Integer
        Dim m_TueOcc As Decimal
        Dim m_WedAm As Integer
        Dim m_WedPm As Integer
        Dim m_WedHc As Integer
        Dim m_WedOcc As Decimal
        Dim m_ThuAm As Integer
        Dim m_ThuPm As Integer
        Dim m_ThuHc As Integer
        Dim m_ThuOcc As Decimal
        Dim m_FriAm As Integer
        Dim m_FriPm As Integer
        Dim m_FriHc As Integer
        Dim m_FriOcc As Decimal
        Dim m_Occ As Decimal
        Dim m_Fte As Decimal
        Dim m_Ft As Decimal
        Dim m_Pt As Decimal
        Dim m_Hours As Decimal
        Dim m_Funded As Decimal
        Dim m_Revenue As Decimal
        Dim m_TotalCapacity As Integer
        Dim m_TotalDays As Integer
        Dim m_TotalHeadcount As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._PeriodYear = 0
                ._PeriodMonth = Nothing
                ._PeriodFrom = Nothing
                ._PeriodTo = Nothing
                ._RoomId = Nothing
                ._RoomName = ""
                ._RoomCapacity = 0
                ._RoomRatio = 0
                ._Mondays = 0
                ._Tuesdays = 0
                ._Wednesdays = 0
                ._Thursdays = 0
                ._Fridays = 0
                ._MonAm = 0
                ._MonPm = 0
                ._MonHc = 0
                ._MonOcc = 0
                ._TueAm = 0
                ._TuePm = 0
                ._TueHc = 0
                ._TueOcc = 0
                ._WedAm = 0
                ._WedPm = 0
                ._WedHc = 0
                ._WedOcc = 0
                ._ThuAm = 0
                ._ThuPm = 0
                ._ThuHc = 0
                ._ThuOcc = 0
                ._FriAm = 0
                ._FriPm = 0
                ._FriHc = 0
                ._FriOcc = 0
                ._Occ = 0
                ._Fte = 0
                ._Ft = 0
                ._Pt = 0
                ._Hours = 0
                ._Funded = 0
                ._Revenue = 0
                ._TotalCapacity = 0
                ._TotalDays = 0
                ._TotalHeadcount = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._PeriodYear = 0
                ._PeriodMonth = Nothing
                ._PeriodFrom = Nothing
                ._PeriodTo = Nothing
                ._RoomId = Nothing
                ._RoomName = ""
                ._RoomCapacity = 0
                ._RoomRatio = 0
                ._Mondays = 0
                ._Tuesdays = 0
                ._Wednesdays = 0
                ._Thursdays = 0
                ._Fridays = 0
                ._MonAm = 0
                ._MonPm = 0
                ._MonHc = 0
                ._MonOcc = 0
                ._TueAm = 0
                ._TuePm = 0
                ._TueHc = 0
                ._TueOcc = 0
                ._WedAm = 0
                ._WedPm = 0
                ._WedHc = 0
                ._WedOcc = 0
                ._ThuAm = 0
                ._ThuPm = 0
                ._ThuHc = 0
                ._ThuOcc = 0
                ._FriAm = 0
                ._FriPm = 0
                ._FriHc = 0
                ._FriOcc = 0
                ._Occ = 0
                ._Fte = 0
                ._Ft = 0
                ._Pt = 0
                ._Hours = 0
                ._Funded = 0
                ._Revenue = 0
                ._TotalCapacity = 0
                ._TotalDays = 0
                ._TotalHeadcount = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@PeriodYear")> _
        Public Property _PeriodYear() As Integer
            Get
                Return m_PeriodYear
            End Get
            Set(ByVal value As Integer)
                m_PeriodYear = value
            End Set
        End Property

        <StoredProcParameter("@PeriodMonth")> _
        Public Property _PeriodMonth() As Byte
            Get
                Return m_PeriodMonth
            End Get
            Set(ByVal value As Byte)
                m_PeriodMonth = value
            End Set
        End Property

        <StoredProcParameter("@PeriodFrom")> _
        Public Property _PeriodFrom() As Date?
            Get
                Return m_PeriodFrom
            End Get
            Set(ByVal value As Date?)
                m_PeriodFrom = value
            End Set
        End Property

        <StoredProcParameter("@PeriodTo")> _
        Public Property _PeriodTo() As Date?
            Get
                Return m_PeriodTo
            End Get
            Set(ByVal value As Date?)
                m_PeriodTo = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@RoomName")> _
        Public Property _RoomName() As String
            Get
                Return m_RoomName
            End Get
            Set(ByVal value As String)
                m_RoomName = value
            End Set
        End Property

        <StoredProcParameter("@RoomCapacity")> _
        Public Property _RoomCapacity() As Integer
            Get
                Return m_RoomCapacity
            End Get
            Set(ByVal value As Integer)
                m_RoomCapacity = value
            End Set
        End Property

        <StoredProcParameter("@RoomRatio")> _
        Public Property _RoomRatio() As Integer
            Get
                Return m_RoomRatio
            End Get
            Set(ByVal value As Integer)
                m_RoomRatio = value
            End Set
        End Property

        <StoredProcParameter("@Mondays")> _
        Public Property _Mondays() As Integer
            Get
                Return m_Mondays
            End Get
            Set(ByVal value As Integer)
                m_Mondays = value
            End Set
        End Property

        <StoredProcParameter("@Tuesdays")> _
        Public Property _Tuesdays() As Integer
            Get
                Return m_Tuesdays
            End Get
            Set(ByVal value As Integer)
                m_Tuesdays = value
            End Set
        End Property

        <StoredProcParameter("@Wednesdays")> _
        Public Property _Wednesdays() As Integer
            Get
                Return m_Wednesdays
            End Get
            Set(ByVal value As Integer)
                m_Wednesdays = value
            End Set
        End Property

        <StoredProcParameter("@Thursdays")> _
        Public Property _Thursdays() As Integer
            Get
                Return m_Thursdays
            End Get
            Set(ByVal value As Integer)
                m_Thursdays = value
            End Set
        End Property

        <StoredProcParameter("@Fridays")> _
        Public Property _Fridays() As Integer
            Get
                Return m_Fridays
            End Get
            Set(ByVal value As Integer)
                m_Fridays = value
            End Set
        End Property

        <StoredProcParameter("@MonAm")> _
        Public Property _MonAm() As Integer
            Get
                Return m_MonAm
            End Get
            Set(ByVal value As Integer)
                m_MonAm = value
            End Set
        End Property

        <StoredProcParameter("@MonPm")> _
        Public Property _MonPm() As Integer
            Get
                Return m_MonPm
            End Get
            Set(ByVal value As Integer)
                m_MonPm = value
            End Set
        End Property

        <StoredProcParameter("@MonHc")> _
        Public Property _MonHc() As Integer
            Get
                Return m_MonHc
            End Get
            Set(ByVal value As Integer)
                m_MonHc = value
            End Set
        End Property

        <StoredProcParameter("@MonOcc")> _
        Public Property _MonOcc() As Decimal
            Get
                Return m_MonOcc
            End Get
            Set(ByVal value As Decimal)
                m_MonOcc = value
            End Set
        End Property

        <StoredProcParameter("@TueAm")> _
        Public Property _TueAm() As Integer
            Get
                Return m_TueAm
            End Get
            Set(ByVal value As Integer)
                m_TueAm = value
            End Set
        End Property

        <StoredProcParameter("@TuePm")> _
        Public Property _TuePm() As Integer
            Get
                Return m_TuePm
            End Get
            Set(ByVal value As Integer)
                m_TuePm = value
            End Set
        End Property

        <StoredProcParameter("@TueHc")> _
        Public Property _TueHc() As Integer
            Get
                Return m_TueHc
            End Get
            Set(ByVal value As Integer)
                m_TueHc = value
            End Set
        End Property

        <StoredProcParameter("@TueOcc")> _
        Public Property _TueOcc() As Decimal
            Get
                Return m_TueOcc
            End Get
            Set(ByVal value As Decimal)
                m_TueOcc = value
            End Set
        End Property

        <StoredProcParameter("@WedAm")> _
        Public Property _WedAm() As Integer
            Get
                Return m_WedAm
            End Get
            Set(ByVal value As Integer)
                m_WedAm = value
            End Set
        End Property

        <StoredProcParameter("@WedPm")> _
        Public Property _WedPm() As Integer
            Get
                Return m_WedPm
            End Get
            Set(ByVal value As Integer)
                m_WedPm = value
            End Set
        End Property

        <StoredProcParameter("@WedHc")> _
        Public Property _WedHc() As Integer
            Get
                Return m_WedHc
            End Get
            Set(ByVal value As Integer)
                m_WedHc = value
            End Set
        End Property

        <StoredProcParameter("@WedOcc")> _
        Public Property _WedOcc() As Decimal
            Get
                Return m_WedOcc
            End Get
            Set(ByVal value As Decimal)
                m_WedOcc = value
            End Set
        End Property

        <StoredProcParameter("@ThuAm")> _
        Public Property _ThuAm() As Integer
            Get
                Return m_ThuAm
            End Get
            Set(ByVal value As Integer)
                m_ThuAm = value
            End Set
        End Property

        <StoredProcParameter("@ThuPm")> _
        Public Property _ThuPm() As Integer
            Get
                Return m_ThuPm
            End Get
            Set(ByVal value As Integer)
                m_ThuPm = value
            End Set
        End Property

        <StoredProcParameter("@ThuHc")> _
        Public Property _ThuHc() As Integer
            Get
                Return m_ThuHc
            End Get
            Set(ByVal value As Integer)
                m_ThuHc = value
            End Set
        End Property

        <StoredProcParameter("@ThuOcc")> _
        Public Property _ThuOcc() As Decimal
            Get
                Return m_ThuOcc
            End Get
            Set(ByVal value As Decimal)
                m_ThuOcc = value
            End Set
        End Property

        <StoredProcParameter("@FriAm")> _
        Public Property _FriAm() As Integer
            Get
                Return m_FriAm
            End Get
            Set(ByVal value As Integer)
                m_FriAm = value
            End Set
        End Property

        <StoredProcParameter("@FriPm")> _
        Public Property _FriPm() As Integer
            Get
                Return m_FriPm
            End Get
            Set(ByVal value As Integer)
                m_FriPm = value
            End Set
        End Property

        <StoredProcParameter("@FriHc")> _
        Public Property _FriHc() As Integer
            Get
                Return m_FriHc
            End Get
            Set(ByVal value As Integer)
                m_FriHc = value
            End Set
        End Property

        <StoredProcParameter("@FriOcc")> _
        Public Property _FriOcc() As Decimal
            Get
                Return m_FriOcc
            End Get
            Set(ByVal value As Decimal)
                m_FriOcc = value
            End Set
        End Property

        <StoredProcParameter("@Occ")> _
        Public Property _Occ() As Decimal
            Get
                Return m_Occ
            End Get
            Set(ByVal value As Decimal)
                m_Occ = value
            End Set
        End Property

        <StoredProcParameter("@Fte")> _
        Public Property _Fte() As Decimal
            Get
                Return m_Fte
            End Get
            Set(ByVal value As Decimal)
                m_Fte = value
            End Set
        End Property

        <StoredProcParameter("@Ft")> _
        Public Property _Ft() As Decimal
            Get
                Return m_Ft
            End Get
            Set(ByVal value As Decimal)
                m_Ft = value
            End Set
        End Property

        <StoredProcParameter("@Pt")> _
        Public Property _Pt() As Decimal
            Get
                Return m_Pt
            End Get
            Set(ByVal value As Decimal)
                m_Pt = value
            End Set
        End Property

        <StoredProcParameter("@Hours")> _
        Public Property _Hours() As Decimal
            Get
                Return m_Hours
            End Get
            Set(ByVal value As Decimal)
                m_Hours = value
            End Set
        End Property

        <StoredProcParameter("@Funded")> _
        Public Property _Funded() As Decimal
            Get
                Return m_Funded
            End Get
            Set(ByVal value As Decimal)
                m_Funded = value
            End Set
        End Property

        <StoredProcParameter("@Revenue")> _
        Public Property _Revenue() As Decimal
            Get
                Return m_Revenue
            End Get
            Set(ByVal value As Decimal)
                m_Revenue = value
            End Set
        End Property

        <StoredProcParameter("@TotalCapacity")> _
        Public Property _TotalCapacity() As Integer
            Get
                Return m_TotalCapacity
            End Get
            Set(ByVal value As Integer)
                m_TotalCapacity = value
            End Set
        End Property

        <StoredProcParameter("@TotalDays")> _
        Public Property _TotalDays() As Integer
            Get
                Return m_TotalDays
            End Get
            Set(ByVal value As Integer)
                m_TotalDays = value
            End Set
        End Property

        <StoredProcParameter("@TotalHeadcount")> _
        Public Property _TotalHeadcount() As Decimal
            Get
                Return m_TotalHeadcount
            End Get
            Set(ByVal value As Decimal)
                m_TotalHeadcount = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As OccMonth

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getOccMonthbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of OccMonth)

            Dim _OccMonthList As List(Of OccMonth)
            _OccMonthList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getOccMonthTable"))
            Return _OccMonthList

        End Function

        Public Shared Sub SaveAll(ByVal OccMonthList As List(Of OccMonth))

            For Each _OccMonth As OccMonth In OccMonthList
                SaveRecord(_OccMonth)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal OccMonth As OccMonth) As Guid
            OccMonth.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, OccMonth, "upsertOccMonth")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteOccMonthbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertOccMonth")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As OccMonth

            Dim _O As OccMonth = Nothing

            If DR IsNot Nothing Then
                _O = New OccMonth()
                With DR
                    _O.IsNew = False
                    _O.IsDeleted = False
                    _O._ID = GetGUID(DR("ID"))
                    _O._PeriodYear = GetInteger(DR("period_year"))
                    _O._PeriodMonth = GetByte(DR("period_month"))
                    _O._PeriodFrom = GetDate(DR("period_from"))
                    _O._PeriodTo = GetDate(DR("period_to"))
                    _O._RoomId = GetGUID(DR("room_id"))
                    _O._RoomName = DR("room_name").ToString.Trim
                    _O._RoomCapacity = GetInteger(DR("room_capacity"))
                    _O._RoomRatio = GetInteger(DR("room_ratio"))
                    _O._Mondays = GetInteger(DR("mondays"))
                    _O._Tuesdays = GetInteger(DR("tuesdays"))
                    _O._Wednesdays = GetInteger(DR("wednesdays"))
                    _O._Thursdays = GetInteger(DR("thursdays"))
                    _O._Fridays = GetInteger(DR("fridays"))
                    _O._MonAm = GetInteger(DR("mon_am"))
                    _O._MonPm = GetInteger(DR("mon_pm"))
                    _O._MonHc = GetInteger(DR("mon_hc"))
                    _O._MonOcc = GetDecimal(DR("mon_occ"))
                    _O._TueAm = GetInteger(DR("tue_am"))
                    _O._TuePm = GetInteger(DR("tue_pm"))
                    _O._TueHc = GetInteger(DR("tue_hc"))
                    _O._TueOcc = GetDecimal(DR("tue_occ"))
                    _O._WedAm = GetInteger(DR("wed_am"))
                    _O._WedPm = GetInteger(DR("wed_pm"))
                    _O._WedHc = GetInteger(DR("wed_hc"))
                    _O._WedOcc = GetDecimal(DR("wed_occ"))
                    _O._ThuAm = GetInteger(DR("thu_am"))
                    _O._ThuPm = GetInteger(DR("thu_pm"))
                    _O._ThuHc = GetInteger(DR("thu_hc"))
                    _O._ThuOcc = GetDecimal(DR("thu_occ"))
                    _O._FriAm = GetInteger(DR("fri_am"))
                    _O._FriPm = GetInteger(DR("fri_pm"))
                    _O._FriHc = GetInteger(DR("fri_hc"))
                    _O._FriOcc = GetDecimal(DR("fri_occ"))
                    _O._Occ = GetDecimal(DR("occ"))
                    _O._Fte = GetDecimal(DR("fte"))
                    _O._Ft = GetDecimal(DR("ft"))
                    _O._Pt = GetDecimal(DR("pt"))
                    _O._Hours = GetDecimal(DR("hours"))
                    _O._Funded = GetDecimal(DR("funded"))
                    _O._Revenue = GetDecimal(DR("revenue"))
                    _O._TotalCapacity = GetInteger(DR("total_capacity"))
                    _O._TotalDays = GetInteger(DR("total_days"))
                    _O._TotalHeadcount = GetDecimal(DR("total_headcount"))

                End With
            End If

            Return _O

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of OccMonth)

            Dim _OccMonthList As New List(Of OccMonth)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _OccMonthList.Add(PropertiesFromData(_DR))
            Next

            Return _OccMonthList

        End Function


#End Region

    End Class

End Namespace
