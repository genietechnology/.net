﻿'*****************************************************
'Generated 20/03/2017 16:45:58 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Turnover
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_tYear As Integer
        Dim m_tMonth As Integer
        Dim m_BatchCount As Integer
        Dim m_NonFundedHours As Decimal
        Dim m_NonFundedValue As Decimal
        Dim m_FundedHours As Decimal
        Dim m_FundedValue As Decimal
        Dim m_AdditionalValue As Decimal
        Dim m_OtherValue As Decimal
        Dim m_BoltOns As Decimal
        Dim m_SubTotal As Decimal
        Dim m_Discount As Decimal
        Dim m_GrandTotal As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._tYear = 0
                ._tMonth = 0
                ._BatchCount = 0
                ._NonFundedHours = 0
                ._NonFundedValue = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._AdditionalValue = 0
                ._OtherValue = 0
                ._BoltOns = 0
                ._SubTotal = 0
                ._Discount = 0
                ._GrandTotal = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._tYear = 0
                ._tMonth = 0
                ._BatchCount = 0
                ._NonFundedHours = 0
                ._NonFundedValue = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._AdditionalValue = 0
                ._OtherValue = 0
                ._BoltOns = 0
                ._SubTotal = 0
                ._Discount = 0
                ._GrandTotal = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@tYear")> _
        Public Property _tYear() As Integer
            Get
                Return m_tYear
            End Get
            Set(ByVal value As Integer)
                m_tYear = value
            End Set
        End Property

        <StoredProcParameter("@tMonth")> _
        Public Property _tMonth() As Integer
            Get
                Return m_tMonth
            End Get
            Set(ByVal value As Integer)
                m_tMonth = value
            End Set
        End Property

        <StoredProcParameter("@BatchCount")> _
        Public Property _BatchCount() As Integer
            Get
                Return m_BatchCount
            End Get
            Set(ByVal value As Integer)
                m_BatchCount = value
            End Set
        End Property

        <StoredProcParameter("@NonFundedHours")> _
        Public Property _NonFundedHours() As Decimal
            Get
                Return m_NonFundedHours
            End Get
            Set(ByVal value As Decimal)
                m_NonFundedHours = value
            End Set
        End Property

        <StoredProcParameter("@NonFundedValue")> _
        Public Property _NonFundedValue() As Decimal
            Get
                Return m_NonFundedValue
            End Get
            Set(ByVal value As Decimal)
                m_NonFundedValue = value
            End Set
        End Property

        <StoredProcParameter("@FundedHours")> _
        Public Property _FundedHours() As Decimal
            Get
                Return m_FundedHours
            End Get
            Set(ByVal value As Decimal)
                m_FundedHours = value
            End Set
        End Property

        <StoredProcParameter("@FundedValue")> _
        Public Property _FundedValue() As Decimal
            Get
                Return m_FundedValue
            End Get
            Set(ByVal value As Decimal)
                m_FundedValue = value
            End Set
        End Property

        <StoredProcParameter("@AdditionalValue")> _
        Public Property _AdditionalValue() As Decimal
            Get
                Return m_AdditionalValue
            End Get
            Set(ByVal value As Decimal)
                m_AdditionalValue = value
            End Set
        End Property

        <StoredProcParameter("@OtherValue")> _
        Public Property _OtherValue() As Decimal
            Get
                Return m_OtherValue
            End Get
            Set(ByVal value As Decimal)
                m_OtherValue = value
            End Set
        End Property

        <StoredProcParameter("@BoltOns")> _
        Public Property _BoltOns() As Decimal
            Get
                Return m_BoltOns
            End Get
            Set(ByVal value As Decimal)
                m_BoltOns = value
            End Set
        End Property

        <StoredProcParameter("@SubTotal")> _
        Public Property _SubTotal() As Decimal
            Get
                Return m_SubTotal
            End Get
            Set(ByVal value As Decimal)
                m_SubTotal = value
            End Set
        End Property

        <StoredProcParameter("@Discount")> _
        Public Property _Discount() As Decimal
            Get
                Return m_Discount
            End Get
            Set(ByVal value As Decimal)
                m_Discount = value
            End Set
        End Property

        <StoredProcParameter("@GrandTotal")> _
        Public Property _GrandTotal() As Decimal
            Get
                Return m_GrandTotal
            End Get
            Set(ByVal value As Decimal)
                m_GrandTotal = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Turnover

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTurnoverbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Turnover

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTurnoverbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Turnover)

            Dim _TurnoverList As List(Of Turnover)
            _TurnoverList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTurnoverTable"))
            Return _TurnoverList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Turnover)

            Dim _TurnoverList As List(Of Turnover)
            _TurnoverList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTurnoverTable"))
            Return _TurnoverList

        End Function

        Public Shared Sub SaveAll(ByVal TurnoverList As List(Of Turnover))

            For Each _Turnover As Turnover In TurnoverList
                SaveRecord(_Turnover)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TurnoverList As List(Of Turnover))

            For Each _Turnover As Turnover In TurnoverList
                SaveRecord(ConnectionString, _Turnover)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Turnover As Turnover) As Guid
            Turnover.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Turnover, "upsertTurnover")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Turnover As Turnover) As Guid
            Turnover.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Turnover, "upsertTurnover")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTurnoverbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteTurnoverbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertTurnover")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertTurnover")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Turnover

            Dim _T As Turnover = Nothing

            If DR IsNot Nothing Then
                _T = New Turnover()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._tYear = GetInteger(DR("t_year"))
                    _T._tMonth = GetInteger(DR("t_month"))
                    _T._BatchCount = GetInteger(DR("batch_count"))
                    _T._NonFundedHours = GetDecimal(DR("non_funded_hours"))
                    _T._NonFundedValue = GetDecimal(DR("non_funded_value"))
                    _T._FundedHours = GetDecimal(DR("funded_hours"))
                    _T._FundedValue = GetDecimal(DR("funded_value"))
                    _T._AdditionalValue = GetDecimal(DR("additional_value"))
                    _T._OtherValue = GetDecimal(DR("other_value"))
                    _T._BoltOns = GetDecimal(DR("bolt_ons"))
                    _T._SubTotal = GetDecimal(DR("sub_total"))
                    _T._Discount = GetDecimal(DR("discount"))
                    _T._GrandTotal = GetDecimal(DR("grand_total"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Turnover)

            Dim _TurnoverList As New List(Of Turnover)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TurnoverList.Add(PropertiesFromData(_DR))
            Next

            Return _TurnoverList

        End Function


#End Region

    End Class

End Namespace
