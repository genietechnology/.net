﻿'*****************************************************
'Generated 24/09/2018 09:47:58 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Family
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Surname = ""
                ._LetterName = ""
                ._Address = ""
                ._Postcode = ""
                ._Balance = 0
                ._eInvoicing = False
                ._FinancialsId = ""
                ._OpeningBalance = 0
                ._ExclInvoicing = False
                ._ExclFinancials = False
                ._InvoiceFreq = ""
                ._InvoiceDue = ""
                ._Archived = False
                ._AccountNo = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Coordinates = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._CompanyName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Surname = ""
                ._LetterName = ""
                ._Address = ""
                ._Postcode = ""
                ._Balance = 0
                ._eInvoicing = False
                ._FinancialsId = ""
                ._OpeningBalance = 0
                ._ExclInvoicing = False
                ._ExclFinancials = False
                ._InvoiceFreq = ""
                ._InvoiceDue = ""
                ._Archived = False
                ._AccountNo = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Coordinates = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._CompanyName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String

        <StoredProcParameter("@LetterName")>
        Public Property _LetterName() As String

        <StoredProcParameter("@Address")>
        Public Property _Address() As String

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String

        <StoredProcParameter("@Balance")>
        Public Property _Balance() As Decimal

        <StoredProcParameter("@eInvoicing")>
        Public Property _eInvoicing() As Boolean

        <StoredProcParameter("@FinancialsId")>
        Public Property _FinancialsId() As String

        <StoredProcParameter("@OpeningBalance")>
        Public Property _OpeningBalance() As Decimal

        <StoredProcParameter("@ExclInvoicing")>
        Public Property _ExclInvoicing() As Boolean

        <StoredProcParameter("@ExclFinancials")>
        Public Property _ExclFinancials() As Boolean

        <StoredProcParameter("@InvoiceFreq")>
        Public Property _InvoiceFreq() As String

        <StoredProcParameter("@InvoiceDue")>
        Public Property _InvoiceDue() As String

        <StoredProcParameter("@Archived")>
        Public Property _Archived() As Boolean

        <StoredProcParameter("@AccountNo")>
        Public Property _AccountNo() As String

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@Coordinates")>
        Public Property _Coordinates() As String

        <StoredProcParameter("@Ref1")>
        Public Property _Ref1() As String

        <StoredProcParameter("@Ref2")>
        Public Property _Ref2() As String

        <StoredProcParameter("@Ref3")>
        Public Property _Ref3() As String

        <StoredProcParameter("@CompanyName")>
        Public Property _CompanyName() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Family
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getFamilybyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Family
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getFamilybyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Family)
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getFamilyTable"))
            Return _FamilyList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Family)
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getFamilyTable"))
            Return _FamilyList
        End Function

        Public Shared Sub SaveAll(ByVal FamilyList As List(Of Family))
            For Each _Family As Family In FamilyList
                SaveRecord(_Family)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal FamilyList As List(Of Family))
            For Each _Family As Family In FamilyList
                SaveRecord(ConnectionString, _Family)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Family As Family) As Guid
            Dim _Current As Family = RetreiveByID(Family._ID.Value)
            Family.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Family, _Current, "upsertFamily")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Family As Family) As Guid
            Dim _Current As Family = RetreiveByID(ConnectionString, Family._ID.Value)
            Family.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Family, _Current, "upsertFamily")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Family = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteFamilybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Family = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteFamilybyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Family = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertFamily")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Family = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertFamily")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Family

            Dim _F As Family = Nothing

            If DR IsNot Nothing Then
                _F = New Family()
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._Surname = DR("surname").ToString.Trim
                    _F._LetterName = DR("letter_name").ToString.Trim
                    _F._Address = DR("address").ToString.Trim
                    _F._Postcode = DR("postcode").ToString.Trim
                    _F._Balance = GetDecimal(DR("balance"))
                    _F._eInvoicing = GetBoolean(DR("e_invoicing"))
                    _F._FinancialsId = DR("financials_id").ToString.Trim
                    _F._OpeningBalance = GetDecimal(DR("opening_balance"))
                    _F._ExclInvoicing = GetBoolean(DR("excl_invoicing"))
                    _F._ExclFinancials = GetBoolean(DR("excl_financials"))
                    _F._InvoiceFreq = DR("invoice_freq").ToString.Trim
                    _F._InvoiceDue = DR("invoice_due").ToString.Trim
                    _F._Archived = GetBoolean(DR("archived"))
                    _F._AccountNo = DR("account_no").ToString.Trim
                    _F._SiteId = GetGUID(DR("site_id"))
                    _F._SiteName = DR("site_name").ToString.Trim
                    _F._Coordinates = DR("coordinates").ToString.Trim
                    _F._Ref1 = DR("ref_1").ToString.Trim
                    _F._Ref2 = DR("ref_2").ToString.Trim
                    _F._Ref3 = DR("ref_3").ToString.Trim
                    _F._CompanyName = DR("company_name").ToString.Trim

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Family)

            Dim _FamilyList As New List(Of Family)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _FamilyList.Add(PropertiesFromData(_DR))
            Next

            Return _FamilyList

        End Function


#End Region

    End Class

End Namespace
