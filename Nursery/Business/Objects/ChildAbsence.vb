﻿'*****************************************************
'Generated 22/07/2018 15:59:44 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildAbsence
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_AbsType As String
        Dim m_AbsSource As String
        Dim m_AbsFrom As Date?
        Dim m_AbsTo As Date?
        Dim m_AbsReason As String
        Dim m_AbsNotes As String
        Dim m_AbsActivity As Guid?
        Dim m_AbsStaffId As Guid?
        Dim m_AbsStaffName As String
        Dim m_AbsStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._AbsType = ""
                ._AbsSource = ""
                ._AbsFrom = Nothing
                ._AbsTo = Nothing
                ._AbsReason = ""
                ._AbsNotes = ""
                ._AbsActivity = Nothing
                ._AbsStaffId = Nothing
                ._AbsStaffName = ""
                ._AbsStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._AbsType = ""
                ._AbsSource = ""
                ._AbsFrom = Nothing
                ._AbsTo = Nothing
                ._AbsReason = ""
                ._AbsNotes = ""
                ._AbsActivity = Nothing
                ._AbsStaffId = Nothing
                ._AbsStaffName = ""
                ._AbsStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@AbsType")>
        Public Property _AbsType() As String
            Get
                Return m_AbsType
            End Get
            Set(ByVal value As String)
                m_AbsType = value
            End Set
        End Property

        <StoredProcParameter("@AbsSource")>
        Public Property _AbsSource() As String
            Get
                Return m_AbsSource
            End Get
            Set(ByVal value As String)
                m_AbsSource = value
            End Set
        End Property

        <StoredProcParameter("@AbsFrom")>
        Public Property _AbsFrom() As Date?
            Get
                Return m_AbsFrom
            End Get
            Set(ByVal value As Date?)
                m_AbsFrom = value
            End Set
        End Property

        <StoredProcParameter("@AbsTo")>
        Public Property _AbsTo() As Date?
            Get
                Return m_AbsTo
            End Get
            Set(ByVal value As Date?)
                m_AbsTo = value
            End Set
        End Property

        <StoredProcParameter("@AbsReason")>
        Public Property _AbsReason() As String
            Get
                Return m_AbsReason
            End Get
            Set(ByVal value As String)
                m_AbsReason = value
            End Set
        End Property

        <StoredProcParameter("@AbsNotes")>
        Public Property _AbsNotes() As String
            Get
                Return m_AbsNotes
            End Get
            Set(ByVal value As String)
                m_AbsNotes = value
            End Set
        End Property

        <StoredProcParameter("@AbsActivity")>
        Public Property _AbsActivity() As Guid?
            Get
                Return m_AbsActivity
            End Get
            Set(ByVal value As Guid?)
                m_AbsActivity = value
            End Set
        End Property

        <StoredProcParameter("@AbsStaffId")>
        Public Property _AbsStaffId() As Guid?
            Get
                Return m_AbsStaffId
            End Get
            Set(ByVal value As Guid?)
                m_AbsStaffId = value
            End Set
        End Property

        <StoredProcParameter("@AbsStaffName")>
        Public Property _AbsStaffName() As String
            Get
                Return m_AbsStaffName
            End Get
            Set(ByVal value As String)
                m_AbsStaffName = value
            End Set
        End Property

        <StoredProcParameter("@AbsStamp")>
        Public Property _AbsStamp() As DateTime?
            Get
                Return m_AbsStamp
            End Get
            Set(ByVal value As DateTime?)
                m_AbsStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildAbsence

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildAbsencebyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ChildAbsence

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getChildAbsencebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildAbsence)

            Dim _ChildAbsenceList As List(Of ChildAbsence)
            _ChildAbsenceList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildAbsenceTable"))
            Return _ChildAbsenceList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ChildAbsence)

            Dim _ChildAbsenceList As List(Of ChildAbsence)
            _ChildAbsenceList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getChildAbsenceTable"))
            Return _ChildAbsenceList

        End Function

        Public Shared Sub SaveAll(ByVal ChildAbsenceList As List(Of ChildAbsence))

            For Each _ChildAbsence As ChildAbsence In ChildAbsenceList
                SaveRecord(_ChildAbsence)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ChildAbsenceList As List(Of ChildAbsence))

            For Each _ChildAbsence As ChildAbsence In ChildAbsenceList
                SaveRecord(ConnectionString, _ChildAbsence)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildAbsence As ChildAbsence) As Guid
            ChildAbsence.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildAbsence, "upsertChildAbsence")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ChildAbsence As ChildAbsence) As Guid
            ChildAbsence.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildAbsence, "upsertChildAbsence")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildAbsencebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteChildAbsencebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildAbsence")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildAbsence")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildAbsence

            Dim _C As ChildAbsence = Nothing

            If DR IsNot Nothing Then
                _C = New ChildAbsence()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._AbsType = DR("abs_type").ToString.Trim
                    _C._AbsSource = DR("abs_source").ToString.Trim
                    _C._AbsFrom = GetDate(DR("abs_from"))
                    _C._AbsTo = GetDate(DR("abs_to"))
                    _C._AbsReason = DR("abs_reason").ToString.Trim
                    _C._AbsNotes = DR("abs_notes").ToString.Trim
                    _C._AbsActivity = GetGUID(DR("abs_activity"))
                    _C._AbsStaffId = GetGUID(DR("abs_staff_id"))
                    _C._AbsStaffName = DR("abs_staff_name").ToString.Trim
                    _C._AbsStamp = GetDateTime(DR("abs_stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildAbsence)

            Dim _ChildAbsenceList As New List(Of ChildAbsence)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildAbsenceList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildAbsenceList

        End Function


#End Region

    End Class

End Namespace
