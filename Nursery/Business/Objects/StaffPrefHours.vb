﻿'*****************************************************
'Generated 25/03/2017 18:26:21 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffPrefHour
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_StaffId As Guid?
        Dim m_Wc As Date?
        Dim m_RoomId As Guid?
        Dim m_CostPh As Decimal
        Dim m_HoursMin As Decimal
        Dim m_HoursMax As Decimal
        Dim m_MonStart As TimeSpan?
        Dim m_MonFinish As TimeSpan?
        Dim m_MonHours As Decimal
        Dim m_MonCost As Decimal
        Dim m_TueStart As TimeSpan?
        Dim m_TueFinish As TimeSpan?
        Dim m_TueHours As Decimal
        Dim m_TueCost As Decimal
        Dim m_WedStart As TimeSpan?
        Dim m_WedFinish As TimeSpan?
        Dim m_WedHours As Decimal
        Dim m_WedCost As Decimal
        Dim m_ThuStart As TimeSpan?
        Dim m_ThuFinish As TimeSpan?
        Dim m_ThuHours As Decimal
        Dim m_ThuCost As Decimal
        Dim m_FriStart As TimeSpan?
        Dim m_FriFinish As TimeSpan?
        Dim m_FriHours As Decimal
        Dim m_FriCost As Decimal
        Dim m_SatStart As TimeSpan?
        Dim m_SatFinish As TimeSpan?
        Dim m_SatHours As Decimal
        Dim m_SatCost As Decimal
        Dim m_SunStart As TimeSpan?
        Dim m_SunFinish As TimeSpan?
        Dim m_SunHours As Decimal
        Dim m_SunCost As Decimal
        Dim m_TotalHours As Decimal
        Dim m_TotalCost As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._StaffId = Nothing
                ._Wc = Nothing
                ._RoomId = Nothing
                ._CostPh = 0
                ._HoursMin = 0
                ._HoursMax = 0
                ._MonStart = Nothing
                ._MonFinish = Nothing
                ._MonHours = 0
                ._MonCost = 0
                ._TueStart = Nothing
                ._TueFinish = Nothing
                ._TueHours = 0
                ._TueCost = 0
                ._WedStart = Nothing
                ._WedFinish = Nothing
                ._WedHours = 0
                ._WedCost = 0
                ._ThuStart = Nothing
                ._ThuFinish = Nothing
                ._ThuHours = 0
                ._ThuCost = 0
                ._FriStart = Nothing
                ._FriFinish = Nothing
                ._FriHours = 0
                ._FriCost = 0
                ._SatStart = Nothing
                ._SatFinish = Nothing
                ._SatHours = 0
                ._SatCost = 0
                ._SunStart = Nothing
                ._SunFinish = Nothing
                ._SunHours = 0
                ._SunCost = 0
                ._TotalHours = 0
                ._TotalCost = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._StaffId = Nothing
                ._Wc = Nothing
                ._RoomId = Nothing
                ._CostPh = 0
                ._HoursMin = 0
                ._HoursMax = 0
                ._MonStart = Nothing
                ._MonFinish = Nothing
                ._MonHours = 0
                ._MonCost = 0
                ._TueStart = Nothing
                ._TueFinish = Nothing
                ._TueHours = 0
                ._TueCost = 0
                ._WedStart = Nothing
                ._WedFinish = Nothing
                ._WedHours = 0
                ._WedCost = 0
                ._ThuStart = Nothing
                ._ThuFinish = Nothing
                ._ThuHours = 0
                ._ThuCost = 0
                ._FriStart = Nothing
                ._FriFinish = Nothing
                ._FriHours = 0
                ._FriCost = 0
                ._SatStart = Nothing
                ._SatFinish = Nothing
                ._SatHours = 0
                ._SatCost = 0
                ._SunStart = Nothing
                ._SunFinish = Nothing
                ._SunHours = 0
                ._SunCost = 0
                ._TotalHours = 0
                ._TotalCost = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@Wc")> _
        Public Property _Wc() As Date?
            Get
                Return m_Wc
            End Get
            Set(ByVal value As Date?)
                m_Wc = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@CostPh")> _
        Public Property _CostPh() As Decimal
            Get
                Return m_CostPh
            End Get
            Set(ByVal value As Decimal)
                m_CostPh = value
            End Set
        End Property

        <StoredProcParameter("@HoursMin")> _
        Public Property _HoursMin() As Decimal
            Get
                Return m_HoursMin
            End Get
            Set(ByVal value As Decimal)
                m_HoursMin = value
            End Set
        End Property

        <StoredProcParameter("@HoursMax")> _
        Public Property _HoursMax() As Decimal
            Get
                Return m_HoursMax
            End Get
            Set(ByVal value As Decimal)
                m_HoursMax = value
            End Set
        End Property

        <StoredProcParameter("@MonStart")> _
        Public Property _MonStart() As TimeSpan?
            Get
                Return m_MonStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_MonStart = value
            End Set
        End Property

        <StoredProcParameter("@MonFinish")> _
        Public Property _MonFinish() As TimeSpan?
            Get
                Return m_MonFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_MonFinish = value
            End Set
        End Property

        <StoredProcParameter("@MonHours")> _
        Public Property _MonHours() As Decimal
            Get
                Return m_MonHours
            End Get
            Set(ByVal value As Decimal)
                m_MonHours = value
            End Set
        End Property

        <StoredProcParameter("@MonCost")> _
        Public Property _MonCost() As Decimal
            Get
                Return m_MonCost
            End Get
            Set(ByVal value As Decimal)
                m_MonCost = value
            End Set
        End Property

        <StoredProcParameter("@TueStart")> _
        Public Property _TueStart() As TimeSpan?
            Get
                Return m_TueStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_TueStart = value
            End Set
        End Property

        <StoredProcParameter("@TueFinish")> _
        Public Property _TueFinish() As TimeSpan?
            Get
                Return m_TueFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_TueFinish = value
            End Set
        End Property

        <StoredProcParameter("@TueHours")> _
        Public Property _TueHours() As Decimal
            Get
                Return m_TueHours
            End Get
            Set(ByVal value As Decimal)
                m_TueHours = value
            End Set
        End Property

        <StoredProcParameter("@TueCost")> _
        Public Property _TueCost() As Decimal
            Get
                Return m_TueCost
            End Get
            Set(ByVal value As Decimal)
                m_TueCost = value
            End Set
        End Property

        <StoredProcParameter("@WedStart")> _
        Public Property _WedStart() As TimeSpan?
            Get
                Return m_WedStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_WedStart = value
            End Set
        End Property

        <StoredProcParameter("@WedFinish")> _
        Public Property _WedFinish() As TimeSpan?
            Get
                Return m_WedFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_WedFinish = value
            End Set
        End Property

        <StoredProcParameter("@WedHours")> _
        Public Property _WedHours() As Decimal
            Get
                Return m_WedHours
            End Get
            Set(ByVal value As Decimal)
                m_WedHours = value
            End Set
        End Property

        <StoredProcParameter("@WedCost")> _
        Public Property _WedCost() As Decimal
            Get
                Return m_WedCost
            End Get
            Set(ByVal value As Decimal)
                m_WedCost = value
            End Set
        End Property

        <StoredProcParameter("@ThuStart")> _
        Public Property _ThuStart() As TimeSpan?
            Get
                Return m_ThuStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_ThuStart = value
            End Set
        End Property

        <StoredProcParameter("@ThuFinish")> _
        Public Property _ThuFinish() As TimeSpan?
            Get
                Return m_ThuFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_ThuFinish = value
            End Set
        End Property

        <StoredProcParameter("@ThuHours")> _
        Public Property _ThuHours() As Decimal
            Get
                Return m_ThuHours
            End Get
            Set(ByVal value As Decimal)
                m_ThuHours = value
            End Set
        End Property

        <StoredProcParameter("@ThuCost")> _
        Public Property _ThuCost() As Decimal
            Get
                Return m_ThuCost
            End Get
            Set(ByVal value As Decimal)
                m_ThuCost = value
            End Set
        End Property

        <StoredProcParameter("@FriStart")> _
        Public Property _FriStart() As TimeSpan?
            Get
                Return m_FriStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_FriStart = value
            End Set
        End Property

        <StoredProcParameter("@FriFinish")> _
        Public Property _FriFinish() As TimeSpan?
            Get
                Return m_FriFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_FriFinish = value
            End Set
        End Property

        <StoredProcParameter("@FriHours")> _
        Public Property _FriHours() As Decimal
            Get
                Return m_FriHours
            End Get
            Set(ByVal value As Decimal)
                m_FriHours = value
            End Set
        End Property

        <StoredProcParameter("@FriCost")> _
        Public Property _FriCost() As Decimal
            Get
                Return m_FriCost
            End Get
            Set(ByVal value As Decimal)
                m_FriCost = value
            End Set
        End Property

        <StoredProcParameter("@SatStart")> _
        Public Property _SatStart() As TimeSpan?
            Get
                Return m_SatStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_SatStart = value
            End Set
        End Property

        <StoredProcParameter("@SatFinish")> _
        Public Property _SatFinish() As TimeSpan?
            Get
                Return m_SatFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_SatFinish = value
            End Set
        End Property

        <StoredProcParameter("@SatHours")> _
        Public Property _SatHours() As Decimal
            Get
                Return m_SatHours
            End Get
            Set(ByVal value As Decimal)
                m_SatHours = value
            End Set
        End Property

        <StoredProcParameter("@SatCost")> _
        Public Property _SatCost() As Decimal
            Get
                Return m_SatCost
            End Get
            Set(ByVal value As Decimal)
                m_SatCost = value
            End Set
        End Property

        <StoredProcParameter("@SunStart")> _
        Public Property _SunStart() As TimeSpan?
            Get
                Return m_SunStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_SunStart = value
            End Set
        End Property

        <StoredProcParameter("@SunFinish")> _
        Public Property _SunFinish() As TimeSpan?
            Get
                Return m_SunFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_SunFinish = value
            End Set
        End Property

        <StoredProcParameter("@SunHours")> _
        Public Property _SunHours() As Decimal
            Get
                Return m_SunHours
            End Get
            Set(ByVal value As Decimal)
                m_SunHours = value
            End Set
        End Property

        <StoredProcParameter("@SunCost")> _
        Public Property _SunCost() As Decimal
            Get
                Return m_SunCost
            End Get
            Set(ByVal value As Decimal)
                m_SunCost = value
            End Set
        End Property

        <StoredProcParameter("@TotalHours")> _
        Public Property _TotalHours() As Decimal
            Get
                Return m_TotalHours
            End Get
            Set(ByVal value As Decimal)
                m_TotalHours = value
            End Set
        End Property

        <StoredProcParameter("@TotalCost")> _
        Public Property _TotalCost() As Decimal
            Get
                Return m_TotalCost
            End Get
            Set(ByVal value As Decimal)
                m_TotalCost = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffPrefHour

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffPrefHourbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As StaffPrefHour

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getStaffPrefHourbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffPrefHour)

            Dim _StaffPrefHourList As List(Of StaffPrefHour)
            _StaffPrefHourList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffPrefHourTable"))
            Return _StaffPrefHourList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of StaffPrefHour)

            Dim _StaffPrefHourList As List(Of StaffPrefHour)
            _StaffPrefHourList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getStaffPrefHourTable"))
            Return _StaffPrefHourList

        End Function

        Public Shared Sub SaveAll(ByVal StaffPrefHourList As List(Of StaffPrefHour))

            For Each _StaffPrefHour As StaffPrefHour In StaffPrefHourList
                SaveRecord(_StaffPrefHour)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal StaffPrefHourList As List(Of StaffPrefHour))

            For Each _StaffPrefHour As StaffPrefHour In StaffPrefHourList
                SaveRecord(ConnectionString, _StaffPrefHour)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffPrefHour As StaffPrefHour) As Guid
            StaffPrefHour.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffPrefHour, "upsertStaffPrefHour")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal StaffPrefHour As StaffPrefHour) As Guid
            StaffPrefHour.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffPrefHour, "upsertStaffPrefHour")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffPrefHourbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteStaffPrefHourbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffPrefHour")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffPrefHour")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffPrefHour

            Dim _S As StaffPrefHour = Nothing

            If DR IsNot Nothing Then
                _S = New StaffPrefHour()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._StaffId = GetGUID(DR("staff_id"))
                    _S._Wc = GetDate(DR("wc"))
                    _S._RoomId = GetGUID(DR("room_id"))
                    _S._CostPh = GetDecimal(DR("cost_ph"))
                    _S._HoursMin = GetDecimal(DR("hours_min"))
                    _S._HoursMax = GetDecimal(DR("hours_max"))
                    _S._MonStart = GetTimeSpan(DR("mon_start"))
                    _S._MonFinish = GetTimeSpan(DR("mon_finish"))
                    _S._MonHours = GetDecimal(DR("mon_hours"))
                    _S._MonCost = GetDecimal(DR("mon_cost"))
                    _S._TueStart = GetTimeSpan(DR("tue_start"))
                    _S._TueFinish = GetTimeSpan(DR("tue_finish"))
                    _S._TueHours = GetDecimal(DR("tue_hours"))
                    _S._TueCost = GetDecimal(DR("tue_cost"))
                    _S._WedStart = GetTimeSpan(DR("wed_start"))
                    _S._WedFinish = GetTimeSpan(DR("wed_finish"))
                    _S._WedHours = GetDecimal(DR("wed_hours"))
                    _S._WedCost = GetDecimal(DR("wed_cost"))
                    _S._ThuStart = GetTimeSpan(DR("thu_start"))
                    _S._ThuFinish = GetTimeSpan(DR("thu_finish"))
                    _S._ThuHours = GetDecimal(DR("thu_hours"))
                    _S._ThuCost = GetDecimal(DR("thu_cost"))
                    _S._FriStart = GetTimeSpan(DR("fri_start"))
                    _S._FriFinish = GetTimeSpan(DR("fri_finish"))
                    _S._FriHours = GetDecimal(DR("fri_hours"))
                    _S._FriCost = GetDecimal(DR("fri_cost"))
                    _S._SatStart = GetTimeSpan(DR("sat_start"))
                    _S._SatFinish = GetTimeSpan(DR("sat_finish"))
                    _S._SatHours = GetDecimal(DR("sat_hours"))
                    _S._SatCost = GetDecimal(DR("sat_cost"))
                    _S._SunStart = GetTimeSpan(DR("sun_start"))
                    _S._SunFinish = GetTimeSpan(DR("sun_finish"))
                    _S._SunHours = GetDecimal(DR("sun_hours"))
                    _S._SunCost = GetDecimal(DR("sun_cost"))
                    _S._TotalHours = GetDecimal(DR("total_hours"))
                    _S._TotalCost = GetDecimal(DR("total_cost"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffPrefHour)

            Dim _StaffPrefHourList As New List(Of StaffPrefHour)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffPrefHourList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffPrefHourList

        End Function


#End Region

    End Class

End Namespace
