﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffClockIn
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Machine As String
        Dim m_TranDate As Date?
        Dim m_TranTime As DateTime?
        Dim m_Staff As String
        Dim m_InOut As String
        Dim m_Verify As String
        Dim m_Workcode As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Machine = ""
                ._TranDate = Nothing
                ._TranTime = Nothing
                ._Staff = ""
                ._InOut = ""
                ._Verify = ""
                ._Workcode = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Machine = ""
                ._TranDate = Nothing
                ._TranTime = Nothing
                ._Staff = ""
                ._InOut = ""
                ._Verify = ""
                ._Workcode = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Machine")> _
        Public Property _Machine() As String
            Get
                Return m_Machine
            End Get
            Set(ByVal value As String)
                m_Machine = value
            End Set
        End Property

        <StoredProcParameter("@TranDate")> _
        Public Property _TranDate() As Date?
            Get
                Return m_TranDate
            End Get
            Set(ByVal value As Date?)
                m_TranDate = value
            End Set
        End Property

        <StoredProcParameter("@TranTime")> _
        Public Property _TranTime() As DateTime?
            Get
                Return m_TranTime
            End Get
            Set(ByVal value As DateTime?)
                m_TranTime = value
            End Set
        End Property

        <StoredProcParameter("@Staff")> _
        Public Property _Staff() As String
            Get
                Return m_Staff
            End Get
            Set(ByVal value As String)
                m_Staff = value
            End Set
        End Property

        <StoredProcParameter("@InOut")> _
        Public Property _InOut() As String
            Get
                Return m_InOut
            End Get
            Set(ByVal value As String)
                m_InOut = value
            End Set
        End Property

        <StoredProcParameter("@Verify")> _
        Public Property _Verify() As String
            Get
                Return m_Verify
            End Get
            Set(ByVal value As String)
                m_Verify = value
            End Set
        End Property

        <StoredProcParameter("@Workcode")> _
        Public Property _Workcode() As String
            Get
                Return m_Workcode
            End Get
            Set(ByVal value As String)
                m_Workcode = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffClockIn

            Dim _StaffClockIn As StaffClockIn
            _StaffClockIn = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffClockInbyID", ID))
            Return _StaffClockIn

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffClockIn)

            Dim _StaffClockInList As List(Of StaffClockIn)
            _StaffClockInList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffClockInTable"))
            Return _StaffClockInList

        End Function

        Public Shared Sub SaveAll(ByVal StaffClockInList As List(Of StaffClockIn))

            For Each _StaffClockIn As StaffClockIn In StaffClockInList
                SaveRecord(_StaffClockIn)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffClockIn As StaffClockIn) As Guid
            DAL.SaveRecord(Session.ConnectionManager, StaffClockIn, "upsertStaffClockIn")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffClockInbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffClockIn")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffClockIn

            Dim _s As New StaffClockIn()

            If DR IsNot Nothing Then
                With DR
                    _s.IsNew = False
                    _s.IsDeleted = False
                    _s._ID = GetGUID(DR("ID"))
                    _s._Machine = DR("machine").ToString.Trim
                    _s._TranDate = GetDate(DR("tran_date"))
                    _s._TranTime = GetDateTime(DR("tran_time"))
                    _s._Staff = DR("staff").ToString.Trim
                    _s._InOut = DR("in_out").ToString.Trim
                    _s._Verify = DR("verify").ToString.Trim
                    _s._Workcode = DR("workcode").ToString.Trim

                End With
            End If

            Return _s

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffClockIn)

            Dim _StaffClockInList As New List(Of StaffClockIn)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffClockInList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffClockInList

        End Function


#End Region

    End Class

End Namespace
