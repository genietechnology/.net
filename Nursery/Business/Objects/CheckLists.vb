﻿'*****************************************************
'Generated 24/05/2017 17:19:06 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CheckList
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChkName As String
        Dim m_ChkStatus As String
        Dim m_ChkScope As String
        Dim m_ChkScopeSiteId As Guid?
        Dim m_ChkScopeSiteName As String
        Dim m_ChkFreq As String
        Dim m_ChkFreqDays As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChkName = ""
                ._ChkStatus = ""
                ._ChkScope = ""
                ._ChkScopeSiteId = Nothing
                ._ChkScopeSiteName = ""
                ._ChkFreq = ""
                ._ChkFreqDays = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChkName = ""
                ._ChkStatus = ""
                ._ChkScope = ""
                ._ChkScopeSiteId = Nothing
                ._ChkScopeSiteName = ""
                ._ChkFreq = ""
                ._ChkFreqDays = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChkName")>
        Public Property _ChkName() As String
            Get
                Return m_ChkName
            End Get
            Set(ByVal value As String)
                m_ChkName = value
            End Set
        End Property

        <StoredProcParameter("@ChkStatus")>
        Public Property _ChkStatus() As String
            Get
                Return m_ChkStatus
            End Get
            Set(ByVal value As String)
                m_ChkStatus = value
            End Set
        End Property

        <StoredProcParameter("@ChkScope")>
        Public Property _ChkScope() As String
            Get
                Return m_ChkScope
            End Get
            Set(ByVal value As String)
                m_ChkScope = value
            End Set
        End Property

        <StoredProcParameter("@ChkScopeSiteId")>
        Public Property _ChkScopeSiteId() As Guid?
            Get
                Return m_ChkScopeSiteId
            End Get
            Set(ByVal value As Guid?)
                m_ChkScopeSiteId = value
            End Set
        End Property

        <StoredProcParameter("@ChkScopeSiteName")>
        Public Property _ChkScopeSiteName() As String
            Get
                Return m_ChkScopeSiteName
            End Get
            Set(ByVal value As String)
                m_ChkScopeSiteName = value
            End Set
        End Property

        <StoredProcParameter("@ChkFreq")>
        Public Property _ChkFreq() As String
            Get
                Return m_ChkFreq
            End Get
            Set(ByVal value As String)
                m_ChkFreq = value
            End Set
        End Property

        <StoredProcParameter("@ChkFreqDays")>
        Public Property _ChkFreqDays() As Integer
            Get
                Return m_ChkFreqDays
            End Get
            Set(ByVal value As Integer)
                m_ChkFreqDays = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CheckList

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCheckListbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CheckList

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCheckListbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CheckList)

            Dim _CheckListList As List(Of CheckList)
            _CheckListList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCheckListTable"))
            Return _CheckListList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CheckList)

            Dim _CheckListList As List(Of CheckList)
            _CheckListList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCheckListTable"))
            Return _CheckListList

        End Function

        Public Shared Sub SaveAll(ByVal CheckListList As List(Of CheckList))

            For Each _CheckList As CheckList In CheckListList
                SaveRecord(_CheckList)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CheckListList As List(Of CheckList))

            For Each _CheckList As CheckList In CheckListList
                SaveRecord(ConnectionString, _CheckList)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CheckList As CheckList) As Guid
            CheckList.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CheckList, "upsertCheckList")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CheckList As CheckList) As Guid
            CheckList.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, CheckList, "upsertCheckList")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCheckListbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCheckListbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCheckList")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCheckList")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CheckList

            Dim _C As CheckList = Nothing

            If DR IsNot Nothing Then
                _C = New CheckList()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChkName = DR("chk_name").ToString.Trim
                    _C._ChkStatus = DR("chk_status").ToString.Trim
                    _C._ChkScope = DR("chk_scope").ToString.Trim
                    _C._ChkScopeSiteId = GetGUID(DR("chk_scope_site_id"))
                    _C._ChkScopeSiteName = DR("chk_scope_site_name").ToString.Trim
                    _C._ChkFreq = DR("chk_freq").ToString.Trim
                    _C._ChkFreqDays = GetInteger(DR("chk_freq_days"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CheckList)

            Dim _CheckListList As New List(Of CheckList)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CheckListList.Add(PropertiesFromData(_DR))
            Next

            Return _CheckListList

        End Function


#End Region

    End Class

End Namespace
