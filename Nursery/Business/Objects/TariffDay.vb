﻿'*****************************************************
'Generated 24/09/2018 09:53:08 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TariffDay
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TariffId = Nothing
                ._AgeId = Nothing
                ._InvoiceText = ""
                ._Days1 = 0
                ._Days2 = 0
                ._Days3 = 0
                ._Days4 = 0
                ._Days5 = 0
                ._Days6 = 0
                ._Days7 = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TariffId = Nothing
                ._AgeId = Nothing
                ._InvoiceText = ""
                ._Days1 = 0
                ._Days2 = 0
                ._Days3 = 0
                ._Days4 = 0
                ._Days5 = 0
                ._Days6 = 0
                ._Days7 = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@TariffId")>
        Public Property _TariffId() As Guid?

        <StoredProcParameter("@AgeId")>
        Public Property _AgeId() As Guid?

        <StoredProcParameter("@InvoiceText")>
        Public Property _InvoiceText() As String

        <StoredProcParameter("@Days1")>
        Public Property _Days1() As Decimal

        <StoredProcParameter("@Days2")>
        Public Property _Days2() As Decimal

        <StoredProcParameter("@Days3")>
        Public Property _Days3() As Decimal

        <StoredProcParameter("@Days4")>
        Public Property _Days4() As Decimal

        <StoredProcParameter("@Days5")>
        Public Property _Days5() As Decimal

        <StoredProcParameter("@Days6")>
        Public Property _Days6() As Decimal

        <StoredProcParameter("@Days7")>
        Public Property _Days7() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TariffDay
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffDaybyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TariffDay
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffDaybyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of TariffDay)
            Dim _TariffDayList As List(Of TariffDay)
            _TariffDayList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffDayTable"))
            Return _TariffDayList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TariffDay)
            Dim _TariffDayList As List(Of TariffDay)
            _TariffDayList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffDayTable"))
            Return _TariffDayList
        End Function

        Public Shared Sub SaveAll(ByVal TariffDayList As List(Of TariffDay))
            For Each _TariffDay As TariffDay In TariffDayList
                SaveRecord(_TariffDay)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffDayList As List(Of TariffDay))
            For Each _TariffDay As TariffDay In TariffDayList
                SaveRecord(ConnectionString, _TariffDay)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal TariffDay As TariffDay) As Guid
            Dim _Current As TariffDay = RetreiveByID(TariffDay._ID.Value)
            TariffDay.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffDay, _Current, "upsertTariffDay")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TariffDay As TariffDay) As Guid
            Dim _Current As TariffDay = RetreiveByID(ConnectionString, TariffDay._ID.Value)
            TariffDay.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffDay, _Current, "upsertTariffDay")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As TariffDay = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffDaybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As TariffDay = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffDaybyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As TariffDay = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffDay")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As TariffDay = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffDay")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TariffDay

            Dim _T As TariffDay = Nothing

            If DR IsNot Nothing Then
                _T = New TariffDay()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._TariffId = GetGUID(DR("tariff_id"))
                    _T._AgeId = GetGUID(DR("age_id"))
                    _T._InvoiceText = DR("invoice_text").ToString.Trim
                    _T._Days1 = GetDecimal(DR("days_1"))
                    _T._Days2 = GetDecimal(DR("days_2"))
                    _T._Days3 = GetDecimal(DR("days_3"))
                    _T._Days4 = GetDecimal(DR("days_4"))
                    _T._Days5 = GetDecimal(DR("days_5"))
                    _T._Days6 = GetDecimal(DR("days_6"))
                    _T._Days7 = GetDecimal(DR("days_7"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TariffDay)

            Dim _TariffDayList As New List(Of TariffDay)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffDayList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffDayList

        End Function


#End Region

    End Class

End Namespace
