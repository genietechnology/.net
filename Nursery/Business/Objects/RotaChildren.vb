﻿'*****************************************************
'Generated 12/05/2017 14:54:10 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RotaChildren
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RcRotaId As Guid?
        Dim m_RcDate As Date?
        Dim m_RcRatioId As Guid?
        Dim m_RcRoom As String
        Dim m_RcStart As TimeSpan?
        Dim m_RcFinish As TimeSpan?
        Dim m_RcHours As Decimal
        Dim m_RcChildId As Guid?
        Dim m_RcChildName As String
        Dim m_RcChildDob As Date?
        Dim m_RcChildAge As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RcRotaId = Nothing
                ._RcDate = Nothing
                ._RcRatioId = Nothing
                ._RcRoom = ""
                ._RcStart = Nothing
                ._RcFinish = Nothing
                ._RcHours = 0
                ._RcChildId = Nothing
                ._RcChildName = ""
                ._RcChildDob = Nothing
                ._RcChildAge = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RcRotaId = Nothing
                ._RcDate = Nothing
                ._RcRatioId = Nothing
                ._RcRoom = ""
                ._RcStart = Nothing
                ._RcFinish = Nothing
                ._RcHours = 0
                ._RcChildId = Nothing
                ._RcChildName = ""
                ._RcChildDob = Nothing
                ._RcChildAge = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RcRotaId")>
        Public Property _RcRotaId() As Guid?
            Get
                Return m_RcRotaId
            End Get
            Set(ByVal value As Guid?)
                m_RcRotaId = value
            End Set
        End Property

        <StoredProcParameter("@RcDate")>
        Public Property _RcDate() As Date?
            Get
                Return m_RcDate
            End Get
            Set(ByVal value As Date?)
                m_RcDate = value
            End Set
        End Property

        <StoredProcParameter("@RcRatioId")>
        Public Property _RcRatioId() As Guid?
            Get
                Return m_RcRatioId
            End Get
            Set(ByVal value As Guid?)
                m_RcRatioId = value
            End Set
        End Property

        <StoredProcParameter("@RcRoom")>
        Public Property _RcRoom() As String
            Get
                Return m_RcRoom
            End Get
            Set(ByVal value As String)
                m_RcRoom = value
            End Set
        End Property

        <StoredProcParameter("@RcStart")>
        Public Property _RcStart() As TimeSpan?
            Get
                Return m_RcStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_RcStart = value
            End Set
        End Property

        <StoredProcParameter("@RcFinish")>
        Public Property _RcFinish() As TimeSpan?
            Get
                Return m_RcFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_RcFinish = value
            End Set
        End Property

        <StoredProcParameter("@RcHours")>
        Public Property _RcHours() As Decimal
            Get
                Return m_RcHours
            End Get
            Set(ByVal value As Decimal)
                m_RcHours = value
            End Set
        End Property

        <StoredProcParameter("@RcChildId")>
        Public Property _RcChildId() As Guid?
            Get
                Return m_RcChildId
            End Get
            Set(ByVal value As Guid?)
                m_RcChildId = value
            End Set
        End Property

        <StoredProcParameter("@RcChildName")>
        Public Property _RcChildName() As String
            Get
                Return m_RcChildName
            End Get
            Set(ByVal value As String)
                m_RcChildName = value
            End Set
        End Property

        <StoredProcParameter("@RcChildDob")>
        Public Property _RcChildDob() As Date?
            Get
                Return m_RcChildDob
            End Get
            Set(ByVal value As Date?)
                m_RcChildDob = value
            End Set
        End Property

        <StoredProcParameter("@RcChildAge")>
        Public Property _RcChildAge() As Integer
            Get
                Return m_RcChildAge
            End Get
            Set(ByVal value As Integer)
                m_RcChildAge = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RotaChildren

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRotaChildrenbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RotaChildren

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRotaChildrenbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RotaChildren)

            Dim _RotaChildrenList As List(Of RotaChildren)
            _RotaChildrenList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRotaChildrenTable"))
            Return _RotaChildrenList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RotaChildren)

            Dim _RotaChildrenList As List(Of RotaChildren)
            _RotaChildrenList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRotaChildrenTable"))
            Return _RotaChildrenList

        End Function

        Public Shared Sub SaveAll(ByVal RotaChildrenList As List(Of RotaChildren))

            For Each _RotaChildren As RotaChildren In RotaChildrenList
                SaveRecord(_RotaChildren)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RotaChildrenList As List(Of RotaChildren))

            For Each _RotaChildren As RotaChildren In RotaChildrenList
                SaveRecord(ConnectionString, _RotaChildren)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RotaChildren As RotaChildren) As Guid
            RotaChildren.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaChildren, "upsertRotaChildren")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RotaChildren As RotaChildren) As Guid
            RotaChildren.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaChildren, "upsertRotaChildren")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRotaChildrenbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRotaChildrenbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaChildren")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaChildren")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RotaChildren

            Dim _R As RotaChildren = Nothing

            If DR IsNot Nothing Then
                _R = New RotaChildren()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._RcRotaId = GetGUID(DR("rc_rota_id"))
                    _R._RcDate = GetDate(DR("rc_date"))
                    _R._RcRatioId = GetGUID(DR("rc_ratio_id"))
                    _R._RcRoom = DR("rc_room").ToString.Trim
                    _R._RcStart = GetTimeSpan(DR("rc_start"))
                    _R._RcFinish = GetTimeSpan(DR("rc_finish"))
                    _R._RcHours = GetDecimal(DR("rc_hours"))
                    _R._RcChildId = GetGUID(DR("rc_child_id"))
                    _R._RcChildName = DR("rc_child_name").ToString.Trim
                    _R._RcChildDob = GetDate(DR("rc_child_dob"))
                    _R._RcChildAge = GetInteger(DR("rc_child_age"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RotaChildren)

            Dim _RotaChildrenList As New List(Of RotaChildren)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RotaChildrenList.Add(PropertiesFromData(_DR))
            Next

            Return _RotaChildrenList

        End Function


#End Region

    End Class

End Namespace
