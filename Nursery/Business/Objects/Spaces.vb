﻿'*****************************************************
'Generated 22/09/2016 13:10:33 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Space
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_RoomId As Guid?
        Dim m_RatioId As Guid?
        Dim m_SpaceDate As Date?
        Dim m_Capacity As Integer
        Dim m_CapacityHrs As Decimal
        Dim m_Wl As Integer
        Dim m_WlAm As Integer
        Dim m_WlPm As Integer
        Dim m_WlHrs As Decimal
        Dim m_Bk As Integer
        Dim m_BkAm As Integer
        Dim m_BkPm As Integer
        Dim m_BkHrs As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._RoomId = Nothing
                ._RatioId = Nothing
                ._SpaceDate = Nothing
                ._Capacity = 0
                ._CapacityHrs = 0
                ._Wl = 0
                ._WlAm = 0
                ._WlPm = 0
                ._WlHrs = 0
                ._Bk = 0
                ._BkAm = 0
                ._BkPm = 0
                ._BkHrs = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._RoomId = Nothing
                ._RatioId = Nothing
                ._SpaceDate = Nothing
                ._Capacity = 0
                ._CapacityHrs = 0
                ._Wl = 0
                ._WlAm = 0
                ._WlPm = 0
                ._WlHrs = 0
                ._Bk = 0
                ._BkAm = 0
                ._BkPm = 0
                ._BkHrs = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@RatioId")> _
        Public Property _RatioId() As Guid?
            Get
                Return m_RatioId
            End Get
            Set(ByVal value As Guid?)
                m_RatioId = value
            End Set
        End Property

        <StoredProcParameter("@SpaceDate")> _
        Public Property _SpaceDate() As Date?
            Get
                Return m_SpaceDate
            End Get
            Set(ByVal value As Date?)
                m_SpaceDate = value
            End Set
        End Property

        <StoredProcParameter("@Capacity")> _
        Public Property _Capacity() As Integer
            Get
                Return m_Capacity
            End Get
            Set(ByVal value As Integer)
                m_Capacity = value
            End Set
        End Property

        <StoredProcParameter("@CapacityHrs")> _
        Public Property _CapacityHrs() As Decimal
            Get
                Return m_CapacityHrs
            End Get
            Set(ByVal value As Decimal)
                m_CapacityHrs = value
            End Set
        End Property

        <StoredProcParameter("@Wl")> _
        Public Property _Wl() As Integer
            Get
                Return m_Wl
            End Get
            Set(ByVal value As Integer)
                m_Wl = value
            End Set
        End Property

        <StoredProcParameter("@WlAm")> _
        Public Property _WlAm() As Integer
            Get
                Return m_WlAm
            End Get
            Set(ByVal value As Integer)
                m_WlAm = value
            End Set
        End Property

        <StoredProcParameter("@WlPm")> _
        Public Property _WlPm() As Integer
            Get
                Return m_WlPm
            End Get
            Set(ByVal value As Integer)
                m_WlPm = value
            End Set
        End Property

        <StoredProcParameter("@WlHrs")> _
        Public Property _WlHrs() As Decimal
            Get
                Return m_WlHrs
            End Get
            Set(ByVal value As Decimal)
                m_WlHrs = value
            End Set
        End Property

        <StoredProcParameter("@Bk")> _
        Public Property _Bk() As Integer
            Get
                Return m_Bk
            End Get
            Set(ByVal value As Integer)
                m_Bk = value
            End Set
        End Property

        <StoredProcParameter("@BkAm")> _
        Public Property _BkAm() As Integer
            Get
                Return m_BkAm
            End Get
            Set(ByVal value As Integer)
                m_BkAm = value
            End Set
        End Property

        <StoredProcParameter("@BkPm")> _
        Public Property _BkPm() As Integer
            Get
                Return m_BkPm
            End Get
            Set(ByVal value As Integer)
                m_BkPm = value
            End Set
        End Property

        <StoredProcParameter("@BkHrs")> _
        Public Property _BkHrs() As Decimal
            Get
                Return m_BkHrs
            End Get
            Set(ByVal value As Decimal)
                m_BkHrs = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Space

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSpacebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Space)

            Dim _SpaceList As List(Of Space)
            _SpaceList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSpaceTable"))
            Return _SpaceList

        End Function

        Public Shared Sub SaveAll(ByVal SpaceList As List(Of Space))

            For Each _Space As Space In SpaceList
                SaveRecord(_Space)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Space As Space) As Guid
            Space.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Space, "upsertSpace")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSpacebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSpace")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Space

            Dim _S As Space = Nothing

            If DR IsNot Nothing Then
                _S = New Space()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._SiteId = GetGUID(DR("site_id"))
                    _S._RoomId = GetGUID(DR("room_id"))
                    _S._RatioId = GetGUID(DR("ratio_id"))
                    _S._SpaceDate = GetDate(DR("space_date"))
                    _S._Capacity = GetInteger(DR("capacity"))
                    _S._CapacityHrs = GetDecimal(DR("capacity_hrs"))
                    _S._Wl = GetInteger(DR("wl"))
                    _S._WlAm = GetInteger(DR("wl_am"))
                    _S._WlPm = GetInteger(DR("wl_pm"))
                    _S._WlHrs = GetDecimal(DR("wl_hrs"))
                    _S._Bk = GetInteger(DR("bk"))
                    _S._BkAm = GetInteger(DR("bk_am"))
                    _S._BkPm = GetInteger(DR("bk_pm"))
                    _S._BkHrs = GetDecimal(DR("bk_hrs"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Space)

            Dim _SpaceList As New List(Of Space)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SpaceList.Add(PropertiesFromData(_DR))
            Next

            Return _SpaceList

        End Function


#End Region

    End Class

End Namespace
