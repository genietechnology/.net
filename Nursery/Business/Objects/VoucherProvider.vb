﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class VoucherProvider
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Reference As String
        Dim m_Address As String
        Dim m_Tel As String
        Dim m_Contact As String
        Dim m_ContactTel As String
        Dim m_ContactEmail As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Reference = ""
                ._Address = ""
                ._Tel = ""
                ._Contact = ""
                ._ContactTel = ""
                ._ContactEmail = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Reference = ""
                ._Address = ""
                ._Tel = ""
                ._Contact = ""
                ._ContactTel = ""
                ._ContactEmail = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Reference")> _
        Public Property _Reference() As String
            Get
                Return m_Reference
            End Get
            Set(ByVal value As String)
                m_Reference = value
            End Set
        End Property

        <StoredProcParameter("@Address")> _
        Public Property _Address() As String
            Get
                Return m_Address
            End Get
            Set(ByVal value As String)
                m_Address = value
            End Set
        End Property

        <StoredProcParameter("@Tel")> _
        Public Property _Tel() As String
            Get
                Return m_Tel
            End Get
            Set(ByVal value As String)
                m_Tel = value
            End Set
        End Property

        <StoredProcParameter("@Contact")> _
        Public Property _Contact() As String
            Get
                Return m_Contact
            End Get
            Set(ByVal value As String)
                m_Contact = value
            End Set
        End Property

        <StoredProcParameter("@ContactTel")> _
        Public Property _ContactTel() As String
            Get
                Return m_ContactTel
            End Get
            Set(ByVal value As String)
                m_ContactTel = value
            End Set
        End Property

        <StoredProcParameter("@ContactEmail")> _
        Public Property _ContactEmail() As String
            Get
                Return m_ContactEmail
            End Get
            Set(ByVal value As String)
                m_ContactEmail = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As VoucherProvider

            Dim _VoucherProvider As VoucherProvider
            _VoucherProvider = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getVoucherProviderbyID", ID))
            Return _VoucherProvider

        End Function

        Public Shared Function RetreiveAll() As List(Of VoucherProvider)

            Dim _VoucherProviderList As List(Of VoucherProvider)
            _VoucherProviderList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getVoucherProviderTable"))
            Return _VoucherProviderList

        End Function

        Public Shared Sub SaveAll(ByVal VoucherProviderList As List(Of VoucherProvider))

            For Each _VoucherProvider As VoucherProvider In VoucherProviderList
                SaveRecord(_VoucherProvider)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal VoucherProvider As VoucherProvider) As Guid
            DAL.SaveRecord(Session.ConnectionManager, VoucherProvider, "upsertVoucherProvider")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteVoucherProviderbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertVoucherProvider")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As VoucherProvider

            Dim _V As New VoucherProvider()

            If DR IsNot Nothing Then
                With DR
                    _V.IsNew = False
                    _V.IsDeleted = False
                    _V._ID = GetGUID(DR("ID"))
                    _V._Name = DR("name").ToString.Trim
                    _V._Reference = DR("reference").ToString.Trim
                    _V._Address = DR("address").ToString.Trim
                    _V._Tel = DR("tel").ToString.Trim
                    _V._Contact = DR("contact").ToString.Trim
                    _V._ContactTel = DR("contact_tel").ToString.Trim
                    _V._ContactEmail = DR("contact_email").ToString.Trim

                End With
            End If

            Return _V

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of VoucherProvider)

            Dim _VoucherProviderList As New List(Of VoucherProvider)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _VoucherProviderList.Add(PropertiesFromData(_DR))
            Next

            Return _VoucherProviderList

        End Function


#End Region

    End Class

End Namespace
