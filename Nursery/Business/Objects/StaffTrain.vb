﻿'*****************************************************
'Generated 05/10/2015 05:46:01 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffTrain
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_StaffId As Guid?
        Dim m_TrainFrom As Date?
        Dim m_TrainTo As Date?
        Dim m_TrainCourse As String
        Dim m_TrainVenue As String
        Dim m_TrainCost As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._StaffId = Nothing
                ._TrainFrom = Nothing
                ._TrainTo = Nothing
                ._TrainCourse = ""
                ._TrainVenue = ""
                ._TrainCost = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._StaffId = Nothing
                ._TrainFrom = Nothing
                ._TrainTo = Nothing
                ._TrainCourse = ""
                ._TrainVenue = ""
                ._TrainCost = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@TrainFrom")> _
        Public Property _TrainFrom() As Date?
            Get
                Return m_TrainFrom
            End Get
            Set(ByVal value As Date?)
                m_TrainFrom = value
            End Set
        End Property

        <StoredProcParameter("@TrainTo")> _
        Public Property _TrainTo() As Date?
            Get
                Return m_TrainTo
            End Get
            Set(ByVal value As Date?)
                m_TrainTo = value
            End Set
        End Property

        <StoredProcParameter("@TrainCourse")> _
        Public Property _TrainCourse() As String
            Get
                Return m_TrainCourse
            End Get
            Set(ByVal value As String)
                m_TrainCourse = value
            End Set
        End Property

        <StoredProcParameter("@TrainVenue")> _
        Public Property _TrainVenue() As String
            Get
                Return m_TrainVenue
            End Get
            Set(ByVal value As String)
                m_TrainVenue = value
            End Set
        End Property

        <StoredProcParameter("@TrainCost")> _
        Public Property _TrainCost() As Decimal
            Get
                Return m_TrainCost
            End Get
            Set(ByVal value As Decimal)
                m_TrainCost = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffTrain

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffTrainbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffTrain)

            Dim _StaffTrainList As List(Of StaffTrain)
            _StaffTrainList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffTrainTable"))
            Return _StaffTrainList

        End Function

        Public Shared Sub SaveAll(ByVal StaffTrainList As List(Of StaffTrain))

            For Each _StaffTrain As StaffTrain In StaffTrainList
                SaveRecord(_StaffTrain)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffTrain As StaffTrain) As Guid
            StaffTrain.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffTrain, "upsertStaffTrain")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffTrainbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffTrain")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffTrain

            Dim _S As StaffTrain = Nothing

            If DR IsNot Nothing Then
                _S = New StaffTrain()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._StaffId = GetGUID(DR("staff_id"))
                    _S._TrainFrom = GetDate(DR("train_from"))
                    _S._TrainTo = GetDate(DR("train_to"))
                    _S._TrainCourse = DR("train_course").ToString.Trim
                    _S._TrainVenue = DR("train_venue").ToString.Trim
                    _S._TrainCost = GetDecimal(DR("train_cost"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffTrain)

            Dim _StaffTrainList As New List(Of StaffTrain)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffTrainList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffTrainList

        End Function


#End Region

    End Class

End Namespace
