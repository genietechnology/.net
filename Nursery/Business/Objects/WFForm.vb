﻿'*****************************************************
'Generated 24/09/2018 09:54:14 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class WFForm
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Mode = ""
                ._FormName = ""
                ._LastEntry = 0
                ._SiteId = Nothing
                ._SiteName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Mode = ""
                ._FormName = ""
                ._LastEntry = 0
                ._SiteId = Nothing
                ._SiteName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Mode")>
        Public Property _Mode() As String

        <StoredProcParameter("@FormName")>
        Public Property _FormName() As String

        <StoredProcParameter("@LastEntry")>
        Public Property _LastEntry() As Integer

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As WFForm
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getWFFormbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As WFForm
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getWFFormbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of WFForm)
            Dim _WFFormList As List(Of WFForm)
            _WFFormList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getWFFormTable"))
            Return _WFFormList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of WFForm)
            Dim _WFFormList As List(Of WFForm)
            _WFFormList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getWFFormTable"))
            Return _WFFormList
        End Function

        Public Shared Sub SaveAll(ByVal WFFormList As List(Of WFForm))
            For Each _WFForm As WFForm In WFFormList
                SaveRecord(_WFForm)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal WFFormList As List(Of WFForm))
            For Each _WFForm As WFForm In WFFormList
                SaveRecord(ConnectionString, _WFForm)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal WFForm As WFForm) As Guid
            Dim _Current As WFForm = RetreiveByID(WFForm._ID.Value)
            WFForm.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, WFForm, _Current, "upsertWFForm")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal WFForm As WFForm) As Guid
            Dim _Current As WFForm = RetreiveByID(ConnectionString, WFForm._ID.Value)
            WFForm.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, WFForm, _Current, "upsertWFForm")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As WFForm = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteWFFormbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As WFForm = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteWFFormbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As WFForm = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertWFForm")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As WFForm = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertWFForm")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As WFForm

            Dim _W As WFForm = Nothing

            If DR IsNot Nothing Then
                _W = New WFForm()
                With DR
                    _W.IsNew = False
                    _W.IsDeleted = False
                    _W._ID = GetGUID(DR("ID"))
                    _W._Name = DR("name").ToString.Trim
                    _W._Mode = DR("mode").ToString.Trim
                    _W._FormName = DR("form_name").ToString.Trim
                    _W._LastEntry = GetInteger(DR("last_entry"))
                    _W._SiteId = GetGUID(DR("site_id"))
                    _W._SiteName = DR("site_name").ToString.Trim

                End With
            End If

            Return _W

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of WFForm)

            Dim _WFFormList As New List(Of WFForm)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _WFFormList.Add(PropertiesFromData(_DR))
            Next

            Return _WFFormList

        End Function


#End Region

    End Class

End Namespace
