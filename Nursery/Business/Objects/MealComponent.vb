﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class MealComponent
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_MealId As Guid?
        Dim m_FoodId As Guid?
        Dim m_FoodName As String
        Dim m_FoodQty As Long

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._MealId = Nothing
                ._FoodId = Nothing
                ._FoodName = ""
                ._FoodQty = 0

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._MealId = Nothing
                ._FoodId = Nothing
                ._FoodName = ""
                ._FoodQty = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@MealId")> _
        Public Property _MealId() As Guid?
            Get
                Return m_MealId
            End Get
            Set(ByVal value As Guid?)
                m_MealId = value
            End Set
        End Property

        <StoredProcParameter("@FoodId")> _
        Public Property _FoodId() As Guid?
            Get
                Return m_FoodId
            End Get
            Set(ByVal value As Guid?)
                m_FoodId = value
            End Set
        End Property

        <StoredProcParameter("@FoodName")> _
        Public Property _FoodName() As String
            Get
                Return m_FoodName
            End Get
            Set(ByVal value As String)
                m_FoodName = value
            End Set
        End Property

        <StoredProcParameter("@FoodQty")> _
        Public Property _FoodQty() As Long
            Get
                Return m_FoodQty
            End Get
            Set(ByVal value As Long)
                m_FoodQty = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As MealComponent

            Dim _MealComponent As MealComponent
            _MealComponent = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMealComponentbyID", ID))
            Return _MealComponent

        End Function

        Public Shared Function RetreiveByMealID(ByVal MealID As Guid) As List(Of MealComponent)

            Dim _MealComponentList As List(Of MealComponent)

            Dim _Params As New List(Of SPParam)
            _Params.Add(New SPParam("@mealid", SqlDbType.UniqueIdentifier, MealID))

            _MealComponentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMealComponentbyMealID", _Params))

            _Params = Nothing

            Return _MealComponentList

        End Function

        Public Shared Function RetreiveAll() As List(Of MealComponent)

            Dim _MealComponentList As List(Of MealComponent)
            _MealComponentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMealComponentTable"))
            Return _MealComponentList

        End Function

        Public Shared Sub SaveAll(ByVal MealComponentList As List(Of MealComponent))

            For Each _MealComponent As MealComponent In MealComponentList
                SaveMealComponent(_MealComponent)
            Next

        End Sub

        Public Shared Function SaveMealComponent(ByVal MealComponent As MealComponent) As Guid
            DAL.SaveRecord(Session.ConnectionManager, MealComponent, "upsertMealComponent")
        End Function

        Public Shared Sub DeletebyID(ByVal ID As Guid)
            Dim _SQL As String = "delete from MealComponents where id = '" & ID.ToString & "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As MealComponent

            Dim _M As New MealComponent()

            If DR IsNot Nothing Then
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._MealId = GetGUID(DR("meal_id"))
                    _M._FoodId = GetGUID(DR("food_id"))
                    _M._FoodName = DR("food_name").ToString.Trim
                    _M._FoodQty = GetLong(DR("food_qty"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of MealComponent)

            Dim _MealComponentList As New List(Of MealComponent)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MealComponentList.Add(PropertiesFromData(_DR))
            Next

            Return _MealComponentList

        End Function


#End Region

    End Class

End Namespace
