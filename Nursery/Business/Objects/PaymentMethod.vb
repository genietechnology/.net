﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class PaymentMethod
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As PaymentMethod

            Dim _PaymentMethod As PaymentMethod
            _PaymentMethod = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPaymentMethodbyID", ID))
            Return _PaymentMethod

        End Function

        Public Shared Function RetreiveAll() As List(Of PaymentMethod)

            Dim _PaymentMethodList As List(Of PaymentMethod)
            _PaymentMethodList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPaymentMethodTable"))
            Return _PaymentMethodList

        End Function

        Public Shared Sub SaveAll(ByVal PaymentMethodList As List(Of PaymentMethod))

            For Each _PaymentMethod As PaymentMethod In PaymentMethodList
                SavePaymentMethod(_PaymentMethod)
            Next

        End Sub

        Public Shared Function SavePaymentMethod(ByVal PaymentMethod As PaymentMethod) As Guid
            DAL.SaveRecord(Session.ConnectionManager, PaymentMethod, "upsertPaymentMethod")
        End Function

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As PaymentMethod

            Dim _P As New PaymentMethod()

            If DR IsNot Nothing Then
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._Name = DR("name").ToString.Trim

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of PaymentMethod)

            Dim _PaymentMethodList As New List(Of PaymentMethod)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PaymentMethodList.Add(PropertiesFromData(_DR))
            Next

            Return _PaymentMethodList

        End Function


#End Region

    End Class

End Namespace
