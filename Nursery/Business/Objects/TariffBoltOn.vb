﻿'*****************************************************
'Generated 24/09/2018 09:52:53 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TariffBoltOn
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Category = ""
                ._Name = ""
                ._Price = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._Discount = False
                ._Breakfast = False
                ._Lunch = False
                ._Tea = False
                ._Snack = False
                ._ChargeClosed = False
                ._ChargeHoliday = False
                ._RecurringScope = Nothing
                ._RecurringScopeDesc = ""
                ._SummaryColumn = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Category = ""
                ._Name = ""
                ._Price = 0
                ._NlCode = ""
                ._NlTracking = ""
                ._Discount = False
                ._Breakfast = False
                ._Lunch = False
                ._Tea = False
                ._Snack = False
                ._ChargeClosed = False
                ._ChargeHoliday = False
                ._RecurringScope = Nothing
                ._RecurringScopeDesc = ""
                ._SummaryColumn = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Category")>
        Public Property _Category() As String

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Price")>
        Public Property _Price() As Decimal

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String

        <StoredProcParameter("@Discount")>
        Public Property _Discount() As Boolean

        <StoredProcParameter("@Breakfast")>
        Public Property _Breakfast() As Boolean

        <StoredProcParameter("@Lunch")>
        Public Property _Lunch() As Boolean

        <StoredProcParameter("@Tea")>
        Public Property _Tea() As Boolean

        <StoredProcParameter("@Snack")>
        Public Property _Snack() As Boolean

        <StoredProcParameter("@ChargeClosed")>
        Public Property _ChargeClosed() As Boolean

        <StoredProcParameter("@ChargeHoliday")>
        Public Property _ChargeHoliday() As Boolean

        <StoredProcParameter("@RecurringScope")>
        Public Property _RecurringScope() As Byte

        <StoredProcParameter("@RecurringScopeDesc")>
        Public Property _RecurringScopeDesc() As String

        <StoredProcParameter("@SummaryColumn")>
        Public Property _SummaryColumn() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TariffBoltOn
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffBoltOnbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TariffBoltOn
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffBoltOnbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of TariffBoltOn)
            Dim _TariffBoltOnList As List(Of TariffBoltOn)
            _TariffBoltOnList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffBoltOnTable"))
            Return _TariffBoltOnList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TariffBoltOn)
            Dim _TariffBoltOnList As List(Of TariffBoltOn)
            _TariffBoltOnList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffBoltOnTable"))
            Return _TariffBoltOnList
        End Function

        Public Shared Sub SaveAll(ByVal TariffBoltOnList As List(Of TariffBoltOn))
            For Each _TariffBoltOn As TariffBoltOn In TariffBoltOnList
                SaveRecord(_TariffBoltOn)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffBoltOnList As List(Of TariffBoltOn))
            For Each _TariffBoltOn As TariffBoltOn In TariffBoltOnList
                SaveRecord(ConnectionString, _TariffBoltOn)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal TariffBoltOn As TariffBoltOn) As Guid
            Dim _Current As TariffBoltOn = RetreiveByID(TariffBoltOn._ID.Value)
            TariffBoltOn.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffBoltOn, _Current, "upsertTariffBoltOn")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TariffBoltOn As TariffBoltOn) As Guid
            Dim _Current As TariffBoltOn = RetreiveByID(ConnectionString, TariffBoltOn._ID.Value)
            TariffBoltOn.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffBoltOn, _Current, "upsertTariffBoltOn")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As TariffBoltOn = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffBoltOnbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As TariffBoltOn = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffBoltOnbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As TariffBoltOn = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffBoltOn")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As TariffBoltOn = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffBoltOn")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TariffBoltOn

            Dim _T As TariffBoltOn = Nothing

            If DR IsNot Nothing Then
                _T = New TariffBoltOn()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Category = DR("category").ToString.Trim
                    _T._Name = DR("name").ToString.Trim
                    _T._Price = GetDecimal(DR("price"))
                    _T._NlCode = DR("nl_code").ToString.Trim
                    _T._NlTracking = DR("nl_tracking").ToString.Trim
                    _T._Discount = GetBoolean(DR("discount"))
                    _T._Breakfast = GetBoolean(DR("breakfast"))
                    _T._Lunch = GetBoolean(DR("lunch"))
                    _T._Tea = GetBoolean(DR("tea"))
                    _T._Snack = GetBoolean(DR("snack"))
                    _T._ChargeClosed = GetBoolean(DR("charge_closed"))
                    _T._ChargeHoliday = GetBoolean(DR("charge_holiday"))
                    _T._RecurringScope = GetByte(DR("recurring_scope"))
                    _T._RecurringScopeDesc = DR("recurring_scope_desc").ToString.Trim
                    _T._SummaryColumn = DR("summary_column").ToString.Trim

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TariffBoltOn)

            Dim _TariffBoltOnList As New List(Of TariffBoltOn)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffBoltOnList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffBoltOnList

        End Function


#End Region

    End Class

End Namespace
