﻿'*****************************************************
'Generated 18/09/2016 10:50:27 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildMove
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_Date As Date?
        Dim m_GroupId As Guid?
        Dim m_GroupName As String
        Dim m_RoomId As Guid?
        Dim m_RoomName As String
        Dim m_Manual As Boolean
        Dim m_EnteredBy As Guid?
        Dim m_EnteredStamp As DateTime?
        Dim m_KeyworkerId As Guid?
        Dim m_KeyworkerName As String
        Dim m_Move As Boolean
        Dim m_RatioId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._Date = Nothing
                ._GroupId = Nothing
                ._GroupName = ""
                ._RoomId = Nothing
                ._RoomName = ""
                ._Manual = False
                ._EnteredBy = Nothing
                ._EnteredStamp = Nothing
                ._KeyworkerId = Nothing
                ._KeyworkerName = ""
                ._Move = False
                ._RatioId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._Date = Nothing
                ._GroupId = Nothing
                ._GroupName = ""
                ._RoomId = Nothing
                ._RoomName = ""
                ._Manual = False
                ._EnteredBy = Nothing
                ._EnteredStamp = Nothing
                ._KeyworkerId = Nothing
                ._KeyworkerName = ""
                ._Move = False
                ._RatioId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As Date?
            Get
                Return m_Date
            End Get
            Set(ByVal value As Date?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@GroupId")> _
        Public Property _GroupId() As Guid?
            Get
                Return m_GroupId
            End Get
            Set(ByVal value As Guid?)
                m_GroupId = value
            End Set
        End Property

        <StoredProcParameter("@GroupName")> _
        Public Property _GroupName() As String
            Get
                Return m_GroupName
            End Get
            Set(ByVal value As String)
                m_GroupName = value
            End Set
        End Property

        <StoredProcParameter("@RoomId")> _
        Public Property _RoomId() As Guid?
            Get
                Return m_RoomId
            End Get
            Set(ByVal value As Guid?)
                m_RoomId = value
            End Set
        End Property

        <StoredProcParameter("@RoomName")> _
        Public Property _RoomName() As String
            Get
                Return m_RoomName
            End Get
            Set(ByVal value As String)
                m_RoomName = value
            End Set
        End Property

        <StoredProcParameter("@Manual")> _
        Public Property _Manual() As Boolean
            Get
                Return m_Manual
            End Get
            Set(ByVal value As Boolean)
                m_Manual = value
            End Set
        End Property

        <StoredProcParameter("@EnteredBy")> _
        Public Property _EnteredBy() As Guid?
            Get
                Return m_EnteredBy
            End Get
            Set(ByVal value As Guid?)
                m_EnteredBy = value
            End Set
        End Property

        <StoredProcParameter("@EnteredStamp")> _
        Public Property _EnteredStamp() As DateTime?
            Get
                Return m_EnteredStamp
            End Get
            Set(ByVal value As DateTime?)
                m_EnteredStamp = value
            End Set
        End Property

        <StoredProcParameter("@KeyworkerId")> _
        Public Property _KeyworkerId() As Guid?
            Get
                Return m_KeyworkerId
            End Get
            Set(ByVal value As Guid?)
                m_KeyworkerId = value
            End Set
        End Property

        <StoredProcParameter("@KeyworkerName")> _
        Public Property _KeyworkerName() As String
            Get
                Return m_KeyworkerName
            End Get
            Set(ByVal value As String)
                m_KeyworkerName = value
            End Set
        End Property

        <StoredProcParameter("@Move")> _
        Public Property _Move() As Boolean
            Get
                Return m_Move
            End Get
            Set(ByVal value As Boolean)
                m_Move = value
            End Set
        End Property

        <StoredProcParameter("@RatioId")> _
        Public Property _RatioId() As Guid?
            Get
                Return m_RatioId
            End Get
            Set(ByVal value As Guid?)
                m_RatioId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildMove

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildMovebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildMove)

            Dim _ChildMoveList As List(Of ChildMove)
            _ChildMoveList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildMoveTable"))
            Return _ChildMoveList

        End Function

        Public Shared Sub SaveAll(ByVal ChildMoveList As List(Of ChildMove))

            For Each _ChildMove As ChildMove In ChildMoveList
                SaveRecord(_ChildMove)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildMove As ChildMove) As Guid
            ChildMove.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildMove, "upsertChildMove")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildMovebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildMove")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildMove

            Dim _C As ChildMove = Nothing

            If DR IsNot Nothing Then
                _C = New ChildMove()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._Date = GetDate(DR("date"))
                    _C._GroupId = GetGUID(DR("group_id"))
                    _C._GroupName = DR("group_name").ToString.Trim
                    _C._RoomId = GetGUID(DR("room_id"))
                    _C._RoomName = DR("room_name").ToString.Trim
                    _C._Manual = GetBoolean(DR("manual"))
                    _C._EnteredBy = GetGUID(DR("entered_by"))
                    _C._EnteredStamp = GetDateTime(DR("entered_stamp"))
                    _C._KeyworkerId = GetGUID(DR("keyworker_id"))
                    _C._KeyworkerName = DR("keyworker_name").ToString.Trim
                    _C._Move = GetBoolean(DR("move"))
                    _C._RatioId = GetGUID(DR("ratio_id"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildMove)

            Dim _ChildMoveList As New List(Of ChildMove)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildMoveList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildMoveList

        End Function


#End Region

    End Class

End Namespace
