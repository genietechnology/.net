﻿'*****************************************************
'Generated 24/09/2018 09:48:47 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Invoice
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BatchId = Nothing
                ._InvoiceNo = 0
                ._InvoiceStatus = ""
                ._InvoiceDate = Nothing
                ._InvoicePeriod = Nothing
                ._InvoicePeriodEnd = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._Address = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._DiscountId = Nothing
                ._DiscountName = ""
                ._InvoiceSub = 0
                ._InvoiceDiscount = 0
                ._InvoiceTotal = 0
                ._FinancialsId = ""
                ._FinancialsDocId = ""
                ._Class = ""
                ._InvoiceDue = ""
                ._InvoiceDueDate = Nothing
                ._FamilyAccountNo = ""
                ._InvoiceAnnualised = False
                ._InvoiceLayout = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing
                ._Xcheck = 0
                ._XcheckFrom = Nothing
                ._XcheckTo = Nothing
                ._XcheckThresh = 0
                ._XcheckPerHour = 0
                ._XcheckAdditional = False
                ._XcheckCount = 0
                ._XcheckAmount = 0
                ._Discountable = 0
                ._AdditionalSessions = Nothing
                ._DuplicateMode = Nothing
                ._TariffFilter = ""
                ._InvoiceHoursNonfunded = 0
                ._InvoiceHoursFunded = 0
                ._TotalNonfunded = 0
                ._TotalFunded = 0
                ._TotalBoltons = 0
                ._StarterDate = Nothing
                ._LeaverDate = Nothing
                ._AdditionalFrom = Nothing
                ._AdditionalTo = Nothing
                ._AnnualType = ""
                ._GenerationMode = Nothing
                ._AnnualId = Nothing
                ._InvoiceRef = ""
                ._VoucherMode = ""
                ._VoucherProportion = 0
                ._VoucherPool = 0
                ._VoucherAmount = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BatchId = Nothing
                ._InvoiceNo = 0
                ._InvoiceStatus = ""
                ._InvoiceDate = Nothing
                ._InvoicePeriod = Nothing
                ._InvoicePeriodEnd = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._Address = ""
                ._Add1 = ""
                ._Add2 = ""
                ._Add3 = ""
                ._Add4 = ""
                ._Postcode = ""
                ._DiscountId = Nothing
                ._DiscountName = ""
                ._InvoiceSub = 0
                ._InvoiceDiscount = 0
                ._InvoiceTotal = 0
                ._FinancialsId = ""
                ._FinancialsDocId = ""
                ._Class = ""
                ._InvoiceDue = ""
                ._InvoiceDueDate = Nothing
                ._FamilyAccountNo = ""
                ._InvoiceAnnualised = False
                ._InvoiceLayout = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing
                ._Xcheck = 0
                ._XcheckFrom = Nothing
                ._XcheckTo = Nothing
                ._XcheckThresh = 0
                ._XcheckPerHour = 0
                ._XcheckAdditional = False
                ._XcheckCount = 0
                ._XcheckAmount = 0
                ._Discountable = 0
                ._AdditionalSessions = Nothing
                ._DuplicateMode = Nothing
                ._TariffFilter = ""
                ._InvoiceHoursNonfunded = 0
                ._InvoiceHoursFunded = 0
                ._TotalNonfunded = 0
                ._TotalFunded = 0
                ._TotalBoltons = 0
                ._StarterDate = Nothing
                ._LeaverDate = Nothing
                ._AdditionalFrom = Nothing
                ._AdditionalTo = Nothing
                ._AnnualType = ""
                ._GenerationMode = Nothing
                ._AnnualId = Nothing
                ._InvoiceRef = ""
                ._VoucherMode = ""
                ._VoucherProportion = 0
                ._VoucherPool = 0
                ._VoucherAmount = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@BatchId")>
        Public Property _BatchId() As Guid?

        <StoredProcParameter("@InvoiceNo")>
        Public Property _InvoiceNo() As Long

        <StoredProcParameter("@InvoiceStatus")>
        Public Property _InvoiceStatus() As String

        <StoredProcParameter("@InvoiceDate")>
        Public Property _InvoiceDate() As Date?

        <StoredProcParameter("@InvoicePeriod")>
        Public Property _InvoicePeriod() As Date?

        <StoredProcParameter("@InvoicePeriodEnd")>
        Public Property _InvoicePeriodEnd() As Date?

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?

        <StoredProcParameter("@ChildName")>
        Public Property _ChildName() As String

        <StoredProcParameter("@FamilyId")>
        Public Property _FamilyId() As Guid?

        <StoredProcParameter("@FamilyName")>
        Public Property _FamilyName() As String

        <StoredProcParameter("@Address")>
        Public Property _Address() As String

        <StoredProcParameter("@Add1")>
        Public Property _Add1() As String

        <StoredProcParameter("@Add2")>
        Public Property _Add2() As String

        <StoredProcParameter("@Add3")>
        Public Property _Add3() As String

        <StoredProcParameter("@Add4")>
        Public Property _Add4() As String

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String

        <StoredProcParameter("@DiscountId")>
        Public Property _DiscountId() As Guid?

        <StoredProcParameter("@DiscountName")>
        Public Property _DiscountName() As String

        <StoredProcParameter("@InvoiceSub")>
        Public Property _InvoiceSub() As Decimal

        <StoredProcParameter("@InvoiceDiscount")>
        Public Property _InvoiceDiscount() As Decimal

        <StoredProcParameter("@InvoiceTotal")>
        Public Property _InvoiceTotal() As Decimal

        <StoredProcParameter("@FinancialsId")>
        Public Property _FinancialsId() As String

        <StoredProcParameter("@FinancialsDocId")>
        Public Property _FinancialsDocId() As String

        <StoredProcParameter("@Class")>
        Public Property _Class() As String

        <StoredProcParameter("@InvoiceDue")>
        Public Property _InvoiceDue() As String

        <StoredProcParameter("@InvoiceDueDate")>
        Public Property _InvoiceDueDate() As Date?

        <StoredProcParameter("@FamilyAccountNo")>
        Public Property _FamilyAccountNo() As String

        <StoredProcParameter("@InvoiceAnnualised")>
        Public Property _InvoiceAnnualised() As Boolean

        <StoredProcParameter("@InvoiceLayout")>
        Public Property _InvoiceLayout() As String

        <StoredProcParameter("@ChangedBy")>
        Public Property _ChangedBy() As Guid?

        <StoredProcParameter("@ChangedStamp")>
        Public Property _ChangedStamp() As DateTime?

        <StoredProcParameter("@Xcheck")>
        Public Property _Xcheck() As Integer

        <StoredProcParameter("@XcheckFrom")>
        Public Property _XcheckFrom() As Date?

        <StoredProcParameter("@XcheckTo")>
        Public Property _XcheckTo() As Date?

        <StoredProcParameter("@XcheckThresh")>
        Public Property _XcheckThresh() As Decimal

        <StoredProcParameter("@XcheckPerHour")>
        Public Property _XcheckPerHour() As Decimal

        <StoredProcParameter("@XcheckAdditional")>
        Public Property _XcheckAdditional() As Boolean

        <StoredProcParameter("@XcheckCount")>
        Public Property _XcheckCount() As Integer

        <StoredProcParameter("@XcheckAmount")>
        Public Property _XcheckAmount() As Decimal

        <StoredProcParameter("@Discountable")>
        Public Property _Discountable() As Decimal

        <StoredProcParameter("@AdditionalSessions")>
        Public Property _AdditionalSessions() As Byte

        <StoredProcParameter("@DuplicateMode")>
        Public Property _DuplicateMode() As Byte

        <StoredProcParameter("@TariffFilter")>
        Public Property _TariffFilter() As String

        <StoredProcParameter("@InvoiceHoursNonfunded")>
        Public Property _InvoiceHoursNonfunded() As Decimal

        <StoredProcParameter("@InvoiceHoursFunded")>
        Public Property _InvoiceHoursFunded() As Decimal

        <StoredProcParameter("@TotalNonfunded")>
        Public Property _TotalNonfunded() As Decimal

        <StoredProcParameter("@TotalFunded")>
        Public Property _TotalFunded() As Decimal

        <StoredProcParameter("@TotalBoltons")>
        Public Property _TotalBoltons() As Decimal

        <StoredProcParameter("@StarterDate")>
        Public Property _StarterDate() As Date?

        <StoredProcParameter("@LeaverDate")>
        Public Property _LeaverDate() As Date?

        <StoredProcParameter("@AdditionalFrom")>
        Public Property _AdditionalFrom() As Date?

        <StoredProcParameter("@AdditionalTo")>
        Public Property _AdditionalTo() As Date?

        <StoredProcParameter("@AnnualType")>
        Public Property _AnnualType() As String

        <StoredProcParameter("@GenerationMode")>
        Public Property _GenerationMode() As Byte

        <StoredProcParameter("@AnnualId")>
        Public Property _AnnualId() As Guid?

        <StoredProcParameter("@InvoiceRef")>
        Public Property _InvoiceRef() As String

        <StoredProcParameter("@VoucherMode")>
        Public Property _VoucherMode() As String

        <StoredProcParameter("@VoucherProportion")>
        Public Property _VoucherProportion() As Decimal

        <StoredProcParameter("@VoucherPool")>
        Public Property _VoucherPool() As Decimal

        <StoredProcParameter("@VoucherAmount")>
        Public Property _VoucherAmount() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Invoice
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInvoicebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Invoice
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getInvoicebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Invoice)
            Dim _InvoiceList As List(Of Invoice)
            _InvoiceList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInvoiceTable"))
            Return _InvoiceList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Invoice)
            Dim _InvoiceList As List(Of Invoice)
            _InvoiceList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getInvoiceTable"))
            Return _InvoiceList
        End Function

        Public Shared Sub SaveAll(ByVal InvoiceList As List(Of Invoice))
            For Each _Invoice As Invoice In InvoiceList
                SaveRecord(_Invoice)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal InvoiceList As List(Of Invoice))
            For Each _Invoice As Invoice In InvoiceList
                SaveRecord(ConnectionString, _Invoice)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Invoice As Invoice) As Guid
            Dim _Current As Invoice = RetreiveByID(Invoice._ID.Value)
            Invoice.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Invoice, _Current, "upsertInvoice")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Invoice As Invoice) As Guid
            Dim _Current As Invoice = RetreiveByID(ConnectionString, Invoice._ID.Value)
            Invoice.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Invoice, _Current, "upsertInvoice")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Invoice = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoicebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Invoice = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteInvoicebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Invoice = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoice")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Invoice = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertInvoice")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Invoice

            Dim _I As Invoice = Nothing

            If DR IsNot Nothing Then
                _I = New Invoice()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._BatchId = GetGUID(DR("batch_id"))
                    _I._InvoiceNo = GetLong(DR("invoice_no"))
                    _I._InvoiceStatus = DR("invoice_status").ToString.Trim
                    _I._InvoiceDate = GetDate(DR("invoice_date"))
                    _I._InvoicePeriod = GetDate(DR("invoice_period"))
                    _I._InvoicePeriodEnd = GetDate(DR("invoice_period_end"))
                    _I._ChildId = GetGUID(DR("child_id"))
                    _I._ChildName = DR("child_name").ToString.Trim
                    _I._FamilyId = GetGUID(DR("family_id"))
                    _I._FamilyName = DR("family_name").ToString.Trim
                    _I._Address = DR("address").ToString.Trim
                    _I._Add1 = DR("add_1").ToString.Trim
                    _I._Add2 = DR("add_2").ToString.Trim
                    _I._Add3 = DR("add_3").ToString.Trim
                    _I._Add4 = DR("add_4").ToString.Trim
                    _I._Postcode = DR("postcode").ToString.Trim
                    _I._DiscountId = GetGUID(DR("discount_id"))
                    _I._DiscountName = DR("discount_name").ToString.Trim
                    _I._InvoiceSub = GetDecimal(DR("invoice_sub"))
                    _I._InvoiceDiscount = GetDecimal(DR("invoice_discount"))
                    _I._InvoiceTotal = GetDecimal(DR("invoice_total"))
                    _I._FinancialsId = DR("financials_id").ToString.Trim
                    _I._FinancialsDocId = DR("financials_doc_id").ToString.Trim
                    _I._Class = DR("class").ToString.Trim
                    _I._InvoiceDue = DR("invoice_due").ToString.Trim
                    _I._InvoiceDueDate = GetDate(DR("invoice_due_date"))
                    _I._FamilyAccountNo = DR("family_account_no").ToString.Trim
                    _I._InvoiceAnnualised = GetBoolean(DR("invoice_annualised"))
                    _I._InvoiceLayout = DR("invoice_layout").ToString.Trim
                    _I._ChangedBy = GetGUID(DR("changed_by"))
                    _I._ChangedStamp = GetDateTime(DR("changed_stamp"))
                    _I._Xcheck = GetInteger(DR("xcheck"))
                    _I._XcheckFrom = GetDate(DR("xcheck_from"))
                    _I._XcheckTo = GetDate(DR("xcheck_to"))
                    _I._XcheckThresh = GetDecimal(DR("xcheck_thresh"))
                    _I._XcheckPerHour = GetDecimal(DR("xcheck_per_hour"))
                    _I._XcheckAdditional = GetBoolean(DR("xcheck_additional"))
                    _I._XcheckCount = GetInteger(DR("xcheck_count"))
                    _I._XcheckAmount = GetDecimal(DR("xcheck_amount"))
                    _I._Discountable = GetDecimal(DR("discountable"))
                    _I._AdditionalSessions = GetByte(DR("additional_sessions"))
                    _I._DuplicateMode = GetByte(DR("duplicate_mode"))
                    _I._TariffFilter = DR("tariff_filter").ToString.Trim
                    _I._InvoiceHoursNonfunded = GetDecimal(DR("invoice_hours_nonfunded"))
                    _I._InvoiceHoursFunded = GetDecimal(DR("invoice_hours_funded"))
                    _I._TotalNonfunded = GetDecimal(DR("total_nonfunded"))
                    _I._TotalFunded = GetDecimal(DR("total_funded"))
                    _I._TotalBoltons = GetDecimal(DR("total_boltons"))
                    _I._StarterDate = GetDate(DR("starter_date"))
                    _I._LeaverDate = GetDate(DR("leaver_date"))
                    _I._AdditionalFrom = GetDate(DR("additional_from"))
                    _I._AdditionalTo = GetDate(DR("additional_to"))
                    _I._AnnualType = DR("annual_type").ToString.Trim
                    _I._GenerationMode = GetByte(DR("generation_mode"))
                    _I._AnnualId = GetGUID(DR("annual_id"))
                    _I._InvoiceRef = DR("invoice_ref").ToString.Trim
                    _I._VoucherMode = DR("voucher_mode").ToString.Trim
                    _I._VoucherProportion = GetDecimal(DR("voucher_proportion"))
                    _I._VoucherPool = GetDecimal(DR("voucher_pool"))
                    _I._VoucherAmount = GetDecimal(DR("voucher_amount"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Invoice)

            Dim _InvoiceList As New List(Of Invoice)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InvoiceList.Add(PropertiesFromData(_DR))
            Next

            Return _InvoiceList

        End Function


#End Region

    End Class

End Namespace
