﻿'*****************************************************
'Generated 07/03/2018 22:09:23 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Credit
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_BatchId As Guid?
        Dim m_CreditNo As Long
        Dim m_CreditStatus As String
        Dim m_CreditRef As String
        Dim m_CreditDate As Date?
        Dim m_CreditDue As Date?
        Dim m_CreditInvoiceId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_FamilyId As Guid?
        Dim m_FamilyName As String
        Dim m_FamilyAccountNo As String
        Dim m_Address As String
        Dim m_Hours As Decimal
        Dim m_FundedHours As Decimal
        Dim m_FundedValue As Decimal
        Dim m_CreditTotal As Decimal
        Dim m_FinancialsId As String
        Dim m_FinancialsDocId As String
        Dim m_ChangedBy As Guid?
        Dim m_ChangedStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BatchId = Nothing
                ._CreditNo = 0
                ._CreditStatus = ""
                ._CreditRef = ""
                ._CreditDate = Nothing
                ._CreditDue = Nothing
                ._CreditInvoiceId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._FamilyAccountNo = ""
                ._Address = ""
                ._Hours = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._CreditTotal = 0
                ._FinancialsId = ""
                ._FinancialsDocId = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BatchId = Nothing
                ._CreditNo = 0
                ._CreditStatus = ""
                ._CreditRef = ""
                ._CreditDate = Nothing
                ._CreditDue = Nothing
                ._CreditInvoiceId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._FamilyAccountNo = ""
                ._Address = ""
                ._Hours = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._CreditTotal = 0
                ._FinancialsId = ""
                ._FinancialsDocId = ""
                ._ChangedBy = Nothing
                ._ChangedStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@BatchId")>
        Public Property _BatchId() As Guid?
            Get
                Return m_BatchId
            End Get
            Set(ByVal value As Guid?)
                m_BatchId = value
            End Set
        End Property

        <StoredProcParameter("@CreditNo")>
        Public Property _CreditNo() As Long
            Get
                Return m_CreditNo
            End Get
            Set(ByVal value As Long)
                m_CreditNo = value
            End Set
        End Property

        <StoredProcParameter("@CreditStatus")>
        Public Property _CreditStatus() As String
            Get
                Return m_CreditStatus
            End Get
            Set(ByVal value As String)
                m_CreditStatus = value
            End Set
        End Property

        <StoredProcParameter("@CreditRef")>
        Public Property _CreditRef() As String
            Get
                Return m_CreditRef
            End Get
            Set(ByVal value As String)
                m_CreditRef = value
            End Set
        End Property

        <StoredProcParameter("@CreditDate")>
        Public Property _CreditDate() As Date?
            Get
                Return m_CreditDate
            End Get
            Set(ByVal value As Date?)
                m_CreditDate = value
            End Set
        End Property

        <StoredProcParameter("@CreditDue")>
        Public Property _CreditDue() As Date?
            Get
                Return m_CreditDue
            End Get
            Set(ByVal value As Date?)
                m_CreditDue = value
            End Set
        End Property

        <StoredProcParameter("@CreditInvoiceId")>
        Public Property _CreditInvoiceId() As Guid?
            Get
                Return m_CreditInvoiceId
            End Get
            Set(ByVal value As Guid?)
                m_CreditInvoiceId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")>
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")>
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@FamilyId")>
        Public Property _FamilyId() As Guid?
            Get
                Return m_FamilyId
            End Get
            Set(ByVal value As Guid?)
                m_FamilyId = value
            End Set
        End Property

        <StoredProcParameter("@FamilyName")>
        Public Property _FamilyName() As String
            Get
                Return m_FamilyName
            End Get
            Set(ByVal value As String)
                m_FamilyName = value
            End Set
        End Property

        <StoredProcParameter("@FamilyAccountNo")>
        Public Property _FamilyAccountNo() As String
            Get
                Return m_FamilyAccountNo
            End Get
            Set(ByVal value As String)
                m_FamilyAccountNo = value
            End Set
        End Property

        <StoredProcParameter("@Address")>
        Public Property _Address() As String
            Get
                Return m_Address
            End Get
            Set(ByVal value As String)
                m_Address = value
            End Set
        End Property

        <StoredProcParameter("@Hours")>
        Public Property _Hours() As Decimal
            Get
                Return m_Hours
            End Get
            Set(ByVal value As Decimal)
                m_Hours = value
            End Set
        End Property

        <StoredProcParameter("@FundedHours")>
        Public Property _FundedHours() As Decimal
            Get
                Return m_FundedHours
            End Get
            Set(ByVal value As Decimal)
                m_FundedHours = value
            End Set
        End Property

        <StoredProcParameter("@FundedValue")>
        Public Property _FundedValue() As Decimal
            Get
                Return m_FundedValue
            End Get
            Set(ByVal value As Decimal)
                m_FundedValue = value
            End Set
        End Property

        <StoredProcParameter("@CreditTotal")>
        Public Property _CreditTotal() As Decimal
            Get
                Return m_CreditTotal
            End Get
            Set(ByVal value As Decimal)
                m_CreditTotal = value
            End Set
        End Property

        <StoredProcParameter("@FinancialsId")>
        Public Property _FinancialsId() As String
            Get
                Return m_FinancialsId
            End Get
            Set(ByVal value As String)
                m_FinancialsId = value
            End Set
        End Property

        <StoredProcParameter("@FinancialsDocId")>
        Public Property _FinancialsDocId() As String
            Get
                Return m_FinancialsDocId
            End Get
            Set(ByVal value As String)
                m_FinancialsDocId = value
            End Set
        End Property

        <StoredProcParameter("@ChangedBy")>
        Public Property _ChangedBy() As Guid?
            Get
                Return m_ChangedBy
            End Get
            Set(ByVal value As Guid?)
                m_ChangedBy = value
            End Set
        End Property

        <StoredProcParameter("@ChangedStamp")>
        Public Property _ChangedStamp() As DateTime?
            Get
                Return m_ChangedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_ChangedStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Credit

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCreditbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Credit

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCreditbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Credit)

            Dim _CreditList As List(Of Credit)
            _CreditList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCreditTable"))
            Return _CreditList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Credit)

            Dim _CreditList As List(Of Credit)
            _CreditList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCreditTable"))
            Return _CreditList

        End Function

        Public Shared Sub SaveAll(ByVal CreditList As List(Of Credit))

            For Each _Credit As Credit In CreditList
                SaveRecord(_Credit)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CreditList As List(Of Credit))

            For Each _Credit As Credit In CreditList
                SaveRecord(ConnectionString, _Credit)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Credit As Credit) As Guid
            Credit.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Credit, "upsertCredit")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Credit As Credit) As Guid
            Credit.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Credit, "upsertCredit")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCreditbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCreditbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCredit")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCredit")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Credit

            Dim _C As Credit = Nothing

            If DR IsNot Nothing Then
                _C = New Credit()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._BatchId = GetGUID(DR("batch_id"))
                    _C._CreditNo = GetLong(DR("credit_no"))
                    _C._CreditStatus = DR("credit_status").ToString.Trim
                    _C._CreditRef = DR("credit_ref").ToString.Trim
                    _C._CreditDate = GetDate(DR("credit_date"))
                    _C._CreditDue = GetDate(DR("credit_due"))
                    _C._CreditInvoiceId = GetGUID(DR("credit_invoice_id"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._ChildName = DR("child_name").ToString.Trim
                    _C._FamilyId = GetGUID(DR("family_id"))
                    _C._FamilyName = DR("family_name").ToString.Trim
                    _C._FamilyAccountNo = DR("family_account_no").ToString.Trim
                    _C._Address = DR("address").ToString.Trim
                    _C._Hours = GetDecimal(DR("hours"))
                    _C._FundedHours = GetDecimal(DR("funded_hours"))
                    _C._FundedValue = GetDecimal(DR("funded_value"))
                    _C._CreditTotal = GetDecimal(DR("credit_total"))
                    _C._FinancialsId = DR("financials_id").ToString.Trim
                    _C._FinancialsDocId = DR("financials_doc_id").ToString.Trim
                    _C._ChangedBy = GetGUID(DR("changed_by"))
                    _C._ChangedStamp = GetDateTime(DR("changed_stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Credit)

            Dim _CreditList As New List(Of Credit)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CreditList.Add(PropertiesFromData(_DR))
            Next

            Return _CreditList

        End Function


#End Region

    End Class

End Namespace
