﻿'*****************************************************
'Generated 25/09/2018 21:05:04 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Site
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Address = ""
                ._Postcode = ""
                ._Tel = ""
                ._NlCode = ""
                ._NlTracking = ""
                ._AccPrefix = ""
                ._AccNext = 0
                ._Coordinates = ""
                ._Photo = Nothing
                ._eGenFrom = ""
                ._eGenAddress = ""
                ._eInvFrom = ""
                ._eInvAddress = ""
                ._eReportFrom = ""
                ._eReportAddress = ""
                ._Hidden = False
                ._FinOverride = False
                ._FinSystem = ""
                ._FinApiKey = ""
                ._FinApiSecret = ""
                ._FinPfxPath = ""
                ._FinPfxPwd = ""
                ._FinSagePath = ""
                ._FinSageVer = ""
                ._FinSageUser = ""
                ._FinSagePwd = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Address = ""
                ._Postcode = ""
                ._Tel = ""
                ._NlCode = ""
                ._NlTracking = ""
                ._AccPrefix = ""
                ._AccNext = 0
                ._Coordinates = ""
                ._Photo = Nothing
                ._eGenFrom = ""
                ._eGenAddress = ""
                ._eInvFrom = ""
                ._eInvAddress = ""
                ._eReportFrom = ""
                ._eReportAddress = ""
                ._Hidden = False
                ._FinOverride = False
                ._FinSystem = ""
                ._FinApiKey = ""
                ._FinApiSecret = ""
                ._FinPfxPath = ""
                ._FinPfxPwd = ""
                ._FinSagePath = ""
                ._FinSageVer = ""
                ._FinSageUser = ""
                ._FinSagePwd = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Address")>
        Public Property _Address() As String

        <StoredProcParameter("@Postcode")>
        Public Property _Postcode() As String

        <StoredProcParameter("@Tel")>
        Public Property _Tel() As String

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String

        <StoredProcParameter("@AccPrefix")>
        Public Property _AccPrefix() As String

        <StoredProcParameter("@AccNext")>
        Public Property _AccNext() As Integer

        <StoredProcParameter("@Coordinates")>
        Public Property _Coordinates() As String

        <StoredProcParameter("@Photo")>
        Public Property _Photo() As Byte()

        <StoredProcParameter("@eGenFrom")>
        Public Property _eGenFrom() As String

        <StoredProcParameter("@eGenAddress")>
        Public Property _eGenAddress() As String

        <StoredProcParameter("@eInvFrom")>
        Public Property _eInvFrom() As String

        <StoredProcParameter("@eInvAddress")>
        Public Property _eInvAddress() As String

        <StoredProcParameter("@eReportFrom")>
        Public Property _eReportFrom() As String

        <StoredProcParameter("@eReportAddress")>
        Public Property _eReportAddress() As String

        <StoredProcParameter("@Hidden")>
        Public Property _Hidden() As Boolean

        <StoredProcParameter("@FinOverride")>
        Public Property _FinOverride() As Boolean

        <StoredProcParameter("@FinSystem")>
        Public Property _FinSystem() As String

        <StoredProcParameter("@FinApiKey")>
        Public Property _FinApiKey() As String

        <StoredProcParameter("@FinApiSecret")>
        Public Property _FinApiSecret() As String

        <StoredProcParameter("@FinPfxPath")>
        Public Property _FinPfxPath() As String

        <StoredProcParameter("@FinPfxPwd")>
        Public Property _FinPfxPwd() As String

        <StoredProcParameter("@FinSagePath")>
        Public Property _FinSagePath() As String

        <StoredProcParameter("@FinSageVer")>
        Public Property _FinSageVer() As String

        <StoredProcParameter("@FinSageUser")>
        Public Property _FinSageUser() As String

        <StoredProcParameter("@FinSagePwd")>
        Public Property _FinSagePwd() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Site
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSitebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Site
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getSitebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Site)
            Dim _SiteList As List(Of Site)
            _SiteList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSiteTable"))
            Return _SiteList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Site)
            Dim _SiteList As List(Of Site)
            _SiteList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getSiteTable"))
            Return _SiteList
        End Function

        Public Shared Sub SaveAll(ByVal SiteList As List(Of Site))
            For Each _Site As Site In SiteList
                SaveRecord(_Site)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal SiteList As List(Of Site))
            For Each _Site As Site In SiteList
                SaveRecord(ConnectionString, _Site)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Site As Site) As Guid
            Dim _Current As Site = RetreiveByID(Site._ID.Value)
            Site.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Site, _Current, "upsertSite")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Site As Site) As Guid
            Dim _Current As Site = RetreiveByID(ConnectionString, Site._ID.Value)
            Site.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Site, _Current, "upsertSite")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Site = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteSitebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Site = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteSitebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Site = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertSite")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Site = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertSite")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Site

            Dim _S As Site = Nothing

            If DR IsNot Nothing Then
                _S = New Site()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._Name = DR("name").ToString.Trim
                    _S._Address = DR("address").ToString.Trim
                    _S._Postcode = DR("postcode").ToString.Trim
                    _S._Tel = DR("tel").ToString.Trim
                    _S._NlCode = DR("nl_code").ToString.Trim
                    _S._NlTracking = DR("nl_tracking").ToString.Trim
                    _S._AccPrefix = DR("acc_prefix").ToString.Trim
                    _S._AccNext = GetInteger(DR("acc_next"))
                    _S._Coordinates = DR("coordinates").ToString.Trim
                    _S._Photo = GetImage(DR("photo"))
                    _S._eGenFrom = DR("e_gen_from").ToString.Trim
                    _S._eGenAddress = DR("e_gen_address").ToString.Trim
                    _S._eInvFrom = DR("e_inv_from").ToString.Trim
                    _S._eInvAddress = DR("e_inv_address").ToString.Trim
                    _S._eReportFrom = DR("e_report_from").ToString.Trim
                    _S._eReportAddress = DR("e_report_address").ToString.Trim
                    _S._Hidden = GetBoolean(DR("hidden"))
                    _S._FinOverride = GetBoolean(DR("fin_override"))
                    _S._FinSystem = DR("fin_system").ToString.Trim
                    _S._FinApiKey = DR("fin_api_key").ToString.Trim
                    _S._FinApiSecret = DR("fin_api_secret").ToString.Trim
                    _S._FinPfxPath = DR("fin_pfx_path").ToString.Trim
                    _S._FinPfxPwd = DR("fin_pfx_pwd").ToString.Trim
                    _S._FinSagePath = DR("fin_sage_path").ToString.Trim
                    _S._FinSageVer = DR("fin_sage_ver").ToString.Trim
                    _S._FinSageUser = DR("fin_sage_user").ToString.Trim
                    _S._FinSagePwd = DR("fin_sage_pwd").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Site)

            Dim _SiteList As New List(Of Site)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SiteList.Add(PropertiesFromData(_DR))
            Next

            Return _SiteList

        End Function


#End Region

    End Class

End Namespace
