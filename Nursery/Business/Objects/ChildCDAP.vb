﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildCDAP
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_CdapId As Guid?
        Dim m_DayId As Guid?
        Dim m_Date As DateTime?
        Dim m_PhotoRef As String
        Dim m_PhotoImported As Boolean
        Dim m_Comments As String
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._CdapId = Nothing
                ._DayId = Nothing
                ._Date = Nothing
                ._PhotoRef = ""
                ._PhotoImported = False
                ._Comments = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._CdapId = Nothing
                ._DayId = Nothing
                ._Date = Nothing
                ._PhotoRef = ""
                ._PhotoImported = False
                ._Comments = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@CdapId")> _
        Public Property _CdapId() As Guid?
            Get
                Return m_CdapId
            End Get
            Set(ByVal value As Guid?)
                m_CdapId = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As DateTime?
            Get
                Return m_Date
            End Get
            Set(ByVal value As DateTime?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@PhotoRef")> _
        Public Property _PhotoRef() As String
            Get
                Return m_PhotoRef
            End Get
            Set(ByVal value As String)
                m_PhotoRef = value
            End Set
        End Property

        <StoredProcParameter("@PhotoImported")> _
        Public Property _PhotoImported() As Boolean
            Get
                Return m_PhotoImported
            End Get
            Set(ByVal value As Boolean)
                m_PhotoImported = value
            End Set
        End Property

        <StoredProcParameter("@Comments")> _
        Public Property _Comments() As String
            Get
                Return m_Comments
            End Get
            Set(ByVal value As String)
                m_Comments = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildCDAP

            Dim _ChildCDAP As ChildCDAP
            _ChildCDAP = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildCDAPbyID", ID))
            Return _ChildCDAP

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildCDAP)

            Dim _ChildCDAPList As List(Of ChildCDAP)
            _ChildCDAPList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildCDAPTable"))
            Return _ChildCDAPList

        End Function

        Public Shared Sub SaveAll(ByVal ChildCDAPList As List(Of ChildCDAP))

            For Each _ChildCDAP As ChildCDAP In ChildCDAPList
                SaveRecord(_ChildCDAP)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildCDAP As ChildCDAP) As Guid
            DAL.SaveRecord(Session.ConnectionManager, ChildCDAP, "upsertChildCDAP")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildCDAPbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildCDAP")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildCDAP

            Dim _C As New ChildCDAP()

            If DR IsNot Nothing Then
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._CdapId = GetGUID(DR("cdap_id"))
                    _C._DayId = GetGUID(DR("day_id"))
                    _C._Date = GetDateTime(DR("date"))
                    _C._PhotoRef = DR("photo_ref").ToString.Trim
                    _C._PhotoImported = GetBoolean(DR("photo_imported"))
                    _C._Comments = DR("comments").ToString.Trim
                    _C._StaffId = GetGUID(DR("staff_id"))
                    _C._StaffName = DR("staff_name").ToString.Trim
                    _C._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildCDAP)

            Dim _ChildCDAPList As New List(Of ChildCDAP)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildCDAPList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildCDAPList

        End Function


#End Region

    End Class

End Namespace
