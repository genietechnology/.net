﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Payment
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_FamilyId As Guid?
        Dim m_PayDate As Date?
        Dim m_PayMethod As Guid?
        Dim m_MethodName As String
        Dim m_Ref1 As String
        Dim m_Ref2 As String
        Dim m_Ref3 As String
        Dim m_PayAmount As Decimal
        Dim m_StampUser As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FamilyId = Nothing
                ._PayDate = Nothing
                ._PayMethod = Nothing
                ._MethodName = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._PayAmount = 0
                ._StampUser = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FamilyId = Nothing
                ._PayDate = Nothing
                ._PayMethod = Nothing
                ._MethodName = ""
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._PayAmount = 0
                ._StampUser = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@FamilyId")> _
        Public Property _FamilyId() As Guid?
            Get
                Return m_FamilyId
            End Get
            Set(ByVal value As Guid?)
                m_FamilyId = value
            End Set
        End Property

        <StoredProcParameter("@PayDate")> _
        Public Property _PayDate() As Date?
            Get
                Return m_PayDate
            End Get
            Set(ByVal value As Date?)
                m_PayDate = value
            End Set
        End Property

        <StoredProcParameter("@PayMethod")> _
        Public Property _PayMethod() As Guid?
            Get
                Return m_PayMethod
            End Get
            Set(ByVal value As Guid?)
                m_PayMethod = value
            End Set
        End Property

        <StoredProcParameter("@MethodName")> _
        Public Property _MethodName() As String
            Get
                Return m_MethodName
            End Get
            Set(ByVal value As String)
                m_MethodName = value
            End Set
        End Property

        <StoredProcParameter("@Ref1")> _
        Public Property _Ref1() As String
            Get
                Return m_Ref1
            End Get
            Set(ByVal value As String)
                m_Ref1 = value
            End Set
        End Property

        <StoredProcParameter("@Ref2")> _
        Public Property _Ref2() As String
            Get
                Return m_Ref2
            End Get
            Set(ByVal value As String)
                m_Ref2 = value
            End Set
        End Property

        <StoredProcParameter("@Ref3")> _
        Public Property _Ref3() As String
            Get
                Return m_Ref3
            End Get
            Set(ByVal value As String)
                m_Ref3 = value
            End Set
        End Property

        <StoredProcParameter("@PayAmount")> _
        Public Property _PayAmount() As Decimal
            Get
                Return m_PayAmount
            End Get
            Set(ByVal value As Decimal)
                m_PayAmount = value
            End Set
        End Property

        <StoredProcParameter("@StampUser")> _
        Public Property _StampUser() As String
            Get
                Return m_StampUser
            End Get
            Set(ByVal value As String)
                m_StampUser = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Payment

            Dim _Payment As Payment
            _Payment = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPaymentbyID", ID))
            Return _Payment

        End Function

        Public Shared Function RetreiveAll() As List(Of Payment)

            Dim _PaymentList As List(Of Payment)
            _PaymentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPaymentTable"))
            Return _PaymentList

        End Function

        Public Shared Sub SaveAll(ByVal PaymentList As List(Of Payment))

            For Each _Payment As Payment In PaymentList
                SavePayment(_Payment)
            Next

        End Sub

        Public Shared Function SavePayment(ByVal Payment As Payment) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Payment, "upsertPayment")
        End Function

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Payment

            Dim _P As New Payment()

            If DR IsNot Nothing Then
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._FamilyId = GetGUID(DR("family_id"))
                    _P._PayDate = GetDate(DR("pay_date"))
                    _P._PayMethod = GetGUID(DR("pay_method"))
                    _P._MethodName = DR("method_name").ToString.Trim
                    _P._Ref1 = DR("ref_1").ToString.Trim
                    _P._Ref2 = DR("ref_2").ToString.Trim
                    _P._Ref3 = DR("ref_3").ToString.Trim
                    _P._PayAmount = GetDecimal(DR("pay_amount"))
                    _P._StampUser = DR("stamp_user").ToString.Trim
                    _P._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Payment)

            Dim _PaymentList As New List(Of Payment)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PaymentList.Add(PropertiesFromData(_DR))
            Next

            Return _PaymentList

        End Function


#End Region

    End Class

End Namespace
