﻿'*****************************************************
'Generated 11/04/2016 11:20:20 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildAttrib
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_Category As String
        Dim m_Attribute As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._Category = ""
                ._Attribute = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._Category = ""
                ._Attribute = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@Category")> _
        Public Property _Category() As String
            Get
                Return m_Category
            End Get
            Set(ByVal value As String)
                m_Category = value
            End Set
        End Property

        <StoredProcParameter("@Attribute")> _
        Public Property _Attribute() As String
            Get
                Return m_Attribute
            End Get
            Set(ByVal value As String)
                m_Attribute = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildAttrib

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildAttribbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildAttrib)

            Dim _ChildAttribList As List(Of ChildAttrib)
            _ChildAttribList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildAttribTable"))
            Return _ChildAttribList

        End Function

        Public Shared Sub SaveAll(ByVal ChildAttribList As List(Of ChildAttrib))

            For Each _ChildAttrib As ChildAttrib In ChildAttribList
                SaveRecord(_ChildAttrib)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildAttrib As ChildAttrib) As Guid
            ChildAttrib.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildAttrib, "upsertChildAttrib")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildAttribbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildAttrib")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildAttrib

            Dim _C As ChildAttrib = Nothing

            If DR IsNot Nothing Then
                _C = New ChildAttrib()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._Category = DR("category").ToString.Trim
                    _C._Attribute = DR("attribute").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildAttrib)

            Dim _ChildAttribList As New List(Of ChildAttrib)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildAttribList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildAttribList

        End Function


#End Region

    End Class

End Namespace
