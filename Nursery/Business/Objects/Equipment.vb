﻿'*****************************************************
'Generated 02/04/2017 21:47:41 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Equipment
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Status As String
        Dim m_Category As String
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_Location As String
        Dim m_Make As String
        Dim m_Model As String
        Dim m_Lifespan As Integer
        Dim m_AccessStaff As Boolean
        Dim m_AccessChild As Boolean
        Dim m_Used As Boolean
        Dim m_Condition As String
        Dim m_PurchaseDate As Date?
        Dim m_PurchasePrice As Decimal
        Dim m_Lease As Boolean
        Dim m_LeaseFrom As Date?
        Dim m_LeaseMonths As Integer
        Dim m_LeaseComplete As Date?
        Dim m_Warranty As Boolean
        Dim m_WarrantyMonths As Integer
        Dim m_WarrantyFrom As Date?
        Dim m_WarrantyExpires As Date?
        Dim m_Insp As Boolean
        Dim m_InspFreq As String
        Dim m_InspFreqDays As Integer
        Dim m_InspNext As Date?
        Dim m_InspLast As Date?
        Dim m_Serv As Boolean
        Dim m_ServFreq As String
        Dim m_ServFreqDays As Integer
        Dim m_ServNext As Date?
        Dim m_ServLast As Date?
        Dim m_Risk As Boolean
        Dim m_RiskFreq As String
        Dim m_RiskFreqDays As Integer
        Dim m_RiskNext As Date?
        Dim m_RiskLast As Date?
        Dim m_Test As Boolean
        Dim m_TestFreq As String
        Dim m_TestFreqDays As Integer
        Dim m_TestNext As Date?
        Dim m_TestLast As Date?
        Dim m_Photo As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Status = ""
                ._Category = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Location = ""
                ._Make = ""
                ._Model = ""
                ._Lifespan = 0
                ._AccessStaff = False
                ._AccessChild = False
                ._Used = False
                ._Condition = ""
                ._PurchaseDate = Nothing
                ._PurchasePrice = 0
                ._Lease = False
                ._LeaseFrom = Nothing
                ._LeaseMonths = 0
                ._LeaseComplete = Nothing
                ._Warranty = False
                ._WarrantyMonths = 0
                ._WarrantyFrom = Nothing
                ._WarrantyExpires = Nothing
                ._Insp = False
                ._InspFreq = ""
                ._InspFreqDays = 0
                ._InspNext = Nothing
                ._InspLast = Nothing
                ._Serv = False
                ._ServFreq = ""
                ._ServFreqDays = 0
                ._ServNext = Nothing
                ._ServLast = Nothing
                ._Risk = False
                ._RiskFreq = ""
                ._RiskFreqDays = 0
                ._RiskNext = Nothing
                ._RiskLast = Nothing
                ._Test = False
                ._TestFreq = ""
                ._TestFreqDays = 0
                ._TestNext = Nothing
                ._TestLast = Nothing
                ._Photo = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Status = ""
                ._Category = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Location = ""
                ._Make = ""
                ._Model = ""
                ._Lifespan = 0
                ._AccessStaff = False
                ._AccessChild = False
                ._Used = False
                ._Condition = ""
                ._PurchaseDate = Nothing
                ._PurchasePrice = 0
                ._Lease = False
                ._LeaseFrom = Nothing
                ._LeaseMonths = 0
                ._LeaseComplete = Nothing
                ._Warranty = False
                ._WarrantyMonths = 0
                ._WarrantyFrom = Nothing
                ._WarrantyExpires = Nothing
                ._Insp = False
                ._InspFreq = ""
                ._InspFreqDays = 0
                ._InspNext = Nothing
                ._InspLast = Nothing
                ._Serv = False
                ._ServFreq = ""
                ._ServFreqDays = 0
                ._ServNext = Nothing
                ._ServLast = Nothing
                ._Risk = False
                ._RiskFreq = ""
                ._RiskFreqDays = 0
                ._RiskNext = Nothing
                ._RiskLast = Nothing
                ._Test = False
                ._TestFreq = ""
                ._TestFreqDays = 0
                ._TestNext = Nothing
                ._TestLast = Nothing
                ._Photo = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Status")> _
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@Category")> _
        Public Property _Category() As String
            Get
                Return m_Category
            End Get
            Set(ByVal value As String)
                m_Category = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")> _
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@Location")> _
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@Make")> _
        Public Property _Make() As String
            Get
                Return m_Make
            End Get
            Set(ByVal value As String)
                m_Make = value
            End Set
        End Property

        <StoredProcParameter("@Model")> _
        Public Property _Model() As String
            Get
                Return m_Model
            End Get
            Set(ByVal value As String)
                m_Model = value
            End Set
        End Property

        <StoredProcParameter("@Lifespan")> _
        Public Property _Lifespan() As Integer
            Get
                Return m_Lifespan
            End Get
            Set(ByVal value As Integer)
                m_Lifespan = value
            End Set
        End Property

        <StoredProcParameter("@AccessStaff")> _
        Public Property _AccessStaff() As Boolean
            Get
                Return m_AccessStaff
            End Get
            Set(ByVal value As Boolean)
                m_AccessStaff = value
            End Set
        End Property

        <StoredProcParameter("@AccessChild")> _
        Public Property _AccessChild() As Boolean
            Get
                Return m_AccessChild
            End Get
            Set(ByVal value As Boolean)
                m_AccessChild = value
            End Set
        End Property

        <StoredProcParameter("@Used")> _
        Public Property _Used() As Boolean
            Get
                Return m_Used
            End Get
            Set(ByVal value As Boolean)
                m_Used = value
            End Set
        End Property

        <StoredProcParameter("@Condition")> _
        Public Property _Condition() As String
            Get
                Return m_Condition
            End Get
            Set(ByVal value As String)
                m_Condition = value
            End Set
        End Property

        <StoredProcParameter("@PurchaseDate")> _
        Public Property _PurchaseDate() As Date?
            Get
                Return m_PurchaseDate
            End Get
            Set(ByVal value As Date?)
                m_PurchaseDate = value
            End Set
        End Property

        <StoredProcParameter("@PurchasePrice")> _
        Public Property _PurchasePrice() As Decimal
            Get
                Return m_PurchasePrice
            End Get
            Set(ByVal value As Decimal)
                m_PurchasePrice = value
            End Set
        End Property

        <StoredProcParameter("@Lease")> _
        Public Property _Lease() As Boolean
            Get
                Return m_Lease
            End Get
            Set(ByVal value As Boolean)
                m_Lease = value
            End Set
        End Property

        <StoredProcParameter("@LeaseFrom")> _
        Public Property _LeaseFrom() As Date?
            Get
                Return m_LeaseFrom
            End Get
            Set(ByVal value As Date?)
                m_LeaseFrom = value
            End Set
        End Property

        <StoredProcParameter("@LeaseMonths")> _
        Public Property _LeaseMonths() As Integer
            Get
                Return m_LeaseMonths
            End Get
            Set(ByVal value As Integer)
                m_LeaseMonths = value
            End Set
        End Property

        <StoredProcParameter("@LeaseComplete")> _
        Public Property _LeaseComplete() As Date?
            Get
                Return m_LeaseComplete
            End Get
            Set(ByVal value As Date?)
                m_LeaseComplete = value
            End Set
        End Property

        <StoredProcParameter("@Warranty")> _
        Public Property _Warranty() As Boolean
            Get
                Return m_Warranty
            End Get
            Set(ByVal value As Boolean)
                m_Warranty = value
            End Set
        End Property

        <StoredProcParameter("@WarrantyMonths")> _
        Public Property _WarrantyMonths() As Integer
            Get
                Return m_WarrantyMonths
            End Get
            Set(ByVal value As Integer)
                m_WarrantyMonths = value
            End Set
        End Property

        <StoredProcParameter("@WarrantyFrom")> _
        Public Property _WarrantyFrom() As Date?
            Get
                Return m_WarrantyFrom
            End Get
            Set(ByVal value As Date?)
                m_WarrantyFrom = value
            End Set
        End Property

        <StoredProcParameter("@WarrantyExpires")> _
        Public Property _WarrantyExpires() As Date?
            Get
                Return m_WarrantyExpires
            End Get
            Set(ByVal value As Date?)
                m_WarrantyExpires = value
            End Set
        End Property

        <StoredProcParameter("@Insp")> _
        Public Property _Insp() As Boolean
            Get
                Return m_Insp
            End Get
            Set(ByVal value As Boolean)
                m_Insp = value
            End Set
        End Property

        <StoredProcParameter("@InspFreq")> _
        Public Property _InspFreq() As String
            Get
                Return m_InspFreq
            End Get
            Set(ByVal value As String)
                m_InspFreq = value
            End Set
        End Property

        <StoredProcParameter("@InspFreqDays")> _
        Public Property _InspFreqDays() As Integer
            Get
                Return m_InspFreqDays
            End Get
            Set(ByVal value As Integer)
                m_InspFreqDays = value
            End Set
        End Property

        <StoredProcParameter("@InspNext")> _
        Public Property _InspNext() As Date?
            Get
                Return m_InspNext
            End Get
            Set(ByVal value As Date?)
                m_InspNext = value
            End Set
        End Property

        <StoredProcParameter("@InspLast")> _
        Public Property _InspLast() As Date?
            Get
                Return m_InspLast
            End Get
            Set(ByVal value As Date?)
                m_InspLast = value
            End Set
        End Property

        <StoredProcParameter("@Serv")> _
        Public Property _Serv() As Boolean
            Get
                Return m_Serv
            End Get
            Set(ByVal value As Boolean)
                m_Serv = value
            End Set
        End Property

        <StoredProcParameter("@ServFreq")> _
        Public Property _ServFreq() As String
            Get
                Return m_ServFreq
            End Get
            Set(ByVal value As String)
                m_ServFreq = value
            End Set
        End Property

        <StoredProcParameter("@ServFreqDays")> _
        Public Property _ServFreqDays() As Integer
            Get
                Return m_ServFreqDays
            End Get
            Set(ByVal value As Integer)
                m_ServFreqDays = value
            End Set
        End Property

        <StoredProcParameter("@ServNext")> _
        Public Property _ServNext() As Date?
            Get
                Return m_ServNext
            End Get
            Set(ByVal value As Date?)
                m_ServNext = value
            End Set
        End Property

        <StoredProcParameter("@ServLast")> _
        Public Property _ServLast() As Date?
            Get
                Return m_ServLast
            End Get
            Set(ByVal value As Date?)
                m_ServLast = value
            End Set
        End Property

        <StoredProcParameter("@Risk")> _
        Public Property _Risk() As Boolean
            Get
                Return m_Risk
            End Get
            Set(ByVal value As Boolean)
                m_Risk = value
            End Set
        End Property

        <StoredProcParameter("@RiskFreq")> _
        Public Property _RiskFreq() As String
            Get
                Return m_RiskFreq
            End Get
            Set(ByVal value As String)
                m_RiskFreq = value
            End Set
        End Property

        <StoredProcParameter("@RiskFreqDays")> _
        Public Property _RiskFreqDays() As Integer
            Get
                Return m_RiskFreqDays
            End Get
            Set(ByVal value As Integer)
                m_RiskFreqDays = value
            End Set
        End Property

        <StoredProcParameter("@RiskNext")> _
        Public Property _RiskNext() As Date?
            Get
                Return m_RiskNext
            End Get
            Set(ByVal value As Date?)
                m_RiskNext = value
            End Set
        End Property

        <StoredProcParameter("@RiskLast")> _
        Public Property _RiskLast() As Date?
            Get
                Return m_RiskLast
            End Get
            Set(ByVal value As Date?)
                m_RiskLast = value
            End Set
        End Property

        <StoredProcParameter("@Test")> _
        Public Property _Test() As Boolean
            Get
                Return m_Test
            End Get
            Set(ByVal value As Boolean)
                m_Test = value
            End Set
        End Property

        <StoredProcParameter("@TestFreq")> _
        Public Property _TestFreq() As String
            Get
                Return m_TestFreq
            End Get
            Set(ByVal value As String)
                m_TestFreq = value
            End Set
        End Property

        <StoredProcParameter("@TestFreqDays")> _
        Public Property _TestFreqDays() As Integer
            Get
                Return m_TestFreqDays
            End Get
            Set(ByVal value As Integer)
                m_TestFreqDays = value
            End Set
        End Property

        <StoredProcParameter("@TestNext")> _
        Public Property _TestNext() As Date?
            Get
                Return m_TestNext
            End Get
            Set(ByVal value As Date?)
                m_TestNext = value
            End Set
        End Property

        <StoredProcParameter("@TestLast")> _
        Public Property _TestLast() As Date?
            Get
                Return m_TestLast
            End Get
            Set(ByVal value As Date?)
                m_TestLast = value
            End Set
        End Property

        <StoredProcParameter("@Photo")> _
        Public Property _Photo() As Guid?
            Get
                Return m_Photo
            End Get
            Set(ByVal value As Guid?)
                m_Photo = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Equipment

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEquipmentbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Equipment

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getEquipmentbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Equipment)

            Dim _EquipmentList As List(Of Equipment)
            _EquipmentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEquipmentTable"))
            Return _EquipmentList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Equipment)

            Dim _EquipmentList As List(Of Equipment)
            _EquipmentList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getEquipmentTable"))
            Return _EquipmentList

        End Function

        Public Shared Sub SaveAll(ByVal EquipmentList As List(Of Equipment))

            For Each _Equipment As Equipment In EquipmentList
                SaveRecord(_Equipment)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal EquipmentList As List(Of Equipment))

            For Each _Equipment As Equipment In EquipmentList
                SaveRecord(ConnectionString, _Equipment)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Equipment As Equipment) As Guid
            Equipment.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Equipment, "upsertEquipment")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Equipment As Equipment) As Guid
            Equipment.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Equipment, "upsertEquipment")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEquipmentbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteEquipmentbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertEquipment")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertEquipment")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Equipment

            Dim _E As Equipment = Nothing

            If DR IsNot Nothing Then
                _E = New Equipment()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._Name = DR("name").ToString.Trim
                    _E._Status = DR("status").ToString.Trim
                    _E._Category = DR("category").ToString.Trim
                    _E._SiteId = GetGUID(DR("site_id"))
                    _E._SiteName = DR("site_name").ToString.Trim
                    _E._Location = DR("location").ToString.Trim
                    _E._Make = DR("make").ToString.Trim
                    _E._Model = DR("model").ToString.Trim
                    _E._Lifespan = GetInteger(DR("lifespan"))
                    _E._AccessStaff = GetBoolean(DR("access_staff"))
                    _E._AccessChild = GetBoolean(DR("access_child"))
                    _E._Used = GetBoolean(DR("used"))
                    _E._Condition = DR("condition").ToString.Trim
                    _E._PurchaseDate = GetDate(DR("purchase_date"))
                    _E._PurchasePrice = GetDecimal(DR("purchase_price"))
                    _E._Lease = GetBoolean(DR("lease"))
                    _E._LeaseFrom = GetDate(DR("lease_from"))
                    _E._LeaseMonths = GetInteger(DR("lease_months"))
                    _E._LeaseComplete = GetDate(DR("lease_complete"))
                    _E._Warranty = GetBoolean(DR("warranty"))
                    _E._WarrantyMonths = GetInteger(DR("warranty_months"))
                    _E._WarrantyFrom = GetDate(DR("warranty_from"))
                    _E._WarrantyExpires = GetDate(DR("warranty_expires"))
                    _E._Insp = GetBoolean(DR("insp"))
                    _E._InspFreq = DR("insp_freq").ToString.Trim
                    _E._InspFreqDays = GetInteger(DR("insp_freq_days"))
                    _E._InspNext = GetDate(DR("insp_next"))
                    _E._InspLast = GetDate(DR("insp_last"))
                    _E._Serv = GetBoolean(DR("serv"))
                    _E._ServFreq = DR("serv_freq").ToString.Trim
                    _E._ServFreqDays = GetInteger(DR("serv_freq_days"))
                    _E._ServNext = GetDate(DR("serv_next"))
                    _E._ServLast = GetDate(DR("serv_last"))
                    _E._Risk = GetBoolean(DR("risk"))
                    _E._RiskFreq = DR("risk_freq").ToString.Trim
                    _E._RiskFreqDays = GetInteger(DR("risk_freq_days"))
                    _E._RiskNext = GetDate(DR("risk_next"))
                    _E._RiskLast = GetDate(DR("risk_last"))
                    _E._Test = GetBoolean(DR("test"))
                    _E._TestFreq = DR("test_freq").ToString.Trim
                    _E._TestFreqDays = GetInteger(DR("test_freq_days"))
                    _E._TestNext = GetDate(DR("test_next"))
                    _E._TestLast = GetDate(DR("test_last"))
                    _E._Photo = GetGUID(DR("photo"))

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Equipment)

            Dim _EquipmentList As New List(Of Equipment)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EquipmentList.Add(PropertiesFromData(_DR))
            Next

            Return _EquipmentList

        End Function


#End Region

    End Class

End Namespace
