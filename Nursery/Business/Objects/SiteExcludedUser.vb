﻿'*****************************************************
'Generated 13/09/2016 22:47:57 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class SiteExcludedUser
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_UserId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._UserId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._UserId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As SiteExcludedUser

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSiteExcludedUserbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of SiteExcludedUser)

            Dim _SiteExcludedUserList As List(Of SiteExcludedUser)
            _SiteExcludedUserList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSiteExcludedUserTable"))
            Return _SiteExcludedUserList

        End Function

        Public Shared Sub SaveAll(ByVal SiteExcludedUserList As List(Of SiteExcludedUser))

            For Each _SiteExcludedUser As SiteExcludedUser In SiteExcludedUserList
                SaveRecord(_SiteExcludedUser)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal SiteExcludedUser As SiteExcludedUser) As Guid
            SiteExcludedUser.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, SiteExcludedUser, "upsertSiteExcludedUser")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSiteExcludedUserbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSiteExcludedUser")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As SiteExcludedUser

            Dim _S As SiteExcludedUser = Nothing

            If DR IsNot Nothing Then
                _S = New SiteExcludedUser()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._SiteId = GetGUID(DR("site_id"))
                    _S._UserId = GetGUID(DR("user_id"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of SiteExcludedUser)

            Dim _SiteExcludedUserList As New List(Of SiteExcludedUser)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SiteExcludedUserList.Add(PropertiesFromData(_DR))
            Next

            Return _SiteExcludedUserList

        End Function


#End Region

    End Class

End Namespace
