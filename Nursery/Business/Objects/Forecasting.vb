﻿'*****************************************************
'Generated 19/03/2017 11:05:41 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Forecasting
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_FcYear As Integer
        Dim m_FcMonth As Integer
        Dim m_InvFrom As Date?
        Dim m_InvTo As Date?
        Dim m_BatchId As Guid?
        Dim m_StarterCount As Integer
        Dim m_StarterValue As Decimal
        Dim m_LeaverCount As Integer
        Dim m_LeaverValue As Decimal
        Dim m_NonFundedHours As Decimal
        Dim m_NonFundedValue As Decimal
        Dim m_FundedHours As Decimal
        Dim m_FundedValue As Decimal
        Dim m_TotalHours As Decimal
        Dim m_InvoiceCount As Integer
        Dim m_InvoiceSub As Decimal
        Dim m_InvoiceDiscount As Decimal
        Dim m_InvoiceTotal As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._SiteName = ""
                ._FcYear = 0
                ._FcMonth = 0
                ._InvFrom = Nothing
                ._InvTo = Nothing
                ._BatchId = Nothing
                ._StarterCount = 0
                ._StarterValue = 0
                ._LeaverCount = 0
                ._LeaverValue = 0
                ._NonFundedHours = 0
                ._NonFundedValue = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._TotalHours = 0
                ._InvoiceCount = 0
                ._InvoiceSub = 0
                ._InvoiceDiscount = 0
                ._InvoiceTotal = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._SiteName = ""
                ._FcYear = 0
                ._FcMonth = 0
                ._InvFrom = Nothing
                ._InvTo = Nothing
                ._BatchId = Nothing
                ._StarterCount = 0
                ._StarterValue = 0
                ._LeaverCount = 0
                ._LeaverValue = 0
                ._NonFundedHours = 0
                ._NonFundedValue = 0
                ._FundedHours = 0
                ._FundedValue = 0
                ._TotalHours = 0
                ._InvoiceCount = 0
                ._InvoiceSub = 0
                ._InvoiceDiscount = 0
                ._InvoiceTotal = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@FcYear")>
        Public Property _FcYear() As Integer
            Get
                Return m_FcYear
            End Get
            Set(ByVal value As Integer)
                m_FcYear = value
            End Set
        End Property

        <StoredProcParameter("@FcMonth")>
        Public Property _FcMonth() As Integer
            Get
                Return m_FcMonth
            End Get
            Set(ByVal value As Integer)
                m_FcMonth = value
            End Set
        End Property

        <StoredProcParameter("@InvFrom")>
        Public Property _InvFrom() As Date?
            Get
                Return m_InvFrom
            End Get
            Set(ByVal value As Date?)
                m_InvFrom = value
            End Set
        End Property

        <StoredProcParameter("@InvTo")>
        Public Property _InvTo() As Date?
            Get
                Return m_InvTo
            End Get
            Set(ByVal value As Date?)
                m_InvTo = value
            End Set
        End Property

        <StoredProcParameter("@BatchId")>
        Public Property _BatchId() As Guid?
            Get
                Return m_BatchId
            End Get
            Set(ByVal value As Guid?)
                m_BatchId = value
            End Set
        End Property

        <StoredProcParameter("@StarterCount")>
        Public Property _StarterCount() As Integer
            Get
                Return m_StarterCount
            End Get
            Set(ByVal value As Integer)
                m_StarterCount = value
            End Set
        End Property

        <StoredProcParameter("@StarterValue")>
        Public Property _StarterValue() As Decimal
            Get
                Return m_StarterValue
            End Get
            Set(ByVal value As Decimal)
                m_StarterValue = value
            End Set
        End Property

        <StoredProcParameter("@LeaverCount")>
        Public Property _LeaverCount() As Integer
            Get
                Return m_LeaverCount
            End Get
            Set(ByVal value As Integer)
                m_LeaverCount = value
            End Set
        End Property

        <StoredProcParameter("@LeaverValue")>
        Public Property _LeaverValue() As Decimal
            Get
                Return m_LeaverValue
            End Get
            Set(ByVal value As Decimal)
                m_LeaverValue = value
            End Set
        End Property

        <StoredProcParameter("@NonFundedHours")>
        Public Property _NonFundedHours() As Decimal
            Get
                Return m_NonFundedHours
            End Get
            Set(ByVal value As Decimal)
                m_NonFundedHours = value
            End Set
        End Property

        <StoredProcParameter("@NonFundedValue")>
        Public Property _NonFundedValue() As Decimal
            Get
                Return m_NonFundedValue
            End Get
            Set(ByVal value As Decimal)
                m_NonFundedValue = value
            End Set
        End Property

        <StoredProcParameter("@FundedHours")>
        Public Property _FundedHours() As Decimal
            Get
                Return m_FundedHours
            End Get
            Set(ByVal value As Decimal)
                m_FundedHours = value
            End Set
        End Property

        <StoredProcParameter("@FundedValue")>
        Public Property _FundedValue() As Decimal
            Get
                Return m_FundedValue
            End Get
            Set(ByVal value As Decimal)
                m_FundedValue = value
            End Set
        End Property

        <StoredProcParameter("@TotalHours")>
        Public Property _TotalHours() As Decimal
            Get
                Return m_TotalHours
            End Get
            Set(ByVal value As Decimal)
                m_TotalHours = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceCount")>
        Public Property _InvoiceCount() As Integer
            Get
                Return m_InvoiceCount
            End Get
            Set(ByVal value As Integer)
                m_InvoiceCount = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceSub")>
        Public Property _InvoiceSub() As Decimal
            Get
                Return m_InvoiceSub
            End Get
            Set(ByVal value As Decimal)
                m_InvoiceSub = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceDiscount")>
        Public Property _InvoiceDiscount() As Decimal
            Get
                Return m_InvoiceDiscount
            End Get
            Set(ByVal value As Decimal)
                m_InvoiceDiscount = value
            End Set
        End Property

        <StoredProcParameter("@InvoiceTotal")>
        Public Property _InvoiceTotal() As Decimal
            Get
                Return m_InvoiceTotal
            End Get
            Set(ByVal value As Decimal)
                m_InvoiceTotal = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Forecasting

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getForecastingbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Forecasting

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getForecastingbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Forecasting)

            Dim _ForecastingList As List(Of Forecasting)
            _ForecastingList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getForecastingTable"))
            Return _ForecastingList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Forecasting)

            Dim _ForecastingList As List(Of Forecasting)
            _ForecastingList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getForecastingTable"))
            Return _ForecastingList

        End Function

        Public Shared Sub SaveAll(ByVal ForecastingList As List(Of Forecasting))

            For Each _Forecasting As Forecasting In ForecastingList
                SaveRecord(_Forecasting)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ForecastingList As List(Of Forecasting))

            For Each _Forecasting As Forecasting In ForecastingList
                SaveRecord(ConnectionString, _Forecasting)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Forecasting As Forecasting) As Guid
            Forecasting.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Forecasting, "upsertForecasting")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Forecasting As Forecasting) As Guid
            Forecasting.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Forecasting, "upsertForecasting")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteForecastingbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteForecastingbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertForecasting")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertForecasting")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Forecasting

            Dim _F As Forecasting = Nothing

            If DR IsNot Nothing Then
                _F = New Forecasting()
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._SiteId = GetGUID(DR("site_id"))
                    _F._SiteName = DR("site_name").ToString.Trim
                    _F._FcYear = GetInteger(DR("fc_year"))
                    _F._FcMonth = GetInteger(DR("fc_month"))
                    _F._InvFrom = GetDate(DR("inv_from"))
                    _F._InvTo = GetDate(DR("inv_to"))
                    _F._BatchId = GetGUID(DR("batch_id"))
                    _F._StarterCount = GetInteger(DR("starter_count"))
                    _F._StarterValue = GetDecimal(DR("starter_value"))
                    _F._LeaverCount = GetInteger(DR("leaver_count"))
                    _F._LeaverValue = GetDecimal(DR("leaver_value"))
                    _F._NonFundedHours = GetDecimal(DR("non_funded_hours"))
                    _F._NonFundedValue = GetDecimal(DR("non_funded_value"))
                    _F._FundedHours = GetDecimal(DR("funded_hours"))
                    _F._FundedValue = GetDecimal(DR("funded_value"))
                    _F._TotalHours = GetDecimal(DR("total_hours"))
                    _F._InvoiceCount = GetInteger(DR("invoice_count"))
                    _F._InvoiceSub = GetDecimal(DR("invoice_sub"))
                    _F._InvoiceDiscount = GetDecimal(DR("invoice_discount"))
                    _F._InvoiceTotal = GetDecimal(DR("invoice_total"))

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Forecasting)

            Dim _ForecastingList As New List(Of Forecasting)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ForecastingList.Add(PropertiesFromData(_DR))
            Next

            Return _ForecastingList

        End Function


#End Region

    End Class

End Namespace
