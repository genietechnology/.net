﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Meal
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Breakfast As Boolean
        Dim m_Snack As Boolean
        Dim m_Lunch As Boolean
        Dim m_Tea As Boolean
        Dim m_Dessert As Boolean
        Dim m_Description As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Breakfast = False
                ._Snack = False
                ._Lunch = False
                ._Tea = False
                ._Dessert = False
                ._Description = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Breakfast = False
                ._Snack = False
                ._Lunch = False
                ._Tea = False
                ._Dessert = False
                ._Description = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Breakfast")> _
        Public Property _Breakfast() As Boolean
            Get
                Return m_Breakfast
            End Get
            Set(ByVal value As Boolean)
                m_Breakfast = value
            End Set
        End Property

        <StoredProcParameter("@Snack")> _
        Public Property _Snack() As Boolean
            Get
                Return m_Snack
            End Get
            Set(ByVal value As Boolean)
                m_Snack = value
            End Set
        End Property

        <StoredProcParameter("@Lunch")> _
        Public Property _Lunch() As Boolean
            Get
                Return m_Lunch
            End Get
            Set(ByVal value As Boolean)
                m_Lunch = value
            End Set
        End Property

        <StoredProcParameter("@Tea")> _
        Public Property _Tea() As Boolean
            Get
                Return m_Tea
            End Get
            Set(ByVal value As Boolean)
                m_Tea = value
            End Set
        End Property

        <StoredProcParameter("@Dessert")> _
        Public Property _Dessert() As Boolean
            Get
                Return m_Dessert
            End Get
            Set(ByVal value As Boolean)
                m_Dessert = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Meal

            Dim _Meal As Meal
            _Meal = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMealbyID", ID))
            Return _Meal

        End Function

        Public Shared Function RetreiveAll() As List(Of Meal)

            Dim _MealList As List(Of Meal)
            _MealList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMealTable"))
            Return _MealList

        End Function

        Public Shared Sub SaveAll(ByVal MealList As List(Of Meal))

            For Each _Meal As Meal In MealList
                SaveRecord(_Meal)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Meal As Meal) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Meal, "upsertMeal")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMealbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertMeal")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Meal

            Dim _M As New Meal()

            If DR IsNot Nothing Then
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._Name = DR("name").ToString.Trim
                    _M._Breakfast = GetBoolean(DR("breakfast"))
                    _M._Snack = GetBoolean(DR("snack"))
                    _M._Lunch = GetBoolean(DR("lunch"))
                    _M._Tea = GetBoolean(DR("tea"))
                    _M._Dessert = GetBoolean(DR("dessert"))
                    _M._Description = DR("description").ToString.Trim

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Meal)

            Dim _MealList As New List(Of Meal)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MealList.Add(PropertiesFromData(_DR))
            Next

            Return _MealList

        End Function


#End Region

    End Class

End Namespace
