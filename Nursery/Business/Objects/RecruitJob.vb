﻿'*****************************************************
'Generated 05/04/2017 12:50:50 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RecruitJob
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Ref As String
        Dim m_Name As String
        Dim m_Status As String
        Dim m_SiteId As Guid?
        Dim m_SiteName As String
        Dim m_Department As String
        Dim m_JobTitle As String
        Dim m_LineMan As String
        Dim m_ContHours As Decimal
        Dim m_ContSalary As Decimal
        Dim m_ContPerhr As Decimal
        Dim m_DateFrom As Date?
        Dim m_DateTo As Date?
        Dim m_DateInt As Date?
        Dim m_DateStart As Date?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Ref = ""
                ._Name = ""
                ._Status = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Department = ""
                ._JobTitle = ""
                ._LineMan = ""
                ._ContHours = 0
                ._ContSalary = 0
                ._ContPerhr = 0
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._DateInt = Nothing
                ._DateStart = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Ref = ""
                ._Name = ""
                ._Status = ""
                ._SiteId = Nothing
                ._SiteName = ""
                ._Department = ""
                ._JobTitle = ""
                ._LineMan = ""
                ._ContHours = 0
                ._ContSalary = 0
                ._ContPerhr = 0
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._DateInt = Nothing
                ._DateStart = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Ref")> _
        Public Property _Ref() As String
            Get
                Return m_Ref
            End Get
            Set(ByVal value As String)
                m_Ref = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Status")> _
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@SiteName")> _
        Public Property _SiteName() As String
            Get
                Return m_SiteName
            End Get
            Set(ByVal value As String)
                m_SiteName = value
            End Set
        End Property

        <StoredProcParameter("@Department")> _
        Public Property _Department() As String
            Get
                Return m_Department
            End Get
            Set(ByVal value As String)
                m_Department = value
            End Set
        End Property

        <StoredProcParameter("@JobTitle")> _
        Public Property _JobTitle() As String
            Get
                Return m_JobTitle
            End Get
            Set(ByVal value As String)
                m_JobTitle = value
            End Set
        End Property

        <StoredProcParameter("@LineMan")> _
        Public Property _LineMan() As String
            Get
                Return m_LineMan
            End Get
            Set(ByVal value As String)
                m_LineMan = value
            End Set
        End Property

        <StoredProcParameter("@ContHours")> _
        Public Property _ContHours() As Decimal
            Get
                Return m_ContHours
            End Get
            Set(ByVal value As Decimal)
                m_ContHours = value
            End Set
        End Property

        <StoredProcParameter("@ContSalary")> _
        Public Property _ContSalary() As Decimal
            Get
                Return m_ContSalary
            End Get
            Set(ByVal value As Decimal)
                m_ContSalary = value
            End Set
        End Property

        <StoredProcParameter("@ContPerhr")> _
        Public Property _ContPerhr() As Decimal
            Get
                Return m_ContPerhr
            End Get
            Set(ByVal value As Decimal)
                m_ContPerhr = value
            End Set
        End Property

        <StoredProcParameter("@DateFrom")> _
        Public Property _DateFrom() As Date?
            Get
                Return m_DateFrom
            End Get
            Set(ByVal value As Date?)
                m_DateFrom = value
            End Set
        End Property

        <StoredProcParameter("@DateTo")> _
        Public Property _DateTo() As Date?
            Get
                Return m_DateTo
            End Get
            Set(ByVal value As Date?)
                m_DateTo = value
            End Set
        End Property

        <StoredProcParameter("@DateInt")> _
        Public Property _DateInt() As Date?
            Get
                Return m_DateInt
            End Get
            Set(ByVal value As Date?)
                m_DateInt = value
            End Set
        End Property

        <StoredProcParameter("@DateStart")> _
        Public Property _DateStart() As Date?
            Get
                Return m_DateStart
            End Get
            Set(ByVal value As Date?)
                m_DateStart = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RecruitJob

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRecruitJobbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RecruitJob

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRecruitJobbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RecruitJob)

            Dim _RecruitJobList As List(Of RecruitJob)
            _RecruitJobList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRecruitJobTable"))
            Return _RecruitJobList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RecruitJob)

            Dim _RecruitJobList As List(Of RecruitJob)
            _RecruitJobList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRecruitJobTable"))
            Return _RecruitJobList

        End Function

        Public Shared Sub SaveAll(ByVal RecruitJobList As List(Of RecruitJob))

            For Each _RecruitJob As RecruitJob In RecruitJobList
                SaveRecord(_RecruitJob)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RecruitJobList As List(Of RecruitJob))

            For Each _RecruitJob As RecruitJob In RecruitJobList
                SaveRecord(ConnectionString, _RecruitJob)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RecruitJob As RecruitJob) As Guid
            RecruitJob.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RecruitJob, "upsertRecruitJob")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RecruitJob As RecruitJob) As Guid
            RecruitJob.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RecruitJob, "upsertRecruitJob")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRecruitJobbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRecruitJobbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRecruitJob")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRecruitJob")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RecruitJob

            Dim _R As RecruitJob = Nothing

            If DR IsNot Nothing Then
                _R = New RecruitJob()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._Ref = DR("ref").ToString.Trim
                    _R._Name = DR("name").ToString.Trim
                    _R._Status = DR("status").ToString.Trim
                    _R._SiteId = GetGUID(DR("site_id"))
                    _R._SiteName = DR("site_name").ToString.Trim
                    _R._Department = DR("department").ToString.Trim
                    _R._JobTitle = DR("job_title").ToString.Trim
                    _R._LineMan = DR("line_man").ToString.Trim
                    _R._ContHours = GetDecimal(DR("cont_hours"))
                    _R._ContSalary = GetDecimal(DR("cont_salary"))
                    _R._ContPerhr = GetDecimal(DR("cont_perhr"))
                    _R._DateFrom = GetDate(DR("date_from"))
                    _R._DateTo = GetDate(DR("date_to"))
                    _R._DateInt = GetDate(DR("date_int"))
                    _R._DateStart = GetDate(DR("date_start"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RecruitJob)

            Dim _RecruitJobList As New List(Of RecruitJob)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RecruitJobList.Add(PropertiesFromData(_DR))
            Next

            Return _RecruitJobList

        End Function


#End Region

    End Class

End Namespace
