﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class FamilyStatement
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_FamilyId As Guid?
        Dim m_FamilyName As String
        Dim m_StatementDate As Date?
        Dim m_Bfwd As Decimal
        Dim m_Charges As Decimal
        Dim m_Payments As Decimal
        Dim m_Balance As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._StatementDate = Nothing
                ._Bfwd = 0
                ._Charges = 0
                ._Payments = 0
                ._Balance = 0

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FamilyId = Nothing
                ._FamilyName = ""
                ._StatementDate = Nothing
                ._Bfwd = 0
                ._Charges = 0
                ._Payments = 0
                ._Balance = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@FamilyId")> _
        Public Property _FamilyId() As Guid?
            Get
                Return m_FamilyId
            End Get
            Set(ByVal value As Guid?)
                m_FamilyId = value
            End Set
        End Property

        <StoredProcParameter("@FamilyName")> _
        Public Property _FamilyName() As String
            Get
                Return m_FamilyName
            End Get
            Set(ByVal value As String)
                m_FamilyName = value
            End Set
        End Property

        <StoredProcParameter("@StatementDate")> _
        Public Property _StatementDate() As Date?
            Get
                Return m_StatementDate
            End Get
            Set(ByVal value As Date?)
                m_StatementDate = value
            End Set
        End Property

        <StoredProcParameter("@Bfwd")> _
        Public Property _Bfwd() As Decimal
            Get
                Return m_Bfwd
            End Get
            Set(ByVal value As Decimal)
                m_Bfwd = value
            End Set
        End Property

        <StoredProcParameter("@Charges")> _
        Public Property _Charges() As Decimal
            Get
                Return m_Charges
            End Get
            Set(ByVal value As Decimal)
                m_Charges = value
            End Set
        End Property

        <StoredProcParameter("@Payments")> _
        Public Property _Payments() As Decimal
            Get
                Return m_Payments
            End Get
            Set(ByVal value As Decimal)
                m_Payments = value
            End Set
        End Property

        <StoredProcParameter("@Balance")> _
        Public Property _Balance() As Decimal
            Get
                Return m_Balance
            End Get
            Set(ByVal value As Decimal)
                m_Balance = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As FamilyStatement

            Dim _FamilyStatement As FamilyStatement
            _FamilyStatement = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getFamilyStatementbyID", ID))
            Return _FamilyStatement

        End Function

        Public Shared Function RetreiveAll() As List(Of FamilyStatement)

            Dim _FamilyStatementList As List(Of FamilyStatement)
            _FamilyStatementList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getFamilyStatementTable"))
            Return _FamilyStatementList

        End Function

        Public Shared Sub SaveAll(ByVal FamilyStatementList As List(Of FamilyStatement))

            For Each _FamilyStatement As FamilyStatement In FamilyStatementList
                SaveFamilyStatement(_FamilyStatement)
            Next

        End Sub

        Public Shared Function SaveFamilyStatement(ByVal FamilyStatement As FamilyStatement) As Guid
            DAL.SaveRecord(Session.ConnectionManager, FamilyStatement, "upsertFamilyStatement")
        End Function

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As FamilyStatement

            Dim _F As New FamilyStatement()

            If DR IsNot Nothing Then
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._FamilyId = GetGUID(DR("family_id"))
                    _F._FamilyName = DR("family_name").ToString.Trim
                    _F._StatementDate = GetDate(DR("statement_date"))
                    _F._Bfwd = GetDecimal(DR("bfwd"))
                    _F._Charges = GetDecimal(DR("charges"))
                    _F._Payments = GetDecimal(DR("payments"))
                    _F._Balance = GetDecimal(DR("balance"))

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of FamilyStatement)

            Dim _FamilyStatementList As New List(Of FamilyStatement)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _FamilyStatementList.Add(PropertiesFromData(_DR))
            Next

            Return _FamilyStatementList

        End Function


#End Region

    End Class

End Namespace
