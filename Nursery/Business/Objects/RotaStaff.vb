﻿'*****************************************************
'Generated 16/05/2017 17:33:42 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RotaStaff
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RsRotaId As Guid?
        Dim m_RsStatus As String
        Dim m_RsDate As Date?
        Dim m_RsRatioId As Guid?
        Dim m_RsRoom As String
        Dim m_RsRoomColour As String
        Dim m_RsStart As TimeSpan?
        Dim m_RsFinish As TimeSpan?
        Dim m_RsHours As Decimal
        Dim m_RsCost As Decimal
        Dim m_RsStaffId As Guid?
        Dim m_RsStaffName As String
        Dim m_RsStaffAge As Integer
        Dim m_RsStaffLevel As Integer
        Dim m_RsStaffRate As Decimal
        Dim m_RsStaffPaed As Boolean
        Dim m_RsStaffFaid As Boolean
        Dim m_RsStaffSenco As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RsRotaId = Nothing
                ._RsStatus = ""
                ._RsDate = Nothing
                ._RsRatioId = Nothing
                ._RsRoom = ""
                ._RsRoomColour = ""
                ._RsStart = Nothing
                ._RsFinish = Nothing
                ._RsHours = 0
                ._RsCost = 0
                ._RsStaffId = Nothing
                ._RsStaffName = ""
                ._RsStaffAge = 0
                ._RsStaffLevel = 0
                ._RsStaffRate = 0
                ._RsStaffPaed = False
                ._RsStaffFaid = False
                ._RsStaffSenco = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RsRotaId = Nothing
                ._RsStatus = ""
                ._RsDate = Nothing
                ._RsRatioId = Nothing
                ._RsRoom = ""
                ._RsRoomColour = ""
                ._RsStart = Nothing
                ._RsFinish = Nothing
                ._RsHours = 0
                ._RsCost = 0
                ._RsStaffId = Nothing
                ._RsStaffName = ""
                ._RsStaffAge = 0
                ._RsStaffLevel = 0
                ._RsStaffRate = 0
                ._RsStaffPaed = False
                ._RsStaffFaid = False
                ._RsStaffSenco = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RsRotaId")>
        Public Property _RsRotaId() As Guid?
            Get
                Return m_RsRotaId
            End Get
            Set(ByVal value As Guid?)
                m_RsRotaId = value
            End Set
        End Property

        <StoredProcParameter("@RsStatus")>
        Public Property _RsStatus() As String
            Get
                Return m_RsStatus
            End Get
            Set(ByVal value As String)
                m_RsStatus = value
            End Set
        End Property

        <StoredProcParameter("@RsDate")>
        Public Property _RsDate() As Date?
            Get
                Return m_RsDate
            End Get
            Set(ByVal value As Date?)
                m_RsDate = value
            End Set
        End Property

        <StoredProcParameter("@RsRatioId")>
        Public Property _RsRatioId() As Guid?
            Get
                Return m_RsRatioId
            End Get
            Set(ByVal value As Guid?)
                m_RsRatioId = value
            End Set
        End Property

        <StoredProcParameter("@RsRoom")>
        Public Property _RsRoom() As String
            Get
                Return m_RsRoom
            End Get
            Set(ByVal value As String)
                m_RsRoom = value
            End Set
        End Property

        <StoredProcParameter("@RsRoomColour")>
        Public Property _RsRoomColour() As String
            Get
                Return m_RsRoomColour
            End Get
            Set(ByVal value As String)
                m_RsRoomColour = value
            End Set
        End Property

        <StoredProcParameter("@RsStart")>
        Public Property _RsStart() As TimeSpan?
            Get
                Return m_RsStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_RsStart = value
            End Set
        End Property

        <StoredProcParameter("@RsFinish")>
        Public Property _RsFinish() As TimeSpan?
            Get
                Return m_RsFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_RsFinish = value
            End Set
        End Property

        <StoredProcParameter("@RsHours")>
        Public Property _RsHours() As Decimal
            Get
                Return m_RsHours
            End Get
            Set(ByVal value As Decimal)
                m_RsHours = value
            End Set
        End Property

        <StoredProcParameter("@RsCost")>
        Public Property _RsCost() As Decimal
            Get
                Return m_RsCost
            End Get
            Set(ByVal value As Decimal)
                m_RsCost = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffId")>
        Public Property _RsStaffId() As Guid?
            Get
                Return m_RsStaffId
            End Get
            Set(ByVal value As Guid?)
                m_RsStaffId = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffName")>
        Public Property _RsStaffName() As String
            Get
                Return m_RsStaffName
            End Get
            Set(ByVal value As String)
                m_RsStaffName = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffAge")>
        Public Property _RsStaffAge() As Integer
            Get
                Return m_RsStaffAge
            End Get
            Set(ByVal value As Integer)
                m_RsStaffAge = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffLevel")>
        Public Property _RsStaffLevel() As Integer
            Get
                Return m_RsStaffLevel
            End Get
            Set(ByVal value As Integer)
                m_RsStaffLevel = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffRate")>
        Public Property _RsStaffRate() As Decimal
            Get
                Return m_RsStaffRate
            End Get
            Set(ByVal value As Decimal)
                m_RsStaffRate = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffPaed")>
        Public Property _RsStaffPaed() As Boolean
            Get
                Return m_RsStaffPaed
            End Get
            Set(ByVal value As Boolean)
                m_RsStaffPaed = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffFaid")>
        Public Property _RsStaffFaid() As Boolean
            Get
                Return m_RsStaffFaid
            End Get
            Set(ByVal value As Boolean)
                m_RsStaffFaid = value
            End Set
        End Property

        <StoredProcParameter("@RsStaffSenco")>
        Public Property _RsStaffSenco() As Boolean
            Get
                Return m_RsStaffSenco
            End Get
            Set(ByVal value As Boolean)
                m_RsStaffSenco = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RotaStaff

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRotaStaffbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RotaStaff

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRotaStaffbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RotaStaff)

            Dim _RotaStaffList As List(Of RotaStaff)
            _RotaStaffList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRotaStaffTable"))
            Return _RotaStaffList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RotaStaff)

            Dim _RotaStaffList As List(Of RotaStaff)
            _RotaStaffList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRotaStaffTable"))
            Return _RotaStaffList

        End Function

        Public Shared Sub SaveAll(ByVal RotaStaffList As List(Of RotaStaff))

            For Each _RotaStaff As RotaStaff In RotaStaffList
                SaveRecord(_RotaStaff)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RotaStaffList As List(Of RotaStaff))

            For Each _RotaStaff As RotaStaff In RotaStaffList
                SaveRecord(ConnectionString, _RotaStaff)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RotaStaff As RotaStaff) As Guid
            RotaStaff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaStaff, "upsertRotaStaff")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RotaStaff As RotaStaff) As Guid
            RotaStaff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaStaff, "upsertRotaStaff")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRotaStaffbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRotaStaffbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaStaff")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaStaff")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RotaStaff

            Dim _R As RotaStaff = Nothing

            If DR IsNot Nothing Then
                _R = New RotaStaff()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._RsRotaId = GetGUID(DR("rs_rota_id"))
                    _R._RsStatus = DR("rs_status").ToString.Trim
                    _R._RsDate = GetDate(DR("rs_date"))
                    _R._RsRatioId = GetGUID(DR("rs_ratio_id"))
                    _R._RsRoom = DR("rs_room").ToString.Trim
                    _R._RsRoomColour = DR("rs_room_colour").ToString.Trim
                    _R._RsStart = GetTimeSpan(DR("rs_start"))
                    _R._RsFinish = GetTimeSpan(DR("rs_finish"))
                    _R._RsHours = GetDecimal(DR("rs_hours"))
                    _R._RsCost = GetDecimal(DR("rs_cost"))
                    _R._RsStaffId = GetGUID(DR("rs_staff_id"))
                    _R._RsStaffName = DR("rs_staff_name").ToString.Trim
                    _R._RsStaffAge = GetInteger(DR("rs_staff_age"))
                    _R._RsStaffLevel = GetInteger(DR("rs_staff_level"))
                    _R._RsStaffRate = GetDecimal(DR("rs_staff_rate"))
                    _R._RsStaffPaed = GetBoolean(DR("rs_staff_paed"))
                    _R._RsStaffFaid = GetBoolean(DR("rs_staff_faid"))
                    _R._RsStaffSenco = GetBoolean(DR("rs_staff_senco"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RotaStaff)

            Dim _RotaStaffList As New List(Of RotaStaff)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RotaStaffList.Add(PropertiesFromData(_DR))
            Next

            Return _RotaStaffList

        End Function


#End Region

    End Class

End Namespace
