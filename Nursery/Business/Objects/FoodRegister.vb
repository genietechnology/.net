﻿'*****************************************************
'Generated 09/06/2016 19:33:56 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class FoodRegister
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_MealId As Guid?
        Dim m_MealType As String
        Dim m_MealName As String
        Dim m_FoodId As Guid?
        Dim m_FoodName As String
        Dim m_FoodStatus As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._MealId = Nothing
                ._MealType = ""
                ._MealName = ""
                ._FoodId = Nothing
                ._FoodName = ""
                ._FoodStatus = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._ChildId = Nothing
                ._ChildName = ""
                ._MealId = Nothing
                ._MealType = ""
                ._MealName = ""
                ._FoodId = Nothing
                ._FoodName = ""
                ._FoodStatus = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@MealId")> _
        Public Property _MealId() As Guid?
            Get
                Return m_MealId
            End Get
            Set(ByVal value As Guid?)
                m_MealId = value
            End Set
        End Property

        <StoredProcParameter("@MealType")> _
        Public Property _MealType() As String
            Get
                Return m_MealType
            End Get
            Set(ByVal value As String)
                m_MealType = value
            End Set
        End Property

        <StoredProcParameter("@MealName")> _
        Public Property _MealName() As String
            Get
                Return m_MealName
            End Get
            Set(ByVal value As String)
                m_MealName = value
            End Set
        End Property

        <StoredProcParameter("@FoodId")> _
        Public Property _FoodId() As Guid?
            Get
                Return m_FoodId
            End Get
            Set(ByVal value As Guid?)
                m_FoodId = value
            End Set
        End Property

        <StoredProcParameter("@FoodName")> _
        Public Property _FoodName() As String
            Get
                Return m_FoodName
            End Get
            Set(ByVal value As String)
                m_FoodName = value
            End Set
        End Property

        <StoredProcParameter("@FoodStatus")> _
        Public Property _FoodStatus() As String
            Get
                Return m_FoodStatus
            End Get
            Set(ByVal value As String)
                m_FoodStatus = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As FoodRegister

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getFoodRegisterbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of FoodRegister)

            Dim _FoodRegisterList As List(Of FoodRegister)
            _FoodRegisterList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getFoodRegisterTable"))
            Return _FoodRegisterList

        End Function

        Public Shared Sub SaveAll(ByVal FoodRegisterList As List(Of FoodRegister))

            For Each _FoodRegister As FoodRegister In FoodRegisterList
                SaveRecord(_FoodRegister)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal FoodRegister As FoodRegister) As Guid
            FoodRegister.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, FoodRegister, "upsertFoodRegister")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteFoodRegisterbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertFoodRegister")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As FoodRegister

            Dim _F As FoodRegister = Nothing

            If DR IsNot Nothing Then
                _F = New FoodRegister()
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._DayId = GetGUID(DR("day_id"))
                    _F._ChildId = GetGUID(DR("child_id"))
                    _F._ChildName = DR("child_name").ToString.Trim
                    _F._MealId = GetGUID(DR("meal_id"))
                    _F._MealType = DR("meal_type").ToString.Trim
                    _F._MealName = DR("meal_name").ToString.Trim
                    _F._FoodId = GetGUID(DR("food_id"))
                    _F._FoodName = DR("food_name").ToString.Trim
                    _F._FoodStatus = DR("food_status").ToString.Trim

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of FoodRegister)

            Dim _FoodRegisterList As New List(Of FoodRegister)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _FoodRegisterList.Add(PropertiesFromData(_DR))
            Next

            Return _FoodRegisterList

        End Function


#End Region

    End Class

End Namespace
