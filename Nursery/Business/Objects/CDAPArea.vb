﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CDAPArea
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Seq As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Seq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Seq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Seq")> _
        Public Property _Seq() As Integer
            Get
                Return m_Seq
            End Get
            Set(ByVal value As Integer)
                m_Seq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CDAPArea

            Dim _CDAPArea As CDAPArea
            _CDAPArea = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCDAPAreabyID", ID))
            Return _CDAPArea

        End Function

        Public Shared Function RetreiveAll() As List(Of CDAPArea)

            Dim _CDAPAreaList As List(Of CDAPArea)
            _CDAPAreaList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCDAPAreaTable"))
            Return _CDAPAreaList

        End Function

        Public Shared Sub SaveAll(ByVal CDAPAreaList As List(Of CDAPArea))

            For Each _CDAPArea As CDAPArea In CDAPAreaList
                SaveRecord(_CDAPArea)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CDAPArea As CDAPArea) As Guid
            DAL.SaveRecord(Session.ConnectionManager, CDAPArea, "upsertCDAPArea")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCDAPAreabyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertCDAPArea")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CDAPArea

            Dim _C As New CDAPArea()

            If DR IsNot Nothing Then
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Name = DR("name").ToString.Trim
                    _C._Seq = GetInteger(DR("seq"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CDAPArea)

            Dim _CDAPAreaList As New List(Of CDAPArea)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CDAPAreaList.Add(PropertiesFromData(_DR))
            Next

            Return _CDAPAreaList

        End Function


#End Region

    End Class

End Namespace
