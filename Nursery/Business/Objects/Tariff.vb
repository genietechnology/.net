﻿'*****************************************************
'Generated 24/09/2018 09:52:24 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Tariff
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Type = ""
                ._Name = ""
                ._InvoiceText = ""
                ._Duration = 0
                ._Minutes = 0
                ._Rate = 0
                ._AvgDays = 0
                ._AvgWeeks = 0
                ._Avg = 0
                ._Override = False
                ._Daily = False
                ._Weekly = False
                ._Monthly = False
                ._Colour = ""
                ._Am = False
                ._Pm = False
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._NlCode = ""
                ._FundingHours = False
                ._Holiday = False
                ._HolidayTariff = Nothing
                ._SessHol = False
                ._SessHolDiscount = 0
                ._RateFund24 = 0
                ._RateFund36 = 0
                ._VoidStart = Nothing
                ._VoidEnd = Nothing
                ._VariableRate = False
                ._ActiveFrom = Nothing
                ._NlTracking = ""
                ._ValidateTimesDur = False
                ._BoltOns = False
                ._Discount = False
                ._SiteId = Nothing
                ._SiteName = ""
                ._Archived = False
                ._SummaryColumn = ""
                ._FutFrom = Nothing
                ._FutRate = 0
                ._FutFundFrom = Nothing
                ._FutFund24 = 0
                ._FutFund36 = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Type = ""
                ._Name = ""
                ._InvoiceText = ""
                ._Duration = 0
                ._Minutes = 0
                ._Rate = 0
                ._AvgDays = 0
                ._AvgWeeks = 0
                ._Avg = 0
                ._Override = False
                ._Daily = False
                ._Weekly = False
                ._Monthly = False
                ._Colour = ""
                ._Am = False
                ._Pm = False
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._NlCode = ""
                ._FundingHours = False
                ._Holiday = False
                ._HolidayTariff = Nothing
                ._SessHol = False
                ._SessHolDiscount = 0
                ._RateFund24 = 0
                ._RateFund36 = 0
                ._VoidStart = Nothing
                ._VoidEnd = Nothing
                ._VariableRate = False
                ._ActiveFrom = Nothing
                ._NlTracking = ""
                ._ValidateTimesDur = False
                ._BoltOns = False
                ._Discount = False
                ._SiteId = Nothing
                ._SiteName = ""
                ._Archived = False
                ._SummaryColumn = ""
                ._FutFrom = Nothing
                ._FutRate = 0
                ._FutFundFrom = Nothing
                ._FutFund24 = 0
                ._FutFund36 = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Type")>
        Public Property _Type() As String

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@InvoiceText")>
        Public Property _InvoiceText() As String

        <StoredProcParameter("@Duration")>
        Public Property _Duration() As Decimal

        <StoredProcParameter("@Minutes")>
        Public Property _Minutes() As Decimal

        <StoredProcParameter("@Rate")>
        Public Property _Rate() As Decimal

        <StoredProcParameter("@AvgDays")>
        Public Property _AvgDays() As Decimal

        <StoredProcParameter("@AvgWeeks")>
        Public Property _AvgWeeks() As Decimal

        <StoredProcParameter("@Avg")>
        Public Property _Avg() As Decimal

        <StoredProcParameter("@Override")>
        Public Property _Override() As Boolean

        <StoredProcParameter("@Daily")>
        Public Property _Daily() As Boolean

        <StoredProcParameter("@Weekly")>
        Public Property _Weekly() As Boolean

        <StoredProcParameter("@Monthly")>
        Public Property _Monthly() As Boolean

        <StoredProcParameter("@Colour")>
        Public Property _Colour() As String

        <StoredProcParameter("@Am")>
        Public Property _Am() As Boolean

        <StoredProcParameter("@Pm")>
        Public Property _Pm() As Boolean

        <StoredProcParameter("@StartTime")>
        Public Property _StartTime() As TimeSpan?

        <StoredProcParameter("@EndTime")>
        Public Property _EndTime() As TimeSpan?

        <StoredProcParameter("@NlCode")>
        Public Property _NlCode() As String

        <StoredProcParameter("@FundingHours")>
        Public Property _FundingHours() As Boolean

        <StoredProcParameter("@Holiday")>
        Public Property _Holiday() As Boolean

        <StoredProcParameter("@HolidayTariff")>
        Public Property _HolidayTariff() As Guid?

        <StoredProcParameter("@SessHol")>
        Public Property _SessHol() As Boolean

        <StoredProcParameter("@SessHolDiscount")>
        Public Property _SessHolDiscount() As Decimal

        <StoredProcParameter("@RateFund24")>
        Public Property _RateFund24() As Decimal

        <StoredProcParameter("@RateFund36")>
        Public Property _RateFund36() As Decimal

        <StoredProcParameter("@VoidStart")>
        Public Property _VoidStart() As TimeSpan?

        <StoredProcParameter("@VoidEnd")>
        Public Property _VoidEnd() As TimeSpan?

        <StoredProcParameter("@VariableRate")>
        Public Property _VariableRate() As Boolean

        <StoredProcParameter("@ActiveFrom")>
        Public Property _ActiveFrom() As Date?

        <StoredProcParameter("@NlTracking")>
        Public Property _NlTracking() As String

        <StoredProcParameter("@ValidateTimesDur")>
        Public Property _ValidateTimesDur() As Boolean

        <StoredProcParameter("@BoltOns")>
        Public Property _BoltOns() As Boolean

        <StoredProcParameter("@Discount")>
        Public Property _Discount() As Boolean

        <StoredProcParameter("@SiteId")>
        Public Property _SiteId() As Guid?

        <StoredProcParameter("@SiteName")>
        Public Property _SiteName() As String

        <StoredProcParameter("@Archived")>
        Public Property _Archived() As Boolean

        <StoredProcParameter("@SummaryColumn")>
        Public Property _SummaryColumn() As String

        <StoredProcParameter("@FutFrom")>
        Public Property _FutFrom() As Date?

        <StoredProcParameter("@FutRate")>
        Public Property _FutRate() As Decimal

        <StoredProcParameter("@FutFundFrom")>
        Public Property _FutFundFrom() As Date?

        <StoredProcParameter("@FutFund24")>
        Public Property _FutFund24() As Decimal

        <StoredProcParameter("@FutFund36")>
        Public Property _FutFund36() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Tariff
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Tariff
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Tariff)
            Dim _TariffList As List(Of Tariff)
            _TariffList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffTable"))
            Return _TariffList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Tariff)
            Dim _TariffList As List(Of Tariff)
            _TariffList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffTable"))
            Return _TariffList
        End Function

        Public Shared Sub SaveAll(ByVal TariffList As List(Of Tariff))
            For Each _Tariff As Tariff In TariffList
                SaveRecord(_Tariff)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffList As List(Of Tariff))
            For Each _Tariff As Tariff In TariffList
                SaveRecord(ConnectionString, _Tariff)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Tariff As Tariff) As Guid
            Dim _Current As Tariff = RetreiveByID(Tariff._ID.Value)
            Tariff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Tariff, _Current, "upsertTariff")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Tariff As Tariff) As Guid
            Dim _Current As Tariff = RetreiveByID(ConnectionString, Tariff._ID.Value)
            Tariff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Tariff, _Current, "upsertTariff")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Tariff = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Tariff = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Tariff = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariff")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Tariff = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariff")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Tariff

            Dim _T As Tariff = Nothing

            If DR IsNot Nothing Then
                _T = New Tariff()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._Type = DR("type").ToString.Trim
                    _T._Name = DR("name").ToString.Trim
                    _T._InvoiceText = DR("invoice_text").ToString.Trim
                    _T._Duration = GetDecimal(DR("duration"))
                    _T._Minutes = GetDecimal(DR("minutes"))
                    _T._Rate = GetDecimal(DR("rate"))
                    _T._AvgDays = GetDecimal(DR("avg_days"))
                    _T._AvgWeeks = GetDecimal(DR("avg_weeks"))
                    _T._Avg = GetDecimal(DR("avg"))
                    _T._Override = GetBoolean(DR("override"))
                    _T._Daily = GetBoolean(DR("daily"))
                    _T._Weekly = GetBoolean(DR("weekly"))
                    _T._Monthly = GetBoolean(DR("monthly"))
                    _T._Colour = DR("colour").ToString.Trim
                    _T._Am = GetBoolean(DR("am"))
                    _T._Pm = GetBoolean(DR("pm"))
                    _T._StartTime = GetTimeSpan(DR("start_time"))
                    _T._EndTime = GetTimeSpan(DR("end_time"))
                    _T._NlCode = DR("nl_code").ToString.Trim
                    _T._FundingHours = GetBoolean(DR("funding_hours"))
                    _T._Holiday = GetBoolean(DR("holiday"))
                    _T._HolidayTariff = GetGUID(DR("holiday_tariff"))
                    _T._SessHol = GetBoolean(DR("sess_hol"))
                    _T._SessHolDiscount = GetDecimal(DR("sess_hol_discount"))
                    _T._RateFund24 = GetDecimal(DR("rate_fund_24"))
                    _T._RateFund36 = GetDecimal(DR("rate_fund_36"))
                    _T._VoidStart = GetTimeSpan(DR("void_start"))
                    _T._VoidEnd = GetTimeSpan(DR("void_end"))
                    _T._VariableRate = GetBoolean(DR("variable_rate"))
                    _T._ActiveFrom = GetDate(DR("active_from"))
                    _T._NlTracking = DR("nl_tracking").ToString.Trim
                    _T._ValidateTimesDur = GetBoolean(DR("validate_times_dur"))
                    _T._BoltOns = GetBoolean(DR("bolt_ons"))
                    _T._Discount = GetBoolean(DR("discount"))
                    _T._SiteId = GetGUID(DR("site_id"))
                    _T._SiteName = DR("site_name").ToString.Trim
                    _T._Archived = GetBoolean(DR("archived"))
                    _T._SummaryColumn = DR("summary_column").ToString.Trim
                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Tariff)

            Dim _TariffList As New List(Of Tariff)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffList

        End Function


#End Region

    End Class

End Namespace
