﻿'*****************************************************
'Generated 24/09/2018 09:53:19 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TariffMatrix
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TariffId = Nothing
                ._AgeId = Nothing
                ._Class = ""
                ._InvoiceText = ""
                ._Value = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TariffId = Nothing
                ._AgeId = Nothing
                ._Class = ""
                ._InvoiceText = ""
                ._Value = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@TariffId")>
        Public Property _TariffId() As Guid?

        <StoredProcParameter("@AgeId")>
        Public Property _AgeId() As Guid?

        <StoredProcParameter("@Class")>
        Public Property _Class() As String

        <StoredProcParameter("@InvoiceText")>
        Public Property _InvoiceText() As String

        <StoredProcParameter("@Value")>
        Public Property _Value() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TariffMatrix
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffMatrixbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TariffMatrix
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffMatrixbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of TariffMatrix)
            Dim _TariffMatrixList As List(Of TariffMatrix)
            _TariffMatrixList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffMatrixTable"))
            Return _TariffMatrixList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TariffMatrix)
            Dim _TariffMatrixList As List(Of TariffMatrix)
            _TariffMatrixList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffMatrixTable"))
            Return _TariffMatrixList
        End Function

        Public Shared Sub SaveAll(ByVal TariffMatrixList As List(Of TariffMatrix))
            For Each _TariffMatrix As TariffMatrix In TariffMatrixList
                SaveRecord(_TariffMatrix)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffMatrixList As List(Of TariffMatrix))
            For Each _TariffMatrix As TariffMatrix In TariffMatrixList
                SaveRecord(ConnectionString, _TariffMatrix)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal TariffMatrix As TariffMatrix) As Guid
            Dim _Current As TariffMatrix = RetreiveByID(TariffMatrix._ID.Value)
            TariffMatrix.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffMatrix, _Current, "upsertTariffMatrix")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TariffMatrix As TariffMatrix) As Guid
            Dim _Current As TariffMatrix = RetreiveByID(ConnectionString, TariffMatrix._ID.Value)
            TariffMatrix.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffMatrix, _Current, "upsertTariffMatrix")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As TariffMatrix = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffMatrixbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As TariffMatrix = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffMatrixbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As TariffMatrix = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffMatrix")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As TariffMatrix = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffMatrix")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TariffMatrix

            Dim _T As TariffMatrix = Nothing

            If DR IsNot Nothing Then
                _T = New TariffMatrix()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._TariffId = GetGUID(DR("tariff_id"))
                    _T._AgeId = GetGUID(DR("age_id"))
                    _T._Class = DR("class").ToString.Trim
                    _T._InvoiceText = DR("invoice_text").ToString.Trim
                    _T._Value = GetDecimal(DR("value"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TariffMatrix)

            Dim _TariffMatrixList As New List(Of TariffMatrix)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffMatrixList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffMatrixList

        End Function


#End Region

    End Class

End Namespace
