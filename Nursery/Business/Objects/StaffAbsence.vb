﻿'*****************************************************
'Generated 08/11/2016 07:22:30 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class StaffAbsence
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_StaffId As Guid?
        Dim m_AbsType As String
        Dim m_AbsFrom As Date?
        Dim m_AbsTo As Date?
        Dim m_AbsCost As Decimal
        Dim m_AbsReason As String
        Dim m_AbsNotes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._StaffId = Nothing
                ._AbsType = ""
                ._AbsFrom = Nothing
                ._AbsTo = Nothing
                ._AbsCost = 0
                ._AbsReason = ""
                ._AbsNotes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._StaffId = Nothing
                ._AbsType = ""
                ._AbsFrom = Nothing
                ._AbsTo = Nothing
                ._AbsCost = 0
                ._AbsReason = ""
                ._AbsNotes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@AbsType")> _
        Public Property _AbsType() As String
            Get
                Return m_AbsType
            End Get
            Set(ByVal value As String)
                m_AbsType = value
            End Set
        End Property

        <StoredProcParameter("@AbsFrom")> _
        Public Property _AbsFrom() As Date?
            Get
                Return m_AbsFrom
            End Get
            Set(ByVal value As Date?)
                m_AbsFrom = value
            End Set
        End Property

        <StoredProcParameter("@AbsTo")> _
        Public Property _AbsTo() As Date?
            Get
                Return m_AbsTo
            End Get
            Set(ByVal value As Date?)
                m_AbsTo = value
            End Set
        End Property

        <StoredProcParameter("@AbsCost")> _
        Public Property _AbsCost() As Decimal
            Get
                Return m_AbsCost
            End Get
            Set(ByVal value As Decimal)
                m_AbsCost = value
            End Set
        End Property

        <StoredProcParameter("@AbsReason")> _
        Public Property _AbsReason() As String
            Get
                Return m_AbsReason
            End Get
            Set(ByVal value As String)
                m_AbsReason = value
            End Set
        End Property

        <StoredProcParameter("@AbsNotes")> _
        Public Property _AbsNotes() As String
            Get
                Return m_AbsNotes
            End Get
            Set(ByVal value As String)
                m_AbsNotes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As StaffAbsence

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStaffAbsencebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of StaffAbsence)

            Dim _StaffAbsenceList As List(Of StaffAbsence)
            _StaffAbsenceList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStaffAbsenceTable"))
            Return _StaffAbsenceList

        End Function

        Public Shared Sub SaveAll(ByVal StaffAbsenceList As List(Of StaffAbsence))

            For Each _StaffAbsence As StaffAbsence In StaffAbsenceList
                SaveRecord(_StaffAbsence)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal StaffAbsence As StaffAbsence) As Guid
            StaffAbsence.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, StaffAbsence, "upsertStaffAbsence")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStaffAbsencebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertStaffAbsence")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As StaffAbsence

            Dim _S As StaffAbsence = Nothing

            If DR IsNot Nothing Then
                _S = New StaffAbsence()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._StaffId = GetGUID(DR("staff_id"))
                    _S._AbsType = DR("abs_type").ToString.Trim
                    _S._AbsFrom = GetDate(DR("abs_from"))
                    _S._AbsTo = GetDate(DR("abs_to"))
                    _S._AbsCost = GetDecimal(DR("abs_cost"))
                    _S._AbsReason = DR("abs_reason").ToString.Trim
                    _S._AbsNotes = DR("abs_notes").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of StaffAbsence)

            Dim _StaffAbsenceList As New List(Of StaffAbsence)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StaffAbsenceList.Add(PropertiesFromData(_DR))
            Next

            Return _StaffAbsenceList

        End Function


#End Region

    End Class

End Namespace
