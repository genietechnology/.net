﻿'*****************************************************
'Generated 09/07/2015 19:18:30 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Food
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_GroupId As Guid?
        Dim m_GroupName As String
        Dim m_ManuId As Guid?
        Dim m_ManuName As String
        Dim m_BabyFood As Boolean
        Dim m_Weight As Decimal
        Dim m_PackSize As Integer
        Dim m_Cost As Decimal
        Dim m_CostItem As Decimal
        Dim m_QtyPhys As Integer
        Dim m_QtyMin As Integer
        Dim m_NutKj As Integer
        Dim m_NutKcal As Integer
        Dim m_NutProt As Decimal
        Dim m_NutCarb As Decimal
        Dim m_NutFat As Decimal
        Dim m_NutFibre As Decimal
        Dim m_NutSalt As Decimal
        Dim m_Ingredients As String
        Dim m_Eggs As Boolean
        Dim m_Milk As Boolean
        Dim m_Fish As Boolean
        Dim m_Crustaceans As Boolean
        Dim m_Molluscs As Boolean
        Dim m_Peanuts As Boolean
        Dim m_Treenuts As Boolean
        Dim m_Sesame As Boolean
        Dim m_Cereals As Boolean
        Dim m_Soya As Boolean
        Dim m_Celery As Boolean
        Dim m_Mustard As Boolean
        Dim m_Lupin As Boolean
        Dim m_Sulphur As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._ManuId = Nothing
                ._ManuName = ""
                ._BabyFood = False
                ._Weight = 0
                ._PackSize = 0
                ._Cost = 0
                ._CostItem = 0
                ._QtyPhys = 0
                ._QtyMin = 0
                ._NutKj = 0
                ._NutKcal = 0
                ._NutProt = 0
                ._NutCarb = 0
                ._NutFat = 0
                ._NutFibre = 0
                ._NutSalt = 0
                ._Ingredients = ""
                ._Eggs = False
                ._Milk = False
                ._Fish = False
                ._Crustaceans = False
                ._Molluscs = False
                ._Peanuts = False
                ._Treenuts = False
                ._Sesame = False
                ._Cereals = False
                ._Soya = False
                ._Celery = False
                ._Mustard = False
                ._Lupin = False
                ._Sulphur = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._ManuId = Nothing
                ._ManuName = ""
                ._BabyFood = False
                ._Weight = 0
                ._PackSize = 0
                ._Cost = 0
                ._CostItem = 0
                ._QtyPhys = 0
                ._QtyMin = 0
                ._NutKj = 0
                ._NutKcal = 0
                ._NutProt = 0
                ._NutCarb = 0
                ._NutFat = 0
                ._NutFibre = 0
                ._NutSalt = 0
                ._Ingredients = ""
                ._Eggs = False
                ._Milk = False
                ._Fish = False
                ._Crustaceans = False
                ._Molluscs = False
                ._Peanuts = False
                ._Treenuts = False
                ._Sesame = False
                ._Cereals = False
                ._Soya = False
                ._Celery = False
                ._Mustard = False
                ._Lupin = False
                ._Sulphur = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@GroupId")> _
        Public Property _GroupId() As Guid?
            Get
                Return m_GroupId
            End Get
            Set(ByVal value As Guid?)
                m_GroupId = value
            End Set
        End Property

        <StoredProcParameter("@GroupName")> _
        Public Property _GroupName() As String
            Get
                Return m_GroupName
            End Get
            Set(ByVal value As String)
                m_GroupName = value
            End Set
        End Property

        <StoredProcParameter("@ManuId")> _
        Public Property _ManuId() As Guid?
            Get
                Return m_ManuId
            End Get
            Set(ByVal value As Guid?)
                m_ManuId = value
            End Set
        End Property

        <StoredProcParameter("@ManuName")> _
        Public Property _ManuName() As String
            Get
                Return m_ManuName
            End Get
            Set(ByVal value As String)
                m_ManuName = value
            End Set
        End Property

        <StoredProcParameter("@BabyFood")> _
        Public Property _BabyFood() As Boolean
            Get
                Return m_BabyFood
            End Get
            Set(ByVal value As Boolean)
                m_BabyFood = value
            End Set
        End Property

        <StoredProcParameter("@Weight")> _
        Public Property _Weight() As Decimal
            Get
                Return m_Weight
            End Get
            Set(ByVal value As Decimal)
                m_Weight = value
            End Set
        End Property

        <StoredProcParameter("@PackSize")> _
        Public Property _PackSize() As Integer
            Get
                Return m_PackSize
            End Get
            Set(ByVal value As Integer)
                m_PackSize = value
            End Set
        End Property

        <StoredProcParameter("@Cost")> _
        Public Property _Cost() As Decimal
            Get
                Return m_Cost
            End Get
            Set(ByVal value As Decimal)
                m_Cost = value
            End Set
        End Property

        <StoredProcParameter("@CostItem")> _
        Public Property _CostItem() As Decimal
            Get
                Return m_CostItem
            End Get
            Set(ByVal value As Decimal)
                m_CostItem = value
            End Set
        End Property

        <StoredProcParameter("@QtyPhys")> _
        Public Property _QtyPhys() As Integer
            Get
                Return m_QtyPhys
            End Get
            Set(ByVal value As Integer)
                m_QtyPhys = value
            End Set
        End Property

        <StoredProcParameter("@QtyMin")> _
        Public Property _QtyMin() As Integer
            Get
                Return m_QtyMin
            End Get
            Set(ByVal value As Integer)
                m_QtyMin = value
            End Set
        End Property

        <StoredProcParameter("@NutKj")> _
        Public Property _NutKj() As Integer
            Get
                Return m_NutKj
            End Get
            Set(ByVal value As Integer)
                m_NutKj = value
            End Set
        End Property

        <StoredProcParameter("@NutKcal")> _
        Public Property _NutKcal() As Integer
            Get
                Return m_NutKcal
            End Get
            Set(ByVal value As Integer)
                m_NutKcal = value
            End Set
        End Property

        <StoredProcParameter("@NutProt")> _
        Public Property _NutProt() As Decimal
            Get
                Return m_NutProt
            End Get
            Set(ByVal value As Decimal)
                m_NutProt = value
            End Set
        End Property

        <StoredProcParameter("@NutCarb")> _
        Public Property _NutCarb() As Decimal
            Get
                Return m_NutCarb
            End Get
            Set(ByVal value As Decimal)
                m_NutCarb = value
            End Set
        End Property

        <StoredProcParameter("@NutFat")> _
        Public Property _NutFat() As Decimal
            Get
                Return m_NutFat
            End Get
            Set(ByVal value As Decimal)
                m_NutFat = value
            End Set
        End Property

        <StoredProcParameter("@NutFibre")> _
        Public Property _NutFibre() As Decimal
            Get
                Return m_NutFibre
            End Get
            Set(ByVal value As Decimal)
                m_NutFibre = value
            End Set
        End Property

        <StoredProcParameter("@NutSalt")> _
        Public Property _NutSalt() As Decimal
            Get
                Return m_NutSalt
            End Get
            Set(ByVal value As Decimal)
                m_NutSalt = value
            End Set
        End Property

        <StoredProcParameter("@Ingredients")> _
        Public Property _Ingredients() As String
            Get
                Return m_Ingredients
            End Get
            Set(ByVal value As String)
                m_Ingredients = value
            End Set
        End Property

        <StoredProcParameter("@Eggs")> _
        Public Property _Eggs() As Boolean
            Get
                Return m_Eggs
            End Get
            Set(ByVal value As Boolean)
                m_Eggs = value
            End Set
        End Property

        <StoredProcParameter("@Milk")> _
        Public Property _Milk() As Boolean
            Get
                Return m_Milk
            End Get
            Set(ByVal value As Boolean)
                m_Milk = value
            End Set
        End Property

        <StoredProcParameter("@Fish")> _
        Public Property _Fish() As Boolean
            Get
                Return m_Fish
            End Get
            Set(ByVal value As Boolean)
                m_Fish = value
            End Set
        End Property

        <StoredProcParameter("@Crustaceans")> _
        Public Property _Crustaceans() As Boolean
            Get
                Return m_Crustaceans
            End Get
            Set(ByVal value As Boolean)
                m_Crustaceans = value
            End Set
        End Property

        <StoredProcParameter("@Molluscs")> _
        Public Property _Molluscs() As Boolean
            Get
                Return m_Molluscs
            End Get
            Set(ByVal value As Boolean)
                m_Molluscs = value
            End Set
        End Property

        <StoredProcParameter("@Peanuts")> _
        Public Property _Peanuts() As Boolean
            Get
                Return m_Peanuts
            End Get
            Set(ByVal value As Boolean)
                m_Peanuts = value
            End Set
        End Property

        <StoredProcParameter("@Treenuts")> _
        Public Property _Treenuts() As Boolean
            Get
                Return m_Treenuts
            End Get
            Set(ByVal value As Boolean)
                m_Treenuts = value
            End Set
        End Property

        <StoredProcParameter("@Sesame")> _
        Public Property _Sesame() As Boolean
            Get
                Return m_Sesame
            End Get
            Set(ByVal value As Boolean)
                m_Sesame = value
            End Set
        End Property

        <StoredProcParameter("@Cereals")> _
        Public Property _Cereals() As Boolean
            Get
                Return m_Cereals
            End Get
            Set(ByVal value As Boolean)
                m_Cereals = value
            End Set
        End Property

        <StoredProcParameter("@Soya")> _
        Public Property _Soya() As Boolean
            Get
                Return m_Soya
            End Get
            Set(ByVal value As Boolean)
                m_Soya = value
            End Set
        End Property

        <StoredProcParameter("@Celery")> _
        Public Property _Celery() As Boolean
            Get
                Return m_Celery
            End Get
            Set(ByVal value As Boolean)
                m_Celery = value
            End Set
        End Property

        <StoredProcParameter("@Mustard")> _
        Public Property _Mustard() As Boolean
            Get
                Return m_Mustard
            End Get
            Set(ByVal value As Boolean)
                m_Mustard = value
            End Set
        End Property

        <StoredProcParameter("@Lupin")> _
        Public Property _Lupin() As Boolean
            Get
                Return m_Lupin
            End Get
            Set(ByVal value As Boolean)
                m_Lupin = value
            End Set
        End Property

        <StoredProcParameter("@Sulphur")> _
        Public Property _Sulphur() As Boolean
            Get
                Return m_Sulphur
            End Get
            Set(ByVal value As Boolean)
                m_Sulphur = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Food

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getFoodbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Food)

            Dim _FoodList As List(Of Food)
            _FoodList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getFoodTable"))
            Return _FoodList

        End Function

        Public Shared Sub SaveAll(ByVal FoodList As List(Of Food))

            For Each _Food As Food In FoodList
                SaveRecord(_Food)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Food As Food) As Guid
            DAL.SaveRecord(Session.ConnectionManager, Food, "upsertFood")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteFoodbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertFood")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Food

            Dim _F As Food = Nothing

            If DR IsNot Nothing Then
                _F = New Food()
                With DR
                    _F.IsNew = False
                    _F.IsDeleted = False
                    _F._ID = GetGUID(DR("ID"))
                    _F._Name = DR("name").ToString.Trim
                    _F._GroupId = GetGUID(DR("group_id"))
                    _F._GroupName = DR("group_name").ToString.Trim
                    _F._ManuId = GetGUID(DR("manu_id"))
                    _F._ManuName = DR("manu_name").ToString.Trim
                    _F._BabyFood = GetBoolean(DR("baby_food"))
                    _F._Weight = GetDecimal(DR("weight"))
                    _F._PackSize = GetInteger(DR("pack_size"))
                    _F._Cost = GetDecimal(DR("cost"))
                    _F._CostItem = GetDecimal(DR("cost_item"))
                    _F._QtyPhys = GetInteger(DR("qty_phys"))
                    _F._QtyMin = GetInteger(DR("qty_min"))
                    _F._NutKj = GetInteger(DR("nut_kj"))
                    _F._NutKcal = GetInteger(DR("nut_kcal"))
                    _F._NutProt = GetDecimal(DR("nut_prot"))
                    _F._NutCarb = GetDecimal(DR("nut_carb"))
                    _F._NutFat = GetDecimal(DR("nut_fat"))
                    _F._NutFibre = GetDecimal(DR("nut_fibre"))
                    _F._NutSalt = GetDecimal(DR("nut_salt"))
                    _F._Ingredients = DR("ingredients").ToString.Trim
                    _F._Eggs = GetBoolean(DR("eggs"))
                    _F._Milk = GetBoolean(DR("milk"))
                    _F._Fish = GetBoolean(DR("fish"))
                    _F._Crustaceans = GetBoolean(DR("crustaceans"))
                    _F._Molluscs = GetBoolean(DR("molluscs"))
                    _F._Peanuts = GetBoolean(DR("peanuts"))
                    _F._Treenuts = GetBoolean(DR("treenuts"))
                    _F._Sesame = GetBoolean(DR("sesame"))
                    _F._Cereals = GetBoolean(DR("cereals"))
                    _F._Soya = GetBoolean(DR("soya"))
                    _F._Celery = GetBoolean(DR("celery"))
                    _F._Mustard = GetBoolean(DR("mustard"))
                    _F._Lupin = GetBoolean(DR("lupin"))
                    _F._Sulphur = GetBoolean(DR("sulphur"))

                End With
            End If

            Return _F

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Food)

            Dim _FoodList As New List(Of Food)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _FoodList.Add(PropertiesFromData(_DR))
            Next

            Return _FoodList

        End Function


#End Region

    End Class

End Namespace
