﻿'*****************************************************
'Generated 16/05/2017 17:33:01 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RotaSlotStaff
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RssRotaId As Guid?
        Dim m_RssSlotId As Guid?
        Dim m_RssStatus As String
        Dim m_RssDate As Date?
        Dim m_RssStart As TimeSpan?
        Dim m_RssFinish As TimeSpan?
        Dim m_RssRatio As Guid?
        Dim m_RssRoom As String
        Dim m_RssHours As Decimal
        Dim m_RssCost As Decimal
        Dim m_RssStaffId As Guid?
        Dim m_RssStaffName As String
        Dim m_RssStaffAge As Integer
        Dim m_RssStaffLevel As Integer
        Dim m_RssStaffRate As Decimal
        Dim m_RssStaffPaed As Boolean
        Dim m_RssStaffFaid As Boolean
        Dim m_RssStaffSenco As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RssRotaId = Nothing
                ._RssSlotId = Nothing
                ._RssStatus = ""
                ._RssDate = Nothing
                ._RssStart = Nothing
                ._RssFinish = Nothing
                ._RssRatio = Nothing
                ._RssRoom = ""
                ._RssHours = 0
                ._RssCost = 0
                ._RssStaffId = Nothing
                ._RssStaffName = ""
                ._RssStaffAge = 0
                ._RssStaffLevel = 0
                ._RssStaffRate = 0
                ._RssStaffPaed = False
                ._RssStaffFaid = False
                ._RssStaffSenco = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RssRotaId = Nothing
                ._RssSlotId = Nothing
                ._RssStatus = ""
                ._RssDate = Nothing
                ._RssStart = Nothing
                ._RssFinish = Nothing
                ._RssRatio = Nothing
                ._RssRoom = ""
                ._RssHours = 0
                ._RssCost = 0
                ._RssStaffId = Nothing
                ._RssStaffName = ""
                ._RssStaffAge = 0
                ._RssStaffLevel = 0
                ._RssStaffRate = 0
                ._RssStaffPaed = False
                ._RssStaffFaid = False
                ._RssStaffSenco = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RssRotaId")>
        Public Property _RssRotaId() As Guid?
            Get
                Return m_RssRotaId
            End Get
            Set(ByVal value As Guid?)
                m_RssRotaId = value
            End Set
        End Property

        <StoredProcParameter("@RssSlotId")>
        Public Property _RssSlotId() As Guid?
            Get
                Return m_RssSlotId
            End Get
            Set(ByVal value As Guid?)
                m_RssSlotId = value
            End Set
        End Property

        <StoredProcParameter("@RssStatus")>
        Public Property _RssStatus() As String
            Get
                Return m_RssStatus
            End Get
            Set(ByVal value As String)
                m_RssStatus = value
            End Set
        End Property

        <StoredProcParameter("@RssDate")>
        Public Property _RssDate() As Date?
            Get
                Return m_RssDate
            End Get
            Set(ByVal value As Date?)
                m_RssDate = value
            End Set
        End Property

        <StoredProcParameter("@RssStart")>
        Public Property _RssStart() As TimeSpan?
            Get
                Return m_RssStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_RssStart = value
            End Set
        End Property

        <StoredProcParameter("@RssFinish")>
        Public Property _RssFinish() As TimeSpan?
            Get
                Return m_RssFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_RssFinish = value
            End Set
        End Property

        <StoredProcParameter("@RssRatio")>
        Public Property _RssRatio() As Guid?
            Get
                Return m_RssRatio
            End Get
            Set(ByVal value As Guid?)
                m_RssRatio = value
            End Set
        End Property

        <StoredProcParameter("@RssRoom")>
        Public Property _RssRoom() As String
            Get
                Return m_RssRoom
            End Get
            Set(ByVal value As String)
                m_RssRoom = value
            End Set
        End Property

        <StoredProcParameter("@RssHours")>
        Public Property _RssHours() As Decimal
            Get
                Return m_RssHours
            End Get
            Set(ByVal value As Decimal)
                m_RssHours = value
            End Set
        End Property

        <StoredProcParameter("@RssCost")>
        Public Property _RssCost() As Decimal
            Get
                Return m_RssCost
            End Get
            Set(ByVal value As Decimal)
                m_RssCost = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffId")>
        Public Property _RssStaffId() As Guid?
            Get
                Return m_RssStaffId
            End Get
            Set(ByVal value As Guid?)
                m_RssStaffId = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffName")>
        Public Property _RssStaffName() As String
            Get
                Return m_RssStaffName
            End Get
            Set(ByVal value As String)
                m_RssStaffName = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffAge")>
        Public Property _RssStaffAge() As Integer
            Get
                Return m_RssStaffAge
            End Get
            Set(ByVal value As Integer)
                m_RssStaffAge = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffLevel")>
        Public Property _RssStaffLevel() As Integer
            Get
                Return m_RssStaffLevel
            End Get
            Set(ByVal value As Integer)
                m_RssStaffLevel = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffRate")>
        Public Property _RssStaffRate() As Decimal
            Get
                Return m_RssStaffRate
            End Get
            Set(ByVal value As Decimal)
                m_RssStaffRate = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffPaed")>
        Public Property _RssStaffPaed() As Boolean
            Get
                Return m_RssStaffPaed
            End Get
            Set(ByVal value As Boolean)
                m_RssStaffPaed = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffFaid")>
        Public Property _RssStaffFaid() As Boolean
            Get
                Return m_RssStaffFaid
            End Get
            Set(ByVal value As Boolean)
                m_RssStaffFaid = value
            End Set
        End Property

        <StoredProcParameter("@RssStaffSenco")>
        Public Property _RssStaffSenco() As Boolean
            Get
                Return m_RssStaffSenco
            End Get
            Set(ByVal value As Boolean)
                m_RssStaffSenco = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RotaSlotStaff

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRotaSlotStaffbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RotaSlotStaff

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRotaSlotStaffbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RotaSlotStaff)

            Dim _RotaSlotStaffList As List(Of RotaSlotStaff)
            _RotaSlotStaffList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRotaSlotStaffTable"))
            Return _RotaSlotStaffList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RotaSlotStaff)

            Dim _RotaSlotStaffList As List(Of RotaSlotStaff)
            _RotaSlotStaffList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRotaSlotStaffTable"))
            Return _RotaSlotStaffList

        End Function

        Public Shared Sub SaveAll(ByVal RotaSlotStaffList As List(Of RotaSlotStaff))

            For Each _RotaSlotStaff As RotaSlotStaff In RotaSlotStaffList
                SaveRecord(_RotaSlotStaff)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RotaSlotStaffList As List(Of RotaSlotStaff))

            For Each _RotaSlotStaff As RotaSlotStaff In RotaSlotStaffList
                SaveRecord(ConnectionString, _RotaSlotStaff)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RotaSlotStaff As RotaSlotStaff) As Guid
            RotaSlotStaff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaSlotStaff, "upsertRotaSlotStaff")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RotaSlotStaff As RotaSlotStaff) As Guid
            RotaSlotStaff.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaSlotStaff, "upsertRotaSlotStaff")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRotaSlotStaffbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRotaSlotStaffbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaSlotStaff")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaSlotStaff")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RotaSlotStaff

            Dim _R As RotaSlotStaff = Nothing

            If DR IsNot Nothing Then
                _R = New RotaSlotStaff()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._RssRotaId = GetGUID(DR("rss_rota_id"))
                    _R._RssSlotId = GetGUID(DR("rss_slot_id"))
                    _R._RssStatus = DR("rss_status").ToString.Trim
                    _R._RssDate = GetDate(DR("rss_date"))
                    _R._RssStart = GetTimeSpan(DR("rss_start"))
                    _R._RssFinish = GetTimeSpan(DR("rss_finish"))
                    _R._RssRatio = GetGUID(DR("rss_ratio"))
                    _R._RssRoom = DR("rss_room").ToString.Trim
                    _R._RssHours = GetDecimal(DR("rss_hours"))
                    _R._RssCost = GetDecimal(DR("rss_cost"))
                    _R._RssStaffId = GetGUID(DR("rss_staff_id"))
                    _R._RssStaffName = DR("rss_staff_name").ToString.Trim
                    _R._RssStaffAge = GetInteger(DR("rss_staff_age"))
                    _R._RssStaffLevel = GetInteger(DR("rss_staff_level"))
                    _R._RssStaffRate = GetDecimal(DR("rss_staff_rate"))
                    _R._RssStaffPaed = GetBoolean(DR("rss_staff_paed"))
                    _R._RssStaffFaid = GetBoolean(DR("rss_staff_faid"))
                    _R._RssStaffSenco = GetBoolean(DR("rss_staff_senco"))

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RotaSlotStaff)

            Dim _RotaSlotStaffList As New List(Of RotaSlotStaff)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RotaSlotStaffList.Add(PropertiesFromData(_DR))
            Next

            Return _RotaSlotStaffList

        End Function


#End Region

    End Class

End Namespace
