﻿'*****************************************************
'Generated 05/12/2017 16:49:54 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class LA
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Ref As String
        Dim m_Address As String
        Dim m_ContactName As String
        Dim m_ContactTel As String
        Dim m_ContactEmail As String
        Dim m_Rate24 As Decimal
        Dim m_Rate36 As Decimal
        Dim m_DefHrsYear As Decimal
        Dim m_TdHrs24 As Decimal
        Dim m_TdHrs36 As Decimal
        Dim m_TdValue24 As Decimal
        Dim m_TdValue36 As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Ref = ""
                ._Address = ""
                ._ContactName = ""
                ._ContactTel = ""
                ._ContactEmail = ""
                ._Rate24 = 0
                ._Rate36 = 0
                ._DefHrsYear = 0
                ._TdHrs24 = 0
                ._TdHrs36 = 0
                ._TdValue24 = 0
                ._TdValue36 = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Ref = ""
                ._Address = ""
                ._ContactName = ""
                ._ContactTel = ""
                ._ContactEmail = ""
                ._Rate24 = 0
                ._Rate36 = 0
                ._DefHrsYear = 0
                ._TdHrs24 = 0
                ._TdHrs36 = 0
                ._TdValue24 = 0
                ._TdValue36 = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")>
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Ref")>
        Public Property _Ref() As String
            Get
                Return m_Ref
            End Get
            Set(ByVal value As String)
                m_Ref = value
            End Set
        End Property

        <StoredProcParameter("@Address")>
        Public Property _Address() As String
            Get
                Return m_Address
            End Get
            Set(ByVal value As String)
                m_Address = value
            End Set
        End Property

        <StoredProcParameter("@ContactName")>
        Public Property _ContactName() As String
            Get
                Return m_ContactName
            End Get
            Set(ByVal value As String)
                m_ContactName = value
            End Set
        End Property

        <StoredProcParameter("@ContactTel")>
        Public Property _ContactTel() As String
            Get
                Return m_ContactTel
            End Get
            Set(ByVal value As String)
                m_ContactTel = value
            End Set
        End Property

        <StoredProcParameter("@ContactEmail")>
        Public Property _ContactEmail() As String
            Get
                Return m_ContactEmail
            End Get
            Set(ByVal value As String)
                m_ContactEmail = value
            End Set
        End Property

        <StoredProcParameter("@Rate24")>
        Public Property _Rate24() As Decimal
            Get
                Return m_Rate24
            End Get
            Set(ByVal value As Decimal)
                m_Rate24 = value
            End Set
        End Property

        <StoredProcParameter("@Rate36")>
        Public Property _Rate36() As Decimal
            Get
                Return m_Rate36
            End Get
            Set(ByVal value As Decimal)
                m_Rate36 = value
            End Set
        End Property

        <StoredProcParameter("@DefHrsYear")>
        Public Property _DefHrsYear() As Decimal
            Get
                Return m_DefHrsYear
            End Get
            Set(ByVal value As Decimal)
                m_DefHrsYear = value
            End Set
        End Property

        <StoredProcParameter("@TdHrs24")>
        Public Property _TdHrs24() As Decimal
            Get
                Return m_TdHrs24
            End Get
            Set(ByVal value As Decimal)
                m_TdHrs24 = value
            End Set
        End Property

        <StoredProcParameter("@TdHrs36")>
        Public Property _TdHrs36() As Decimal
            Get
                Return m_TdHrs36
            End Get
            Set(ByVal value As Decimal)
                m_TdHrs36 = value
            End Set
        End Property

        <StoredProcParameter("@TdValue24")>
        Public Property _TdValue24() As Decimal
            Get
                Return m_TdValue24
            End Get
            Set(ByVal value As Decimal)
                m_TdValue24 = value
            End Set
        End Property

        <StoredProcParameter("@TdValue36")>
        Public Property _TdValue36() As Decimal
            Get
                Return m_TdValue36
            End Get
            Set(ByVal value As Decimal)
                m_TdValue36 = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As LA

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getLAbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As LA

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getLAbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of LA)

            Dim _LAList As List(Of LA)
            _LAList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getLATable"))
            Return _LAList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of LA)

            Dim _LAList As List(Of LA)
            _LAList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getLATable"))
            Return _LAList

        End Function

        Public Shared Sub SaveAll(ByVal LAList As List(Of LA))

            For Each _LA As LA In LAList
                SaveRecord(_LA)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal LAList As List(Of LA))

            For Each _LA As LA In LAList
                SaveRecord(ConnectionString, _LA)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal LA As LA) As Guid
            LA.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, LA, "upsertLA")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal LA As LA) As Guid
            LA.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, LA, "upsertLA")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteLAbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteLAbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertLA")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertLA")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As LA

            Dim _L As LA = Nothing

            If DR IsNot Nothing Then
                _L = New LA()
                With DR
                    _L.IsNew = False
                    _L.IsDeleted = False
                    _L._ID = GetGUID(DR("ID"))
                    _L._Name = DR("name").ToString.Trim
                    _L._Ref = DR("ref").ToString.Trim
                    _L._Address = DR("address").ToString.Trim
                    _L._ContactName = DR("contact_name").ToString.Trim
                    _L._ContactTel = DR("contact_tel").ToString.Trim
                    _L._ContactEmail = DR("contact_email").ToString.Trim
                    _L._Rate24 = GetDecimal(DR("rate_24"))
                    _L._Rate36 = GetDecimal(DR("rate_36"))
                    _L._DefHrsYear = GetDecimal(DR("def_hrs_year"))
                    _L._TdHrs24 = GetDecimal(DR("td_hrs_24"))
                    _L._TdHrs36 = GetDecimal(DR("td_hrs_36"))
                    _L._TdValue24 = GetDecimal(DR("td_value_24"))
                    _L._TdValue36 = GetDecimal(DR("td_value_36"))

                End With
            End If

            Return _L

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of LA)

            Dim _LAList As New List(Of LA)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _LAList.Add(PropertiesFromData(_DR))
            Next

            Return _LAList

        End Function


#End Region

    End Class

End Namespace
