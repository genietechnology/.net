﻿'*****************************************************
'Generated 24/09/2018 09:53:31 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TariffVariable
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TariffId = Nothing
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._Units = 0
                ._Rate = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TariffId = Nothing
                ._StartTime = Nothing
                ._EndTime = Nothing
                ._Units = 0
                ._Rate = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@TariffId")>
        Public Property _TariffId() As Guid?

        <StoredProcParameter("@StartTime")>
        Public Property _StartTime() As TimeSpan?

        <StoredProcParameter("@EndTime")>
        Public Property _EndTime() As TimeSpan?

        <StoredProcParameter("@Units")>
        Public Property _Units() As Integer

        <StoredProcParameter("@Rate")>
        Public Property _Rate() As Decimal


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TariffVariable
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTariffVariablebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TariffVariable
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTariffVariablebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of TariffVariable)
            Dim _TariffVariableList As List(Of TariffVariable)
            _TariffVariableList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTariffVariableTable"))
            Return _TariffVariableList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TariffVariable)
            Dim _TariffVariableList As List(Of TariffVariable)
            _TariffVariableList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTariffVariableTable"))
            Return _TariffVariableList
        End Function

        Public Shared Sub SaveAll(ByVal TariffVariableList As List(Of TariffVariable))
            For Each _TariffVariable As TariffVariable In TariffVariableList
                SaveRecord(_TariffVariable)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TariffVariableList As List(Of TariffVariable))
            For Each _TariffVariable As TariffVariable In TariffVariableList
                SaveRecord(ConnectionString, _TariffVariable)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal TariffVariable As TariffVariable) As Guid
            Dim _Current As TariffVariable = RetreiveByID(TariffVariable._ID.Value)
            TariffVariable.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffVariable, _Current, "upsertTariffVariable")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TariffVariable As TariffVariable) As Guid
            Dim _Current As TariffVariable = RetreiveByID(ConnectionString, TariffVariable._ID.Value)
            TariffVariable.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, TariffVariable, _Current, "upsertTariffVariable")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As TariffVariable = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffVariablebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As TariffVariable = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteTariffVariablebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As TariffVariable = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffVariable")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As TariffVariable = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertTariffVariable")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TariffVariable

            Dim _T As TariffVariable = Nothing

            If DR IsNot Nothing Then
                _T = New TariffVariable()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._TariffId = GetGUID(DR("tariff_id"))
                    _T._StartTime = GetTimeSpan(DR("start_time"))
                    _T._EndTime = GetTimeSpan(DR("end_time"))
                    _T._Units = GetInteger(DR("units"))
                    _T._Rate = GetDecimal(DR("rate"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TariffVariable)

            Dim _TariffVariableList As New List(Of TariffVariable)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TariffVariableList.Add(PropertiesFromData(_DR))
            Next

            Return _TariffVariableList

        End Function


#End Region

    End Class

End Namespace
