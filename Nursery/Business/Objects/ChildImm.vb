﻿'*****************************************************
'Generated 28/02/2016 22:34:00 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ChildImm
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ChildId As Guid?
        Dim m_ImmDate As Date?
        Dim m_Immunisation As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ChildId = Nothing
                ._ImmDate = Nothing
                ._Immunisation = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ChildId = Nothing
                ._ImmDate = Nothing
                ._Immunisation = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ImmDate")> _
        Public Property _ImmDate() As Date?
            Get
                Return m_ImmDate
            End Get
            Set(ByVal value As Date?)
                m_ImmDate = value
            End Set
        End Property

        <StoredProcParameter("@Immunisation")> _
        Public Property _Immunisation() As String
            Get
                Return m_Immunisation
            End Get
            Set(ByVal value As String)
                m_Immunisation = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ChildImm

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getChildImmbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ChildImm)

            Dim _ChildImmList As List(Of ChildImm)
            _ChildImmList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getChildImmTable"))
            Return _ChildImmList

        End Function

        Public Shared Sub SaveAll(ByVal ChildImmList As List(Of ChildImm))

            For Each _ChildImm As ChildImm In ChildImmList
                SaveRecord(_ChildImm)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ChildImm As ChildImm) As Guid
            ChildImm.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, ChildImm, "upsertChildImm")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteChildImmbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertChildImm")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ChildImm

            Dim _C As ChildImm = Nothing

            If DR IsNot Nothing Then
                _C = New ChildImm()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._ChildId = GetGUID(DR("child_id"))
                    _C._ImmDate = GetDate(DR("imm_date"))
                    _C._Immunisation = DR("immunisation").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ChildImm)

            Dim _ChildImmList As New List(Of ChildImm)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ChildImmList.Add(PropertiesFromData(_DR))
            Next

            Return _ChildImmList

        End Function


#End Region

    End Class

End Namespace
