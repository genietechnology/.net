﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Activity
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_KeyId As Guid?
        Dim m_KeyName As String
        Dim m_Type As String
        Dim m_Description As String
        Dim m_Value1 As String
        Dim m_Value2 As String
        Dim m_Value3 As String
        Dim m_Stamp As DateTime?
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_Sms As Boolean
        Dim m_SmsId As Guid?
        Dim m_Notes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._KeyId = Nothing
                ._KeyName = ""
                ._Type = ""
                ._Description = ""
                ._Value1 = ""
                ._Value2 = ""
                ._Value3 = ""
                ._Stamp = Nothing
                ._StaffId = Nothing
                ._StaffName = ""
                ._Sms = False
                ._SmsId = Nothing
                ._Notes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._KeyId = Nothing
                ._KeyName = ""
                ._Type = ""
                ._Description = ""
                ._Value1 = ""
                ._Value2 = ""
                ._Value3 = ""
                ._Stamp = Nothing
                ._StaffId = Nothing
                ._StaffName = ""
                ._Sms = False
                ._SmsId = Nothing
                ._Notes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@KeyId")> _
        Public Property _KeyId() As Guid?
            Get
                Return m_KeyId
            End Get
            Set(ByVal value As Guid?)
                m_KeyId = value
            End Set
        End Property

        <StoredProcParameter("@KeyName")> _
        Public Property _KeyName() As String
            Get
                Return m_KeyName
            End Get
            Set(ByVal value As String)
                m_KeyName = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@Value1")> _
        Public Property _Value1() As String
            Get
                Return m_Value1
            End Get
            Set(ByVal value As String)
                m_Value1 = value
            End Set
        End Property

        <StoredProcParameter("@Value2")> _
        Public Property _Value2() As String
            Get
                Return m_Value2
            End Get
            Set(ByVal value As String)
                m_Value2 = value
            End Set
        End Property

        <StoredProcParameter("@Value3")> _
        Public Property _Value3() As String
            Get
                Return m_Value3
            End Get
            Set(ByVal value As String)
                m_Value3 = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@Sms")> _
        Public Property _Sms() As Boolean
            Get
                Return m_Sms
            End Get
            Set(ByVal value As Boolean)
                m_Sms = value
            End Set
        End Property

        <StoredProcParameter("@SmsId")> _
        Public Property _SmsId() As Guid?
            Get
                Return m_SmsId
            End Get
            Set(ByVal value As Guid?)
                m_SmsId = value
            End Set
        End Property

        <StoredProcParameter("@Notes")> _
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Activity

            Dim _Activity As Activity
            _Activity = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getActivitybyID", ID))
            Return _Activity

        End Function

        Public Shared Function RetreiveAll() As List(Of Activity)

            Dim _ActivityList As List(Of Activity)
            _ActivityList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getActivityTable"))
            Return _ActivityList

        End Function

        Public Shared Sub SaveAll(ByVal ActivityList As List(Of Activity))

            For Each _Activity As Activity In ActivityList
                SaveRecord(_Activity)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Activity As Activity) As Guid
            DAL.SaveRecord(Session.ConnectionString, Activity, "upsertActivity")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteActivitybyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertActivity")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Activity

            Dim _A As New Activity()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._DayId = GetGUID(DR("day_id"))
                    _A._KeyId = GetGUID(DR("key_id"))
                    _A._KeyName = DR("key_name").ToString.Trim
                    _A._Type = DR("type").ToString.Trim
                    _A._Description = DR("description").ToString.Trim
                    _A._Value1 = DR("value_1").ToString.Trim
                    _A._Value2 = DR("value_2").ToString.Trim
                    _A._Value3 = DR("value_3").ToString.Trim
                    _A._Stamp = GetDateTime(DR("stamp"))
                    _A._StaffId = GetGUID(DR("staff_id"))
                    _A._StaffName = DR("staff_name").ToString.Trim
                    _A._Sms = GetBoolean(DR("sms"))
                    _A._SmsId = GetGUID(DR("sms_id"))
                    _A._Notes = DR("notes").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Activity)

            Dim _ActivityList As New List(Of Activity)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ActivityList.Add(PropertiesFromData(_DR))
            Next

            Return _ActivityList

        End Function


#End Region

    End Class

End Namespace
