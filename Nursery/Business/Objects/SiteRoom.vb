﻿'*****************************************************
'Generated 13/09/2016 23:08:11 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class SiteRoom
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SiteId As Guid?
        Dim m_RoomName As String
        Dim m_RoomHoursFrom As TimeSpan?
        Dim m_RoomHoursTo As TimeSpan?
        Dim m_RoomCloseFrom As Date?
        Dim m_RoomCloseTo As Date?
        Dim m_RoomCheckStaff As Boolean
        Dim m_RoomCheckChildren As Boolean
        Dim m_RoomMonitor As Boolean
        Dim m_RoomMonitorSerial As String
        Dim m_RoomColour As String
        Dim m_RoomPhoto As Byte()
        Dim m_RoomSequence As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SiteId = Nothing
                ._RoomName = ""
                ._RoomHoursFrom = Nothing
                ._RoomHoursTo = Nothing
                ._RoomCloseFrom = Nothing
                ._RoomCloseTo = Nothing
                ._RoomCheckStaff = False
                ._RoomCheckChildren = False
                ._RoomMonitor = False
                ._RoomMonitorSerial = ""
                ._RoomColour = ""
                ._RoomPhoto = Nothing
                ._RoomSequence = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SiteId = Nothing
                ._RoomName = ""
                ._RoomHoursFrom = Nothing
                ._RoomHoursTo = Nothing
                ._RoomCloseFrom = Nothing
                ._RoomCloseTo = Nothing
                ._RoomCheckStaff = False
                ._RoomCheckChildren = False
                ._RoomMonitor = False
                ._RoomMonitorSerial = ""
                ._RoomColour = ""
                ._RoomPhoto = Nothing
                ._RoomSequence = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SiteId")> _
        Public Property _SiteId() As Guid?
            Get
                Return m_SiteId
            End Get
            Set(ByVal value As Guid?)
                m_SiteId = value
            End Set
        End Property

        <StoredProcParameter("@RoomName")> _
        Public Property _RoomName() As String
            Get
                Return m_RoomName
            End Get
            Set(ByVal value As String)
                m_RoomName = value
            End Set
        End Property

        <StoredProcParameter("@RoomHoursFrom")> _
        Public Property _RoomHoursFrom() As TimeSpan?
            Get
                Return m_RoomHoursFrom
            End Get
            Set(ByVal value As TimeSpan?)
                m_RoomHoursFrom = value
            End Set
        End Property

        <StoredProcParameter("@RoomHoursTo")> _
        Public Property _RoomHoursTo() As TimeSpan?
            Get
                Return m_RoomHoursTo
            End Get
            Set(ByVal value As TimeSpan?)
                m_RoomHoursTo = value
            End Set
        End Property

        <StoredProcParameter("@RoomCloseFrom")> _
        Public Property _RoomCloseFrom() As Date?
            Get
                Return m_RoomCloseFrom
            End Get
            Set(ByVal value As Date?)
                m_RoomCloseFrom = value
            End Set
        End Property

        <StoredProcParameter("@RoomCloseTo")> _
        Public Property _RoomCloseTo() As Date?
            Get
                Return m_RoomCloseTo
            End Get
            Set(ByVal value As Date?)
                m_RoomCloseTo = value
            End Set
        End Property

        <StoredProcParameter("@RoomCheckStaff")> _
        Public Property _RoomCheckStaff() As Boolean
            Get
                Return m_RoomCheckStaff
            End Get
            Set(ByVal value As Boolean)
                m_RoomCheckStaff = value
            End Set
        End Property

        <StoredProcParameter("@RoomCheckChildren")> _
        Public Property _RoomCheckChildren() As Boolean
            Get
                Return m_RoomCheckChildren
            End Get
            Set(ByVal value As Boolean)
                m_RoomCheckChildren = value
            End Set
        End Property

        <StoredProcParameter("@RoomMonitor")> _
        Public Property _RoomMonitor() As Boolean
            Get
                Return m_RoomMonitor
            End Get
            Set(ByVal value As Boolean)
                m_RoomMonitor = value
            End Set
        End Property

        <StoredProcParameter("@RoomMonitorSerial")> _
        Public Property _RoomMonitorSerial() As String
            Get
                Return m_RoomMonitorSerial
            End Get
            Set(ByVal value As String)
                m_RoomMonitorSerial = value
            End Set
        End Property

        <StoredProcParameter("@RoomColour")> _
        Public Property _RoomColour() As String
            Get
                Return m_RoomColour
            End Get
            Set(ByVal value As String)
                m_RoomColour = value
            End Set
        End Property

        <StoredProcParameter("@RoomPhoto")> _
        Public Property _RoomPhoto() As Byte()
            Get
                Return m_RoomPhoto
            End Get
            Set(ByVal value As Byte())
                m_RoomPhoto = value
            End Set
        End Property

        <StoredProcParameter("@RoomSequence")> _
        Public Property _RoomSequence() As Integer
            Get
                Return m_RoomSequence
            End Get
            Set(ByVal value As Integer)
                m_RoomSequence = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As SiteRoom

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSiteRoombyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of SiteRoom)

            Dim _SiteRoomList As List(Of SiteRoom)
            _SiteRoomList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSiteRoomTable"))
            Return _SiteRoomList

        End Function

        Public Shared Sub SaveAll(ByVal SiteRoomList As List(Of SiteRoom))

            For Each _SiteRoom As SiteRoom In SiteRoomList
                SaveRecord(_SiteRoom)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal SiteRoom As SiteRoom) As Guid
            SiteRoom.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, SiteRoom, "upsertSiteRoom")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSiteRoombyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertSiteRoom")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As SiteRoom

            Dim _S As SiteRoom = Nothing

            If DR IsNot Nothing Then
                _S = New SiteRoom()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._SiteId = GetGUID(DR("site_id"))
                    _S._RoomName = DR("room_name").ToString.Trim
                    _S._RoomHoursFrom = GetTimeSpan(DR("room_hours_from"))
                    _S._RoomHoursTo = GetTimeSpan(DR("room_hours_to"))
                    _S._RoomCloseFrom = GetDate(DR("room_close_from"))
                    _S._RoomCloseTo = GetDate(DR("room_close_to"))
                    _S._RoomCheckStaff = GetBoolean(DR("room_check_staff"))
                    _S._RoomCheckChildren = GetBoolean(DR("room_check_children"))
                    _S._RoomMonitor = GetBoolean(DR("room_monitor"))
                    _S._RoomMonitorSerial = DR("room_monitor_serial").ToString.Trim
                    _S._RoomColour = DR("room_colour").ToString.Trim
                    _S._RoomPhoto = GetImage(DR("room_photo"))
                    _S._RoomSequence = GetInteger(DR("room_sequence"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of SiteRoom)

            Dim _SiteRoomList As New List(Of SiteRoom)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SiteRoomList.Add(PropertiesFromData(_DR))
            Next

            Return _SiteRoomList

        End Function


#End Region

    End Class

End Namespace
