﻿'*****************************************************
'Generated 22/05/2017 17:05:25 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RotaSlot
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RotaId As Guid?
        Dim m_SlotId As Guid?
        Dim m_SlotDate As Date?
        Dim m_SlotOpen As Boolean
        Dim m_SlotStart As TimeSpan?
        Dim m_SlotFinish As TimeSpan?
        Dim m_SlotRatio As Guid?
        Dim m_SlotRoom As String
        Dim m_ChildCount As Integer
        Dim m_StaffRatio As Integer
        Dim m_StaffRequired As Integer
        Dim m_StaffAvailable As Integer
        Dim m_StaffRostered As Integer
        Dim m_StaffRosteredList As String
        Dim m_StaffShort As Integer
        Dim m_StaffSpare As Integer
        Dim m_StaffSpareList As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RotaId = Nothing
                ._SlotId = Nothing
                ._SlotDate = Nothing
                ._SlotOpen = False
                ._SlotStart = Nothing
                ._SlotFinish = Nothing
                ._SlotRatio = Nothing
                ._SlotRoom = ""
                ._ChildCount = 0
                ._StaffRatio = 0
                ._StaffRequired = 0
                ._StaffAvailable = 0
                ._StaffRostered = 0
                ._StaffRosteredList = ""
                ._StaffShort = 0
                ._StaffSpare = 0
                ._StaffSpareList = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RotaId = Nothing
                ._SlotId = Nothing
                ._SlotDate = Nothing
                ._SlotOpen = False
                ._SlotStart = Nothing
                ._SlotFinish = Nothing
                ._SlotRatio = Nothing
                ._SlotRoom = ""
                ._ChildCount = 0
                ._StaffRatio = 0
                ._StaffRequired = 0
                ._StaffAvailable = 0
                ._StaffRostered = 0
                ._StaffRosteredList = ""
                ._StaffShort = 0
                ._StaffSpare = 0
                ._StaffSpareList = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RotaId")>
        Public Property _RotaId() As Guid?
            Get
                Return m_RotaId
            End Get
            Set(ByVal value As Guid?)
                m_RotaId = value
            End Set
        End Property

        <StoredProcParameter("@SlotId")>
        Public Property _SlotId() As Guid?
            Get
                Return m_SlotId
            End Get
            Set(ByVal value As Guid?)
                m_SlotId = value
            End Set
        End Property

        <StoredProcParameter("@SlotDate")>
        Public Property _SlotDate() As Date?
            Get
                Return m_SlotDate
            End Get
            Set(ByVal value As Date?)
                m_SlotDate = value
            End Set
        End Property

        <StoredProcParameter("@SlotOpen")>
        Public Property _SlotOpen() As Boolean
            Get
                Return m_SlotOpen
            End Get
            Set(ByVal value As Boolean)
                m_SlotOpen = value
            End Set
        End Property

        <StoredProcParameter("@SlotStart")>
        Public Property _SlotStart() As TimeSpan?
            Get
                Return m_SlotStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_SlotStart = value
            End Set
        End Property

        <StoredProcParameter("@SlotFinish")>
        Public Property _SlotFinish() As TimeSpan?
            Get
                Return m_SlotFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_SlotFinish = value
            End Set
        End Property

        <StoredProcParameter("@SlotRatio")>
        Public Property _SlotRatio() As Guid?
            Get
                Return m_SlotRatio
            End Get
            Set(ByVal value As Guid?)
                m_SlotRatio = value
            End Set
        End Property

        <StoredProcParameter("@SlotRoom")>
        Public Property _SlotRoom() As String
            Get
                Return m_SlotRoom
            End Get
            Set(ByVal value As String)
                m_SlotRoom = value
            End Set
        End Property

        <StoredProcParameter("@ChildCount")>
        Public Property _ChildCount() As Integer
            Get
                Return m_ChildCount
            End Get
            Set(ByVal value As Integer)
                m_ChildCount = value
            End Set
        End Property

        <StoredProcParameter("@StaffRatio")>
        Public Property _StaffRatio() As Integer
            Get
                Return m_StaffRatio
            End Get
            Set(ByVal value As Integer)
                m_StaffRatio = value
            End Set
        End Property

        <StoredProcParameter("@StaffRequired")>
        Public Property _StaffRequired() As Integer
            Get
                Return m_StaffRequired
            End Get
            Set(ByVal value As Integer)
                m_StaffRequired = value
            End Set
        End Property

        <StoredProcParameter("@StaffAvailable")>
        Public Property _StaffAvailable() As Integer
            Get
                Return m_StaffAvailable
            End Get
            Set(ByVal value As Integer)
                m_StaffAvailable = value
            End Set
        End Property

        <StoredProcParameter("@StaffRostered")>
        Public Property _StaffRostered() As Integer
            Get
                Return m_StaffRostered
            End Get
            Set(ByVal value As Integer)
                m_StaffRostered = value
            End Set
        End Property

        <StoredProcParameter("@StaffRosteredList")>
        Public Property _StaffRosteredList() As String
            Get
                Return m_StaffRosteredList
            End Get
            Set(ByVal value As String)
                m_StaffRosteredList = value
            End Set
        End Property

        <StoredProcParameter("@StaffShort")>
        Public Property _StaffShort() As Integer
            Get
                Return m_StaffShort
            End Get
            Set(ByVal value As Integer)
                m_StaffShort = value
            End Set
        End Property

        <StoredProcParameter("@StaffSpare")>
        Public Property _StaffSpare() As Integer
            Get
                Return m_StaffSpare
            End Get
            Set(ByVal value As Integer)
                m_StaffSpare = value
            End Set
        End Property

        <StoredProcParameter("@StaffSpareList")>
        Public Property _StaffSpareList() As String
            Get
                Return m_StaffSpareList
            End Get
            Set(ByVal value As String)
                m_StaffSpareList = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As RotaSlot

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getRotaSlotbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As RotaSlot

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getRotaSlotbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of RotaSlot)

            Dim _RotaSlotList As List(Of RotaSlot)
            _RotaSlotList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getRotaSlotTable"))
            Return _RotaSlotList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of RotaSlot)

            Dim _RotaSlotList As List(Of RotaSlot)
            _RotaSlotList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getRotaSlotTable"))
            Return _RotaSlotList

        End Function

        Public Shared Sub SaveAll(ByVal RotaSlotList As List(Of RotaSlot))

            For Each _RotaSlot As RotaSlot In RotaSlotList
                SaveRecord(_RotaSlot)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal RotaSlotList As List(Of RotaSlot))

            For Each _RotaSlot As RotaSlot In RotaSlotList
                SaveRecord(ConnectionString, _RotaSlot)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal RotaSlot As RotaSlot) As Guid
            RotaSlot.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaSlot, "upsertRotaSlot")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal RotaSlot As RotaSlot) As Guid
            RotaSlot.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, RotaSlot, "upsertRotaSlot")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteRotaSlotbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteRotaSlotbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaSlot")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Me, "upsertRotaSlot")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RotaSlot

            Dim _R As RotaSlot = Nothing

            If DR IsNot Nothing Then
                _R = New RotaSlot()
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._RotaId = GetGUID(DR("rota_id"))
                    _R._SlotId = GetGUID(DR("slot_id"))
                    _R._SlotDate = GetDate(DR("slot_date"))
                    _R._SlotOpen = GetBoolean(DR("slot_open"))
                    _R._SlotStart = GetTimeSpan(DR("slot_start"))
                    _R._SlotFinish = GetTimeSpan(DR("slot_finish"))
                    _R._SlotRatio = GetGUID(DR("slot_ratio"))
                    _R._SlotRoom = DR("slot_room").ToString.Trim
                    _R._ChildCount = GetInteger(DR("child_count"))
                    _R._StaffRatio = GetInteger(DR("staff_ratio"))
                    _R._StaffRequired = GetInteger(DR("staff_required"))
                    _R._StaffAvailable = GetInteger(DR("staff_available"))
                    _R._StaffRostered = GetInteger(DR("staff_rostered"))
                    _R._StaffRosteredList = DR("staff_rostered_list").ToString.Trim
                    _R._StaffShort = GetInteger(DR("staff_short"))
                    _R._StaffSpare = GetInteger(DR("staff_spare"))
                    _R._StaffSpareList = DR("staff_spare_list").ToString.Trim

                End With
            End If

            Return _R

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RotaSlot)

            Dim _RotaSlotList As New List(Of RotaSlot)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _RotaSlotList.Add(PropertiesFromData(_DR))
            Next

            Return _RotaSlotList

        End Function


#End Region

    End Class

End Namespace
