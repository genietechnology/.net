﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class DayRatio
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayId As Guid?
        Dim m_Date As Date?
        Dim m_Am As Boolean
        Dim m_Pm As Boolean
        Dim m_ChildId As Guid?
        Dim m_ChildName As String
        Dim m_Age As Byte
        Dim m_Moveable As String
        Dim m_InspGroupId As Guid?
        Dim m_InspGroupName As String
        Dim m_GroupId As Guid?
        Dim m_GroupName As String
        Dim m_ActualId As Guid?
        Dim m_ActualName As String
        Dim m_StaffId As Guid?
        Dim m_StaffName As String
        Dim m_SessionId As Guid?
        Dim m_SessionName As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayId = Nothing
                ._Date = Nothing
                ._Am = False
                ._Pm = False
                ._ChildId = Nothing
                ._ChildName = ""
                ._Age = Nothing
                ._Moveable = ""
                ._InspGroupId = Nothing
                ._InspGroupName = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._ActualId = Nothing
                ._ActualName = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._SessionId = Nothing
                ._SessionName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayId = Nothing
                ._Date = Nothing
                ._Am = False
                ._Pm = False
                ._ChildId = Nothing
                ._ChildName = ""
                ._Age = Nothing
                ._Moveable = ""
                ._InspGroupId = Nothing
                ._InspGroupName = ""
                ._GroupId = Nothing
                ._GroupName = ""
                ._ActualId = Nothing
                ._ActualName = ""
                ._StaffId = Nothing
                ._StaffName = ""
                ._SessionId = Nothing
                ._SessionName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayId")> _
        Public Property _DayId() As Guid?
            Get
                Return m_DayId
            End Get
            Set(ByVal value As Guid?)
                m_DayId = value
            End Set
        End Property

        <StoredProcParameter("@Date")> _
        Public Property _Date() As Date?
            Get
                Return m_Date
            End Get
            Set(ByVal value As Date?)
                m_Date = value
            End Set
        End Property

        <StoredProcParameter("@Am")> _
        Public Property _Am() As Boolean
            Get
                Return m_Am
            End Get
            Set(ByVal value As Boolean)
                m_Am = value
            End Set
        End Property

        <StoredProcParameter("@Pm")> _
        Public Property _Pm() As Boolean
            Get
                Return m_Pm
            End Get
            Set(ByVal value As Boolean)
                m_Pm = value
            End Set
        End Property

        <StoredProcParameter("@ChildId")> _
        Public Property _ChildId() As Guid?
            Get
                Return m_ChildId
            End Get
            Set(ByVal value As Guid?)
                m_ChildId = value
            End Set
        End Property

        <StoredProcParameter("@ChildName")> _
        Public Property _ChildName() As String
            Get
                Return m_ChildName
            End Get
            Set(ByVal value As String)
                m_ChildName = value
            End Set
        End Property

        <StoredProcParameter("@Age")> _
        Public Property _Age() As Byte
            Get
                Return m_Age
            End Get
            Set(ByVal value As Byte)
                m_Age = value
            End Set
        End Property

        <StoredProcParameter("@Moveable")> _
        Public Property _Moveable() As String
            Get
                Return m_Moveable
            End Get
            Set(ByVal value As String)
                m_Moveable = value
            End Set
        End Property

        <StoredProcParameter("@InspGroupId")> _
        Public Property _InspGroupId() As Guid?
            Get
                Return m_InspGroupId
            End Get
            Set(ByVal value As Guid?)
                m_InspGroupId = value
            End Set
        End Property

        <StoredProcParameter("@InspGroupName")> _
        Public Property _InspGroupName() As String
            Get
                Return m_InspGroupName
            End Get
            Set(ByVal value As String)
                m_InspGroupName = value
            End Set
        End Property

        <StoredProcParameter("@GroupId")> _
        Public Property _GroupId() As Guid?
            Get
                Return m_GroupId
            End Get
            Set(ByVal value As Guid?)
                m_GroupId = value
            End Set
        End Property

        <StoredProcParameter("@GroupName")> _
        Public Property _GroupName() As String
            Get
                Return m_GroupName
            End Get
            Set(ByVal value As String)
                m_GroupName = value
            End Set
        End Property

        <StoredProcParameter("@ActualId")> _
        Public Property _ActualId() As Guid?
            Get
                Return m_ActualId
            End Get
            Set(ByVal value As Guid?)
                m_ActualId = value
            End Set
        End Property

        <StoredProcParameter("@ActualName")> _
        Public Property _ActualName() As String
            Get
                Return m_ActualName
            End Get
            Set(ByVal value As String)
                m_ActualName = value
            End Set
        End Property

        <StoredProcParameter("@StaffId")> _
        Public Property _StaffId() As Guid?
            Get
                Return m_StaffId
            End Get
            Set(ByVal value As Guid?)
                m_StaffId = value
            End Set
        End Property

        <StoredProcParameter("@StaffName")> _
        Public Property _StaffName() As String
            Get
                Return m_StaffName
            End Get
            Set(ByVal value As String)
                m_StaffName = value
            End Set
        End Property

        <StoredProcParameter("@SessionId")> _
        Public Property _SessionId() As Guid?
            Get
                Return m_SessionId
            End Get
            Set(ByVal value As Guid?)
                m_SessionId = value
            End Set
        End Property

        <StoredProcParameter("@SessionName")> _
        Public Property _SessionName() As String
            Get
                Return m_SessionName
            End Get
            Set(ByVal value As String)
                m_SessionName = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As DayRatio

            Dim _DayRatio As DayRatio
            _DayRatio = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDayRatiobyID", ID))
            Return _DayRatio

        End Function

        Public Shared Function RetreiveAll() As List(Of DayRatio)

            Dim _DayRatioList As List(Of DayRatio)
            _DayRatioList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDayRatioTable"))
            Return _DayRatioList

        End Function

        Public Shared Sub SaveAll(ByVal DayRatioList As List(Of DayRatio))

            For Each _DayRatio As DayRatio In DayRatioList
                SaveRecord(_DayRatio)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal DayRatio As DayRatio) As Guid
            DAL.SaveRecord(CType(Session.ConnectionString, String), DayRatio, "upsertDayRatio")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteDayRatiobyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(CType(Session.ConnectionString, String), Me, "upsertDayRatio")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As DayRatio

            Dim _D As New DayRatio()

            If DR IsNot Nothing Then
                With DR
                    _D.IsNew = False
                    _D.IsDeleted = False
                    _D._ID = GetGUID(DR("ID"))
                    _D._DayId = GetGUID(DR("day_id"))
                    _D._Date = GetDate(DR("date"))
                    _D._Am = GetBoolean(DR("am"))
                    _D._Pm = GetBoolean(DR("pm"))
                    _D._ChildId = GetGUID(DR("child_id"))
                    _D._ChildName = DR("child_name").ToString.Trim
                    _D._Age = GetByte(DR("age"))
                    _D._Moveable = DR("moveable").ToString.Trim
                    _D._InspGroupId = GetGUID(DR("insp_group_id"))
                    _D._InspGroupName = DR("insp_group_name").ToString.Trim
                    _D._GroupId = GetGUID(DR("group_id"))
                    _D._GroupName = DR("group_name").ToString.Trim
                    _D._ActualId = GetGUID(DR("actual_id"))
                    _D._ActualName = DR("actual_name").ToString.Trim
                    _D._StaffId = GetGUID(DR("staff_id"))
                    _D._StaffName = DR("staff_name").ToString.Trim
                    _D._SessionId = GetGUID(DR("session_id"))
                    _D._SessionName = DR("session_name").ToString.Trim

                End With
            End If

            Return _D

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of DayRatio)

            Dim _DayRatioList As New List(Of DayRatio)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DayRatioList.Add(PropertiesFromData(_DR))
            Next

            Return _DayRatioList

        End Function


#End Region

    End Class

End Namespace
