﻿Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms
Imports Care.Shared

Namespace Business
    Partial Class Day

        Public Shared Function FindDay(ByVal ParentForm As Form) As String

            Dim _SQL As String = ""
            _SQL += "select d.date as 'Date', DATENAME(WEEKDAY, date) as ' ', d.site_name as 'Site', d.breakfast_name as 'Breakfast', d.lunch_name as 'Lunch', d.tea_name as 'Tea'"
            _SQL += " from Day d"

            Dim _Find As New GenericFind
            With _Find
                .ConnectionString = Session.ConnectionString
                .Caption = "Find Day Record from the Last 14 days"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .GridSQL = _SQL
                .GridWhereClause = " and DATEDIFF(DAY, d.date, getdate()) <= 14 and site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
                .GridOrderBy = " order by d.date desc"
                .ReturnField = "ID"
                .FormWidth = 1000
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

        Public Shared Function RetreiveByDateAndSite(ByVal CheckDate As Date, ByVal SiteID As Guid) As Day

            Dim _SQL As String = ""
            _SQL += "select * from Day"
            _SQL += " where date = " + ValueHandler.SQLDate(CheckDate, True)
            _SQL += " and site_id = '" + SiteID.ToString + "'"

            Dim _Day As Day = PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
            Return _Day

        End Function

        Public Shared Function RetreiveByDateOnly(ByVal CheckDate As Date) As Day
            Dim _SQL As String = "select * from Day where date = " + ValueHandler.SQLDate(CheckDate, True)
            Dim _Day As Day = PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
            Return _Day
        End Function

    End Class

End Namespace
