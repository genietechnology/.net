﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Namespace Business

    Partial Class Meal

        Public Enum EnumMealType
            Breakfast
            Snack
            Lunch
            Tea
            Dessert
        End Enum

        Public Shared Function FindMeal(ByVal ParentForm As Form, Optional ByVal MealType As EnumMealType? = Nothing) As String

            Dim _Find As New GenericFind

            Dim _SQL As String = "select name as 'Name', breakfast as 'Breakfast', snack as 'Snack', lunch as 'Lunch', tea as 'Tea', dessert as 'Dessert'," & _
                                 " (select count(*) from MealComponents where MealComponents.meal_id = Meals.ID) as 'Items'" & _
                                 " from Meals"

            Dim _Where As String = ""
            If MealType = EnumMealType.Breakfast Then _Where = " and breakfast = 1"
            If MealType = EnumMealType.Snack Then _Where = " and snack = 1"
            If MealType = EnumMealType.Lunch Then _Where = " and lunch = 1"
            If MealType = EnumMealType.Tea Then _Where = " and tea = 1"
            If MealType = EnumMealType.Dessert Then _Where = " and dessert = 1"

            With _Find
                .Caption = "Find Meals"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = _SQL
                .GridWhereClause = _Where
                .GridOrderBy = "order by name"
                .ReturnField = "Meals.ID"
                .Option1Caption = "Name"
                .Option1Field = "Meals.name"
                .Option2Caption = "Description"
                .Option2Field = "Meals.description"
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

        Public Function ContainsComponent(ByVal FoodID As Guid) As Boolean

            Dim _Return As Boolean = False
            Dim _SQL As String = "select ID from MealComponents" & _
                                 " where meal_id = '" + Me._ID.ToString + "'" & _
                                 " and food_id = '" + FoodID.ToString + "'"

            Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then
                If _DT.Rows.Count > 0 Then
                    _Return = True
                End If
            End If

            _DT.Dispose()
            _DT = Nothing

            Return _Return

        End Function

    End Class

End Namespace

