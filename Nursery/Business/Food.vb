﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Namespace Business

    Partial Class Food

        Public Shared Function FindFood(ByVal ParentForm As Form) As String

            Dim _Find As New GenericFind
            With _Find
                .ConnectionString = Session.ConnectionString
                .Caption = "Find Food"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .GridSQL = "select name as 'Name', group_name as 'Group', baby_food as 'Baby Food'," & _
                           " (select count(*) from MealComponents where MealComponents.food_id = Food.ID) as 'Meal Count'" & _
                           " from Food"
                .GridOrderBy = "order by group_name, name"
                .ReturnField = "ID"
                .Option1Caption = "Name"
                .Option1Field = "name"
                .Option2Caption = "Group"
                .Option2Field = "group_name"
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

        Private Sub CalculateUnit()

            If m_Cost > 0 And m_PackSize > 0 Then
                m_CostItem = Decimal.Round(m_Cost / m_PackSize, 2)
            End If

        End Sub

    End Class

End Namespace
