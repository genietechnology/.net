﻿

Imports Care.Global
Imports Care.Data

Namespace Business
    Partial Class Contact

        Public Shared Function SaveContact(ByRef Contact As Contact) As Guid

            Contact._Fullname = Contact._Forename + " " + Contact._Surname
            If Contact._PrimaryCont = True Then ClearPrimaryContacts(Contact._FamilyId.ToString)

            Dim _Current As Contact = RetreiveByID(Session.ConnectionString, Contact._ID.Value)
            Contact.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Contact, _Current, "upsertContact")

        End Function

        Private Shared Sub ClearPrimaryContacts(FamilyID As String)
            Dim _SQL As String = "update Contacts set primary_cont = 0 where family_id = '" & FamilyID & "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

        Public Shared Sub AddContact(ByVal FamilyID As Guid)

            Dim _frm As New frmContact
            With _frm
                .AddContact = True
                .ID = FamilyID
                .Text = "Add Contact"
                .ShowDialog()
            End With

        End Sub

        Public Shared Sub ViewContact(ByVal ContactID As Guid)

            Dim _frm As New frmContact
            With _frm
                .AddContact = False
                .ID = ContactID
                .Text = "View Contact"
                .ShowDialog()
            End With

        End Sub

        Public Shared Sub DeleteContact(ByVal ContactID As Guid)
            Dim _SQL As String = "delete from Contacts where id = '" & ContactID.ToString & "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

        Public Shared Function RetreiveByFamilyID(ByVal FamilyID As String) As List(Of Contact)
            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Contacts where family_id = '" + FamilyID + "'"))
            Return _ContactList
        End Function

        Public Shared Function RetrievePrimaryContact(FamilyID As String) As Business.Contact

            Dim _SQL As String = "select * from contacts where family_id = '" + FamilyID + "'" & _
                                 " and primary_cont = 1"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

            If _DR IsNot Nothing Then
                Return PropertiesFromData(_DR)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function RetrieveInvoiceContacts(FamilyID As Guid) As List(Of Contact)

            Dim _SQL As String = ""

            _SQL += "select * from contacts"
            _SQL += " where family_id = '" + FamilyID.ToString + "'"
            _SQL += " and invoice_email = 1"
            _SQL += " and len(email) > 0"

            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ContactList

        End Function

        Public Shared Function RetrieveContactsBySite(SiteID As Guid) As List(Of Contact)

            Dim _SQL As String = ""
            _SQL += "select c.* from contacts c"
            _SQL += " left join Family f on f.ID = c.family_id"
            _SQL += " where f.site_id = '" + SiteID.ToString() + "'"

            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ContactList

        End Function

        Private Sub BuildFullname()
            _Fullname = _Forename & " " & _Surname
        End Sub

        Public Shared Sub ResetPrimaryContacts(ByVal FamilyID As Guid)

            Dim _SQL As String = "update contacts set primary_cont = 0" & _
                                 " where family_id = '" & FamilyID.ToString & "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

    End Class

End Namespace