﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms

Namespace Business

    Partial Class Child

        Private Shared m_FindForm As frmChildFind = Nothing

        Public ReadOnly Property AgeInMonths As Integer
            Get
                If Me._Dob.HasValue Then
                    Return CInt(DateDiff(DateInterval.Month, Me._Dob.Value, Today))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public Function ReturnInspGroup() As Business.InspGroup
            Return Business.InspGroup.RetreiveByAge(Me.AgeInMonths)
        End Function

        Public Sub ProcessFirstDay()
            Me._Status = "Current"
            Me.Store()
        End Sub

        Public Sub ProcessLastDay()
            Me._Status = "Left"
            Me.Store()
        End Sub

        Public Overrides Sub SetDefaultValues()

            If Me.IsNew Then
                Me._CreatedBy = Session.CurrentUser.ID
                Me._CreatedStamp = Now
            End If

            If ParameterHandler.ReturnBoolean("USECLASS") Then
                If Me._Class = "" Then Me._Class = ParameterHandler.ReturnString("DEFCHILDCLASS")
            End If

            If Not Me._SiteId.HasValue Then
                If Me._SiteName = "" Then Me._SiteName = ParameterHandler.ReturnString("DEFSITE")
                Dim _S As Business.Site = Business.Site.RetrieveByName(Me._SiteName)
                If _S IsNot Nothing Then
                    Me._SiteId = _S._ID
                    Me._SiteName = _S._Name
                End If
            End If

            If Me._TermType = "" Then Me._TermType = ParameterHandler.ReturnString("DEFCHILDTERM")
            If Me._AllergyRating = "" Then Me._AllergyRating = "None"
            If Me._DietRestrict = "" Then Me._DietRestrict = "None"

            If Me._FundingType = "" Then Me._FundingType = "None"
            If Me._PaymentMethod = "" Then Me._PaymentMethod = "Unknown"

            If Me._FamilyId.HasValue Then
                If Me._InvoiceDue = "" OrElse Me._InvoiceFreq = "" Then
                    Dim _F As Business.Family = Business.Family.RetreiveByID(Me._FamilyId.Value)
                    If _F IsNot Nothing Then
                        If Me._InvoiceDue = "" Then Me._InvoiceDue = _F._InvoiceDue
                        If Me._InvoiceFreq = "" Then Me._InvoiceFreq = _F._InvoiceFreq
                    End If
                End If
            End If

            If Me._Knownas = "" Then
                Me._Fullname = Me._Forename + " " + Me._Surname
                Me._SurnameForename = Me._Surname + ", " + Me._Forename
            Else
                Me._Fullname = Me._Knownas + " " + Me._Surname
                Me._SurnameForename = Me._Surname + ", " + Me._Knownas
            End If

            If Me._MoveMode = "" Then
                Me._MoveMode = ParameterHandler.ReturnString("DEFMOVEMODE")
                If Me._MoveMode = "" Then Me._MoveMode = "Disabled"
            End If

        End Sub

        Public Shared Function FindChild(ByVal ParentForm As Form) As String

            If m_FindForm Is Nothing Then m_FindForm = New frmChildFind
            m_FindForm.ShowDialog()

            If m_FindForm.SelectedRecordID <> "" Then
                Return m_FindForm.SelectedRecordID
            Else
                Return ""
            End If

        End Function

        Public Shared Sub DrillDown(ByVal ChildID As Guid, Optional SessionsTab As Boolean = False)

            Dim _frm As New frmChildren(ChildID, SessionsTab)
            With _frm
                .Text = "View Child"
                .StartPosition = FormStartPosition.CenterScreen
                .ShowDialog()
            End With

        End Sub

        Public Shared Function RetreiveByFamilyID(ByVal FamilyID As Guid) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where family_id = '" & FamilyID.ToString & "'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetreiveByGroupID(ByVal GroupID As Guid) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where group_id = '" & GroupID.ToString & "'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveAllByAge() As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " order by dob, surname, forename"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLiveChildren(FamilyID As Guid) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where Children.family_id = '" & FamilyID.ToString & "'" & _
                                 " and Children.status = 'Current'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLiveChildrenBySite(SiteID As Guid) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where Children.site_id = '" & SiteID.ToString & "'" & _
                                 " and Children.status = 'Current'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLiveChildren() As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where Children.status = 'Current'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLiveAndWaitingChildrenByAge() As List(Of Child)

            Dim _SQL As String = ""

            _SQL += "select * from Children"
            _SQL += " where Children.status in ('Current', 'On Waiting List')"
            _SQL += " order by Children.dob"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLiveAndWaitingChildrenBySiteName(ByVal SiteName As String) As List(Of Child)

            Dim _SQL As String = ""

            _SQL += "select * from Children"
            _SQL += " where site_name = '" + SiteName + "'"
            _SQL += " and Children.status in ('Current', 'On Waiting List')"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveDueInToday() As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where " + Today.DayOfWeek.ToString + " is not null"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Sub CheckForDuplicates(ByVal DOB As Date)

            Dim _SQL As String = "select * from Children" & _
                                     " where Children.DOB = '" & ValueHandler.SQLDate(DOB) & "'" & _
                                     " and Children.status <> 'Left'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            If _ChildList IsNot Nothing Then
                If _ChildList.Count > 0 Then

                    Dim _Children As String = ""
                    For Each _Child As Business.Child In _ChildList
                        If _Children = "" Then
                            _Children = _Child._Fullname
                        Else
                            _Children += vbCrLf + _Child._Fullname
                        End If
                    Next

                    CareMessage("Have you entered this Child before?" + vbCrLf + vbCrLf + "The following children share this birthday:" + vbCrLf + vbCrLf + _Children, MessageBoxIcon.Information, "Duplicate Child Check")

                End If
            End If

        End Sub

        Public Shared Function RetrieveStarters(FamilyID As Guid, ActionDate As Date) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where Children.family_id = '" & FamilyID.ToString & "'" & _
                                 " and Children.started <= '" & Format(ActionDate, "yyyy-MM-dd") & "'" & _
                                 " and Children.status = 'On Waiting List'"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveLeavers(FamilyID As Guid, FirstDate As Date, LastDate As Date) As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where Children.family_id = '" & FamilyID.ToString & "'" & _
                                 " and Children.status = 'Left'" & _
                                 " and (Children.date_left between '" & Format(FirstDate, "yyyy-MM-dd") & "' and '" & Format(LastDate, "yyyy-MM-dd") & "'" & _
                                 " or Children.date_left > '" & Format(LastDate, "yyyy-MM-dd") & "')"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function RetrieveMovers() As List(Of Child)

            Dim _SQL As String = "select * from Children" & _
                                 " where status = 'Current'" & _
                                 " and move_mode in ('Auto', 'Manual')" & _
                                 " order by surname, forename"

            Dim _ChildList As List(Of Child)
            _ChildList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildList

        End Function

        Public Shared Function ProcessStarters(ActionDate As Date) As Integer

            Dim _i As Integer = 0
            Dim _SQL As String = "select * from Children" & _
                                 " where Children.started <= '" & Format(ActionDate, "yyyy-MM-dd") & "'" & _
                                 " and Children.status = 'On Waiting List'"

            Dim _ChildList As List(Of Child) = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            For Each _Child As Business.Child In _ChildList
                _Child.ProcessFirstDay()
                _i += 1
            Next

            _ChildList = Nothing

            Return _i

        End Function

        Public Shared Function ProcessLeavers(ActionDate As Date) As Integer

            Dim _i As Integer = 0
            Dim _SQL As String = "select * from Children" & _
                     " where Children.date_left < '" & Format(ActionDate, "yyyy-MM-dd") & "'" & _
                     " and Children.status = 'Current'"

            Dim _ChildList As List(Of Child) = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            For Each _Child As Business.Child In _ChildList
                _Child.ProcessLastDay()
                _i += 1
            Next

            _ChildList = Nothing

            Return _i

        End Function

        Public Shared Sub BuildDayPatternsForEveryone(ByVal CheckDate As Date)

            Dim _Children As List(Of Business.Child) = Business.Child.RetrieveLiveChildren
            If _Children IsNot Nothing Then
                If _Children.Count > 0 Then
                    Session.SetupProgressBar("Rebuilding Attendance Patterns...", _Children.Count)
                    For Each _C As Business.Child In _Children
                        _C.BuildDayPatterns(CheckDate)
                        Session.StepProgressBar()
                    Next
                    Session.HideProgressBar()
                End If
            End If

        End Sub

        Public Sub BuildDayPatterns(ByVal CheckDate As Date)

            Me._Monday = False
            Me._Tuesday = False
            Me._Wednesday = False
            Me._Thursday = False
            Me._Friday = False
            Me._Saturday = False
            Me._Sunday = False

            Dim _FromDate As Date = CheckDate
            If _FromDate.DayOfWeek <> DayOfWeek.Monday Then _FromDate = ValueHandler.NearestDate(_FromDate, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

            Dim _ToDate As Date = CheckDate
            If _ToDate.DayOfWeek <> DayOfWeek.Sunday Then _ToDate = ValueHandler.NearestDate(_ToDate, DayOfWeek.Sunday, ValueHandler.EnumDirection.Forwards)

            'fetch the bookings for this week
            Dim _SQL As String = ""
            _SQL += "select booking_date, session_count from Bookings"
            _SQL += " where child_id = '" + Me._ID.ToString + "'"
            _SQL += " and booking_status = 'Booked'"
            _SQL += " and booking_date between " + ValueHandler.SQLDate(_FromDate, True) + " And " + ValueHandler.SQLDate(_ToDate, True)
            _SQL += " order by booking_date"

            Dim _Bookings As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _Bookings IsNot Nothing Then

                For Each _b As DataRow In _Bookings.Rows

                    Dim _BookingDate As Date = ValueHandler.ConvertDate(_b.Item("booking_date")).Value
                    Dim _BookingCount As Integer = ValueHandler.ConvertInteger(_b.Item("session_count"))

                    If _BookingCount > 0 Then

                        Select Case _BookingDate.DayOfWeek

                            Case DayOfWeek.Monday
                                Me._Monday = True

                            Case DayOfWeek.Tuesday
                                Me._Tuesday = True

                            Case DayOfWeek.Wednesday
                                Me._Wednesday = True

                            Case DayOfWeek.Thursday
                                Me._Thursday = True

                            Case DayOfWeek.Friday
                                Me._Friday = True

                            Case DayOfWeek.Saturday
                                Me._Saturday = True

                            Case DayOfWeek.Sunday
                                Me._Sunday = True

                        End Select

                    End If

                Next

                Me.Store()

            End If

        End Sub

        Public Sub BuildBookings()
            Dim _BE As New BookingsEngine
            _BE.BuildBookings(Me._ID.Value, Me._Started, Bookings.CalendarCutOff)
        End Sub

        Public Shared Sub BuildBookings(ByVal ChildID As Guid)

            Dim _C As Business.Child = Business.Child.RetreiveByID(ChildID)
            If _C IsNot Nothing Then
                Dim _BE As New BookingsEngine
                _BE.BuildBookings(_C._ID.Value, _C._Started, Bookings.CalendarCutOff)
                _C.BuildDayPatterns(Today)
                _C = Nothing
            End If

        End Sub

        Public Sub Delete()

            DeleteChildTable("ChildAbsence")
            DeleteChildTable("ChildActivity")
            DeleteChildTable("ChildAdvanceSessions")
            DeleteChildTable("ChildAnnual")
            DeleteChildTable("ChildAttrib")
            DeleteChildTable("ChildCalculation")
            DeleteChildTable("ChildCDAP")
            DeleteChildTable("ChildCharges")
            DeleteChildTable("ChildConsent")
            DeleteChildTable("ChildDeposits")
            DeleteChildTable("ChildHolidays")
            DeleteChildTable("ChildImms")
            DeleteChildTable("ChildMoves")

            Dim _SQL As String = ""

            _SQL = "delete from Bookings where child_id = '" + Me._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from BookingSlots where child_id = '" + Me._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from MedicineAuth where chil_id = '" + Me._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from Children where ID = '" + Me._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Private Sub DeleteChildTable(ByVal TableName As String)
            Dim _SQL As String = "delete from " + TableName + " where child_id = '" + Me._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

        Public Shared Function ReturnDepositBalance(ByVal ChildID As Guid) As Decimal

            Dim _SQL As String = ""

            _SQL += "select isnull(sum(dep_value),0) as 'balance' from ChildDeposits"
            _SQL += " where child_id = '" & ChildID.ToString & "'"

            Return ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        End Function

    End Class

End Namespace