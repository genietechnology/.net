﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class TariffDay

        Public Shared Function RetreiveByTariffID(ByVal TariffID As Guid) As List(Of TariffDay)

            Dim _SQL As String = "select * from TariffDays" &
                                 " where tariff_id = '" & TariffID.ToString & "'"

            Return PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))

        End Function

        Public Shared Function RetrieveByDaysPerWeekAndAge(ByVal TariffID As Guid, ByVal DaysPerWeek As Integer, ByVal AgeInMonths As Long, ByRef ReturnAgeBandDesc As String) As Decimal

            Dim _Field As String = "days_" + DaysPerWeek.ToString

            Dim _SQL As String = "select " + _Field + ", description from TariffDays m" & _
                                 " left join TariffAgeBands a on a.ID = m.age_id" & _
                                 " where tariff_id = '" & TariffID.ToString & "'" & _
                                 " and " & AgeInMonths.ToString & " between a.min_age and a.max_age"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

            If _DR IsNot Nothing Then
                ReturnAgeBandDesc = _DR.Item("description").ToString
                Return ValueHandler.ConvertDecimal(_DR.Item(_Field))
            Else
                Return 0
            End If

        End Function

        Public Shared Function RetrieveByDaysPerWeekAndAgeBand(ByVal TariffID As Guid, ByVal DaysPerWeek As Integer, ByVal AgeBand As Guid, ByRef ReturnAgeBandDesc As String) As Decimal

            Dim _Field As String = "days_" + DaysPerWeek.ToString

            Dim _SQL As String = "select " + _Field + ", description from TariffDays m" & _
                                 " left join TariffAgeBands a on a.ID = m.age_id" & _
                                 " where tariff_id = '" & TariffID.ToString & "'" & _
                                 " and age_id = '" & AgeBand.ToString & "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

            If _DR IsNot Nothing Then
                ReturnAgeBandDesc = _DR.Item("description").ToString
                Return ValueHandler.ConvertDecimal(_DR.Item(_Field))
            Else
                Return 0
            End If

        End Function

    End Class

End Namespace

