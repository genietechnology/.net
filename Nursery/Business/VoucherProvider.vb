﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Namespace Business

    Partial Class VoucherProvider

        Public Shared Function Find(ByVal ParentForm As Form) As String

            Dim _Find As New GenericFind
            With _Find
                .ConnectionString = Session.ConnectionString
                .Caption = "Find Voucher Provider"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .GridSQL = "select name as 'Name', tel as 'Telephone', contact as 'Contact Name' from VoucherProvider"
                .GridOrderBy = "order by name"
                .ReturnField = "ID"
                .Option1Caption = "Name"
                .Option1Field = "name"
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

        Public Shared Sub DrillDown(ByVal ID As Guid)

            Dim _frm As New frmVoucherProvider
            With _frm
                .StartPosition = FormStartPosition.CenterScreen
                .Show()
                .BringToFront()
                .DrillDown(ID)
            End With

        End Sub

    End Class


End Namespace

