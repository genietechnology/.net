﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class InvoiceLine

        Public Shared Function RetreiveAllByInvoiceID(ByVal InvoiceID As Guid) As List(Of InvoiceLine)

            Dim _SQL As String = "select * from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "' order by line_no"

            Dim _InvoiceLineList As List(Of InvoiceLine)
            _InvoiceLineList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _InvoiceLineList

        End Function

        Public Shared Function RetreiveAllByInvoiceAndDateRange(ByVal InvoiceID As Guid, ByVal InvoiceFrom As Date, InvoiceTo As Date) As List(Of InvoiceLine)

            Dim _SQL As String = ""
            _SQL += "select * from InvoiceLines"
            _SQL += " where invoice_id = '" + InvoiceID.ToString + "'"
            _SQL += " and action_date between " + ValueHandler.SQLDate(InvoiceFrom, True) + " and " + ValueHandler.SQLDate(InvoiceTo, True)
            _SQL += " order by line_no"

            Dim _InvoiceLineList As List(Of InvoiceLine)
            _InvoiceLineList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _InvoiceLineList

        End Function

    End Class

End Namespace

