﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared
Imports Care.Data

Namespace Business

    Partial Class Family

        Public ReadOnly Property ChildNames() As String

            Get

                Dim _Count As Integer = 1
                Dim _Return As String = ""

                For Each _C In Business.Child.RetreiveByFamilyID(Me._ID.Value)
                    If _C._Status = "Current" Then
                        If _Count = 1 Then
                            _Return += _C._Fullname
                        Else
                            _Return += vbCrLf + _C._Fullname
                        End If
                        _Count += 1
                    End If
                Next

                Return _Return

            End Get

        End Property

        Public Overrides Sub SetDefaultValues()

            If Me._InvoiceFreq = "" Then Me._InvoiceFreq = ParameterHandler.ReturnString("DEFINVFREQ")
            If Me._InvoiceDue = "" Then Me._InvoiceDue = ParameterHandler.ReturnString("DEFINVDUE")

            If Not Me._SiteId.HasValue Then
                If Me._SiteName = "" Then Me._SiteName = ParameterHandler.ReturnString("DEFSITE")
                Dim _S As Business.Site = Business.Site.RetrieveByName(Me._SiteName)
                If _S IsNot Nothing Then
                    Me._SiteId = _S._ID
                    Me._SiteName = _S._Name
                End If
            End If

            If Me._AccountNo = "" AndAlso Me._SiteId.HasValue Then

                Dim _S As Business.Site = Business.Site.RetreiveByID(Me._SiteId.Value)
                If _S IsNot Nothing Then

                    If _S._AccNext = 0 Then _S._AccNext = 1
                    Dim _No As Integer = _S._AccNext

                    If ParameterHandler.ReturnBoolean("SURNAMEACCOUNTNO") Then
                        Me._AccountNo = _S._AccPrefix.ToString + GenerateSurnameAccount(Me._Surname)
                    Else
                        Me._AccountNo = _S._AccPrefix.ToString + Format(_No, "00000").ToString
                        _S._AccNext += 1
                    End If

                    _S.Store()

                End If

            End If

            Dim contactName As String = ParameterHandler.ReturnString("DEFCONTACT")
            If Not String.IsNullOrWhiteSpace(contactName) Then

                Dim contact As New Contact
                With contact
                    ._FamilyId = Me._ID
                    ._Fullname = contactName
                    ._PrimaryCont = True
                    ._EmerCont = True
                    ._Parent = True
                    ._Collect = True
                    .Store()
                End With
            End If

        End Sub

        Private Function GenerateSurnameAccount(ByVal Surname As String) As String

            If Surname = "" Then Return ""

            'This routine was written for Gainford Group
            'We output an account number that is 5 in length. Sage supports 8 in length as an account number
            'The first three characters are prefixed for the Site, outside of this routine
            'The next three are the Surname, followed by two numbers on the end
            'i.e.
            'SWARIC01

            'This routine is only outputting RIC01 in this example

            Dim _Return As String = ""

            Dim _Sur3 As String = ""
            If Surname.Length >= 3 Then
                _Sur3 = Surname.ToUpper.Substring(0, 3)
            Else
                _Sur3 = Surname.ToUpper
            End If

            Dim _SQL As String = ""

            _SQL += "select top 1 substring(account_no,4,5) as 'account_no' from Family"
            _SQL += " where substring(account_no,4,5) Like '" + _Sur3 + "%'"
            _SQL += " order by substring(account_no,4,5) desc"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR Is Nothing Then
                _Return = _Sur3 + "01"
            Else
                Dim _AccountNo As String = _DR.Item("account_no").ToString
                Dim _NextNo As Integer = CInt(_AccountNo.Substring(3, 2)) + 1
                _Return = _Sur3 + Format(_NextNo, "00")
                Return _Return
            End If

            Return _Return

        End Function

        Public Shared Sub DrillDown(ByVal ID As Guid)

            Dim _frm As New frmFamily
            With _frm
                .StartPosition = FormStartPosition.CenterScreen
                .Show()
                .BringToFront()
                .DrillDown(ID)
            End With

        End Sub

        Public Shared Function FindFamily(ByVal ParentForm As Form, Optional ByVal OmitChildren As Boolean = False) As String

            Dim _SQL As String = "select Family.site_name As 'Site', Family.account_no as 'Account No', Family.surname as 'Family Name', Family.letter_name as 'Addressee', Children.fullname as 'Child'," &
                                 " Contacts.fullname as 'Contact', Contacts.Relationship as 'Relationship', Contacts.email as 'Email'," &
                                 " e_invoicing as 'e-Inv.', archived 'Archived', Family.opening_balance as 'Op. Bal.', Family.balance as 'Balance', Children.invoice_freq as 'Invoice Frequency'" &
                                 " from Family" &
                                 " left join Children on Children.family_id = Family.ID" &
                                 " left join Contacts on Contacts.family_id = Family.ID"

            Dim _Order As String = "order by Family.surname, Children.surname, Children.forename"
            Dim _Where As String = " and (Children.status is null OR Children.status <> '<BLANK>') and Family.site_id not in (select SiteExcludedUsers.site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"

            If OmitChildren Then
                _SQL = "select account_no as 'Account No', surname as Surname, address as 'Address', e_invoicing as 'Email Invoicing' from family"
                _Order = " order by surname"
                _Where = " and site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
            End If

            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Families"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = _SQL
                .GridWhereClause = _Where
                .GridOrderBy = _Order
                .ReturnField = "Family.ID"
                .Option1Caption = "Child"
                .Option1Field = "Children.fullname"
                .Option2Caption = "Address"
                .Option2Field = "address"
                .FormWidth = 1000
                .Show()
                Return .ReturnValue
            End With

        End Function

        Public Shared Function ProcessArchived(ByVal ActionDate As Date) As Integer

            'archive any families where all children left more then x days ago
            Dim _Leavers As Integer = ArchiveLeavers(ActionDate)

            'de-archive families if they have current or waiting list children (happens if they go and come back, or new sibblings etc)
            DeArchiveCurrent()

            Return _Leavers

        End Function

        Private Shared Function ArchiveLeavers(ByVal ActionDate As Date) As Integer

            Dim _i As Integer = 0

            Dim _Days As String = ParameterHandler.ReturnInteger("ARCHIVEDAYS", False).ToString
            If _Days = "" Then _Days = "60"

            Dim _SQL As String = "update Family Set archived = 0 where archived Is null"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = ""
            _SQL += "Select ID,"
            _SQL += " (Select COUNT (*) from Children tc where tc.family_id = f.ID) 'total_children',"
            _SQL += " (select COUNT (*) from Children lc where lc.family_id = f.ID "
            _SQL += " and lc.date_left is not null and DATEDIFF(D, lc.date_left, " + ValueHandler.SQLDate(ActionDate, True) + ") > " + _Days + ") as 'left_x'"
            _SQL += " from Family f"
            _SQL += " where f.archived = 0"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _Total As Integer = ValueHandler.ConvertInteger(_DR.Item("total_children"))
                    Dim _Left As Integer = ValueHandler.ConvertInteger(_DR.Item("left_x"))

                    If _Total = _Left Then

                        Dim _F As Business.Family = Business.Family.RetreiveByID(New Guid(_DR.Item("ID").ToString))
                        If _F IsNot Nothing Then
                            _F._Archived = True
                            _F.Store()
                        End If

                        _i += 1

                    End If

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

            Return _i

        End Function

        Private Shared Sub DeArchiveCurrent()

            Dim _SQL As String = ""
            _SQL += "select c.family_id, f.archived from Children c"
            _SQL += " left join Family f on f.ID = c.family_id"
            _SQL += " where c.status in ('On Waiting List', 'Current')"
            _SQL += " and f.archived = 1"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _F As Business.Family = Business.Family.RetreiveByID(New Guid(_DR.Item("family_id").ToString))
                    If _F IsNot Nothing Then
                        _F._Archived = False
                        _F.Store()
                    End If

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End Sub

        Public ReadOnly Property PrimaryContactWarning As String
            Get
                Return ReturnPrimaryContactStatus()
            End Get
        End Property

        Private Function ReturnPrimaryContactStatus() As String
            If Not _ID.HasValue Then Return ""
            Dim _SQL As String = "select email from Contacts where Family_ID = '" + _ID.Value.ToString + "' and primary_cont = 1"
            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                Dim _Email As String = _DR.Item("email").ToString
                If _Email.Trim = "" Then
                    Return "Primary Contact has no email address!"
                End If
            Else
                Return "No Primary Contact has been setup!"
            End If
            Return ""
        End Function

        Public Shared Function RetrieveCurrentFamilies(ByVal SiteID As Guid) As List(Of Family)

            Dim _SQL As String = "select * from Family where site_id = '" + SiteID.ToString + "' and archived = 0 order by surname"
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _FamilyList

        End Function

        Public Shared Function RetrieveCurrentFamilies() As List(Of Family)

            Dim _SQL As String = "select * from Family where archived = 0 order by surname"
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _FamilyList

        End Function

        Public Shared Function RetrieveFamiliesWithoutAccountNos() As List(Of Family)

            Dim _SQL As String = "select * from Family where archived = 0 and (account_no is null or len(account_no) = 0) order by surname"
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _FamilyList

        End Function

        Public Shared Function RetrieveFamiliesWithOpeningBalances() As List(Of Family)

            Dim _SQL As String = "select * from Family where opening_balance <> 0"
            Dim _FamilyList As List(Of Family)
            _FamilyList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _FamilyList

        End Function

        Public Shared Sub UpdateBalance(ByVal FamilyID As Guid)

            Dim _Balance As Decimal = ReturnInvoices(FamilyID) - ReturnPayments(FamilyID)

            Dim _Family As Business.Family = Business.Family.RetreiveByID(FamilyID)

            _Family._Balance = _Balance
            _Family.Store()

            _Family = Nothing

        End Sub

        Public Shared Function ReturnInvoices(ByVal FamilyID As Guid, Optional ByVal FromDate As String = "", Optional ByVal ToDate As String = "") As Decimal

            Dim _Total As Decimal = 0
            Dim _SQL As String = "select isnull(sum(invoice_total),0) as 'invoice_total'" &
                                 " from Invoices" &
                                 " where family_id = '" & FamilyID.ToString & "'"

            If FromDate <> "" AndAlso ToDate <> "" Then
                _SQL += " and invoice_period between '" & FromDate & "' and '" & ToDate & "'"
            Else
                If FromDate <> "" Then
                    _SQL += " and invoice_period < '" & FromDate & "'"
                End If
            End If

            Dim _dr As SqlClient.SqlDataReader = DAL.GetDataReaderfromSQL(Session.ConnectionString, _SQL)
            If Not _dr Is Nothing Then
                If _dr.HasRows Then
                    _dr.Read()
                    _Total = Decimal.Parse(_dr.Item("invoice_total").ToString)
                End If
                _dr = Nothing
            End If

            Return _Total

        End Function

        Public Shared Function ReturnPayments(ByVal FamilyID As Guid, Optional ByVal FromDate As String = "", Optional ByVal ToDate As String = "") As Decimal

            Dim _Total As Decimal = 0
            Dim _SQL As String = "select isnull(sum(pay_amount),0) as 'pay_amount'" &
                                 " from Payments" &
                                 " where family_id = '" & FamilyID.ToString & "'"

            If FromDate <> "" AndAlso ToDate <> "" Then
                _SQL += " and pay_date between '" & FromDate & "' and '" & ToDate & "'"
            Else
                If FromDate <> "" Then
                    _SQL += " and pay_date < '" & FromDate & "'"
                End If
            End If

            Dim _dr As SqlClient.SqlDataReader = DAL.GetDataReaderfromSQL(Session.ConnectionString, _SQL)
            If Not _dr Is Nothing Then
                If _dr.HasRows Then
                    _dr.Read()
                    _Total = Decimal.Parse(_dr.Item("pay_amount").ToString)
                End If
                _dr = Nothing
            End If

            Return _Total

        End Function

        Public Sub Delete()

            Dim _SQL As String = ""

            _SQL = "delete from Contacts where family_id = '" + Me._ID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            For Each _C As Business.Child In Business.Child.RetreiveByFamilyID(Me._ID.Value)
                _C.Delete()
            Next

            _SQL = "delete from Family where ID = '" + Me._ID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

    End Class

End Namespace
