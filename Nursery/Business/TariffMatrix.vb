﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class TariffMatrix

        Public Shared Function RetreiveByTariffID(ByVal TariffID As Guid) As List(Of TariffMatrix)

            Dim _SQL As String = "select * from TariffMatrix" &
                                 " where tariff_id = '" & TariffID.ToString & "'"

            Return PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))

        End Function

        Public Shared Function RetrieveByClassAndAge(ByVal TariffID As Guid, ByVal Classification As String, ByVal AgeInMonths As Long) As Business.TariffMatrix

            Dim _SQL As String = "select * from TariffMatrix m" &
                                 " left join TariffAgeBands a on a.ID = m.age_id" &
                                 " where tariff_id = '" & TariffID.ToString & "'" &
                                 " and " & AgeInMonths.ToString & " between a.min_age and a.max_age" &
                                 " and m.class = '" & Classification & "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

            If _DR IsNot Nothing Then
                Return PropertiesFromData(_DR)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function RetrieveByClassAndGroup(ByVal TariffID As Guid, ByVal Classification As String, ByVal GroupID As Guid?) As Business.TariffMatrix

            Dim _SQL As String = "select * from TariffMatrix" &
                                 " where tariff_id = '" & TariffID.ToString & "'" &
                                 " and age_id = '" & GroupID.Value.ToString & "'" &
                                 " and class = '" & Classification & "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                Return PropertiesFromData(_DR)
            Else
                Return Nothing
            End If

        End Function

    End Class

End Namespace

