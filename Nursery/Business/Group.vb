﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class Group

        Public Sub UpdateGroupNames()

            Dim _SQL As String = ""

            _SQL = "update children" & _
                   " set group_name = '" & Me._Name & "'" & _
                   " where group_id = '" & Me._ID.ToString & "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "update staff" & _
                   " set group_name = '" & Me._Name & "'" & _
                   " where group_id = '" & Me._ID.ToString & "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Public Shared Function RetreiveAllByMinimumAge() As List(Of Group)

            Dim _GroupList As List(Of Group)
            _GroupList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Groups order by age_min"))
            Return _GroupList

        End Function

    End Class

End Namespace
