﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class Booking

        Public Shared Function RetreiveByChildID(ByVal ChildID As Guid) As List(Of Business.Booking)

            Dim _SQL As String = "select * from Bookings" & _
                                 " where child_id = '" & ChildID.ToString & "'"

            Dim _BookingList As List(Of Business.Booking)
            _BookingList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _BookingList

        End Function

        Public Shared Function RetreiveBySiteFromDate(ByVal SiteID As Guid, ByVal FromDate As Date) As List(Of Business.Booking)

            Dim _SQL As String = ""

            _SQL += "select * from Bookings"
            _SQL += " where site_id = '" + SiteID.ToString + "'"
            _SQL += " and booking_date >= " + ValueHandler.SQLDate(FromDate, True)

            Dim _BookingList As List(Of Business.Booking)
            _BookingList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _BookingList

        End Function

        Public Shared Function RetreiveBySiteDateRange(ByVal SiteID As Guid, ByVal FromDate As Date, ByVal ToDate As Date) As List(Of Business.Booking)

            Dim _SQL As String = ""

            _SQL += "select * from Bookings"
            _SQL += " where site_id = '" + SiteID.ToString + "'"
            _SQL += " and booking_date between " + ValueHandler.SQLDate(FromDate, True)
            _SQL += " and " + ValueHandler.SQLDate(ToDate, True)

            Dim _BookingList As List(Of Business.Booking)
            _BookingList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _BookingList

        End Function

    End Class

End Namespace
