﻿

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class RoomRatios

        Inherits DataObjectBase

        Public Property SiteID As Guid?
        Public Property RoomID As Guid?
        Public Property RoomName As String
        Public Property HoursFrom As TimeSpan?
        Public Property HoursTo As TimeSpan?
        Public Property ClosedFrom As Date?
        Public Property ClosedTo As Date?
        Public Property RatioID As Guid?
        Public Property AgeFrom As Integer
        Public Property AgeTo As Integer
        Public Property Capacity As Integer
        Public Property Ratio As Integer
        Public Property Sequence As Integer

        Public Shared Function RetreiveRoomNameFromRatioID(ByVal RatioID As Guid?) As String

            If RatioID Is Nothing Then Return ""
            If Not RatioID.HasValue Then Return ""

            Dim _Return As String = ""

            Dim _SQL As String = ""
            _SQL += "select room_name from SiteRoomRatios rr"
            _SQL += " left join SiteRooms r on r.ID = rr.room_id"
            _SQL += " where rr.ID = '" + RatioID.ToString + "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("room_name").ToString
            End If

            Return _Return

        End Function

        Public Shared Function RetreiveSiteRoomSQL(ByVal SiteID As String) As String

            If SiteID = "" Then Return ""

            Dim _SQL As String = ""
            _SQL += "select ID, room_name from SiteRooms"
            _SQL += " where site_id = '" + SiteID + "'"
            _SQL += " order by room_sequence"

            Return _SQL

        End Function

        Public Shared Function RetreiveSiteRoomRatioSQL(ByVal SiteID As String) As String

            If SiteID = "" Then Return ""

            Dim _SQL As String = ""

            _SQL += "select rr.ID,"
            _SQL += " room_name + ' (' + cast(age_from as varchar(3)) + '-' + cast(age_to as varchar(3)) + ' months)' as 'desc'"
            _SQL += " from SiteRoomRatios rr"
            _SQL += " left join SiteRooms r on r.ID = rr.room_id"
            _SQL += " where r.site_id = '" + SiteID + "'"
            _SQL += " order by r.room_sequence, rr.age_from"

            Return _SQL

        End Function

        Public Shared Function RetreiveBySite(ByVal SiteID As Guid) As List(Of RoomRatios)

            Dim _SQL As String = ""
            _SQL += "select r.site_id, r.ID as 'room_id', r.room_name, r.room_hours_from, r.room_hours_to,"
            _SQL += " r.room_close_from, r.room_close_to,"
            _SQL += " rr.ID as 'ratio_id', rr.age_from, rr.age_to, rr.capacity, rr.ratio, r.room_sequence"
            _SQL += " from SiteRooms r"
            _SQL += " inner join SiteRoomRatios rr on r.ID = rr.room_id"
            _SQL += " where r.room_check_children = 1"
            _SQL += " and r.site_id = '" + SiteID.ToString + "'"
            _SQL += " order by r.site_id, rr.age_from, r.room_sequence"

            Dim _Ratios As List(Of RoomRatios)
            _Ratios = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _Ratios

        End Function

        Public Shared Function RetreiveAll() As List(Of RoomRatios)

            Dim _SQL As String = ""
            _SQL += "select r.site_id, r.ID as 'room_id', r.room_name, r.room_hours_from, r.room_hours_to,"
            _SQL += " r.room_close_from, r.room_close_to,"
            _SQL += " rr.ID as 'ratio_id', rr.age_from, rr.age_to, rr.capacity, rr.ratio, r.room_sequence"
            _SQL += " from SiteRooms r"
            _SQL += " inner join SiteRoomRatios rr on r.ID = rr.room_id"
            _SQL += " where r.room_check_children = 1"
            _SQL += " order by r.site_id, rr.age_from, r.room_sequence"

            Dim _Ratios As List(Of RoomRatios)
            _Ratios = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _Ratios

        End Function

        Public Shared Function RetreiveByAge() As List(Of RoomRatios)

            Dim _SQL As String = ""
            _SQL += "select r.site_id, r.ID as 'room_id', r.room_name, r.room_hours_from, r.room_hours_to,"
            _SQL += " r.room_close_from, r.room_close_to,"
            _SQL += " rr.ID as 'ratio_id', rr.age_from, rr.age_to, rr.capacity, rr.ratio, r.room_sequence"
            _SQL += " from SiteRooms r"
            _SQL += " inner join SiteRoomRatios rr on r.ID = rr.room_id"
            _SQL += " where r.room_check_children = 1"
            _SQL += " order by r.site_id, rr.age_from, r.room_sequence"

            Dim _Ratios As List(Of RoomRatios)
            _Ratios = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _Ratios

        End Function

        Public Shared Function RetreiveByID(ratioID As Guid) As RoomRatios

            Dim sql As String = ""
            sql = String.Concat("select r.site_id, r.ID as 'room_id', r.room_name, r.room_hours_from, r.room_hours_to,",
                                " r.room_close_from, r.room_close_to,",
                                " rr.ID as 'ratio_id', rr.age_from, rr.age_to, rr.capacity, rr.ratio, r.room_sequence",
                                " from SiteRooms r",
                                " inner join SiteRoomRatios rr on r.ID = rr.room_id",
                                " where r.room_check_children = 1",
                                " and rr.ID = '", ratioID, "'",
                                " order by r.site_id, rr.age_from, r.room_sequence")

            Dim table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)
            If table.Rows.Count > 0 Then
                Return PropertiesFromData(table.Rows(0))
            Else
                Return Nothing
            End If

        End Function

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As RoomRatios

            Dim _C As RoomRatios = Nothing

            If DR IsNot Nothing Then
                _C = New RoomRatios()
                With DR
                    _C.SiteID = GetGUID(DR("site_id"))
                    _C.RoomID = GetGUID(DR("room_id"))
                    _C.RoomName = DR("room_name").ToString.Trim
                    _C.HoursFrom = GetTimeSpan(DR("room_hours_from"))
                    _C.HoursTo = GetTimeSpan(DR("room_hours_to"))
                    _C.ClosedFrom = GetDate(DR("room_close_from"))
                    _C.ClosedTo = GetDate(DR("room_close_to"))
                    _C.RatioID = GetGUID(DR("ratio_id"))
                    _C.AgeFrom = GetInteger(DR("age_from"))
                    _C.AgeTo = GetInteger(DR("age_to"))
                    _C.Capacity = GetInteger(DR("capacity"))
                    _C.Ratio = GetInteger(DR("ratio"))
                    _C.Sequence = GetInteger(DR("room_sequence"))
                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of RoomRatios)

            Dim _Ratios As New List(Of RoomRatios)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _Ratios.Add(PropertiesFromData(_DR))
            Next

            Return _Ratios

        End Function

    End Class

End Namespace
