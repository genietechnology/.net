﻿Imports Care.Data
Imports Care.Global
Imports Care.Shared

Namespace Business

    Public Class InvoicingEngine

        Private m_BatchID As Guid? = Nothing

        Private m_InvoiceNo As Integer = 0
        Private m_InvoiceDate As Date?

        Private m_FirstDate As Date?
        Private m_LastDate As Date?

        Private m_Classification As String = ""
        Private m_Frequency As String = ""

        Private m_GroupName As String = ""
        Private m_LayoutName As String = ""

        Private m_XCheck As Invoicing.EnumXCheckMode = Invoicing.EnumXCheckMode.None
        Private m_XCheckFrom As Date? = Nothing
        Private m_XCheckTo As Date? = Nothing
        Private m_XCheckThresh As Decimal = 0
        Private m_XCheckPerHour As Decimal = 0
        Private m_XCheckAdditional As Boolean = False

        Private m_SiteID As Guid = Nothing
        Private m_SiteName As String = ""
        Private m_AddtionalSessionMode As Invoicing.EnumAdditionalSessionMode
        Private m_DuplicateMode As Invoicing.EnumDuplicateMode
        Private m_TariffFilter As String = ""

        Private m_PaymentMethod As String = ""
        Private m_FundingType As String = ""
        Private m_Tags As String = ""
        Private m_BatchPeriod As String = ""

        Private m_AddlFrom As Date? = Nothing
        Private m_AddlTo As Date? = Nothing
        Private m_GenerationMode As Invoicing.EnumGenerationMode

        Public Sub New()

        End Sub

#Region "Properties"

        Public ReadOnly Property BatchID As Guid?
            Get
                Return m_BatchID
            End Get
        End Property

#End Region

        Public Function CheckforInvoices() As Boolean

            Dim _Found As Boolean = False

            ''check if any invoices have been produced for the specified period
            'Dim _SQL As String = "select top 1 * from InvoiceLines" & _
            '                     " where MONTH(action_date) = " & m_InvoiceMonth.ToString & _
            '                     " and YEAR(action_date) = " & m_InvoiceYear.ToString

            'Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            'If _dt IsNot Nothing Then
            '    If _dt.Rows.Count > 0 Then
            '        _Found = True
            '    End If
            '    _dt.Dispose()
            '    _dt = Nothing
            'End If

            Return _Found

        End Function

        Public Function Run(ByVal BatchID As Guid) As Boolean

            Dim _BatchNo As Integer = 0

            Dim _Header As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(BatchID)
            If _Header IsNot Nothing Then

                With _Header

                    'save batch no for use later
                    _BatchNo = ValueHandler.ConvertInteger(._BatchNo)

                    m_BatchID = BatchID
                    m_BatchPeriod = ._BatchPeriod

                    m_InvoiceNo = Invoicing.LastInvoiceNo 'we dont increment the last invoice number here, as its done in GenerateInvoice

                    m_FirstDate = ._DateFirst
                    m_LastDate = ._DateLast
                    m_InvoiceDate = ._BatchDate

                    m_Classification = ._BatchClass
                    m_Frequency = ._BatchFreq

                    m_GroupName = ._BatchGroup
                    m_LayoutName = ._BatchLayout

                    m_XCheck = CType(._Xcheck, Invoicing.EnumXCheckMode)
                    m_XCheckFrom = ._XcheckFrom
                    m_XCheckTo = ._XcheckTo
                    m_XCheckThresh = ._XcheckThresh
                    m_XCheckPerHour = ._XcheckPerHour
                    m_XCheckAdditional = ._XcheckAdditional

                    m_SiteID = ._SiteId.Value
                    m_SiteName = ._SiteName
                    m_AddtionalSessionMode = CType(._AdditionalSessions, Invoicing.EnumAdditionalSessionMode)
                    m_DuplicateMode = CType(._DuplicateMode, Invoicing.EnumDuplicateMode)
                    m_TariffFilter = ._TariffFilter

                    m_PaymentMethod = ._PaymentMethod
                    m_FundingType = ._FundingType
                    m_Tags = ._TagFilter

                    m_AddlFrom = ._AdditionalFrom
                    m_AddlTo = ._AdditionalTo
                    m_GenerationMode = CType(._GenerationMode, Invoicing.EnumGenerationMode)

                End With

                Select Case _Header._BatchPeriod

                    Case "Empty Batch"
                        'do nothing

                    Case "Opening Balances"

                        'fetch all the families
                        Dim _Families As List(Of Business.Family) = Business.Family.RetrieveFamiliesWithOpeningBalances

                        Session.SetupProgressBar("Generating Invoices...", _Families.Count)

                        For Each _Family As Business.Family In _Families
                            If Not _Family._ExclInvoicing Then
                                Invoicing.GenerateOneLineInvoice(m_BatchID.Value, _BatchNo, _Family._ID.Value, Nothing, m_InvoiceDate.Value, Nothing, Nothing, m_LayoutName, "Opening Balance", _Family._OpeningBalance, m_InvoiceNo)
                            End If
                            Session.StepProgressBar()
                        Next

                        Session.HideProgressBar()
                        _Families = Nothing


                    Case "Annual Recalculation"

                        Dim _Annuals As List(Of Business.ChildAnnual) = Business.ChildAnnual.RetreiveAll

                        Session.SetupProgressBar("Generating Invoices...", _Annuals.Count)

                        For Each _A As Business.ChildAnnual In _Annuals
                            If _A._Active = True Then
                                Dim _InvoiceNo As Integer = 0
                                _A.Delete()
                                _A.Recalculate(m_BatchID.Value, _InvoiceNo)
                            End If
                            Session.StepProgressBar()
                        Next

                        Session.HideProgressBar()
                        _Annuals = Nothing

                    Case Else

                        'fetch all the families
                        Dim _Families As List(Of Business.Family) = Business.Family.RetreiveAll

                        Session.SetupProgressBar("Generating Invoices...", _Families.Count)

                        For Each _Family As Business.Family In _Families

                            'check for exclusions
                            If Not _Family._ExclInvoicing Then
                                ProcessFamily(_BatchNo, _Family)
                            End If

                            Session.StepProgressBar()

                        Next

                        Session.HideProgressBar()
                        _Families = Nothing

                End Select

                'update the last invoice number
                _Header._InvLast = m_InvoiceNo
                _Header.Store()

                Return True

            Else
                Return False
            End If

        End Function

        Private Sub ProcessFamily(ByVal BatchNo As Long, ByVal Family As Business.Family)

            Dim _Children As New List(Of Business.Child)
            Dim _Forecasting As Boolean = False

            If m_BatchPeriod = "Forecasting" Then _Forecasting = True

            'add the current children into the list of children
            _Children.AddRange(Business.Child.RetrieveLiveChildren(Family._ID.Value))

            'fetch leavers this period
            _Children.AddRange(Business.Child.RetrieveLeavers(Family._ID.Value, m_FirstDate.Value, m_LastDate.Value))

            'fetch pending starters
            _Children.AddRange(Business.Child.RetrieveStarters(Family._ID.Value, m_LastDate.Value))

            For Each _Child As Business.Child In _Children

                If FilterCheck(_Child) Then

                    Invoicing.GenerateInvoice(m_BatchID.Value, BatchNo, _Child._ID.Value, m_InvoiceDate.Value, m_FirstDate.Value, m_LastDate.Value, m_InvoiceNo, False, m_LayoutName,
                                                  m_XCheck, m_XCheckFrom, m_XCheckTo, m_XCheckThresh, m_XCheckPerHour, m_XCheckAdditional,
                                                  m_AddtionalSessionMode, m_DuplicateMode, m_TariffFilter, m_GenerationMode, m_AddlFrom, m_AddlTo, _Forecasting)

                End If

            Next

            _Children = Nothing

        End Sub

        Private Function FilterCheck(ByRef Child As Business.Child) As Boolean

            'forecasting
            '*******************************************************************************************************************
            If m_BatchPeriod = "Forecasting" Then
                'filter by site
                If Child._SiteId.HasValue Then
                    If Child._SiteId.Value <> m_SiteID Then Return False
                End If
                Return True
            End If

            '*******************************************************************************************************************
            'standard filters
            '*******************************************************************************************************************

            If m_Classification <> "" AndAlso Child._Class <> m_Classification Then Return False

            If Child._SiteId.HasValue Then
                If Child._SiteId.Value <> m_SiteID Then Return False
            Else
                Return False
            End If

            If m_Frequency <> "" AndAlso Child._InvoiceFreq <> m_Frequency Then Return False

            'optional filters
            '*******************************************************************************************************************

            If m_PaymentMethod <> "" AndAlso Child._PaymentMethod <> m_PaymentMethod Then Return False
            If m_GroupName <> "" AndAlso Child._GroupName <> m_GroupName Then Return False

            'tariffs are passed through and processed in Invoicing.GenerateInvoice

            If Not ProcessCheckedList(m_FundingType, Child._FundingType) Then Return False
            If Not ProcessCheckedList(m_Tags, Child._Tags) Then Return False

            '*******************************************************************************************************************

            Return True

        End Function

        Private Function ProcessCheckedList(ByVal ListItems As String, ByVal FieldToCheck As String) As Boolean

            If ListItems <> "" Then

                If FieldToCheck = "" Then
                    Return False
                Else

                    Dim _OK As Boolean = True

                    For Each _Item In m_Tags.Split(CType(",", Char())).ToList
                        _Item = _Item.Trim.ToUpper
                        If Not TagPresent(FieldToCheck, _Item) Then
                            _OK = False
                            Exit For
                        End If
                    Next

                    Return _OK

                End If

            Else
                'no items in filter
                Return True
            End If

        End Function

        Private Function TagPresent(ByVal FieldToCheck As String, ByVal ItemToFind As String) As Boolean
            For Each _Item In FieldToCheck.Split(CType(",", Char())).ToList
                _Item = _Item.Trim.ToUpper
                If _Item = ItemToFind Then
                    Return True
                End If
            Next
            Return False
        End Function

#Region "Public Calculations"

        Public Shared Sub UpdateCurrentStatement(ByVal FamilyID As Guid)

            'get current statement period
            Dim _Statement As Business.FamilyStatement = Business.FamilyStatement.RetreiveCurrentStatement(FamilyID)
            If _Statement IsNot Nothing Then

                'set period
                Dim _FirstDate As Date = DateSerial(Year(_Statement._StatementDate.Value), Month(_Statement._StatementDate.Value), 1)
                Dim _LastDate As Date = DateSerial(Year(_Statement._StatementDate.Value), Month(_Statement._StatementDate.Value), Date.DaysInMonth(Year(_Statement._StatementDate.Value), Month(_Statement._StatementDate.Value)))

                'get payments and charges from invoice period
                Dim _Charges As Decimal = Business.Family.ReturnInvoices(FamilyID, Format(_FirstDate, "yyyy-MM-dd"), Format(_LastDate, "yyyy-MM-dd"))
                Dim _Payments As Decimal = Business.Family.ReturnPayments(FamilyID, Format(_FirstDate, "yyyy-MM-dd"), Format(_LastDate, "yyyy-MM-dd"))

                With _Statement
                    ._Charges = _Charges
                    ._Payments = _Payments
                    ._Balance = (._Bfwd + ._Charges) - ._Payments
                End With

                Business.FamilyStatement.SaveFamilyStatement(_Statement)

                Business.Family.UpdateBalance(FamilyID)

                _Statement = Nothing

            End If


        End Sub

        Public Shared Sub CreateStatement(ByVal FamilyID As Guid, ByVal InvoiceMonth As Integer, InvoiceYear As Integer)

            'set period from transaction date
            Dim _FirstDate As Date = DateSerial(InvoiceYear, InvoiceMonth, 1)
            Dim _LastDate As Date = DateSerial(InvoiceYear, InvoiceMonth, Date.DaysInMonth(InvoiceYear, InvoiceMonth))

            'get payments and charges to date
            Dim _BwfdCharges = Business.Family.ReturnInvoices(FamilyID, Format(_FirstDate, "yyyy-MM-dd"))
            Dim _BwfdPayments = Business.Family.ReturnPayments(FamilyID, Format(_FirstDate, "yyyy-MM-dd"))

            'get payments and charges from invoice period
            Dim _Charges As Decimal = Business.Family.ReturnInvoices(FamilyID, Format(_FirstDate, "yyyy-MM-dd"), Format(_LastDate, "yyyy-MM-dd"))
            Dim _Payments As Decimal = Business.Family.ReturnPayments(FamilyID, Format(_FirstDate, "yyyy-MM-dd"), Format(_LastDate, "yyyy-MM-dd"))

            'get family name
            Dim _FamilyName As String = ""
            Dim _Family As Business.Family = Business.Family.RetreiveByID(FamilyID)
            If _Family IsNot Nothing Then _FamilyName = _Family._Surname
            _Family = Nothing

            'fetch statement if one exists
            Dim _Statement As New Business.FamilyStatement

            With _Statement
                ._FamilyId = FamilyID
                ._FamilyName = _FamilyName
                ._StatementDate = _LastDate
                ._Bfwd = _BwfdCharges - _BwfdPayments
                ._Charges = _Charges
                ._Payments = _Payments
                ._Balance = (._Bfwd + ._Charges) - ._Payments
            End With

            Business.FamilyStatement.SaveFamilyStatement(_Statement)
            _Statement = Nothing

            Business.Family.UpdateBalance(FamilyID)

        End Sub

#End Region

    End Class

End Namespace

