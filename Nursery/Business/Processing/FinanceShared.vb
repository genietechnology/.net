﻿Imports Care.Global
Imports Care.Shared

Public Class FinanceShared

    Private Shared m_DefaultNLCode As String = ""
    Private Shared m_DefaultNLTracking As String = ""
    Private Shared m_NLTrackingCategory As String = ""
    Private Shared m_Initialised As Boolean = False

    Public Shared ReadOnly Property DefaultNLCode() As String
        Get
            If Not m_Initialised Then FetchParameters()
            Return m_DefaultNLCode
        End Get
    End Property

    Public Shared ReadOnly Property DefaultNLTracking() As String
        Get
            If Not m_Initialised Then FetchParameters()
            Return m_DefaultNLTracking
        End Get
    End Property

    Public Shared ReadOnly Property TrackingCategory() As String
        Get
            If Not m_Initialised Then FetchParameters()
            Return m_NLTrackingCategory
        End Get
    End Property

    Private Shared Sub FetchParameters()
        m_DefaultNLCode = ParameterHandler.ReturnString("NLCODE")
        m_DefaultNLTracking = ParameterHandler.ReturnString("NLTRACKING")
        m_NLTrackingCategory = ParameterHandler.ReturnString("NLTRACKINGCAT")
        m_Initialised = True
    End Sub

    Public Shared Function ReturnNLCode(ByVal Child As Business.Child, ByVal ItemNLCode As String) As String

        'check if the NL code is set against the child
        If Child IsNot Nothing Then
            If Child._NlCode <> "" Then Return Child._NlCode
        End If

        'now check the tariff or bolt-on (passed in)
        If ItemNLCode <> "" Then Return ItemNLCode

        If Child IsNot Nothing Then

            'now check the site
            If Child._SiteId.HasValue Then
                Dim _S As Business.Site = Business.Site.RetreiveByID(Child._SiteId.Value)
                If _S._NlCode <> "" Then
                    Return _S._NlCode
                End If
            End If

            'now check the classification
            If ParameterHandler.ReturnBoolean("USECLASS") Then
                If Child._Class <> "" Then
                    Dim _i As ListHandler.ListItem = ListHandler.ReturnItem("Classifications", Child._Class)
                    If _i IsNot Nothing Then
                        If _i.Ref1 <> "" Then
                            Return _i.Ref1
                        End If
                    End If
                End If
            End If

        End If

        'finally, check the parameters
        If DefaultNLCode <> "" Then
            Return DefaultNLCode
        End If

        'if we get here - we have a problem!
        Return "*INVALID*"

    End Function

    Public Shared Function ReturnNLTracking(ByVal Child As Business.Child, ByVal ItemNLTracking As String) As String

        'if we have no tracking category defined, we are not using tracking
        If TrackingCategory = "" Then Return ""

        'check if the NL tracking is set against the child
        If Child IsNot Nothing Then
            If Child._NlTracking <> "" Then Return Child._NlTracking
        End If

        'now check the tariff or bolt-on (passed in)
        If ItemNLTracking <> "" Then Return ItemNLTracking

        If Child IsNot Nothing Then

            'now check the site
            If Child._SiteId.HasValue Then
                Dim _S As Business.Site = Business.Site.RetreiveByID(Child._SiteId.Value)
                If _S._NlTracking <> "" Then
                    Return _S._NlTracking
                End If
            End If

            'now check the classification
            If ParameterHandler.ReturnBoolean("USECLASS") Then
                If Child._Class <> "" Then
                    Dim _i As ListHandler.ListItem = ListHandler.ReturnItem("Classifications", Child._Class)
                    If _i IsNot Nothing Then
                        If _i.Ref2 <> "" Then
                            Return _i.Ref2
                        End If
                    End If
                End If
            End If

        End If

        'finally, check the parameters
        If m_DefaultNLTracking <> "" Then
            Return m_DefaultNLTracking
        End If

        'if we get here - we have a problem!
        Return "*INVALID*"

    End Function

    Public Shared Function ReturnFinancialSystem(ByVal SiteID As Guid?) As String
        Dim _System As String = ""
        Dim _i As Care.Financials.IntegrationDetail = ReturnFinancialsIntegration(SiteID)
        If _i IsNot Nothing Then
            _System = _i.Partner.ToString.ToUpper()
        End If
        Return _System
    End Function

    Public Shared Function ReturnFinancialsIntegration(ByVal SiteID As Guid?) As Care.Financials.IntegrationDetail

        If Not Session.FinancialsIntegrated Then
            Return Nothing
        End If

        Dim _FID As Care.Financials.IntegrationDetail = Nothing
        Dim _Populated As Boolean = False
        Dim _FinancialSystem As String = ""
        Dim _APIKey As String = ""
        Dim _APISecret As String = ""
        Dim _PFXPath As String = ""
        Dim _PFXPassword As String = ""
        Dim _SageCompanyPath As String = ""
        Dim _SageUser As String = ""
        Dim _SagePassword As String = ""
        Dim _SageVersion As String = ""

        'obtain site-level financials integration details
        If SiteID.HasValue Then
            Dim _S As Business.Site = Business.Site.RetreiveByID(SiteID.Value)
            If _S._FinOverride Then
                _Populated = SetFromSite(_S, _FinancialSystem, _APIKey, _APISecret, _PFXPath, _PFXPassword, _SageCompanyPath, _SageUser, _SagePassword, _SageVersion)
            End If
        End If

        'no integration details set, so we get the defaults from parameters
        If Not _Populated Then
            _Populated = SetFromParameters(_FinancialSystem, _APIKey, _APISecret, _PFXPath, _PFXPassword, _SageCompanyPath, _SageUser, _SagePassword, _SageVersion)
        End If

        'still not populated, then we have a problem...
        If Not _Populated Then
            Return Nothing
        End If

        Select Case _FinancialSystem.ToUpper

            Case "CLEARBOOKS"

                If _APIKey = "" Then Return Nothing
                If _APISecret = "" Then Return Nothing

                _FID = New Care.Financials.IntegrationDetail
                With _FID
                    .Partner = Care.Financials.IntegrationDetail.EnumIntegrationPartner.ClearBooks
                    .APIKey = _APIKey
                    .APISecret = _APISecret
                End With

            Case "XERO"

                If _APIKey = "" Then Return Nothing
                If _APISecret = "" Then Return Nothing
                If _PFXPath = "" Then Return Nothing
                If _PFXPassword = "" Then Return Nothing

                If IO.File.Exists(Session.FinancialsPFXPath) Then

                    _FID = New Care.Financials.IntegrationDetail
                    With _FID
                        .Partner = Care.Financials.IntegrationDetail.EnumIntegrationPartner.Xero
                        .APIKey = _APIKey
                        .APISecret = _APISecret
                        .PFXPath = _PFXPath
                        .PFXPassword = _PFXPassword
                    End With

                    Dim _TrackingCat As String = ParameterHandler.ReturnString("NLTRACKINGCAT")
                    If _TrackingCat <> "" Then
                        Dim _Options As List(Of Care.Financials.TrackingOption) = Care.Financials.XeroEngine.PopulateTracking(_FID, _TrackingCat)
                        _FID.TrackingOptions = _Options
                    End If

                Else
                    CareMessage("Unable to access the specified PFX File (" + Session.FinancialsPFXPath + ")", MessageBoxIcon.Exclamation, "PFX File")
                End If


            Case "SAGE"

                If _SageCompanyPath = "" Then Return Nothing
                If _SageUser = "" Then Return Nothing
                If _SagePassword = "" Then Return Nothing

                If IO.Directory.Exists(Session.FinancialsSagePath) Then

                    _FID = New Care.Financials.IntegrationDetail
                    With _FID
                        .Partner = Care.Financials.IntegrationDetail.EnumIntegrationPartner.Sage
                        .SageCompanyPath = _SageCompanyPath
                        .SageUser = _SageUser
                        .SagePassword = _SagePassword
                        .SageVersion = _SageVersion
                    End With

                Else
                    CareMessage("Unable to access the specified Sage Company Path (" + Session.FinancialsSagePath + ")", MessageBoxIcon.Exclamation, "Sage Company Path")
                End If

        End Select

        Return _FID

    End Function

    Private Shared Function SetFromParameters(ByRef FinancialSystem As String, ByRef APIKey As String, ByRef APISecret As String, ByRef PFXPath As String, ByRef PFXPassword As String,
                                       ByRef SagePath As String, ByRef SageUser As String, ByRef SagePassword As String, ByRef SageVersion As String) As Boolean

        FinancialSystem = Session.FinancialSystem
        APIKey = Session.FinancialsAPIKey
        APISecret = Session.FinancialsAPISecret
        PFXPath = Session.FinancialsPFXPath
        PFXPassword = Session.FinancialsPFXPassword
        SagePath = Session.FinancialsSagePath
        SageUser = Session.FinancialsSageUser
        SagePassword = Session.FinancialsSagePassword
        SageVersion = Session.FinancialsSageVersion

        Return True

    End Function

    Private Shared Function SetFromSite(ByVal Site As Business.Site, ByRef FinancialSystem As String, ByRef APIKey As String, ByRef APISecret As String, ByRef PFXPath As String, ByRef PFXPassword As String,
                                       ByRef SagePath As String, ByRef SageUser As String, ByRef SagePassword As String, ByRef SageVersion As String) As Boolean

        If Site IsNot Nothing Then

            FinancialSystem = Site._FinSystem
            APIKey = Site._FinApiKey
            APISecret = Site._FinApiSecret
            PFXPath = Site._FinPfxPath
            PFXPassword = EncyptionHandler.DecryptString(Site._FinPfxPwd)
            SagePath = Site._FinSagePath
            SageUser = Site._FinSageUser
            SagePassword = EncyptionHandler.DecryptString(Site._FinSagePwd)
            SageVersion = Site._FinSageVer

            Return True

        Else
            Return False
        End If

    End Function

End Class
