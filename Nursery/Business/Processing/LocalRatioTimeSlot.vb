﻿Public Class LocalRatioTimeSlot

    Public Sub New(ByVal RatioIDIn As Guid?, ByVal TimeSlotIDIn As Guid?)
        RatioID = RatioIDIn
        TimeSlotID = TimeSlotIDIn
        HeadCount = 1
    End Sub

    Public Property RatioID As Guid?
    Public Property TimeSlotID As Guid?
    Public Property HeadCount As Integer

End Class