﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class BookingsEngine

    'cached records to save trips to the db
    Private m_RoomRatios As New List(Of Business.RoomRatios)
    Private m_Tariffs As New List(Of Business.Tariff)
    Private m_TimeSlots As IOrderedEnumerable(Of Business.TimeSlot) = Nothing
    Private m_BoltOns As New List(Of Business.TariffBoltOn)

    'records we are generating...
    Private m_DTBookings As DataTable = Nothing
    Private m_DTBookingSlots As DataTable = Nothing
    Private m_Errors As String = ""

    'current child record
    Private m_Child As Business.Child = Nothing

    Private m_LocalBookings As New List(Of LocalBooking)

    Private m_FromDate As Date?
    Private m_CutOff As Date?

    Private m_FullTimeHours As Decimal = 10
    Private m_TimeEarly As TimeSpan
    Private m_TimeLate As TimeSpan

    Private m_SchoolLeaveMonths As Integer? = Nothing
    Private m_Points As New List(Of Business.AgePoint)

    Private m_HashDates As New Hashtable
    Private m_CheckSpace As Boolean = False

    Public ReadOnly Property SchoolLeaveMonths As Integer
        Get
            If Not m_SchoolLeaveMonths.HasValue Then
                m_SchoolLeaveMonths = ParameterHandler.ReturnInteger("SCHOOLMONTHS", False)
            End If
            Return m_SchoolLeaveMonths.Value
        End Get
    End Property

    Private Enum EnumStartOrEnd
        StartTime
        EndTime
    End Enum

    Private Enum EnumPaidMeal
        Breakfast
        Lunch
        Tea
        Snack
    End Enum

    Private Enum EnumRatioStatus As Integer
        OK = 0
        ManualOverride = 1
        RoomsFull = -1
        NoRooms = -2
        RoomError = -99
    End Enum

    Public Sub New()

        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

    End Sub

    Private Function SetupData() As Boolean

        If Not m_FromDate.HasValue Then Return False
        If Not m_FromDate.HasValue Then Return False

        m_RoomRatios = Business.RoomRatios.RetreiveByAge
        m_Tariffs = Business.Tariff.RetreiveAll
        m_Points = Business.AgePoint.RetreiveAll()
        m_BoltOns = Business.TariffBoltOn.RetreiveAll

        Dim _Slots As List(Of Business.TimeSlot) = Business.TimeSlot.RetreiveAll
        If _Slots IsNot Nothing Then
            m_TimeSlots = From _T As Business.TimeSlot In Business.TimeSlot.RetreiveAll Order By _T._StartTime
            m_TimeEarly = m_TimeSlots.First._StartTime.Value
            m_TimeLate = m_TimeSlots.Last._EndTime.Value
        End If

        m_CheckSpace = ParameterHandler.ReturnBoolean("CHECKSPACE")

        Return True

    End Function

    Public Sub BuildBookings(ByVal Child As Guid, ByVal FromDate As Date?, ByVal Cutoff As Date?)

        m_FromDate = FromDate
        m_CutOff = Cutoff

        If Not SetupData() Then Exit Sub

        DAL.ExecuteSQL(Session.ConnectionString, "delete from Bookings where child_id = '" + Child.ToString + "'")
        DAL.ExecuteSQL(Session.ConnectionString, "delete from BookingSlots where child_id = '" + Child.ToString + "'")

        m_DTBookings = DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Bookings where child_id = '" + Child.ToString + "'")
        m_DTBookingSlots = DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from BookingSlots where child_id = '" + Child.ToString + "'")

        m_Child = Business.Child.RetreiveByID(Child)

        DoBuild()

        DAL.BulkCopy(Session.ConnectionString, "Bookings", m_DTBookings)
        DAL.BulkCopy(Session.ConnectionString, "BookingSlots", m_DTBookingSlots)

    End Sub

    ''' <summary>
    ''' This routine is also called from the Task Manager, so we don't want to have to pass arguments
    ''' </summary>
    ''' <returns>
    ''' 0 = Successful
    ''' Negative Value on Error
    ''' </returns>
    Public Function BuildBookingsForEveryone() As Integer

        Try

            m_FromDate = Today
            m_CutOff = Bookings.CalendarCutOff

            If Not SetupData() Then Return -1

            m_Errors = ""

            DAL.ExecuteSQL(Session.ConnectionString, "delete from Bookings")
            DAL.ExecuteSQL(Session.ConnectionString, "delete from BookingSlots")

            m_DTBookings = DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Bookings")
            m_DTBookingSlots = DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from BookingSlots")

            Dim _Children As List(Of Business.Child) = Business.Child.RetrieveLiveAndWaitingChildrenByAge

            Session.SetupProgressBar("Rebuilding Bookings...", _Children.Count)

            For Each _C As Business.Child In _Children

                m_DTBookings.Clear()
                m_DTBookingSlots.Clear()

                m_FromDate = Today

                'calculate funding and end dates
                _C._CalcLeaveDate = Bookings.ReturnSchoolLeaveDate(_C._Dob, Me.SchoolLeaveMonths)
                _C.Store()

                If _C._Status <> "Left" Then
                    If _C._Started <= m_CutOff Then
                        If _C._DateLeft.HasValue Then
                            If _C._DateLeft >= m_FromDate Then
                                m_Child = _C
                                DoBuild()
                            End If
                        Else
                            m_Child = _C
                            DoBuild()
                        End If
                    End If
                End If

                If DAL.BulkCopy(Session.ConnectionString, "Bookings", m_DTBookings) Then

                    If Not DAL.BulkCopy(Session.ConnectionString, "BookingSlots", m_DTBookingSlots) Then
                        LogError(_C._ID.Value.ToString + " - " + _C._Fullname)
                    End If

                    _C.BuildDayPatterns(m_FromDate.Value)

                Else
                    LogError(_C._ID.Value.ToString + " - " + _C._Fullname)
                End If

                Session.StepProgressBar()

            Next

            If m_Errors <> "" Then
                My.Computer.Clipboard.Clear()
                My.Computer.Clipboard.SetText(m_Errors)
                CareMessage("One or more errors were encountered. Check Clipboard for more details...", MessageBoxIcon.Exclamation, "Build Bookings")
                Return -2
            End If

            Session.SetProgressMessage("Rebuild Complete.", 3)

            Return 0

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
            Return -99
        End Try

    End Function

    Private Sub LogError(ByVal ErrorText As String)
        m_Errors += ErrorText + vbCrLf
    End Sub

    Private Function NonWeekendDay(ByVal DateIn As Date) As Date

        Select Case DateIn.DayOfWeek

            Case DayOfWeek.Saturday
                Return DateIn.AddDays(2)

            Case DayOfWeek.Sunday
                Return DateIn.AddDays(1)

            Case Else
                Return DateIn

        End Select

    End Function

    Private Function EasterSunday(ByVal YearIn As Integer) As Date?

        Dim _Return As Date? = Nothing
        Dim iMonth, iDay, iMoon, iEpact, iSunDay, iGold, iCent, iCorx, iCorz As Integer

        YearIn = CInt(YearIn)

        If (YearIn >= 1583) And (YearIn <= 8702) Then
            iGold = ((YearIn Mod 19) + 1) 'the golden number of the year in the 19 year metonic cycle
            iCent = CInt(((Int(YearIn / 100)) + 1)) 'calculate the century
            iCorx = CInt((Int((3 * iCent) / 4) - 12)) 'no. of years in which leap year was dropped in order to keep in step with the sun
            iCorz = CInt((Int((8 * iCent + 5) / 25) - 5)) 'special correction to syncronize easter with the moon's orbit
            iSunDay = CInt((Int((5 * YearIn) / 4) - iCorx - 10)) 'find sunday
            iEpact = ((11 * iGold + 20 + iCorz - iCorx) Mod 30) 'set epact (specifies occurance of full moon

            If (iEpact < 0) Then
                iEpact = iEpact + 30
            End If

            If ((iEpact = 25) And (iGold > 11)) Or (iEpact = 24) Then
                iEpact = iEpact + 1
            End If

            iMoon = 44 - iEpact 'Find Full Moon

            If (iMoon < 21) Then
                iMoon = iMoon + 30
            End If

            iMoon = (iMoon + 7 - ((iSunDay + iMoon) Mod 7)) 'advance to sunday

            If (iMoon > 31) Then
                iMonth = 4
                iDay = (iMoon - 31)
            Else
                iMonth = 3
                iDay = iMoon
            End If

            _Return = DateSerial(YearIn, iMonth, iDay)

        End If

        Return _Return

    End Function

    Public Function BuildCalendar(ByVal YearIn As Integer) As Boolean

        Dim _SQL As String = "delete from Calendar where year(date) = " & YearIn.ToString
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Dim _Date As Date = DateSerial(YearIn, 1, 1)
        Dim _To As Date = DateSerial(YearIn, 12, 31)

        Dim _EasterSunday As Date? = EasterSunday(YearIn)
        Dim _BHGoodFriday As Date = _EasterSunday.Value.AddDays(-2)
        Dim _BHEasterMonday As Date = _EasterSunday.Value.AddDays(1)

        Dim _BHNewYearsDay As Date = Nothing
        Dim _BHEarlyMay As Date = Nothing
        Dim _BHSpring As Date = Nothing
        Dim _BHSummer As Date = Nothing
        Dim _BHChristmasDay As Date = Nothing
        Dim _BHBoxingDay As Date = Nothing

        '**************************************************************************************************************************
        'pre-process the year and calculate the bank holidays

        While _Date <= _To

            If _Date = DateSerial(YearIn, 1, 1) Then
                _BHNewYearsDay = NonWeekendDay(_Date)
            End If

            If _Date.Month = 5 Then
                If _Date.DayOfWeek = DayOfWeek.Monday Then
                    If _BHEarlyMay = Nothing Then
                        _BHEarlyMay = _Date
                    Else
                        _BHSpring = _Date
                    End If
                End If
            End If

            If _Date.Month = 8 Then
                If _Date.DayOfWeek = DayOfWeek.Monday Then
                    _BHSummer = _Date
                End If
            End If

            If _Date = DateSerial(YearIn, 12, 25) Then
                _BHChristmasDay = NonWeekendDay(_Date)
            End If

            If _Date = DateSerial(YearIn, 12, 26) Then
                _BHBoxingDay = NonWeekendDay(_BHChristmasDay.AddDays(1))
            End If

            _Date = _Date.AddDays(1)

        End While

        '**************************************************************************************************************************

        _Date = DateSerial(YearIn, 1, 1)
        While _Date <= _To

            Dim _C As New Business.Calendar
            With _C

                ._Date = _Date

                ._Dayofweek = _Date.DayOfWeek.ToString

                If _Date.DayOfWeek = DayOfWeek.Saturday Or _Date.DayOfWeek = DayOfWeek.Sunday Then
                    ._Weekend = True
                Else
                    ._Weekend = False
                End If

                ._Closed = False
                ._NoCharge = False
                ._Holiday = False

                If _Date = _BHNewYearsDay Then
                    ._Holiday = True
                    ._HolidayName = "Bank Holiday"
                End If

                If _Date = _BHGoodFriday Then
                    ._Holiday = True
                    ._HolidayName = "Good Friday"
                End If

                If _Date = _BHEasterMonday Then
                    ._Holiday = True
                    ._HolidayName = "Easter Monday"
                End If

                If _Date = _BHEarlyMay Then
                    ._Holiday = True
                    ._HolidayName = "Early May Bank Holiday"
                End If

                If _Date = _BHSpring Then
                    ._Holiday = True
                    ._HolidayName = "Spring Bank Holiday"
                End If

                If _Date = _BHSummer Then
                    ._Holiday = True
                    ._HolidayName = "Summer Bank Holiday"
                End If

                If _Date = _BHChristmasDay Then
                    ._Holiday = True
                    ._HolidayName = "Bank Holiday"
                End If

                If _Date = _BHBoxingDay Then
                    ._Holiday = True
                    ._HolidayName = "Bank Holiday"
                End If

                .Store()

            End With

            _Date = _Date.AddDays(1)

        End While

        '**************************************************************************************************************************

        Return True

    End Function

    Private Sub DoBuild()

        If Not m_Child._Dob.HasValue Then Exit Sub

        'manage start and end dates
        Dim _EndDate As Date? = Nothing
        m_FromDate = m_Child._Started.Value

        'check parameter to override start date
        Dim startDate As Date? = ParameterHandler.ReturnDate("BOOKINGSSTART")
        If Not IsNothing(startDate) Then m_FromDate = startDate

        'assume cut off date
        _EndDate = m_CutOff

        'do we have a leaving date?
        If m_Child._DateLeft.HasValue Then
            'if leave date is prior to end date, use this
            If m_Child._DateLeft < _EndDate Then _EndDate = m_Child._DateLeft
        Else
            'if calculated leave date is prior to end date, use this
            If m_Child._CalcLeaveDate < _EndDate Then _EndDate = m_Child._CalcLeaveDate
        End If

        Dim _Sessions As List(Of Business.ChildAdvanceSession) = Business.ChildAdvanceSession.RetrieveByChildID(m_Child._ID.Value)
        Dim _Holidays As List(Of Business.ChildHoliday) = Business.ChildHoliday.RetreiveRange(m_Child._ID.Value, m_FromDate, _EndDate)
        Dim _AgePoints As Integer = 0

        'loop through all the dates in the range
        Dim _Date As Date = m_FromDate.Value
        While _Date <= _EndDate

            m_LocalBookings.Clear()

            Dim _Category As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(_Date, m_Child._TermType)

            'check if date is excluded for this child
            Dim _Excluded As Bookings.EnumExcluded = Bookings.IsDateExcluded(_Date, m_Child, _Holidays, Bookings.EnumCheckMode.Bookings, _Category)

            Select Case _Excluded

                'only pickup the non-excluded children and the children on holidays
                'we will still store all the booking information even if the child is on holiday, we just change the status of the booking
                Case Bookings.EnumExcluded.NotExcluded, Bookings.EnumExcluded.ChildOnHoliday

                    'check if there is an override for this date
                    Dim _OverrideFound As Boolean = CheckForOverride(_Sessions, _Date, _Category)
                    If Not _OverrideFound Then
                        CheckForRecurringAndAddtional(_Sessions, _Date, _Category)
                    End If

                    'write local back to db
                    UpdateBookingsDT(_Date, _Excluded)

            End Select

            _Date = _Date.AddDays(1)

        End While

    End Sub

    Private Function CheckForOverride(ByRef Sessions As List(Of Business.ChildAdvanceSession), ByVal DateIn As Date, ByVal DateCategory As Bookings.EnumDateCategory) As Boolean
        Return ProcessMultiThenSingle(Sessions, DateIn, Business.ChildAdvanceSession.EnumPatternType.Override, DateCategory)
    End Function

    Private Function CheckForRecurringAndAddtional(ByRef Sessions As List(Of Business.ChildAdvanceSession), ByVal DateIn As Date, ByVal DateCategory As Bookings.EnumDateCategory) As Boolean

        Dim _Recurring As Boolean = ProcessMultiThenSingle(Sessions, DateIn, Business.ChildAdvanceSession.EnumPatternType.Recurring, DateCategory)
        Dim _Additional As Boolean = ProcessMultiThenSingle(Sessions, DateIn, Business.ChildAdvanceSession.EnumPatternType.Additional, DateCategory)

        If _Recurring Or _Additional Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function ProcessMultiThenSingle(ByRef Sessions As List(Of Business.ChildAdvanceSession), ByVal DateIn As Date,
                                            ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType, ByVal DateCategory As Bookings.EnumDateCategory) As Boolean

        'potentially, there could be a daily and weekly pattern for this child for this date
        'first, we must check if there is a daily pattern, if there is - we use this
        'otherwise we go for the weekly pattern

        Dim _Daily As Business.ChildAdvanceSession = RetrieveSession(Sessions, m_Child._ID, DateIn, Business.ChildAdvanceSession.EnumMode.Daily, PatternType, DateCategory)
        If _Daily IsNot Nothing Then
            'in daily mode, there are potentially multiple tariffs per day
            If ProcessDailyMode(DateIn, _Daily) Then
                Return True
            Else
                'if we are in override mode, we return true as the user has said they don't want a tariff to be applied on this date
                If PatternType = Business.ChildAdvanceSession.EnumPatternType.Override Then Return True
            End If
        Else
            'no tariff details for the specified date
        End If

        'in weekly mode, there can only be one session per day
        Dim _Session As Business.ChildAdvanceSession = RetrieveSession(Sessions, m_Child._ID, DateIn, Business.ChildAdvanceSession.EnumMode.Weekly, PatternType, DateCategory)
        If _Session IsNot Nothing Then

            Dim _Tariff As Business.Tariff = FetchTariffFromWeek(DateIn, _Session, m_Tariffs)
            If _Tariff IsNot Nothing Then

                Dim _StartTime As TimeSpan? = Nothing
                Dim _EndTime As TimeSpan? = Nothing
                Dim _FundedHours As Decimal = 0
                Dim _BoltOns As String = ""

                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DateIn.DayOfWeek, _Tariff, _Session)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DateIn.DayOfWeek, _Tariff, _Session)
                SetFundedHours(_FundedHours, DateIn.DayOfWeek, _Tariff, _Session)
                SetBoltOns(_BoltOns, DateIn.DayOfWeek, _Tariff, _Session)

                CreateLocalBookingRecord(DateIn, _Session, _Tariff, _StartTime, _EndTime, _FundedHours, _BoltOns)

                Return True

            Else
                'no tariff details for the specified date
                'if we are in override mode, we return true as the user has said they don't want a tariff to be applied on this date
                If PatternType = Business.ChildAdvanceSession.EnumPatternType.Override Then Return True
            End If

        Else
            'no tariff details for the specified date
        End If

        Return False

    End Function

    Private Function RetrieveSession(ByRef Sessions As List(Of Business.ChildAdvanceSession), ByVal ChildID As Guid?, ByVal FromDate As Date,
                                     ByVal SessionMode As Business.ChildAdvanceSession.EnumMode, ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType,
                                     ByVal DateCategory As Bookings.EnumDateCategory) As Business.ChildAdvanceSession

        Dim _Return As Business.ChildAdvanceSession = Nothing

        If PatternType = Business.ChildAdvanceSession.EnumPatternType.Recurring Then

            'Scopes
            '0 = Disabled
            '1 = Enabled, All the time
            '2 = Enabled, Term-Time Only
            '3 = Enabled, Holidays Only

            Dim _Q As IEnumerable(Of Business.ChildAdvanceSession) = Nothing

            If SessionMode = Business.ChildAdvanceSession.EnumMode.Daily Then

                If DateCategory = Bookings.EnumDateCategory.TermDate Then

                    _Q = From _S As Business.ChildAdvanceSession In Sessions
                         Where _S._ChildId = ChildID _
                         And _S._DateFrom <= FromDate _
                         And _S._Mode = SessionMode.ToString _
                         And _S._DateFrom.Value.DayOfWeek = FromDate.DayOfWeek _
                         And _S._PatternType = PatternType.ToString _
                         And (_S._RecurringScope = 1 Or _S._RecurringScope = 2)
                         Order By _S._DateFrom Descending

                Else

                    _Q = From _S As Business.ChildAdvanceSession In Sessions
                         Where _S._ChildId = ChildID _
                         And _S._DateFrom <= FromDate _
                         And _S._Mode = SessionMode.ToString _
                         And _S._DateFrom.Value.DayOfWeek = FromDate.DayOfWeek _
                         And _S._PatternType = PatternType.ToString _
                         And (_S._RecurringScope = 1 Or _S._RecurringScope = 3)
                         Order By _S._DateFrom Descending

                End If

                If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
                    _Return = _Q.First
                End If

            Else

                If DateCategory = Bookings.EnumDateCategory.TermDate Then

                    _Q = From _S As Business.ChildAdvanceSession In Sessions
                         Where _S._ChildId = ChildID _
                         And _S._DateFrom <= FromDate _
                         And _S._Mode = SessionMode.ToString _
                         And _S._PatternType = PatternType.ToString _
                         And (_S._RecurringScope = 1 Or _S._RecurringScope = 2)
                         Order By _S._DateFrom Descending

                Else

                    _Q = From _S As Business.ChildAdvanceSession In Sessions
                         Where _S._ChildId = ChildID _
                         And _S._DateFrom <= FromDate _
                         And _S._Mode = SessionMode.ToString _
                         And _S._PatternType = PatternType.ToString _
                         And (_S._RecurringScope = 1 Or _S._RecurringScope = 3)
                         Order By _S._DateFrom Descending

                End If


                If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
                    _Return = _Q.First
                End If

            End If

        Else

            'non-recurring

            If SessionMode = Business.ChildAdvanceSession.EnumMode.Daily Then

                Dim _Q As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                         Where _S._ChildId = ChildID _
                                                                         And _S._DateFrom = FromDate _
                                                                         And _S._Mode = SessionMode.ToString _
                                                                         And _S._DateFrom.Value.DayOfWeek = FromDate.DayOfWeek _
                                                                         And _S._PatternType = PatternType.ToString
                                                                         Order By _S._DateFrom Descending

                If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
                    _Return = _Q.First
                End If

            Else

                'this routine is passed in each day of a week, but for a non-recurring pattern
                'we need to ensure that we always change the date back to the monday as single sessions
                'are only processed on a monday...

                Dim _WC As Date = ValueHandler.NearestDate(FromDate, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

                Dim _Q As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                         Where _S._ChildId = ChildID _
                                                                         And _S._DateFrom = _WC _
                                                                         And _S._Mode = SessionMode.ToString _
                                                                         And _S._PatternType = PatternType.ToString
                                                                         Order By _S._DateFrom Descending

                If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
                    _Return = _Q.First
                End If

            End If

        End If

        Return _Return

    End Function

    Private Function ProcessDailyMode(ByVal BookingDate As Date, ByRef SessionRecord As Business.ChildAdvanceSession) As Boolean

        'check each "day" for a session
        'these are not days - we are just using the same fields to support seven sessions per day

        Dim _CreateBooking As Boolean = False
        Dim _StartTime As TimeSpan? = Nothing
        Dim _EndTime As TimeSpan? = Nothing
        Dim _FundedHours As Decimal = 0
        Dim _BoltOns As String = ""

        Dim _T As Business.Tariff = Nothing

        If SessionRecord._Monday.HasValue Then
            _T = FetchTariff(SessionRecord._Monday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Monday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Monday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Monday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Monday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Tuesday.HasValue Then
            _T = FetchTariff(SessionRecord._Tuesday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Tuesday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Tuesday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Tuesday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Tuesday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Wednesday.HasValue Then
            _T = FetchTariff(SessionRecord._Wednesday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Wednesday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Wednesday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Wednesday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Wednesday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Thursday.HasValue Then
            _T = FetchTariff(SessionRecord._Thursday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Thursday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Thursday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Thursday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Thursday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Friday.HasValue Then
            _T = FetchTariff(SessionRecord._Friday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Friday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Friday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Friday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Friday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Saturday.HasValue Then
            _T = FetchTariff(SessionRecord._Saturday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Saturday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Saturday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Saturday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Saturday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        If SessionRecord._Sunday.HasValue Then
            _T = FetchTariff(SessionRecord._Sunday.Value)
            If _T IsNot Nothing Then
                _CreateBooking = True
                SetTimeSpan(_StartTime, EnumStartOrEnd.StartTime, DayOfWeek.Sunday, _T, SessionRecord)
                SetTimeSpan(_EndTime, EnumStartOrEnd.EndTime, DayOfWeek.Sunday, _T, SessionRecord)
                SetFundedHours(_FundedHours, DayOfWeek.Sunday, _T, SessionRecord)
                SetBoltOns(_BoltOns, DayOfWeek.Sunday, _T, SessionRecord)
                CreateLocalBookingRecord(BookingDate, SessionRecord, _T, _StartTime, _EndTime, _FundedHours, _BoltOns)
            End If
        End If

        _T = Nothing

        Return _CreateBooking

    End Function

    Private Function FetchTariff(ByVal TariffID As Guid) As Business.Tariff

        Dim _Return As Business.Tariff = Nothing
        Dim _Q As IEnumerable(Of Business.Tariff) = From _T As Business.Tariff In m_Tariffs Where _T._ID = TariffID
        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                _Return = _Q.First
            End If
            _Q = Nothing
        End If

        Return _Return

    End Function

    Private Sub SetBoltOns(ByRef BoltOns As String, ByVal DayOfWeek As DayOfWeek,
                           ByVal Tariff As Business.Tariff, ByRef ChildSession As Business.ChildAdvanceSession)

        Dim _Value As String = ""
        Select Case DayOfWeek

            Case System.DayOfWeek.Monday
                _Value = ChildSession._MondayBo

            Case System.DayOfWeek.Tuesday
                _Value = ChildSession._TuesdayBo

            Case System.DayOfWeek.Wednesday
                _Value = ChildSession._WednesdayBo

            Case System.DayOfWeek.Thursday
                _Value = ChildSession._ThursdayBo

            Case System.DayOfWeek.Friday
                _Value = ChildSession._FridayBo

            Case System.DayOfWeek.Saturday
                _Value = ChildSession._SaturdayBo

            Case System.DayOfWeek.Sunday
                _Value = ChildSession._SundayBo

        End Select

        BoltOns += _Value

    End Sub

    Private Sub SetFundedHours(ByRef CurrentFundedHours As Decimal, ByVal DayOfWeek As DayOfWeek,
                               ByVal Tariff As Business.Tariff, ByRef ChildSession As Business.ChildAdvanceSession)

        Dim _Value As Decimal = 0
        Select Case DayOfWeek

            Case System.DayOfWeek.Monday
                _Value = ChildSession._MondayFund

            Case System.DayOfWeek.Tuesday
                _Value = ChildSession._TuesdayFund

            Case System.DayOfWeek.Wednesday
                _Value = ChildSession._WednesdayFund

            Case System.DayOfWeek.Thursday
                _Value = ChildSession._ThursdayFund

            Case System.DayOfWeek.Friday
                _Value = ChildSession._FridayFund

            Case System.DayOfWeek.Saturday
                _Value = ChildSession._SaturdayFund

            Case System.DayOfWeek.Sunday
                _Value = ChildSession._SundayFund

        End Select

        CurrentFundedHours += _Value

    End Sub

    Private Sub SetTimeSpan(ByRef CurrentValue As TimeSpan?, ByVal StartOrEnd As EnumStartOrEnd, ByVal DayOfWeek As DayOfWeek,
                            ByVal Tariff As Business.Tariff, ByRef ChildSession As Business.ChildAdvanceSession)

        'check if the time is on the child session record
        Select Case Tariff._Type

            Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                CurrentValue = ReturnManualTime(ChildSession, DayOfWeek, StartOrEnd)

            Case Else

                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    If Tariff._StartTime.HasValue Then
                        If CurrentValue.HasValue = False Then
                            CurrentValue = Tariff._StartTime
                        Else
                            If Tariff._StartTime <= CurrentValue.Value Then
                                CurrentValue = Tariff._StartTime
                            End If
                        End If
                    End If
                Else
                    If Tariff._EndTime.HasValue Then
                        If CurrentValue.HasValue = False Then
                            CurrentValue = Tariff._EndTime
                        Else
                            If Tariff._EndTime >= CurrentValue.Value Then
                                CurrentValue = Tariff._EndTime
                            End If
                        End If
                    End If
                End If

        End Select

    End Sub

    Private Function ReturnManualTime(ByRef ChildSession As Business.ChildAdvanceSession, ByVal DayOfWeek As DayOfWeek, ByVal StartOrEnd As EnumStartOrEnd) As TimeSpan?

        Dim _Value As Decimal = 0
        Select Case DayOfWeek

            Case System.DayOfWeek.Monday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Monday1
                Else
                    _Value = ChildSession._Monday2
                End If

            Case System.DayOfWeek.Tuesday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Tuesday1
                Else
                    _Value = ChildSession._Tuesday2
                End If

            Case System.DayOfWeek.Wednesday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Wednesday1
                Else
                    _Value = ChildSession._Wednesday2
                End If

            Case System.DayOfWeek.Thursday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Thursday1
                Else
                    _Value = ChildSession._Thursday2
                End If

            Case System.DayOfWeek.Friday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Friday1
                Else
                    _Value = ChildSession._Friday2
                End If

            Case System.DayOfWeek.Saturday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Saturday1
                Else
                    _Value = ChildSession._Saturday2
                End If

            Case System.DayOfWeek.Sunday
                If StartOrEnd = EnumStartOrEnd.StartTime Then
                    _Value = ChildSession._Sunday1
                Else
                    _Value = ChildSession._Sunday2
                End If

        End Select

        If _Value > 0 Then
            Dim _D As Date? = ValueHandler.ReturnTime(CDbl(_Value))
            If _D.HasValue Then
                Dim _T As TimeSpan = _D.Value.TimeOfDay
                Return _T
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    Private Sub CreateLocalBookingRecord(ByVal BookingDate As Date,
                                         ByRef ChildSession As Business.ChildAdvanceSession, ByRef Tariff As Business.Tariff,
                                         ByVal StartTime As TimeSpan?, ByVal EndTime As TimeSpan?, ByVal FundedHours As Decimal, ByVal BoltOns As String)

        Dim _StartTime As TimeSpan? = StartTime
        Dim _EndTime As TimeSpan? = EndTime

        If Not _StartTime.HasValue Then _StartTime = m_TimeEarly
        If Not _EndTime.HasValue Then _EndTime = m_TimeLate

        Dim _B As New LocalBooking
        With _B

            .BookingDate = BookingDate
            .BookingFrom = _StartTime.Value
            .BookingTo = _EndTime.Value

            .ChildID = m_Child._ID.Value

            If Tariff Is Nothing Then
                .TariffID = Nothing
                .TariffName = ""
                .TariffRate = 0
                .TariffBoltOns = ""
                .VoidFrom = Nothing
                .VoidTo = Nothing
            Else
                .TariffID = Tariff._ID
                .TariffName = Tariff._Name
                .TariffRate = Tariff._Rate
                .TariffBoltOns = BoltOns
                .VoidFrom = Tariff._VoidStart
                .VoidTo = Tariff._VoidEnd
            End If

            If _StartTime.HasValue AndAlso _EndTime.HasValue Then
                .Hours = CDec(_EndTime.Value.TotalHours - _StartTime.Value.TotalHours)
                .Times = BuildTime(StartTime, EndTime)
            Else
                .Hours = 0
                .Times = ""
            End If

            .FundedHours = FundedHours
            .BookingStatus = ChildSession._BookingStatus

        End With

        m_LocalBookings.Add(_B)

    End Sub

    Private Function BuildTime(ByVal StartTime As TimeSpan?, ByVal EndTime As TimeSpan?) As String
        If StartTime.HasValue AndAlso EndTime.HasValue Then
            Dim _From As String = HHMM(StartTime.Value)
            Dim _To As String = HHMM(EndTime.Value)
            Return _From + "-" + _To
        Else
            Return ""
        End If
    End Function

    Private Function HHMM(ByVal TimeSpanIn As TimeSpan) As String
        Return Format(TimeSpanIn.Hours, "00") + ":" + Format(TimeSpanIn.Minutes, "00")
    End Function

    Private Function CalculateAgePoints(ByVal MonthsOld As Long) As Integer

        Dim _Return As Integer = 0
        Dim _Q As IEnumerable(Of Business.AgePoint) = From _P As Business.AgePoint In m_Points Where MonthsOld >= _P._AgeMin AndAlso MonthsOld <= _P._AgeMax Order By _P._AgeMax Descending
        If _Q IsNot Nothing Then
            If _Q.Count >= 1 Then
                _Return = _Q.First._Points
            End If
            _Q = Nothing
        End If

        Return _Return

    End Function

    Private Sub SetHours(ByRef LocalBookingRecord As LocalBooking, ByRef ChildSession As Business.ChildAdvanceSession)

    End Sub

    Private Sub UpdateBookingsDT(ByVal DateIn As Date, ByVal Excluded As Bookings.EnumExcluded)

        If m_LocalBookings.Count = 0 Then Exit Sub

        Dim _StartTime As TimeSpan? = Nothing
        Dim _EndTime As TimeSpan? = Nothing

        Dim _VoidFrom As TimeSpan? = Nothing
        Dim _VoidTo As TimeSpan? = Nothing

        Dim _TariffID As Guid? = Nothing
        Dim _TariffName As String = ""
        Dim _TariffRate As Decimal = 0
        Dim _BoltOns As String = ""
        Dim _Count As Integer = 0

        Dim _Hours As Decimal = 0
        Dim _FundedHours As Decimal = 0

        Dim _Status As String = ""
        Dim _Times As String = ""

        For Each _B In m_LocalBookings

            If _Count = 0 Then
                _TariffID = _B.TariffID
            Else
                'there is more than one tariff used for this date, so we set the tariff to nothing
                _TariffID = Nothing
            End If

            If Excluded = Bookings.EnumExcluded.ChildOnHoliday Then
                _Status = "Holiday"
            Else
                _Status = _B.BookingStatus
            End If

            If _StartTime.HasValue Then
                If _B.BookingFrom < _StartTime Then _StartTime = _B.BookingFrom
            Else
                _StartTime = _B.BookingFrom
            End If

            If _EndTime.HasValue Then
                If _B.BookingTo > _EndTime Then _EndTime = _B.BookingTo
            Else
                _EndTime = _B.BookingTo
            End If

            If _VoidFrom.HasValue Then
                If _B.VoidFrom < _VoidFrom Then _VoidFrom = _B.VoidFrom
            Else
                _VoidFrom = _B.VoidFrom
            End If

            If _VoidTo.HasValue Then
                If _B.VoidTo > _VoidTo Then _VoidTo = _B.VoidTo
            Else
                _VoidTo = _B.VoidTo
            End If

            If _TariffName = "" Then
                _TariffName = _B.TariffName
            Else
                _TariffName += vbCrLf + _B.TariffName
            End If

            _TariffRate += _B.TariffRate
            _Hours += _B.Hours
            _FundedHours += _B.FundedHours
            _BoltOns += _B.TariffBoltOns

            If _Times = "" Then
                _Times = _B.Times
            Else
                _Times += vbCrLf + _B.Times
            End If

            _Count += 1

        Next

        InsertBookingRow(DateIn, _StartTime.Value, _EndTime.Value, _VoidFrom, _VoidTo, _Status, _Times,
                         _TariffID, _TariffName, _TariffRate, _BoltOns, _Count, _Hours, _FundedHours)

    End Sub

    Private Sub ProcessTimeSlots(ByVal BookingID As Guid, ByVal BookingDate As Date, ByVal StartTime As TimeSpan, ByVal EndTime As TimeSpan, _
                                 ByVal VoidFrom As TimeSpan?, ByVal VoidTo As TimeSpan?, ByVal AgeInMonths As Long, _
                                 ByVal BoltOns As String, ByRef RatioID As Guid?, ByRef RatioStatus As EnumRatioStatus, ByRef AM As Integer, ByRef PM As Integer, _
                                 ByRef Breakfast As Integer, ByRef Lunch As Integer, ByRef Tea As Integer, ByRef Snack As Integer, _
                                 ByRef TimeSlots As String)

        'process all the time-slots for the booking
        Dim _TimeSlots As IEnumerable(Of Business.TimeSlot) = From _T As Business.TimeSlot In m_TimeSlots _
                                                      Where _T._StartTime <= EndTime _
                                                      And _T._EndTime >= StartTime _
                                                      Order By _T._StartTime

        If _TimeSlots IsNot Nothing Then

            For Each _TS In _TimeSlots

                'check if this timeslot falls within a void period defined on a tariff
                'i.e. wrap around children where the void period is the school day
                If Not FallsWithinVoidPeriod(_TS, VoidFrom, VoidTo) Then

                    If RatioID Is Nothing Then
                        RatioID = GetRatioID(BookingDate, _TS._ID, AgeInMonths, RatioStatus)
                    End If

                    InsertBookingSlot(BookingID, BookingDate, m_Child._ID.Value, RatioID, _TS)

                    If _TS._Am Then AM = 1
                    If _TS._Pm Then PM = 1

                    If _TS._Breakfast Then Breakfast = 1
                    If _TS._Lunch Then Lunch = 1
                    If _TS._Tea Then Tea = 1
                    If _TS._Snack Then Snack = 1

                    If TimeSlots = "" Then
                        TimeSlots = _TS._Name
                    Else
                        TimeSlots += vbCrLf + _TS._Name
                    End If

                End If

            Next

            _TimeSlots = Nothing

        End If

    End Sub

    Private Function FallsWithinVoidPeriod(ByVal TimeSlot As Business.TimeSlot, VoidFrom As TimeSpan?, VoidTo As TimeSpan?) As Boolean

        If Not VoidFrom.HasValue Then Return False
        If Not VoidTo.HasValue Then Return False

        'timeslot ends before the void period starts
        If TimeSlot._EndTime.Value < VoidFrom Then Return False

        'timeslot begins after the void period ends
        If TimeSlot._StartTime.Value > VoidTo Then Return False

        Return True

    End Function

    Private Sub InsertBookingRow(ByVal BookingDate As Date, ByVal StartTime As TimeSpan, ByVal EndTime As TimeSpan, _
                                 ByVal VoidFrom As TimeSpan?, ByVal VoidTo As TimeSpan?, _
                                 ByVal BookingStatus As String, ByVal Times As String, _
                                 ByVal TariffID As Guid?, ByVal TariffName As String, ByVal TariffRate As Decimal, ByVal BoltOns As String, _
                                 ByVal BookingCount As Integer, ByVal Hours As Decimal, ByVal FundedHours As Decimal)

        'we are not inserting the booking into the datatable which will be bulk inserted later
        'this record will effectively summarise the booking for this child for this day

        'we must process the timeslots covered by this booking
        'this will determine AM/PM as well as checking the room/ratio we are using for the child

        Dim _ID As Guid = Guid.NewGuid

        Dim _AgeInMonths As Long = 0
        If m_Child._Dob.HasValue Then _AgeInMonths = ValueHandler.ReturnExactMonths(m_Child._Dob.Value, BookingDate)

        Dim _RatioID As Guid? = Nothing
        Dim _AM As Integer = 0
        Dim _PM As Integer = 0

        Dim _Breakfast As Integer = 0
        Dim _Lunch As Integer = 0
        Dim _Tea As Integer = 0
        Dim _Snack As Integer = 0

        Dim _TimeSlots As String = ""
        Dim _TariffName As String = ""

        Dim _RatioStatus As EnumRatioStatus = EnumRatioStatus.OK

        ProcessTimeSlots(_ID, BookingDate, StartTime, EndTime, VoidFrom, VoidTo, _AgeInMonths, BoltOns, _RatioID, _RatioStatus, _AM, _PM,
                         _Breakfast, _Lunch, _Tea, _Snack, _TimeSlots)

        Dim _B As DataRow = m_DTBookings.NewRow
        With _B

            .Item("ID") = _ID

            .Item("site_id") = m_Child._SiteId
            .Item("site_name") = m_Child._SiteName

            .Item("booking_date") = BookingDate
            .Item("booking_from") = StartTime
            .Item("booking_to") = EndTime

            .Item("void_from") = HandleNull(VoidFrom)
            .Item("void_to") = HandleNull(VoidTo)

            .Item("booking_status") = BookingStatus

            .Item("child_id") = m_Child._ID
            .Item("child_name") = m_Child._Fullname
            .Item("child_dob") = m_Child._Dob

            .Item("child_age") = _AgeInMonths
            .Item("points") = CalculateAgePoints(_AgeInMonths)


            .Item("ratio_id") = HandleNull(_RatioID)

            Select Case _RatioStatus

                Case EnumRatioStatus.NoRooms
                    _TariffName = "***  No Room Applicable for Age *** " + TariffName

                Case EnumRatioStatus.RoomsFull
                    _TariffName = "***  No Space *** " + TariffName

                Case EnumRatioStatus.ManualOverride
                    _TariffName = TariffName + " (Manual)"

                Case EnumRatioStatus.OK
                    _TariffName = TariffName

                Case Else
                    _TariffName = "***  Unhandled Error *** " + TariffName

            End Select

            .Item("session_count") = BookingCount

            .Item("hours") = Hours
            .Item("funded_hours") = FundedHours

            .Item("am") = _AM
            .Item("pm") = _PM

            .Item("breakfast") = _Breakfast
            .Item("lunch") = _Lunch
            .Item("tea") = _Tea
            .Item("snacks") = _Snack

            If Hours >= m_FullTimeHours Then
                .Item("full_time") = 1
            Else
                .Item("full_time") = 0
            End If

            .Item("tariff_id") = HandleNull(TariffID)
            .Item("tariff_name") = _TariffName
            .Item("tariff_rate") = TariffRate

            .Item("bolt_ons") = BoltOns
            .Item("timeslots") = _TimeSlots
            .Item("times") = Times

        End With

        m_DTBookings.Rows.Add(_B)

        _B = Nothing

    End Sub

    Private Function HandleNull(ByVal ValueIn As Object) As Object
        If ValueIn Is Nothing Then
            Return DBNull.Value
        Else
            Return ValueIn
        End If
    End Function

    Private Function GetRatioID(ByVal BookingDate As Date, ByVal TimeSlot As Guid?, ByVal AgeInMonths As Long, ByRef RatioStatus As EnumRatioStatus) As Guid?

        Dim _Q As IEnumerable(Of Business.RoomRatios) = Nothing

        If m_Child._MoveMode = "Manual" AndAlso m_Child._MoveDate.HasValue Then

            If BookingDate >= m_Child._MoveDate.Value Then

                _Q = From _RR As Business.RoomRatios In m_RoomRatios _
                     Where _RR.RatioID = m_Child._MoveRatioId.Value

                RatioStatus = EnumRatioStatus.ManualOverride

            End If

        End If

        If _Q Is Nothing Then

            _Q = From _RR As Business.RoomRatios In m_RoomRatios _
                 Where _RR.SiteID = m_Child._SiteId _
                 And _RR.AgeFrom <= AgeInMonths _
                 And _RR.AgeTo >= AgeInMonths _
                 Order By _RR.AgeFrom, _RR.Sequence, _RR.RatioID

        End If

        If _Q IsNot Nothing Then

            If _Q.Count > 0 Then

                If m_CheckSpace Then

                    'loop through all available ratios
                    'there could be several rooms with the same age ranges, so we need to fill the rooms to capacity then use the next room...
                    For Each _R As Business.RoomRatios In _Q

                        'ensure there is enough space in this room
                        Dim _Booked As Integer = GetBooked(BookingDate, _R.RatioID.Value, TimeSlot)

                        'we add onto the booked already amount as this call is going to count to the bookings total
                        Dim _Space As Integer = _R.Capacity - _Booked

                        If _Space >= 1 Then
                            RatioStatus = EnumRatioStatus.OK
                            Return _R.RatioID
                        Else
                            'not enough space in this room
                        End If

                    Next

                    'all rooms are full
                    RatioStatus = EnumRatioStatus.RoomsFull
                    Return Nothing

                Else
                    RatioStatus = EnumRatioStatus.OK
                    Return _Q.First.RatioID
                End If

            End If

        End If

        'no rooms for the age band at this site
        RatioStatus = EnumRatioStatus.NoRooms
        Return Nothing

    End Function

    Private Function GetBooked(ByVal BookingDate As Date, ByVal RatioID As Guid?, ByVal TimeSlot As Guid?) As Integer

        Dim _Return As Integer = 0

        'get the ratios and timeslots for the booking date using the hashtable
        Dim _Key As Long = GetHashKey(BookingDate)

        'check if we have already stored this date
        If m_HashDates.ContainsKey(_Key) Then
            Dim _RTSList As List(Of LocalRatioTimeSlot) = CType(m_HashDates.Item(_Key), List(Of LocalRatioTimeSlot))
            For Each _RTS As LocalRatioTimeSlot In _RTSList
                If _RTS.RatioID = RatioID AndAlso _RTS.TimeSlotID = TimeSlot Then
                    _Return = _RTS.HeadCount
                End If
            Next
        Else
            'not even had a booking for this date...
        End If

        Return _Return

    End Function

    Private Sub IncrementSpaceUsed(ByVal BookingDate As Date, ByVal RatioID As Guid?, ByVal TimeSlot As Guid?)

        If Not m_CheckSpace Then Exit Sub

        Dim _Key As Long = GetHashKey(BookingDate)
        Dim _RTSList As New List(Of LocalRatioTimeSlot)

        'check if we have already stored this date
        If m_HashDates.ContainsKey(_Key) Then

            'fetch the list of ratios and timeslots for this date
            _RTSList = CType(m_HashDates.Item(_Key), List(Of LocalRatioTimeSlot))

            'loop through the list and increment count if exists
            Dim _CreateNew As Boolean = True
            For Each _RTS As LocalRatioTimeSlot In _RTSList
                If _RTS.RatioID = RatioID AndAlso _RTS.TimeSlotID = TimeSlot Then
                    _CreateNew = False
                    _RTS.HeadCount += 1
                    Exit For
                End If
            Next

            If _CreateNew Then
                'we do not already have a headcount for this ratio and timeslot
                Dim _RTS As New LocalRatioTimeSlot(RatioID, TimeSlot)
                _RTSList.Add(_RTS)
            End If

            'update the existing hashtable value
            m_HashDates.Item(_Key) = _RTSList

        Else

            'create a new hashtable entry for this date

            Dim _RTS As New LocalRatioTimeSlot(RatioID, TimeSlot)
            _RTSList.Add(_RTS)

            m_HashDates.Add(_Key, _RTSList)

        End If

    End Sub

    Private Function GetHashKey(ByVal BookingDate As Date) As Long
        Return DateDiff(DateInterval.Day, DateSerial(2000, 1, 1), BookingDate)
    End Function

    Private Sub InsertBookingSlot(ByVal BookingID As Guid, ByVal BookingDate As Date, ByVal ChildID As Guid, ByVal RatioID As Guid?, ByVal TimeSlot As Business.TimeSlot)

        'ensure we do not already have a booking slot stored for this child for this timeslot
        If Not SlotExists(BookingID, TimeSlot._ID) Then

            Dim _BS As DataRow = m_DTBookingSlots.NewRow
            With _BS

                .Item("ID") = Guid.NewGuid
                .Item("booking_id") = BookingID
                .Item("booking_date") = BookingDate
                .Item("child_id") = ChildID

                If RatioID Is Nothing Then
                    .Item("ratio_id") = DBNull.Value
                Else
                    .Item("ratio_id") = RatioID
                End If

                .Item("slot_id") = TimeSlot._ID
                .Item("slot_name") = TimeSlot._Name
                .Item("slot_start") = TimeSlot._StartTime
                .Item("slot_end") = TimeSlot._EndTime
                .Item("booked") = True
            End With

            m_DTBookingSlots.Rows.Add(_BS)

            _BS = Nothing

            IncrementSpaceUsed(BookingDate, RatioID, TimeSlot._ID)

        Else
            'slot already exists
            Stop
        End If

    End Sub

    Private Function SlotExists(ByVal BookingID As Guid?, ByVal TimeSlotID As Guid?) As Boolean

        If m_DTBookingSlots.Rows.Count = 0 Then Return False

        For Each _DR As DataRow In m_DTBookingSlots.Rows
            If _DR.Item("booking_id").ToString = BookingID.ToString AndAlso _DR.Item("slot_id").ToString = TimeSlotID.ToString Then
                Return True
            End If
        Next

        Return False

    End Function

    Private Function FetchTariffFromWeek(ByVal DateIn As Date, ByRef SessionRecord As Business.ChildAdvanceSession, ByRef Tariffs As List(Of Business.Tariff)) As Business.Tariff

        Dim _TariffID As Guid? = Nothing
        If DateIn.DayOfWeek = DayOfWeek.Monday Then _TariffID = SessionRecord._Monday
        If DateIn.DayOfWeek = DayOfWeek.Tuesday Then _TariffID = SessionRecord._Tuesday
        If DateIn.DayOfWeek = DayOfWeek.Wednesday Then _TariffID = SessionRecord._Wednesday
        If DateIn.DayOfWeek = DayOfWeek.Thursday Then _TariffID = SessionRecord._Thursday
        If DateIn.DayOfWeek = DayOfWeek.Friday Then _TariffID = SessionRecord._Friday
        If DateIn.DayOfWeek = DayOfWeek.Saturday Then _TariffID = SessionRecord._Saturday
        If DateIn.DayOfWeek = DayOfWeek.Sunday Then _TariffID = SessionRecord._Sunday

        If _TariffID.HasValue Then
            Dim _Q As IEnumerable(Of Business.Tariff) = From _T As Business.Tariff In Tariffs Where _T._ID = _TariffID
            If _Q IsNot Nothing Then
                If _Q.Count = 1 Then
                    Return _Q.First
                End If
                _Q = Nothing
            End If
        End If

        Return Nothing

    End Function

End Class
