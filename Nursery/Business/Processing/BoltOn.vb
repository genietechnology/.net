﻿

Public Class BoltOn

    Public Property ID As Guid
    Public Property Name As String
    Public Property Price As Decimal
    Public Property NLCode As String
    Public Property NLTracking As String
    Public Property Discount As Boolean
    Public Property SummaryColumn As String
    Public Property ChargeClosed As Boolean
    Public Property ChargeHolidays As Boolean
    Public Property RecurringScope As Byte

    Public Sub New(ByVal ID As Guid, ByVal Name As String, ByVal Price As Decimal, ByVal NLCode As String, ByVal NLTracking As String, _
                   ByVal Discount As Boolean, ByVal SummaryColumn As String, _
                   ByVal ChargeClosed As Boolean, ByVal ChargeHoliday As Boolean, ByVal RecurringScope As Byte)

        Me.ID = ID
        Me.Name = Name
        Me.Price = Price
        Me.NLCode = NLCode
        Me.NLTracking = NLTracking
        Me.Discount = Discount
        Me.SummaryColumn = SummaryColumn
        Me.ChargeClosed = ChargeClosed
        Me.ChargeHolidays = ChargeHoliday
        Me.RecurringScope = RecurringScope

    End Sub

End Class
