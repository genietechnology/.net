﻿

Imports Care.Global
Imports Care.Data
Imports System.Globalization

Public Class OccupancyEngine

    Private m_FTHours As Decimal = 10
    Private m_Rooms As New List(Of Business.RoomRatios)

    Public Sub RebuildOccupancy(ByVal ExcludeWL As Boolean, ByVal ExcludeHols As Boolean)

        Dim _SQL As String = ""

        _SQL = "delete from OccMonths"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from OccWeeks"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from OccDetail"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        m_Rooms = Business.RoomRatios.RetreiveAll

        For Each _s As Business.Site In Business.Site.RetreiveAll
            RebuildOccupancy(_s._ID.Value, _s._Name, ExcludeWL, ExcludeHols)
        Next

        'build weeks
        BuildWeeks()

        'build months
        BuildMonths()

        Session.HideProgressBar()

    End Sub

    Private Sub RebuildOccupancy(ByVal SiteID As Guid, SiteName As String, ByVal ExcludeWL As Boolean, ByVal ExcludeHols As Boolean)

        Dim _Room As Business.RoomRatios = Nothing

        Dim _SQL As String = ""
        _SQL += "select booking_date, ratio_id"
        _SQL += " from Bookings"
        _SQL += " where site_id = '" + SiteID.ToString + "'"
        _SQL += " group by booking_date, ratio_id"
        _SQL += " order by booking_date"

        Dim _DTPeriods As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DTPeriods IsNot Nothing Then

            Session.SetupProgressBar("Building Occupancy Detail for " + SiteName + "...", _DTPeriods.Rows.Count)

            For Each _DR As DataRow In _DTPeriods.Rows

                _Room = GetRoom(_DR.Item("ratio_id").ToString, m_Rooms)
                If _Room IsNot Nothing Then

                    Dim _LoopDate As Date? = ValueHandler.ConvertDate(_DR.Item("booking_date"))
                    StoreDay(_LoopDate.Value, _Room, m_FTHours, ExcludeWL, ExcludeHols)

                Else
                    'room is not set
                    'this is usually down to misconfiguration etc
                    'i.e. Children with incorrect DOBs or invalid Room Config.
                End If

                Session.StepProgressBar()

            Next

        End If

    End Sub

    Private Shared Sub StoreDay(ByVal DateIn As Date, ByVal RoomRatio As Business.RoomRatios, ByVal FullTimeHours As Decimal, ByVal ExcludeWL As Boolean, ByVal ExcludeHols As Boolean)

        Dim _SQL As String = ""
        _SQL += "select datepart(MONTH, booking_date) as 'month', datepart(WEEK, booking_date) as 'week',"
        _SQL += " count(*) as 'rowcount', isnull(sum(cast(am as int)),0) as 'am', isnull(sum(cast(pm as int)),0) as 'pm',"
        _SQL += " isnull(sum(cast(full_time as int)),0) as 'ft',"
        _SQL += " isnull(sum(hours),0) as 'hours', isnull(sum(funded_hours),0) as 'funded', isnull(sum(tariff_rate),0) as 'revenue'"
        _SQL += " from Bookings"
        _SQL += " where booking_date = " + ValueHandler.SQLDate(DateIn, True)
        _SQL += " and ratio_id = '" + RoomRatio.RatioID.ToString + "'"

        If ExcludeWL Then _SQL += " and booking_status <> 'Waiting'"
        If ExcludeHols Then _SQL += " and booking_status <> 'Holiday'"

        _SQL += " group by booking_date"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then

            Dim _Detail As New Business.OccDetail
            With _Detail

                ._OccDate = DateIn
                ._OccYear = DateIn.Year
                ._OccMonth = ValueHandler.ConvertByte(_DR.Item("month"))
                ._OccWeek = ValueHandler.ConvertByte(_DR.Item("week"))
                ._RoomId = RoomRatio.RatioID

                ._Am = ValueHandler.ConvertInteger(_DR.Item("am"))
                ._Pm = ValueHandler.ConvertInteger(_DR.Item("pm"))

                If ._Am > ._Pm Then
                    ._Headcount = ._Am
                Else
                    ._Headcount = ._Pm
                End If

                ._Occ = CDec(._Headcount / RoomRatio.Capacity) * 100

                ._Hours = ValueHandler.ConvertDecimal(_DR.Item("hours"))
                ._Funded = ValueHandler.ConvertDecimal(_DR.Item("funded"))
                ._Revenue = ValueHandler.ConvertDecimal(_DR.Item("revenue"))

                ._Ft = ValueHandler.ConvertInteger(_DR.Item("ft"))
                ._Fte = CDec(._Hours / FullTimeHours)
                ._Pt = ._Fte - ._Ft

                .Store()

            End With

        End If

    End Sub

    Private Function GetRoom(ByVal RatioID As String, ByVal Rooms As List(Of Business.RoomRatios)) As Business.RoomRatios
        For Each _r In Rooms
            If _r.RatioID.ToString = RatioID Then
                Return _r
            End If
        Next
        Return Nothing
    End Function

    Private Sub BuildMonths()

        'process all the months available in OccDetail

        Dim _SQL As String = ""
        _SQL += "select occ_year, occ_month, room_id from OccDetail"
        _SQL += " group by occ_year, occ_month, room_id"
        _SQL += " order by occ_year, occ_month, room_id"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Building Occupancy by Month...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows
                BuildMonth(_DR.Item("occ_year").ToString, _DR.Item("occ_month").ToString, _DR.Item("room_id").ToString)
                Session.StepProgressBar()
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub BuildWeeks()

        'process all the weeks available in OccDetail

        Dim _SQL As String = ""
        _SQL += "select occ_year, occ_week, room_id from OccDetail"
        _SQL += " group by occ_year, occ_week, room_id"
        _SQL += " order by occ_year, occ_week, room_id"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Building Occupancy by Week...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows
                BuildWeek(_DR.Item("occ_year").ToString, _DR.Item("occ_week").ToString, _DR.Item("room_id").ToString)
                Session.StepProgressBar()
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub BuildWeek(ByVal YearIn As String, ByVal WeekIn As String, ByVal RatioID As String)

        Dim _Room As Business.RoomRatios = GetRoom(RatioID, m_Rooms)
        If _Room Is Nothing Then Exit Sub

        Dim _FirstDate As Date? = Nothing
        Dim _LastDate As Date? = Nothing

        Dim _Days As Integer = 0
        Dim _AM As Integer = 0
        Dim _PM As Integer = 0
        Dim _HeadCount As Integer = 0
        Dim _FT As Integer = 0
        Dim _Hours As Decimal = 0
        Dim _Funded As Decimal = 0
        Dim _Revenue As Decimal = 0

        Dim _WorkingDays As Integer = 0

        Dim _Mondays As Integer = 0
        Dim _MonAM As Integer = 0
        Dim _MonPM As Integer = 0
        Dim _MonHC As Integer = 0
        Dim _MonOcc As Decimal = 0

        Dim _Tuesdays As Integer = 0
        Dim _TueAM As Integer = 0
        Dim _TuePM As Integer = 0
        Dim _TueHC As Integer = 0
        Dim _TueOcc As Decimal = 0

        Dim _Wednesdays As Integer = 0
        Dim _WedAM As Integer = 0
        Dim _WedPM As Integer = 0
        Dim _WedHC As Integer = 0
        Dim _WedOcc As Decimal = 0

        Dim _Thursdays As Integer = 0
        Dim _ThuAM As Integer = 0
        Dim _ThuPM As Integer = 0
        Dim _ThuHC As Integer = 0
        Dim _ThuOcc As Decimal = 0

        Dim _Fridays As Integer = 0
        Dim _FriAM As Integer = 0
        Dim _FriPM As Integer = 0
        Dim _FriHC As Integer = 0
        Dim _FriOcc As Decimal = 0

        Dim _SQL As String = ""
        _SQL += "select occ_date, headcount, am, pm, ft, hours, funded, revenue"
        _SQL += " from OccDetail"
        _SQL += " where occ_year = " + YearIn
        _SQL += " and occ_week = " + WeekIn
        _SQL += " and room_id = '" + RatioID + "'"
        _SQL += " order by occ_date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _Date As Date? = ValueHandler.ConvertDate(_DR.Item("occ_date"))

                _FirstDate = ValueHandler.NearestDate(_Date.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
                _LastDate = ValueHandler.NearestDate(_Date.Value, DayOfWeek.Sunday, ValueHandler.EnumDirection.Forwards)

                Select Case _Date.Value.DayOfWeek

                    Case DayOfWeek.Monday
                        _Mondays += 1
                        _MonAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _MonPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _MonHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _MonOcc = CDec((_MonHC / (_Room.Capacity * _Mondays)) * 100)

                    Case DayOfWeek.Tuesday
                        _Tuesdays += 1
                        _TueAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _TuePM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _TueHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _TueOcc = CDec((_TueHC / (_Room.Capacity * _Tuesdays)) * 100)

                    Case DayOfWeek.Wednesday
                        _Wednesdays += 1
                        _WedAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _WedPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _WedHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _WedOcc = CDec((_WedHC / (_Room.Capacity * _Wednesdays)) * 100)

                    Case DayOfWeek.Thursday
                        _Thursdays += 1
                        _ThuAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _ThuPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _ThuHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _ThuOcc = CDec((_ThuHC / (_Room.Capacity * _Thursdays)) * 100)

                    Case DayOfWeek.Friday
                        _Fridays += 1
                        _FriAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _FriPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _FriHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _FriOcc = CDec((_FriHC / (_Room.Capacity * _Fridays)) * 100)

                End Select

                _AM += ValueHandler.ConvertInteger(_DR.Item("am"))
                _PM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                _HeadCount += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                _FT += ValueHandler.ConvertInteger(_DR.Item("ft"))
                _Hours += ValueHandler.ConvertDecimal(_DR.Item("hours"))
                _Funded += ValueHandler.ConvertDecimal(_DR.Item("funded"))
                _Revenue += ValueHandler.ConvertDecimal(_DR.Item("revenue"))

            Next

            _WorkingDays = _Mondays + _Tuesdays + _Wednesdays + _Thursdays + _Fridays

            Dim _HeadCountForWeek As Integer = _MonHC + _TueHC + _WedHC + _ThuHC + _FriHC
            Dim _CapacityForWeek As Integer = _Room.Capacity * _WorkingDays

            Dim _Occupancy As Decimal = 0
            If _CapacityForWeek > 0 Then
                _Occupancy = CDec((_HeadCount / _CapacityForWeek) * 100)
            Else
                _Occupancy = 0
            End If

            Dim _M As New Business.OccWeek
            With _M

                ._PeriodYear = ValueHandler.ConvertInteger(YearIn)
                ._PeriodWeek = ValueHandler.ConvertByte(WeekIn)
                ._PeriodFrom = _FirstDate
                ._PeriodTo = _LastDate

                ._RoomId = _Room.RatioID
                ._RoomName = _Room.RoomName
                ._RoomCapacity = _Room.Capacity
                ._RoomRatio = _Room.Ratio

                ._MonAm = _MonAM
                ._MonPm = _MonPM
                ._MonHc = _MonHC
                ._MonOcc = _MonOcc

                ._TueAm = _TueAM
                ._TuePm = _TuePM
                ._TueHc = _TueHC
                ._TueOcc = _TueOcc

                ._WedAm = _WedAM
                ._WedPm = _WedPM
                ._WedHc = _WedHC
                ._WedOcc = _WedOcc

                ._ThuAm = _ThuAM
                ._ThuPm = _ThuPM
                ._ThuHc = _ThuHC
                ._ThuOcc = _ThuOcc

                ._FriAm = _FriAM
                ._FriPm = _FriPM
                ._FriHc = _FriHC
                ._FriOcc = _FriOcc

                ._TotalDays = _WorkingDays
                ._TotalCapacity = _CapacityForWeek
                ._TotalHeadcount = _HeadCountForWeek

                ._Occ = _Occupancy
                ._Fte = _Hours / m_FTHours
                ._Ft = _FT
                ._Pt = ._Fte - _FT

                ._Hours = _Hours
                ._Funded = _Funded
                ._Revenue = _Revenue

                .Store()

            End With

        End If

    End Sub

    Private Sub BuildMonth(ByVal YearIn As String, ByVal MonthIn As String, ByVal RatioID As String)

        Dim _Room As Business.RoomRatios = GetRoom(RatioID, m_Rooms)
        If _Room Is Nothing Then Exit Sub

        Dim _FirstDate As Date? = Nothing
        Dim _LastDate As Date? = Nothing

        _FirstDate = ValueHandler.FirstMondayInMonth(CInt(YearIn), CInt(MonthIn))
        _LastDate = ValueHandler.LastFridayInMonth(CInt(YearIn), CInt(MonthIn))

        Dim _Days As Integer = 0
        Dim _AM As Integer = 0
        Dim _PM As Integer = 0
        Dim _HeadCount As Integer = 0
        Dim _FT As Integer = 0
        Dim _Hours As Decimal = 0
        Dim _Funded As Decimal = 0
        Dim _Revenue As Decimal = 0

        Dim _WorkingDays As Integer = 0

        Dim _Mondays As Integer = 0
        Dim _MonAM As Integer = 0
        Dim _MonPM As Integer = 0
        Dim _MonHC As Integer = 0
        Dim _MonOcc As Decimal = 0

        Dim _Tuesdays As Integer = 0
        Dim _TueAM As Integer = 0
        Dim _TuePM As Integer = 0
        Dim _TueHC As Integer = 0
        Dim _TueOcc As Decimal = 0

        Dim _Wednesdays As Integer = 0
        Dim _WedAM As Integer = 0
        Dim _WedPM As Integer = 0
        Dim _WedHC As Integer = 0
        Dim _WedOcc As Decimal = 0

        Dim _Thursdays As Integer = 0
        Dim _ThuAM As Integer = 0
        Dim _ThuPM As Integer = 0
        Dim _ThuHC As Integer = 0
        Dim _ThuOcc As Decimal = 0

        Dim _Fridays As Integer = 0
        Dim _FriAM As Integer = 0
        Dim _FriPM As Integer = 0
        Dim _FriHC As Integer = 0
        Dim _FriOcc As Decimal = 0

        Dim _SQL As String = ""
        _SQL += "select occ_date, headcount, am, pm, ft, hours, funded, revenue"
        _SQL += " from OccDetail"
        _SQL += " where occ_year = " + YearIn
        _SQL += " and occ_month = " + MonthIn
        _SQL += " and room_id = '" + RatioID + "'"
        _SQL += " order by occ_date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _Date As Date? = ValueHandler.ConvertDate(_DR.Item("occ_date"))

                Select Case _Date.Value.DayOfWeek

                    Case DayOfWeek.Monday
                        _Mondays += 1
                        _MonAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _MonPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _MonHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _MonOcc = CDec((_MonHC / (_Room.Capacity * _Mondays)) * 100)

                    Case DayOfWeek.Tuesday
                        _Tuesdays += 1
                        _TueAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _TuePM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _TueHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _TueOcc = CDec((_TueHC / (_Room.Capacity * _Tuesdays)) * 100)

                    Case DayOfWeek.Wednesday
                        _Wednesdays += 1
                        _WedAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _WedPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _WedHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _WedOcc = CDec((_WedHC / (_Room.Capacity * _Wednesdays)) * 100)

                    Case DayOfWeek.Thursday
                        _Thursdays += 1
                        _ThuAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _ThuPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _ThuHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _ThuOcc = CDec((_ThuHC / (_Room.Capacity * _Thursdays)) * 100)

                    Case DayOfWeek.Friday
                        _Fridays += 1
                        _FriAM += ValueHandler.ConvertInteger(_DR.Item("am"))
                        _FriPM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                        _FriHC += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                        _FriOcc = CDec((_FriHC / (_Room.Capacity * _Fridays)) * 100)

                End Select

                _AM += ValueHandler.ConvertInteger(_DR.Item("am"))
                _PM += ValueHandler.ConvertInteger(_DR.Item("pm"))
                _HeadCount += ValueHandler.ConvertInteger(_DR.Item("headcount"))
                _FT += ValueHandler.ConvertInteger(_DR.Item("ft"))
                _Hours += ValueHandler.ConvertDecimal(_DR.Item("hours"))
                _Funded += ValueHandler.ConvertDecimal(_DR.Item("funded"))
                _Revenue += ValueHandler.ConvertDecimal(_DR.Item("revenue"))

            Next

            _WorkingDays = _Mondays + _Tuesdays + _Wednesdays + _Thursdays + _Fridays

            Dim _HeadCountForMonth As Integer = _MonHC + _TueHC + _WedHC + _ThuHC + _FriHC
            Dim _CapacityForMonth As Integer = _Room.Capacity * _WorkingDays

            Dim _Occupancy As Decimal = 0
            If _CapacityForMonth > 0 Then
                _Occupancy = CDec((_HeadCount / _CapacityForMonth) * 100)
            Else
                _Occupancy = 0
            End If

            Dim _M As New Business.OccMonth
            With _M

                ._PeriodYear = ValueHandler.ConvertInteger(YearIn)
                ._PeriodMonth = ValueHandler.ConvertByte(MonthIn)

                ._PeriodFrom = _FirstDate
                ._PeriodTo = _LastDate

                ._RoomId = _Room.RatioID
                ._RoomName = _Room.RoomName
                ._RoomCapacity = _Room.Capacity
                ._RoomRatio = _Room.Ratio

                ._Mondays = _Mondays
                ._MonAm = _MonAM
                ._MonPm = _MonPM
                ._MonHc = _MonHC
                ._MonOcc = _MonOcc

                ._Tuesdays = _Tuesdays
                ._TueAm = _TueAM
                ._TuePm = _TuePM
                ._TueHc = _TueHC
                ._TueOcc = _TueOcc

                ._Wednesdays = _Wednesdays
                ._WedAm = _WedAM
                ._WedPm = _WedPM
                ._WedHc = _WedHC
                ._WedOcc = _WedOcc

                ._Thursdays = _Thursdays
                ._ThuAm = _ThuAM
                ._ThuPm = _ThuPM
                ._ThuHc = _ThuHC
                ._ThuOcc = _ThuOcc

                ._Fridays = _Fridays
                ._FriAm = _FriAM
                ._FriPm = _FriPM
                ._FriHc = _FriHC
                ._FriOcc = _FriOcc

                ._TotalDays = _WorkingDays
                ._TotalCapacity = _CapacityForMonth
                ._TotalHeadcount = _HeadCountForMonth

                ._Occ = _Occupancy
                ._Fte = _Hours / m_FTHours
                ._Ft = _FT
                ._Pt = ._Fte - _FT

                ._Hours = _Hours
                ._Funded = _Funded
                ._Revenue = _Revenue

                .Store()

            End With

        End If

    End Sub

End Class
