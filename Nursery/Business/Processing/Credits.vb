﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class Credits

    Public Shared Function LastBatchNo() As Integer
        Dim _SQL As String = "select max(batch_no) as 'batch_no' from CreditBatch"
        Dim _DBNo As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return _DBNo
    End Function

    Public Shared Function LastCreditNo() As Integer

        Dim _SQL As String = ""

        _SQL += "select max(c.credit_no) as 'credit_no' from Credits c"
        _SQL += " left join CreditBatch b on b.ID = c.batch_id"
        _SQL += " where b.batch_status = 'Posted'"

        Dim _DBNo As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        'if there are no invoices, we need to take from parameters
        If _DBNo <= 0 Then
            _DBNo = ParameterHandler.ReturnInteger("NEXTCREDIT", False)
        End If

        Return _DBNo

    End Function

    Public Shared Function PostBatch(ByVal CreditBatchID As Guid, ByVal SiteID As Guid) As Boolean

        Dim _Return As Boolean = False
        Dim _frm As New frmFinancialsPost(CreditBatchID, frmFinancialsPost.EnumBatchType.Credits, SiteID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then _Return = True

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Function AmendCredit(CreditID As Guid) As Boolean

        Dim _Amended As Boolean = False
        Dim _frmCredit As New frmCredit(frmCredit.EnumMode.AmendCreditNote, CreditID, Nothing)
        _frmCredit.ShowDialog()

        If _frmCredit.DialogResult = DialogResult.OK Then _Amended = True

        _frmCredit.Dispose()
        _frmCredit = Nothing

        Return _Amended

    End Function

    Public Shared Sub ViewCredit(CreditID As Guid)

        Dim _Mode As frmCredit.EnumMode = frmCredit.EnumMode.ViewCreditNote

        Dim _frmCredit As New frmCredit(_Mode, CreditID, Nothing)
        _frmCredit.ShowDialog()

        _frmCredit.Dispose()
        _frmCredit = Nothing

    End Sub

    Public Shared Sub DeleteCredit(CreditID As String)


    End Sub

    Public Shared Function GenerateOneLineCredit(ByVal BatchID As Guid, ByVal BatchNo As Integer, ByVal CreditNo As Integer, ByVal FamilyID As Guid, ByVal ChildID As Guid?,
                                                 ByVal CreditDate As Date, ByVal LineDescription As String, ByVal LineValue As Decimal) As Guid

        Dim _Return As Guid = Nothing
        Dim _Family As Business.Family = Business.Family.RetreiveByID(FamilyID)
        If _Family IsNot Nothing Then

            Dim _CreditID As Guid = Guid.NewGuid

            Dim _ChildRecord As Business.Child = Nothing
            Dim _ChildName As String = ""
            Dim _Class As String = ""
            Dim _NLCode As String = FinanceShared.DefaultNLCode 'use default NL code
            Dim _NLTracking As String = ""

            If ChildID.HasValue Then
                _ChildRecord = Business.Child.RetreiveByID(ChildID.Value)
                _ChildName = _ChildRecord._Fullname
                _Class = _ChildRecord._Class
                _NLCode = Invoicing.ReturnNLCode(_ChildRecord, "")
                _NLTracking = Invoicing.ReturnNLTracking(_ChildRecord, "")
            Else
                'no child, so we default from the family site
                If _Family._SiteId.HasValue Then
                    Dim _S As Business.Site = Business.Site.RetreiveByID(_Family._SiteId.Value)
                    If _S IsNot Nothing Then
                        _NLCode = _S._NlCode
                        _NLTracking = _S._NlTracking
                    End If
                End If
            End If

            If LineDescription <> "" Then
                'CreateLine(_InvoiceID, 1, "2", InvoiceDate, LineDescription, FamilyID.ToString, "", "", _NLCode, _NLTracking, 0, LineValue, 0)
            End If

            'increment the credit number
            CreditNo += 1

            'create an credit note
            Dim _Inv As New Business.Credit
            With _Inv

                ._ID = _CreditID
                ._BatchId = BatchID
                ._CreditNo = CreditNo
                ._CreditStatus = "Generated"
                ._CreditDate = CreditDate
                ._CreditDue = CreditDate

                ._ChildId = ChildID
                ._ChildName = _ChildName

                ._FamilyId = _Family._ID
                ._FamilyName = _Family._LetterName
                ._FamilyAccountNo = _Family._AccountNo
                ._Address = _Family._Address

                ._CreditTotal = LineValue

                'external id (the customer ID in the financials application)
                ._FinancialsId = _Family._FinancialsId

                .Store()

                'return the CreditID
                _Return = _CreditID

            End With

            _Inv = Nothing

        End If

        _Family = Nothing

        Return _Return

    End Function

    Public Shared Function CreateFinancialsDocumentFromCreditID(ByVal OurDocumentID As Guid, ByRef Document As Care.Financials.Document) As Integer

        Dim _TrackingCategory As String = FinanceShared.TrackingCategory

        'fetch our document
        Dim _Doc As Business.Credit = Business.Credit.RetreiveByID(OurDocumentID)
        If _Doc Is Nothing Then Return -1

        'external ID is not on the document...
        If _Doc._FinancialsId = "" Then Return -2

        Dim _ExtDocument As New Care.Financials.Document
        With _ExtDocument
            .DateCreated = _Doc._CreditDate.Value
            .DateDue = _Doc._CreditDue.Value
            .EntityID = _Doc._FinancialsId
            .DocumentType = Care.Financials.Document.EnumDocumentType.SalesCreditNote
            .Reference = _Doc._CreditNo.ToString
            .Description = _Doc._ChildName
        End With

        Dim _ExtItems As New List(Of Care.Financials.DocumentItem)
        For Each _Line As Business.CreditLine In Business.CreditLine.RetreiveAllByCreditID(_Doc._ID.Value)

            'check we have a nominal code
            If _Line._NlCode = "" Then Return -3

            'check we have tracking (if enabled)
            If FinanceShared.TrackingCategory <> "" AndAlso _Line._NlTracking = "" Then Return -4

            Dim _FIL As New Care.Financials.DocumentItem
            With _FIL
                .Description = Format(_Line._ActionDate, "dd/MM/yyyy") + " - " + _Line._Description
                .NLCode = _Line._NlCode
                .NLTrackingCategory = _TrackingCategory
                .NLTracking = _Line._NlTracking
                .Quantity = 1
                .UnitPrice = _Line._Value
                .VATAmount = 0
            End With

            _ExtItems.Add(_FIL)

        Next

        'add the lines to the document
        _ExtDocument.Items = _ExtItems

        Document = _ExtDocument
        Return 0

    End Function

    Public Shared Sub UpdateCreditStatus(ByVal CreditID As String, ByVal CreditStatus As String, Optional ByVal ExternalCreditID As String = "")

        Dim _C As Business.Credit = Business.Credit.RetreiveByID(New Guid(CreditID))
        If _C IsNot Nothing Then

            Select Case CreditStatus

                Case "Printed"
                    'if we want to mark this invoice as printed, check is has not already been marked as emailed...
                    If _C._CreditStatus = "Emailed" Then
                        'leave it alone
                    Else
                        _C._CreditStatus = CreditStatus
                    End If

                Case Else
                    _C._CreditStatus = CreditStatus

            End Select

            If ExternalCreditID <> "" Then _C._FinancialsDocId = ExternalCreditID

            _C.Store()
            _C = Nothing

        End If

    End Sub

End Class
