﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class FinancialUtilities

        Private m_Eng As New Business.InvoicingEngine

#Region "Public Method Calls"

        Public Sub BuildInvoiceDays()
            Session.ConnectionManager.HaltAuditting()
            GenerateInvoiceDays()
            Session.ConnectionManager.ResumeAuditting()
        End Sub

        Public Sub BuildForecastDataForThisYear()
            Session.ConnectionManager.HaltAuditting()
            ClearForecastData(Today.Year)
            ProcessForecastYear(Today.Year)
            Session.ConnectionManager.ResumeAuditting()
        End Sub

        Public Sub BuildForecastData(ByVal Year As Integer)
            Session.ConnectionManager.HaltAuditting()
            ClearForecastData(Year)
            ProcessForecastYear(Year)
            Session.ConnectionManager.ResumeAuditting()
        End Sub

        Public Function UpdateBalances() As Integer
            Return FetchBalances()
        End Function

        Public Sub BuildTurnoverData()
            Session.ConnectionManager.HaltAuditting()
            ClearTurnoverData()
            ProcessTurnoverData()
            Session.ConnectionManager.ResumeAuditting()
        End Sub

#End Region

#Region "InvoiceDays"

        Private Sub GenerateInvoiceDays()

            Dim _SQL As String = ""
            _SQL += "select i.ID, c.forename, c.surname from Invoices i"
            _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
            _SQL += " left join Children c on c.ID = i.child_id"
            _SQL += " where b.batch_status <> 'Abandoned'"
            _SQL += " order by b.batch_no, i.invoice_no"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Session.SetupProgressBar("Generating Invoice Days...", _DT.Rows.Count)

                For Each _DR As DataRow In _DT.Rows

                    Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
                    Dim _Forename As String = _DR.Item("forename").ToString
                    Dim _Surname As String = _DR.Item("surname").ToString

                    Invoicing.GenerateInvoiceDays(_ID, Invoicing.EnumAnnualised.None, Nothing, _Forename, _Surname)

                    Session.StepProgressBar()

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End Sub

#End Region

#Region "Turnover"

        Private Sub ClearTurnoverData()
            Dim _SQL As String = "delete from Turnover"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

        Private Sub ProcessTurnoverData()

            Dim _SQL As String = ""
            _SQL += "select year(invoice_date), sum(invoice_sub) as 'invoice_sub', sum(invoice_sub) as 'invoice_sub'"
            _SQL += " from Invoices i"
            _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
            _SQL += " where b.batch_status = 'Posted'"
            _SQL += " order by b.batch_no, i.invoice_no"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Session.SetupProgressBar("Generating Turnover...", _DT.Rows.Count)
                For Each _DR As DataRow In _DT.Rows







                    Session.StepProgressBar()

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End Sub

#End Region

#Region "Financials Integration"

        Private Function FetchBalances() As Integer

            For Each _s In Business.Site.RetreiveAll

                Dim _FI As Care.Financials.IntegrationDetail = FinanceShared.ReturnFinancialsIntegration(_s._ID.Value)
                If _FI IsNot Nothing Then

                    Dim _Eng As New Care.Financials.Engine(_FI)
                    If _Eng IsNot Nothing Then

                        Dim _Families As List(Of Business.Family) = Business.Family.RetrieveCurrentFamilies(_s._ID.Value)

                        For Each _F As Business.Family In _Families

                            If _F._ExclFinancials = False AndAlso _F._FinancialsId <> "" Then

                                Dim _Balance As Decimal = 0
                                Dim _R As Care.Financials.Response = _Eng.GetBalance(_F._FinancialsId, _Balance)
                                If _R.Success Then
                                    _F._Balance = _Balance
                                    _F.Store()
                                End If

                                'if we are using xero we must wait 1.5 second between each call
                                'xero has a maximum of 60 calls into the API per minute
                                If Session.FinancialSystem.ToUpper = "XERO" Then
                                    Threading.Thread.Sleep(1500)
                                End If

                            End If

                        Next

                        _Eng.Disconnect()

                    End If

                End If

                Threading.Thread.Sleep(3000)

            Next

            Return 0

        End Function

#End Region

#Region "Forecasting"

        Private Sub ClearForecastData(ByVal YearIn As Integer)

            Dim _SQL As String = ""

            'fetch all forecasted batches
            _SQL = ""
            _SQL += "select ID from InvoiceBatch"
            _SQL += " where year(batch_date) = " + YearIn.ToString
            _SQL += " and batch_period = 'Forecasting'"
            _SQL += " order by batch_date"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _BatchID As String = _DR.Item("ID").ToString

                    'delete invoices
                    _SQL = "delete from Invoices where batch_id = '" + _BatchID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    'delete invoice days
                    _SQL = "delete from Invoices where invoice_batch = '" + _BatchID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    'delete the batch itself
                    _SQL = "delete from InvoiceBatch where ID = '" + _BatchID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Next

            End If

            'delete orphaned invoice lines
            _SQL = "delete from InvoiceLines where invoice_id not in (select ID from Invoices)"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            'forecasting table
            _SQL = "delete from Forecasting where fc_year = " + YearIn.ToString
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Private Sub ProcessForecastYear(ByVal Year As Integer)

            For Each _S As Business.Site In Business.Site.RetreiveAll

                For _iMonth = 1 To 12

                    Dim _BatchID As Guid = Guid.NewGuid

                    Dim _F As New Business.Forecasting
                    With _F
                        ._SiteId = _S._ID
                        ._SiteName = _S._Name
                        ._FcYear = Year
                        ._FcMonth = _iMonth
                        ._InvFrom = DateSerial(Year, _iMonth, 1)
                        ._InvTo = DateSerial(Year, _iMonth, Date.DaysInMonth(Year, _iMonth))
                        ._BatchId = _BatchID
                    End With

                    'go off and build an invoice batch for this month
                    CreateForecastBatch(_BatchID, _S._ID.Value, _S._Name, _F._InvFrom.Value, _F._InvTo.Value)

                    Dim _SQL As String = ""

                    _SQL += "select sum(hours_non_funded) as 'hours_non_funded', sum(value_non_funded) as 'value_non_funded',"
                    _SQL += " sum(hours_funded) As 'hours_funded', sum(value_funded) as 'value_funded', sum(value_boltons) as 'bolt-ons'"
                    _SQL += " from InvoiceDays"
                    _SQL += " where invoice_batch = '" + _BatchID.ToString + "'"

                    Dim _DR1 As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                    If _DR1 IsNot Nothing Then
                        _F._NonFundedHours = ValueHandler.ConvertDecimal(_DR1.Item("hours_non_funded"))
                        _F._NonFundedValue = ValueHandler.ConvertDecimal(_DR1.Item("value_non_funded"))
                        _F._FundedHours = ValueHandler.ConvertDecimal(_DR1.Item("hours_funded"))
                        _F._FundedValue = ValueHandler.ConvertDecimal(_DR1.Item("value_funded"))
                    End If

                    _SQL = ""
                    _SQL += "select count(*) as 'count', sum(invoice_sub) as 'sub', sum(invoice_discount) as 'discount', sum(invoice_total) as 'total'"
                    _SQL += " from Invoices"
                    _SQL += " where batch_id = '" + _BatchID.ToString + "'"

                    Dim _DR2 As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                    If _DR2 IsNot Nothing Then
                        _F._InvoiceCount = ValueHandler.ConvertInteger(_DR2.Item("count"))
                        _F._InvoiceSub = ValueHandler.ConvertDecimal(_DR2.Item("sub"))
                        _F._InvoiceDiscount = ValueHandler.ConvertDecimal(_DR2.Item("discount"))
                        _F._InvoiceTotal = ValueHandler.ConvertDecimal(_DR2.Item("total"))
                        _F._TotalHours = _F._NonFundedHours + _F._FundedHours
                    End If

                    _F.Store()

                Next

            Next

        End Sub

        Private Function CreateForecastBatch(ByVal BatchID As Guid, ByVal SiteID As Guid, ByVal SiteName As String, ByVal FromDate As Date, ByVal ToDate As Date) As Boolean

            Dim _B As New Business.InvoiceBatch
            With _B

                ._ID = BatchID
                ._BatchNo = Invoicing.LastBatchNo + 1

                ._BatchStatus = "Abandoned"
                ._BatchPeriod = "Forecasting"

                ._InvFirst = Invoicing.LastInvoiceNo + 1

                ._SiteId = SiteID
                ._SiteName = SiteName

                'invoice dates
                ._DateFirst = FromDate
                ._DateLast = ToDate
                ._BatchDate = FromDate

                ._Comments = "Forecast Batch"
                ._Stamp = Now

                .Store()

            End With

            Return m_Eng.Run(BatchID)

        End Function

#End Region

    End Class

End Namespace

