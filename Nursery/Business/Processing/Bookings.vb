﻿

Imports Care.Shared
Imports Care.Global
Imports Care.Data

Public Class Bookings

    Private Shared m_CutOff As Date? = Nothing
    Private Shared m_CalendarTable As List(Of Business.Calendar) = Business.Calendar.RetreiveAll
    Private Shared m_TermTable As List(Of Business.Term) = Business.Term.RetreiveAll

    Public Enum EnumDateCategory
        TermDate
        HalfTerm
        NotTerm
    End Enum

    Public Enum EnumCheckMode
        Bookings
        Invoicing
    End Enum

    Public Enum EnumExcluded
        NotExcluded
        GlobalClosed
        GlobalNoCharge
        ChildOnHoliday
        SchoolHolidays
    End Enum

    Private Shared ReadOnly Property CalendarDates As List(Of Business.Calendar)
        Get
            Return m_CalendarTable
        End Get
    End Property

    Private Shared ReadOnly Property TermDates As List(Of Business.Term)
        Get
            Return m_TermTable
        End Get
    End Property

    Public Shared ReadOnly Property CalendarCutOff As Date
        Get
            If Not m_CutOff.HasValue Then m_CutOff = GetCutOffDate()
            Return m_CutOff.Value
        End Get
    End Property

    Private Shared Function GetCutOffDate() As Date

        Dim _CutOff As Date? = Nothing

        'check the parameters
        If ParameterHandler.CheckExists("CALENDARCUTOFF") Then
            _CutOff = ParameterHandler.ReturnDate("CALENDARCUTOFF")
        Else
            ParameterHandler.Create("CALENDARCUTOFF", ParameterHandler.EnumParameterType.DateType, "Bookings", "Future Bookings Cut Off", Format(_CutOff, "dd/MM/yyyy"))
        End If

        If Not _CutOff.HasValue Then _CutOff = DateSerial(Today.Year + 1, 12, 31)

        Return _CutOff.Value

    End Function

    Public Shared Sub BookingsDrillDown(ByVal BookingDate As Date, ByVal SiteID As Guid?, ByVal RoomID As Guid?, ByVal FormCaption As String)

        Session.CursorWaiting()

        Dim _frmDrillDown As New frmBookingsDrillDown(BookingDate, SiteID, RoomID)
        _frmDrillDown.Text = FormCaption
        _frmDrillDown.ShowDialog()

        _frmDrillDown.Dispose()

    End Sub

    Public Shared Function IsClosed(ByVal ActionDate As Date) As Boolean

        Dim _Q As IEnumerable(Of Business.Calendar) = From _C As Business.Calendar In m_CalendarTable _
                                                      Where _C._Date = ActionDate

        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                If _Q.First._Closed Then
                    Return True
                End If
            End If
        End If

        Return False

    End Function

    Public Shared Function IsDateExcluded(ByVal ActionDate As Date, ByVal Child As Business.Child, ByRef Holidays As List(Of Business.ChildHoliday), ByVal CheckMode As EnumCheckMode, ByVal DateCategory As EnumDateCategory) As Bookings.EnumExcluded

        'global calendar check
        '*******************************************************************************************************************************

        Dim _Q As IEnumerable(Of Business.Calendar) = From _C As Business.Calendar In m_CalendarTable _
                                                      Where _C._Date = ActionDate

        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                If CheckMode = EnumCheckMode.Bookings Then
                    If _Q.First._Closed Then
                        Return EnumExcluded.GlobalClosed
                    End If
                Else
                    If _Q.First._NoCharge Then
                        Return EnumExcluded.GlobalNoCharge
                    End If
                End If
            End If
        End If

        'holiday?
        '*******************************************************************************************************************************
        If Holidays IsNot Nothing AndAlso Holidays.Count > 0 Then

            Dim _CHQ As IEnumerable(Of Business.ChildHoliday) = From _H As Business.ChildHoliday In Holidays _
                                                                Where ActionDate >= _H._FromDate And ActionDate <= _H._ToDate

            If _CHQ IsNot Nothing Then
                If _CHQ.Count = 1 Then
                    Return EnumExcluded.ChildOnHoliday
                End If
            End If

        End If

        'term time only?
        '*******************************************************************************************************************************

        If Child._TermOnly Then
            If DateCategory <> EnumDateCategory.TermDate Then
                Return EnumExcluded.SchoolHolidays
            End If
        End If

        Return EnumExcluded.NotExcluded

    End Function

    Public Shared Function ReturnDateCategory(ByVal DateIn As Date, ByVal ChildTermType As String) As EnumDateCategory

        Dim _Return As EnumDateCategory = EnumDateCategory.NotTerm
        Dim _SQL As String = ""
        Dim _DR As DataRow = Nothing

        If ChildTermType = "" Then ChildTermType = "School Term"

        Dim _Q As IEnumerable(Of Business.Term) = From _T As Business.Term In m_TermTable _
                                                  Where _T._TermType = ChildTermType _
                                                  And DateIn >= _T._TermStart _
                                                  And DateIn <= _T._TermEnd

        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then

                'we have fallen inside a term

                Dim _HT As IEnumerable(Of Business.Term) = From _T As Business.Term In m_TermTable _
                                                           Where _T._TermType = ChildTermType _
                                                           And DateIn >= _T._HtStart _
                                                           And DateIn <= _T._HtEnd

                If _HT IsNot Nothing Then
                    If _HT.Count = 1 Then
                        'we have fallen inside half-term
                        _Return = EnumDateCategory.HalfTerm
                    Else
                        _Return = EnumDateCategory.TermDate
                    End If
                End If

            End If

        End If

        Return _Return

    End Function

    Public Shared Function ReturnSchoolLeaveDate(ByVal DOB As Date?, ByVal SchoolLeaveMonths As Integer) As Date?

        If DOB.HasValue Then

            'add the months onto the DOB
            Dim _Return As Date? = DOB.Value.AddMonths(SchoolLeaveMonths)
            Dim _Year As Integer = _Return.Value.Year

            Select Case _Return.Value.Month

                Case 9, 10, 11, 12
                    _Year += 1

            End Select

            Return DateSerial(_Year, 8, 31)

        Else
            Return Nothing
        End If

    End Function

End Class
