﻿Imports Care.Global
Imports Care.Data

Public Class FundingEngine

    Public Enum EnumConsolidation
        PerDay
        PerWeek
        PerMonth
        PerPeriod
    End Enum

    Public Shared Sub CreateDefaultLA()

        'check if a la exits, if not, we create one and default the current school terms...

        Dim _SQL As String = "select top 1 * from LA"
        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR Is Nothing Then

            Dim _ID As Guid = Guid.NewGuid

            'For Each _T In Business.Term.RetreiveAll
            '    _T._LaId = _ID
            '    _T.Store()
            'Next

            Dim _LA As New Business.LA
            With _LA
                ._ID = _ID
                ._Name = "Default Local Authority"
                .Store()
            End With

        End If

    End Sub

    Public Shared Sub BuildFundingData(ByVal LocalAuthID As Guid, ByVal FromDate As Date, ByVal ToDate As Date, ByVal ConsolidationMethod As EnumConsolidation)

        Dim _LA As Business.LA = Business.LA.RetreiveByID(LocalAuthID)
        If _LA IsNot Nothing Then

            Dim _BatchID As Guid = Guid.NewGuid

            Dim _from As String = ValueHandler.SQLDate(FromDate, True)
            Dim _to As String = ValueHandler.SQLDate(ToDate, True)

            'fetch all the dates inbetween the dates specified
            Dim _SQL As String = "select d.invoice_date, d.child_id, c.dob, hours_funded from InvoiceDays d"
            _SQL += " left join InvoiceBatch b on b.ID = d.invoice_batch"
            _SQL += " left join Invoices i on i.ID = d.invoice_id"
            _SQL += " left join Children c On C.ID = d.child_id"
            _SQL += " left join Terms t On t.term_type = c.term_type"
            _SQL += " where t.la_id = '" + LocalAuthID.ToString + "'"
            _SQL += " and d.invoice_date between " + _from + " and " + _to
            _SQL += " and d.hours_funded > 0"
            _SQL += " and b.batch_status = 'Posted'"
            _SQL += " and i.invoice_status = 'Posted'"
            _SQL += " and b.batch_period <> 'Annual Invoice'"
            _SQL += " order by d.invoice_date"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _FD As New Business.FundDay

                    _FD._BatchId = _BatchID
                    _FD._ChildId = ValueHandler.ConvertGUID(_DR.Item("child_id").ToString)
                    _FD._ActionDate = ValueHandler.ConvertDate(_DR.Item("action_date"))
                    _FD._FundHrs = ValueHandler.ConvertDecimal(_DR.Item("hours"))

                    _FD.Store()

                Next

            End If

        End If


















    End Sub






End Class
