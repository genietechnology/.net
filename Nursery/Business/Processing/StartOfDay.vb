﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Namespace Business

    Public Class StartOfDay

        Public Event Progress(ByVal sender As Object, ByVal Progress As ProgressEvent)

        Private m_DefaultActivityText As String = ""

        Public Function Run() As Integer

            Dim _Status As Integer = 0

            Try

                If Not DayExists() Then

                    m_DefaultActivityText = ParameterHandler.ReturnString("ACTIVITYTEXT")

                    RaiseEvent Progress(Me, New ProgressEvent("Creating Day Records"))
                    CreateDayRecords()

                    RaiseEvent Progress(Me, New ProgressEvent("Processing Starters"))
                    Child.ProcessStarters(Date.Today)

                    RaiseEvent Progress(Me, New ProgressEvent("Processing Leavers"))
                    Child.ProcessLeavers(Date.Today)

                    RaiseEvent Progress(Me, New ProgressEvent("Processing Archived"))
                    Family.ProcessArchived(Date.Today)

                    RaiseEvent Progress(Me, New ProgressEvent("Performing Auto-Moves"))
                    PerformAutoMoves()

                    RaiseEvent Progress(Me, New ProgressEvent("Update Waiting to Booked"))
                    UpdateWaitingToBooked()

                    RaiseEvent Progress(Me, New ProgressEvent("Rebuilding Day Patterns"))
                    Child.BuildDayPatternsForEveryone(Today)

                    RaiseEvent Progress(Me, New ProgressEvent("Processing Staff Statistics"))
                    UpdateStaffStatistics(Today)

                    RaiseEvent Progress(Me, New ProgressEvent("Processing Touchscreen Absence"))
                    UpdateAbsence()

                    RaiseEvent Progress(Me, New ProgressEvent("Housekeeping"))
                    DoHouseKeeping()

                Else
                    _Status = 1
                End If

            Catch ex As Exception
                _Status = -1
            End Try

            Return _Status

        End Function

        Private Sub UpdateAbsence()

            Dim _SQL As String = ""
            _SQL += "select ID, stamp, key_id, staff_id, staff_name, description, notes from Activity"
            _SQL += " where type = 'ABSENCE'"
            _SQL += " and ID not in (select abs_activity from ChildAbsence)"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _a As New ChildAbsence
                    _a._ChildId = New Guid(_DR.Item("key_id").ToString)
                    _a._AbsType = "Absence"
                    _a._AbsSource = "Touchscreen"
                    _a._AbsFrom = ValueHandler.ConvertDate(_DR.Item("stamp")).Value.Date
                    _a._AbsTo = ValueHandler.ConvertDate(_DR.Item("stamp")).Value.Date
                    _a._AbsReason = _DR.Item("description").ToString
                    _a._AbsNotes = _DR.Item("notes").ToString
                    _a._AbsActivity = New Guid(_DR.Item("ID").ToString)
                    _a._AbsStaffId = New Guid(_DR.Item("staff_id").ToString)
                    _a._AbsStaffName = _DR.Item("staff_name").ToString
                    _a._AbsStamp = ValueHandler.ConvertDate(_DR.Item("stamp"))
                    _a.Store()

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End Sub


        Private Sub UpdateStaffStatistics(ByVal DateIn As Date)
            'fetch all the current staff
            For Each _s In Business.Staff.RetrieveLiveStaff
                _s.UpdateStatistics()
                _s.Store()
            Next
        End Sub

        Private Sub ProcessRepeatingMedications(dayID As Guid, siteID As Guid)

            Dim hideNames As Boolean = False
            Dim sql As String = ""

            sql = String.Concat("Select * from TabletParams",
                                " where device = 'MASTER'",
                                " and param_id = 'HIDENAMES'")

            Dim table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)
            If table IsNot Nothing Then
                For Each row As DataRow In table.Rows
                    hideNames = ValueHandler.ConvertBoolean(row.Item("param_value").ToString)
                Next
            End If

            For Each auth In MedicineAuth.RetreiveRepeating(siteID)

                For Each dose In Split(auth._DosagesDue, ", ")

                    Dim medicineLog As New MedicineLog
                    Dim child As Child = Child.RetreiveByID(auth._ChildId.Value)

                    With medicineLog
                        ._ID = Guid.NewGuid
                        ._DayId = dayID
                        ._AuthId = auth._ID
                        ._Due = Date.Parse(Today & " " & dose)
                        ._ChildId = auth._ChildId
                        ._ChildName = auth._ChildName
                        ._Illness = auth._Illness
                        ._Medicine = auth._Medicine
                        ._Dosage = auth._Dosage
                        ._Button = IIf(hideNames, child._Knownas, child._Forename).ToString & " @ " & dose
                        .Store()
                    End With

                Next
            Next

        End Sub

        Private Sub DoHouseKeeping()

            Dim sql As String = ""

            'remove bookings for deleted children
            sql = "delete from Bookings where child_id not in (select id from Children)"
            DAL.ExecuteSQL(Session.ConnectionString, sql)

            'clear today_notes for all children
            sql = "update children set today_notes = null"
            DAL.ExecuteSQL(Session.ConnectionString, sql)

        End Sub

        Private Function ChildInList(ByRef ListofIDs As List(Of Guid), ByVal ChildID As Guid) As Boolean
            For Each _ID In ListofIDs
                If _ID = ChildID Then Return True
            Next
            Return False
        End Function

        Private Sub UpdateWaitingToBooked()

            'keep a list of children requiring a bookings rebuild
            Dim _ChildrenRequiringBookingsBuild As New List(Of Guid)

            'loop through all the Waiting Sessions that need to be marked as booked
            For Each _s In Business.ChildAdvanceSession.RetrieveWaitingToBooked

                _s._BookingStatus = "Booked"
                _s.Store()

                If Not ChildInList(_ChildrenRequiringBookingsBuild, _s._ChildId.Value) Then
                    _ChildrenRequiringBookingsBuild.Add(_s._ChildId.Value)
                End If

            Next

            'now rebuild booking for the children
            For Each _ID In _ChildrenRequiringBookingsBuild
                Business.Child.BuildBookings(_ID)
            Next

        End Sub

        Private Sub PerformAutoMoves()

            Dim _RoomRatios As List(Of Business.RoomRatios) = Business.RoomRatios.RetreiveByAge
            If _RoomRatios IsNot Nothing AndAlso _RoomRatios.Count > 0 Then

                Dim _Movers As List(Of Business.Child) = Business.Child.RetrieveMovers
                If _Movers IsNot Nothing Then

                    For Each _C In Business.Child.RetrieveMovers

                        If _C._Dob.HasValue Then

                            Dim _Months As Long = ValueHandler.ReturnExactMonths(_C._Dob.Value, Today)

                            If _C._MoveMode = "Auto" Then

                                Dim _Q = From _RR As Business.RoomRatios In _RoomRatios
                                         Where _RR.SiteID = _C._SiteId _
                                         And _RR.AgeFrom <= _Months _
                                         And _RR.AgeTo >= _Months _
                                         And _RR.RoomID <> _C._GroupId.Value
                                         Order By _RR.AgeFrom, _RR.Sequence, _RR.RatioID

                                If _Q IsNot Nothing AndAlso _Q.Count > 0 Then

                                    Dim _RR As Business.RoomRatios = _Q.First

                                    CreateChildActivity(_C._ID.Value, _C._Fullname, "MOVEAUTO", _C._GroupId.Value, _C._GroupName, _RR.RoomID.Value, _RR.RoomName)

                                    _C._GroupId = _RR.RoomID
                                    _C._GroupName = _RR.RoomName
                                    _C._GroupStarted = Today

                                    _C._MoveDate = Nothing
                                    _C._MoveRatioId = Nothing

                                    _C.Store()

                                End If

                            Else

                                If _C._MoveDate <= Today Then

                                    Dim _Q = From _RR As Business.RoomRatios In _RoomRatios
                                             Where _RR.RatioID = _C._MoveRatioId.Value _
                                             And _RR.RoomID <> _C._GroupId.Value

                                    If _Q IsNot Nothing AndAlso _Q.Count > 0 Then

                                        Dim _RR As Business.RoomRatios = _Q.First

                                        CreateChildActivity(_C._ID.Value, _C._Fullname, "MOVEMANUAL", _C._GroupId.Value, _C._GroupName, _RR.RoomID.Value, _RR.RoomName)

                                        _C._GroupId = _RR.RoomID
                                        _C._GroupName = _RR.RoomName
                                        _C._GroupStarted = Today

                                        _C._MoveMode = ParameterHandler.ReturnString("DEFMOVEMODE")
                                        _C._MoveDate = Nothing
                                        _C._MoveRatioId = Nothing

                                        _C.Store()

                                    End If

                                End If

                            End If

                        End If

                    Next

                Else
                    'no children on auto or manual
                End If

            Else
                'no room ratios
            End If

        End Sub

        Private Sub CreateChildActivity(ByVal ChildID As Guid, ByVal ChildName As String, ByVal ActivityType As String, OldRoomID As Guid, OldRoomName As String, NewRoomID As Guid, NewRoomName As String)

            Dim _A As New Business.Activity
            _A._KeyId = ChildID
            _A._KeyName = ChildName
            _A._Type = ActivityType

            If ActivityType = "MOVEAUTO" Then
                _A._Description = "Auto Move Performed: " + OldRoomName + " > " + NewRoomName
            Else
                _A._Description = "Manual Move Performed: " + OldRoomName + " > " + NewRoomName
            End If

            _A._Value1 = OldRoomID.ToString
            _A._Value2 = ActivityType
            _A._Stamp = Now

            _A.Store()

            _A = Nothing


        End Sub

        Private Function DayExists() As Boolean
            Dim _Day As Business.Day = Business.Day.RetreiveByDateOnly(Date.Today)
            If _Day IsNot Nothing Then
                If _Day.IsNew Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End Function

        Private Sub CreateDayRecords()
            For Each _S In Business.Site.RetreiveAll
                CreateDay(_S._ID.Value, _S._Name)
            Next
        End Sub

        Private Sub CreateDay(ByVal SiteID As Guid, ByVal SiteName As String)

            Dim _D As New Business.Day
            With _D

                'create the day
                ._Date = Today
                ._SiteId = SiteID
                ._SiteName = SiteName

                'check if we are using the rolling menus
                If ParameterHandler.ReturnBoolean("ROLLINGMENU") Then

                    Dim _SQL As String = ""
                    Dim _SeasonID As String = ""
                    Dim _WeekID As String = ""
                    Dim _WeekNo As Integer = ParameterHandler.ReturnInteger("ROLLINGMENUWEEK", False)
                    Dim _MaxWeeks As Integer = 0
                    Dim _DayOfWeek As Integer = ReturnDayNumber(Today)

                    'return the season based upon the current date
                    _SQL = "select top 1 ID, weeks from MenuSeason where date_from <= " + ValueHandler.SQLDate(Today, True) + " order by date_from desc"
                    Dim _DRSeason As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                    If _DRSeason IsNot Nothing Then

                        _SeasonID = _DRSeason.Item("ID").ToString
                        _MaxWeeks = ValueHandler.ConvertInteger(_DRSeason.Item("weeks"))

                        Dim _LastRoll As Date? = Nothing

                        'fetch the last monday we rolled the week from parameters
                        If ParameterHandler.CheckExists("ROLLINGLASTMONDAY") Then
                            _LastRoll = ParameterHandler.ReturnDate("ROLLINGLASTMONDAY")
                            If Not _LastRoll.HasValue Then
                                _LastRoll = ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
                                ParameterHandler.SetDate("ROLLINGLASTMONDAY", _LastRoll)
                            End If
                        Else
                            _LastRoll = ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
                            ParameterHandler.Create("ROLLINGLASTMONDAY", ParameterHandler.EnumParameterType.DateType, "Rolling Menus", "Last Monday Rolled", _LastRoll.ToString)
                        End If

                        'if its 7 or more days since the last roll, we roll now and update the last monday
                        If DateDiff(DateInterval.Day, _LastRoll.Value, Today) > 6 Then

                            'increment the week
                            _WeekNo += 1

                            'reset to week one if we are over the max
                            If _WeekNo >= _MaxWeeks Then _WeekNo = 1

                            ParameterHandler.SetString("ROLLINGMENUWEEK", _WeekNo.ToString)
                            ParameterHandler.SetDate("ROLLINGLASTMONDAY", ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards))

                        End If

                        _SQL = "select ID from MenuWeek where season_id = '" + _SeasonID + "' and week = " + _WeekNo.ToString
                        Dim _DRWeek As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                        If _DRWeek IsNot Nothing Then

                            _WeekID = _DRWeek.Item("ID").ToString

                            'fetch the menu using the day of the week
                            _SQL = "select ID from MenuDay where season_id = '" + _SeasonID + "' and week_id = '" + _WeekID + "' and day = " + _DayOfWeek.ToString
                            Dim _DRDay As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                            If _DRDay IsNot Nothing Then

                                Dim _M As Business.MenuDay = Business.MenuDay.RetreiveByID(New Guid(_DRDay.Item("ID").ToString))
                                If _M IsNot Nothing Then

                                    ._BreakfastId = _M._Breakfast
                                    ._BreakfastName = _M._BreakfastName
                                    ._SnackId = _M._SnackAm
                                    ._SnackName = _M._SnackAmName
                                    ._LunchId = _M._Lunch
                                    ._LunchName = _M._LunchName
                                    ._LunchDesId = _M._LunchDessert
                                    ._LunchDesName = _M._LunchDessertName
                                    ._SnackPmId = _M._SnackPm
                                    ._SnackPmName = _M._SnackPmName
                                    ._TeaId = _M._Tea
                                    ._TeaName = _M._TeaName
                                    ._TeaDesId = _M._TeaDessert
                                    ._TeaDesName = _M._TeaDessertName

                                End If

                            End If

                        End If

                    End If

                End If

                .Store()

                'set the group messages
                CreateBlankActivity(SiteID, ._ID.Value)

                'create repeating medication log records
                ProcessRepeatingMedications(_D._ID.Value, SiteID)

            End With

        End Sub

        Private Function ReturnDayNumber(ByVal DateIn As Date) As Integer
            Return DateAndTime.Weekday(DateIn, FirstDayOfWeek.Monday) - 1
        End Function

        Private Sub CreateBlankActivity(ByVal SiteID As Guid, ByVal DayID As Guid)

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, RoomRatios.RetreiveSiteRoomSQL(SiteID.ToString))
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _RoomName As String = _DR("room_name").ToString
                    Dim _A As New Business.DayActivity

                    _A._DayId = DayID
                    _A._GroupId = New Guid(_DR.Item("ID").ToString)

                    If m_DefaultActivityText = "" Then
                        _A._ActivityText = "Today in the " + _RoomName + " we..."
                    Else
                        _A._ActivityText = m_DefaultActivityText
                        _A._ActivityText = _A._ActivityText.Replace("<GroupName>", _RoomName)
                    End If

                    _A.Store()

                Next

            End If

        End Sub

    End Class

    Public Class ProgressEvent

        Inherits EventArgs

        Public Sub New(ByVal EventText As String)
            ProgressText = EventText
        End Sub

        Public Property ProgressText As String

    End Class

End Namespace
