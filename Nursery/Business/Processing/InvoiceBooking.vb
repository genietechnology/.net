﻿

Imports Care.Global
Imports Care.Shared

Namespace Processing

    Public Class InvoiceBooking

        Public Enum EnumExtractMode
            RecurringAndOverride
            RecurringOnly
            AdditionalOnly
        End Enum

        Public Enum EnumSingleMulti
            SingleSessionMode
            MultiSessionMode
        End Enum

        Private Shared Function ExtractBookingsNew(ByRef BookingDays As List(Of BookingDay), ByRef Sessions As List(Of Business.ChildAdvanceSession), ByVal WC As Date, ByVal ActionDate As Date, _
                                                   ByVal DateCategory As Bookings.EnumDateCategory, ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As Boolean

            Dim _Session As Business.ChildAdvanceSession = Nothing

            'check for a multi-session first
            If ReturnSession(Sessions, WC, ActionDate, DateCategory, EnumSingleMulti.MultiSessionMode, PatternType, _Session) Then
                BookingDays.AddRange(ExtractBookingsFromMultiSession(_Session, ActionDate, ActionDate, PatternType))
                Return True
            End If

            'OK, so we didn't find a multi-session, let's try single session
            If ReturnSession(Sessions, WC, ActionDate, DateCategory, EnumSingleMulti.SingleSessionMode, PatternType, _Session) Then
                BookingDays.AddRange(ExtractBookingsFromSingleSession(_Session, ActionDate, ActionDate, PatternType))
                Return True
            End If

            Return False

        End Function

        Public Shared Function ReturnSession(ByVal Sessions As List(Of Business.ChildAdvanceSession), ByVal WC As Date, ByVal ActionDate As Date, _
                                             ByVal DateCategory As Bookings.EnumDateCategory, ByVal Mode As EnumSingleMulti, ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType,
                                             ByRef Session As Business.ChildAdvanceSession) As Boolean

            '**********************************************************************************************************************************************************************
            'if we are checking a recurring pattern, we need to take the scope into consideration
            'i.e. all the time, holidays only etc
            '**********************************************************************************************************************************************************************
            If PatternType = Business.ChildAdvanceSession.EnumPatternType.Recurring Then

                If Mode = EnumSingleMulti.SingleSessionMode Then

                    If DateCategory = Bookings.EnumDateCategory.TermDate Then

                        Dim _QS As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                                  Where _S._DateFrom <= WC And _S._Mode = "Weekly" And _S._PatternType = PatternType.ToString _
                                                                                  And (_S._RecurringScope = 1 Or _S._RecurringScope = 2) _
                                                                                  Order By _S._DateFrom Descending

                        If _QS IsNot Nothing AndAlso _QS.Count > 0 Then
                            Session = _QS.First
                            Return True
                        End If

                    Else

                        Dim _QS As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                                  Where _S._DateFrom <= WC And _S._Mode = "Weekly" And _S._PatternType = PatternType.ToString _
                                                                                  And (_S._RecurringScope = 1 Or _S._RecurringScope = 3) _
                                                                                  Order By _S._DateFrom Descending

                        If _QS IsNot Nothing AndAlso _QS.Count > 0 Then
                            Session = _QS.First
                            Return True
                        End If

                    End If

                Else

                    'multi-session
                    If DateCategory = Bookings.EnumDateCategory.TermDate Then

                        Dim _QM As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                                  Where _S._DateFrom <= ActionDate And _S._Mode = "Daily" And _S._PatternType = PatternType.ToString _
                                                                                  And _S._DateFrom.Value.DayOfWeek = ActionDate.DayOfWeek _
                                                                                  And (_S._RecurringScope = 1 Or _S._RecurringScope = 2) _
                                                                                  Order By _S._DateFrom Descending


                        If _QM IsNot Nothing AndAlso _QM.Count > 0 Then
                            Session = _QM.First
                            Return True
                        End If

                    Else

                        Dim _QM As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                                  Where _S._DateFrom <= ActionDate And _S._Mode = "Daily" And _S._PatternType = PatternType.ToString _
                                                                                  And _S._DateFrom.Value.DayOfWeek = ActionDate.DayOfWeek _
                                                                                  And (_S._RecurringScope = 1 Or _S._RecurringScope = 3) _
                                                                                  Order By _S._DateFrom Descending

                        If _QM IsNot Nothing AndAlso _QM.Count > 0 Then
                            Session = _QM.First
                            Return True
                        End If

                    End If

                End If

            Else

                '**********************************************************************************************************************************************************************

                'override or additional

                If Mode = EnumSingleMulti.SingleSessionMode Then

                    Dim _QS As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                              Where _S._DateFrom = WC And _S._Mode = "Weekly" And _S._PatternType = PatternType.ToString _
                                                                              Order By _S._DateFrom Descending

                    If _QS IsNot Nothing AndAlso _QS.Count > 0 Then
                        Session = _QS.First
                        Return True
                    End If

                Else

                    Dim _QM As IEnumerable(Of Business.ChildAdvanceSession) = From _S As Business.ChildAdvanceSession In Sessions
                                                                              Where _S._DateFrom = ActionDate And _S._Mode = "Daily" And _S._PatternType = PatternType.ToString _
                                                                              Order By _S._DateFrom Descending

                    If _QM IsNot Nothing AndAlso _QM.Count > 0 Then
                        Session = _QM.First
                        Return True
                    End If

                End If

                '**********************************************************************************************************************************************************************

            End If

            'return empty handed
            Session = Nothing
            Return False

        End Function

        Public Shared Function ReturnBookingsNew(ByVal Child As Business.Child, ByVal StartDate As Date, ByVal EndDate As Date, ByVal AdditionalSessionMode As Invoicing.EnumAdditionalSessionMode, _
                                                 ByVal AdditionalFrom As Date?, ByVal AdditionalTo As Date?) As List(Of BookingDay)

            Dim _Sessions As List(Of Business.ChildAdvanceSession) = Business.ChildAdvanceSession.RetrieveByChildID(Child._ID.Value)

            Dim _RecurringBookings As New List(Of BookingDay)
            Dim _OverrideBookings As New List(Of BookingDay)
            Dim _AdditionalBookings As New List(Of BookingDay)

            Dim _Date As Date = StartDate
            Do While _Date <= EndDate

                If AdditionalSessionMode <> Invoicing.EnumAdditionalSessionMode.Only Then

                    Dim _DateCategory As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(_Date, Child._TermType)
                    Dim _WC As Date = ValueHandler.NearestDate(_Date, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

                    'check for an override session
                    If ExtractBookingsNew(_OverrideBookings, _Sessions, _WC, _Date, _DateCategory, Business.ChildAdvanceSession.EnumPatternType.Override) Then
                        'we use this override session
                    Else
                        'now check for a recurring session
                        ExtractBookingsNew(_RecurringBookings, _Sessions, _WC, _Date, _DateCategory, Business.ChildAdvanceSession.EnumPatternType.Recurring)
                    End If

                Else
                    Exit Do
                End If

                _Date = _Date.AddDays(1)

            Loop

            'process addtional sessions, based upon the additional session date range (which might be the same as the invoice date range)
            If AdditionalSessionMode <> Invoicing.EnumAdditionalSessionMode.Excluded Then

                Dim _AdditionalFrom As Date = StartDate
                Dim _AdditionalTo As Date = EndDate

                If AdditionalFrom.HasValue Then _AdditionalFrom = AdditionalFrom.Value
                If AdditionalTo.HasValue Then _AdditionalTo = AdditionalTo.Value

                _Date = _AdditionalFrom
                While _Date <= AdditionalTo

                    Dim _DateCategory As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(_Date, Child._TermType)
                    Dim _WC As Date = ValueHandler.NearestDate(_Date, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

                    ExtractBookingsNew(_AdditionalBookings, _Sessions, _WC, _Date, _DateCategory, Business.ChildAdvanceSession.EnumPatternType.Additional)

                    _Date = _Date.AddDays(1)

                End While

            End If

            Dim _Return As New List(Of BookingDay)
            _Return.AddRange(_RecurringBookings)
            _Return.AddRange(_OverrideBookings)
            _Return.AddRange(_AdditionalBookings)

            Return _Return

        End Function

        Private Shared Function ExtractBookingsFromSingleSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date,
                                           ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            If PatternType = Business.ChildAdvanceSession.EnumPatternType.Recurring Then
                Return ExtractRecurringBookingsFromSingleSession(SessionRecord, ActionDate, LastDate, PatternType)
            Else
                Return ExtractNonRecurringBookingsFromSingleSession(SessionRecord, ActionDate, LastDate, PatternType)
            End If

        End Function

        Private Shared Function ExtractBookingsFromMultiSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date, _
                                           ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            If PatternType = Business.ChildAdvanceSession.EnumPatternType.Recurring Then
                Return ExtractRecurringBookingsFromMultiSession(SessionRecord, ActionDate, LastDate, PatternType)
            Else
                Return ExtractNonRecurringBookingsFromMultiSession(SessionRecord, ActionDate, LastDate, PatternType)
            End If

        End Function

        Private Shared Function ExtractRecurringBookingsFromSingleSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date, _
                                   ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            Dim _Bookings As New List(Of BookingDay)

            If ActionDate.DayOfWeek = DayOfWeek.Monday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Tuesday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Wednesday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Thursday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Friday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Saturday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            If PatternType <> Business.ChildAdvanceSession.EnumPatternType.Recurring Then ActionDate = ActionDate.AddDays(1)

            If ActionDate.DayOfWeek = DayOfWeek.Sunday Then
                If ActionDate <= LastDate Then
                    _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
                End If
            End If

            Return _Bookings

        End Function

        Private Shared Function ExtractRecurringBookingsFromMultiSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date, _
                                           ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            Dim _Bookings As New List(Of BookingDay)

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Monday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Tuesday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Wednesday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Thursday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Friday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Saturday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Sunday, SessionRecord))
            End If

            Return _Bookings

        End Function

        Private Shared Function ExtractNonRecurringBookingsFromSingleSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date, _
                                   ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            Dim _Bookings As New List(Of BookingDay)
            _Bookings.Add(New BookingDay(ActionDate, ActionDate.DayOfWeek, SessionRecord))
            Return _Bookings

        End Function

        Private Shared Function ExtractNonRecurringBookingsFromMultiSession(ByVal SessionRecord As Business.ChildAdvanceSession, ByVal ActionDate As Date, ByVal LastDate As Date, _
                                           ByVal PatternType As Business.ChildAdvanceSession.EnumPatternType) As List(Of BookingDay)

            Dim _Bookings As New List(Of BookingDay)

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Monday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Tuesday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Wednesday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Thursday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Friday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Saturday, SessionRecord))
            End If

            If ActionDate <= LastDate Then
                _Bookings.Add(New BookingDay(ActionDate, DayOfWeek.Sunday, SessionRecord))
            End If

            Return _Bookings

        End Function

    End Class

End Namespace

