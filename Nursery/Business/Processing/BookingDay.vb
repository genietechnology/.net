﻿Namespace Processing

    Public Class BookingDay

        Public Sub New(ByVal ActionDateIn As Date, ByVal ActionDayOfWeek As DayOfWeek, ByRef SessionRecordIn As Business.ChildAdvanceSession)

            ActionDate = ActionDateIn
            ActionDay = ActionDateIn.DayOfWeek
            SessionID = SessionRecordIn._ID.Value
            SessionMode = SessionRecordIn._Mode
            PatternType = SessionRecordIn._PatternType

            Select Case ActionDayOfWeek

                Case DayOfWeek.Monday
                    If SessionRecordIn._Monday.HasValue Then
                        TariffID = SessionRecordIn._Monday.Value
                        TariffName = SessionRecordIn._MondayDesc
                        Value1 = SessionRecordIn._Monday1
                        Value2 = SessionRecordIn._Monday2
                        FundedHours = SessionRecordIn._MondayFund
                        BoltOns = SessionRecordIn._MondayBoid
                    End If

                Case DayOfWeek.Tuesday
                    If SessionRecordIn._Tuesday.HasValue Then
                        TariffID = SessionRecordIn._Tuesday.Value
                        TariffName = SessionRecordIn._TuesdayDesc
                        Value1 = SessionRecordIn._Tuesday1
                        Value2 = SessionRecordIn._Tuesday2
                        FundedHours = SessionRecordIn._TuesdayFund
                        BoltOns = SessionRecordIn._TuesdayBoid
                    End If

                Case DayOfWeek.Wednesday
                    If SessionRecordIn._Wednesday.HasValue Then
                        TariffID = SessionRecordIn._Wednesday.Value
                        TariffName = SessionRecordIn._WednesdayDesc
                        Value1 = SessionRecordIn._Wednesday1
                        Value2 = SessionRecordIn._Wednesday2
                        FundedHours = SessionRecordIn._WednesdayFund
                        BoltOns = SessionRecordIn._WednesdayBoid
                    End If

                Case DayOfWeek.Thursday
                    If SessionRecordIn._Thursday.HasValue Then
                        TariffID = SessionRecordIn._Thursday.Value
                        TariffName = SessionRecordIn._ThursdayDesc
                        Value1 = SessionRecordIn._Thursday1
                        Value2 = SessionRecordIn._Thursday2
                        FundedHours = SessionRecordIn._ThursdayFund
                        BoltOns = SessionRecordIn._ThursdayBoid
                    End If

                Case DayOfWeek.Friday
                    If SessionRecordIn._Friday.HasValue Then
                        TariffID = SessionRecordIn._Friday.Value
                        TariffName = SessionRecordIn._FridayDesc
                        Value1 = SessionRecordIn._Friday1
                        Value2 = SessionRecordIn._Friday2
                        FundedHours = SessionRecordIn._FridayFund
                        BoltOns = SessionRecordIn._FridayBoid
                    End If

                Case DayOfWeek.Saturday
                    If SessionRecordIn._Saturday.HasValue Then
                        TariffID = SessionRecordIn._Saturday.Value
                        TariffName = SessionRecordIn._SaturdayDesc
                        Value1 = SessionRecordIn._Saturday1
                        Value2 = SessionRecordIn._Saturday2
                        FundedHours = SessionRecordIn._SaturdayFund
                        BoltOns = SessionRecordIn._SaturdayBoid
                    End If

                Case DayOfWeek.Sunday
                    If SessionRecordIn._Sunday.HasValue Then
                        TariffID = SessionRecordIn._Sunday.Value
                        TariffName = SessionRecordIn._SundayDesc
                        Value1 = SessionRecordIn._Sunday1
                        Value2 = SessionRecordIn._Sunday2
                        FundedHours = SessionRecordIn._SundayFund
                        BoltOns = SessionRecordIn._SundayBoid
                    End If

            End Select

        End Sub

        Public Property ActionDate As Date
        Public Property ActionDay As DayOfWeek

        Public Property SessionID As Guid
        Public Property SessionMode As String

        Public Property TariffID As Guid? 'tariff might be blank on purpose
        Public Property TariffName As String
        Public Property Value1 As Decimal
        Public Property Value2 As Decimal
        Public Property FundedHours As Decimal

        Public Property BoltOns As String

        Public Property PatternType As String

    End Class

End Namespace
