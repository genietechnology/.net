﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class SessionDataObject

#Region "Variables"

    Private m_IsPopulated As Boolean = False
    Private m_Hours As Decimal = 0
    Private m_Rate As Decimal = 0
    Private m_Total As Decimal = 0
    Private m_Value1 As Decimal = 0
    Private m_Value2 As Decimal = 0
    Private m_FEEEHours As Decimal = 0
    Private m_FEEEDescription As String = ""
    Private m_Description As String = ""
    Private m_Section As String = ""
    Private m_TariffID As Guid? = Nothing
    Private m_NLCode As String = ""
    Private m_NLTracking As String = ""
    Private m_BoltOns As New List(Of BoltOn)
    Private m_DiscountPermitted As Boolean = False
    Private m_HourlyRateCalculation As Boolean = False

    Private m_FEEEValue As Decimal = 0
    Private m_NonFundedRate As Decimal = 0
    Private m_FundedRate As Decimal = 0
    Private m_BoltOnsValue As Decimal = 0

    Private m_SummaryColumn As String = ""

#End Region

#Region "Properties"

    Public ReadOnly Property IsPopulated As Boolean
        Get
            Return m_IsPopulated
        End Get
    End Property

    Public ReadOnly Property TariffID As Guid?
        Get
            Return m_TariffID
        End Get
    End Property

    Public ReadOnly Property Description As String
        Get
            Return m_Description
        End Get
    End Property

    Public ReadOnly Property Hours As Decimal
        Get
            Return m_Hours
        End Get
    End Property

    Public ReadOnly Property Section As String
        Get
            Return m_Section
        End Get
    End Property

    Public ReadOnly Property Rate As Decimal
        Get
            Return m_Rate
        End Get
    End Property

    Public ReadOnly Property TotalValue As Decimal
        Get
            Return m_Total
        End Get
    End Property

    Public ReadOnly Property Value_1 As Decimal
        Get
            Return m_Value1
        End Get
    End Property

    Public ReadOnly Property Value_2 As Decimal
        Get
            Return m_Value2
        End Get
    End Property

    Public ReadOnly Property FEEE As Decimal
        Get
            Return m_FEEEHours
        End Get
    End Property

    Public ReadOnly Property FEEEDescription As String
        Get
            Return m_FEEEDescription
        End Get
    End Property

    Public ReadOnly Property NLCode As String
        Get
            Return m_NLCode
        End Get
    End Property

    Public ReadOnly Property NLTracking As String
        Get
            Return m_NLTracking
        End Get
    End Property

    Public ReadOnly Property BoltOns As List(Of BoltOn)
        Get
            Return m_BoltOns
        End Get
    End Property

    Public ReadOnly Property DiscountPermitted As Boolean
        Get
            Return m_DiscountPermitted
        End Get
    End Property

    Public ReadOnly Property FEEEValue As Decimal
        Get
            Return m_FEEEValue
        End Get
    End Property

    Public ReadOnly Property NonFundedRate As Decimal
        Get
            Return m_NonFundedRate
        End Get
    End Property

    Public ReadOnly Property FundedRate As Decimal
        Get
            Return m_FundedRate
        End Get
    End Property

    Public ReadOnly Property BoltOnsValue As Decimal
        Get
            Return m_BoltOnsValue
        End Get
    End Property

    Public ReadOnly Property SummaryColumn As String
        Get
            Return m_SummaryColumn
        End Get
    End Property

#End Region

    Public Sub New(ByVal Booking As Processing.BookingDay, ByVal ChildRecord As Business.Child, ByVal TariffFilter As String, ByVal Holidays As List(Of Business.ChildHoliday))

        If Booking Is Nothing Then Exit Sub

        'blank tariff
        If Not Booking.TariffID.HasValue Then Exit Sub

        '**************************************************************************************************************************************************************

        'tariff filtering
        If TariffFilter <> "" Then
            If Not TariffFilter.ToUpper.Contains(Booking.TariffID.ToString.ToUpper) Then
                Exit Sub
            End If
        End If

        '**************************************************************************************************************************************************************

        m_HourlyRateCalculation = False
        m_Value1 = Booking.Value1
        m_Value2 = Booking.Value2
        m_FEEEHours = Booking.FundedHours

        '**************************************************************************************************************************************************************

        Dim _Tariff As Business.Tariff = Business.Tariff.RetreiveByID(Booking.TariffID.Value)

        If _Tariff IsNot Nothing Then

            m_TariffID = _Tariff._ID

            m_Description = _Tariff._InvoiceText
            m_Hours = _Tariff._Duration
            m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, _Tariff._Rate, Holidays)
            m_Total = m_Rate
            m_Section = "2"

            m_NLCode = Invoicing.ReturnNLCode(ChildRecord, _Tariff._NlCode)
            m_NLTracking = Invoicing.ReturnNLTracking(ChildRecord, _Tariff._NlTracking)

            m_DiscountPermitted = _Tariff._Discount

            Select Case _Tariff._Type

                Case "Automatic"
                    'fetch times from register etc

                Case "Complimentary"

                Case "Funded"
                    m_Section = "1"

                Case "Fixed Price"
                    If _Tariff._Override Then
                        m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, CDec(m_Value1), Holidays)
                        m_Total = m_Rate
                    End If

                Case "Duration (Hours)"

                    If _Tariff._Override Then
                        m_Hours = m_Value1
                    End If

                    'hours x rate
                    m_HourlyRateCalculation = True
                    m_Total = m_Hours * m_Rate
                    m_Description = m_Hours.ToString + " hours @ " + Format(m_Rate, "0.00")

                Case "Monthly Average"

                Case "Manual Times", "Time Matrix"

                    m_HourlyRateCalculation = True

                    'always overridden, so no need to check flag

                    'value 1 and 2 are times
                    Dim _From As Date? = ValueHandler.ReturnTime(m_Value1)
                    Dim _To As Date? = ValueHandler.ReturnTime(m_Value2)

                    If _From.HasValue And _To.HasValue Then

                        Dim _Mins As Integer = 0
                        Dim _PerMin As Decimal = m_Rate / 60

                        If _To > _From Then

                            _Mins = CInt(DateDiff(DateInterval.Minute, _From.Value, _To.Value))
                            m_Hours = CDec(_Mins / 60)
                            Dim _HoursAndMinutes As String = ValueHandler.ReturnHoursAndMinutes(_Mins)

                            If _Tariff._VariableRate Then
                                m_Total = CalculateFromVariableRate(ChildRecord._ID.Value, Booking.ActionDate, ChildRecord._TermType, _Tariff, _From, _To, m_FEEEHours, Holidays)
                                m_Description = Format(_From, "HH:mm") + " - " + Format(_To, "HH:mm") + " = " + _HoursAndMinutes + " @ " + _Tariff._InvoiceText
                            Else

                                If _Tariff._Type = "Manual Times" Then
                                    m_Total = _Mins * _PerMin
                                    m_Description = Format(_From, "HH:mm") + " - " + Format(_To, "HH:mm") + " = " + _HoursAndMinutes + " @ " + Format(m_Rate, "0.00") + " per hour"
                                Else

                                    'time matrix

                                    'calculate the age of the child in months based upon the action date...
                                    Dim _Months As Long = ValueHandler.ReturnExactMonths(ChildRecord._Dob.Value, Booking.ActionDate)

                                    Dim _m As Business.TariffMatrix = Business.TariffMatrix.RetrieveByClassAndAge(TariffID.Value, ChildRecord._Class, _Months)
                                    If _m IsNot Nothing Then
                                        m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, _m._Value, Holidays)
                                        _PerMin = m_Rate / 60
                                        m_Total = _Mins * _PerMin
                                        m_Description = Format(_From, "HH:mm") + " - " + Format(_To, "HH:mm") + " = " + _HoursAndMinutes + " @ " + Format(m_Rate, "0.00") + " per hour"
                                    Else
                                        m_Total = 0
                                        m_Description = "Time Matrix record not found..."
                                    End If

                                End If

                            End If

                        Else
                            m_Hours = 0
                            m_Total = 0
                            m_Description = "Invalid Times entered"
                        End If

                    Else
                        m_Hours = 0
                        m_Total = 0
                        m_Description = "Invalid Times entered"
                    End If

                Case "Group Matrix"

                    Dim bookings As List(Of Business.Booking) = Business.Booking.RetreiveByChildID(New Guid(ChildRecord._ID.ToString))
                    If bookings IsNot Nothing Then

                        Dim query = From qBookings As Business.Booking In bookings
                                    Where qBookings._BookingDate = Booking.ActionDate

                        If query IsNot Nothing AndAlso query.Count > 0 Then

                            Dim actualBooking As Business.Booking = query.First
                            Dim roomRatio As Business.RoomRatios = Business.RoomRatios.RetreiveByID(New Guid(actualBooking._RatioId.ToString))
                            If roomRatio IsNot Nothing Then
                                Dim tariffMatrix As Business.TariffMatrix = Business.TariffMatrix.RetrieveByClassAndGroup(TariffID.Value, ChildRecord._Class, roomRatio.RoomID.Value)
                                If tariffMatrix IsNot Nothing Then
                                    m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, tariffMatrix._Value, Holidays)
                                    m_Total = m_Rate
                                    m_Description = tariffMatrix._InvoiceText
                                End If
                            End If

                        End If
                    End If

                Case "Age Matrix", "Age Matrix (Times)"

                    'calculate the age of the child in months based upon the action date...
                    Dim _Months As Long = ValueHandler.ReturnExactMonths(ChildRecord._Dob.Value, Booking.ActionDate)

                    Dim _m As Business.TariffMatrix = Business.TariffMatrix.RetrieveByClassAndAge(TariffID.Value, ChildRecord._Class, _Months)
                    If _m IsNot Nothing Then
                        m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, _m._Value, Holidays)
                        m_Total = m_Rate
                        m_Description = _m._InvoiceText
                    End If

                Case "Age Matrix (Rate)"

                    'calculate the age of the child in months based upon the action date...
                    Dim _Months As Long = ValueHandler.ReturnExactMonths(ChildRecord._Dob.Value, Booking.ActionDate)

                    Dim _m As Business.TariffMatrix = Business.TariffMatrix.RetrieveByClassAndAge(TariffID.Value, ChildRecord._Class, _Months)
                    If _m IsNot Nothing Then
                        m_Rate = SetRate(ChildRecord._ID.Value, Booking.ActionDate, _m._Value, Holidays)
                        m_Total = m_Rate
                        m_Description = _m._InvoiceText
                    End If

                    If _Tariff._Override Then
                        m_Hours = m_Value1
                    End If

                    'hours x rate
                    m_HourlyRateCalculation = True
                    m_Total = m_Hours * m_Rate
                    m_Description = m_Hours.ToString + " hours @ " + Format(m_Rate, "0.00")

            End Select

            '**************************************************************************************************************************************************************

            Dim _DateCategory As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(Booking.ActionDate, ChildRecord._TermType)

            m_FEEEValue = 0
            m_NonFundedRate = 0
            m_FundedRate = 0

            '**************************************************************************************************************************************************************

            'apply FEEE funding where applicable
            If m_FEEEHours > 0 AndAlso m_Hours > 0 Then

                If _DateCategory = Bookings.EnumDateCategory.TermDate Then

                    m_FEEEDescription = Format(m_FEEEHours, "0.00") + " hours FEEE"
                    m_Description += " (less " + m_FEEEDescription + ")"

                    If _Tariff._VariableRate Then
                        'if we are using variable rates, we have already taken the FEEE hours out...
                    Else

                        Dim _Rate As Decimal = m_Rate
                        Dim _Duration As Decimal = m_Hours

                        Dim _NonFundedRate As Decimal = 0

                        If m_HourlyRateCalculation Then
                            _NonFundedRate = _Rate
                        Else
                            'if we are not dealing with an hourly rate, we need to divide the rate by the hours...
                            _NonFundedRate = _Rate / _Duration
                        End If

                        Dim _NonFundedValue As Decimal = m_Hours * _NonFundedRate

                        Dim _FundedRate As Decimal = ReturnFEEERecoveryRate(_Tariff, ChildRecord._Dob.Value, Booking.ActionDate, _NonFundedRate)
                        Dim _FEEEValue As Decimal = m_FEEEHours * _FundedRate

                        'check if the funded value is greater than the normal session value
                        'this can happen with 2 year olds - for example:
                        'child attends 10 hour day, lets say that normally costs £40, if they are in receipt of 2 Year Old funding @ £6.00 per hour
                        'this can lead to the child getting a credit if the sessions are configured as having 10 hours of funding for example.

                        'we therefore check the calculated funded value against the amount before we take funding off.

                        If _FEEEValue > m_Total Then
                            'funding amount is more than the non-funded, so we make them the same so they calculate to £0.00, not producing a credit
                            _FEEEValue = _NonFundedValue
                        End If

                        m_FEEEValue = _FEEEValue
                        m_NonFundedRate = _NonFundedRate
                        m_FundedRate = _FundedRate

                        m_Total = _NonFundedValue - _FEEEValue

                    End If

                Else
                    'clear the FEEE hours as they are N/A
                    m_FEEEHours = 0
                End If

            End If

            '**************************************************************************************************************************************************************

            'extract the bolt-ons
            m_BoltOnsValue = 0
            m_BoltOns.Clear()
            If Booking.BoltOns <> "" Then
                ProcessBoltOns(ChildRecord, Booking.BoltOns, _DateCategory)
            End If

            '**************************************************************************************************************************************************************

            'apply summary column
            If m_Section = "1" Then
                m_SummaryColumn = "0"
            Else
                m_SummaryColumn = _Tariff._SummaryColumn
            End If

            '**************************************************************************************************************************************************************

            m_IsPopulated = True

            '**************************************************************************************************************************************************************

        End If

    End Sub

    Private Function ReturnFEEERecoveryRate(ByVal Tariff As Business.Tariff, ByVal DOB As Date, ByVal CheckDate As Date, ByVal HourlyRate As Decimal) As Decimal

        Dim _Under3 As Boolean = ChildUnder3(DOB, CheckDate)
        If _Under3 Then
            If Tariff._RateFund24 > 0 Then
                Return Tariff._RateFund24
            Else
                Return HourlyRate
            End If
        Else
            If Tariff._RateFund36 > 0 Then
                Return Tariff._RateFund36
            Else
                Return HourlyRate
            End If
        End If

    End Function

    Private Function ChildUnder3(ByVal DOB As Date, ByVal CheckDate As Date) As Boolean
        Dim _Months As Long = ValueHandler.ReturnExactMonths(DOB, CheckDate)
        If _Months < 36 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SetRate(ByVal ChildID As Guid, ByVal ActionDate As Date, ByVal NewRate As Decimal, ByVal Holidays As List(Of Business.ChildHoliday)) As Decimal

        Dim _Return As Decimal = NewRate

        'check if we need to apply a holiday discount
        Dim _HolidayDiscount As Decimal = CheckForHolidayDiscount(ChildID, ActionDate, Holidays)
        If _HolidayDiscount > 0 Then
            Dim _DiscountValue As Decimal = NewRate / 100 * _HolidayDiscount
            _Return = NewRate - _DiscountValue
        End If

        Return _Return

    End Function

    Private Function CheckForHolidayDiscount(ByVal ChildID As Guid?, ByVal ActionDate As Date, ByVal Holidays As List(Of Business.ChildHoliday)) As Decimal

        If Holidays IsNot Nothing Then
            If Holidays.Count > 0 Then

                Dim _Q As IEnumerable(Of Business.ChildHoliday) = Nothing

                _Q = From _H As Business.ChildHoliday In Holidays _
                     Where ActionDate >= _H._FromDate And ActionDate <= _H._ToDate _

                If _Q IsNot Nothing Then
                    If _Q.Count > 0 Then
                        Return _Q.First._Discount
                    End If
                End If

            End If
        End If

        Return 0

        'Dim _SQL As String = ""
        '_SQL += "select discount from ChildHolidays"
        '_SQL += " where child_id = '" + ChildID.Value.ToString + "'"
        '_SQL += " and '" + ValueHandler.SQLDate(ActionDate) + "' between from_date and to_date"

        'Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        'If _DR IsNot Nothing Then
        '    Return CleanData.ReturnDecimal(_DR.Item("discount"))
        '    _DR = Nothing
        'Else
        '    Return 0
        'End If

    End Function

    Private Function CalculateFromVariableRate(ByVal ChildID As Guid, ByVal ActionDate As Date, ByVal TermType As String, _
                                               ByRef Tariff As Business.Tariff, _
                                               ByVal StartTime As Date?, ByVal EndTime As Date?, _
                                               ByVal FEEEHours As Decimal, ByVal Holidays As List(Of Business.ChildHoliday)) As Decimal

        'if we have funded hours, reduce the session times by the funded hours
        If FEEEHours > 0 Then

            Dim _DateCategory As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(ActionDate, TermType)
            If _DateCategory = Bookings.EnumDateCategory.TermDate Then

                Dim _ReduceAM As Boolean = ParameterHandler.ReturnBoolean("VARFUNDINGAM")
                If _ReduceAM Then

                    'reduce the funding from the start time
                    'i.e. session is 08:00-14:00, with 5 hours funding
                    'we add 5 hours onto 08:00 and charge from 13:00-14:00
                    'this was added for Kabuki

                    StartTime = StartTime.Value.AddHours(FEEEHours)

                Else

                    'reduce the funding from the end time (default)
                    'i.e. session is 08:00-14:00, with 5 hours funding
                    'we subtract 5 hours from 14:00 and charge from 09:00-10:00

                    EndTime = EndTime.Value.AddHours(FEEEHours * -1)

                End If

            End If

        End If

        Dim _Total As Decimal = 0

        'loop through all the variable intervals
        Dim _SQL As String = ""
        _SQL += "select * from TariffVariable"
        _SQL += " where tariff_id = '" + Tariff._ID.Value.ToString + "'"
        _SQL += " order by start_time"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _IntervalUnits As Integer = CleanData.ReturnInteger(_DR.Item("units"))
                Dim _IntervalRate As Decimal = CleanData.ReturnDecimal(_DR.Item("rate"))

                'check for holiday discount etc
                _IntervalRate = SetRate(ChildID, ActionDate, _IntervalRate, Holidays)

                Dim _IntervalStart As TimeSpan? = CleanData.ReturnTimeSpan(_DR.Item("start_time"))
                Dim _IntervalEnd As TimeSpan? = CleanData.ReturnTimeSpan(_DR.Item("end_time"))

                Dim _Start As TimeSpan? = Nothing
                Dim _End As TimeSpan? = Nothing

                'ensure session actually spans this time slot

                'this interval ends after the session starts
                If StartTime.Value.TimeOfDay > _IntervalEnd.Value Then Continue For

                'this interval starts before the session ends
                If EndTime.Value.TimeOfDay < _IntervalStart.Value Then Continue For

                If StartTime.Value.TimeOfDay <= _IntervalEnd Then
                    If StartTime.Value.TimeOfDay >= _IntervalStart Then
                        _Start = StartTime.Value.TimeOfDay
                    Else
                        _Start = _IntervalStart
                    End If
                End If

                If EndTime.Value.TimeOfDay <= _IntervalEnd Then
                    _End = EndTime.Value.TimeOfDay
                Else
                    _End = _IntervalEnd
                End If

                If _Start.HasValue AndAlso _End.HasValue Then

                    'calculate the minutes
                    Dim _IntervalSpan As TimeSpan = _End.Value - _Start.Value
                    Dim _IntervalMins As Integer = CInt(_IntervalSpan.TotalMinutes)

                    'calculate how many intervals lapsed
                    Dim _IntervalCount As Decimal = CDec(_IntervalMins / _IntervalUnits)
                    Dim _IntervalRounded As Decimal = Math.Ceiling(_IntervalCount)

                    'calculate interval cost
                    Dim _IntervalCost As Decimal = CDec(_IntervalRounded * _IntervalRate)

                    _Total += _IntervalCost

                End If

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Total

    End Function

    Private Sub ProcessBoltOns(ByRef ChildRecord As Business.Child, ByVal BoltOns As String, ByVal DateCategory As Bookings.EnumDateCategory)

        For Each _ID As String In BoltOns.Split(","c)

            Dim _B As Business.TariffBoltOn = Business.TariffBoltOn.RetreiveByID(New Guid(_ID))
            If _B IsNot Nothing Then

                Dim _NLCode As String = Invoicing.ReturnNLCode(ChildRecord, _B._NlCode)
                Dim _NLTracking As String = Invoicing.ReturnNLTracking(ChildRecord, _B._NlTracking)

                m_BoltOns.Add(New BoltOn(_B._ID.Value, _B._Name, _B._Price, _NLCode, _NLTracking, _B._Discount, _B._SummaryColumn, _B._ChargeClosed, _B._ChargeHoliday, _B._RecurringScope))
                m_BoltOnsValue += _B._Price

            End If

        Next

    End Sub

End Class
