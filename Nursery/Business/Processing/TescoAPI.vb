﻿Imports System.Net
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Windows.Forms
Imports System.Web.Script.Serialization

Public Class TescoAPI

    Private m_SecureServiceURL As String = "https://secure.techfortesco.com/groceryapi_b1/restservice.aspx?"
    Private m_ServiceURL As String = "http://www.techfortesco.com/groceryapi_b1/restservice.aspx?"

    Private m_AppKey As String = "B9668DCFE9AF77AC920D"
    Private m_DeveloperKey As String = "0ujRTU8FlFnyfad11Ium"
    Private m_Email As String = "james@caresoftware.co.uk"
    Private m_Password As String = "Ell122126"

    Private m_LoginSuccessful As Boolean = False
    Private m_SessionKey As String = ""

    Public ReadOnly Property LoggedIn As Boolean
        Get
            Return m_LoginSuccessful
        End Get
    End Property

    Public ReadOnly Property SessionKey As String
        Get
            Return m_SessionKey
        End Get
    End Property

    Public Function Login() As Boolean

        Dim _C As New List(Of CommandValue)
        _C.Add(New CommandValue("command", "LOGIN"))
        _C.Add(New CommandValue("email", m_Email))
        _C.Add(New CommandValue("password", m_Password))
        _C.Add(New CommandValue("developerkey", m_DeveloperKey))
        _C.Add(New CommandValue("applicationkey", m_AppKey))

        Dim _L As LoginInfo = Nothing
        If GetObject(Of LoginInfo)(_C, _L, True) Then
            m_LoginSuccessful = True
            m_SessionKey = _L.SessionKey
            Return True
        Else
            Return False
        End If

    End Function

    Public Function Search(ByVal SearchText As String) As List(Of Product)

        If Not m_LoginSuccessful Then Return Nothing

        Dim _C As New List(Of CommandValue)
        _C.Add(New CommandValue("command", "PRODUCTSEARCH"))
        _C.Add(New CommandValue("sessionkey", m_SessionKey))
        _C.Add(New CommandValue("searchtext", SearchText))

        Dim _URL As String = BuildURL(_C)
        Dim _R As String = ""
        If GetResponse(_URL, _R) Then

            Dim _SR As SearchResults = Nothing
            If GetObject(Of SearchResults)(_C, _SR) Then
                Return _SR.Products.ToList
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    Public Function SearchExtended(ByVal SearchText As String) As List(Of ProductExtended)

        If Not m_LoginSuccessful Then Return Nothing

        Dim _C As New List(Of CommandValue)
        _C.Add(New CommandValue("command", "PRODUCTSEARCH"))
        _C.Add(New CommandValue("sessionkey", m_SessionKey))
        _C.Add(New CommandValue("searchtext", SearchText))
        _C.Add(New CommandValue("extendedinfo", "Y"))

        Dim _URL As String = BuildURL(_C)
        Dim _R As String = ""
        If GetResponse(_URL, _R) Then
            Dim _SR As SearchResultsExtended = Nothing
            If GetObject(Of SearchResultsExtended)(_C, _SR) Then
                Return _SR.Products.ToList
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

#Region "Helpers"

    Private Function GetResponse(ByVal URL As String, ByRef Response As String) As Boolean

        Try
            Dim _req As HttpWebRequest = CType(WebRequest.Create(URL), HttpWebRequest)
            Dim _res As HttpWebResponse = CType(_req.GetResponse, HttpWebResponse)
            Dim _s As Stream = _res.GetResponseStream()
            Dim _sr As New StreamReader(_s)
            Dim _Return As String = _sr.ReadToEnd()

            Response = _Return

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Private Function BuildURL(ByVal Commands As List(Of CommandValue), Optional Secure As Boolean = False) As String

        Dim _URL As String = ""
        For Each _C As CommandValue In Commands
            Dim _CV As String = ""
            If _URL = "" Then
                _URL += _C.Command + "=" + _C.Value
            Else
                _URL += "&" + _C.Command + "=" + _C.Value
            End If
            _URL += _CV
        Next

        If Secure Then
            Return m_SecureServiceURL + _URL
        Else
            Return m_ServiceURL + _URL
        End If

    End Function

    Private Function GetObject(Of T)(ByVal Commands As List(Of CommandValue), ByRef ReturnObject As T, Optional ByVal Secure As Boolean = False) As Boolean

        Dim _Response As String = ""
        Dim _URL As String = BuildURL(Commands, Secure)

        If GetResponse(_URL, _Response) Then

            Try
                Dim _jss As New JavaScriptSerializer()
                ReturnObject = _jss.Deserialize(Of T)(_Response)

            Catch ex As Exception
                Return False
            End Try

        End If

        Return True

    End Function

#End Region

    Private Class CommandValue

        Public Property Command As String
        Public Property Value As String

        Public Sub New(ByVal CommandIn As String, ByVal CommandValueIn As String)
            Command = CommandIn
            Value = CommandValueIn
        End Sub

    End Class

#Region "Tesco Classes"

    <DataContract>
    Private Class LoginInfo

        <DataMember>
        Public Property StatusCode As String
        <DataMember>
        Public Property StatusInfo As String
        <DataMember>
        Public Property BranchNumber As String
        <DataMember>
        Public Property CustomerID As String
        <DataMember>
        Public Property CustomerName As String
        <DataMember>
        Public Property SessionKey As String
        <DataMember>
        Public Property InAmendOrderMode As String
        <DataMember>
        Public Property BasketID As String
        <DataMember>
        Public Property ChosenDeliverySlotInfo As String
        <DataMember>
        Public Property CustomerForename As String

    End Class

    <DataContract>
    Private Class SearchResults

        <DataMember>
        Public Property StatusCode As String
        <DataMember>
        Public Property StatusInfo As String
        <DataMember>
        Public Property PageNumber As String
        <DataMember>
        Public Property TotalPageCount As String
        <DataMember>
        Public Property TotalProductCount As String
        <DataMember>
        Public Property PageProductCount As String
        <DataMember>
        Public Property Products As Product()

    End Class

    <DataContract>
    Private Class SearchResultsExtended

        <DataMember>
        Public Property StatusCode As String
        <DataMember>
        Public Property StatusInfo As String
        <DataMember>
        Public Property PageNumber As String
        <DataMember>
        Public Property TotalPageCount As String
        <DataMember>
        Public Property TotalProductCount As String
        <DataMember>
        Public Property PageProductCount As String
        <DataMember>
        Public Property Products As ProductExtended()

    End Class

    <DataContract>
    Public Class Product

        <DataMember>
        Public Property BaseProductId As String
        <DataMember>
        Public Property EANBarcode As String
        <DataMember>
        Public Property CheaperAlternativeProductId As String
        <DataMember>
        Public Property HealthierAlternativeProductId As String
        <DataMember>
        Public Property ImagePath As String
        <DataMember>
        Public Property MaximumPurchaseQuantity As String
        <DataMember>
        Public Property Name As String
        <DataMember>
        Public Property OfferPromotion As String
        <DataMember>
        Public Property OfferValidity As String
        <DataMember>
        Public Property OfferLabelImagePath As String
        <DataMember>
        Public Property ShelfCategory As String
        <DataMember>
        Public Property ShelfCategoryName As String
        <DataMember>
        Public Property Price As String
        <DataMember>
        Public Property PriceDescription As String
        <DataMember>
        Public Property ProductId As String
        <DataMember>
        Public Property ProductType As String
        <DataMember>
        Public Property UnitPrice As String
        <DataMember>
        Public Property UnitType As String

    End Class

    <DataContract>
    Public Class ProductExtended

        <DataMember>
        Public Property BaseProductId As String
        <DataMember>
        Public Property EANBarcode As String
        <DataMember>
        Public Property CheaperAlternativeProductId As String
        <DataMember>
        Public Property CookingAndUsage As String
        <DataMember>
        Public Property ExtendedDescription As String
        <DataMember>
        Public Property HealthierAlternativeProductId As String
        <DataMember>
        Public Property ImagePath As String
        <DataMember>
        Public Property MaximumPurchaseQuantity As String
        <DataMember>
        Public Property Name As String
        <DataMember>
        Public Property OfferPromotion As String
        <DataMember>
        Public Property OfferValidity As String
        <DataMember>
        Public Property OfferLabelImagePath As String
        <DataMember>
        Public Property ShelfCategory As String
        <DataMember>
        Public Property ShelfCategoryName As String
        <DataMember>
        Public Property Price As String
        <DataMember>
        Public Property PriceDescription As String
        <DataMember>
        Public Property ProductId As String
        <DataMember>
        Public Property ProductType As String
        <DataMember>
        Public Property Rating As String
        <DataMember>
        Public Property StorageInfo As String
        <DataMember>
        Public Property UnitPrice As String
        <DataMember>
        Public Property UnitType As String
        <DataMember>
        Public Property RDA_Calories_Count As String
        <DataMember>
        Public Property RDA_Calories_Percent As String
        <DataMember>
        Public Property RDA_Sugar_Grammes As String
        <DataMember>
        Public Property RDA_Sugar_Percent As String
        <DataMember>
        Public Property RDA_Fat_Grammes As String
        <DataMember>
        Public Property RDA_Fat_Percent As String
        <DataMember>
        Public Property RDA_Saturates_Grammes As String
        <DataMember>
        Public Property RDA_Saturates_Percent As String
        <DataMember>
        Public Property RDA_Salt_Grammes As String
        <DataMember>
        Public Property RDA_Salt_Percent As String
        <DataMember>
        Public Property NutrientsCount As String
        <DataMember>
        Public Property Nutrients As Nutrient()
        <DataMember>
        Public Property IngredientsCount As String
        <DataMember>
        Public Property Ingredients As Ingredient()
    End Class

    <DataContract>
    Public Class Nutrient
        <DataMember>
        Public Property NutrientName As String
        <DataMember>
        Public Property SampleDescription As String
        <DataMember>
        Public Property SampleSize As String
        <DataMember>
        Public Property ServingDescription As String
        <DataMember>
        Public Property ServingSize As String
    End Class

    <DataContract>
    Public Class Ingredient
        <DataMember>
        Public Property Name As String
    End Class

#End Region

End Class
