﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports Nursery.Processing

Public Class Invoicing

    Public Enum EnumXCheckMode As Integer
        None = 0
        RegisterVsInvoiced = 1
        RegisterVsBookings = 2
    End Enum

    Public Enum EnumAdditionalSessionMode As Integer
        Excluded = 0
        Included = 1
        Only = 2
    End Enum

    Public Enum EnumDuplicateMode As Integer
        Include = 0
        ExcludeAllInvoices = 1
        ExcludeOnlyPostedInvoices = 2
    End Enum

    Public Enum EnumAnnualised As Integer
        None = 0
        ManualCalculation = 1
        AnnualCalculationFromPatterns = 2
    End Enum

    Public Enum EnumGenerationMode As Integer
        Standard = 0
        Summarised = 1
    End Enum

    Private Enum SessionDate As Integer
        sessionStart = 0
        sessionEnd = 1
    End Enum

    Private Shared m_ParametersLoaded As Boolean = False
    Private Shared m_StrictSessions As Boolean = False
    Private Shared m_DefaultNLCode As String = ""
    Private Shared m_DefaultNLTracking As String = ""
    Private Shared m_NLTrackingCategory As String = ""
    Private Shared m_InvoiceDueDays As Integer = 0
    Private Shared m_ManualCalculationLines As Boolean = False

    Private Shared Sub FetchParameters()
        m_StrictSessions = ParameterHandler.ReturnBoolean("INVOICESTRICT")
        m_InvoiceDueDays = ParameterHandler.ReturnInteger("INVDUEDAYS", False)
        m_DefaultNLCode = ParameterHandler.ReturnString("NLCODE")
        m_DefaultNLTracking = ParameterHandler.ReturnString("NLTRACKING")
        m_NLTrackingCategory = ParameterHandler.ReturnString("NLTRACKINGCAT")
        m_ManualCalculationLines = ParameterHandler.ReturnBoolean("MANUALCALCLINES")
        m_ParametersLoaded = True
    End Sub

    Private Shared ReadOnly Property StrictSessions() As Boolean
        Get
            If Not m_ParametersLoaded Then FetchParameters()
            Return m_StrictSessions
        End Get
    End Property

    Private Shared ReadOnly Property InvoiceDueDays() As Integer
        Get
            If Not m_ParametersLoaded Then FetchParameters()
            Return m_InvoiceDueDays
        End Get
    End Property

    Public Shared Function LastBatchNo() As Integer
        Dim _SQL As String = "select max(batch_no) as 'batch_no' from InvoiceBatch"
        Dim _DBNo As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return _DBNo
    End Function

    Public Shared Function LastInvoiceNo() As Integer

        Dim _SQL As String = ""

        _SQL += "select max(i.invoice_no) as 'invoice_no' from Invoices i"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " where b.batch_status = 'Posted'"
        _SQL += " and b.batch_period <> 'Annual Segments'"

        Dim _DBNo As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        'if there are no invoices, we need to take from parameters
        If _DBNo <= 0 Then
            _DBNo = ParameterHandler.ReturnInteger("NEXTINV", False)
        End If

        Return _DBNo

    End Function

    Public Shared Function LastInvoiceDate() As Date?

        Dim _SQL As String = "select max(invoice_period_end) as 'invoice_period_end' from Invoices" &
                             " left join InvoiceBatch on InvoiceBatch.ID = Invoices.batch_id" &
                             " where batch_status <> 'Abandoned' and invoice_status <> 'Generated'"

        Dim _Date As Date? = ValueHandler.ConvertDate(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        If _Date Is Nothing Then
            _Date = ParameterHandler.ReturnDate("INVOICELASTPERIOD")
        End If

        Return _Date

    End Function

    Public Shared Function BatchReadyToPost(ByVal BatchID As Guid) As Boolean

        'fetch invoices, ignorning any zero value invoices.
        Dim _SQL As String = "select count(*) as 'count' from Invoices" &
                             " where batch_id = '" + BatchID.ToString + "'" &
                             " and invoice_status = 'Generated'" &
                             " and invoice_total <> 0"

        Dim _Count As Integer = 0
        Dim _CountSQL As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
        If _CountSQL IsNot Nothing Then _Count = CInt(_CountSQL)

        If _Count > 0 Then
            'there are still generated invoices...
            Return False
        Else
            Return True
        End If

    End Function

    Public Shared Sub UpdateInvoiceStatus(ByVal InvoiceID As String, ByVal InvoiceStatus As String, Optional ByVal ExternalInvoceID As String = "")

        Dim _I As Business.Invoice = Business.Invoice.RetreiveByID(New Guid(InvoiceID))
        If _I IsNot Nothing Then

            Select Case InvoiceStatus

                Case "Printed"
                    'if we want to mark this invoice as printed, check is has not already been marked as emailed...
                    If _I._InvoiceStatus = "Emailed" Then
                        'leave it alone
                    Else
                        _I._InvoiceStatus = InvoiceStatus
                    End If

                Case Else
                    _I._InvoiceStatus = InvoiceStatus

            End Select

            If ExternalInvoceID <> "" Then _I._FinancialsDocId = ExternalInvoceID

            _I.Store()
            _I = Nothing

        End If

    End Sub

    Public Shared Function SendInvoices(ByVal Invoices As List(Of Guid), ByVal EmailOnly As Boolean) As Boolean

        Dim _frm As New frmInvoiceSend(Invoices, EmailOnly)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function ReApplyInvoiceNumbers(ByVal InvoiceBatchID As Guid, ByVal Interactive As Boolean) As Boolean

        'fetch all the invoices in the batch and re-apply the numbers
        Dim _SQL As String = "select ID, invoice_no from Invoices where batch_id = '" + InvoiceBatchID.ToString + "' order by invoice_no"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            If Interactive Then Session.SetupProgressBar("Re-Applying Invoice Numbers...", _DT.Rows.Count)

            Dim _NextInvoiceNumber As Long = LastInvoiceNo() + 1

            Dim _Batch As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(InvoiceBatchID)
            _Batch._InvFirst = _NextInvoiceNumber

            For Each _DR As DataRow In _DT.Rows
                UpdateInvoiceNumber(_DR.Item("ID").ToString, CInt(_NextInvoiceNumber))
                _NextInvoiceNumber += 1
                If Interactive Then Session.StepProgressBar()
            Next

            _Batch._InvLast = _NextInvoiceNumber - 1 'we would have incremented the last one...
            _Batch.Store()

            _Batch = Nothing

            If Interactive Then Session.HideProgressBar()

        End If

        _DT.Dispose()
        _DT = Nothing

        Return True

    End Function

    Public Shared Function PostBatch(ByVal InvoiceBatchID As Guid, ByVal SiteID As Guid) As Boolean

        Dim _Return As Boolean = False
        Dim _frm As New frmFinancialsPost(InvoiceBatchID, frmFinancialsPost.EnumBatchType.Invoices, SiteID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then _Return = True

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Private Shared Function UpdateInvoiceNumber(ByVal InvoiceID As String, ByVal InvoiceNo As Integer) As Boolean

        Dim _I As Business.Invoice = Business.Invoice.RetreiveByID(New Guid(InvoiceID))
        With _I
            ._InvoiceNo = InvoiceNo
            .Store()
        End With
        _I = Nothing

        Return True

    End Function

    Public Shared Sub PrintInvoiceSummary(ByVal InvoiceBatch As Guid, ByVal DesignMode As Boolean)

        Dim _SQL As String = "select Children.group_name, Invoices.invoice_period, Invoices.invoice_no, Invoices.family_name, Invoices.child_name," &
                             " Invoices.invoice_sub, Invoices.invoice_discount, Invoices.invoice_total," &
                             " b.batch_no, b.batch_status, b.batch_period, b.date_first, b.date_last, b.batch_class, b.batch_freq, Groups.sequence," &
                             " Children.surname, Children.surname_forename" &
                             " from Invoices" &
                             " left join Children on Children.id = Invoices.child_id" &
                             " left join Groups on Groups.ID = Children.group_id" &
                             " left join InvoiceBatch b on b.id = Invoices.batch_id" &
                             " where batch_id = '" & InvoiceBatch.ToString & "'" &
                             " order by Children.group_name, invoice_no"

        If DesignMode Then
            ReportHandler.DesignReport("BatchSummary.repx ", Session.ConnectionString, _SQL)
        Else
            ReportHandler.RunReport("BatchSummary.repx ", Session.ConnectionString, _SQL)
        End If

    End Sub

    Public Shared Sub PrintInvoice(InvoiceID As Guid)

        Dim _InvoicesToPrint As New List(Of Guid)
        _InvoicesToPrint.Add(InvoiceID)

        Dim _Eng As New Reports.InvoiceRun
        _Eng.RunInvoices(Reports.InvoiceRun.EnumOutputMode.ForcePrint, _InvoicesToPrint, "", "", "", "")
        _Eng = Nothing

    End Sub

    Public Shared Function AmendInvoice(InvoiceID As Guid) As Boolean

        Dim _Amended As Boolean = False
        Dim _frmInvoice As New frmInvoice(frmInvoice.EnumMode.AmendInvoice, InvoiceID)
        _frmInvoice.ShowDialog()

        If _frmInvoice.DialogResult = DialogResult.OK Then _Amended = True

        _frmInvoice.Dispose()
        _frmInvoice = Nothing

        Return _Amended

    End Function

    Public Shared Sub ViewInvoice(InvoiceID As Guid)

        Dim _Mode As frmInvoice.EnumMode = frmInvoice.EnumMode.ViewInvoice

        Dim _frmInvoice As New frmInvoice(_Mode, InvoiceID)
        _frmInvoice.ShowDialog()

        _frmInvoice.Dispose()
        _frmInvoice = Nothing

    End Sub

    Public Shared Sub ViewBatch(BatchID As Guid, ByVal Caption As String)

        Dim _frm As New frmInvoiceBatch(BatchID, Caption)

        _frm.MaximizeBox = True
        _frm.LoadMaximised = True
        _frm.WindowState = FormWindowState.Maximized
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Public Shared Sub ClearInvoices(ByVal PeriodMonth As Integer, ByVal PeriodYear As Integer, ByVal ActualInvoiceDate As Date)

        Dim _InvoicePeriod As Date = DateSerial(PeriodYear, PeriodMonth, 1)
        Dim _InvoicePeriodString As String = Format(_InvoicePeriod, "yyyy-MM-dd")
        Dim _LastDate As Date = DateSerial(PeriodYear, PeriodMonth, Date.DaysInMonth(PeriodYear, PeriodMonth))
        Dim _LastDateString As String = Format(_LastDate, "yyyy-MM-dd")

        Dim _SQL As String = "select id, family_id from Invoices where invoice_period between '" + _InvoicePeriodString + "' and '" + _LastDateString + "'"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _dr As DataRow In _DT.Rows
                DeleteInvoice(_dr.Item("id").ToString)
                DeleteStatement(_dr.Item("family_id").ToString, _LastDate)
            Next

        End If

    End Sub

    Public Shared Sub DeleteInvoice(ByVal InvoiceID As String)

        Dim _SQL As String = ""

        _SQL = "delete from Invoices where id = '" + InvoiceID + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from InvoiceLines where invoice_id = '" + InvoiceID + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Public Shared Sub DeleteStatement(ByVal FamilyID As String, LastDate As Date)

        Dim _SQL As String = "delete from FamilyStatement where family_id = '" + FamilyID + "'" &
                             " and statement_date = '" + Format(LastDate, "yyyy-MM-dd") + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Public Shared Sub ReApplyNLCodes(ByRef InvoiceIn As Business.Invoice)

        Dim _C As Business.Child = Nothing

        Dim _NLCode As String = FinanceShared.DefaultNLCode 'use default NL code
        Dim _NLTracking As String = ""

        If InvoiceIn._ChildId.HasValue Then
            _C = Business.Child.RetreiveByID(InvoiceIn._ChildId.Value)
        Else
            'no child, so we default from the family site
            Dim _F As Business.Family = Business.Family.RetreiveByID(InvoiceIn._FamilyId.Value)
            If _F IsNot Nothing Then
                If _F._SiteId.HasValue Then
                    Dim _S As Business.Site = Business.Site.RetreiveByID(_F._SiteId.Value)
                    If _S IsNot Nothing Then
                        _NLCode = _S._NlCode
                        _NLTracking = _S._NlTracking
                    End If
                End If
            End If
        End If

        Dim _Lines As List(Of Business.InvoiceLine) = Business.InvoiceLine.RetreiveAllByInvoiceID(InvoiceIn._ID.Value)
        For Each _Line As Business.InvoiceLine In _Lines

            If _C Is Nothing Then
                _Line._NlCode = _NLCode
                _Line._NlTracking = _NLTracking
            Else
                If _Line._Ref3 <> "" Then
                    Dim _T As Business.Tariff = Business.Tariff.RetreiveByID(New Guid(_Line._Ref3.ToString))
                    If _T IsNot Nothing Then
                        _Line._NlCode = ReturnNLCode(_C, _T._NlCode)
                        _Line._NlTracking = ReturnNLCode(_C, _T._NlTracking)
                    Else
                        _Line._NlCode = ReturnNLCode(_C, "")
                        _Line._NlTracking = ReturnNLTracking(_C, "")
                    End If
                Else
                    _Line._NlCode = ReturnNLCode(_C, "")
                    _Line._NlTracking = ReturnNLTracking(_C, "")
                End If
            End If

            _Line.Store()

        Next

    End Sub

    Public Shared Sub RecalculateInvoice(ByVal InvoiceID As Guid)

        'fetch the invoice
        Dim _Invoice As Business.Invoice = Business.Invoice.RetreiveByID(InvoiceID)
        If _Invoice IsNot Nothing Then

            If _Invoice._InvoicePeriod.HasValue AndAlso _Invoice._InvoicePeriodEnd.HasValue Then

                Dim _Annualised As Invoicing.EnumAnnualised = EnumAnnualised.None
                Dim _AnnualID As Guid? = Nothing
                Dim _Child As Business.Child = Nothing

                GenerateInvoiceLines(InvoiceID, _Invoice._ChildId.Value, _Invoice._InvoicePeriod.Value, _Invoice._InvoicePeriodEnd.Value, False,
                                 CType(_Invoice._Xcheck, EnumXCheckMode), _Invoice._XcheckFrom, _Invoice._XcheckTo,
                                 _Invoice._XcheckThresh, _Invoice._XcheckPerHour, _Invoice._XcheckAdditional, _Annualised, _AnnualID, ReturnSessionModeFromByte(_Invoice._AdditionalSessions),
                                 ReturnDuplicateModeFromByte(_Invoice._DuplicateMode), _Invoice._TariffFilter, _Child, _Invoice._AdditionalFrom, _Invoice._AdditionalTo, False)

                'update invoice totals
                With _Invoice

                    If _Annualised = EnumAnnualised.None Then
                        ._InvoiceAnnualised = False
                    Else
                        ._InvoiceAnnualised = True
                        ._AnnualType = _Annualised.ToString
                        ._AnnualId = _AnnualID
                    End If

                    ._ChangedBy = Nothing
                    ._ChangedStamp = Nothing

                    'set the starter and leaver information if they fall within the invoice period
                    If _Child._Started.HasValue Then
                        If _Child._Started.Value >= _Invoice._InvoicePeriod AndAlso _Child._Started.Value <= _Invoice._InvoicePeriodEnd Then
                            ._StarterDate = _Child._Started.Value
                        End If
                    End If

                    If _Child._DateLeft.HasValue Then
                        If _Child._DateLeft.Value >= _Invoice._InvoicePeriod AndAlso _Child._DateLeft.Value <= _Invoice._InvoicePeriodEnd Then
                            ._LeaverDate = _Child._DateLeft.Value
                        End If
                    End If

                    CalculateInvoiceTotals(_Invoice, _Child, _Annualised)

                    ProcessVouchers(_Invoice, _Child)

                    .Store()

                    'generate invoice days entries
                    '*****************************************************************************************************************************
                    Invoicing.GenerateInvoiceDays(InvoiceID, _Annualised, _AnnualID, _Child._Forename, _Child._Surname)
                    '*****************************************************************************************************************************

                End With

            Else
                'recalculate must have been called from a manual invoice
            End If

        End If

        _Invoice = Nothing

    End Sub

    Public Shared Sub CalculateInvoiceTotals(ByRef Invoice As Business.Invoice, ByRef ChildRecord As Business.Child, ByVal Annualised As Invoicing.EnumAnnualised)

        Dim _Totals As InvoiceTotals = ReturnInvoiceTotals(Invoice._ID.Value, ChildRecord, Annualised, Invoice._InvoicePeriodEnd)
        If _Totals IsNot Nothing Then

            With Invoice

                'discount detail
                ._Discountable = _Totals.Discountable
                ._DiscountId = _Totals.DiscountID
                ._DiscountName = _Totals.DiscountText

                'invoice totals
                ._InvoiceSub = _Totals.SubTotal
                ._InvoiceDiscount = _Totals.DiscountAmount
                ._InvoiceTotal = _Totals.Total

                ._XcheckCount = _Totals.XCheckCount
                ._XcheckAmount = _Totals.XCheckAmount

                ._InvoiceHoursNonfunded = _Totals.HoursNonFunded
                ._InvoiceHoursFunded = _Totals.HoursFunded
                ._TotalNonfunded = _Totals.TotalNonFunded
                ._TotalFunded = _Totals.TotalFunded
                ._TotalBoltons = _Totals.TotalBoltOns

            End With

        End If

        _Totals = Nothing

    End Sub

    Private Shared Function GenerateInvoiceLines(ByVal InvoiceID As Guid, ByVal ChildID As Guid,
                                                 ByRef FirstDate As Date, ByRef LastDate As Date,
                                                 ByVal CreatingAnnualInvoice As Boolean,
                                                 ByVal XCheck As Invoicing.EnumXCheckMode, ByVal XCheckFrom As Date?, ByVal XCheckTo As Date?,
                                                 ByVal XCheckThreshold As Decimal, ByVal XCheckPerHour As Decimal, ByVal XCheckAdditional As Boolean,
                                                 ByRef Annualised As Invoicing.EnumAnnualised, ByRef AnnualID As Guid?,
                                                 ByVal AdditionalSessionMode As EnumAdditionalSessionMode, ByVal DuplicateMode As Invoicing.EnumDuplicateMode, ByVal TariffFilter As String,
                                                 ByRef Child As Business.Child, ByVal AdditionalFrom As Date?, ByVal AdditionalTo As Date?, ByVal Forecasting As Boolean) As Boolean

        If Not m_ParametersLoaded Then FetchParameters()

        Annualised = EnumAnnualised.None
        Dim _AnnualID As Guid? = Nothing

        'delete existing lines
        Dim _SQL As String = "delete from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Dim _Child As Business.Child = Business.Child.RetreiveByID(ChildID)
        Child = _Child

        'fetch the holidays for this child
        Dim _Holidays As List(Of Business.ChildHoliday) = Business.ChildHoliday.RetreiveByChildID(ChildID)

        'default the invoice range to cover the entire month
        Dim _FirstDate As Date = FirstDate
        Dim _LastDate As Date = LastDate

        'if the start date occurs after this invoice period, we don't need to generate any lines...
        If _Child._Started > _LastDate Then Return False

        'if the end date occurs before this invoice period, we don't need to generate any lines...
        If _Child._DateLeft < _FirstDate Then Return False

        'if the start date falls within the date range, we might need to use this...
        If _Child._Started >= _FirstDate And _Child._Started <= _LastDate Then
            _FirstDate = _Child._Started.Value
        End If

        'if the leaving date falls within the date range, we might need to use this...
        If _Child._DateLeft.HasValue Then
            If _Child._DateLeft >= _FirstDate And _Child._DateLeft <= _LastDate Then
                _LastDate = _Child._DateLeft.Value
            End If
        End If

        If Forecasting Then

            If Not Child._CalcLeaveDate.HasValue Then
                Dim _SchoolLeaveMonths = ParameterHandler.ReturnInteger("SCHOOLMONTHS", False)
                _Child._CalcLeaveDate = Bookings.ReturnSchoolLeaveDate(_Child._Dob, _SchoolLeaveMonths)
            End If

            'if the calculated leaving date occurs before this invoice period, we don't need to generate any lines...
            If _Child._CalcLeaveDate < _FirstDate Then Return False

            'if the calculated leaving date falls within the date range, we might need to use this...
            If _Child._CalcLeaveDate >= _FirstDate And _Child._CalcLeaveDate <= _LastDate Then
                _LastDate = _Child._CalcLeaveDate.Value
            End If

        End If

        '*****************************************************************************************************************************

        Dim _LineNo As Long = 0
        Dim _ProcessSessions As Boolean = False

        '*****************************************************************************************************************************

        If AdditionalSessionMode = EnumAdditionalSessionMode.Only Then
            'additional sessions only, so we are not interested in any overrides etc.
            _ProcessSessions = True
        Else

            If XCheckAdditional Then
                'we won't process any sessions as we are doing an invoice register check only batch
                'basically, we are only billing non-booked sessions
            Else
                'if we are creating the annual invoice, we want to process the sessions normally
                If CreatingAnnualInvoice Then
                    _ProcessSessions = True
                Else
                    'check if the is a manual calculation defined i.e. £40 x 3 days x 51 weeks / 12 months etc
                    If ManualCalculationDefined(InvoiceID, _Child, _LineNo, FirstDate, LastDate, _AnnualID) Then
                        _ProcessSessions = False
                        Annualised = EnumAnnualised.ManualCalculation
                    Else
                        'ok, so let's check if there is an average invoice generated we can just pickup
                        If AverageInvoiceProcessed(InvoiceID, _Child, _LineNo, FirstDate, LastDate, _AnnualID) Then
                            _ProcessSessions = False
                            Annualised = EnumAnnualised.AnnualCalculationFromPatterns
                        Else
                            'check if the is an override defined i.e. £1145 per month for 4 days per week (Piplings etc)
                            If ProcessOverride(InvoiceID, _Child, _FirstDate, _LastDate, _LineNo) Then
                                'overide was used
                                _ProcessSessions = False
                            Else
                                _ProcessSessions = True
                            End If
                        End If
                    End If
                End If
            End If
        End If

        '*****************************************************************************************************************************

        'fetch the bookings for this child for the invoice period
        Dim _Bookings As List(Of Processing.BookingDay) = Nothing
        _Bookings = Processing.InvoiceBooking.ReturnBookingsNew(_Child, _FirstDate, _LastDate, AdditionalSessionMode, AdditionalFrom, AdditionalTo)

        '*****************************************************************************************************************************

        If _Bookings.Count > 0 Then

            For Each _B In _Bookings

                'additional sessions
                If _B.PatternType = "Additional" Then
                    If AdditionalSessionMode = EnumAdditionalSessionMode.Excluded Then
                        'excluded
                    Else
                        CreateLine(InvoiceID, _LineNo, _Child, New SessionDataObject(_B, _Child, TariffFilter, _Holidays), _B.ActionDate, _Holidays)
                    End If
                Else
                    'recurring and overrides
                    If _ProcessSessions Then
                        CreateLine(InvoiceID, _LineNo, _Child, New SessionDataObject(_B, _Child, TariffFilter, _Holidays), _B.ActionDate, _Holidays)
                    Else
                        'we do not want to add the booking to the invoice (usually because we are doing an average etc)
                    End If
                End If

            Next

        End If

        'process any ad-hoc charges
        '*****************************************************************************************************************************
        If AdditionalSessionMode <> EnumAdditionalSessionMode.Only Then
            If Not XCheckAdditional Then
                Dim _Charges As List(Of Business.ChildCharge) = Business.ChildCharge.RetrieveUsingRange(ChildID, FirstDate, LastDate)
                If _Charges IsNot Nothing Then
                    For Each _C In _Charges
                        CreateLine(InvoiceID, _LineNo, "3", _C._ActionDate, _C._Description, _C._Reference, "", "", Invoicing.ReturnNLCode(_Child, ""), Invoicing.ReturnNLTracking(_Child, ""), 0, _C._Value, 0)
                        _LineNo += 1
                    Next
                End If
            End If
        End If

        'register cross-reference check
        '*****************************************************************************************************************************
        If XCheck > EnumXCheckMode.None Then
            If XCheckFrom.HasValue AndAlso XCheckTo.HasValue Then
                ProcessRegisterCheck(ChildID, XCheck, XCheckFrom.Value, XCheckTo.Value, XCheckThreshold, XCheckPerHour, XCheckAdditional, InvoiceID, _LineNo)
            End If
        End If

        '*****************************************************************************************************************************

        AnnualID = _AnnualID

        If _LineNo > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Shared Function AlreadyInvoicedThisBooking(ByVal ChildID As Guid, ByVal DuplicateMode As Invoicing.EnumDuplicateMode, ByVal Booking As BookingDay) As Boolean

        'we want duplicates (default), so we just return false
        If DuplicateMode = EnumDuplicateMode.Include Then Return False

        Dim _SQL As String = ""

        _SQL += "select count(*) as 'Lines' from InvoiceLines l"
        _SQL += " left join Invoices i on i.ID = l.invoice_id"
        _SQL += " where i.child_id = '" + ChildID.ToString + "'"
        _SQL += " and l.action_date = " + ValueHandler.SQLDate(Booking.ActionDate, True)

        If DuplicateMode = EnumDuplicateMode.ExcludeOnlyPostedInvoices Then
            _SQL += " and i.invoice_status = 'Posted'"
        End If

        Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Shared Function ProcessOverride(ByVal InvoiceID As Guid, ByVal ChildRecord As Business.Child, ByVal FirstDate As Date, ByVal LastDate As Date, ByRef LineNo As Long) As Boolean

        Dim _Return As Boolean = False
        Dim _SQL As String = ""

        _SQL += "select ID from ChildWeekMonth"
        _SQL += " where child_id = '" + ChildRecord._ID.ToString + "'"
        _SQL += " and date_from <= " + ValueHandler.SQLDate(LastDate, True)
        _SQL += " order by date_from desc"

        Dim _DRAnnual As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRAnnual IsNot Nothing Then

            Dim _ID As New Guid(_DRAnnual.Item("ID").ToString)
            Dim _C As Business.ChildWeekMonth = Business.ChildWeekMonth.RetreiveByID(_ID)
            If _C IsNot Nothing Then

                If LineNo = 0 Then LineNo = 1

                Dim _SpecificDescription As String = ""
                Dim _AgeBandDescription As String = ""
                Dim _Rate As Decimal = ReturnOverrideMatrix(_C, ChildRecord._Dob, FirstDate, _AgeBandDescription, _SpecificDescription)

                Dim _Days As String = ""
                Dim _Description As String = ""
                Dim _FundingText As String = ""

                If _SpecificDescription.ToLower.Contains("fund") Then
                    _FundingText = " (incorporating for 3 & 4 year old Government funding)"
                End If

                'piplings
                '_Days = ReturnDays(_C._DaysPerWeek)
                '_Description = "Fixed " + _C._Frequency + " fee based upon " + _Days + " per week."

                'baby room
                _Days = ReturnDaysWord(_C._DaysPerWeek)
                _Description = _AgeBandDescription + " year old monthly fee for " + _Days + " booking pattern" + _FundingText

                CreateLine(InvoiceID, LineNo, "2", FirstDate, _Description, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Rate, 0)

                LineNo += 1

                _Return = True

            End If

        End If

        _DRAnnual = Nothing
        Return _Return

    End Function

    Private Shared Function ReturnDaysWord(ByVal DaysPerWeek As Integer) As String
        If DaysPerWeek = 1 Then Return "one day"
        If DaysPerWeek = 2 Then Return "two day"
        If DaysPerWeek = 3 Then Return "three day"
        If DaysPerWeek = 4 Then Return "four day"
        If DaysPerWeek = 5 Then Return "five day"
        If DaysPerWeek = 6 Then Return "six day"
        If DaysPerWeek = 7 Then Return "seventy day"
        Return ""
    End Function

    Private Shared Function ReturnDays(ByVal DaysPerWeek As Integer) As String
        If DaysPerWeek = 1 Then
            Return "1 day"
        Else
            Return DaysPerWeek.ToString + " days"
        End If
    End Function

    Private Shared Function ReturnOverrideMatrix(ByVal ChildWeekMonth As Business.ChildWeekMonth, ByVal DOB As Date?, ByVal DateFrom As Date, ByRef AgeBandDescription As String, ByRef SpecificDescription As String) As Decimal

        Dim _Rate As Decimal = 0

        'always get the rate and description based upon age
        Dim _Months As Long = DateAndTime.DateDiff(DateInterval.Month, DOB.Value, DateFrom)
        _Rate = Business.TariffDay.RetrieveByDaysPerWeekAndAge(ChildWeekMonth._TariffId.Value, ChildWeekMonth._DaysPerWeek, _Months, AgeBandDescription)

        If ChildWeekMonth._AgeMode <> "Use DOB" Then
            'if there is an override, get this rate and description
            _Rate = Business.TariffDay.RetrieveByDaysPerWeekAndAgeBand(ChildWeekMonth._TariffId.Value, ChildWeekMonth._DaysPerWeek, ChildWeekMonth._AgeId.Value, SpecificDescription)
        End If

        Return _Rate

    End Function

    Private Shared Sub ProcessRegisterCheck(ByVal childID As Guid,
                                            ByVal xCheckType As Invoicing.EnumXCheckMode, ByVal xCheckFrom As Date, ByVal xCheckTo As Date,
                                            ByVal xCheckThreshold As Decimal, ByVal xCheckRate As Decimal, ByVal xCheckAdditional As Boolean,
                                            ByVal invoiceID As Guid, ByRef lineNumber As Long)

        If lineNumber = 0 Then lineNumber = 1

        'fetch the actual register for the period
        Dim sql As String = Business.Register.ReturnChildRegister(childID, xCheckFrom, xCheckTo)
        Dim registerTable As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)

        If registerTable IsNot Nothing Then

            For Each row As DataRow In registerTable.Rows

                Dim registerDate As Date? = ValueHandler.ConvertDate(row.Item("Date"))
                Dim timeIn As Date? = ValueHandler.ConvertDate(row.Item("In"))
                Dim timeOut As Date = Nothing

                If ValueHandler.ConvertDate(row.Item("Out")).HasValue Then timeOut = ValueHandler.ConvertDate(row.Item("Out")).Value

                Dim sessionStart As Date = Nothing
                Dim sessionEnd As Date = Nothing
                Dim extraMinutes As Long = 0

                If Not IsNothing(timeIn) AndAlso Not IsNothing(timeOut) Then

                    Dim checkType As String = ""

                    If xCheckType = EnumXCheckMode.RegisterVsInvoiced Then
                        checkType = "Invoiced"
                        extraMinutes = ReturnInvoicedMins(childID, registerDate.Value)

                    ElseIf xCheckType = EnumXCheckMode.RegisterVsBookings Then
                        checkType = "Booked"
                        sessionStart = ReturnSessionDate(childID, registerDate.Value, SessionDate.sessionStart)
                        sessionEnd = ReturnSessionDate(childID, registerDate.Value, SessionDate.sessionEnd)

                        Dim extraBeforeStart As Long = 0
                        Dim extraAfterEnd As Long = 0

                        extraBeforeStart = DateDiff(DateInterval.Minute, timeIn.Value, sessionStart)
                        extraAfterEnd = DateDiff(DateInterval.Minute, sessionEnd, timeOut)

                        extraMinutes = extraBeforeStart + extraAfterEnd

                    End If

                    If extraMinutes >= xCheckThreshold Then

                        Dim extraHours As Decimal = CDec(extraMinutes / 60)
                        Dim extraCost As Decimal = extraHours * xCheckRate
                        Dim description As String = ""

                        If xCheckType = EnumXCheckMode.RegisterVsBookings Then
                            description = String.Concat("Attended: ", timeIn.Value.TimeOfDay.ToString, " - ", timeOut.TimeOfDay.ToString, Environment.NewLine,
                                                    "Session: ", sessionStart.TimeOfDay.ToString, " - ", sessionEnd.TimeOfDay.ToString, Environment.NewLine,
                                                    "Time not invoiced: ", ValueHandler.MoneyAsText(extraMinutes), " minutes.", Environment.NewLine)
                        Else
                            description = String.Concat("Attended: ", timeIn.Value.TimeOfDay.ToString, " - ", timeOut.TimeOfDay.ToString, Environment.NewLine,
                                                    "Invoiced Hours: ", ReturnInvoicedMins(childID, registerDate.Value) * 60,
                                                    "Time not invoiced: ", ValueHandler.MoneyAsText(extraMinutes), " minutes.", Environment.NewLine)
                        End If

                        CreateLine(invoiceID, lineNumber, "4", registerDate, description, "", "", "", "", "", extraHours, extraCost, ReturnBookedMins(childID, registerDate.Value) * 60)
                        lineNumber += 1

                    End If
                End If
            Next

            registerTable.Dispose()
            registerTable = Nothing

        End If

    End Sub

    Private Shared Function ReturnInvoicedMins(ByVal ChildID As Guid, ByVal ActionDate As Date) As Long

        Dim _Return As Long = 0

        Dim _SQL As String = ""
        _SQL += "select isnull(l.hours,0) as 'hours' from InvoiceLines l"
        _SQL += " left join Invoices i on i.ID = l.invoice_id"
        _SQL += " where l.action_date = " + ValueHandler.SQLDate(ActionDate, True)
        _SQL += " and i.child_id = '" + ChildID.ToString + "'"
        _SQL += " and i.invoice_status = 'Posted'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = CLng(_DR.Item("hours"))
            _Return = _Return * 60
            _DR = Nothing
        End If

        Return _Return

    End Function

    Private Shared Function ReturnBookedMins(ByVal ChildID As Guid, ByVal ActionDate As Date) As Long

        Dim _Return As Long = 0

        Dim _SQL As String = ""
        _SQL += "select DATEDIFF(MINUTE, booking_from, booking_to) as 'mins' from Bookings"
        _SQL += " where booking_date = " + ValueHandler.SQLDate(ActionDate, True)
        _SQL += " and child_id = '" + ChildID.ToString + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = CLng(_DR.Item("mins"))
            _DR = Nothing
        End If

        Return _Return

    End Function

    Private Shared Function ReturnManualCalculationID(ByVal ChildID As Guid, ByVal LastDate As Date) As Guid?

        Dim _Return As Guid? = Nothing
        Dim _SQL As String = ""

        _SQL += "select ID from ChildCalculation"
        _SQL += " where child_id = '" + ChildID.ToString + "'"
        _SQL += " and date_from <= " + ValueHandler.SQLDate(LastDate, True)
        _SQL += " and active = 1"
        _SQL += " order by date_from desc"

        Dim _DRAnnual As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRAnnual IsNot Nothing Then
            _Return = New Guid(_DRAnnual.Item("ID").ToString)
            _DRAnnual = Nothing
        End If

        Return _Return

    End Function

    Private Shared Function ManualCalculationDefined(ByVal InvoiceID As Guid, ByRef ChildRecord As Business.Child, ByRef LineNo As Long,
                                                     ByVal FirstDate As Date, ByVal LastDate As Date, ByRef AnnualID As Guid?) As Boolean

        'check if there is manual calculation defined covering the invoice period
        Dim _ID As Guid? = ReturnManualCalculationID(ChildRecord._ID.Value, LastDate)
        If _ID.HasValue Then

            Dim _C As Business.ChildCalculation = Business.ChildCalculation.RetreiveByID(_ID.Value)
            If _C IsNot Nothing Then

                AnnualID = _C._ID

                If LineNo = 0 Then LineNo = 1

                'by default, we use the block of text generated against the calculation
                If m_ManualCalculationLines = False Then

                    Dim _InvoiceTotal As Decimal = 0
                    _InvoiceTotal = (_C._TotalCalcAnnum - _C._FundCalcAnnum) / Invoicing.ReturnSplitInto(_C._SplitType)

                    CreateLine(InvoiceID, LineNo, "2", FirstDate, _C._InvoiceText, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _InvoiceTotal, 0)

                Else

                    'some nurseries require the full breakdown, this requires a different invoice layout file
                    'i.e. PRDN.

                    Dim _CalcFormat As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALCALC")
                    Dim _CalcFundingFormat As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALFUNDCALC")
                    Dim _Text As String = ""
                    Dim _Value As Decimal = 0

                    _Value = SetValue("+", _C._CalcAnnum)
                    _Text = _C.BuildLine(_CalcFormat, _C._Units, _C._Multiplier, _C._Rate, _C._Per, _C._Weeks, _C._CalcAnnum, _C._CalcMonth, _C._CalcWeek, _C._CalcDay)
                    CreateLine(InvoiceID, LineNo, "0", FirstDate, _Text, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Value, 0)

                    If _C._C2Sign <> "" Then
                        LineNo += 1
                        _Value = SetValue(_C._C2Sign, _C._C2CalcAnnum)
                        _Text = _C.BuildLine(_CalcFormat, _C._C2Units, _C._C2Multiplier, _C._C2Rate, _C._C2Per, _C._C2Weeks, _C._C2CalcAnnum, _C._C2CalcMonth, _C._C2CalcWeek, _C._C2CalcDay)
                        CreateLine(InvoiceID, LineNo, "0", FirstDate, _Text, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Value, 0)
                    End If

                    If _C._C3Sign <> "" Then
                        LineNo += 1
                        _Value = SetValue(_C._C3Sign, _C._C3CalcAnnum)
                        _Text = _C.BuildLine(_CalcFormat, _C._C3Units, _C._C3Multiplier, _C._C3Rate, _C._C3Per, _C._C3Weeks, _C._C3CalcAnnum, _C._C3CalcMonth, _C._C3CalcWeek, _C._C3CalcDay)
                        CreateLine(InvoiceID, LineNo, "0", FirstDate, _Text, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Value, 0)
                    End If

                    If _C._C4Sign <> "" Then
                        LineNo += 1
                        _Value = SetValue(_C._C4Sign, _C._C4CalcAnnum)
                        _Text = _C.BuildLine(_CalcFormat, _C._C4Units, _C._C4Multiplier, _C._C4Rate, _C._C4Per, _C._C4Weeks, _C._C4CalcAnnum, _C._C4CalcMonth, _C._C4CalcWeek, _C._C4CalcDay)
                        CreateLine(InvoiceID, LineNo, "0", FirstDate, _Text, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Value, 0)
                    End If

                    If _C._C5Sign <> "" Then
                        LineNo += 1
                        _Value = SetValue(_C._C5Sign, _C._C5CalcAnnum)
                        _Text = _C.BuildLine(_CalcFormat, _C._C5Units, _C._C5Multiplier, _C._C5Rate, _C._C5Per, _C._C5Weeks, _C._C5CalcAnnum, _C._C5CalcMonth, _C._C5CalcWeek, _C._C5CalcDay)
                        CreateLine(InvoiceID, LineNo, "0", FirstDate, _Text, _ID.ToString, "", "", Invoicing.ReturnNLCode(ChildRecord, ""), Invoicing.ReturnNLTracking(ChildRecord, ""), 0, _Value, 0)
                    End If

                End If

                LineNo += 1

                Return True

            End If

        End If

        Return False

    End Function

    Private Shared Function SetValue(ByVal Sign As String, ByVal ValueIn As Decimal) As Decimal
        If Sign = "-" Then
            Return ValueIn * -1
        Else
            Return ValueIn
        End If
    End Function

    Private Shared Function AverageInvoiceProcessed(ByVal InvoiceID As Guid, ByRef ChildRecord As Business.Child, ByRef LineNo As Long,
                                                    ByVal FirstDate As Date, ByVal LastDate As Date, ByRef AnnualInvoiceID As Guid?) As Boolean

        Dim _Return As Boolean = False

        'check if there is an average record created covering the invoice period
        Dim _SQL As String = ""

        _SQL += "select split_batch, annual_invoice from ChildAnnual"
        _SQL += " where child_id = '" + ChildRecord._ID.Value.ToString + "'"
        _SQL += " and " + ValueHandler.SQLDate(FirstDate, True) + " between date_from and date_to"
        _SQL += " and active = 1"
        _SQL += " order by date_from desc"

        Dim _DRAnnual As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRAnnual IsNot Nothing Then

            If _DRAnnual.Item("annual_invoice").ToString <> "" Then
                AnnualInvoiceID = New Guid(_DRAnnual.Item("annual_invoice").ToString)
            End If

            If _DRAnnual.Item("split_batch").ToString <> "" Then

                Dim _BatchID As Guid = New Guid(_DRAnnual.Item("split_batch").ToString)

                _SQL = ""
                _SQL += "select ID from Invoices"
                _SQL += " where batch_id = '" + _BatchID.ToString + "'"
                _SQL += " and invoice_period = " + ValueHandler.SQLDate(FirstDate, True)

                Dim _DRInvoice As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                If _DRInvoice IsNot Nothing Then

                    If _DRInvoice.Item("ID").ToString <> "" Then

                        Dim _Inv As Business.Invoice = Business.Invoice.RetreiveByID(New Guid(_DRInvoice.Item("ID").ToString))
                        If _Inv IsNot Nothing Then

                            If LineNo = 0 Then LineNo = 1

                            'fetch the lines (but there should only be one)
                            For Each _Line As Business.InvoiceLine In Business.InvoiceLine.RetreiveAllByInvoiceID(_Inv._ID.Value)
                                CreateLine(InvoiceID, LineNo, "2", _Inv._InvoiceDate, _Line._Description, "", "", "", _Line._NlCode, _Line._NlTracking, _Line._Hours, _Line._Value, 0)
                                LineNo += 1
                            Next

                            _Return = True

                        End If

                    End If

                End If

            End If

        End If

        Return _Return

    End Function

    Public Shared Function GenerateOneLineInvoice(ByVal BatchID As Guid, ByVal BatchNo As Long, ByVal FamilyID As Guid, ByVal ChildID As Guid?,
                                                  ByVal InvoiceDate As Date, ByVal PeriodFrom As Date?, ByVal PeriodTo As Date?, ByVal InvoiceLayout As String,
                                                  ByVal LineDescription As String, ByVal LineValue As Decimal,
                                                  ByRef InvoiceNumber As Integer) As Guid

        Dim _Return As Guid = Nothing
        Dim _Family As Business.Family = Business.Family.RetreiveByID(FamilyID)
        If _Family IsNot Nothing Then

            Dim _InvoiceID As Guid = Guid.NewGuid

            Dim _ChildRecord As Business.Child = Nothing
            Dim _ChildName As String = ""
            Dim _Class As String = ""
            Dim _NLCode As String = FinanceShared.DefaultNLCode 'use default NL code
            Dim _NLTracking As String = ""

            If ChildID.HasValue Then
                _ChildRecord = Business.Child.RetreiveByID(ChildID.Value)
                _ChildName = _ChildRecord._Fullname
                _Class = _ChildRecord._Class
                _NLCode = Invoicing.ReturnNLCode(_ChildRecord, "")
                _NLTracking = Invoicing.ReturnNLTracking(_ChildRecord, "")
            Else
                'no child, so we default from the family site
                If _Family._SiteId.HasValue Then
                    Dim _S As Business.Site = Business.Site.RetreiveByID(_Family._SiteId.Value)
                    If _S IsNot Nothing Then
                        _NLCode = _S._NlCode
                        _NLTracking = _S._NlTracking
                    End If
                End If
            End If

            If LineDescription <> "" Then
                CreateLine(_InvoiceID, 1, "2", InvoiceDate, LineDescription, FamilyID.ToString, "", "", _NLCode, _NLTracking, 0, LineValue, 0)
            End If

            Dim _Totals As InvoiceTotals = ReturnInvoiceTotals(_InvoiceID, _ChildRecord, EnumAnnualised.None, PeriodTo)
            If _Totals IsNot Nothing Then

                'increment the invoice number
                InvoiceNumber += 1

                'create an invoice
                Dim _Inv As New Business.Invoice
                With _Inv

                    ._ID = _InvoiceID
                    ._BatchId = BatchID
                    ._InvoiceNo = InvoiceNumber
                    ._InvoiceRef = BatchNo.ToString + "-" + InvoiceNumber.ToString
                    ._InvoiceStatus = "Generated"
                    ._InvoiceDate = InvoiceDate

                    ._InvoicePeriod = PeriodFrom
                    ._InvoicePeriodEnd = PeriodTo

                    ._ChildId = ChildID
                    ._ChildName = _ChildName

                    ._FamilyId = _Family._ID
                    ._FamilyName = _Family._LetterName
                    ._FamilyAccountNo = _Family._AccountNo

                    ._Address = _Family._Address
                    ._Postcode = _Family._Postcode

                    'discount detail
                    ._Discountable = 0
                    ._DiscountId = Nothing
                    ._DiscountName = ""

                    'invoice totals
                    ._InvoiceSub = _Totals.SubTotal
                    ._InvoiceDiscount = _Totals.DiscountAmount
                    ._InvoiceTotal = _Totals.Total

                    'external id (the customer ID in the financials application)
                    ._FinancialsId = _Family._FinancialsId

                    ._Class = _Class
                    ._InvoiceDue = _Family._InvoiceDue
                    ._InvoiceDueDate = ReturnInvoiceDueDate(_Family._InvoiceDue, InvoiceDate, PeriodFrom)
                    ._InvoiceLayout = HandleInvoiceLayout(InvoiceLayout)

                    ._Xcheck = 0
                    ._XcheckFrom = Nothing
                    ._XcheckTo = Nothing
                    ._XcheckThresh = 0
                    ._XcheckPerHour = 0
                    ._XcheckAdditional = False
                    ._XcheckCount = 0
                    ._XcheckAmount = 0

                    ProcessVouchers(_Inv, _ChildRecord)

                    .Store()

                    'return the InvoiceID
                    _Return = _InvoiceID

                End With

                _Inv = Nothing

            End If

            _Family = Nothing

        End If

        Return _Return

    End Function

    Public Shared Function HandleInvoiceLayout(ByVal InvoiceLayout As String) As String

        If InvoiceLayout <> "" Then Return InvoiceLayout

        Dim _InvoiceFile As String = ParameterHandler.ReturnString("INVOICEFILE")
        If _InvoiceFile <> "" Then Return _InvoiceFile

        Return "InvoiceWithSummary.repx"

    End Function

    Public Shared Function GenerateInvoice(ByVal BatchID As Guid, ByVal BatchNo As Long, ByVal ChildID As Guid, ByVal InvoiceDate As Date,
                                            ByVal FirstDate As Date, ByVal LastDate As Date,
                                            ByRef InvoiceNumber As Integer,
                                            ByVal CreatingAnnualInvoice As Boolean, ByVal InvoiceLayout As String, ByVal AddtionalSessionMode As EnumAdditionalSessionMode,
                                            ByVal DuplicateMode As Invoicing.EnumDuplicateMode, ByVal TariffFilter As String,
                                            ByVal GenerationMode As Invoicing.EnumGenerationMode, ByVal AdditionalFrom As Date?, ByVal AddtionalTo As Date?) As Guid?

        'used for generating invoices that have no x-check
        Return GenerateInvoice(BatchID, BatchNo, ChildID, InvoiceDate, FirstDate, LastDate, InvoiceNumber, CreatingAnnualInvoice, InvoiceLayout,
                               EnumXCheckMode.None, Nothing, Nothing, 0, 0, False, AddtionalSessionMode, DuplicateMode, TariffFilter,
                               GenerationMode, AdditionalFrom, AddtionalTo, False)

    End Function

    Public Shared Function GenerateInvoice(ByVal BatchID As Guid, ByVal BatchNo As Long, ByVal ChildID As Guid, ByVal InvoiceDate As Date,
                                            ByVal FirstDate As Date, ByVal LastDate As Date,
                                            ByRef InvoiceNumber As Integer,
                                            ByVal CreatingAnnualInvoice As Boolean, ByVal InvoiceLayout As String,
                                            ByVal XCheck As Invoicing.EnumXCheckMode, ByVal XCheckFrom As Date?, ByVal XCheckTo As Date?,
                                            ByVal XCheckThreshold As Decimal, ByVal XCheckPerHour As Decimal, ByVal XCheckAdditional As Boolean,
                                            ByVal AddtionalSessionMode As EnumAdditionalSessionMode,
                                            ByVal DuplicateMode As Invoicing.EnumDuplicateMode, ByVal TariffFilter As String,
                                            ByVal GenerationMode As Invoicing.EnumGenerationMode, ByVal AdditionalFrom As Date?, ByVal AdditionalTo As Date?,
                                            ByVal Forecasting As Boolean) As Guid?

        Dim _InvoiceID As Guid? = Guid.NewGuid

        'store these dates as these are the "period" dates, the actual dates might be different
        'due to starting/leaving dates and are returned from GenerateInvoiceLines
        Dim _PeriodFirst As Date = FirstDate
        Dim _PeriodEnd As Date = LastDate

        Dim _ActualFirst As Date = FirstDate
        Dim _ActualEnd As Date = LastDate

        Dim _Annualised As Invoicing.EnumAnnualised = EnumAnnualised.None
        Dim _AnnualID As Guid? = Nothing
        Dim _Child As Business.Child = Nothing

        If GenerateInvoiceLines(_InvoiceID.Value, ChildID, _ActualFirst, _ActualEnd, CreatingAnnualInvoice,
                                XCheck, XCheckFrom, XCheckTo, XCheckThreshold, XCheckPerHour, XCheckAdditional,
                                _Annualised, _AnnualID, AddtionalSessionMode, DuplicateMode, TariffFilter, _Child, AdditionalFrom, AdditionalTo, Forecasting) Then

            If _Child IsNot Nothing Then

                Dim _Family As Business.Family = Business.Family.RetreiveByID(_Child._FamilyId.Value)
                If _Family IsNot Nothing Then

                    Dim _Totals As InvoiceTotals = ReturnInvoiceTotals(_InvoiceID.Value, _Child, _Annualised, _ActualEnd)
                    If _Totals IsNot Nothing Then

                        'increment the invoice number
                        InvoiceNumber += 1

                        'create an invoice
                        Dim _Inv As New Business.Invoice
                        With _Inv

                            ._ID = _InvoiceID
                            ._BatchId = BatchID
                            ._InvoiceNo = InvoiceNumber
                            ._InvoiceRef = BatchNo.ToString + "-" + InvoiceNumber.ToString
                            ._InvoiceStatus = "Generated"
                            ._InvoiceDate = InvoiceDate

                            ._InvoicePeriod = _PeriodFirst
                            ._InvoicePeriodEnd = _PeriodEnd

                            ._ChildId = _Child._ID
                            ._ChildName = _Child._Fullname

                            ._FamilyId = _Family._ID
                            ._FamilyName = _Family._LetterName
                            ._FamilyAccountNo = _Family._AccountNo

                            ._Address = _Family._Address
                            ._Postcode = _Family._Postcode

                            'discount detail
                            ._Discountable = _Totals.Discountable
                            ._DiscountId = _Totals.DiscountID
                            ._DiscountName = _Totals.DiscountText

                            'invoice totals
                            ._InvoiceSub = _Totals.SubTotal
                            ._InvoiceDiscount = _Totals.DiscountAmount
                            ._InvoiceTotal = _Totals.Total

                            'external id (the customer ID in the financials application)
                            ._FinancialsId = _Family._FinancialsId

                            ._Class = _Child._Class

                            ._InvoiceDue = _Family._InvoiceDue
                            ._InvoiceDueDate = ReturnInvoiceDueDate(_Family._InvoiceDue, InvoiceDate, FirstDate)

                            ._AnnualType = _Annualised.ToString
                            If _Annualised = EnumAnnualised.None Then
                                ._InvoiceAnnualised = False
                            Else
                                ._InvoiceAnnualised = True
                            End If

                            ._InvoiceLayout = HandleInvoiceLayout(InvoiceLayout)

                            'cross-check
                            ._Xcheck = XCheck
                            ._XcheckFrom = XCheckFrom
                            ._XcheckTo = XCheckTo
                            ._XcheckThresh = XCheckThreshold
                            ._XcheckPerHour = XCheckPerHour
                            ._XcheckAdditional = XCheckAdditional

                            ._XcheckCount = _Totals.XCheckCount
                            ._XcheckAmount = _Totals.XCheckAmount

                            ._AdditionalSessions = CByte(AddtionalSessionMode)

                            ._DuplicateMode = CByte(DuplicateMode)
                            ._TariffFilter = TariffFilter

                            'set the starter and leaver information if they fall within the invoice period
                            If _Child._Started.HasValue Then
                                If _Child._Started.Value >= FirstDate AndAlso _Child._Started.Value <= LastDate Then
                                    ._StarterDate = _Child._Started.Value
                                End If
                            End If

                            If _Child._DateLeft.HasValue Then
                                If _Child._DateLeft.Value >= FirstDate AndAlso _Child._DateLeft.Value <= LastDate Then
                                    ._LeaverDate = _Child._DateLeft.Value
                                End If
                            End If

                            ._AdditionalFrom = AdditionalFrom
                            ._AdditionalTo = AdditionalTo
                            ._GenerationMode = CByte(GenerationMode)

                            ProcessVouchers(_Inv, _Child)

                            .Store()

                        End With

                        'generate invoice days entries
                        '*****************************************************************************************************************************
                        Invoicing.GenerateInvoiceDays(_InvoiceID.Value, _Annualised, _AnnualID, _Child._Forename, _Child._Surname)
                        '*****************************************************************************************************************************

                        _Inv = Nothing

                    End If

                    _Family = Nothing

                End If

                _Child = Nothing

            End If

        Else
            _InvoiceID = Nothing
        End If

        Return _InvoiceID

    End Function

    Public Shared Function ReturnInvoiceDueDate(ByVal InvoiceDueMode As String, ByVal InvoiceDate As Date, ByVal InvoicePeriod As Date?) As Date

        Dim _Return As Date = InvoiceDate

        If InvoicePeriod.HasValue Then

            Dim _InvoicePeriodNextMonth As Date = InvoicePeriod.Value.AddMonths(1)
            Dim _InvoicePeriodPreviousMonth As Date = InvoicePeriod.Value.AddMonths(-1)
            Select Case InvoiceDueMode

                Case "", "x days from invoice date"
                    _Return = InvoiceDate.AddDays(InvoiceDueDays)

                Case "1st of the invoiced month"
                    _Return = DateSerial(InvoicePeriod.Value.Year, InvoicePeriod.Value.Month, 1)

                Case "1st of the following invoiced month"
                    _Return = DateSerial(_InvoicePeriodNextMonth.Year, _InvoicePeriodNextMonth.Month, 1)

                Case "28th of the invoiced month"
                    _Return = DateSerial(InvoicePeriod.Value.Year, InvoicePeriod.Value.Month, 28)

                Case "28th of the following invoiced month"
                    _Return = DateSerial(_InvoicePeriodNextMonth.Year, _InvoicePeriodNextMonth.Month, 28)

                Case "Last day of the invoiced month"
                    _Return = DateSerial(InvoicePeriod.Value.Year, InvoicePeriod.Value.Month, Date.DaysInMonth(InvoicePeriod.Value.Year, InvoicePeriod.Value.Month))

                Case "Last day of the following invoiced month"
                    _Return = DateSerial(_InvoicePeriodNextMonth.Year, _InvoicePeriodNextMonth.Month, Date.DaysInMonth(_InvoicePeriodNextMonth.Year, _InvoicePeriodNextMonth.Month))

                Case "Last day of the previous invoiced month"
                    _Return = DateSerial(_InvoicePeriodPreviousMonth.Year, _InvoicePeriodPreviousMonth.Month, Date.DaysInMonth(_InvoicePeriodPreviousMonth.Year, _InvoicePeriodPreviousMonth.Month))

                Case Else
                    CareMessage("Return Invoice Due Date: Unhandled Mode!")

            End Select

        Else
            _Return = InvoiceDate.AddDays(InvoiceDueDays)
        End If

        Return _Return

    End Function

    Private Shared Function ReturnInvoiceTotals(ByVal InvoiceID As Guid, ByRef ChildRecord As Business.Child,
                                                ByVal AnnualisedType As Invoicing.EnumAnnualised, ByVal PeriodEndDate As Date?) As InvoiceTotals

        Dim _DiscountID As Guid? = Nothing
        Dim _DiscountName As String = ""
        Dim _DiscountPcnt As Decimal = 0

        'it is possible for the ChildRecord to be nothing, i.e. Opening Balance invoices etc.
        If ChildRecord IsNot Nothing Then
            _DiscountID = ChildRecord._Discount
        End If

        'if we are producing an invoice with an annual calculation defined, the discount will already have been taken off on the annual amount
        If AnnualisedType = EnumAnnualised.AnnualCalculationFromPatterns Then
            'we therefore do not take it off again...
        Else
            'check if we are giving a percentage discount off the invoice total
            If _DiscountID.HasValue Then
                Dim _D As Business.Discount = Business.Discount.RetreiveByID(_DiscountID.Value)
                If _D IsNot Nothing Then
                    If _D._DiscountType = "Percentage" AndAlso _D._DiscountValue > 0 Then
                        _DiscountID = _DiscountID
                        _DiscountName = _D._Name
                        _DiscountPcnt = _D._DiscountValue
                    End If
                    _D = Nothing
                End If
            End If
        End If

        'calculate the line sub-total
        Dim _SubTotal As Decimal = 0
        Dim _AdditionalHoursValue As Decimal = 0
        Dim _AdditionalHoursCount As Integer = 0
        Dim _Discountable As Decimal = 0

        Dim _SQL As String = ""

        If AnnualisedType = EnumAnnualised.ManualCalculation AndAlso m_ManualCalculationLines Then
            If ChildRecord IsNot Nothing AndAlso PeriodEndDate.HasValue Then
                Dim _ID As Guid? = ReturnManualCalculationID(ChildRecord._ID.Value, PeriodEndDate.Value)
                If _ID.HasValue Then
                    Dim _Calc As Business.ChildCalculation = Business.ChildCalculation.RetreiveByID(_ID.Value)
                    If _Calc IsNot Nothing Then
                        _SubTotal = (_Calc._TotalCalcAnnum - _Calc._FundCalcAnnum) / Invoicing.ReturnSplitInto(_Calc._SplitType)
                        _Discountable = _SubTotal
                    End If
                End If
            End If
        Else

            _SQL = "select isnull(sum(value),0) as 'value' from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "'"
            _SubTotal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            _SQL = "select isnull(sum(discountable),0) as 'discountable' from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "'"
            _Discountable = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        End If

        _SQL = "select isnull(sum(value),0) as 'value' from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "'"
        _SQL += " and section = '4'"

        _AdditionalHoursValue = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        _SQL = "select count(*) as 'count' from InvoiceLines where invoice_id = '" + InvoiceID.ToString + "'"
        _SQL += " and section = '4'"

        _AdditionalHoursCount = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        Dim _T As New InvoiceTotals
        With _T

            .SubTotal = _SubTotal

            If _DiscountPcnt > 0 Then
                .DiscountID = _DiscountID
                .DiscountText = _DiscountName
                .DiscountAmount = (_Discountable / 100 * _DiscountPcnt) * -1
            Else
                .DiscountID = Nothing
                .DiscountText = ""
                .DiscountAmount = 0
            End If

            .Total = .SubTotal + .DiscountAmount

            .XCheckCount = _AdditionalHoursCount
            .XCheckAmount = _AdditionalHoursValue

        End With

        _SQL = ""
        _SQL += "select sum(hours_non_funded) as 'hours_non_funded', sum(hours_funded) as 'hours_funded',"
        _SQL += " sum(value_non_funded) as 'value_non_funded', sum(value_funded) as 'value_funded', sum(value_boltons) as 'value_boltons'"
        _SQL += " from InvoiceDays"
        _SQL += " where invoice_id = '" + InvoiceID.ToString + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            With _T
                .HoursNonFunded = ValueHandler.ConvertDecimal(_DR.Item("hours_non_funded"))
                .HoursFunded = ValueHandler.ConvertDecimal(_DR.Item("hours_funded"))
                .TotalNonFunded = ValueHandler.ConvertDecimal(_DR.Item("value_non_funded"))
                .TotalFunded = ValueHandler.ConvertDecimal(_DR.Item("value_funded"))
                .TotalBoltOns = ValueHandler.ConvertDecimal(_DR.Item("value_boltons"))
            End With
        End If

        Return _T

    End Function

    Private Class InvoiceTotals
        Property SubTotal As Decimal
        Property DiscountID As Guid?
        Property DiscountText As String
        Property DiscountAmount As Decimal
        Property Total As Decimal
        Property XCheckCount As Integer
        Property XCheckAmount As Decimal
        Property Discountable As Decimal
        Property HoursNonFunded As Decimal
        Property HoursFunded As Decimal
        Property TotalNonFunded As Decimal
        Property TotalFunded As Decimal
        Property TotalBoltOns As Decimal
    End Class

    Private Shared Sub IncludeAdHocDays(ByRef LineNo As Integer, ByRef SubTotal As Decimal, ByVal ChildID As Guid, ByVal PeriodMonth As Integer, ByVal PeriodYear As Integer)

        Dim _PreviousMonth As Integer = 0
        Dim _PreviousYear As Integer = 0

        If PeriodMonth = 1 Then
            _PreviousMonth = 12
            _PreviousYear = PeriodYear - 1
        Else
            _PreviousMonth = PeriodMonth - 1
            _PreviousYear = PeriodYear
        End If

        Dim _PreviousMonthStart As String = Format(DateSerial(_PreviousYear, _PreviousMonth, 1), "yyyy-MM-dd")
        Dim _PeriodMonthStart As String = Format(DateSerial(PeriodYear, PeriodMonth, 1), "yyyy-MM-dd")

        Dim _SQL As String = "select distinct date from Register" &
                             " where person_id = '" & ChildID.ToString & "'" &
                             " and Register.date >= '" & _PreviousMonthStart & "' and Register.date < '" & _PeriodMonthStart & "'" &
                             " and in_out = 'I'" &
                             " and Register.date not in (select action_date from InvoiceLines where InvoiceLines.ref_1 <> '' and InvoiceLines.ref_1 = Register.person_id)"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            'loop through each day
            For Each _DR As DataRow In _DT.Rows

                Dim _D As Date? = ValueHandler.ConvertDate(_DR.Item("date"))
                Dim _CheckIn As Date? = Nothing
                Dim _CheckOut As Date? = Nothing

                SetHours(ChildID, _D, _CheckIn, _CheckOut)
                Dim _Tariff As Business.Tariff = Business.Tariff.GetTariffFromHours(_CheckIn, _CheckOut)

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Shared Sub SetHours(ByVal ChildID As Guid, ByVal RegisterDate As Date?, ByRef CheckIn As Date?, ByRef CheckOut As Date?)

        'get the first checkin
        CheckIn = GetTime(ChildID, RegisterDate, True)

        'get the last checkout
        CheckOut = GetTime(ChildID, RegisterDate, False)

    End Sub

    Private Shared Function GetTime(ByVal ChildID As Guid?, ByVal RegisterDate As Date?, ByVal CheckIn As Boolean) As Date?

        Dim _Return As Date? = Nothing
        Dim _Field As String = ""
        Dim _Order As String = ""
        Dim _InOut As String = ""

        If CheckIn Then
            _Field = "stamp_in"
            _Order = "asc"
            _InOut = "I"
        Else
            _Field = "stamp_out"
            _Order = "desc"
            _InOut = "O"
        End If

        Dim _SQL As String = "select top 1 " + _Field + " from Register" &
                             " where person_id = '" + ChildID.Value.ToString + "'" &
                             " and date = '" + Format(RegisterDate, "yyyy-MM-dd") + "'" &
                             " and in_out = '" + _InOut + "'" &
                             " order by " + _Field + " " + _Order

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = ValueHandler.ConvertDate(_DR.Item(0))
        End If

        Return _Return

    End Function

    Private Shared Sub CreateLine(ByVal InvoiceID As Guid, ByVal LineNo As Long, ByVal Section As String, ByVal InvoiceDate As Date?, ByVal Description As String,
                                  ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String,
                                  ByVal NLCode As String, ByVal NLTracking As String,
                                  ByVal Hours As Decimal, ByVal Value As Decimal, ByVal OriginalHours As Decimal)

        Dim _Line As New Business.InvoiceLine
        With _Line
            ._InvoiceId = InvoiceID
            ._LineNo = LineNo
            ._ActionDate = InvoiceDate
            ._Description = Description
            ._Ref1 = Ref1
            ._Ref2 = Ref2
            ._Ref3 = Ref3
            ._Value = Value
            ._Section = Section
            ._Hours = Hours
            ._NlCode = NLCode
            ._NlTracking = NLTracking
            ._XcheckOrigHours = OriginalHours
            ._Discountable = Value
            ._SummaryColumn = SetSummaryColumn(Section, ._SummaryColumn)
            .Store()
        End With

        _Line = Nothing

    End Sub

    Public Shared Function SetSummaryColumn(ByVal Section As String, ByVal SummaryColumn As String) As String

        If SummaryColumn = "" Then

            '1 = FEEE
            If Section = "1" Then Return "0"

            '2 = Chargable Session
            If Section = "2" Then Return "1"

            '3 = Additional Charges, these will be listed on the invoice, so we return X
            If Section = "3" Then Return "X"

            '4 = Additional Sessions, these will be listed on the invoice, so we return X
            If Section = "4" Then Return "X"

            '5 = Bolt-Ons
            If Section = "5" Then Return "9"

            'treat anything else as a chargeable session AKA Column 1
            Return "1"

        Else
            Return SummaryColumn
        End If

    End Function

    Private Shared Sub CreateLine(ByVal InvoiceID As Guid, ByRef LineNo As Long, ByVal Child As Business.Child, ByVal SessionData As SessionDataObject, ByVal ActionDate As Date, ByVal ChildHolidays As List(Of Business.ChildHoliday))

        If Not SessionData.IsPopulated Then Exit Sub

        Dim _Line As Business.InvoiceLine = Nothing

        Dim _IsClosed As Boolean = Bookings.IsClosed(ActionDate)
        Dim _DateCategory As Bookings.EnumDateCategory = Bookings.ReturnDateCategory(ActionDate, Child._TermType)

        Dim _Excluded As Bookings.EnumExcluded = Bookings.IsDateExcluded(ActionDate, Child, ChildHolidays, Bookings.EnumCheckMode.Invoicing, _DateCategory)
        If _Excluded = Bookings.EnumExcluded.NotExcluded OrElse _Excluded = Bookings.EnumExcluded.ChildOnHoliday Then

            If LineNo = 0 Then LineNo += 1

            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            'check if we need to create an additional line to cover the FEEE records that can be generated by providing the hours
            If SessionData.FEEE > 0 Then

                _Line = New Business.InvoiceLine
                With _Line
                    ._InvoiceId = InvoiceID
                    ._LineNo = LineNo
                    ._ActionDate = ActionDate
                    ._Description = SessionData.FEEEDescription
                    ._Ref1 = Child._ID.ToString
                    ._Ref2 = Child._Fullname
                    ._Ref3 = SessionData.TariffID.Value.ToString
                    ._Value = 0
                    ._Section = "1" 'hardcode to FEEE section
                    ._Hours = ValueHandler.ConvertDecimal(SessionData.FEEE)
                    ._NlCode = SessionData.NLCode
                    ._NlTracking = SessionData.NLTracking
                    ._XcheckOrigHours = 0
                    ._Discountable = 0
                    ._PhNonfunded = SessionData.NonFundedRate
                    ._PhFunded = SessionData.FundedRate
                    ._ValueFunded = SessionData.FEEEValue
                    ._SummaryColumn = "0"

                    .Store()

                End With

                LineNo += 1

            End If

            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            'because of the above FEEE adjustments, this line might actually have been adjusted to zero.
            'i.e. 10 hour day with 10 hours of FEEE
            'so we check if the hours are > 0

            If SessionData.Hours > 0 Then

                _Line = New Business.InvoiceLine
                With _Line

                    ._InvoiceId = InvoiceID
                    ._LineNo = LineNo
                    ._ActionDate = ActionDate
                    ._Description = SessionData.Description
                    ._Ref1 = Child._ID.ToString
                    ._Ref2 = Child._Fullname
                    ._Ref3 = SessionData.TariffID.Value.ToString
                    ._Value = ValueHandler.ConvertDecimal(SessionData.TotalValue)
                    ._Section = SessionData.Section
                    ._Hours = ValueHandler.ConvertDecimal(SessionData.Hours)
                    ._NlCode = SessionData.NLCode
                    ._NlTracking = SessionData.NLTracking
                    ._XcheckOrigHours = 0

                    If SessionData.DiscountPermitted Then
                        ._Discountable = ._Value
                    Else
                        ._Discountable = 0
                    End If

                    ._PhNonfunded = SessionData.NonFundedRate
                    ._PhFunded = 0
                    ._ValueFunded = 0

                    ._SummaryColumn = SessionData.SummaryColumn

                    .Store()

                End With

                LineNo += 1

            End If

            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            If SessionData.BoltOns.Count > 0 Then

                For Each _B As BoltOn In SessionData.BoltOns

                    'check this bolton is permitted
                    If BoltOnPermitted(_DateCategory, _Excluded, _B, _IsClosed) Then

                        _Line = New Business.InvoiceLine
                        With _Line

                            ._InvoiceId = InvoiceID
                            ._LineNo = LineNo
                            ._ActionDate = ActionDate
                            ._Description = _B.Name
                            ._Ref1 = Child._ID.ToString
                            ._Ref2 = Child._Fullname
                            ._Ref3 = _B.ID.ToString
                            ._Value = ValueHandler.ConvertDecimal(_B.Price)
                            ._Section = "5"
                            ._Hours = 0
                            ._NlCode = _B.NLCode
                            ._NlTracking = _B.NLTracking
                            ._XcheckOrigHours = 0

                            If _B.Discount Then
                                ._Discountable = ._Value
                            Else
                                ._Discountable = 0
                            End If

                            ._PhNonfunded = 0
                            ._PhFunded = 0
                            ._ValueFunded = 0

                            ._SummaryColumn = _B.SummaryColumn

                            .Store()

                        End With

                        LineNo += 1

                    End If

                Next

            End If

            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            _Line = Nothing

        End If

    End Sub

    Private Shared Function BoltOnPermitted(ByVal DateCategory As Bookings.EnumDateCategory, Excluded As Bookings.EnumExcluded, BoltOnRecord As BoltOn, ByVal IsClosed As Boolean) As Boolean

        'globally closed
        If BoltOnRecord.ChargeClosed = False AndAlso IsClosed Then Return False

        'child on holiday
        If BoltOnRecord.ChargeHolidays = False AndAlso Excluded = Bookings.EnumExcluded.ChildOnHoliday Then Return False

        'bolt-on disabled
        If BoltOnRecord.RecurringScope = 0 Then Return False

        'term-time only
        If BoltOnRecord.RecurringScope = 2 AndAlso DateCategory <> Bookings.EnumDateCategory.TermDate Then Return False

        'holidays only
        If BoltOnRecord.RecurringScope = 3 AndAlso DateCategory = Bookings.EnumDateCategory.TermDate Then Return False

        Return True

    End Function

    Public Shared Function CreateFinancialsDocumentFromInvoiceID(ByVal OurDocumentID As Guid, ByRef Document As Care.Financials.Document) As Integer

        Dim _TrackingCategory As String = FinanceShared.TrackingCategory

        'fetch our document
        Dim _Doc As Business.Invoice = Business.Invoice.RetreiveByID(OurDocumentID)
        If _Doc Is Nothing Then Return -1

        'external ID is not on the document...
        If _Doc._FinancialsId = "" Then Return -2
        If _Doc._FinancialsId = "00000000-0000-0000-0000-000000000000" Then Return -2

        Dim _DocType As Care.Financials.Document.EnumDocumentType = Care.Financials.Document.EnumDocumentType.Sales

        If _Doc._InvoiceTotal < 0 Then
            _DocType = Care.Financials.Document.EnumDocumentType.SalesCreditNote
        End If

        Dim _ExtDocument As New Care.Financials.Document
        With _ExtDocument
            .DateCreated = _Doc._InvoiceDate.Value
            .DateDue = _Doc._InvoiceDueDate.Value
            .EntityID = _Doc._FinancialsId
            .DocumentType = _DocType
            .Reference = _Doc._InvoiceNo.ToString
            .Description = _Doc._ChildName + " - " + Format(_Doc._InvoicePeriod, "MMMM yyyy")
        End With

        Dim _ExtItems As New List(Of Care.Financials.DocumentItem)
        For Each _Line As Business.InvoiceLine In Business.InvoiceLine.RetreiveAllByInvoiceID(_Doc._ID.Value)

            'check we have a nominal code
            If _Line._NlCode = "" Then Return -3

            'check we have tracking (if enabled)
            If FinanceShared.TrackingCategory <> "" AndAlso _Line._NlTracking = "" Then Return -4

            Dim _FIL As New Care.Financials.DocumentItem
            With _FIL
                .Description = Format(_Line._ActionDate, "dd/MM/yyyy") + " - " + _Line._Description
                .NLCode = _Line._NlCode
                .NLTrackingCategory = _TrackingCategory
                .NLTracking = _Line._NlTracking
                .Quantity = 1
                .UnitPrice = _Line._Value
                .VATAmount = 0
            End With

            _ExtItems.Add(_FIL)

        Next

        If _Doc._InvoiceDiscount <> 0 Then

            Dim _Child As Business.Child = Business.Child.RetreiveByID(_Doc._ChildId.Value)
            If _Child IsNot Nothing Then

                Dim _FIL As New Care.Financials.DocumentItem
                With _FIL

                    .Description = _Doc._DiscountName

                    .NLCode = ReturnNLCode(_Child, "")
                    .NLTrackingCategory = _TrackingCategory
                    If _TrackingCategory <> "" Then .NLTracking = ReturnNLTracking(_Child, "")

                    .Quantity = 1
                    .UnitPrice = _Doc._InvoiceDiscount
                    .VATAmount = 0

                End With

                _ExtItems.Add(_FIL)

            End If

        End If

        'add the lines to the document
        _ExtDocument.Items = _ExtItems

        Document = _ExtDocument
        Return 0

    End Function

    Public Shared Function ReturnNLCode(ByVal Child As Business.Child, ByVal ItemNLCode As String) As String
        'Left the calls the ReturnNLCode here to save doing a massive Find and Replace
        Return FinanceShared.ReturnNLCode(Child, ItemNLCode)
    End Function

    Public Shared Function ReturnNLTracking(ByVal Child As Business.Child, ByVal ItemNLTracking As String) As String
        'Left the calls the ReturnNLTracking here to save doing a massive Find and Replace
        Return FinanceShared.ReturnNLTracking(Child, ItemNLTracking)
    End Function

    Private Shared Function ReturnSessionModeFromByte(ByVal ByteIn As Byte) As EnumAdditionalSessionMode
        If ByteIn = 1 Then Return EnumAdditionalSessionMode.Included
        If ByteIn = 2 Then Return EnumAdditionalSessionMode.Only
        Return EnumAdditionalSessionMode.Excluded
    End Function

    Private Shared Function ReturnDuplicateModeFromByte(ByVal ByteIn As Byte) As EnumDuplicateMode
        If ByteIn = 1 Then Return EnumDuplicateMode.ExcludeAllInvoices
        If ByteIn = 2 Then Return EnumDuplicateMode.ExcludeOnlyPostedInvoices
        Return EnumDuplicateMode.Include
    End Function

    Public Shared Function ReturnSplitInto(ByVal SplitIntoText As String) As Integer

        'this will be blank (old data)
        '"month", so 12
        '"51 weeks"
        '"5 months"

        Select Case SplitIntoText

            Case "", "month"
                Return 12

            Case Else
                Dim _Number As String = SplitIntoText.Substring(0, SplitIntoText.IndexOf(" "))
                Return ValueHandler.ConvertInteger(_Number)
        End Select

    End Function

    Public Shared Sub GenerateInvoiceDays(ByVal InvoiceID As Guid, ByVal Annualised As Invoicing.EnumAnnualised, ByVal AnnualID As Guid?, ByVal ChildForename As String, ByVal ChildSurname As String)

        Dim _Invoice As Business.Invoice = Business.Invoice.RetreiveByID(InvoiceID)
        If _Invoice IsNot Nothing Then

            If _Invoice._ChildId.HasValue Then

                Dim _SQL As String = "delete from InvoiceDays where invoice_id = '" + InvoiceID.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Select Case Annualised

                    Case EnumAnnualised.None
                        'the source and target invoice are the same
                        GenerateDaysFromLines(_Invoice, _Invoice, ChildForename, ChildSurname)

                    Case EnumAnnualised.ManualCalculation
                        'this cannot be done at the moment

                    Case EnumAnnualised.AnnualCalculationFromPatterns
                        'the source is the annual invoice, and the target is "this invoice"
                        If AnnualID.HasValue Then
                            Dim _AnnualInvoice As Business.Invoice = Business.Invoice.RetreiveByID(AnnualID.Value)
                            If _AnnualInvoice IsNot Nothing Then
                                GenerateDaysFromLines(_Invoice, _AnnualInvoice, ChildForename, ChildSurname)
                            End If
                        End If

                End Select

            Else
                'could not find child record
            End If

        Else
            'could not find invoice
        End If

    End Sub

    Private Shared Sub GenerateDaysFromLines(ByVal TargetInvoice As Business.Invoice, ByVal SourceInvoice As Business.Invoice, ByVal ChildForename As String, ByVal ChildSurname As String)

        Dim _Days As New List(Of Business.InvoiceDay)

        'this routine might be called from a derived from patterns invoice, so we need to ensure we only accumulate the days within the invoice period
        'not the whole period defined in the derived from patterns calculation
        Dim _Lines As List(Of Business.InvoiceLine) = Business.InvoiceLine.RetreiveAllByInvoiceAndDateRange(SourceInvoice._ID.Value, TargetInvoice._InvoicePeriod.Value, TargetInvoice._InvoicePeriodEnd.Value)

        For Each _Line In _Lines

            'we need to run through all the invoice lines and work out the accumulated hours, costs etc.
            'once we have been through the lines, we go through the days and "fix" the funding

            Dim _IsNew As Boolean
            Dim _Day As Business.InvoiceDay = GetDay(_Days, _Line._ActionDate.Value)
            If _Day Is Nothing Then
                _IsNew = True
                _Day = New Business.InvoiceDay
                _Day._InvoiceBatch = TargetInvoice._BatchId
                _Day._InvoiceId = TargetInvoice._ID
                _Day._ChildId = TargetInvoice._ChildId
                _Day._InvoiceDate = _Line._ActionDate
                _Day._ChildForename = ChildForename
                _Day._ChildSurname = ChildSurname
            Else
                _IsNew = False
            End If

            With _Day

                'value gross = non-funded + funded + bolt-ons
                'value net   = (non-funded + bolt-ons) - funded

                Select Case _Line._Section

                    'funded
                    Case "1"

                        ._HoursFunded += _Line._Hours
                        ._ValueFunded += _Line._ValueFunded

                        ._HoursTotal += _Line._Hours
                        ._ValueGross += _Line._ValueFunded

                    'bolt-ons
                    Case "5"

                        ._ValueBoltons += _Line._Value
                        ._ValueGross += _Line._Value
                        ._ValueNet += _Line._Value

                    Case Else

                        ._HoursNonFunded += _Line._Hours
                        ._ValueNonFunded += _Line._Value

                        ._HoursTotal += _Line._Hours
                        ._ValueGross += _Line._Value
                        ._ValueNet += _Line._Value


                End Select

            End With

            If _IsNew Then
                _Days.Add(_Day)
            End If

        Next

        'now we "fix" the funding
        For Each _d In _Days
            _d._HoursNonFunded = _d._HoursNonFunded - _d._HoursFunded
            _d._HoursTotal = _d._HoursNonFunded + _d._HoursFunded
        Next

        'store the days
        Business.InvoiceDay.SaveAll(_Days)

    End Sub

    Private Shared Function GetDay(ByRef Days As List(Of Business.InvoiceDay), ByVal DateIn As Date) As Business.InvoiceDay
        If Days.Count = 0 Then
            Return Nothing
        Else
            For Each _Day In Days
                If _Day._InvoiceDate = DateIn Then
                    Return _Day
                End If
            Next
            Return Nothing
        End If
    End Function

    Private Shared Function ReturnSessionDate(ByVal childID As Guid, ByVal bookingDate As Date, returnDate As SessionDate) As Date

        Dim sql As String = ""
        sql = String.Concat("select booking_from, booking_to from Bookings",
                                " where booking_date = ", ValueHandler.SQLDate(bookingDate, True),
                                " and child_id = '", childID.ToString, "'")

        Dim row As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, sql)
        If row IsNot Nothing Then
            If returnDate = SessionDate.sessionStart Then
                Return CDate(Format(bookingDate, "dd/MM/yyyy") & " " & row.Item("booking_from").ToString)
            ElseIf returnDate = SessionDate.sessionEnd Then
                Return CDate(Format(bookingDate, "dd/MM/yyyy") & " " & row.Item("booking_to").ToString)
            End If
        End If
        Return Nothing

    End Function

    Private Shared Sub ProcessVouchers(ByRef Invoice As Business.Invoice, ByRef Child As Business.Child)

        Invoice._VoucherMode = Child._VoucherMode
        Invoice._VoucherProportion = Child._VoucherProportion
        Invoice._VoucherPool = 0
        Invoice._VoucherAmount = 0

        'get the maximum voucher value from all family contacts
        Dim _SQL As String = ""
        _SQL += "select sum(voucher_value) as 'pool' from Contacts"
        _SQL += " where family_ID = '" + Child._FamilyId.ToString + "'"
        _SQL += " and voucher = 1"

        Invoice._VoucherPool = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        'do we have a voucher pool?
        If Invoice._VoucherPool > 0 Then

            'are we taking a percentage or fixed amount from the pool?
            If Child._VoucherMode = "Fixed Percentage" Then
                Invoice._VoucherAmount = (Invoice._VoucherPool / 100) * Child._VoucherProportion
            Else
                Invoice._VoucherAmount = Child._VoucherProportion
            End If

            'ensure the voucher amount does not exceed the invoice total
            If Invoice._VoucherAmount > Invoice._InvoiceTotal Then Invoice._VoucherAmount = Invoice._InvoiceTotal

        End If

    End Sub

    Public Shared Function CreditInvoice(ByVal InvoiceToCredit As Guid, ByRef BatchNo As Integer) As Boolean

        Dim _I As Business.Invoice = Business.Invoice.RetreiveByID(InvoiceToCredit)
        If _I IsNot Nothing Then

            Dim _CH As New Business.CreditBatch
            With _CH
                ._BatchNo = Credits.LastBatchNo + 1
                ._BatchDate = Today
                ._BatchStatus = "Open"
                ._UserId = Session.CurrentUser.ID
                ._Stamp = Now
                .Store()
            End With

            'create a new credit
            Dim _CreditID As Guid = Guid.NewGuid
            Dim _C As New Business.Credit
            With _C
                ._ID = _CreditID
                ._BatchId = _CH._ID
                ._CreditNo = Credits.LastCreditNo + 1
                ._CreditStatus = "Generated"
                ._CreditRef = _CH._BatchNo.ToString + "-" + _C._CreditNo.ToString
                ._CreditDate = Today
                ._CreditDue = Today
                ._CreditInvoiceId = _I._ID.Value
                ._ChildId = _I._ChildId
                ._ChildName = _I._ChildName
                ._FamilyId = _I._FamilyId
                ._FamilyName = _I._FamilyName
                ._FamilyAccountNo = _I._FamilyAccountNo
                ._Address = _I._Address
                ._Hours = _I._InvoiceHoursNonfunded
                ._FundedHours = _I._InvoiceHoursFunded
                ._FundedValue = _I._TotalFunded
                ._CreditTotal = _I._InvoiceSub
                ._FinancialsId = _I._FinancialsId
            End With

            'get the invoice lines and create credit lines from them
            Dim _InvoiceLines As List(Of Business.InvoiceLine) = Business.InvoiceLine.RetreiveAllByInvoiceID(InvoiceToCredit)
            For Each _IL In _InvoiceLines

                Dim _CL As New Business.CreditLine
                With _CL
                    ._CreditId = _C._ID
                    ._LineNo = _IL._LineNo
                    ._ActionDate = _IL._ActionDate
                    ._Description = _IL._Description
                    ._Ref1 = _IL._Ref1
                    ._Ref2 = _IL._Ref1
                    ._Ref3 = _IL._Ref1
                    ._Value = _IL._Value * -1
                    ._Hours = _IL._Hours * -1
                    ._FundedValue = 0
                    ._FundedHours = 0
                    ._NlCode = _IL._NlCode
                    ._NlTracking = _IL._NlTracking
                    .Store()
                End With

            Next

            'handle the discount if there is one...
            If _I._InvoiceDiscount < 0 Then
                Dim _CL As New Business.CreditLine
                With _CL
                    ._CreditId = _C._ID
                    ._LineNo = 999
                    ._ActionDate = _I._InvoiceDate
                    ._Description = "Invoice Discount (Reversed)"
                    ._Ref1 = ""
                    ._Ref2 = ""
                    ._Ref3 = ""
                    ._Value = _I._InvoiceDiscount * -1
                    ._Hours = 0
                    ._FundedValue = 0
                    ._FundedHours = 0
                    ._NlCode = FinanceShared.ReturnNLCode(Nothing, "")
                    ._NlTracking = FinanceShared.ReturnNLTracking(Nothing, "")
                    .Store()
                End With
            End If

            _C._CreditTotal = _I._InvoiceTotal
            _C.Store()

            BatchNo = CInt(_CH._BatchNo)
            Return True

        Else
            BatchNo = 0
            Return False
        End If

    End Function

End Class
