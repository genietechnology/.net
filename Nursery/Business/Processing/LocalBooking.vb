﻿Public Class LocalBooking

    Public Sub New()

    End Sub

    Public Property BookingDate As Date
    Public Property BookingFrom As TimeSpan
    Public Property BookingTo As TimeSpan
    Public Property VoidFrom As TimeSpan?
    Public Property VoidTo As TimeSpan?
    Public Property BookingStatus As String

    Public Property ChildID As Guid

    Public Property Hours As Decimal
    Public Property FundedHours As Decimal

    Public Property TariffID As Guid?
    Public Property TariffName As String
    Public Property TariffRate As Decimal
    Public Property TariffBoltOns As String

    Public Property Times As String

End Class