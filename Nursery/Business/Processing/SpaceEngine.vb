﻿Imports Care.Global
Imports Care.Data

Public Class SpaceEngine

    Private Enum EnumSum
        am
        pm
        hours
        funded_hours
    End Enum

    Private Enum EnumBookingStatus
        Booked
        Waiting
    End Enum

    Public Sub InitialiseSpaces()

        Dim _SQL As String = "delete from Spaces"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, "select top 1 * from Spaces where ID = '" + Guid.NewGuid.ToString + "'")

        Dim _Sites As List(Of Business.Site) = Business.Site.RetreiveAll
        For Each _Site As Business.Site In _Sites

            Dim _AllRatios As List(Of Business.RoomRatios) = Business.RoomRatios.RetreiveByAge
            If _AllRatios IsNot Nothing Then

                Dim _Ratios As IEnumerable(Of Business.RoomRatios) = From _RR As Business.RoomRatios In _AllRatios _
                                              Where _RR.SiteID = _Site._ID _
                                              Order By _RR.AgeFrom

                If _Ratios IsNot Nothing Then

                    _SQL = ""
                    _SQL += "select date,"

                    'ratio loop
                    For Each _r In _Ratios
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.am, EnumBookingStatus.Booked) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.pm, EnumBookingStatus.Booked) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.hours, EnumBookingStatus.Booked) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.funded_hours, EnumBookingStatus.Booked) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.am, EnumBookingStatus.Waiting) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.pm, EnumBookingStatus.Waiting) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.hours, EnumBookingStatus.Waiting) + ","
                        _SQL += ReturnSubQuery(_Site._ID.Value, _r.RatioID.Value, EnumSum.funded_hours, EnumBookingStatus.Waiting) + ","
                    Next

                    'remove the last comma
                    _SQL = _SQL.Substring(0, _SQL.Length - 1)

                    _SQL += " from Calendar c"
                    _SQL += " order by date"

                    'DT Calc is a table with Ratios (AM, PM, Hours etc) as columns
                    'Each row is a date in the range passed as parameters

                    Dim _DTCalc As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                    If Not _DTCalc Is Nothing Then

                        PopulateSpaceRow(_Site, _DTCalc, _Ratios, _DT)

                        If Not DAL.BulkCopy(Session.ConnectionString, "Spaces", _DT) Then
                            'Stop
                        End If

                    End If

                Else
                    'ratios nothing
                End If
            Else
                'all ratios nothing
            End If

        Next

    End Sub

    Private Sub PopulateSpaceRow(ByVal Site As Business.Site, ByRef CalcTable As DataTable, ByRef Ratios As IEnumerable(Of Business.RoomRatios), ByRef SpacesTable As DataTable)

        SpacesTable.Clear()

        'DT Calc is a table with Ratios (AM, PM, Hours etc) as columns
        'Each row is a date in the range passed as parameters

        'loop through each day in the date range
        For Each _DRCalc As DataRow In CalcTable.Rows

            Dim _RoomNo As Integer = 1
            Dim _ColumnNo As Integer = 1
            Dim _NewRatioID As Boolean = True

            Dim _LastColumn As Boolean = False
            Dim _Date As Date? = ValueHandler.ConvertDate(_DRCalc.Item("date"))

            Dim _DR As DataRow = Nothing

            'loop through the ratios (which are the columns of the Calc Row)
            'each ratio has 8 columns
            Dim _ColumnCount As Integer = 8

            'am, pm, hours, funded_hours for both booked and waiting...

            'column name is formatted like this...
            'r_bca90557_b9ca_4a40_b9b4_03c038ddfc1c_B_am

            For Each C As DataColumn In CalcTable.Columns

                If C.ColumnName = "date" Then
                    Continue For
                End If

                If _ColumnNo = _ColumnCount Then
                    _LastColumn = True
                Else
                    _LastColumn = False
                End If

                If _ColumnNo > _ColumnCount Then
                    'next ratio
                    _RoomNo += 1
                    _ColumnNo = 1
                End If

                'if we are on the first column, then its a new ratio
                If _ColumnNo = 1 Then
                    _NewRatioID = True
                Else
                    _NewRatioID = False
                End If

                Dim _RatioID As String = C.ColumnName.Substring(2, 36).Replace("_", "-")
                Dim _Ratio As Business.RoomRatios = GetRatio(Ratios, _RatioID)
                Dim _Status As String = C.ColumnName.Substring(39, 1)
                Dim _Calc As String = C.ColumnName.Substring(41)

                If _NewRatioID Then

                    _DR = SpacesTable.NewRow

                    _DR.Item("ID") = Guid.NewGuid
                    _DR.Item("site_id") = Site._ID
                    _DR.Item("room_id") = _Ratio.RoomID
                    _DR.Item("ratio_id") = _Ratio.RatioID
                    _DR.Item("space_date") = _Date
                    _DR.Item("capacity") = _Ratio.Capacity
                    _DR.Item("capacity_hrs") = _Ratio.Capacity * 10

                    _DR.Item("wl") = 0
                    _DR.Item("wl_am") = 0
                    _DR.Item("wl_pm") = 0
                    _DR.Item("wl_hrs") = 0
                    _DR.Item("wl_funded_hrs") = 0

                    _DR.Item("bk") = 0
                    _DR.Item("bk_am") = 0
                    _DR.Item("bk_pm") = 0
                    _DR.Item("bk_hrs") = 0
                    _DR.Item("bk_funded_hrs") = 0

                End If

                'build fieldname from status and calc
                Dim _FieldStatus As String = ""
                If _Status = "B" Then _FieldStatus = "bk"
                If _Status = "W" Then _FieldStatus = "wl"

                Dim _FieldCalc As String = ""
                If _Calc = "am" Then _FieldCalc = "am"
                If _Calc = "pm" Then _FieldCalc = "pm"
                If _Calc = "hours" Then _FieldCalc = "hrs"
                If _Calc = "funded_hours" Then _FieldCalc = "funded_hrs"

                If _FieldStatus <> "" AndAlso _FieldCalc <> "" Then
                    Dim _Field As String = _FieldStatus + "_" + _FieldCalc
                    _DR.Item(_Field) = _DRCalc.Item(C.ColumnName)
                End If

                If _LastColumn Then

                    If CInt(_DR.Item("bk_am")) > CInt(_DR.Item("bk_pm")) Then
                        _DR.Item("bk") = _DR.Item("bk_am")
                    Else
                        _DR.Item("bk") = _DR.Item("bk_pm")
                    End If

                    If CInt(_DR.Item("wl_am")) > CInt(_DR.Item("wl_pm")) Then
                        _DR.Item("wl") = _DR.Item("wl_am")
                    Else
                        _DR.Item("wl") = _DR.Item("wl_pm")
                    End If

                    SpacesTable.Rows.Add(_DR)

                End If

                _ColumnNo += 1

            Next

            'next row, means next date
            _RoomNo = 1
            _ColumnNo = 1

        Next

    End Sub

    Private Function GetRatio(ByRef Ratios As IEnumerable(Of Business.RoomRatios), ByVal RatioID As String) As Business.RoomRatios

        Dim _Ratios As IEnumerable(Of Business.RoomRatios) = From _RR As Business.RoomRatios In Ratios _
                                      Where _RR.RatioID = New Guid(RatioID)

        If _Ratios IsNot Nothing Then
            Return _Ratios.First
        Else
            Return Nothing
        End If

    End Function

    Private Function ReturnSubQuery(ByVal SiteID As Guid, ByVal RatioID As Guid, ByVal SumToDo As EnumSum, ByVal BookingStatus As EnumBookingStatus) As String

        Dim _Sum As String = SumToDo.ToString
        Dim _Status As String = BookingStatus.ToString.Substring(0, 1)

        'create an alias which always starts with r_
        'replace the dashes in the GUID with underscores
        Dim _Alias As String = "r_" + RatioID.ToString.Replace("-", "_") + "_" + _Status + "_" + _Sum

        Dim _SQL As String = ""
        _SQL += "("
        _SQL += "select isnull(sum(cast(" + _Alias + "." + _Sum + " as money)),0) from Bookings " + _Alias
        _SQL += " where " + _Alias + ".site_id = '" + SiteID.ToString + "'"
        _SQL += " and " + _Alias + ".ratio_id = '" + RatioID.ToString + "'"
        _SQL += " and " + _Alias + ".booking_date = c.date"

        'if we are counting bookings, we need to count holidays as well because the child is only temporarily not in
        If BookingStatus = EnumBookingStatus.Booked Then
            _SQL += " and " + _Alias + ".booking_status <> 'Waiting'"
        Else
            _SQL += " and " + _Alias + ".booking_status = 'Waiting'"
        End If

        _SQL += ")"
        _SQL += " as '" + _Alias + "'"

        Return _SQL

    End Function

End Class

