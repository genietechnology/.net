﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared
Imports Care.Data

Namespace Business

    Partial Class Tariff

        Public Shared Function FindTariff(ByVal ParentForm As Form) As String

            Dim _Find As New GenericFind
            With _Find
                .SearchMode = GenericFind.EnumSearchMode.SearchAsYouType
                .ConnectionString = Session.ConnectionString
                .Caption = "Find Tariff"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .GridSQL = "select name as 'Name', type as 'Type', override as 'Override', funding_hours as 'Funding', bolt_ons as 'Bolt Ons', discount as 'Discount'," & _
                           " weekly as 'Single', daily as 'Multi', nl_code as 'NL Code', nl_tracking as 'NL Tracking'," & _
                           " start_time as 'Start', end_time as 'End', duration as 'Duration'," & _
                           " rate as 'Rate'" & _
                           " " & _
                           " from Tariffs"
                .GridOrderBy = "order by name, rate"
                .ReturnField = "ID"
                .FormWidth = 1000
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

        Public Shared Function GetTariffFromHours(ByVal CheckIn As Date?, ByVal CheckOut As Date?) As Business.Tariff

            'if there is no checkout time, we assume 18:00
            If Not CheckOut.HasValue Then ValueHandler.BuildDateTime(CheckIn.Value, New TimeSpan(18, 0, 0))

            Dim _Hours As Long = DateDiff(DateInterval.Hour, CheckIn.Value, CheckOut.Value)

            Return Nothing

        End Function

        Public Shared Function RetreiveAllMonthlyTariffs(ByVal SiteID As Guid) As List(Of Tariff)

            Dim _SQL As String = ""
            _SQL += "select * from Tariffs"
            _SQL += " where site_id = '" + SiteID.ToString + "'"
            _SQL += " and archived = 0"
            _SQL += " and monthly = 1"
            _SQL += " order by name"

            Dim _TariffList As List(Of Tariff)
            _TariffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _TariffList

        End Function

        Public Shared Function RetreiveAllWeeklyTariffs(ByVal SiteID As Guid) As List(Of Tariff)

            Dim _SQL As String = ""
            _SQL += "select * from Tariffs"
            _SQL += " where site_id = '" + SiteID.ToString + "'"
            _SQL += " and archived = 0"
            _SQL += " and weekly = 1"
            _SQL += " and type <> 'Monthly Matrix'"
            _SQL += " order by name"

            Dim _TariffList As List(Of Tariff)
            _TariffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _TariffList

        End Function

        Public Shared Function RetreiveAllDailyTariffs(ByVal SiteID As Guid) As List(Of Tariff)

            Dim _SQL As String = ""
            _SQL += "select * from Tariffs"
            _SQL += " where site_id = '" + SiteID.ToString + "'"
            _SQL += " and archived = 0"
            _SQL += " and daily = 1"
            _SQL += " and type <> 'Monthly Matrix'"
            _SQL += " order by name"

            Dim _TariffList As List(Of Tariff)
            _TariffList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _TariffList

        End Function

    End Class

End Namespace
