﻿

Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class ChildHoliday

        Public Shared Function RetreiveRange(ByVal ChildID As Guid, ByVal FromDate As Date?, ByVal ToDate As Date?) As List(Of Business.ChildHoliday)

            Dim _SQL As String = "select * from ChildHolidays" & _
                                 " where child_id = '" & ChildID.ToString & "'" & _
                                 " and from_date >= '" & Format(FromDate, "yyyy-MM-dd") & "' and to_date <= '" & Format(ToDate, "yyyy-MM-dd") & "'" & _
                                 " order by from_date"

            Dim _ChildHols As New List(Of ChildHoliday)
            _ChildHols = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildHols

        End Function

        Public Shared Function RetreiveByChildID(ByVal ChildID As Guid) As List(Of Business.ChildHoliday)

            Dim _SQL As String = "select * from ChildHolidays" & _
                                 " where child_id = '" & ChildID.ToString & "'" & _
                                 " order by from_date"

            Dim _ChildHols As New List(Of ChildHoliday)
            _ChildHols = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildHols

        End Function

    End Class

End Namespace
