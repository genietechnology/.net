﻿Namespace Business

    Public Enum EnumMedInc
        Incident
        PriorIncident
        Medication
        UnscheduledMedication
        Absence
    End Enum

    Public Class MedicineIncidents
        Public Property ActionDate As Date
        Public Property ItemType As String
        Public Property Tooltip As String
    End Class

End Namespace
