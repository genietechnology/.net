﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class ChildAnnual

        Public Shared Sub DeleteAnnualData(ByVal AnnualID As Guid)

            Dim _A As Business.ChildAnnual = Business.ChildAnnual.RetreiveByID(AnnualID)
            If _A IsNot Nothing Then

                'if there is a split batch prepared, we can delete that
                DeleteInvoiceBatch(_A._SplitBatch)

                'we can also delete the annual invoice batch
                DeleteInvoiceBatch(_A._ID)

                _A = Nothing

            End If

        End Sub

        Private Shared Sub DeleteInvoiceBatch(ByVal BatchID As Guid?)

            If Not BatchID.HasValue Then Exit Sub

            'delete the annual invoice batch (and invoice)
            DeleteAssociatedInvoices(BatchID.Value)

            Dim _SQL As String = "delete from InvoiceBatch where ID = '" + BatchID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Private Shared Sub DeleteAssociatedInvoices(ByVal BatchID As Guid)

            Dim _SQL As String = ""
            _SQL += "select i.ID as 'ID' from InvoiceBatch b"
            _SQL += " left join Invoices i on i.batch_id = b.ID"
            _SQL += " where b.id = '" + BatchID.ToString + "'"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows
                    Dim _ID As String = _DR.Item("ID").ToString
                    If _ID <> "" Then
                        Invoicing.DeleteInvoice(_ID)
                    End If
                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End Sub

        Public Sub Delete()
            DAL.ExecuteSQL(Session.ConnectionString, "delete from ChildAnnual where ID = '" + Me._ID.Value.ToString + "'")
            DeleteAnnualData(Me._ID.Value)
        End Sub

        Public Function Recalculate(ByVal BatchID As Guid, ByRef InvoiceNo As Integer) As Boolean
            Dim _SQL As String = "select batch_no from InvoiceBatches where ID = '" + BatchID.ToString + "'"
            Dim _BatchNo As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
            If _BatchNo = 0 Then _BatchNo = Invoicing.LastBatchNo + 1
            Return Calculate(BatchID, _BatchNo, InvoiceNo, Me._DateInvoiced.Value, Me._DateDue.Value, Me._DateFrom.Value, Me._DateTo.Value, Me._SplitType, Me._SplitSegments)
        End Function

        Public Function Calculate(ByVal BatchID As Guid, ByVal BatchNo As Integer, ByRef InvoiceNo As Integer, ByVal InvoiceDate As Date, ByVal InvoiceDue As Date,
                                  ByVal InvoiceFrom As Date, ByVal InvoiceTo As Date,
                                  ByVal SplitType As String, ByVal SplitSegments As Integer) As Boolean

            With Me

                ._AnnualInvoice = Invoicing.GenerateInvoice(BatchID, BatchNo, _ChildId.Value, InvoiceDate, InvoiceFrom, InvoiceTo, InvoiceNo, True, "",
                                                            Invoicing.EnumAdditionalSessionMode.Excluded, Invoicing.EnumDuplicateMode.Include, "",
                                                            Invoicing.EnumGenerationMode.Standard, Nothing, Nothing)

                If ._AnnualInvoice IsNot Nothing Then
                    If Me._ID.HasValue Then

                        ._DateFrom = InvoiceFrom
                        ._DateTo = InvoiceTo
                        ._DateInvoiced = InvoiceDate
                        ._DateDue = InvoiceDue

                        ._AnnualTotal = Business.Invoice.RetreiveByID(._AnnualInvoice.Value)._InvoiceTotal

                        ._SplitBatch = Guid.NewGuid
                        ._SplitType = SplitType
                        ._SplitSegments = CByte(SplitSegments)
                        ._SplitTotal = ._AnnualTotal / ._SplitSegments

                        ._Active = True

                        .Store()

                        CreateSegmentBatch(._SplitBatch.Value, BatchNo, ._ID.Value)

                        Return True

                    End If
                End If


            End With

            Return False

        End Function

        Private Sub CreateSegmentBatch(ByVal BatchID As Guid, ByVal BatchNo As Integer, ByVal AnnualID As Guid)

            Dim _A As Business.ChildAnnual = Business.ChildAnnual.RetreiveByID(AnnualID)
            If _A IsNot Nothing Then

                Dim _Split As String = ""
                If _A._SplitType = "" OrElse _A._SplitType = "Monthly" Then
                    _Split = " split into equal payments over 12 months."
                Else
                    _Split = " split into equal payments over " + _A._SplitType + "."
                End If

                Dim _Description As String = "Annual Invoice for £" + ValueHandler.MoneyAsText(_A._AnnualTotal) + _Split

                Dim _C As Business.Child = Business.Child.RetreiveByID(_A._ChildId.Value)
                If _C IsNot Nothing Then

                    Dim _InvFirst As Integer = Invoicing.LastInvoiceNo + 1
                    Dim _InvoiceNo As Integer = Invoicing.LastInvoiceNo
                    Dim _Segment As Integer = 1

                    While _Segment <= _A._SplitSegments

                        Dim _InvoiceDate As Date = _A._DateFrom.Value
                        If _Segment > 1 Then _InvoiceDate = _InvoiceDate.AddMonths(_Segment - 1)

                        Invoicing.GenerateOneLineInvoice(BatchID, BatchNo, _C._FamilyId.Value, _C._ID, _InvoiceDate, _InvoiceDate, ValueHandler.LastDateInMonth(_InvoiceDate), "", _Description, _A._SplitTotal, _InvoiceNo)

                        _Segment += 1

                    End While

                    '_InvoiceNo -= 1
                    CreateBatchHeader(BatchID, Invoicing.LastBatchNo + 1, "Annual Segments ", _A._DateDue.Value, _A._DateFrom.Value, _A._DateTo.Value, _InvFirst, _InvoiceNo)

                End If

            End If

        End Sub

        Public Sub CreateBatchHeader(ByVal BatchID As Guid, ByVal BatchNo As Integer, BatchType As String, _
                                      ByVal BatchDate As Date, ByVal BatchFrom As Date, ByVal BatchTo As Date, _
                                      ByVal FirstInvoice As Integer, ByVal LastInvoice As Integer)

            Dim _H As New Business.InvoiceBatch
            With _H
                ._ID = BatchID
                ._BatchNo = BatchNo
                ._BatchDate = BatchDate
                ._BatchPeriod = BatchType
                ._BatchStatus = "Posted"
                ._DateFirst = BatchFrom
                ._DateLast = BatchTo
                ._InvFirst = FirstInvoice
                ._InvLast = LastInvoice
                ._UserId = Session.CurrentUser.ID
                ._Stamp = Now
                .Store()
            End With

            _H = Nothing

        End Sub

    End Class

End Namespace
