﻿

Imports Care.Global

Namespace Business

    Partial Class ChildCalculation

        Public Sub CalculateTotals(ByVal SplitInto As Integer)

            Dim _SubTotalAnnum As Decimal = 0

            With Me

                _SubTotalAnnum += ._CalcAnnum
                _SubTotalAnnum += FetchCalculated(_C2Sign, ._C2CalcAnnum)
                _SubTotalAnnum += FetchCalculated(_C3Sign, ._C3CalcAnnum)
                _SubTotalAnnum += FetchCalculated(_C4Sign, ._C4CalcAnnum)
                _SubTotalAnnum += FetchCalculated(_C5Sign, ._C5CalcAnnum)

                ._TotalCalcAnnum = _SubTotalAnnum
                ._TotalCalcMonth = _SubTotalAnnum / 12

                If SplitInto = 12 Then
                    'we want to split by month - but we calculate the weekly amount for completeness
                    ._TotalCalcWeek = _SubTotalAnnum / 52
                Else
                    ._TotalCalcWeek = _SubTotalAnnum / SplitInto
                End If

            End With

        End Sub

        Public Sub BuildInvoiceText(ByVal SplitInto As Integer)

            CalculateTotals(SplitInto)

            Dim _C As Business.Child = Business.Child.RetreiveByID(Me._ChildId.Value)

            If _C IsNot Nothing Then

                'get calculation formats
                Dim _CalcFormat As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALCALC")
                Dim _CalcFundingFormat As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALFUNDCALC")

                Dim _Text As String = ""

                Dim _Header As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALHEADER")
                If _Header <> "" Then
                    _Text = _Header
                    _Text = _Text.Replace("{CHILDFULLNAME}", _C._Fullname)
                    _Text = _Text.Replace("{CHILDFORENAME}", _C._Forename)
                    _Text = _Text.Replace("{CHILDSURNAME}", _C._Surname)
                    _Text += vbCrLf
                End If

                '*********************************************************************************************************************************************************

                _Text += BuildLine(_CalcFormat, Me._Units, Me._Multiplier, Me._Rate, Me._Per, Me._Weeks, Me._CalcAnnum, Me._CalcMonth, Me._CalcWeek, Me._CalcDay)
                _Text += vbCrLf

                If Me._C2Sign <> "" Then
                    _Text += Me._C2Sign + " " + BuildLine(_CalcFormat, Me._C2Units, Me._C2Multiplier, Me._C2Rate, Me._C2Per, Me._C2Weeks, Me._C2CalcAnnum, Me._C2CalcMonth, Me._C2CalcWeek, Me._C2CalcDay)
                    _Text += vbCrLf
                End If

                If Me._C3Sign <> "" Then
                    _Text += Me._C3Sign + " " + BuildLine(_CalcFormat, Me._C3Units, Me._C3Multiplier, Me._C3Rate, Me._C3Per, Me._C3Weeks, Me._C3CalcAnnum, Me._C3CalcMonth, Me._C3CalcWeek, Me._C3CalcDay)
                    _Text += vbCrLf
                End If

                If Me._C4Sign <> "" Then
                    _Text += Me._C4Sign + " " + BuildLine(_CalcFormat, Me._C4Units, Me._C4Multiplier, Me._C4Rate, Me._C4Per, Me._C4Weeks, Me._C4CalcAnnum, Me._C4CalcMonth, Me._C4CalcWeek, Me._C4CalcDay)
                    _Text += vbCrLf
                End If

                If Me._C5Sign <> "" Then
                    _Text += Me._C5Sign + " " + BuildLine(_CalcFormat, Me._C5Units, Me._C5Multiplier, Me._C5Rate, Me._C5Per, Me._C5Weeks, Me._C5CalcAnnum, Me._C5CalcMonth, Me._C5CalcWeek, Me._C5CalcDay)
                    _Text += vbCrLf
                End If

                '*********************************************************************************************************************************************************

                Dim _SubTotal As Decimal = 0
                _SubTotal += Me._CalcAnnum
                _SubTotal += FetchCalculated(Me._C2Sign, Me._C2CalcAnnum)
                _SubTotal += FetchCalculated(Me._C3Sign, Me._C3CalcAnnum)
                _SubTotal += FetchCalculated(Me._C4Sign, Me._C4CalcAnnum)
                _SubTotal += FetchCalculated(Me._C5Sign, Me._C5CalcAnnum)

                If Care.Shared.ParameterHandler.ReturnString("TEMPANNUALSUBTOTAL") <> "" Then
                    _Text += Care.Shared.ParameterHandler.ReturnString("TEMPANNUALSUBTOTAL")
                Else
                    _Text += vbCrLf
                    _Text += "Sub Total = £" + ValueHandler.MoneyAsText(_SubTotal) + " per annum"
                    _Text += vbCrLf
                End If

                '*********************************************************************************************************************************************************

                Dim _FundedAnnual As Decimal = 0
                If Me._FundCalcAnnum > 0 Then
                    If Care.Shared.ParameterHandler.ReturnString("TEMPANNUALFUNDING") <> "" Then
                        _Text += Care.Shared.ParameterHandler.ReturnString("TEMPANNUALFUNDING")
                        _Text += BuildLine(_CalcFundingFormat, Me._FundUnits, Me._FundMultiplier, Me._FundRate, Me._FundPer, Me._FundWeeks, Me._FundCalcAnnum, Me._FundCalcMonth, Me._FundCalcWeek, Me._FundCalcDay)
                    Else
                        _Text += vbCrLf
                        _Text += "Less funding as calculated:"
                        _Text += vbCrLf
                        _Text += BuildLine(_CalcFundingFormat, Me._FundUnits, Me._FundMultiplier, Me._FundRate, Me._FundPer, Me._FundWeeks, Me._FundCalcAnnum, Me._FundCalcMonth, Me._FundCalcWeek, Me._FundCalcDay)
                        _Text += vbCrLf
                    End If
                End If

                '*********************************************************************************************************************************************************

                Dim _Annual As Decimal = _SubTotal - Me._FundCalcAnnum
                Dim _Monthly As Decimal = _Annual / 12

                Dim _Weekly As Decimal = 0
                If SplitInto = 12 Then
                    'we want to split by month - but we calculate the weekly amount for completeness
                    _Weekly = _Annual / 52
                Else
                    _Weekly = _Annual / SplitInto
                End If

                Dim _Footer As String = Care.Shared.ParameterHandler.ReturnString("TEMPANNUALFOOTER")
                If _Footer <> "" Then
                    _Text += _Footer
                    _Text = _Text.Replace("{TOTALANNUM}", ValueHandler.MoneyAsText(_Annual))
                    _Text = _Text.Replace("{TOTALMONTH}", ValueHandler.MoneyAsText(_Monthly))
                    _Text = _Text.Replace("{TOTALWEEK}", ValueHandler.MoneyAsText(_Weekly))
                Else
                    _Text += vbCrLf
                    _Text += "= £" + ValueHandler.MoneyAsText(_Annual) + " per annum"
                    _Text += vbCrLf
                    _Text += "= £" + ValueHandler.MoneyAsText(_Monthly) + " per month"
                    _Text += vbCrLf
                    _Text += "= £" + ValueHandler.MoneyAsText(_Weekly) + " per week"
                End If

                '*********************************************************************************************************************************************************

                _Text = _Text.Replace("{BLANK}", "")
                _Text = _Text.Replace("{ENTER}", vbCrLf)
                _Text = _Text.Replace("{RETURN}", vbCrLf)
                _Text = _Text.Replace("{CR}", vbCrLf)
                _Text = _Text.Replace("{CRLF}", vbCrLf)

                Me._InvoiceText = _Text

            End If

        End Sub

        Private Function FetchCalculated(ByVal Sign As String, ByVal CalculatedIn As Decimal) As Decimal
            Dim _Return As Decimal = 0
            If Sign = "+" Then _Return = CalculatedIn
            If Sign = "-" Then _Return = CalculatedIn * -1
            Return _Return
        End Function

        Public Function BuildLine(ByVal CalculationFormat As String, ByVal Units As Decimal, ByVal Multiplier As String, ByVal Rate As Decimal, ByVal Per As String, _
                                   ByVal Weeks As Decimal, _
                                   ByVal CalculatedAnnum As Decimal, ByVal CalculatedMonth As Decimal, ByVal CalculatedWeek As Decimal, ByVal CalculatedDay As Decimal) As String

            If CalculationFormat = "{BLANK}" Then Return ""

            Dim _Return As String = ""

            _Return = CalculationFormat
            If _Return = "" Then
                _Return = Units.ToString + " " + Multiplier + " x £" + ValueHandler.MoneyAsText(Rate) + " per " + Per + " x " + Weeks.ToString + " weeks = £" + ValueHandler.MoneyAsText(CalculatedAnnum) + " per annum"
            Else

                _Return = _Return.Replace("{UNITS}", Format(Units, "0").ToString)
                _Return = _Return.Replace("{MULTIPLIER}", Multiplier)
                _Return = _Return.Replace("{RATE}", ValueHandler.MoneyAsText(Rate))
                _Return = _Return.Replace("{PER}", Per)
                _Return = _Return.Replace("{WEEKS}", Format(Weeks, "0").ToString)

                _Return = _Return.Replace("{CALCANNUM}", ValueHandler.MoneyAsText(CalculatedAnnum))
                _Return = _Return.Replace("{CALCMONTH}", ValueHandler.MoneyAsText(CalculatedMonth))
                _Return = _Return.Replace("{CALCWEEK}", ValueHandler.MoneyAsText(CalculatedWeek))
                _Return = _Return.Replace("{CALCDAY}", ValueHandler.MoneyAsText(CalculatedDay))

            End If

            Return _Return

        End Function

    End Class

End Namespace

