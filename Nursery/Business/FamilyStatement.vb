﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class FamilyStatement

        Public Shared Function RetreivebyStatementDate(ByVal FamilyID As Guid, ByVal StatementDate As Date) As Business.FamilyStatement

            Dim _SQL As String = "select * from FamilyStatement" & _
                                 " where family_id = '" & FamilyID.ToString & "'" & _
                                 " and statement_date = '" & Format(StatementDate, "yyyy-MM-dd") & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _dr IsNot Nothing Then
                Return PropertiesFromData(_dr)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function RetreiveCurrentStatement(ByVal FamilyID As Guid) As Business.FamilyStatement

            Dim _SQL As String = "select top 1 * from FamilyStatement" & _
                                 " where family_id = '" & FamilyID.ToString & "'" & _
                                 " order by statement_date desc"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _dr IsNot Nothing Then
                Return PropertiesFromData(_dr)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ReturnTotalBalance(ByVal FamilyID As Guid) As Decimal

            Dim _Return As Decimal = 0
            Dim _SQL As String = "select sum(balance) from FamilyStatement" & _
                                 " where family_id = '" & FamilyID.ToString & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _dr IsNot Nothing Then
                If _dr.Item(0).ToString <> "" Then
                    _Return = Decimal.Parse(_dr.Item(0).ToString)
                End If
                _dr = Nothing
            End If

            Return _Return

        End Function

    End Class

End Namespace
