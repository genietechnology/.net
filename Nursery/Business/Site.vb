﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class Site

        Public Enum EnumEmailAddressType
            General
            DailyReport
            Finance
        End Enum

        Public Shared Function RetrieveByName(ByVal SiteName As String) As Site

            Dim _SQL As String = "select * from Sites where name = '" + SiteName + "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                Return PropertiesFromData(_DR)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ListSQL() As String

            Dim _SQL As String = ""

            _SQL += "select id, name from Sites s"
            _SQL += " where s.id not in"
            _SQL += " (select x.site_id from SiteExcludedUsers x where x.user_id = '" + Session.CurrentUser.ID.ToString + "')"
            _SQL += " order by s.name"

            Return _SQL

        End Function

        Public Shared Function UserHasExclusions() As Boolean

            Dim _SQL As String = ""
            _SQL += "select count(*) from SiteExcludedUsers"
            _SQL += " where user_id = '" + Session.CurrentUser.ID.ToString + "'"

            Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
            If _Count > 0 Then Return True

            Return False

        End Function

        Public Shared Function ReturnEmailAddress(ByVal SiteID As Guid, ByVal AddressType As EnumEmailAddressType) As Net.Mail.MailAddress

            Dim _S As Business.Site = Business.Site.RetreiveByID(SiteID)
            If _S IsNot Nothing Then

                Select Case AddressType

                    Case EnumEmailAddressType.General
                        If _S._eGenAddress <> "" AndAlso _S._eGenFrom <> "" Then
                            Return New Net.Mail.MailAddress(_S._eGenAddress, _S._eGenFrom)
                        End If

                    Case EnumEmailAddressType.DailyReport
                        If _S._eReportAddress <> "" AndAlso _S._eReportFrom <> "" Then
                            Return New Net.Mail.MailAddress(_S._eReportAddress, _S._eReportFrom)
                        End If

                    Case EnumEmailAddressType.Finance
                        If _S._eInvAddress <> "" AndAlso _S._eInvFrom <> "" Then
                            Return New Net.Mail.MailAddress(_S._eInvAddress, _S._eInvFrom)
                        End If

                End Select

            End If

            Return Nothing

        End Function

    End Class

End Namespace
