﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Namespace Business

    Partial Class Lead

        Public Enum EnumMergeFields
            ContactForename
            ContactSurname
            ContactFullName
            ChildForename
            ChildSurname
            ChildFullname
        End Enum

        Public Shared Function FindLead(ByVal ParentForm As Form) As String

            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Lead"
                .ParentForm = ParentForm
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = "select date_entered as 'Entered', contact_surname as 'Surname', contact_forename as 'Forename'," & _
                           " child_surname as 'Child Surname', child_forename as 'Child Forename', status as 'Status'" & _
                           " from Leads"
                .GridOrderBy = " order by contact_surname, contact_forename"
                .ReturnField = "ID"
                .Show()
                Return .ReturnValue
            End With

        End Function

        Public Shared Function RetrieveOpenLeads() As List(Of Lead)

            Dim _SQL As String = "select * from Leads where closed = 0"

            Dim Leads As List(Of Lead)
            Leads = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return Leads

        End Function

    End Class

End Namespace


