﻿Imports Care.Data
Imports Care.Global

Namespace Business

    Partial Class Register

        Public Enum EnumPersonType
            Child
            Staff
        End Enum

        Public Enum EnumInOut
            [In]
            [Out]
        End Enum

        Private Shared m_Month As String = "DATENAME(MONTH, r.date)"
        Private Shared m_In As String = "(SELECT top 1 stamp_in FROM Register i WHERE i.date = r.date AND i.person_id = r.person_id AND i.in_out = 'I' ORDER BY i.stamp_in)"
        Private Shared m_Out As String = "(SELECT top 1 stamp_out FROM Register o WHERE o.date = r.date AND o.person_id = r.person_id AND o.in_out = 'O' ORDER BY o.stamp_out DESC)"
        Private Shared m_DurationMins As String = "DATEDIFF(mi, " + m_In + ", " + m_Out + ")"
        Private Shared m_DurationHours As String = "CAST(" + m_DurationMins + " AS MONEY) / 60"

        Public Shared Function RetreiveByDate(ByVal DateIn As Date) As List(Of Business.Register)
            Dim _SQL As String = "select * from Register where date = " + ValueHandler.SQLDate(DateIn, True)
            Return PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Shared Function RetreiveEarliestRecord(ByVal DayID As Guid, ByVal PersonID As Guid, ByVal InOut As EnumInOut) As Register

            Dim _InOut As String = "I"
            If InOut = EnumInOut.Out Then _InOut = "O"

            Dim _SQL As String = ""
            _SQL += "select top 1 * from Register"
            _SQL += " where date = '" + ValueHandler.SQLDate(Today) + "'"
            _SQL += " and person_id = '" + PersonID.ToString + "'"
            _SQL += " and in_out = '" + _InOut + "'"

            If InOut = EnumInOut.In Then
                _SQL += " order by stamp_in"
            Else
                _SQL += " order by stamp_out"
            End If

            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))

        End Function

        Public Shared Function ReturnStaffRegister(ByVal StaffID As Guid) As String

            Dim _SQL As String = "SELECT "
            _SQL += m_Month + " AS 'Month',"
            _SQL += "r.date AS 'Date',"
            _SQL += m_In + " AS 'In',"
            _SQL += m_Out + " AS 'Out',"
            _SQL += m_DurationHours + " AS 'Hours'"
            _SQL += " FROM RegisterSummary r"
            _SQL += " WHERE r.person_type = 'S'"
            _SQL += " AND r.person_id = '" + StaffID.ToString + "'"
            _SQL += " GROUP BY r.date, r.person_id"
            _SQL += " ORDER BY r.date DESC"

            Return _SQL

        End Function

        Public Shared Function ReturnChildRegister(ByVal ChildID As Guid) As String

            Dim _SQL As String = "SELECT "
            _SQL += m_Month + " AS 'Month',"
            _SQL += "r.date AS 'Date',"
            _SQL += m_In + " AS 'In',"
            _SQL += m_Out + " AS 'Out',"
            _SQL += m_DurationHours + " AS 'Hours'"
            _SQL += " FROM RegisterSummary r"
            _SQL += " WHERE r.person_type = 'C'"
            _SQL += " AND r.person_id = '" + ChildID.ToString + "'"
            _SQL += " GROUP BY r.date, r.person_id"
            _SQL += " ORDER BY r.date DESC"

            Return _SQL

        End Function

        Public Shared Function ReturnChildRegister(ByVal ChildID As Guid, ByVal FromDate As Date, ByVal ToDate As Date) As String

            Dim _SQL As String = "SELECT "
            _SQL += m_Month + " AS 'Month',"
            _SQL += "r.date AS 'Date',"
            _SQL += m_In + " AS 'In',"
            _SQL += m_Out + " AS 'Out',"
            _SQL += m_DurationHours + " AS 'Hours'"
            _SQL += " FROM RegisterSummary r"
            _SQL += " WHERE r.person_type = 'C'"
            _SQL += " AND r.person_id = '" + ChildID.ToString + "'"
            _SQL += " AND r.date between " + ValueHandler.SQLDate(FromDate, True) + " and " + ValueHandler.SQLDate(ToDate, True)
            _SQL += " GROUP BY r.date, r.person_id"
            _SQL += " ORDER BY r.date DESC"

            Return _SQL

        End Function

    End Class

End Namespace

