﻿Imports Care.Global
Imports Care.Shared

Namespace Business
    Partial Class Risk
        Public Shared Function FindRecord() As String

            Dim _Find As New GenericFind

            Dim _SQL As String = "select title as 'Title', site_name as 'Site', area as 'Area', status as 'Status' from Risks"

            With _Find
                .Caption = "Find Risk Assessment"
                .ParentForm = Nothing
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = _SQL
                .GridOrderBy = "order by title"
                .ReturnField = "ID"
                .Show()
            End With

            Return _Find.ReturnValue

        End Function

    End Class

End Namespace

