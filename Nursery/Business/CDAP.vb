﻿Public Class CDAP

    Public Shared Sub DisplayCDAPDetail(ByVal CDAPID As Guid)

        Dim _frm As New frmCDAPDetail(CDAPID)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

End Class
