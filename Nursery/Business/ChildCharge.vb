﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class ChildCharge

        Public Shared Function RetrieveUsingRange(ByVal ChildID As Guid, ByVal FromDate As Date, ByVal ToDate As Date) As List(Of ChildCharge)

            Dim _SQL As String = "select * from ChildCharges" & _
                                 " where child_id = '" & ChildID.ToString & "'" & _
                                 " and action_date between '" & Format(FromDate, "yyyy-MM-dd") & "' and '" & Format(ToDate, "yyyy-MM-dd") & "'" & _
                                 " order by action_date"

            Dim _ChildChargeList As New List(Of ChildCharge)
            _ChildChargeList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildChargeList

        End Function

    End Class

End Namespace

