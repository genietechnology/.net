﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class ChildAdvanceSession

        Public Enum EnumMode
            Daily
            Weekly
        End Enum

        Public Enum EnumPatternType
            Recurring
            Override
            Additional
        End Enum

        Public Enum EnumScope
            Disabled
            AllTheTime
            TermTimeOnly
            HolidaysOnly
        End Enum

        Public Enum EnumWeekDay
            UseActionDate = 0
            Monday = 1
            Tuesday = 2
            Wednesday = 3
            Thursday = 4
            Friday = 5
            Saturday = 6
            Sunday = 7
        End Enum

        Public Overrides Sub SetDefaultValues()
            If Me._BookingStatus = "" Then Me._BookingStatus = "Booked"
        End Sub

        Public Shared Function RetrieveRecurringBooking(ByRef ChildSessions As List(Of Business.ChildAdvanceSession), ByVal ChildID As String, ByVal ActionDate As Date, ByVal SearchType As EnumMode, ByVal DateCategory As Bookings.EnumDateCategory, _
                                                       Optional ByVal WeekDay As EnumWeekDay = EnumWeekDay.UseActionDate) _
                                                       As ChildAdvanceSession

            Dim _Q As IEnumerable(Of Business.ChildAdvanceSession) = Nothing
            _Q = From _S As Business.ChildAdvanceSession In ChildSessions _
                 Where _S._DateFrom <= ActionDate _
                 And _S._Mode = SearchType.ToString _
                 And _S._PatternType = "Recurring" _
                 Order By _S._DateFrom Descending

            If _Q IsNot Nothing Then

                For Each _S In _Q

                    If SearchType = EnumMode.Daily Then
                        If WeekDay = EnumWeekDay.UseActionDate Then
                            If _S._DateFrom.Value.DayOfWeek.ToString <> ActionDate.DayOfWeek.ToString Then
                                Continue For
                            End If
                        Else
                            If _S._DateFrom.Value.DayOfWeek.ToString <> WeekDay.ToString Then
                                Continue For
                            End If
                        End If
                    End If

                    'Scopes
                    '0 = Disabled
                    '1 = Enabled, All the time
                    '2 = Enabled, Term-Time Only
                    '3 = Enabled, Holidays Only

                    If DateCategory = Bookings.EnumDateCategory.TermDate Then
                        If _S._RecurringScope <> 1 AndAlso _S._RecurringScope <> 2 Then
                            Continue For
                        End If
                    Else
                        If _S._RecurringScope <> 1 AndAlso _S._RecurringScope <> 3 Then
                            Continue For
                        End If
                    End If

                    'if we get to here, we have a winner!
                    Return _S

                Next

            End If

            Return Nothing

        End Function

        Public Shared Function RetrieveSpecificDate(ByRef ChildSessions As List(Of Business.ChildAdvanceSession), ByVal ChildID As String, ByVal ActionDate As Date, ByVal SearchType As EnumMode, _
                                                    ByVal RecurringMode As EnumPatternType, _
                                                    Optional ByVal WeekDay As EnumWeekDay = EnumWeekDay.UseActionDate) _
                                                    As ChildAdvanceSession

            'if we are single session (weekly) and dealing with override patterns or additional sessions
            'we might need to change the action date to be in the previous month
            'i.e. the 1st and 2nd of September 2016 is part of w/c Monday 29th August
            If SearchType = EnumMode.Weekly Then
                If RecurringMode <> EnumPatternType.Recurring Then
                    If ActionDate.DayOfWeek <> DayOfWeek.Monday Then
                        ActionDate = ValueHandler.NearestDate(ActionDate, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
                    End If
                End If
            End If

            Dim _Q As IEnumerable(Of Business.ChildAdvanceSession) = Nothing
            _Q = From _S As Business.ChildAdvanceSession In ChildSessions _
                 Where _S._DateFrom <= ActionDate _
                 And _S._Mode = SearchType.ToString _
                 And _S._PatternType = RecurringMode.ToString _
                 Order By _S._DateFrom Descending

            If _Q IsNot Nothing Then

                For Each _S In _Q

                    If SearchType = EnumMode.Daily Then
                        If WeekDay = EnumWeekDay.UseActionDate Then
                            If _S._DateFrom.Value.DayOfWeek.ToString <> ActionDate.DayOfWeek.ToString Then
                                Continue For
                            End If
                        Else
                            If _S._DateFrom.Value.DayOfWeek.ToString <> WeekDay.ToString Then
                                Continue For
                            End If
                        End If
                    End If

                    'if we get to here, we have a winner!
                    Return _S

                Next

            End If

            Return Nothing

        End Function

        Public Shared Function RetrieveUsingRange(ByVal ChildID As Guid, _
                                          ByVal FromDate As Date, ByVal ToDate As Date) As List(Of ChildAdvanceSession)

            Dim _SQL As String = "select * from ChildAdvanceSessions" & _
                                 " where child_id = '" & ChildID.ToString & "'" & _
                                 " and date_from between '" & Format(FromDate, "yyyy-MM-dd") & "' and '" & Format(ToDate, "yyyy-MM-dd") & "'"

            _SQL += " order by date_from"

            Dim _ChildAdvanceSessionList As New List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildAdvanceSessionList

        End Function

        Public Shared Function RetrieveUsingRange(ByVal ChildID As Guid, _
                                                  ByVal PatternType As EnumPatternType, _
                                                  ByVal FromDate As Date, ByVal ToDate As Date) As List(Of ChildAdvanceSession)

            Dim _SQL As String = "select * from ChildAdvanceSessions" & _
                                 " where child_id = '" & ChildID.ToString & "'" & _
                                 " and date_from between '" & Format(FromDate, "yyyy-MM-dd") & "' and '" & Format(ToDate, "yyyy-MM-dd") & "'"

            _SQL += " and pattern_type = '" + PatternType.ToString + "'"
            _SQL += " order by date_from"

            Dim _ChildAdvanceSessionList As New List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildAdvanceSessionList

        End Function

        Public Shared Function RetrieveWaitingToBooked() As List(Of ChildAdvanceSession)

            Dim _SQL As String = "select * from ChildAdvanceSessions" &
                                 " where booking_status = 'Waiting'" &
                                 " and date_from <= " + ValueHandler.SQLDate(Today, True)

            Dim _ChildAdvanceSessionList As New List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildAdvanceSessionList

        End Function


        Public Shared Sub DeleteSession(ByVal SessionID As Guid)
            Business.ChildAnnual.DeleteAnnualData(SessionID)
            Business.ChildAdvanceSession.DeleteRecord(SessionID)
        End Sub

        Public Shared Function RetrieveByChildID(ByVal ChildID As Guid) As List(Of ChildAdvanceSession)

            Dim _SQL As String = "select * from ChildAdvanceSessions" & _
                                 " where child_id = '" & ChildID.ToString & "'"

            _SQL += " order by date_from"

            Dim _ChildAdvanceSessionList As New List(Of ChildAdvanceSession)
            _ChildAdvanceSessionList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL))
            Return _ChildAdvanceSessionList

        End Function

    End Class

End Namespace
