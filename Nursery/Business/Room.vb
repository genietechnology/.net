﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class Room

        Public Shared Function RetreiveByAge(ByVal AgeInMonths As Integer) As Room

            Dim _SQL As String = "select top 1 * from rooms where min_age <= " + AgeInMonths.ToString & _
                                 " and max_age >= " + AgeInMonths.ToString + " order by max_age desc"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

            If _DR IsNot Nothing Then
                Return PropertiesFromData(_DR)
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function RetreiveAllOrderByAge() As List(Of Room)

            Dim _RoomList As List(Of Room)
            _RoomList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Rooms order by max_age"))
            Return _RoomList

        End Function

    End Class

End Namespace

