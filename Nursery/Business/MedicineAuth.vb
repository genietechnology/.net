﻿Imports Care.Global
Imports Care.Data

Namespace Business
    Partial Class MedicineAuth

        Public Shared Function RetreiveRepeating(ByVal siteID As Guid) As List(Of MedicineAuth)

            Dim medicineAuthList As List(Of MedicineAuth)
            Dim sql As String = ""

            sql = String.Concat("select * from medicineauth m",
                            " left join children c on c.id = m.child_id",
                            " where day_id is null",
                            " and c.site_id = '", siteID, "'")

            medicineAuthList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, sql))
            Return medicineAuthList

        End Function

    End Class

End Namespace