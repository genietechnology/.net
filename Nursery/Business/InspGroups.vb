﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class InspGroup

        Public Shared Function RetreiveByAge(ByVal AgeinMonths As Integer) As InspGroup

            Dim _InspGroups As InspGroup = Nothing
            Dim _SQL As String

            _SQL = "select * from InspGroups" & _
                   " where months_min !> " & AgeinMonths.ToString & _
                   " and months_max !< " & AgeinMonths.ToString

            Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _dt.Rows.Count = 1 Then
                _InspGroups = PropertiesFromData(_dt.Rows(0))
            End If

            _dt.Dispose()
            _dt = Nothing

            Return _InspGroups

        End Function

    End Class

End Namespace
