﻿Public Class User

    Public Property UserCode As String
    Public Property FullName As String
    Public Property ID As Guid
    Public Property Type As String
    Public Property GroupID As String
    Public Property Email As String

    Public ReadOnly Property IsAdministrator As Boolean
        Get
            If Type = "Administrator" Or Type = "Super Administrator" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property IsSuperAdministrator As Boolean
        Get
            If Type = "Super Administrator" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property IsStandardUser As Boolean
        Get
            If Type = "Administrator" Or Type = "Super Administrator" Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property
End Class
