﻿Public Class DebugLogItem

    Public Enum EnumDebugType
        Log
        LineBreakFirst
        Create
        Dispose
        BeginProcess
        EndProcess
        BeginDB
        EndDB
    End Enum

    Public Property LogType As EnumDebugType
    Public Property LogText As String
    Public Property LogStamp As Date

    Public ReadOnly Property LineText As String
        Get
            If LogType = EnumDebugType.LineBreakFirst Then
                Return "************************************************************" + vbCrLf +
                       Format(LogStamp, "HH:mm:ss:fff").ToString + "   " + LogText
            Else
                Return Format(LogStamp, "HH:mm:ss:fff").ToString + " " + ReturnSep() + " " + LogText
            End If
        End Get
    End Property

    Private Function ReturnSep() As String
        If Me.LogType = EnumDebugType.BeginDB Then Return ">>>"
        If Me.LogType = EnumDebugType.EndDB Then Return "<<<"
        If Me.LogType = EnumDebugType.BeginProcess Then Return "("
        If Me.LogType = EnumDebugType.EndProcess Then Return ")"
        If Me.LogType = EnumDebugType.Create Then Return "+"
        If Me.LogType = EnumDebugType.Dispose Then Return "-"
        Return " "
    End Function

    Public Sub New(ByVal LogType As EnumDebugType, LogText As String)
        Me.LogType = LogType
        Me.LogText = LogText
        Me.LogStamp = Now
    End Sub

    Public Overrides Function ToString() As String
        Return Me.LineText
    End Function

End Class
