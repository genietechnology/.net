﻿Imports Care.Connect
Imports System.Windows.Forms
Imports DevExpress.XtraSplashScreen

Public Class SessionObject

    Private m_Interactive As Boolean = True
    Private m_CTICalls As New List(Of Dictionary.CTICallLog)
    Private m_ConnectionManager As New ConnectionManagerObject
    Private m_ConnectionString As String = ""

#Region "Properties"

    Public ReadOnly Property RegistrySystemPath As String
        Get
            Return "Software\Care Software\System"
        End Get
    End Property

    Public ReadOnly Property InteractiveSession As Boolean
        Get
            Return m_Interactive
        End Get
    End Property

    Public ReadOnly Property ConnectionManager As ConnectionManagerObject
        Get
            Return m_ConnectionManager
        End Get
    End Property

    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(value As String)
            m_ConnectionString = value
            m_ConnectionManager.Setup(m_ConnectionString)
        End Set
    End Property

    Public Property KickStartConnectionString() As String

    Public Property ApplicationCode() As String
    Public Property ApplicationName() As String
    Public Property CurrentUser() As User

    Public Property IconFolder() As String
    Public Property ReportFolder() As String
    Public Property LetterFolder() As String
    Public Property TempFolder() As String

    Public Property SMTPEnabled() As Boolean
    Public Property SMTPServer() As String
    Public Property SMTPPort() As Integer
    Public Property SMTPSSL() As Boolean
    Public Property SMTPAuth() As Boolean
    Public Property SMTPSenderEmail As String
    Public Property SMTPSenderName As String
    Public Property SMTPUser As String
    Public Property SMTPPassword As String
    Public Property SMTPBCC As String
    Public Property SMTPTestMode As Boolean

    Public Property CTIEnabled As Boolean

    Public ReadOnly Property CTICalls As List(Of Dictionary.CTICallLog)
        Get
            Return m_CTICalls
        End Get
    End Property

    Public ReadOnly Property CTILastCall As Dictionary.CTICallLog
        Get
            If m_CTICalls.Count = 0 Then
                Return Nothing
            Else
                Return m_CTICalls(0)
            End If
        End Get
    End Property

    Public Property SMSEnabled As Boolean
    Public Property SMSSender As String
    Public Property SMSDomain As String

    Public Property MandrillEnabled As Boolean
    Public Property MandrillAPI As String

    Public Property SlackEnabled As Boolean
    Public Property SlackURL As String
    Public Property SlackUser As String

    Public Property CustomerID As String

    Public ReadOnly Property FinancialsIntegrated As Boolean
        Get
            If FinancialSystem = "" Or FinancialSystem.ToUpper = "NONE" Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public Property FinancialSystem As String
    Public Property FinancialsAPIID As String
    Public Property FinancialsAPIKey As String
    Public Property FinancialsAPISecret As String
    Public Property FinancialsPFXPath As String
    Public Property FinancialsPFXPassword As String
    Public Property FinancialsSagePath As String
    Public Property FinancialsSageUser As String
    Public Property FinancialsSagePassword As String
    Public Property FinancialsSageVersion As String

    Public Property ChangeColour() As System.Drawing.Color
    Public Property FindColour() As System.Drawing.Color

    Public Property SpellCheck As Boolean
    Public Property SpellCheckGrammmarPath As String
    Public Property SpellCheckDictionaryPath As String

    Public Property DictionaryParameters As New List(Of Dictionary.Parameter)
    Public Property DictionaryLists As New List(Of Dictionary.ApplicationList)
    Public Property DictionaryControlSecurity As New List(Of Dictionary.ControlSecurity)
    Public Property DictionaryControlCaptions As New List(Of Dictionary.ControlCaption)

    Public Property DebugMode As Boolean

    Private Property MDIForm As IMDIForm

    Public Property MDIFormObject As IMDIForm
        Get
            Return MDIForm
        End Get
        Set(value As IMDIForm)
            MDIForm = value
        End Set
    End Property

#End Region

    Public Sub ServiceMode()
        m_Interactive = False
    End Sub

    Public Sub CTIPopup(ByVal IncomingNumber As String, ByVal CallerMatched As Boolean, ByVal CallerName As String, ByVal Ref1 As String, ByVal Ref2 As String)
        If Not m_Interactive Then Exit Sub
        Try
            MDIForm.CTIPopup(IncomingNumber, CallerMatched, CallerName, Ref1, Ref2)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub CTILogCall(IncomingNumber As String, CallerMatched As Boolean, CallerName As String, Ref1 As String, Ref2 As String)

        Dim _Call As New Dictionary.CTICallLog
        With _Call
            .IncomingNumber = IncomingNumber
            .CallerMatched = CallerMatched
            .CallerName = CallerName
            .Ref1 = Ref1
            .Ref2 = Ref2
            .Stamp = Now
        End With

        m_CTICalls.Insert(0, _Call)

    End Sub

    Public Sub CursorDefault()
        If Not m_Interactive Then Exit Sub
        Cursor.Current = Cursors.Default
        Cursor.Show()
    End Sub

    Public Sub CursorWaiting()
        If Not m_Interactive Then Exit Sub
        Cursor.Current = Cursors.WaitCursor
        Cursor.Show()
    End Sub

    Public Sub CursorApplicationStarting()
        If Not m_Interactive Then Exit Sub
        Cursor.Current = Cursors.AppStarting
        Cursor.Show()
    End Sub

    Public Sub WaitFormShow()
        If Not m_Interactive Then Exit Sub
        Try
            SplashScreenManager.ShowDefaultWaitForm()
            Application.DoEvents()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub WaitFormClose()
        If Not m_Interactive Then Exit Sub
        Try
            SplashScreenManager.CloseDefaultWaitForm()
            Application.DoEvents()
        Catch ex As Exception

        End Try
    End Sub

#Region "Progress Bar and Messages in MDI"

    Public Sub SetProgressMessage(ByVal MessageText As String, Optional ByVal TimeOut As Integer = 0)
        If Not m_Interactive Then Exit Sub
        Try
            MDIForm.SetProgressMessage(MessageText, TimeOut)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetupProgressBar(ByVal MessageText As String, ByVal MaximumValue As Integer)
        If Not m_Interactive Then Exit Sub
        Try
            MDIForm.SetupProgressBar(MessageText, MaximumValue)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub StepProgressBar(Optional ByVal MessageText As String = "")
        If Not m_Interactive Then Exit Sub
        Try
            MDIForm.StepProgressBar(MessageText)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub HideProgressBar()
        If Not m_Interactive Then Exit Sub
        Try
            MDIForm.HideProgressBar()
        Catch ex As Exception

        End Try
    End Sub

#End Region

End Class
