﻿Imports Care.Global

Public Interface IMDIForm

    Sub CTIPopup(ByVal IncomingNumber As String, ByVal CallerMatched As Boolean, ByVal CallerName As String, ByVal Ref1 As String, ByVal Ref2 As String)
    Sub CTIShowCallLog()
    Sub SetProgressMessage(ByVal MessageText As String, Optional ByVal TimeOut As Integer = 0)
    Sub SetupProgressBar(ByVal MessageText As String, ByVal MaximumValue As Integer)
    Sub StepProgressBar(Optional ByVal MessageText As String = "")
    Sub EmailExportFile(ByVal FilePath As String)
    Sub HideProgressBar()
    ReadOnly Property MDIChildrenArray As System.Windows.Forms.Form()

End Interface
