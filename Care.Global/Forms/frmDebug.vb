﻿Imports System.ComponentModel
Imports System.Windows.Forms

Public Class frmDebug

    Public Sub Insert(ByVal LineText As String)
        txtLog.Text = txtLog.Text.Insert(0, LineText + vbCrLf)
    End Sub

    Private Sub frmDebug_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            Me.Hide()
        End If
    End Sub

    Private Sub frmDebug_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtLog.Clear()
        'For Each _i In KernelStack
        '    txtLog.Text += _i.LineText + vbCrLf
        'Next
    End Sub

End Class