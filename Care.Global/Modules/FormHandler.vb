﻿Imports Care.Global
Imports System.Windows.Forms

Public Class FormHandler

    Private Shared Function GetAssembly(ByVal AssemblyName As String) As System.Reflection.Assembly

        Dim _asbpath As String = Application.StartupPath + "\" + AssemblyName + ".dll"

        If IO.File.Exists(_asbpath) Then

            Dim _asb As System.Reflection.Assembly = Nothing
            Try
                _asb = System.Reflection.Assembly.LoadFrom(_asbpath)

            Catch ex As Exception
                CareMessage("Could not load Assembly: " + ex.Message, MessageBoxIcon.Exclamation, "GetAssembly")
                _asb = Nothing
            End Try

            Return _asb

        Else
            CareMessage("Could not find Assembly: " + _asbpath, MessageBoxIcon.Exclamation, "GetAssembly")
            Return Nothing
        End If

    End Function

    Public Shared Function ReturnObject(ByVal ObjectName As String, ByRef ErrorHit As Exception) As Object

        If ObjectName = "" Then Return Nothing

        Dim _AssemblyName As String = ObjectName.Substring(0, ObjectName.IndexOf("."))
        Dim _ObjectName As String = ObjectName.Substring(ObjectName.IndexOf(".") + 1)

        If ObjectName.StartsWith("Care.Shared.") Then
            _AssemblyName = "Care.Shared"
            _ObjectName = ObjectName.Replace("Care.Shared.", "")
        Else
            _AssemblyName = ObjectName.Substring(0, ObjectName.IndexOf("."))
            _ObjectName = ObjectName.Substring(ObjectName.IndexOf(".") + 1)
        End If

        Return ReturnObject(_AssemblyName, _ObjectName, ErrorHit)

    End Function

    Public Shared Function ReturnObject(ByVal AssemblyName As String, ByVal ObjectName As String, ByRef ErrorHit As Exception) As Object

        Dim _asb As System.Reflection.Assembly = GetAssembly(AssemblyName)
        If _asb IsNot Nothing Then

            Try
                Dim _Name As String = AssemblyName & "." & ObjectName
                Dim _Return As Object = _asb.CreateInstance(_Name)
                Return _Return

            Catch ex As Exception
                ErrorHit = ex
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

    Public Shared Function ReturnInstance(Of T)(ByVal AssemblyName As String, ByVal FormName As String, ByRef ErrorHit As Exception) As T

        Dim _asb As System.Reflection.Assembly = GetAssembly(AssemblyName)
        If _asb IsNot Nothing Then

            Try
                Dim _Name As String = AssemblyName & "." & FormName
                Dim _Return As T = DirectCast(_asb.CreateInstance(_Name), T)
                Return _Return

            Catch ex As Exception
                ErrorHit = ex
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Load a form from an Assembly
    ''' </summary>
    ''' <param name="ClassAndFormName">Class and Form i.e. Nursery.frmChildren</param>
    ''' <param name="FormCaption">The Caption for the Form</param>
    ''' <param name="FormOptions">Options for the Form (M=Multiple, R=ReadOnly etc)</param>
    ''' <remarks></remarks>
    Public Shared Sub LoadFormUsingReflection(ByVal ClassAndFormName As String, ByVal FormCaption As String, Optional ByVal FormOptions As String = "")

        Dim _AssemblyName As String = ClassAndFormName.Substring(0, ClassAndFormName.IndexOf("."))
        Dim _FormName As String = ClassAndFormName.Substring(ClassAndFormName.IndexOf(".") + 1)

        'check if we can open this form multiple times
        If Not FormOptions.ToUpper.Contains("M") Then
            'check if the form is already open, if it is just give it focus
            If FormAlreadyOpen(_FormName) Then
                Exit Sub
            End If
        End If

        LoadForm(_AssemblyName, _FormName, FormCaption, FormOptions)

    End Sub

    Private Shared Sub LoadForm(ByVal AssemblyName As String, ByVal FormName As String, ByVal FormCaption As String, ByVal FormOptions As String)

        Cursor.Current = Cursors.AppStarting
        Application.DoEvents()

        Dim _Ex As Exception = Nothing
        Dim _Form As Form = ReturnInstance(Of Form)(AssemblyName, FormName, _Ex)
        If _Form IsNot Nothing Then
            ShowMDIForm(_Form, FormCaption, False, FormOptions)
        Else

            Cursor.Current = Cursors.Default
            Application.DoEvents()

            Dim _Mess As String = "Error Loading Form."
            If _Ex IsNot Nothing Then
                _Mess += vbCrLf + vbCrLf + "Message: " + _Ex.Message.ToString
                If _Ex.InnerException IsNot Nothing Then
                    _Mess += vbCrLf + vbCrLf + "Inner Exception Message: " + _Ex.InnerException.Message.ToString
                End If
            End If

            CareMessage(_Mess, MessageBoxIcon.Error, "LoadFormUsingReflection: " & AssemblyName & "." & FormName)

        End If

    End Sub

    Private Shared Function FormAlreadyOpen(ByVal FormName As String) As Boolean

        Dim _found As Boolean = False
        Dim _frm As Form
        For Each _frm In Session.MDIFormObject.MDIChildrenArray
            If _frm.Name.ToLower = FormName.ToLower Then
                _found = True
                _frm.WindowState = FormWindowState.Normal
                _frm.BringToFront()
                Exit For
            End If
        Next

        Return _found

    End Function

    Public Shared Sub ShowMDIForm(ByVal FormObject As Form, ByVal FormCaption As String, ByVal Modal As Boolean, Optional ByVal FormOptions As String = "")

        If FormObject IsNot Nothing Then

            FormObject.Text = FormCaption

            If Modal Then
                With FormObject
                    .StartPosition = FormStartPosition.CenterScreen
                    .Tag = FormOptions
                    .ShowDialog()
                End With
            Else
                With FormObject
                    .MdiParent = CType(Session.MDIFormObject, Form)
                    .StartPosition = FormStartPosition.Manual
                    .Tag = FormOptions
                    .Left = 0
                    .Top = 0
                    .Show()
                End With
            End If

        End If

    End Sub

End Class
