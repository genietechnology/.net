﻿
Imports DevExpress.XtraEditors

Public Module MessageHandler

    Public Function Msgbox(ByVal Message As String) As DialogResult
        If Session.InteractiveSession Then
            Return XtraMessageBox.Show(Message, Session.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Return DialogResult.OK
        End If
    End Function

    Public Function Msgbox(ByVal Message As String,
                           ByVal MessageIcon As MessageBoxIcon,
                           ByVal Title As String) As DialogResult

        If Session.InteractiveSession Then

            Dim _Title = Title
            If _Title = "" Then _Title = Session.ApplicationName

            Return XtraMessageBox.Show(Message, _Title, MessageBoxButtons.OK, MessageIcon)

        Else
            Return DialogResult.OK
        End If

    End Function

    Public Function CareMessage(ByVal Message As String,
                                ByVal MessageIcon As MessageBoxIcon,
                                ByVal MessageButtons As MessageBoxButtons,
                                ByVal Title As String) As DialogResult

        If Session.InteractiveSession Then

            Dim _Title = Title
            If _Title = "" Then _Title = Session.ApplicationName

            Return XtraMessageBox.Show(Message, _Title, MessageButtons, MessageIcon)

        Else
            Return DialogResult.OK
        End If

    End Function

    Public Function CareMessage(ByVal Message As String,
                                ByVal MessageIcon As MessageBoxIcon,
                                ByVal Title As String) As DialogResult

        If Session.InteractiveSession Then

            Dim _Title = Title
            If _Title = "" Then _Title = Session.ApplicationName

            Return XtraMessageBox.Show(Message, _Title, MessageBoxButtons.OK, MessageIcon)

        Else
            Return DialogResult.OK
        End If

    End Function

    Public Function CareMessage(ByVal Message As String,
                                ByVal MessageIcon As MessageBoxIcon) As DialogResult

        If Session.InteractiveSession Then
            Return XtraMessageBox.Show(Message, Session.ApplicationName, MessageBoxButtons.OK, MessageIcon)
        Else
            Return DialogResult.OK
        End If

    End Function

    Public Function CareMessage(ByVal Message As String) As DialogResult
        If Session.InteractiveSession Then
            Return XtraMessageBox.Show(Message, Session.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Return DialogResult.OK
        End If
    End Function

    'Public Function CareMessage(ByVal MessageNo As Integer,
    '                            Optional ByVal MessageIcon As MessageBoxIcon = MessageBoxIcon.Information,
    '                            Optional ByVal MessageButtons As MessageBoxButtons = MessageBoxButtons.OK,
    '                            ) As DialogResult

    '    If Session.InteractiveSession Then
    '        Return XtraMessageBox.Show(GetMessageText(MessageNo), Session.ApplicationName, MessageButtons, MessageIcon)
    '    Else
    '        Return DialogResult.OK
    '    End If

    'End Function

    'Public Function CareMessage(ByVal MessageNo As Integer,
    '                            ByVal MessageIcon As MessageBoxIcon,
    '                            ) As DialogResult

    '    If Session.InteractiveSession Then
    '        Return XtraMessageBox.Show(GetMessageText(MessageNo), Session.ApplicationName, MessageBoxButtons.OK, MessageIcon)
    '    Else
    '        Return DialogResult.OK
    '    End If

    'End Function

    Private Function GetMessageText(ByVal MessageNo As Integer) As String
        Return ""
    End Function

End Module
