﻿Public Module Globals

    Public Const DateTimeFormat As String = "dd/MM/yy HH:mm:ss"
    Public g_Font As System.Drawing.Font

    Private _SessionObject As New SessionObject
    Public ReadOnly Property Session As SessionObject
        Get
            Return _SessionObject
        End Get
    End Property

#Region "Kernel Logging"

    Private m_KernelLogging As Boolean = False
    Private m_frmDebug As New frmDebug
    Private m_KernelStack As New List(Of DebugLogItem)

    Public Sub StartKernelLogging()
        m_KernelLogging = True
        KernelLog("Kernel Logging Started", DebugLogItem.EnumDebugType.LineBreakFirst)
    End Sub

    Public ReadOnly Property KernelStack As List(Of DebugLogItem)
        Get
            Return m_KernelStack
        End Get
    End Property

    Public Sub KernelShow()
        If m_KernelLogging Then
            m_frmDebug.Show()
        Else
            StartKernelLogging()
            CareMessage("Kernel Logging Started", MessageBoxIcon.Information, "Globals.KernelShow")
        End If
    End Sub

    Public Sub KernelLog(ByVal LogText As String, ByVal LogType As DebugLogItem.EnumDebugType)
        If Not m_KernelLogging Then Exit Sub
        Dim _dbi As New DebugLogItem(LogType, LogText)
        m_KernelStack.Insert(0, _dbi)
        If m_frmDebug.Visible Then
            m_frmDebug.Insert(_dbi.LineText)
            m_frmDebug.txtLog.Refresh()
        End If
    End Sub

    Public Sub KernelLog(ByVal LogText As String)
        If Not m_KernelLogging Then Exit Sub
        Dim _dbi As New DebugLogItem(DebugLogItem.EnumDebugType.Log, LogText)
        m_KernelStack.Insert(0, _dbi)
        If m_frmDebug.Visible Then
            m_frmDebug.Insert(_dbi.LineText)
            m_frmDebug.txtLog.Refresh()
        End If
    End Sub

#End Region

End Module
