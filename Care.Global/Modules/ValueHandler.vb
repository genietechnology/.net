﻿Imports Care.Global

Public Class ValueHandler

    Public Enum EnumDirection
        Forwards
        Backwards
    End Enum

    Public Shared Function IsStrongPassword(ByVal Password As String,
                                            Optional ByVal MinUpper As Integer = 1,
                                            Optional ByVal MinLower As Integer = 1,
                                            Optional ByVal MinNumber As Integer = 1,
                                            Optional ByVal MinSymbol As Integer = 1,
                                            Optional ByVal MinLength As Integer = 8) As Boolean

        If Password.Length < MinLength Then Return False

        Dim _Upper As Integer = 0
        Dim _Lower As Integer = 0
        Dim _Number As Integer = 0
        Dim _Symbol As Integer = 0

        For Each _c In Password.ToCharArray
            If Char.IsLower(_c) Then _Lower += 1
            If Char.IsUpper(_c) Then _Upper += 1
            If Char.IsNumber(_c) Then _Number += 1
            If Char.IsSymbol(_c) Then _Symbol += 1
        Next

        If _Upper < MinUpper Then Return False
        If _Lower < MinLower Then Return False
        If _Number < MinNumber Then Return False
        If _Symbol < MinSymbol Then Return False

        Return True

    End Function

    Public Shared Function Convert2DP(ByVal ValueIn As Decimal) As Decimal
        Return Math.Round(ValueIn, 2)
    End Function

    Public Shared Function Convert2DP(ByVal ValueIn As Object) As Decimal
        Dim _Value As Decimal = ConvertDecimal(ValueIn)
        _Value = Math.Round(_Value, 2)
        Return _Value
    End Function

    Public Shared Function MarkUp(ByVal ValueIn As Decimal, ByVal MarkUpPercent As Decimal) As Decimal
        If ValueIn = 0 Then
            Return 0
        Else
            Dim _MarkUp As Decimal = PercentageChange(ValueIn, MarkUpPercent)
            Return ValueIn + _MarkUp
        End If
    End Function

    Public Shared Function MarkUp2DP(ByVal ValueIn As Decimal, ByVal MarkUpPercent As Decimal) As Decimal
        If ValueIn = 0 Then
            Return 0
        Else
            Dim _MarkUp As Decimal = PercentageChange(ValueIn, MarkUpPercent)
            Return ValueIn + _MarkUp
        End If
    End Function

    Public Shared Function PercentageChange(ByVal ValueIn As Decimal, ByVal Percentage As Decimal) As Decimal
        If ValueIn = 0 Then
            Return 0
        Else
            Return ValueIn / 100 * Percentage
        End If
    End Function

    Public Shared Function SQLDate(ByVal DateIn As Date, Optional ByVal IncludeQuotes As Boolean = False) As String
        If IncludeQuotes Then
            Return "'" + Format(DateIn, "yyyy-MM-dd") + "'"
        Else
            Return Format(DateIn, "yyyy-MM-dd")
        End If
    End Function

    Public Shared Function SQLDateTime(ByVal DateIn As Date, Optional ByVal IncludeQuotes As Boolean = False) As String
        If IncludeQuotes Then
            Return "'" + Format(DateIn, "yyyy-MM-dd HH:mm") + "'"
        Else
            Return Format(DateIn, "yyyy-MM-dd HH:mm")
        End If
    End Function

    Public Shared Function BuildDateTime(ByVal DateIn As Date, ByVal TimeIn As Date) As Date
        Dim _Return As New Date(DateIn.Year, DateIn.Month, DateIn.Day, TimeIn.Hour, TimeIn.Minute, TimeIn.Second)
        Return _Return
    End Function

    Public Shared Function BuildDateTime(ByVal DateIn As Date, ByVal TimeIn As TimeSpan) As Date
        Dim _Return As New Date(DateIn.Year, DateIn.Month, DateIn.Day, TimeIn.Hours, TimeIn.Minutes, TimeIn.Seconds)
        Return _Return
    End Function

    Public Shared Function MoneyAsText(ByVal DecimalValueIn As Decimal?) As String
        If DecimalValueIn Is Nothing Then Return "0.00"
        If Not DecimalValueIn.HasValue Then Return "0.00"
        Return Format(DecimalValueIn.Value, "0.00")
    End Function

    Public Shared Function MoneyAsText(ByVal DoubleValueIn As Double?) As String
        If DoubleValueIn Is Nothing Then Return "0.00"
        If Not DoubleValueIn.HasValue Then Return "0.00"
        Return Format(DoubleValueIn.Value, "0.00")
    End Function

    Public Shared Function LastDateInMonth(ByVal DateIn As Date) As Date
        Return DateSerial(DateIn.Year, DateIn.Month, Date.DaysInMonth(DateIn.Year, DateIn.Month))
    End Function

    Public Shared Function FirstMondayInMonth(ByVal YearIn As Integer, ByVal MonthIn As Integer) As Date
        Dim _Date As Date = DateSerial(YearIn, MonthIn, 1)
        Return NearestDate(_Date, DayOfWeek.Monday, EnumDirection.Forwards)
    End Function

    Public Shared Function LastFridayInMonth(ByVal YearIn As Integer, ByVal MonthIn As Integer) As Date
        Dim _Date As Date = DateSerial(YearIn, MonthIn, Date.DaysInMonth(YearIn, MonthIn))
        Return NearestDate(_Date, DayOfWeek.Friday, EnumDirection.Backwards)
    End Function

    Public Shared Function FirstMondayInMonth(ByVal DateIn As Date) As Date
        Dim _Date As Date = DateSerial(DateIn.Year, DateIn.Month, 1)
        Return NearestDate(_Date, DayOfWeek.Monday, EnumDirection.Forwards)
    End Function

    Public Shared Function ReturnExactMonths(ByVal FromDate As Date, ByVal ToDate As Date) As Long

        Dim _Months As Integer = 0
        Dim _Days As Integer = 0

        DateDiffMonths(FromDate, ToDate, False, _Months, _Days)

        Return CLng(_Months)

    End Function

    <Obsolete("Use ReturnExactMonths instead")>
    Public Shared Function ReturnExactMonthsOld(ByVal FromDate As Date, ByVal ToDate As Date) As Long

        Dim _y As Integer, _m As Integer, _d As Integer
        Dim _return As Integer = 0

        _y = DatePart(DateInterval.Year, ToDate) - DatePart(DateInterval.Year, FromDate)
        _m = DatePart(DateInterval.Month, ToDate) - DatePart(DateInterval.Month, FromDate)
        _d = DatePart(DateInterval.Day, ToDate) - DatePart(DateInterval.Day, FromDate)

        'current month is less than birthday month
        If _m < 0 Then
            _y -= 1
            _m = 12 + _m
        Else
            If _m = 0 Then
                If _d < 0 Then
                    _y -= 1
                    _m = 11
                    _d = FromDate.Day + _d
                End If
            End If
        End If

        _return = _y * 12
        _return += _m

        If _return < 0 Then _return = 0

        Return _return

    End Function

    Private Shared Function DateAddMonths(startDate As DateTime, monthCount As Integer, sticky As Boolean) As DateTime
        Dim result As DateTime = startDate.AddMonths(monthCount)
        If sticky Then
            If DateTime.DaysInMonth(startDate.Year, startDate.Month) = startDate.Day Then
                Dim resultYear As Integer = result.Year
                Dim resultMonth As Integer = result.Month
                result = New DateTime(resultYear, resultMonth, DateTime.DaysInMonth(resultYear, resultMonth))
            End If
        End If
        Return result
    End Function

    Private Shared Sub DateDiffMonths(FromDate As DateTime, ToDate As DateTime, StickyMode As Boolean, ByRef ReturnMonths As Integer, ByRef ReturnDays As Integer)

        If ToDate < FromDate Then
            Dim t As DateTime = ToDate
            ToDate = FromDate
            FromDate = t
        End If

        ' make a guess at the answer; using 31 means that we'll be close but won't exceed
        Dim monthCount As Integer = CInt(((ToDate - FromDate).Days / 31))

        ' find the maximum number of months that's less than or equal to the second date
        Dim testDate As DateTime = DateAddMonths(FromDate, monthCount, StickyMode)
        While testDate < ToDate
            testDate = DateAddMonths(FromDate, System.Threading.Interlocked.Increment(monthCount), StickyMode)
        End While

        ' if we've hit the exact date, return the number of months and zero days
        If testDate = ToDate Then
            ReturnMonths = monthCount
            ReturnDays = 0
            Return
        End If

        ' otherwise we exceeded the second date, back up and return the correct values
        testDate = DateAddMonths(FromDate, System.Threading.Interlocked.Decrement(monthCount), StickyMode)
        ReturnMonths = monthCount
        ReturnDays = (ToDate - testDate).Days

    End Sub

    Public Shared Function NearestDate(ByVal DateIn As Date, ByVal NearestWeekDay As DayOfWeek, ByVal SearchDirection As EnumDirection) As Date

        Dim _Date As Date = DateIn
        Dim _DayIncrement As Integer = 1

        If SearchDirection = EnumDirection.Backwards Then _DayIncrement = -1

        While _Date.DayOfWeek <> NearestWeekDay
            _Date = _Date.AddDays(_DayIncrement)
        End While

        Return _Date

    End Function

    Public Shared Function WorkingDays(ByVal DateFrom As Date, ByVal DateTo As Date) As Integer

        Dim _Days As Integer = 0
        Dim _Date As Date = DateFrom

        While _Date <= DateTo
            If _Date.DayOfWeek = DayOfWeek.Saturday OrElse _Date.DayOfWeek = DayOfWeek.Sunday Then
                'ignore weekends
            Else
                _Days += 1
            End If
            _Date = _Date.AddDays(1)
        End While

        Return _Days

    End Function

    Public Shared Function DateDifferenceAsTextYM(ByVal DateFrom As Date?, ByVal DateTo As Date?) As String

        Dim _y As Integer, _m As Integer, _d As Integer
        Dim _yt As String = "", _mt As String = "", _dt As String = "", _return As String = ""

        If Not IsDate(DateFrom) Then Return ""
        If Not IsDate(DateTo) Then Return ""
        If DateFrom > DateTo Then Return ""

        _y = DatePart(DateInterval.Year, DateTo.Value) - DatePart(DateInterval.Year, DateFrom.Value)
        _m = DatePart(DateInterval.Month, DateTo.Value) - DatePart(DateInterval.Month, DateFrom.Value)
        _d = DatePart(DateInterval.Day, DateTo.Value) - DatePart(DateInterval.Day, DateFrom.Value)

        'current month is less than birthday month
        If _m < 0 Then
            _y -= 1
            _m = 12 + _m
        Else
            If _m = 0 Then
                If _d < 0 Then
                    _y -= 1
                    _m = 11
                    _d = DateFrom.Value.Day + _d
                End If
            End If
        End If

        Return _y.ToString + "Y " + _m.ToString + "M"

    End Function

    Public Shared Function DateDifferenceAsText(ByVal DateFrom As Date, ByVal DateTo As Date,
                                     Optional ByVal IncludeYears As Boolean = True,
                                     Optional ByVal IncludeMonths As Boolean = True,
                                     Optional ByVal IncludeDays As Boolean = True) As String

        Dim _y As Integer, _m As Integer, _d As Integer
        Dim _yt As String = "", _mt As String = "", _dt As String = "", _return As String = ""

        If Not IsDate(DateFrom) Then Return ""
        If Not IsDate(DateTo) Then Return ""
        If DateFrom > DateTo Then Return ""

        _y = DatePart(DateInterval.Year, DateTo) - DatePart(DateInterval.Year, DateFrom)
        _m = DatePart(DateInterval.Month, DateTo) - DatePart(DateInterval.Month, DateFrom)
        _d = DatePart(DateInterval.Day, DateTo) - DatePart(DateInterval.Day, DateFrom)


        'if current day is less than birthday day
        If _d < 0 Then
            _m -= 1
        End If

        'current month is less than birthday month
        If _m < 0 Then
            _y -= 1
            _m = 12 + _m
        Else
            If _m = 0 Then
                If _d < 0 Then
                    _y -= 1
                    _m = 11
                    If _d <> 0 Then _d = DateTime.DaysInMonth(DatePart(DateInterval.Year, DateFrom), DatePart(DateInterval.Month, DateFrom)) + _d
                End If
            End If
        End If

        If _y > 0 Then
            If _y = 1 Then
                _yt = _y.ToString & " year"
            Else
                _yt = _y.ToString & " years"
            End If
        End If

        If _m > 0 Then
            If _m = 1 Then
                _mt = _m.ToString & " month"
            Else
                _mt = _m.ToString & " months"
            End If
        End If

        If _d > 0 Then
            If _d = 1 Then
                _dt = _d.ToString & " day"
            Else
                _dt = _d.ToString & " days"
            End If
        Else
            If _d <> 0 Then
                _d = DateTime.DaysInMonth(DatePart(DateInterval.Year, DateFrom), DatePart(DateInterval.Month, DateFrom)) + _d
                _dt = _d.ToString & " days"
            End If
        End If

        If IncludeYears And _yt.Length > 0 Then _return = _yt

        If IncludeMonths And _mt.Length > 0 Then
            If _y = 0 Then
                _return += _mt
            Else
                _return += ", " & _mt
            End If
        End If

        If IncludeDays And _dt.Length > 0 Then _return += ", " & _dt

        Return _return

    End Function

    Public Shared Function ReturnValueColour(ByVal ValueIn As Decimal, Optional ByVal NegativeIsBlue As Boolean = True) As Drawing.Color

        If ValueIn = 0 Then Return Drawing.Color.Black

        If NegativeIsBlue Then
            If ValueIn < 0 Then
                Return Drawing.Color.Blue
            Else
                Return Drawing.Color.Red
            End If
        Else
            If ValueIn < 0 Then
                Return Drawing.Color.Red
            Else
                Return Drawing.Color.Blue
            End If
        End If

    End Function

    Public Shared Function ConvertBoolean(ByVal ValueIn As Object) As Boolean

        If ValueIn Is Nothing Then Return False
        If ValueIn.ToString = "" Then Return False

        Dim _Return As Boolean = False

        Try
            _Return = Boolean.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertInteger(ByVal ValueIn As Object) As Integer

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Integer = 0

        Try
            _Return = Integer.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertByte(ByVal ValueIn As Object) As Byte

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Byte = 0

        Try
            _Return = Byte.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertDecimal(ByVal ValueIn As Object) As Decimal

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Decimal = 0

        Try
            _Return = Decimal.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertDouble(ByVal ValueIn As Object) As Double

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Double = 0

        Try
            _Return = Double.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDouble.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertTimeSpan(ByVal ObjectIn As Object) As TimeSpan?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, TimeSpan) = TimeSpan.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, TimeSpan)
            End If
        End If

    End Function

    Public Shared Function ConvertTimeSpan(ByVal HHMM As String) As TimeSpan?
        Dim _Return As TimeSpan
        If TimeSpan.TryParse(HHMM, _Return) Then
            Return _Return
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ConvertDate(ByVal ValueIn As Object) As Date?

        If ValueIn Is Nothing Then Return Nothing
        If ValueIn.ToString = "" Then Return Nothing

        Dim _Return As Date = Nothing

        Try
            _Return = Date.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertDateOmitTime(ByVal ValueIn As Object) As Date?

        Dim _Return As Date? = ConvertDate(ValueIn)
        If _Return IsNot Nothing Then
            If _Return.HasValue Then
                _Return = _Return.Value.Date
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ConvertGUID(ByVal ValueIn As Object) As Guid

        If ValueIn Is Nothing Then Return Nothing
        If ValueIn.ToString = "" Then Return Nothing

        Dim _Return As Guid = Nothing

        Try
            _Return = New Guid(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ReturnHoursAndMinutes(ByVal Mins As Integer) As String

        Dim _TS As New TimeSpan(0, Mins, 0)

        Dim _Hours As String = ""
        Dim _Mins As String = ""
        Dim _Return As String = ""

        If _TS.Hours > 0 Then
            If _TS.Hours = 1 Then
                _Hours = "1 hour"
            Else
                _Hours = _TS.Hours.ToString + " hours"
            End If
        End If

        If _TS.Minutes > 0 Then
            If _TS.Minutes = 1 Then
                _Mins = "1 minute"
            Else
                _Mins = _TS.Minutes.ToString + " minutes"
            End If
        End If

        If _Hours <> "" Then
            _Return = _Hours
            If _Mins <> "" Then
                _Return += ", " + _Mins
            End If
        Else
            _Return = _Mins
        End If

        Return _Return

    End Function

    Public Shared Function ReturnTime(ByVal ValueIn As Double) As Date

        If ValueIn > 0 Then

            Dim _ValueString = ValueIn.ToString
            Dim _Hours As Integer = 0
            Dim _Mins As Integer = 0

            Dim _Value As Integer = CInt(ValueIn)
            If _Value.ToString.Length = 3 Then
                _Hours = ValueHandler.ConvertInteger(_ValueString.Substring(0, 1))
                _Mins = ValueHandler.ConvertInteger(_ValueString.Substring(1))
            Else
                _Hours = ValueHandler.ConvertInteger(_ValueString.Substring(0, 2))
                _Mins = ValueHandler.ConvertInteger(_ValueString.Substring(2))
            End If

            Dim _Date As New Date(1900, 1, 1, _Hours, _Mins, 0)
            Return _Date

        Else
            Return New Date
        End If

    End Function

    Public Shared Function ReturnTime(ByVal ValueIn As Object) As Date?

        If ValueIn Is Nothing Then Return Nothing

        If TypeOf ValueIn Is TimeSpan Then
            Dim _TS As TimeSpan = CType(ValueIn, TimeSpan)
            Return New Date(1900, 1, 1, _TS.Hours, _TS.Minutes, 0)
        Else
            Return Nothing
        End If

    End Function

    Public Shared Function AgeYearsMonths(ByVal Months As Double) As String

        Dim years As Double = Math.Floor(Months / 12)
        Dim remainingMonths As Double = Months - (years * 12)

        Return String.Concat(years, " years and ", remainingMonths, " months")

    End Function

End Class
