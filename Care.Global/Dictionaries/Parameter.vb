﻿Namespace Dictionary

    Public Class Parameter
        Public Property _ID As Guid
        Public Property _Name As String
        Public Property _Description As String
        Public Property _Type As String
        Public Property _Value As String
    End Class

End Namespace

