﻿Namespace Dictionary

    Public Class ApplicationList

        Implements IEnumerable

        Public Property ListItems As New List(Of ApplicationListItem)

        Public Property ID As Guid
        Public Property Name As String
        Public Property ListType As String
        Public Property ListCategory As String

        Public Class ApplicationListItem

            Public Property ItemID As Guid
            Public Property Name As String
            Public Property Sequence As Integer

            Public Property Ref1 As String
            Public Property Ref2 As String
            Public Property Ref3 As String

            Public Property Hide As Boolean

        End Class

        Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace

