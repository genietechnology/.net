﻿Namespace Dictionary

    Public Class ControlSecurity

        Public Enum EnumControlType
            Frame
            Tab
        End Enum

        Public Enum EnumSecurityType
            User
            Group
        End Enum

        Property FormName As String
        Property ControlName As String
        Property ControlType As EnumControlType
        Property ControlTabName As String
        Property ControlSecurityType As EnumSecurityType
        Property ControlSecurityID As Guid?
        Property ControlSecurityName As String

    End Class

End Namespace
