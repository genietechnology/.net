﻿
Namespace Dictionary

    Public Class CTICallLog
        Property IncomingNumber As String
        Property CallerMatched As Boolean
        Property CallerName As String
        Property Ref1 As String
        Property Ref2 As String
        Property Stamp As Date
    End Class

End Namespace
