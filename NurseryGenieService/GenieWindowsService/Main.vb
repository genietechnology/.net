﻿Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.Diagnostics
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Threading
Imports GenieServiceLib
Imports System.ServiceProcess

Public Class Main

    Private WithEvents m_ServiceHost As ServiceHost = Nothing
    Private WithEvents m_DataServiceHost As ServiceHost = Nothing

    Private m_Scheduler As TaskScheduler = Nothing
    Private m_Minute As Timer
    Private m_ServicePort As String = ""
    Private m_DataPort As String = ""

    Private m_10Mins As New TimeSpan(0, 0, 10, 0)
    Private m_BufferSize As Long = 2147483647

    Public Sub New()


        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        EventLogging.SetupLog(My.Settings.LogName)

    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)

        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        EventLogging.LogEvent("Starting Nursery Genie Service Version " + My.Application.Info.Version.ToString, EventLogEntryType.Information)

        EventLogging.LogEvent("Setup Unhandled Exception Hander", EventLogEntryType.Information)
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExeception

        EventLogging.LogEvent("Get Port Configuration", EventLogEntryType.Information)
        SetPorts()

        EventLogging.LogEvent("Listening on Ports: " + m_DataPort + ", " + m_ServicePort, EventLogEntryType.Information)

        '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        SetupDataHost("net.tcp://LOCALHOST:" + m_DataPort + "/NurseryGenieDataService")

        If SetupHost("net.tcp://LOCALHOST:" + m_ServicePort + "/NurseryGenieService") Then


            '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            EventLogging.LogEvent("Create Scheduler Instance", EventLogEntryType.Information)

            m_Scheduler = New TaskScheduler(My.Settings.SQLServer, My.Settings.SQLDatabase,
                                            My.Settings.SQLUser, My.Settings.SQLPassword,
                                            My.Settings.ReportPath, My.Settings.TempPath)

            If m_Scheduler.StartScheduler() Then

                EventLogging.LogEvent("StartScheduler OK", EventLogEntryType.Information)

                EventLogging.LogEvent("Check for Paxton Integration", EventLogEntryType.Information)
                PaxtonHelper.Connect()

            Else
                EventLogging.LogEvent("StartScheduler Failed", EventLogEntryType.Error)
            End If

        End If

        '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    End Sub

    Protected Overrides Sub OnStop()

        ' Add code here to perform any tear-down necessary to stop your service.
        EventLogging.LogEvent("Stopping Service...", EventLogEntryType.Information)

        Try
            If m_Minute IsNot Nothing Then
                m_Minute.Dispose()
            End If

        Catch ex As Exception
            EventLogging.LogEvent("Could not dispose Timer Object", EventLogEntryType.Warning)
        End Try

    End Sub

    Private Sub UnhandledExeception(sender As Object, e As UnhandledExceptionEventArgs)
        Try
            EventLogging.LogEvent("UnhandledException", e.ExceptionObject)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetPorts()

        If My.Settings.ServicePort = "" Then
            m_ServicePort = "9001"
        Else
            m_ServicePort = My.Settings.ServicePort
        End If

        If My.Settings.DataPort = "" Then
            m_DataPort = "9002"
        Else
            m_DataPort = My.Settings.DataPort
        End If

    End Sub

    Private Function SetupHost(ByVal URIString As String) As Boolean

        Try

            Dim _Uri As New Uri(URIString)
            Dim _URIList As New List(Of Uri)
            _URIList.Add(_Uri)

            Dim _Behaviour As New ServiceMetadataBehavior

            Dim _TCPBinding As New NetTcpBinding
            With _TCPBinding

                .ReceiveTimeout = m_10Mins
                .SendTimeout = m_10Mins
                .MaxBufferPoolSize = m_BufferSize
                .MaxBufferSize = m_BufferSize
                .MaxReceivedMessageSize = m_BufferSize

                .ReaderQuotas.MaxDepth = m_BufferSize
                .ReaderQuotas.MaxArrayLength = m_BufferSize
                .ReaderQuotas.MaxStringContentLength = m_BufferSize
                .ReaderQuotas.MaxBytesPerRead = m_BufferSize
                .ReaderQuotas.MaxNameTableCharCount = m_BufferSize

                .Security.Mode = SecurityMode.None

                '.Security.Mode = SecurityMode.Transport
                '.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate
                '.Security.Transport.ProtectionLevel = ProtectionLevel.EncryptAndSign

            End With

            m_ServiceHost = New ServiceHost(GetType(GenieServiceLib.NurseryGenieLocal), _URIList.ToArray)
            With m_ServiceHost
                .Description.Behaviors.Add(_Behaviour)
                .AddServiceEndpoint(GetType(GenieServiceLib.INurseryGenieLocal), _TCPBinding, _Uri)
                .AddServiceEndpoint(GetType(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding, "mex")
                .Open()
            End With

        Catch ex As Exception
            EventLogging.LogEvent("SetupHost", ex)
            Return False
        End Try

        EventLogging.LogEvent("Setup NurseryGenieService Host Successfully", EventLogEntryType.Information)
        Return True

    End Function

    Private Function SetupDataHost(ByVal URIString As String) As Boolean

        If My.Settings.CertPath = "" Then
            EventLogging.LogEvent("NurseryGenieDataService: Certificate Path is blank.", EventLogEntryType.Information)
            Return False
        Else

            If IO.File.Exists(My.Settings.CertPath) Then

                Try

                    Dim _Uri As New Uri(URIString)
                    Dim _URIList As New List(Of Uri)
                    _URIList.Add(_Uri)

                    Dim _Behaviour As New ServiceMetadataBehavior
                    Dim _Cert As New X509Certificate2(My.Settings.CertPath, My.Settings.CertPassword)

                    Dim _TCPBinding As New NetTcpBinding
                    With _TCPBinding

                        .ReceiveTimeout = m_10Mins
                        .SendTimeout = m_10Mins
                        .MaxBufferPoolSize = m_BufferSize
                        .MaxBufferSize = m_BufferSize
                        .MaxReceivedMessageSize = m_BufferSize

                        .ReaderQuotas.MaxDepth = m_BufferSize
                        .ReaderQuotas.MaxArrayLength = m_BufferSize
                        .ReaderQuotas.MaxStringContentLength = m_BufferSize
                        .ReaderQuotas.MaxBytesPerRead = m_BufferSize
                        .ReaderQuotas.MaxNameTableCharCount = m_BufferSize

                        .Security.Mode = SecurityMode.None

                        '.Security.Mode = SecurityMode.Transport
                        '.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate
                        '.Security.Transport.ProtectionLevel = ProtectionLevel.EncryptAndSign

                    End With

                    m_DataServiceHost = New ServiceHost(GetType(GenieServiceLib.NurseryGenieData), _URIList.ToArray)
                    With m_DataServiceHost
                        .Description.Behaviors.Add(_Behaviour)
                        '.Credentials.ServiceCertificate.Certificate = _Cert
                        .AddServiceEndpoint(GetType(GenieServiceLib.INurseryGenieData), _TCPBinding, _Uri)
                        .AddServiceEndpoint(GetType(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding, "mex")
                        .Open()
                    End With

                Catch ex As Exception
                    EventLogging.LogEvent("SetupDataHost", ex)
                    Return False
                End Try

            Else
                EventLogging.LogEvent("NurseryGenieDataService: Certificate not found in " + My.Settings.CertPath, EventLogEntryType.Warning)
                Return False
            End If

        End If

        EventLogging.LogEvent("Setup NurseryGenieDataService Host Successfully", EventLogEntryType.Information)
        Return True

    End Function

End Class
