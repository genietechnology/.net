﻿Imports System.ComponentModel
Imports System.Configuration.Install

Public Class GenieInstaller

    Public Sub New()

        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent

    End Sub

    Protected Overrides Sub OnBeforeInstall(savedState As IDictionary)
        SetServiceDetails()
        MyBase.OnBeforeInstall(savedState)
    End Sub

    Protected Overrides Sub OnBeforeUninstall(savedState As IDictionary)
        SetServiceDetails()
        MyBase.OnBeforeUninstall(savedState)
    End Sub

    Private Sub SetServiceDetails()

        If Context.Parameters.ContainsKey("ServiceName") Then
            ServiceInstaller1.ServiceName = Context.Parameters("ServiceName")
        End If

        If Context.Parameters.ContainsKey("DisplayName") Then
            ServiceInstaller1.DisplayName = Context.Parameters("DisplayName")
        End If

    End Sub



End Class
