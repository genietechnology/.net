﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnPDF = New System.Windows.Forms.Button()
        Me.btnService = New System.Windows.Forms.Button()
        Me.btnGeorge = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnPDF
        '
        Me.btnPDF.Location = New System.Drawing.Point(12, 60)
        Me.btnPDF.Name = "btnPDF"
        Me.btnPDF.Size = New System.Drawing.Size(210, 23)
        Me.btnPDF.TabIndex = 0
        Me.btnPDF.Text = "Get PDF"
        Me.btnPDF.UseVisualStyleBackColor = True
        '
        'btnService
        '
        Me.btnService.Location = New System.Drawing.Point(12, 12)
        Me.btnService.Name = "btnService"
        Me.btnService.Size = New System.Drawing.Size(210, 23)
        Me.btnService.TabIndex = 1
        Me.btnService.Text = "Start Service"
        Me.btnService.UseVisualStyleBackColor = True
        '
        'btnGeorge
        '
        Me.btnGeorge.Location = New System.Drawing.Point(12, 89)
        Me.btnGeorge.Name = "btnGeorge"
        Me.btnGeorge.Size = New System.Drawing.Size(210, 23)
        Me.btnGeorge.TabIndex = 2
        Me.btnGeorge.Text = "Email George Pig"
        Me.btnGeorge.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(789, 279)
        Me.Controls.Add(Me.btnGeorge)
        Me.Controls.Add(Me.btnService)
        Me.Controls.Add(Me.btnPDF)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnService As Button
    Friend WithEvents btnGeorge As Button
    Friend WithEvents btnPDF As Button
End Class
