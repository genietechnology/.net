﻿Imports System.ServiceModel
Imports GenieServiceLib

Public Class Form1

    Private Sub btnPDF_Click(sender As Object, e As EventArgs) Handles btnPDF.Click

        Dim ChildID As String = InputBox("ChildID:")
        Dim _Service = New NurseryGenieService.NurseryGenieLocalClient
        Dim _File As NurseryGenieService.GeneratedFile = Nothing

        Try
            _File = _Service.GetReportPDF(ChildID)
            _Service.Close()

        Catch fex As FaultException
            MsgBox(fex.Message, MsgBoxStyle.Exclamation, "Fault Exception")
            _Service.Abort()

        Catch tex As TimeoutException
            MsgBox(tex.Message, MsgBoxStyle.Exclamation, "TimeOut Exception")
            _Service.Abort()

        Catch cex As CommunicationException
            MsgBox(cex.Message, MsgBoxStyle.Exclamation, "Communication Exception")
            _Service.Abort()
        End Try

        If _File IsNot Nothing Then

            Dim _PDF As String = My.Application.Info.DirectoryPath + "\" + _File.FileName
            IO.File.WriteAllBytes(_PDF, _File.FileData)

            If IO.File.Exists(_PDF) Then
                Process.Start(_PDF)
            End If

        Else
            MsgBox("File is nothing.")
        End If

    End Sub

    Private Sub btnService_Click(sender As Object, e As EventArgs) Handles btnService.Click

        btnService.Enabled = False
        Me.Refresh()

        Dim _Scheduler = New TaskScheduler("localhost", "dev", "careuser", "careuser", "", "")

        If _Scheduler.StartScheduler() Then

        Else

        End If

    End Sub

    Private Sub btnGeorge_Click(sender As Object, e As EventArgs) Handles btnGeorge.Click

        Dim _Service = New NurseryGenieLocal
        _Service.EmailReports("", "", "JAMES-W530", "0E5EA0B1-44EC-417B-9A21-0C90EE5221F3")

    End Sub
End Class
