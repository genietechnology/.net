﻿' NOTE: You can use the "Rename" command on the context menu to change the interface name "IService1" in both code and config file together.
<ServiceContract()>
Public Interface INurseryGenieLocal

    <OperationContract()>
    Function GetReportPDF(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal ChildID As String) As GeneratedFile

    <OperationContract()>
    Function EmailReports(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal ChildID As String) As Integer

End Interface

<DataContract()>
Public Class GeneratedFile

    <DataMember()>
    Public Property FileName() As String

    <DataMember()>
    Public Property FileType() As String

    <DataMember()>
    Public Property FileData() As Byte()

    <DataMember()>
    Public Property FileStatus() As Integer

End Class

<DataContract()>
Public Class LiveRegister

    <DataMember()>
    Public Property ChildID() As String

    <DataMember()>
    Public Property GroupName() As String

    <DataMember()>
    Public Property ChildName() As String

    <DataMember()>
    Public Property StampIn() As Date?

    <DataMember()>
    Public Property StampOut() As Date?

    <DataMember()>
    Public Property LastToilet() As Date?

    <DataMember()>
    Public Property LastSleep() As Date?

    <DataMember()>
    Public Property LastBottle() As Date?

End Class
