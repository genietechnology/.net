﻿
Public Class EventLogging

    Private Shared m_Log As EventLog = Nothing

    Public Shared Sub SetupLog(ByVal ServiceName As String)

        Try

            If ServiceName = "" Then ServiceName = "GenieWindowsService"

            m_Log = New EventLog

            If Not EventLog.SourceExists(ServiceName) Then
                EventLog.CreateEventSource(ServiceName, "ServiceLog")
            End If

            m_Log.Source = ServiceName

        Catch ex As Exception
            m_Log = Nothing
            LogEvent("SetupLog", ex)
        End Try

    End Sub

    Public Shared Sub LogEvent(ByVal LogMessage As String, ByVal LogType As EventLogEntryType)
        If m_Log IsNot Nothing Then
            Try
                m_Log.WriteEntry(LogMessage, LogType)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Public Shared Sub LogEvent(ByVal MessageTitle As String, ByVal CaughtException As Exception)

        If m_Log IsNot Nothing Then

            Try

                Dim _Message As String = ""
                _Message += MessageTitle
                _Message += vbCrLf
                _Message += vbCrLf
                _Message += CaughtException.Message
                _Message += vbCrLf
                _Message += vbCrLf
                _Message += CaughtException.StackTrace

                m_Log.WriteEntry(_Message, EventLogEntryType.Error)

            Catch ex As Exception

            End Try

        End If

    End Sub

End Class
