﻿
Public Class TaskResult

    Property Result As Integer
    Property ErrorText As String
    Property ex As Exception

    Public Sub New()
        Me.Result = 0
        Me.ErrorText = ""
        Me.ex = Nothing
    End Sub

    Public Sub New(ByVal Result As Integer)
        Me.Result = Result
        Me.ErrorText = ""
        Me.ex = Nothing
    End Sub

    Public Sub New(ByVal Result As Integer, ByVal ErrorText As String, ByVal Ex As Exception)
        Me.Result = Result
        Me.ErrorText = ErrorText
        Me.ex = Ex
    End Sub

End Class
