﻿Option Strict On

Imports Care.Global
Imports Care.Shared

Public Module PaxtonHelper

    Private Enum EnumInOut
        CheckedIn
        CheckedOut
    End Enum

    Private m_ACUSerial As String = ""
    Private WithEvents m_PaxtonController As PaxtonController = Nothing

    Public Sub Connect()

        Dim _Enabled As Boolean = ParameterHandler.ReturnBoolean("PAXTON")
        Dim _IP As String = ParameterHandler.ReturnString("PAXTONSERVER")
        Dim _User As String = ParameterHandler.ReturnString("PAXTONUSER")
        Dim _Password As String = ParameterHandler.ReturnString("PAXTONPASSWORD")
        m_ACUSerial = ParameterHandler.ReturnString("PAXTONACU")

        m_PaxtonController = New PaxtonController(_Enabled, _IP, _User, _Password, m_ACUSerial)
        If m_PaxtonController.ReadyToConnect Then
            m_PaxtonController.Connect()
        End If

    End Sub

    Private Sub HandleAccessEvent(ByVal UserID As Integer, ByVal EventDescription As String, ByVal SubAddress As Short)

        If EventDescription.ToUpper.StartsWith("ACCESSPERMITTED") Then

            'lookup the staff by the clockin number
            Dim _S As Nursery.Business.Staff = Nursery.Business.Staff.RetreiveByClockNo(UserID.ToString)
            If _S IsNot Nothing Then

                'check if the ACU is associated with a room
                Dim _Room As String = ReturnRoom(m_ACUSerial)

                If SubAddress = 1 Then
                    'interface 1 (IN)
                    CheckIn(_S, _Room)
                Else
                    'interface 2 (OUT, not always connected)
                    CheckOut(_S, _Room)
                End If

            End If

        End If

    End Sub

    Private Function ReturnRoom(ByVal ACUSerial As String) As String
        Return ""
    End Function

    Private Sub CheckIn(ByVal StaffRecord As Nursery.Business.Staff, ByVal Room As String)

        If Room = "" Then
            If Not RegisterCheck(StaffRecord._ID.Value, EnumInOut.CheckedIn) Then
                DoCheckIn(StaffRecord._ID.Value)
            End If
        Else
            'room move
        End If

    End Sub

    Private Sub CheckOut(ByVal StaffRecord As Nursery.Business.Staff, ByVal Room As String)

        If Room = "" Then
            'check if this person is already checked out...
            If Not RegisterCheck(StaffRecord._ID.Value, EnumInOut.CheckedOut) Then
                DoCheckOut(StaffRecord._ID.Value)
            End If
        Else
            'room move
        End If

    End Sub

    Private Function RegisterCheck(ByVal StaffID As Guid, ByVal InOut As EnumInOut) As Boolean

        Return False

    End Function

    Public Sub DoCheckIn(ByVal StaffID As Guid)


    End Sub

    Public Sub DoCheckOut(ByVal StaffID As Guid)


    End Sub

    Private Sub m_PaxtonController_AccessEvent(sender As Object, e As Paxton.Net2.OemClientLibrary.IEventView) Handles m_PaxtonController.AccessEvent
        HandleAccessEvent(e.UserId, e.EventTypeDescription, e.SubAddress)
    End Sub

End Module
