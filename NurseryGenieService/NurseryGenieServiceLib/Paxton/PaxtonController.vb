﻿Option Strict On

Imports Care.Global
Imports Care.Shared

Public Class PaxtonController

    Private WithEvents m_ACU As Care.TAAC.Controller = Nothing

    Public Event AccessEvent(sender As Object, e As Paxton.Net2.OemClientLibrary.IEventView)

    Private m_Net2ServerIP As String = ""
    Private m_Net2User As String = ""
    Private m_Net2Password As String = ""

    Private m_ACUSerial As String = ""

    Private m_Connected As Boolean = False
    Private m_ReadyToConnect As Boolean = False

    Public ReadOnly Property ReadyToConnect As Boolean
        Get
            Return m_ReadyToConnect
        End Get
    End Property

    Public ReadOnly Property Connected As Boolean
        Get
            Return m_Connected
        End Get
    End Property

    Public Sub New(ByVal Enabled As Boolean, _
                   ByVal Net2ServerIP As String, ByVal Net2User As String, ByVal Net2Password As String, ByVal ACUSerial As String)

        If Enabled Then

            m_Net2ServerIP = Net2ServerIP
            m_Net2User = Net2User
            m_Net2Password = Net2Password
            m_ACUSerial = ACUSerial

            If m_Net2ServerIP = "" Then EventLogging.LogEvent("Paxton Server parameter is blank.", EventLogEntryType.Warning)
            If m_Net2User = "" Then EventLogging.LogEvent("Paxton User parameter is blank.", EventLogEntryType.Warning)
            If m_Net2Password = "" Then EventLogging.LogEvent("Paxton Password parameter is blank.", EventLogEntryType.Warning)
            If m_ACUSerial = "" Then EventLogging.LogEvent("Paxton Integration is enabled - but no ACU to monitor.", EventLogEntryType.Warning)

            If m_Net2ServerIP <> "" AndAlso m_Net2User <> "" AndAlso m_Net2Password <> "" AndAlso m_ACUSerial <> "" Then
                m_ReadyToConnect = True
            End If

        Else
            EventLogging.LogEvent("Paxton Integration is disabled", EventLogEntryType.Information)
        End If

    End Sub

    Public Function Connect() As Boolean

        If Not m_ReadyToConnect Then Return False

        '***********************************************************************************************

        EventLogging.LogEvent("Connecting to Net2 Server: " + m_Net2ServerIP, EventLogEntryType.Information)

        m_ACU = New Care.TAAC.Controller(True, m_Net2ServerIP, m_Net2User, m_Net2Password)

        Dim _ErrorText As String = ""
        If m_ACU.Connect(_ErrorText) Then
            If m_ACU.MonitorDoor(ValueHandler.ConvertInteger(m_ACUSerial)) Then
                EventLogging.LogEvent("Monitoring ACU: " + m_ACUSerial, EventLogEntryType.Information)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If


        '***********************************************************************************************

    End Function

    Public Sub Disconnect()
        m_ACU.Disconnect()
    End Sub

    Private Sub m_ACU_AccessEvent(sender As Object, e As Paxton.Net2.OemClientLibrary.IEventView) Handles m_ACU.AccessEvent
        RaiseEvent AccessEvent(sender, e)
    End Sub

End Class
