﻿<ServiceContract()>
Public Interface INurseryGenieData


#Region "Static Items - Children, Contacts, Staff etc"

    <OperationContract()>
    Function GetChildren(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Child)

    <OperationContract()>
    Function GetRooms(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.SiteRoom)

    <OperationContract()>
    Function GetStaff(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Staff)

    <OperationContract()>
    Function GetContacts(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Contact)

#End Region

#Region "Day Specific Items"

    <OperationContract()>
    Function GetDay(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As Model.Day

    <OperationContract()>
    Function GetBookings(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Booking)

    <OperationContract()>
    Function GetActivity(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Activity)

    <OperationContract()>
    Function UploadActivity(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal ActivityData As List(Of Model.Activity)) As Integer

    <OperationContract()>
    Function GetAss(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Ass)

    <OperationContract()>
    Function GetFoodRegister(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.FoodRegister)

    <OperationContract()>
    Function UploadFoodRegister(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal FoodRegisterData As List(Of Model.FoodRegister)) As Integer

    <OperationContract()>
    Function GetIncidents(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Incident)

    <OperationContract()>
    Function UploadIncidents(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal IncidentData As List(Of Model.Incident)) As Integer

    <OperationContract()>
    Function GetMedia(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Media)

    <OperationContract()>
    Function UploadMedia(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal MediaData As List(Of Model.Media)) As Integer

    <OperationContract()>
    Function GetMedicineAuth(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.MedicineAuth)

    <OperationContract()>
    Function GetMedicineLog(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.MedicineLog)

    <OperationContract()>
    Function GetObservations(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Obs)

    <OperationContract()>
    Function GetRegister(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Register)

    <OperationContract()>
    Function UploadRegister(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal RegisterData As List(Of Model.Register)) As Integer

    <OperationContract()>
    Function GetSignatures(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal DateFilter As Date) As List(Of Model.Signature)

    <OperationContract()>
    Function UploadSignatures(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ByVal SignatureData As List(Of Model.Signature)) As Integer

#End Region

#Region "Supporting Elements - List, Parameters etc"

    <OperationContract()>
    Function GetParameters(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.TabletParam)

    <OperationContract()>
    Function GetElementLists(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.AppList)

    <OperationContract()>
    Function GetFood(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Food)

    <OperationContract()>
    Function GetRequests(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Requests)

    <OperationContract()>
    Function GetSites(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As List(Of Model.Site)

#End Region

#Region "Other Functions"

    <OperationContract()>
    Function Ping(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As String

    <OperationContract()>
    Function GetLiveRegister(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As LiveRegister()

    <OperationContract()>
    Function AttachChildPhoto(ByVal APIKey As String, ByVal APIPassword As String, ByVal ChildID As String, ByVal MediaID As String) As Integer

    <OperationContract()>
    Function AttachStaffPhoto(ByVal APIKey As String, ByVal APIPassword As String, ByVal StaffID As String, ByVal File As GeneratedFile) As Integer

    <OperationContract()>
    Function AttachContactPhoto(ByVal APIKey As String, ByVal APIPassword As String, ByVal ContactID As String, ByVal File As GeneratedFile) As Integer

#End Region

End Interface
