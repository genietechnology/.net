﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.ComponentModel
Imports System.Timers

Public Class TaskScheduler

    Private WithEvents m_Timer10Seconds As Timers.Timer
    Private WithEvents m_TimerMinute As Timers.Timer

    Private WithEvents m_Worker1 As New BackgroundWorker
    Private WithEvents m_Worker2 As New BackgroundWorker
    Private WithEvents m_Worker3 As New BackgroundWorker

    Private m_SQLServer As String = ""
    Private m_SQLDatabase As String = ""
    Private m_SQLUser As String = ""
    Private m_SQLPassword As String = ""
    Private m_ConnectionString As String = ""
    Private m_ConnectDone As Boolean = False

    Private m_ReportFolder As String = ""
    Private m_TempFolder As String = ""

    Public Sub New(ByVal SQLServer As String, ByVal SQLDatabase As String,
               ByVal SQLUser As String, ByVal SQLPassword As String,
               ByVal ReportFolder As String, ByVal TempFolder As String)

        m_SQLServer = SQLServer
        m_SQLDatabase = SQLDatabase
        m_SQLUser = SQLUser
        m_SQLPassword = SQLPassword
        m_ReportFolder = ReportFolder
        m_TempFolder = TempFolder

    End Sub

#Region "Public Methods"

    Public Function StartScheduler() As Boolean

        m_ConnectDone = Me.SetupSession()

        m_Timer10Seconds = New Timers.Timer
        m_Timer10Seconds.Interval = 10000
        m_Timer10Seconds.Start()


        m_TimerMinute = New Timers.Timer
        m_TimerMinute.Interval = 60000
        m_TimerMinute.Start()

        Return m_ConnectDone

    End Function

    Public Function StopScheduler() As Boolean

    End Function

#End Region

    Private Sub ProcessQueue()

        m_Timer10Seconds.Stop()

        Dim _SQL As String = "select ID from AppTaskQueue where status = 2 order by entered_stamp"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _ID As String = _DR.Item("ID").ToString
                If _ID <> "" Then

                    Dim _q As Business.AppTaskQueue = Business.AppTaskQueue.RetreiveByID(New Guid(_ID))
                    If _q IsNot Nothing Then

                        'loop until we have a free worker
                        Dim _Worker As Integer = 0
                        While _Worker < 1

                            If Not m_Worker1.IsBusy Then
                                _Worker = 1
                                m_Worker1.RunWorkerAsync(_ID)
                            Else
                                If Not m_Worker2.IsBusy Then
                                    _Worker = 2
                                    m_Worker2.RunWorkerAsync(_ID)
                                Else
                                    If Not m_Worker3.IsBusy Then
                                        _Worker = 3
                                        m_Worker3.RunWorkerAsync(_ID)
                                    End If
                                End If
                            End If

                        End While
                    End If
                End If
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        m_Timer10Seconds.Start()

    End Sub

    Private Sub AddToQueue()

        'dump any scheduled tasks into the queue

        Dim _CurrentTime As String = Format(Now, "HH:mm").ToString
        Dim _SQL As String = "select * from AppTasks where (time = '" + _CurrentTime + "' OR time is null) order by time"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As String = _DR.Item("ID").ToString
                If _ID <> "" Then
                    Dim _q As New Business.AppTaskQueue
                    _q._TaskId = New Guid(_DR.Item("ID").ToString)
                    _q._Status = 2
                    _q._Name = _DR.Item("name").ToString
                    _q._Description = _DR.Item("description").ToString
                    _q._TaskType = _DR.Item("task_type").ToString
                    _q._ObjectName = _DR.Item("object_name").ToString
                    _q._Command = _DR.Item("command").ToString
                    _q._Args = _DR.Item("args").ToString
                    _q._EnteredStamp = Now
                    _q.Store()
                End If
            Next
            _DT.Dispose()
            _DT = Nothing
        End If

    End Sub

    Private Sub ExecuteTask(e As DoWorkEventArgs)
        Dim _TaskID As Guid = New Guid(e.Argument.ToString)
        Dim _t As New Task(m_SQLServer, m_SQLDatabase, m_SQLUser, m_SQLPassword, m_ReportFolder, m_TempFolder, _TaskID)
        Dim _r As TaskResult = _t.Execute()
    End Sub

#Region "Helpers"

    Private Function SetupSession() As Boolean

        Dim _Return As Boolean = False

        Session.ServiceMode()

        Session.CurrentUser = New Care.Global.User()
        Session.CurrentUser.ID = Guid.NewGuid
        Session.CurrentUser.FullName = "Nursery Genie Service"

        If m_ConnectionString = "" Then

            Dim _b As New ConnectionBuilder
            With _b
                .Server = m_SQLServer
                .DB = m_SQLDatabase
                .User = m_SQLUser
                .Password = m_SQLPassword
                .IntegratedSecurity = False
                .TestConnection()
            End With

            If _b.ConnectionOK Then
                m_ConnectionString = _b.ConnectionString
                Session.ConnectionString = m_ConnectionString
                EventLogging.LogEvent("Connection to SQL Successful", EventLogEntryType.Information)
                _Return = Setup.Execute
            End If

        Else
            _Return = Setup.Execute
        End If

        If Session.TempFolder = "" Then Session.TempFolder = m_TempFolder
        If Session.ReportFolder = "" Then Session.ReportFolder = m_ReportFolder

        If _Return Then
            EventLogging.LogEvent("Setup Session OK", EventLogEntryType.Information)
        Else
            EventLogging.LogEvent("Setup Session Failed", EventLogEntryType.Error)
        End If

        Return _Return

    End Function

#End Region

#Region "Timers"

    Private Sub m_Timer10Seconds_Elapsed(sender As Object, e As ElapsedEventArgs) Handles m_Timer10Seconds.Elapsed
        ProcessQueue()
    End Sub

    Private Sub m_TimerMinute_Elapsed(sender As Object, e As ElapsedEventArgs) Handles m_TimerMinute.Elapsed
        AddToQueue()
    End Sub

#End Region

#Region "Workers"

    Private Sub m_Worker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles m_Worker1.DoWork
        ExecuteTask(e)
    End Sub

    Private Sub m_Worker2_DoWork(sender As Object, e As DoWorkEventArgs) Handles m_Worker2.DoWork
        ExecuteTask(e)
    End Sub

    Private Sub m_Worker3_DoWork(sender As Object, e As DoWorkEventArgs) Handles m_Worker3.DoWork
        ExecuteTask(e)
    End Sub

    Private Sub m_Worker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles m_Worker1.RunWorkerCompleted

    End Sub

    Private Sub m_Worker2_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles m_Worker2.RunWorkerCompleted

    End Sub

    Private Sub m_Worker3_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles m_Worker3.RunWorkerCompleted

    End Sub

#End Region

End Class
