﻿Option Strict On

Imports System.Net.Security
Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports GenieServiceLib.Model
Imports Nursery.Business
Imports Activity = GenieServiceLib.Model.Activity

Public Class NurseryGenieData

    Implements INurseryGenieData

    Public Function Ping(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As String Implements INurseryGenieData.Ping
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return ""
        Return My.Application.Info.Version.ToString()
    End Function

    Private Function Authenticate(ByVal APIKey As String, ByVal APIPassword As String) As Boolean
        Return True
    End Function

    Private Function Authenticate(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String) As Boolean
        Return True
    End Function

#Region "Getters"

    Public Function GetBookings(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.Booking) Implements INurseryGenieData.GetBookings

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Dim _Return As New List(Of Model.Booking)

        For Each _B In Nursery.Business.Booking.RetreiveBySiteDateRange(ReturnSiteID(DeviceName), DateFilter, DateFilter)

            Dim _BookingRecord As New Model.Booking
            With _BookingRecord
                .ID = _B._ID
                .BookingDate = _B._BookingDate
                .BookingFrom = _B._BookingFrom
                .BookingTo = _B._BookingTo
                .ChildId = _B._ChildId
                .ChildName = _B._ChildName
                .ChildDob = _B._ChildDob
                .ChildAge = _B._ChildAge
                .ChildGroup = ""
                .TariffId = _B._TariffId
                .TariffName = _B._TariffName
                .TariffRate = _B._TariffRate
                .RoomId = Nothing
                .RoomName = GetRoomName(_B._RatioId.ToString)
                .Am = False
                .Pm = False
                .BookingCount = 0
                .Hours = _B._Hours
                .FundedHours = _B._FundedHours
                .FullTime = False
                .Points = 0
            End With

            _Return.Add(_BookingRecord)

        Next

        Return _Return

    End Function

    Private Function GetRoomName(RatioID As String) As String

        Dim _sql As String = ""

        _sql = String.Concat("select room_name from SiteRoomRatios srr",
                                " left join SiteRooms sr on sr.ID = srr.room_id",
                                " where srr.id = '", RatioID, "'")

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _sql)
        If _DT IsNot Nothing Then
            Return _DT.Rows(0).Item("room_name").ToString
        End If

    End Function

    Public Function GetActivity(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Activity) Implements INurseryGenieData.GetActivity

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        'Dim _DayID As Guid? = Nursery.Business.Day.ReturnIDFromDate(DateFilter)
        'If Not _DayID.HasValue Then Return Nothing

        'Dim _DB As List(Of Nursery.Business.FoodRegister) = Nursery.Business.FoodRegister.RetreiveByDate(_DayID.Value)
        'If _DB IsNot Nothing Then

        '    Dim _Return As New List(Of Model.FoodRegister)
        '    For Each _DBRegister In _DB

        '        Dim _R As New Model.FoodRegister
        '        With _DBRegister
        '            _R.ID = ._ID.Value
        '            _R.DayID = ._DayId.Value
        '            _R.ChildID = _DBRegister._ChildId.Value
        '            _R.ChildName = _DBRegister._ChildName
        '            _R.MealID = _DBRegister._MealId.Value
        '            _R.MealType = _DBRegister._MealType
        '            _R.FoodID = _DBRegister._FoodId.Value
        '            _R.FoodName = _DBRegister._FoodName
        '            _R.FoodStatus = ValueHandler.ConvertInteger(_DBRegister._FoodStatus)
        '        End With

        '        _Return.Add(_R)

        '    Next

        '    Return _Return

        'Else
        '    Return Nothing
        'End If

    End Function

    Public Function GetAss(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Ass) Implements INurseryGenieData.GetAss

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

    End Function

    Public Function GetDay(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As Model.Day Implements INurseryGenieData.GetDay

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _Day As Nursery.Business.Day = Nursery.Business.Day.RetreiveByDateAndSite(Today, ReturnSiteID(DeviceName))

        If _Day IsNot Nothing Then

            Dim _Return As New Model.Day
            With _Return
                .ID = _Day._ID
                .DayDate = _Day._Date
                .BreakfastId = _Day._BreakfastId
                .BreakfastName = _Day._BreakfastName
                .SnackId = _Day._SnackId
                .SnackName = _Day._SnackName
                .LunchId = _Day._LunchId
                .LunchName = _Day._LunchName
                .LunchDesId = _Day._LunchDesId
                .LunchDesName = _Day._LunchDesName
                .PMSnackId = _Day._SnackPmId
                .PMSnackName = _Day._SnackPmName
                .TeaId = _Day._TeaId
                .TeaName = _Day._TeaName
                .TeaDesId = _Day._TeaDesId
                .TeaDesName = _Day._TeaDesName
                .Message = _Day._Message
            End With

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Public Function GetElementLists(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of AppList) Implements INurseryGenieData.GetElementLists

        Dim _Return As New List(Of AppList)
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return _Return

        For Each _List In Business.AppList.RetreiveAll()

            Dim _Items As DataRowCollection = ListHandler.ReturnItemsWithReferences(_List._Name)
            For Each _DR As DataRow In _Items

                Dim _AppList As New AppList
                With _AppList

                    .ListId = _List._ID
                    .ListName = _List._Name

                    .ID = New Guid(_DR.Item(("ID")).ToString)
                    .Name = _DR.Item("name").ToString
                    .Ref1 = _DR.Item("ref_1").ToString
                    .Ref2 = _DR.Item("ref_2").ToString
                    .Ref3 = _DR.Item("ref_3").ToString
                    .Hide = ValueHandler.ConvertBoolean(_DR.Item("hide"))
                    .Seq = ValueHandler.ConvertInteger(_DR.Item("seq"))

                End With

                _Return.Add(_AppList)

            Next

        Next

        Return _Return

    End Function

    Public Function GetFood(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Food) Implements INurseryGenieData.GetFood

        Dim _Return As New List(Of Model.Food)
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return _Return

        Dim _SQL As String = ""
        _SQL += "select c.meal_id, m.name as 'meal_name', m.breakfast, m.snack, m.lunch, m.tea, m.dessert,"
        _SQL += " c.food_id, f.name as 'food_name', f.group_name, f.baby_food"
        _SQL += " from MealComponents c"
        _SQL += " left join Meals m on m.ID = c.meal_id"
        _SQL += " left join Food f on f.id = c.food_id"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _F As New Model.Food
                With _F

                    .MealID = New Guid(_DR.Item("meal_id").ToString())
                    .MealName = _DR.Item("meal_name").ToString()

                    .Breakfast = ValueHandler.ConvertBoolean(_DR.Item("breakfast"))
                    .Snack = ValueHandler.ConvertBoolean(_DR.Item("snack"))
                    .Lunch = ValueHandler.ConvertBoolean(_DR.Item("lunch"))
                    .Dessert = ValueHandler.ConvertBoolean(_DR.Item("dessert"))
                    .Tea = ValueHandler.ConvertBoolean(_DR.Item("tea"))

                    .FoodID = New Guid(_DR.Item("food_id").ToString())
                    .FoodName = _DR.Item("food_name").ToString()
                    .FoodGroup = _DR.Item("group_name").ToString()
                    .BabyFood = ValueHandler.ConvertBoolean(_DR.Item("baby_food"))

                End With

                _Return.Add(_F)

            Next

        End If

        Return _Return

    End Function

    Public Function GetRequests(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Requests) Implements INurseryGenieData.GetRequests

        Dim _Return As New List(Of Requests)
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return _Return

        For Each _Request In Request.RetreiveAll()

            Dim _R As New Requests
            With _R
                .ID = _Request._ID.Value
                .Name = _Request._Description
                .ReportText = _Request._ReportText
            End With

            _Return.Add(_R)

        Next

        Return _Return

    End Function

    Public Function GetSites(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Site) Implements INurseryGenieData.GetSites

        Dim _Return As New List(Of Model.Site)
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return _Return

        For Each _Site In Nursery.Business.Site.RetreiveAll()

            Dim _S As New Model.Site
            With _S
                .ID = _Site._ID.Value
                .Name = _Site._Name
            End With

            _Return.Add(_S)

        Next

        Return _Return

    End Function

    Public Function GetFoodRegister(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.FoodRegister) Implements INurseryGenieData.GetFoodRegister

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _DayID As Guid = ReturnDayID(ReturnSiteID(DeviceName))

        Dim _DB As List(Of Nursery.Business.FoodRegister) = Nursery.Business.FoodRegister.RetreiveByDate(_DayID)
        If _DB IsNot Nothing Then

            Dim _Return As New List(Of Model.FoodRegister)
            For Each _DBRegister In _DB

                Dim _R As New Model.FoodRegister
                With _DBRegister
                    _R.ID = ._ID.Value
                    _R.DayID = ._DayId.Value
                    _R.ChildID = _DBRegister._ChildId.Value
                    _R.ChildName = _DBRegister._ChildName
                    _R.MealID = _DBRegister._MealId.Value
                    _R.MealType = _DBRegister._MealType
                    _R.FoodID = _DBRegister._FoodId.Value
                    _R.FoodName = _DBRegister._FoodName
                    _R.FoodStatus = _DBRegister._FoodStatus
                End With

                _Return.Add(_R)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Public Function GetIncidents(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.Incident) Implements INurseryGenieData.GetIncidents
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetMedia(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.Media) Implements INurseryGenieData.GetMedia
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetMedicineAuth(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.MedicineAuth) Implements INurseryGenieData.GetMedicineAuth
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetMedicineLog(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.MedicineLog) Implements INurseryGenieData.GetMedicineLog
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetObservations(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Obs) Implements INurseryGenieData.GetObservations
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetRegister(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.Register) Implements INurseryGenieData.GetRegister

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _DB As List(Of Nursery.Business.Register) = Nursery.Business.Register.RetreiveByDate(DateFilter)
        If _DB IsNot Nothing Then

            Dim _Return As New List(Of Model.Register)
            For Each _DBRegister In _DB

                Dim _R As New Model.Register
                With _DBRegister
                    _R.ID = ._ID.Value
                    _R.RegisterDate = ._Date.Value
                    _R.RegisterType = _DBRegister._Type
                    _R.InOut = _DBRegister._InOut
                    _R.PersonID = _DBRegister._PersonId.Value
                    _R.PersonName = _DBRegister._Name
                    _R.StampIn = _DBRegister._StampIn
                    _R.StampOut = _DBRegister._StampOut
                    _R.Location = _DBRegister._Location
                    _R.Ref1 = _DBRegister._Ref1
                    _R.Ref2 = _DBRegister._Ref2
                    _R.Ref3 = _DBRegister._Ref3
                End With

                _Return.Add(_R)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Public Function GetSignatures(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, DateFilter As Date) As List(Of Model.Signature) Implements INurseryGenieData.GetSignatures
        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing
        Return Nothing
    End Function

    Public Function GetParameters(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.TabletParam) Implements INurseryGenieData.GetParameters

        Dim _DB As List(Of Nursery.Business.TabletParam) = Nursery.Business.TabletParam.RetreiveAll()
        If _DB IsNot Nothing Then

            Dim _Return As New List(Of Model.TabletParam)
            For Each _DBParam In _DB

                Dim _P As New Model.TabletParam
                With _P
                    .ID = _DBParam._ID
                    .Parent = _DBParam._Parent
                    .Device = _DBParam._Device
                    .ParamId = _DBParam._ParamId
                    .ParamDesc = _DBParam._ParamDesc
                    .ParamType = _DBParam._ParamType
                    .ParamValue = _DBParam._ParamValue
                End With

                _Return.Add(_P)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

#End Region

#Region "Uploads"

    Public Function UploadRegister(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, RegisterData As List(Of Model.Register)) As Integer Implements INurseryGenieData.UploadRegister

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        Dim _DayID As Guid = ReturnDayID(ReturnSiteID(DeviceName))

        For Each _UR In RegisterData

            'first, we check if this ID has already been stored
            Dim _R As Nursery.Business.Register = Nursery.Business.Register.RetreiveByID(_UR.ID)
            If _R IsNot Nothing Then
                'register record already exists
                Continue For
            End If

            _R = New Nursery.Business.Register()

            Dim _StoreRegisterEntry As Boolean = True

            If _UR.InOut = "I" Then

                'check-in
                'get the earliest check-in for this person
                'if this check-in is earlier, we will replace the one we have already
                'this can happen if one site has multiple tablets and the child has been "seen" earlier on this tablet.

                'update _StoreRegisterEntry accordingly

            Else

                'check-out, same logic as check-in, as we will be interested in the earliest time a child is checked out
                If _UR.InOut = "O" Then

                    'update _StoreRegisterEntry accordingly


                    'obviously, if the child has already been checked out, we won't want to send reports again etc.

                End If

            End If

            If _StoreRegisterEntry Then

                With _R

                    ._ID = _UR.ID
                    ._Date = _UR.RegisterDate
                    ._Type = _UR.RegisterType
                    ._InOut = _UR.InOut
                    ._PersonId = _UR.PersonID
                    ._Name = _UR.PersonName

                    ._StampIn = Nothing
                    ._StampOut = Nothing

                    If _UR.InOut = "I" Then
                        ._StampIn = _UR.StampIn
                    Else
                        If _UR.InOut = "O" Then
                            ._StampOut = _UR.StampOut
                        End If
                    End If

                    ._Location = _UR.Location
                    ._Ref1 = _UR.Ref1
                    ._Ref2 = _UR.Ref2
                    ._Ref3 = _UR.Ref3

                    .Store()

                End With

                'update the register summary table
                UpdateRegisterSummary(_DayID, _UR.PersonID, _UR.PersonName, _UR.RegisterType)

            End If

        Next

        Return 0

    End Function

    Public Function UploadActivity(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, ActivityData As List(Of Activity)) As Integer Implements INurseryGenieData.UploadActivity

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        For Each _A In ActivityData

            'first, we check if this ID has already been stored
            Dim _Check As Nursery.Business.Activity = Nursery.Business.Activity.RetreiveByID(_A.ID)
            If _Check IsNot Nothing AndAlso Not _Check.IsNew Then
                'register record already exists
                Continue For
            End If

            Dim _StoreActivity As New Nursery.Business.Activity
            With _StoreActivity
                ._ID = _A.ID
                ._DayId = _A.DayID
                ._KeyId = _A.KeyID
                ._KeyName = _A.KeyName
                ._Type = _A.KeyType
                ._Description = _A.Description
                ._Value1 = _A.Value1
                ._Value2 = _A.Value2
                ._Value3 = _A.Value3
                ._Stamp = _A.Stamp
                ._StaffId = _A.StaffID
                ._StaffName = _A.StaffName
                ._Notes = _A.Notes
                .Store()
            End With

        Next

        Return 0

    End Function

    Public Function UploadFoodRegister(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, FoodRegisterData As List(Of Model.FoodRegister)) As Integer Implements INurseryGenieData.UploadFoodRegister

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        For Each _FR In FoodRegisterData

            'first, we check if this ID has already been stored
            Dim _Check As Nursery.Business.FoodRegister = Nursery.Business.FoodRegister.RetreiveByID(_FR.ID)
            If _Check IsNot Nothing Then
                'food register record already exists
                With _Check
                    ._DayId = _FR.DayID
                    ._ChildId = _FR.ChildID
                    ._ChildName = _FR.ChildName
                    ._MealId = _FR.MealID
                    ._MealType = _FR.MealType
                    ._MealName = _FR.MealName
                    ._FoodId = _FR.FoodID
                    ._FoodName = _FR.FoodName
                    ._FoodStatus = _FR.FoodStatus
                    .Store()
                End With
                Continue For
            End If

            Dim _StoreRegister As New Nursery.Business.FoodRegister
            With _StoreRegister
                ._ID = _FR.ID
                ._DayId = _FR.DayID
                ._ChildId = _FR.ChildID
                ._ChildName = _FR.ChildName
                ._MealId = _FR.MealID
                ._MealType = _FR.MealType
                ._MealName = _FR.MealName
                ._FoodId = _FR.FoodID
                ._FoodName = _FR.FoodName
                ._FoodStatus = _FR.FoodStatus
                .Store()
            End With

        Next

        Return 0

    End Function

    Public Function UploadSignatures(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, SignatureData As List(Of Model.Signature)) As Integer Implements INurseryGenieData.UploadSignatures

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        For Each _S In SignatureData

            'first, we check if this ID has already been stored
            Dim _Check As Nursery.Business.Signature = Nursery.Business.Signature.RetreiveByID(_S.ID.Value)
            If _Check IsNot Nothing AndAlso Not _Check.IsNew Then
                Continue For
            End If

            Dim _StoreSignature As New Nursery.Business.Signature
            With _StoreSignature
                ._ID = _S.ID
                ._DayId = _S.DayId
                ._PersonId = _S.PersonId
                ._PersonType = _S.PersonType
                ._PersonName = _S.PersonName
                ._Signature = _S.Signature
                ._Stamp = _S.Stamp
                .Store()
            End With

        Next

        Return 0

    End Function

    Public Function UploadMedia(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, MediaData As List(Of Model.Media)) As Integer Implements INurseryGenieData.UploadMedia

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        For Each _M In MediaData

            'first, we check if this ID has already been stored
            Dim _Check As Nursery.Business.Media = Nursery.Business.Media.RetreiveByID(_M.ID.Value)
            If _Check IsNot Nothing AndAlso Not _Check.IsNew Then
                Continue For
            End If

            Dim _StoreMedia As New Nursery.Business.Media
            With _StoreMedia
                ._ID = _M.ID
                ._DayId = _M.DayId
                ._KeyType = _M.KeyType
                ._KeyId = _M.KeyId
                ._KeyName = ""
                ._Data = _M.Data
                ._DataSize = _M.FileSize
                ._DeviceName = _M.DeviceName
                ._UploadStamp = _M.Stamp
                .Store()
            End With

        Next

        Return 0

    End Function

    Public Function UploadIncidents(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String, IncidentData As List(Of Model.Incident)) As Integer Implements INurseryGenieData.UploadIncidents

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return -1

        For Each _I In IncidentData

            'first, we check if this ID has already been stored
            Dim _Check As Nursery.Business.Incident = Nursery.Business.Incident.RetreiveByID(_I.ID.Value)
            If _Check IsNot Nothing AndAlso Not _Check.IsNew Then
                Continue For
            End If

            Dim _StoreIncident As New Nursery.Business.Incident
            With _StoreIncident
                ._ID = _I.ID
                ._DayId = _I.DayId
                ._ChildId = _I.ChildId
                ._ChildName = _I.ChildName
                ._StaffId = _I.StaffId
                ._StaffName = _I.StaffName
                ._IncidentType = _I.IncidentType
                ._Location = _I.Location
                ._Details = _I.Details
                ._Injury = _I.Injury
                ._Treatment = _I.Treatment
                ._Action = _I.Action
                ._SigStaff = _I.SigStaff
                ._SigWitness = _I.SigWitness
                ._SigContact = _I.SigContact
                ._Photo = _I.Photo
                ._BodyMap = _I.BodyMap
                ._Stamp = _I.Stamp

                .Store()

            End With

        Next

        Return 0

    End Function

#End Region

    Public Function GetLiveRegister(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As LiveRegister() Implements INurseryGenieData.GetLiveRegister

        Dim _SQL As String = ""

        _SQL += "select c.ID, c.group_name as 'Group', c.fullname as 'Child', b.tariff_name as 'Session', ri.stamp_in as 'In', ro.stamp_out as 'Out',"
        _SQL += " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'TOILET' and a.day_id = d.ID order by a.stamp desc) as 'Last Toilet',"
        _SQL += " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'SLEEP' and a.day_id = d.ID order by a.stamp desc) as 'Last Sleep',"
        _SQL += " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'MILK' and a.day_id = d.ID order by a.stamp desc) as 'Last Bottle'"
        _SQL += " from Bookings b"
        _SQL += " left join Register ri on ri.person_id = b.child_id and ri.date = b.booking_date and ri.in_out = 'I'"
        _SQL += " left join Register ro on ro.person_id = b.child_id and ro.date = b.booking_date and ro.in_out = 'O'"
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += " left join Groups g on g.ID = c.group_id"
        _SQL += " left join Day d on d.date = b.booking_date"
        _SQL += " where b.booking_date = '" + ValueHandler.SQLDate(Today) + "'"
        _SQL += " order by g.sequence, ri.stamp_in desc, c.forename"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Dim _RL As New List(Of LiveRegister)
            For Each _DR As DataRow In _DT.Rows

                Dim _LR As New LiveRegister
                With _LR
                    .ChildID = _DR.Item("ID").ToString
                    .GroupName = _DR.Item("Group").ToString
                    .ChildName = _DR.Item("Child").ToString
                    .StampIn = ValueHandler.ConvertDate(_DR.Item("In"))
                End With

                _RL.Add(_LR)

            Next

            Return _RL.ToArray

        Else
            Return Nothing
        End If

    End Function

    Public Function AttachChildPhoto(ByVal APIKey As String, ByVal APIPassword As String, ChildID As String, ByVal MediaID As String) As Integer Implements INurseryGenieData.AttachChildPhoto

        If Not Authenticate(APIKey, APIPassword) Then Return -1
        Dim _Return As Integer = 0

        Try

            Dim _M As Nursery.Business.Media = Nursery.Business.Media.RetreiveByID(New Guid(MediaID))
            If _M IsNot Nothing Then

                Dim _C As Nursery.Business.Child = Nursery.Business.Child.RetreiveByID(New Guid(ChildID))
                If _C IsNot Nothing Then

                    If _C._Photo.HasValue Then
                        DocumentHandler.UpdateDocument(_C._Photo.Value, DocumentHandler.DocumentType.Application, _M._Data, "Photo of " + _C._Fullname)
                        _Return = 1
                    Else
                        Dim _ID As Guid? = DocumentHandler.InsertDocument(New Guid(ChildID), DocumentHandler.DocumentType.Application, _M._Data, "Photo of " + _C._Fullname, "")
                        _C._Photo = _ID
                        _C.Store()
                        _Return = 1
                    End If

                End If

            End If

        Catch ex As Exception
            _Return = -99
        End Try

        Return _Return

    End Function

    Public Function AttachContactPhoto(ByVal APIKey As String, ByVal APIPassword As String, ContactID As String, File As GeneratedFile) As Integer Implements INurseryGenieData.AttachContactPhoto
        If Not Authenticate(APIKey, APIPassword) Then Return -1
        Return 0
    End Function

    Public Function AttachStaffPhoto(ByVal APIKey As String, ByVal APIPassword As String, StaffID As String, File As GeneratedFile) As Integer Implements INurseryGenieData.AttachStaffPhoto
        If Not Authenticate(APIKey, APIPassword) Then Return -1
        Return 0
    End Function

    Public Function GetChildren(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Child) Implements INurseryGenieData.GetChildren

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _DBRecords As List(Of Nursery.Business.Child)

        If ParameterHandler.ReturnBoolean("ALLSITES") Then
            _DBRecords = Nursery.Business.Child.RetrieveLiveChildren
        Else
            _DBRecords = Nursery.Business.Child.RetrieveLiveChildrenBySite(ReturnSiteID(DeviceName))
        End If

        If _DBRecords IsNot Nothing Then

            Dim _Return As New List(Of Model.Child)
            For Each _DB In _DBRecords

                Dim _C As New Model.Child
                With _C

                    .ID = _DB._ID.Value

                    .Forename = _DB._Forename
                    .Surname = _DB._Surname
                    .FullName = _DB._Fullname

                    .FamilyID = _DB._FamilyId.Value
                    .GroupID = _DB._GroupId.Value
                    .GroupName = _DB._GroupName
                    .KeyWorkerID = _DB._KeyworkerId.Value
                    .KeyWorkerName = _DB._KeyworkerName

                    .DOB = _DB._Dob.Value
                    .Gender = _DB._Gender

                    .Milk = _DB._Milk
                    .BabyFood = _DB._BabyFood
                    .OffMenu = _DB._OffMenu

                    .DietRestrict = _DB._DietRestrict
                    .DietNotes = _DB._DietNotes

                    .AllergyRating = _DB._AllergyRating
                    .AllergyNotes = _DB._MedAllergies

                    .Medication = _DB._MedMedication
                    .MedicalNotes = _DB._MedNotes

                    .Doctor = _DB._SurgDoc
                    .DoctorSurgery = _DB._SurgName
                    .DoctorTel = _DB._SurgTel

                    .HVName = _DB._HvName
                    .HVTel = _DB._HvTel
                    .HVMobile = _DB._HvMobile

                    .Monday = _DB._Monday
                    .Tuesday = _DB._Tuesday
                    .Wednesday = _DB._Wednesday
                    .Thursday = _DB._Thursday
                    .Friday = _DB._Friday
                    .Saturday = _DB._Saturday
                    .Sunday = _DB._Sunday

                    .Attributes = GetAttributes(.ID)

                End With

                _Return.Add(_C)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Private Function GetAttributes(ByVal ChildID As Guid) As List(Of ChildAttribute)

        Dim _List As New List(Of ChildAttribute)

        _List.AddRange(GetAllergies(ChildID))
        _List.AddRange(GetConsent(ChildID))

        Return _List

    End Function

    Private Function GetAllergies(ByVal ChildID As Guid) As List(Of ChildAttribute)

        Dim _List As New List(Of ChildAttribute)

        Dim _SQL As String = "select attribute from ChildAttrib where child_id = '" + ChildID.ToString + "' and category = 'Allergies' order by attribute"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _A As New ChildAttribute
                _A.ChildID = ChildID
                _A.AttributeType = "AllergyMatrix"
                _A.Description = _DR.Item("attribute").ToString
                _A.Flag = True

                _List.Add(_A)

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _List

    End Function

    Private Function GetConsent(ByVal ChildID As Guid) As List(Of ChildAttribute)

        Dim _List As New List(Of ChildAttribute)

        Dim _SQL As String = "select consent from ChildConsent where child_id = '" + ChildID.ToString + "' and given = 1 order by consent"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _A As New ChildAttribute
                _A.ChildID = ChildID
                _A.AttributeType = "Consent"
                _A.Description = _DR.Item("consent").ToString
                _A.Flag = True

                _List.Add(_A)

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _List

    End Function

    Public Function GetContacts(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Contact) Implements INurseryGenieData.GetContacts

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _DBRecords As List(Of Nursery.Business.Contact)

        If ParameterHandler.ReturnBoolean("ALLSITES") Then
            _DBRecords = Nursery.Business.Contact.RetreiveAll
        Else
            _DBRecords = Nursery.Business.Contact.RetrieveContactsBySite(ReturnSiteID(DeviceName))
        End If

        If _DBRecords IsNot Nothing Then

            Dim _Return As New List(Of Model.Contact)
            For Each _DB In _DBRecords

                Dim _C As New Model.Contact
                With _C

                    .ID = _DB._ID.Value
                    .FamilyId = _DB._FamilyId.Value

                    .Forename = _DB._Forename
                    .Surname = _DB._Surname
                    .Fullname = _DB._Fullname

                    .Relationship = _DB._Relationship
                    .Password = _DB._Password

                    .PrimaryCont = _DB._PrimaryCont
                    .Parent = _DB._Parent
                    .EmerCont = _DB._EmerCont
                    .Collect = _DB._Collect

                    .TelHome = _DB._TelHome
                    .TelMobile = _DB._TelMobile
                    .JobTel = _DB._JobTel

                End With

                _Return.Add(_C)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Public Function GetRooms(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.SiteRoom) Implements INurseryGenieData.GetRooms

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _SiteID As Guid? = ReturnSiteID(DeviceName)
        If _SiteID.HasValue Then

            Dim _SQL As String = ""
            _SQL += "select ID, room_name, room_sequence from SiteRooms"
            _SQL += " where site_id = '" + _SiteID.ToString + "'"
            _SQL += " and room_check_children = 1"
            _SQL += " order by room_sequence"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Dim _Return As New List(Of Model.SiteRoom)

                For Each _Row As DataRow In _DT.Rows

                    Dim _G As New Model.SiteRoom
                    With _G
                        .ID = New Guid(_Row.Item("ID").ToString)
                        .Name = _Row.Item("room_name").ToString
                        .Sequence = ValueHandler.ConvertInteger(_Row.Item("room_sequence"))
                    End With

                    _Return.Add(_G)

                Next

                Return _Return

            End If

        End If

        Return Nothing

    End Function

    Public Function GetStaff(ByVal APIKey As String, ByVal APIPassword As String, DeviceName As String) As List(Of Model.Staff) Implements INurseryGenieData.GetStaff

        If Not Authenticate(APIKey, APIPassword, DeviceName) Then Return Nothing

        Dim _DBRecords As List(Of Nursery.Business.Staff)

        If ParameterHandler.ReturnBoolean("ALLSITES") Then
            _DBRecords = Nursery.Business.Staff.RetrieveLiveStaff
        Else
            _DBRecords = Nursery.Business.Staff.RetrieveLiveStaffBySite(ReturnSiteID(DeviceName))
        End If

        If _DBRecords IsNot Nothing Then

            Dim _Return As New List(Of Model.Staff)
            For Each _DB In _DBRecords

                Dim _S As New Model.Staff
                With _S
                    .ID = _DB._ID.Value
                    .Name = _DB._Fullname
                    .Forename = _DB._Forename
                    .Surname = _DB._Surname
                    .SurnameForename = _DB._Surname + ", " + _DB._Forename
                End With

                _Return.Add(_S)

            Next

            Return _Return

        Else
            Return Nothing
        End If

    End Function

    Private Function ReturnSiteID(ByVal DeviceName As String) As Guid

        Dim _Return As Guid? = Nothing

        'check if there is a specific site for this device
        _Return = ReturnTabletSite(DeviceName)

        If Not _Return.HasValue Then

            'otherwise, we just assume the default site
            Dim _DefaultSite As String = ParameterHandler.ReturnString("DEFSITE")
            If _DefaultSite <> "" Then
                Dim _S As Nursery.Business.Site = Nursery.Business.Site.RetrieveByName(_DefaultSite)
                If _S IsNot Nothing AndAlso _S._ID.HasValue Then
                    _Return = _S._ID.Value
                End If
            End If

        End If

        If _Return.HasValue Then
            Return _Return.Value
        Else
            Return Nothing
        End If

    End Function

    Private Function ReturnTabletSite(ByVal DeviceName As String) As Guid?

        Dim _SQL As String = "select param_value from TabletParams where device = '" + DeviceName + "' and param_id = 'SITE'"

        Dim _o As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
        If _o IsNot Nothing Then

            Dim _SiteName As String = _o.ToString

            Dim _S As Nursery.Business.Site = Nursery.Business.Site.RetrieveByName(_SiteName)
            If _S IsNot Nothing Then
                Return _S._ID
            End If

        Else
            Return Nothing
        End If

    End Function

    Private Sub UpdateRegisterSummary(ByVal DayID As Guid, ByVal PersonID As Guid, ByVal PersonName As String, ByVal PersonType As String)

        'fetch the earliest checkin
        Dim _In As Nursery.Business.Register = Nursery.Business.Register.RetreiveEarliestRecord(DayID, PersonID, Nursery.Business.Register.EnumInOut.In)

        'fetch the earliest checkout
        Dim _Out As Nursery.Business.Register = Nursery.Business.Register.RetreiveEarliestRecord(DayID, PersonID, Nursery.Business.Register.EnumInOut.Out)

        Dim _Summary As RegisterSummary = RegisterSummary.RetreiveByPerson(DayID, PersonID)

        If _Summary Is Nothing Then
            _Summary = New RegisterSummary()
            _Summary._DayId = DayID
            _Summary._Date = Today
            _Summary._PersonId = PersonID
            _Summary._PersonName = PersonName
            _Summary._PersonType = PersonType
        End If

        With _Summary

            If _In IsNot Nothing Then

                ._InOut = "I"
                ._FirstStamp = _In._StampIn
                ._Location = _In._Location

                If _Out IsNot Nothing Then
                    If _Out._StampOut < ._LastStamp OrElse IsNothing(._LastStamp) Then
                        ._InOut = "O"
                        ._LastStamp = _Out._StampOut
                        ._Duration = CInt(DateDiff(DateInterval.Minute, CDate(._FirstStamp), CDate(._LastStamp)))
                        ._Location = _Out._Location
                    End If
                End If

                .Store()

            End If

        End With

    End Sub

    Private Function ReturnDayID(ByVal SiteID As Guid) As Guid

        Dim _D As Nursery.Business.Day = Nursery.Business.Day.RetreiveByDateAndSite(Today, SiteID)
        If _D IsNot Nothing Then
            Return _D._ID.Value
        Else
            Return Nothing
        End If

    End Function

End Class
