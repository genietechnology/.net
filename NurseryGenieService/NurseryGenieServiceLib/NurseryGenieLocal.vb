﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

' NOTE: You can use the "Rename" command on the context menu to change the class name "Service1" in both code and config file together.
Public Class NurseryGenieLocal

    Implements INurseryGenieLocal

    Public Function GetReportPDF(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ChildID As String) As GeneratedFile Implements INurseryGenieLocal.GetReportPDF

        EventLogging.LogEvent("GetReportPDF ReportFolder: " + Session.ReportFolder, EventLogEntryType.Information)
        EventLogging.LogEvent("GetReportPDF TempFolder: " + Session.TempFolder, EventLogEntryType.Information)

        Dim _ID As Guid = Guid.NewGuid
        Dim _SiteID As Guid = ReturnSiteID(DeviceName)
        Dim _Result As Integer = 0

        Try
            Dim _DailyReport As New Nursery.Reports.DailyReport(Nursery.Reports.DailyReport.EnumRunMode.Task, Today, _SiteID, "", ChildID, False, _ID)
            _Result = _DailyReport.Run

        Catch ex As Exception
            EventLogging.LogEvent("GetReportPDF Unhandled: " + ex.Message + vbCrLf + ex.StackTrace, EventLogEntryType.Error)
        End Try

        If _Result = 0 Then

            EventLogging.LogEvent("GetReportPDF Result: " + _Result.ToString, EventLogEntryType.Information)

            Dim _Path As String = Session.TempFolder + _ID.ToString + ".pdf"
            If IO.File.Exists(_Path) Then

                Dim _GF As New GeneratedFile

                Try
                    With _GF
                        .FileName = _ID.ToString + ".pdf"
                        .FileData = IO.File.ReadAllBytes(_Path)
                    End With

                Catch ex As Exception
                    EventLogging.LogEvent("GetReportPDF: " + ex.Message, EventLogEntryType.Warning)
                End Try

                Return _GF

            Else
                EventLogging.LogEvent("GetReportPDF " + _Path + " does not exist." + _Result.ToString, EventLogEntryType.Warning)
                Return Nothing
            End If

        Else
            EventLogging.LogEvent("GetReportPDF Result: " + _Result.ToString, EventLogEntryType.Warning)
            Return Nothing
        End If

    End Function

    Public Function EmailReports(ByVal APIKey As String, ByVal APIPassword As String, ByVal DeviceName As String, ChildID As String) As Integer Implements INurseryGenieLocal.EmailReports

        If ParameterHandler.ReturnBoolean("DISABLEREPORTEMAIL") Then
            EventLogging.LogEvent("EmailReports (" + ChildID + ") Report not send due to DISABLEREPORTEMAIL = True", EventLogEntryType.Information)
            Return 0
        End If

        Dim _Errors As String = ""
        Dim _Result As Integer = 0

        Dim _Child As Nursery.Business.Child = Nursery.Business.Child.RetreiveByID(New Guid(ChildID))
        If _Child IsNot Nothing Then

            Dim _Day As Nursery.Business.Day = Nursery.Business.Day.RetreiveByDateAndSite(Today, _Child._SiteId.Value)
            If _Day IsNot Nothing Then

                Try
                    CreateEmailTask(ChildID, _Child._Fullname)

                Catch ex As Exception
                    _Result = -99
                    _Errors = ex.Message
                End Try

            Else
                'no day record
                _Result = -97
                _Errors = "Unable to find Day Record"
            End If

        Else
            'cannot find child record
            _Result = -98
            _Errors = "Unable to find Child Record"
        End If

        If _Result = 0 Then
            EventLogging.LogEvent("EmailReports (" + ChildID + ") Result: " + _Result.ToString, EventLogEntryType.Information)
        Else
            If _Result < 0 Then
                EventLogging.LogEvent("EmailReports (" + ChildID + ") Result: " + _Result.ToString + vbCrLf + _Errors, EventLogEntryType.Error)
            Else
                EventLogging.LogEvent("EmailReports (" + ChildID + ") Result: " + _Result.ToString + vbCrLf + _Errors, EventLogEntryType.Information)
            End If
        End If

        Return _Result

    End Function

    Private Sub CreateEmailTask(ByVal ChildID As String, ByVal ChildName As String)

        Dim _TQ As New Business.AppTaskQueue
        _TQ._Status = 2
        _TQ._Name = "Email Daily Report"
        _TQ._Description = "Email Daily Report for ChildID: " + ChildID + " - " + ChildName
        _TQ._TaskType = "Business Object"
        _TQ._ObjectName = "Nursery.Reports.DailyReport"
        _TQ._Command = "EmailReportForToday"
        _TQ._Args = ChildID
        _TQ._EnteredStamp = Now
        _TQ.Store()

    End Sub

    Private Function ReturnSiteID(ByVal DeviceName As String) As Guid

        Dim _Return As Guid? = Nothing

        'check if there is a specific site for this device
        _Return = ReturnTabletSite(DeviceName)

        If Not _Return.HasValue Then

            'otherwise, we just assume the default site
            Dim _DefaultSite As String = ParameterHandler.ReturnString("DEFSITE")
            If _DefaultSite <> "" Then
                Dim _S As Nursery.Business.Site = Nursery.Business.Site.RetrieveByName(_DefaultSite)
                If _S IsNot Nothing AndAlso _S._ID.HasValue Then
                    _Return = _S._ID.Value
                End If
            End If

        End If

        If _Return.HasValue Then
            Return _Return.Value
        Else
            Return Nothing
        End If

    End Function

    Private Function ReturnTabletSite(ByVal DeviceName As String) As Guid?

        Dim _SQL As String = "select param_value from TabletParams where device = '" + DeviceName + "' and param_id = 'SITE'"

        Dim _o As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
        If _o IsNot Nothing Then

            Dim _SiteName As String = _o.ToString

            Dim _S As Nursery.Business.Site = Nursery.Business.Site.RetrieveByName(_SiteName)
            If _S IsNot Nothing Then
                Return _S._ID
            End If

        Else
            Return Nothing
        End If

    End Function

End Class




