﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NurseryGenieServiceLib")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Care Software Ltd")> 
<Assembly: AssemblyProduct("NurseryGenieServiceLib")> 
<Assembly: AssemblyCopyright("Copyright ©  2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("bd48312e-bb94-410e-a2d9-e7eab13c80e1")>
<Assembly: AssemblyVersion("1.19.1.3")>
<Assembly: AssemblyFileVersion("1.19.1.3")>



' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 