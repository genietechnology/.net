﻿
Namespace Model

    <DataContract()>
    Public Class ChildAttribute

        <DataMember()>
        Public Property ChildID() As Guid

        <DataMember()>
        Public Property AttributeType() As String

        <DataMember()>
        Public Property Description() As String

        <DataMember()>
        Public Property Flag() As Boolean

    End Class

End Namespace
