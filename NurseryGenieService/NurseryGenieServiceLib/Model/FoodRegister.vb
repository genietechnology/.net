﻿Namespace Model

    <DataContract()>
    Public Class FoodRegister

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property DayID As Guid

        <DataMember()>
        Public Property ChildID As Guid

        <DataMember()>
        Public Property ChildName As String

        <DataMember()>
        Public Property MealID As Guid?

        <DataMember()>
        Public Property MealType As String

        <DataMember()>
        Public Property MealName As String

        <DataMember()>
        Public Property FoodID As Guid?

        <DataMember()>
        Public Property FoodName As String

        <DataMember()>
        Public Property FoodStatus As String

    End Class

End Namespace

