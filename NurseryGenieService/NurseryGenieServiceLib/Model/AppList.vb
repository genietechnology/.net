﻿'*****************************************************
'Generated 15/05/2016 20:48:56 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class AppList

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property ListId As Guid?

        <DataMember()>
        Public Property ListName As String

        <DataMember()>
        Public Property Name As String

        <DataMember()>
        Public Property Seq As Integer

        <DataMember()>
        Public Property Ref1 As String

        <DataMember()>
        Public Property Ref2 As String

        <DataMember()>
        Public Property Ref3 As String

        <DataMember()>
        Public Property Hide As Boolean


    End Class
End Namespace
