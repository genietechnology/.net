﻿'*****************************************************
'Generated 14/05/2016 14:41:10 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class TabletParam

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property Device As String

        <DataMember()>
        Public Property Parent As String

        <DataMember()>
        Public Property ParamId As String

        <DataMember()>
        Public Property ParamType As String

        <DataMember()>
        Public Property ParamDesc As String

        <DataMember()>
        Public Property ParamValue As String


    End Class
End Namespace
