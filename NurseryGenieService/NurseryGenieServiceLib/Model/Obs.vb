﻿'*****************************************************
'Generated 14/05/2016 14:43:46 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Obs

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property ObsDate As DateTime?

        <DataMember()>
        Public Property Title As String

        <DataMember()>
        Public Property Comments As String

        <DataMember()>
        Public Property StaffId As Guid?

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
