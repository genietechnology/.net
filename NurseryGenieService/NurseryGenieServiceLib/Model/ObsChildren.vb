﻿'*****************************************************
'Generated 14/05/2016 14:44:24 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class ObsChildren

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property ObsId As Guid?

        <DataMember()>
        Public Property ChildId As Guid?


    End Class
End Namespace
