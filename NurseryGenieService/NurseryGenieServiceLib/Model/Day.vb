﻿'*****************************************************
'Generated 14/05/2016 18:29:31 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Day

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayDate As Date?

        <DataMember()>
        Public Property BreakfastId As Guid?

        <DataMember()>
        Public Property BreakfastName As String

        <DataMember()>
        Public Property SnackId As Guid?

        <DataMember()>
        Public Property SnackName As String

        <DataMember()>
        Public Property LunchId As Guid?

        <DataMember()>
        Public Property LunchName As String

        <DataMember()>
        Public Property LunchDesId As Guid?

        <DataMember()>
        Public Property LunchDesName As String

        <DataMember()>
        Public Property PMSnackId As Guid?

        <DataMember()>
        Public Property PMSnackName As String

        <DataMember()>
        Public Property TeaId As Guid?

        <DataMember()>
        Public Property TeaName As String

        <DataMember()>
        Public Property TeaDesId As Guid?

        <DataMember()>
        Public Property TeaDesName As String

        <DataMember()>
        Public Property Message As String


    End Class
End Namespace
