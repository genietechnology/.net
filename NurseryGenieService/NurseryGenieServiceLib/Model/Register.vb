﻿Namespace Model

    <DataContract()>
    Public Class Register

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property RegisterDate As Date

        <DataMember()>
        Public Property RegisterType As String

        <DataMember()>
        Public Property InOut As String

        <DataMember()>
        Public Property PersonID As Guid

        <DataMember()>
        Public Property PersonName As String

        <DataMember()>
        Public Property Description As String

        <DataMember()>
        Public Property StampIn As Date?

        <DataMember()>
        Public Property StampOut As Date?

        <DataMember()>
        Public Property Location As String

        <DataMember()>
        Public Property Ref1 As String

        <DataMember()>
        Public Property Ref2 As String

        <DataMember()>
        Public Property Ref3 As String

    End Class

End Namespace

