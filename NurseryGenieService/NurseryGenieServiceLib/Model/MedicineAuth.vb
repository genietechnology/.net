﻿'*****************************************************
'Generated 14/05/2016 14:40:24 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class MedicineAuth

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property ChildId As Guid?

        <DataMember()>
        Public Property ChildName As String

        <DataMember()>
        Public Property ContactId As Guid?

        <DataMember()>
        Public Property ContactName As String

        <DataMember()>
        Public Property ContactSig As String

        <DataMember()>
        Public Property StaffId As Guid?

        <DataMember()>
        Public Property StaffName As String

        <DataMember()>
        Public Property StaffSig As String

        <DataMember()>
        Public Property Illness As String

        <DataMember()>
        Public Property Medicine As String

        <DataMember()>
        Public Property Dosage As String

        <DataMember()>
        Public Property LastGiven As DateTime?

        <DataMember()>
        Public Property Frequency As String

        <DataMember()>
        Public Property DosagesDue As String

        <DataMember()>
        Public Property CollectId As Guid?

        <DataMember()>
        Public Property CollectName As String

        <DataMember()>
        Public Property CollectSig As String

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
