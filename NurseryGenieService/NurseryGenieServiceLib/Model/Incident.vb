﻿'*****************************************************
'Generated 14/05/2016 14:38:44 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Incident

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property ChildId As Guid?

        <DataMember()>
        Public Property ChildName As String

        <DataMember()>
        Public Property StaffId As Guid?

        <DataMember()>
        Public Property StaffName As String

        <DataMember()>
        Public Property IncidentType As String

        <DataMember()>
        Public Property Location As String

        <DataMember()>
        Public Property Details As String

        <DataMember()>
        Public Property Injury As String

        <DataMember()>
        Public Property Treatment As String

        <DataMember()>
        Public Property Action As String

        <DataMember()>
        Public Property SigStaff As Guid?

        <DataMember()>
        Public Property SigWitness As Guid?

        <DataMember()>
        Public Property SigContact As Guid?

        <DataMember()>
        Public Property Photo As Byte()

        <DataMember()>
        Public Property BodyMap As Byte()

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
