﻿Namespace Model

    <DataContract()>
    Public Class Food

        <DataMember()>
        Public Property MealID As Guid

        <DataMember()>
        Public Property MealName As String

        <DataMember()>
        Public Property Breakfast As Boolean

        <DataMember()>
        Public Property Snack As Boolean

        <DataMember()>
        Public Property Lunch As Boolean

        <DataMember()>
        Public Property Dessert As Boolean

        <DataMember()>
        Public Property Tea As Boolean

        <DataMember()>
        Public Property FoodID As Guid

        <DataMember()>
        Public Property FoodName As String

        <DataMember()>
        Public Property FoodGroup As String

        <DataMember()>
        Public Property BabyFood As Boolean

    End Class

End Namespace
