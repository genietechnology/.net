﻿'*****************************************************
'Generated 14/05/2016 14:43:31 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Media

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property KeyType As String

        <DataMember()>
        Public Property KeyId As Guid?

        <DataMember()>
        Public Property Data As Byte()

        <DataMember()>
        Public Property DeviceName As String

        <DataMember()>
        Public Property FilePath As String

        <DataMember()>
        Public Property FileSize As Integer

        <DataMember()>
        Public Property FileExt As String

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
