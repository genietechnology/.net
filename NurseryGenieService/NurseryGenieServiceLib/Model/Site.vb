﻿Namespace Model

    <DataContract()>
    Public Class Site

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property Name() As String

    End Class

End Namespace
