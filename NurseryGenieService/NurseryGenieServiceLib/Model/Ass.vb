﻿'*****************************************************
'Generated 14/05/2016 14:36:15 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Ass

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property ChildId As Guid?

        <DataMember()>
        Public Property ObsId As Guid?

        <DataMember()>
        Public Property AssDate As DateTime?

        <DataMember()>
        Public Property AssStatus As String

        <DataMember()>
        Public Property LsWellbeing As Byte

        <DataMember()>
        Public Property LsWellbeingDesc As String

        <DataMember()>
        Public Property LsInvolvement As Byte

        <DataMember()>
        Public Property LsInvolvementDesc As String

        <DataMember()>
        Public Property Comments As String

        <DataMember()>
        Public Property NextSteps As String

        <DataMember()>
        Public Property StaffId As Guid?

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
