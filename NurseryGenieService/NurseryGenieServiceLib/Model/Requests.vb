﻿Namespace Model

    <DataContract()>
    Public Class Requests

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property Name As String

        <DataMember()>
        Public Property ReportText As String

    End Class

End Namespace

