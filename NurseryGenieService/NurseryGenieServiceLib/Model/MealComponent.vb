﻿'*****************************************************
'Generated 14/05/2016 14:45:25 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class MealComponent

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property MealId As Guid?

        <DataMember()>
        Public Property FoodId As Guid?

        <DataMember()>
        Public Property FoodName As String

        <DataMember()>
        Public Property FoodQty As Long


    End Class
End Namespace
