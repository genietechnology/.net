﻿'*****************************************************
'Generated 14/05/2016 14:41:41 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Signature

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property PersonId As Guid?

        <DataMember()>
        Public Property PersonType As String

        <DataMember()>
        Public Property PersonName As String

        <DataMember()>
        Public Property Signature As Byte()

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
