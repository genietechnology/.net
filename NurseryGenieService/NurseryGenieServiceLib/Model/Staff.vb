﻿Namespace Model

    <DataContract()>
    Public Class Staff

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property Name() As String

        <DataMember()>
        Public Property Forename() As String

        <DataMember()>
        Public Property Surname() As String

        <DataMember()>
        Public Property SurnameForename() As String

    End Class

End Namespace
