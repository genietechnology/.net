﻿Namespace Model

    <DataContract()>
    Public Class Child

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property FullName() As String

        <DataMember()>
        Public Property Forename() As String

        <DataMember()>
        Public Property Surname() As String

        <DataMember()>
        Public Property DOB() As Date

        <DataMember()>
        Public Property Gender As String

        <DataMember()>
        Public Property FamilyID() As Guid

        <DataMember()>
        Public Property GroupID() As Guid

        <DataMember()>
        Public Property GroupName() As String

        <DataMember()>
        Public Property KeyWorkerID() As Guid

        <DataMember()>
        Public Property KeyWorkerName() As String

        <DataMember()>
        Public Property Nappies() As Boolean

        <DataMember()>
        Public Property OffMenu() As Boolean

        <DataMember()>
        Public Property Milk() As Boolean

        <DataMember()>
        Public Property BabyFood() As Boolean

        <DataMember()>
        Public Property DietRestrict() As String

        <DataMember()>
        Public Property DietNotes() As String

        <DataMember()>
        Public Property AllergyRating() As String

        <DataMember()>
        Public Property AllergyNotes() As String

        <DataMember()>
        Public Property Medication() As String

        <DataMember()>
        Public Property MedicalNotes() As String

        <DataMember()>
        Public Property Doctor() As String

        <DataMember()>
        Public Property DoctorSurgery() As String

        <DataMember()>
        Public Property DoctorTel() As String

        <DataMember()>
        Public Property HVName() As String

        <DataMember()>
        Public Property HVTel() As String

        <DataMember()>
        Public Property HVMobile() As String

        <DataMember()>
        Public Property Monday() As Boolean

        <DataMember()>
        Public Property Tuesday() As Boolean

        <DataMember()>
        Public Property Wednesday() As Boolean

        <DataMember()>
        Public Property Thursday() As Boolean

        <DataMember()>
        Public Property Friday() As Boolean

        <DataMember()>
        Public Property Saturday() As Boolean

        <DataMember()>
        Public Property Sunday() As Boolean

        <DataMember()>
        Public Property Attributes() As List(Of ChildAttribute)

    End Class

End Namespace
