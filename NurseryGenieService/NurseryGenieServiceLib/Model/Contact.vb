﻿'*****************************************************
'Generated 14/05/2016 14:39:12 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Contact

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property FamilyId As Guid?

        <DataMember()>
        Public Property Surname As String

        <DataMember()>
        Public Property Forename As String

        <DataMember()>
        Public Property Fullname As String

        <DataMember()>
        Public Property Relationship As String

        <DataMember()>
        Public Property PrimaryCont As Boolean

        <DataMember()>
        Public Property EmerCont As Boolean

        <DataMember()>
        Public Property Password As String

        <DataMember()>
        Public Property TelHome As String

        <DataMember()>
        Public Property TelMobile As String

        <DataMember()>
        Public Property JobTel As String

        <DataMember()>
        Public Property Parent As Boolean

        <DataMember()>
        Public Property Collect As Boolean

    End Class
End Namespace
