﻿Namespace Model

    <DataContract()>
    Public Class Activity

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property DayID As Guid

        <DataMember()>
        Public Property KeyID As Guid

        <DataMember()>
        Public Property KeyName As String

        <DataMember()>
        Public Property KeyType As String

        <DataMember()>
        Public Property Description As String

        <DataMember()>
        Public Property Value1 As String

        <DataMember()>
        Public Property Value2 As String

        <DataMember()>
        Public Property Value3 As String

        <DataMember()>
        Public Property Stamp As Date

        <DataMember()>
        Public Property StaffID As Guid?

        <DataMember()>
        Public Property StaffName As String

        <DataMember()>
        Public Property Notes As String

    End Class

End Namespace

