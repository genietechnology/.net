﻿Namespace Model

    <DataContract()>
    Public Class SiteRoom

        <DataMember()>
        Public Property ID As Guid

        <DataMember()>
        Public Property Name() As String

        <DataMember()>
        Public Property Sequence() As Integer

    End Class

End Namespace
