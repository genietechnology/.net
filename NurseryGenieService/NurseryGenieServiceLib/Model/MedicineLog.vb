﻿'*****************************************************
'Generated 14/05/2016 14:44:46 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class MedicineLog

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property DayId As Guid?

        <DataMember()>
        Public Property AuthId As Guid?

        <DataMember()>
        Public Property Due As DateTime?

        <DataMember()>
        Public Property ChildId As Guid?

        <DataMember()>
        Public Property ChildName As String

        <DataMember()>
        Public Property Illness As String

        <DataMember()>
        Public Property Medicine As String

        <DataMember()>
        Public Property Dosage As String

        <DataMember()>
        Public Property StaffId As Guid?

        <DataMember()>
        Public Property StaffName As String

        <DataMember()>
        Public Property StaffSig As String

        <DataMember()>
        Public Property WitnessId As Guid?

        <DataMember()>
        Public Property WitnessName As String

        <DataMember()>
        Public Property WitnessSig As String

        <DataMember()>
        Public Property Actual As DateTime?

        <DataMember()>
        Public Property Button As String

        <DataMember()>
        Public Property Stamp As DateTime?


    End Class
End Namespace
