﻿'*****************************************************
'Generated 26/06/2016 16:39:28 using Version 1.16.6.0
'*****************************************************

Namespace Model

    <DataContract()>
    Public Class Booking

        <DataMember()>
        Public Property ID As Guid?

        <DataMember()>
        Public Property BookingDate As Date?

        <DataMember()>
        Public Property BookingFrom As TimeSpan?

        <DataMember()>
        Public Property BookingTo As TimeSpan?

        <DataMember()>
        Public Property ChildId As Guid?

        <DataMember()>
        Public Property ChildName As String

        <DataMember()>
        Public Property ChildDob As Date?

        <DataMember()>
        Public Property ChildAge As Byte

        <DataMember()>
        Public Property ChildGroup As String

        <DataMember()>
        Public Property TariffId As Guid?

        <DataMember()>
        Public Property TariffName As String

        <DataMember()>
        Public Property TariffRate As Decimal

        <DataMember()>
        Public Property RoomId As Guid?

        <DataMember()>
        Public Property RoomName As String

        <DataMember()>
        Public Property Am As Boolean

        <DataMember()>
        Public Property Pm As Boolean

        <DataMember()>
        Public Property BookingCount As Byte

        <DataMember()>
        Public Property Hours As Decimal

        <DataMember()>
        Public Property FundedHours As Decimal

        <DataMember()>
        Public Property FullTime As Boolean

        <DataMember()>
        Public Property Points As Byte

    End Class
End Namespace
