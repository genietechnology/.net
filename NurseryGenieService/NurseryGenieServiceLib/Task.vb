﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class Task

    Private m_SQLServer As String = ""
    Private m_SQLDatabase As String = ""
    Private m_SQLUser As String = ""
    Private m_SQLPassword As String = ""
    Private m_ConnectionString As String = ""
    Private m_ConnectDone As Boolean = False

    Private m_ReportFolder As String = ""
    Private m_TempFolder As String = ""
    Private m_TaskID As Guid? = Nothing

    Public Sub New(ByVal SQLServer As String, ByVal SQLDatabase As String,
                   ByVal SQLUser As String, ByVal SQLPassword As String,
                   ByVal ReportFolder As String, ByVal TempFolder As String, ByVal TaskID As Guid)

        m_SQLServer = SQLServer
        m_SQLDatabase = SQLDatabase
        m_SQLUser = SQLUser
        m_SQLPassword = SQLPassword
        m_ReportFolder = ReportFolder
        m_TempFolder = TempFolder
        m_TaskID = TaskID

        SetupSession()

    End Sub

    Private Function SetupSession() As Boolean

        Dim _Return As Boolean = False

        Session.ServiceMode()

        Session.CurrentUser = New Care.Global.User()
        Session.CurrentUser.ID = Guid.NewGuid
        Session.CurrentUser.FullName = "Nursery Genie Service"

        If m_ConnectionString = "" Then

            Dim _b As New ConnectionBuilder
            With _b
                .Server = m_SQLServer
                .DB = m_SQLDatabase
                .User = m_SQLUser
                .Password = m_SQLPassword
                .IntegratedSecurity = False
                .TestConnection()
            End With

            If _b.ConnectionOK Then
                m_ConnectionString = _b.ConnectionString
                Session.ConnectionString = m_ConnectionString
                EventLogging.LogEvent("Connection to SQL Successful", EventLogEntryType.Information)
                _Return = Setup.Execute
            End If

        Else
            _Return = Setup.Execute
        End If

        If Session.TempFolder = "" Then Session.TempFolder = m_TempFolder
        If Session.ReportFolder = "" Then Session.ReportFolder = m_ReportFolder

        If _Return Then
            EventLogging.LogEvent("Setup Session OK", EventLogEntryType.Information)
        Else
            EventLogging.LogEvent("Setup Session Failed", EventLogEntryType.Error)
        End If

        Return _Return

    End Function

    Public Function Execute() As TaskResult

        Dim _r As New TaskResult
        Dim _q As Business.AppTaskQueue = Business.AppTaskQueue.RetreiveByID(m_TaskID.Value)
        If _q IsNot Nothing Then

            EventLogging.LogEvent("Executing Task - " + _q._Name + "...", EventLogEntryType.Information)

            'mark task as started
            _q._Status = 1
            _q._StartedStamp = Now
            _q.Store()

            Select Case _q._TaskType

                Case "SQL Command"
                    _r = ExecuteSQL(_q._ID.Value, _q._Command)

                Case "OS Command"

                Case "Business Object"
                    _r = ExecuteBusinessObject(_q._ID.Value, _q._ObjectName, _q._Args, _q._Command)

            End Select

            'mark task as completed
            _q._Status = 0
            _q._FinishedStamp = Now
            _q._Result = _r.Result
            _q._ErrorText = _r.ErrorText
            _q.Store()

            'if this originated from a task template, make sure that is updated too
            If _q._TaskId.HasValue Then
                LogTask(_q._TaskId.Value, _r)
            End If

            'update application event log
            If _r.Result = 0 Then
                EventLogging.LogEvent(_q._Name + " - Executed Successfully.", EventLogEntryType.Information)
            Else
                If _r.Result > 1 Then
                    EventLogging.LogEvent(_q._Name + " - Executed with Warnings: " + _r.ErrorText, EventLogEntryType.Warning)
                Else
                    EventLogging.LogEvent(_q._Name + " - Failed: " + _r.ErrorText, EventLogEntryType.Error)
                End If
            End If

        End If

        Return _r

    End Function

    Private Function ExecuteBusinessObject(ByVal TaskID As Guid, ByVal ObjectName As String, ByVal Args As String, ByVal Command As String) As TaskResult

        Dim _r As New TaskResult
        Dim _ex As Exception = Nothing
        Dim _obj As Object = FormHandler.ReturnObject(ObjectName, _ex)
        If _obj IsNot Nothing Then

            Try

                If Args = "" Then
                    Dim _CallReturn As Object = CallByName(_obj, Command, CallType.Method)
                    _r = New TaskResult(0)

                Else

                    'split the args into a string array
                    Dim _StringArray As String() = Args.Split(CType(",", Char()))

                    Dim _Args As New List(Of Object)
                    For Each _s In _StringArray
                        _Args.Add(_s)
                    Next

                    Dim _CallReturn As Object = CallByName(_obj, Command, CallType.Method, _Args.ToArray)
                    _r = New TaskResult(0)

                End If

            Catch ex As Exception
                _r = New TaskResult(-2, "ExecuteBusinessObject: " + ex.Message, _ex)
            End Try

        Else
            _r = New TaskResult(-1, "ExecuteBusinessObject.ReturnObject: " + ObjectName + " failed.", _ex)
        End If

        Return _r

    End Function

    Private Function ExecuteSQL(ByVal TaskID As Guid, ByVal SQL As String) As TaskResult

        Dim _r As New TaskResult
        Dim _Result As Integer = 0
        Dim _Error As String = ""

        'REPLACE ANY PLACEHOLDERS
        SQL = SQL.Replace("{BACKUPPATH}", ParameterHandler.ReturnString("BACKUPPATH"))
        SQL = SQL.Replace("{FILETIMESTAMP}", Format(Now, "ddMMyyyy_HHmmss").ToString)

        Dim _cmd As SqlClient.SqlCommand = DAL.ReturnTextCommand(Session.ConnectionString, SQL)

        Try
            _cmd.ExecuteNonQuery()

        Catch ex As Exception
            _Result = -1
            _Error = ex.Message
        End Try

        DAL.DisposeofCommand(_cmd)

        Return _r

    End Function

    Private Sub LogTask(ByVal TaskID As Guid, ByVal TaskResult As TaskResult)

        'store last result against task
        Dim _T As Business.AppTask = Business.AppTask.RetreiveByID(TaskID)
        If _T IsNot Nothing Then

            _T._LastRun = Now
            _T._LastResult = TaskResult.Result
            _T.Store()

            'update task history
            Dim _H As New Business.AppTaskHistory

            _H._TaskId = TaskID
            _H._Result = TaskResult.Result
            _H._Errors = TaskResult.ErrorText
            _H._Stamp = Now

            _H.Store()
            _H = Nothing

            _T = Nothing

        End If

    End Sub

End Class
