﻿Imports Care.Data
Imports Care.Global
Imports Care.Shared

Public Class frmTest

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If Not DoSetup() Then
            CareMessage("Problem with DoSetup")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ExecuteBO("Nursery.BookingsEngine", "BuildBookingsForEveryone", "")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ExecuteBO("Nursery.Business.FinancialUtilities", "BuildInvoiceDays", "")
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ExecuteBO("Nursery.Business.FinancialUtilities", "BuildForecastDataForThisYear", "")
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ExecuteBO("Nursery.Business.FinancialUtilities", "UpdateBalances", "")
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        'fails
        'ExecuteBO("Nursery.Business.FinancialUtilities", "BuildTurnoverData", "")
    End Sub

#Region "Helpers"

    Private Sub ExecuteBO(ByVal ObjectName As String, ByVal Command As String, ByVal Args As String)

        Dim _ex As Exception = Nothing
        Dim _obj As Object = FormHandler.ReturnObject(ObjectName, _ex)
        If _obj IsNot Nothing Then
            Try
                Dim _CallReturn As Object = CallByName(_obj, Command, CallType.Method)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "CallByName")
            End Try
        Else
            If _ex Is Nothing Then
                MessageBox.Show("FormHandler.ReturnObject returned Nothing", "FormHandler.ReturnObject")
            Else
                MessageBox.Show(_ex.Message, "FormHandler.ReturnObject")
            End If
        End If

    End Sub

    Private Function DoSetup() As Boolean

        Session.ServiceMode()

        Session.CurrentUser = New Care.Global.User()
        Session.CurrentUser.ID = Guid.NewGuid
        Session.CurrentUser.FullName = "Nursery Genie Service"

        Dim _b As New ConnectionBuilder
        With _b
            .Server = "LOCALHOST"
            .DB = "dev"
            .User = "james"
            .Password = "$chm31chel"
            .IntegratedSecurity = False
            .TestConnection()
        End With

        If _b.ConnectionOK Then
            Session.ConnectionString = _b.ConnectionString
            Return Setup.Execute
        Else
            Return False
        End If

    End Function

#End Region

End Class
