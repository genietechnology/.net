﻿'*****************************************************
'Generated 02/07/2017 18:54:07 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Job
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Seq As Integer
        Dim m_Enabled As Boolean
        Dim m_TargetWorkstation As Boolean
        Dim m_TargetDb As Boolean
        Dim m_TargetService As Boolean
        Dim m_TargetTouchscreen As Boolean
        Dim m_TargetTablet As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Seq = 0
                ._Enabled = False
                ._TargetWorkstation = False
                ._TargetDb = False
                ._TargetService = False
                ._TargetTouchscreen = False
                ._TargetTablet = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Seq = 0
                ._Enabled = False
                ._TargetWorkstation = False
                ._TargetDb = False
                ._TargetService = False
                ._TargetTouchscreen = False
                ._TargetTablet = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")>
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Seq")>
        Public Property _Seq() As Integer
            Get
                Return m_Seq
            End Get
            Set(ByVal value As Integer)
                m_Seq = value
            End Set
        End Property

        <StoredProcParameter("@Enabled")>
        Public Property _Enabled() As Boolean
            Get
                Return m_Enabled
            End Get
            Set(ByVal value As Boolean)
                m_Enabled = value
            End Set
        End Property

        <StoredProcParameter("@TargetWorkstation")>
        Public Property _TargetWorkstation() As Boolean
            Get
                Return m_TargetWorkstation
            End Get
            Set(ByVal value As Boolean)
                m_TargetWorkstation = value
            End Set
        End Property

        <StoredProcParameter("@TargetDb")>
        Public Property _TargetDb() As Boolean
            Get
                Return m_TargetDb
            End Get
            Set(ByVal value As Boolean)
                m_TargetDb = value
            End Set
        End Property

        <StoredProcParameter("@TargetService")>
        Public Property _TargetService() As Boolean
            Get
                Return m_TargetService
            End Get
            Set(ByVal value As Boolean)
                m_TargetService = value
            End Set
        End Property

        <StoredProcParameter("@TargetTouchscreen")>
        Public Property _TargetTouchscreen() As Boolean
            Get
                Return m_TargetTouchscreen
            End Get
            Set(ByVal value As Boolean)
                m_TargetTouchscreen = value
            End Set
        End Property

        <StoredProcParameter("@TargetTablet")>
        Public Property _TargetTablet() As Boolean
            Get
                Return m_TargetTablet
            End Get
            Set(ByVal value As Boolean)
                m_TargetTablet = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Job

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Job

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getJobbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Job)

            Dim _JobList As List(Of Job)
            _JobList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobTable"))
            Return _JobList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Job)

            Dim _JobList As List(Of Job)
            _JobList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getJobTable"))
            Return _JobList

        End Function

        Public Shared Sub SaveAll(ByVal JobList As List(Of Job))

            For Each _Job As Job In JobList
                SaveRecord(_Job)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal JobList As List(Of Job))

            For Each _Job As Job In JobList
                SaveRecord(ConnectionString, _Job)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Job As Job) As Guid
            Job.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Job, "upsertJob")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Job As Job) As Guid
            Job.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Job, "upsertJob")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteJobbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJob")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertJob")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Job

            Dim _J As Job = Nothing

            If DR IsNot Nothing Then
                _J = New Job()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._Name = DR("name").ToString.Trim
                    _J._Seq = GetInteger(DR("seq"))
                    _J._Enabled = GetBoolean(DR("enabled"))
                    _J._TargetWorkstation = GetBoolean(DR("target_workstation"))
                    _J._TargetDb = GetBoolean(DR("target_db"))
                    _J._TargetService = GetBoolean(DR("target_service"))
                    _J._TargetTouchscreen = GetBoolean(DR("target_touchscreen"))
                    _J._TargetTablet = GetBoolean(DR("target_tablet"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Job)

            Dim _JobList As New List(Of Job)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobList.Add(PropertiesFromData(_DR))
            Next

            Return _JobList

        End Function


#End Region

    End Class

End Namespace
