﻿'*****************************************************
'Generated 02/07/2017 18:52:25 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CustomerApp
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_CustomerId As Guid?
        Dim m_AppName As String
        Dim m_AppPath As String
        Dim m_AppServer As String
        Dim m_AppCs As String
        Dim m_AppType As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._CustomerId = Nothing
                ._AppName = ""
                ._AppPath = ""
                ._AppServer = ""
                ._AppCs = ""
                ._AppType = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._CustomerId = Nothing
                ._AppName = ""
                ._AppPath = ""
                ._AppServer = ""
                ._AppCs = ""
                ._AppType = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@CustomerId")>
        Public Property _CustomerId() As Guid?
            Get
                Return m_CustomerId
            End Get
            Set(ByVal value As Guid?)
                m_CustomerId = value
            End Set
        End Property

        <StoredProcParameter("@AppName")>
        Public Property _AppName() As String
            Get
                Return m_AppName
            End Get
            Set(ByVal value As String)
                m_AppName = value
            End Set
        End Property

        <StoredProcParameter("@AppPath")>
        Public Property _AppPath() As String
            Get
                Return m_AppPath
            End Get
            Set(ByVal value As String)
                m_AppPath = value
            End Set
        End Property

        <StoredProcParameter("@AppServer")>
        Public Property _AppServer() As String
            Get
                Return m_AppServer
            End Get
            Set(ByVal value As String)
                m_AppServer = value
            End Set
        End Property

        <StoredProcParameter("@AppCs")>
        Public Property _AppCs() As String
            Get
                Return m_AppCs
            End Get
            Set(ByVal value As String)
                m_AppCs = value
            End Set
        End Property

        <StoredProcParameter("@AppType")>
        Public Property _AppType() As String
            Get
                Return m_AppType
            End Get
            Set(ByVal value As String)
                m_AppType = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CustomerApp

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCustomerAppbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CustomerApp

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCustomerAppbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CustomerApp)

            Dim _CustomerAppList As List(Of CustomerApp)
            _CustomerAppList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCustomerAppTable"))
            Return _CustomerAppList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CustomerApp)

            Dim _CustomerAppList As List(Of CustomerApp)
            _CustomerAppList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCustomerAppTable"))
            Return _CustomerAppList

        End Function

        Public Shared Sub SaveAll(ByVal CustomerAppList As List(Of CustomerApp))

            For Each _CustomerApp As CustomerApp In CustomerAppList
                SaveRecord(_CustomerApp)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CustomerAppList As List(Of CustomerApp))

            For Each _CustomerApp As CustomerApp In CustomerAppList
                SaveRecord(ConnectionString, _CustomerApp)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CustomerApp As CustomerApp) As Guid
            CustomerApp.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, CustomerApp, "upsertCustomerApp")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CustomerApp As CustomerApp) As Guid
            CustomerApp.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, CustomerApp, "upsertCustomerApp")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCustomerAppbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCustomerAppbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertCustomerApp")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertCustomerApp")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CustomerApp

            Dim _C As CustomerApp = Nothing

            If DR IsNot Nothing Then
                _C = New CustomerApp()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._CustomerId = GetGUID(DR("customer_id"))
                    _C._AppName = DR("app_name").ToString.Trim
                    _C._AppPath = DR("app_path").ToString.Trim
                    _C._AppServer = DR("app_server").ToString.Trim
                    _C._AppCs = DR("app_cs").ToString.Trim
                    _C._AppType = DR("app_type").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CustomerApp)

            Dim _CustomerAppList As New List(Of CustomerApp)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CustomerAppList.Add(PropertiesFromData(_DR))
            Next

            Return _CustomerAppList

        End Function


#End Region

    End Class

End Namespace
