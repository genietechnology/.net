﻿'*****************************************************
'Generated 02/07/2017 18:54:32 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobStep
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_Name As String
        Dim m_Type As String
        Dim m_Arg1 As String
        Dim m_Arg2 As String
        Dim m_Arg3 As String
        Dim m_Memo As String
        Dim m_Seq As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._Name = ""
                ._Type = ""
                ._Arg1 = ""
                ._Arg2 = ""
                ._Arg3 = ""
                ._Memo = ""
                ._Seq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._Name = ""
                ._Type = ""
                ._Arg1 = ""
                ._Arg2 = ""
                ._Arg3 = ""
                ._Memo = ""
                ._Seq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@Name")>
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Type")>
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Arg1")>
        Public Property _Arg1() As String
            Get
                Return m_Arg1
            End Get
            Set(ByVal value As String)
                m_Arg1 = value
            End Set
        End Property

        <StoredProcParameter("@Arg2")>
        Public Property _Arg2() As String
            Get
                Return m_Arg2
            End Get
            Set(ByVal value As String)
                m_Arg2 = value
            End Set
        End Property

        <StoredProcParameter("@Arg3")>
        Public Property _Arg3() As String
            Get
                Return m_Arg3
            End Get
            Set(ByVal value As String)
                m_Arg3 = value
            End Set
        End Property

        <StoredProcParameter("@Memo")>
        Public Property _Memo() As String
            Get
                Return m_Memo
            End Get
            Set(ByVal value As String)
                m_Memo = value
            End Set
        End Property

        <StoredProcParameter("@Seq")>
        Public Property _Seq() As Integer
            Get
                Return m_Seq
            End Get
            Set(ByVal value As Integer)
                m_Seq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobStep

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobStepbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As JobStep

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getJobStepbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobStep)

            Dim _JobStepList As List(Of JobStep)
            _JobStepList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobStepTable"))
            Return _JobStepList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of JobStep)

            Dim _JobStepList As List(Of JobStep)
            _JobStepList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getJobStepTable"))
            Return _JobStepList

        End Function

        Public Shared Sub SaveAll(ByVal JobStepList As List(Of JobStep))

            For Each _JobStep As JobStep In JobStepList
                SaveRecord(_JobStep)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal JobStepList As List(Of JobStep))

            For Each _JobStep As JobStep In JobStepList
                SaveRecord(ConnectionString, _JobStep)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobStep As JobStep) As Guid
            JobStep.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobStep, "upsertJobStep")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal JobStep As JobStep) As Guid
            JobStep.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, JobStep, "upsertJobStep")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobStepbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteJobStepbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobStep")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertJobStep")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobStep

            Dim _J As JobStep = Nothing

            If DR IsNot Nothing Then
                _J = New JobStep()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._Name = DR("name").ToString.Trim
                    _J._Type = DR("type").ToString.Trim
                    _J._Arg1 = DR("arg_1").ToString.Trim
                    _J._Arg2 = DR("arg_2").ToString.Trim
                    _J._Arg3 = DR("arg_3").ToString.Trim
                    _J._Memo = DR("memo").ToString.Trim
                    _J._Seq = GetInteger(DR("seq"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobStep)

            Dim _JobStepList As New List(Of JobStep)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobStepList.Add(PropertiesFromData(_DR))
            Next

            Return _JobStepList

        End Function


#End Region

    End Class

End Namespace
