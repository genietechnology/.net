﻿'*****************************************************
'Generated 02/07/2017 18:52:56 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Endpoint
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_CustomerId As Guid?
        Dim m_ComputerName As String
        Dim m_LastCheckin As DateTime?
        Dim m_LicenseStatus As Integer
        Dim m_LicenseExpires As Date?
        Dim m_LicenseData As String
        Dim m_AllowWorkstation As Boolean
        Dim m_AllowDb As Boolean
        Dim m_AllowService As Boolean
        Dim m_AllowTouchscreen As Boolean
        Dim m_AllowTablet As Boolean
        Dim m_LastUser As String
        Dim m_LastWorkstation As String
        Dim m_LastService As String
        Dim m_LastDatabase As String
        Dim m_LastStateService As Integer
        Dim m_LastStateLivedrive As Integer
        Dim m_LastBackupScheduled As Boolean
        Dim m_LastBackupPerformed As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._CustomerId = Nothing
                ._ComputerName = ""
                ._LastCheckin = Nothing
                ._LicenseStatus = 0
                ._LicenseExpires = Nothing
                ._LicenseData = ""
                ._AllowWorkstation = False
                ._AllowDb = False
                ._AllowService = False
                ._AllowTouchscreen = False
                ._AllowTablet = False
                ._LastUser = ""
                ._LastWorkstation = ""
                ._LastService = ""
                ._LastDatabase = ""
                ._LastStateService = 0
                ._LastStateLivedrive = 0
                ._LastBackupScheduled = False
                ._LastBackupPerformed = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._CustomerId = Nothing
                ._ComputerName = ""
                ._LastCheckin = Nothing
                ._LicenseStatus = 0
                ._LicenseExpires = Nothing
                ._LicenseData = ""
                ._AllowWorkstation = False
                ._AllowDb = False
                ._AllowService = False
                ._AllowTouchscreen = False
                ._AllowTablet = False
                ._LastUser = ""
                ._LastWorkstation = ""
                ._LastService = ""
                ._LastDatabase = ""
                ._LastStateService = 0
                ._LastStateLivedrive = 0
                ._LastBackupScheduled = False
                ._LastBackupPerformed = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@CustomerId")>
        Public Property _CustomerId() As Guid?
            Get
                Return m_CustomerId
            End Get
            Set(ByVal value As Guid?)
                m_CustomerId = value
            End Set
        End Property

        <StoredProcParameter("@ComputerName")>
        Public Property _ComputerName() As String
            Get
                Return m_ComputerName
            End Get
            Set(ByVal value As String)
                m_ComputerName = value
            End Set
        End Property

        <StoredProcParameter("@LastCheckin")>
        Public Property _LastCheckin() As DateTime?
            Get
                Return m_LastCheckin
            End Get
            Set(ByVal value As DateTime?)
                m_LastCheckin = value
            End Set
        End Property

        <StoredProcParameter("@LicenseStatus")>
        Public Property _LicenseStatus() As Integer
            Get
                Return m_LicenseStatus
            End Get
            Set(ByVal value As Integer)
                m_LicenseStatus = value
            End Set
        End Property

        <StoredProcParameter("@LicenseExpires")>
        Public Property _LicenseExpires() As Date?
            Get
                Return m_LicenseExpires
            End Get
            Set(ByVal value As Date?)
                m_LicenseExpires = value
            End Set
        End Property

        <StoredProcParameter("@LicenseData")>
        Public Property _LicenseData() As String
            Get
                Return m_LicenseData
            End Get
            Set(ByVal value As String)
                m_LicenseData = value
            End Set
        End Property

        <StoredProcParameter("@AllowWorkstation")>
        Public Property _AllowWorkstation() As Boolean
            Get
                Return m_AllowWorkstation
            End Get
            Set(ByVal value As Boolean)
                m_AllowWorkstation = value
            End Set
        End Property

        <StoredProcParameter("@AllowDb")>
        Public Property _AllowDb() As Boolean
            Get
                Return m_AllowDb
            End Get
            Set(ByVal value As Boolean)
                m_AllowDb = value
            End Set
        End Property

        <StoredProcParameter("@AllowService")>
        Public Property _AllowService() As Boolean
            Get
                Return m_AllowService
            End Get
            Set(ByVal value As Boolean)
                m_AllowService = value
            End Set
        End Property

        <StoredProcParameter("@AllowTouchscreen")>
        Public Property _AllowTouchscreen() As Boolean
            Get
                Return m_AllowTouchscreen
            End Get
            Set(ByVal value As Boolean)
                m_AllowTouchscreen = value
            End Set
        End Property

        <StoredProcParameter("@AllowTablet")>
        Public Property _AllowTablet() As Boolean
            Get
                Return m_AllowTablet
            End Get
            Set(ByVal value As Boolean)
                m_AllowTablet = value
            End Set
        End Property

        <StoredProcParameter("@LastUser")>
        Public Property _LastUser() As String
            Get
                Return m_LastUser
            End Get
            Set(ByVal value As String)
                m_LastUser = value
            End Set
        End Property

        <StoredProcParameter("@LastWorkstation")>
        Public Property _LastWorkstation() As String
            Get
                Return m_LastWorkstation
            End Get
            Set(ByVal value As String)
                m_LastWorkstation = value
            End Set
        End Property

        <StoredProcParameter("@LastService")>
        Public Property _LastService() As String
            Get
                Return m_LastService
            End Get
            Set(ByVal value As String)
                m_LastService = value
            End Set
        End Property

        <StoredProcParameter("@LastDatabase")>
        Public Property _LastDatabase() As String
            Get
                Return m_LastDatabase
            End Get
            Set(ByVal value As String)
                m_LastDatabase = value
            End Set
        End Property

        <StoredProcParameter("@LastStateService")>
        Public Property _LastStateService() As Integer
            Get
                Return m_LastStateService
            End Get
            Set(ByVal value As Integer)
                m_LastStateService = value
            End Set
        End Property

        <StoredProcParameter("@LastStateLivedrive")>
        Public Property _LastStateLivedrive() As Integer
            Get
                Return m_LastStateLivedrive
            End Get
            Set(ByVal value As Integer)
                m_LastStateLivedrive = value
            End Set
        End Property

        <StoredProcParameter("@LastBackupScheduled")>
        Public Property _LastBackupScheduled() As Boolean
            Get
                Return m_LastBackupScheduled
            End Get
            Set(ByVal value As Boolean)
                m_LastBackupScheduled = value
            End Set
        End Property

        <StoredProcParameter("@LastBackupPerformed")>
        Public Property _LastBackupPerformed() As DateTime?
            Get
                Return m_LastBackupPerformed
            End Get
            Set(ByVal value As DateTime?)
                m_LastBackupPerformed = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Endpoint

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEndpointbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Endpoint

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getEndpointbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Endpoint)

            Dim _EndpointList As List(Of Endpoint)
            _EndpointList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEndpointTable"))
            Return _EndpointList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Endpoint)

            Dim _EndpointList As List(Of Endpoint)
            _EndpointList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getEndpointTable"))
            Return _EndpointList

        End Function

        Public Shared Sub SaveAll(ByVal EndpointList As List(Of Endpoint))

            For Each _Endpoint As Endpoint In EndpointList
                SaveRecord(_Endpoint)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal EndpointList As List(Of Endpoint))

            For Each _Endpoint As Endpoint In EndpointList
                SaveRecord(ConnectionString, _Endpoint)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Endpoint As Endpoint) As Guid
            Endpoint.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Endpoint, "upsertEndpoint")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Endpoint As Endpoint) As Guid
            Endpoint.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Endpoint, "upsertEndpoint")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEndpointbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteEndpointbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertEndpoint")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertEndpoint")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Endpoint

            Dim _E As Endpoint = Nothing

            If DR IsNot Nothing Then
                _E = New Endpoint()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._CustomerId = GetGUID(DR("customer_id"))
                    _E._ComputerName = DR("computer_name").ToString.Trim
                    _E._LastCheckin = GetDateTime(DR("last_checkin"))
                    _E._LicenseStatus = GetInteger(DR("license_status"))
                    _E._LicenseExpires = GetDate(DR("license_expires"))
                    _E._LicenseData = DR("license_data").ToString.Trim
                    _E._AllowWorkstation = GetBoolean(DR("allow_workstation"))
                    _E._AllowDb = GetBoolean(DR("allow_db"))
                    _E._AllowService = GetBoolean(DR("allow_service"))
                    _E._AllowTouchscreen = GetBoolean(DR("allow_touchscreen"))
                    _E._AllowTablet = GetBoolean(DR("allow_tablet"))
                    _E._LastUser = DR("last_user").ToString.Trim
                    _E._LastWorkstation = DR("last_workstation").ToString.Trim
                    _E._LastService = DR("last_service").ToString.Trim
                    _E._LastDatabase = DR("last_database").ToString.Trim
                    _E._LastStateService = GetInteger(DR("last_state_service"))
                    _E._LastStateLivedrive = GetInteger(DR("last_state_livedrive"))
                    _E._LastBackupScheduled = GetBoolean(DR("last_backup_scheduled"))
                    _E._LastBackupPerformed = GetDateTime(DR("last_backup_performed"))

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Endpoint)

            Dim _EndpointList As New List(Of Endpoint)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EndpointList.Add(PropertiesFromData(_DR))
            Next

            Return _EndpointList

        End Function


#End Region

    End Class

End Namespace
