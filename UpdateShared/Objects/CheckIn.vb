﻿'*****************************************************
'Generated 02/07/2017 18:51:17 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class CheckIn
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_CustomerId As Guid?
        Dim m_ComputerName As String
        Dim m_UserName As String
        Dim m_VerWorkstation As String
        Dim m_VerService As String
        Dim m_VerDatabase As String
        Dim m_StateService As Integer
        Dim m_StateLivedrive As Integer
        Dim m_BackupScheduled As Boolean
        Dim m_BackupLast As DateTime?
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._CustomerId = Nothing
                ._ComputerName = ""
                ._UserName = ""
                ._VerWorkstation = ""
                ._VerService = ""
                ._VerDatabase = ""
                ._StateService = 0
                ._StateLivedrive = 0
                ._BackupScheduled = False
                ._BackupLast = Nothing
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._CustomerId = Nothing
                ._ComputerName = ""
                ._UserName = ""
                ._VerWorkstation = ""
                ._VerService = ""
                ._VerDatabase = ""
                ._StateService = 0
                ._StateLivedrive = 0
                ._BackupScheduled = False
                ._BackupLast = Nothing
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@CustomerId")>
        Public Property _CustomerId() As Guid?
            Get
                Return m_CustomerId
            End Get
            Set(ByVal value As Guid?)
                m_CustomerId = value
            End Set
        End Property

        <StoredProcParameter("@ComputerName")>
        Public Property _ComputerName() As String
            Get
                Return m_ComputerName
            End Get
            Set(ByVal value As String)
                m_ComputerName = value
            End Set
        End Property

        <StoredProcParameter("@UserName")>
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@VerWorkstation")>
        Public Property _VerWorkstation() As String
            Get
                Return m_VerWorkstation
            End Get
            Set(ByVal value As String)
                m_VerWorkstation = value
            End Set
        End Property

        <StoredProcParameter("@VerService")>
        Public Property _VerService() As String
            Get
                Return m_VerService
            End Get
            Set(ByVal value As String)
                m_VerService = value
            End Set
        End Property

        <StoredProcParameter("@VerDatabase")>
        Public Property _VerDatabase() As String
            Get
                Return m_VerDatabase
            End Get
            Set(ByVal value As String)
                m_VerDatabase = value
            End Set
        End Property

        <StoredProcParameter("@StateService")>
        Public Property _StateService() As Integer
            Get
                Return m_StateService
            End Get
            Set(ByVal value As Integer)
                m_StateService = value
            End Set
        End Property

        <StoredProcParameter("@StateLivedrive")>
        Public Property _StateLivedrive() As Integer
            Get
                Return m_StateLivedrive
            End Get
            Set(ByVal value As Integer)
                m_StateLivedrive = value
            End Set
        End Property

        <StoredProcParameter("@BackupScheduled")>
        Public Property _BackupScheduled() As Boolean
            Get
                Return m_BackupScheduled
            End Get
            Set(ByVal value As Boolean)
                m_BackupScheduled = value
            End Set
        End Property

        <StoredProcParameter("@BackupLast")>
        Public Property _BackupLast() As DateTime?
            Get
                Return m_BackupLast
            End Get
            Set(ByVal value As DateTime?)
                m_BackupLast = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As CheckIn

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCheckInbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As CheckIn

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCheckInbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of CheckIn)

            Dim _CheckInList As List(Of CheckIn)
            _CheckInList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCheckInTable"))
            Return _CheckInList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of CheckIn)

            Dim _CheckInList As List(Of CheckIn)
            _CheckInList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCheckInTable"))
            Return _CheckInList

        End Function

        Public Shared Sub SaveAll(ByVal CheckInList As List(Of CheckIn))

            For Each _CheckIn As CheckIn In CheckInList
                SaveRecord(_CheckIn)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CheckInList As List(Of CheckIn))

            For Each _CheckIn As CheckIn In CheckInList
                SaveRecord(ConnectionString, _CheckIn)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal CheckIn As CheckIn) As Guid
            CheckIn.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, CheckIn, "upsertCheckIn")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal CheckIn As CheckIn) As Guid
            CheckIn.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, CheckIn, "upsertCheckIn")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCheckInbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCheckInbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertCheckIn")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertCheckIn")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As CheckIn

            Dim _C As CheckIn = Nothing

            If DR IsNot Nothing Then
                _C = New CheckIn()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._CustomerId = GetGUID(DR("customer_id"))
                    _C._ComputerName = DR("computer_name").ToString.Trim
                    _C._UserName = DR("user_name").ToString.Trim
                    _C._VerWorkstation = DR("ver_workstation").ToString.Trim
                    _C._VerService = DR("ver_service").ToString.Trim
                    _C._VerDatabase = DR("ver_database").ToString.Trim
                    _C._StateService = GetInteger(DR("state_service"))
                    _C._StateLivedrive = GetInteger(DR("state_livedrive"))
                    _C._BackupScheduled = GetBoolean(DR("backup_scheduled"))
                    _C._BackupLast = GetDateTime(DR("backup_last"))
                    _C._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of CheckIn)

            Dim _CheckInList As New List(Of CheckIn)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CheckInList.Add(PropertiesFromData(_DR))
            Next

            Return _CheckInList

        End Function


#End Region

    End Class

End Namespace
