﻿'*****************************************************
'Generated 02/07/2017 18:53:31 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class EndpointJob
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_EndpointId As Guid?
        Dim m_AppId As Guid?
        Dim m_JobId As Guid?
        Dim m_Immediate As Boolean
        Dim m_Status As Byte
        Dim m_LastRun As DateTime?
        Dim m_LastLog As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._EndpointId = Nothing
                ._AppId = Nothing
                ._JobId = Nothing
                ._Immediate = False
                ._Status = Nothing
                ._LastRun = Nothing
                ._LastLog = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._EndpointId = Nothing
                ._AppId = Nothing
                ._JobId = Nothing
                ._Immediate = False
                ._Status = Nothing
                ._LastRun = Nothing
                ._LastLog = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@EndpointId")>
        Public Property _EndpointId() As Guid?
            Get
                Return m_EndpointId
            End Get
            Set(ByVal value As Guid?)
                m_EndpointId = value
            End Set
        End Property

        <StoredProcParameter("@AppId")>
        Public Property _AppId() As Guid?
            Get
                Return m_AppId
            End Get
            Set(ByVal value As Guid?)
                m_AppId = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@Immediate")>
        Public Property _Immediate() As Boolean
            Get
                Return m_Immediate
            End Get
            Set(ByVal value As Boolean)
                m_Immediate = value
            End Set
        End Property

        <StoredProcParameter("@Status")>
        Public Property _Status() As Byte
            Get
                Return m_Status
            End Get
            Set(ByVal value As Byte)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@LastRun")>
        Public Property _LastRun() As DateTime?
            Get
                Return m_LastRun
            End Get
            Set(ByVal value As DateTime?)
                m_LastRun = value
            End Set
        End Property

        <StoredProcParameter("@LastLog")>
        Public Property _LastLog() As String
            Get
                Return m_LastLog
            End Get
            Set(ByVal value As String)
                m_LastLog = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As EndpointJob

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEndpointJobbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As EndpointJob

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getEndpointJobbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of EndpointJob)

            Dim _EndpointJobList As List(Of EndpointJob)
            _EndpointJobList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEndpointJobTable"))
            Return _EndpointJobList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of EndpointJob)

            Dim _EndpointJobList As List(Of EndpointJob)
            _EndpointJobList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getEndpointJobTable"))
            Return _EndpointJobList

        End Function

        Public Shared Sub SaveAll(ByVal EndpointJobList As List(Of EndpointJob))

            For Each _EndpointJob As EndpointJob In EndpointJobList
                SaveRecord(_EndpointJob)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal EndpointJobList As List(Of EndpointJob))

            For Each _EndpointJob As EndpointJob In EndpointJobList
                SaveRecord(ConnectionString, _EndpointJob)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal EndpointJob As EndpointJob) As Guid
            EndpointJob.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, EndpointJob, "upsertEndpointJob")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal EndpointJob As EndpointJob) As Guid
            EndpointJob.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, EndpointJob, "upsertEndpointJob")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEndpointJobbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteEndpointJobbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertEndpointJob")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertEndpointJob")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As EndpointJob

            Dim _E As EndpointJob = Nothing

            If DR IsNot Nothing Then
                _E = New EndpointJob()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._EndpointId = GetGUID(DR("endpoint_id"))
                    _E._AppId = GetGUID(DR("app_id"))
                    _E._JobId = GetGUID(DR("job_id"))
                    _E._Immediate = GetBoolean(DR("immediate"))
                    _E._Status = GetByte(DR("status"))
                    _E._LastRun = GetDateTime(DR("last_run"))
                    _E._LastLog = DR("last_log").ToString.Trim

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of EndpointJob)

            Dim _EndpointJobList As New List(Of EndpointJob)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EndpointJobList.Add(PropertiesFromData(_DR))
            Next

            Return _EndpointJobList

        End Function


#End Region

    End Class

End Namespace
