﻿'*****************************************************
'Generated 02/07/2017 18:51:52 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Customer
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Active As Boolean
        Dim m_UrlSalesforce As String
        Dim m_UrlScreenconnect As String
        Dim m_SfAccount As String
        Dim m_SfProduct As String
        Dim m_SfLicenseExp As Date?
        Dim m_SfLast As DateTime?
        Dim m_LastCheckin As DateTime?
        Dim m_LastUpdate As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Active = False
                ._UrlSalesforce = ""
                ._UrlScreenconnect = ""
                ._SfAccount = ""
                ._SfProduct = ""
                ._SfLicenseExp = Nothing
                ._SfLast = Nothing
                ._LastCheckin = Nothing
                ._LastUpdate = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Active = False
                ._UrlSalesforce = ""
                ._UrlScreenconnect = ""
                ._SfAccount = ""
                ._SfProduct = ""
                ._SfLicenseExp = Nothing
                ._SfLast = Nothing
                ._LastCheckin = Nothing
                ._LastUpdate = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")>
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Active")>
        Public Property _Active() As Boolean
            Get
                Return m_Active
            End Get
            Set(ByVal value As Boolean)
                m_Active = value
            End Set
        End Property

        <StoredProcParameter("@UrlSalesforce")>
        Public Property _UrlSalesforce() As String
            Get
                Return m_UrlSalesforce
            End Get
            Set(ByVal value As String)
                m_UrlSalesforce = value
            End Set
        End Property

        <StoredProcParameter("@UrlScreenconnect")>
        Public Property _UrlScreenconnect() As String
            Get
                Return m_UrlScreenconnect
            End Get
            Set(ByVal value As String)
                m_UrlScreenconnect = value
            End Set
        End Property

        <StoredProcParameter("@SfAccount")>
        Public Property _SfAccount() As String
            Get
                Return m_SfAccount
            End Get
            Set(ByVal value As String)
                m_SfAccount = value
            End Set
        End Property

        <StoredProcParameter("@SfProduct")>
        Public Property _SfProduct() As String
            Get
                Return m_SfProduct
            End Get
            Set(ByVal value As String)
                m_SfProduct = value
            End Set
        End Property

        <StoredProcParameter("@SfLicenseExp")>
        Public Property _SfLicenseExp() As Date?
            Get
                Return m_SfLicenseExp
            End Get
            Set(ByVal value As Date?)
                m_SfLicenseExp = value
            End Set
        End Property

        <StoredProcParameter("@SfLast")>
        Public Property _SfLast() As DateTime?
            Get
                Return m_SfLast
            End Get
            Set(ByVal value As DateTime?)
                m_SfLast = value
            End Set
        End Property

        <StoredProcParameter("@LastCheckin")>
        Public Property _LastCheckin() As DateTime?
            Get
                Return m_LastCheckin
            End Get
            Set(ByVal value As DateTime?)
                m_LastCheckin = value
            End Set
        End Property

        <StoredProcParameter("@LastUpdate")>
        Public Property _LastUpdate() As DateTime?
            Get
                Return m_LastUpdate
            End Get
            Set(ByVal value As DateTime?)
                m_LastUpdate = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Customer

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getCustomerbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Customer

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getCustomerbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Customer)

            Dim _CustomerList As List(Of Customer)
            _CustomerList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getCustomerTable"))
            Return _CustomerList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Customer)

            Dim _CustomerList As List(Of Customer)
            _CustomerList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getCustomerTable"))
            Return _CustomerList

        End Function

        Public Shared Sub SaveAll(ByVal CustomerList As List(Of Customer))

            For Each _Customer As Customer In CustomerList
                SaveRecord(_Customer)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal CustomerList As List(Of Customer))

            For Each _Customer As Customer In CustomerList
                SaveRecord(ConnectionString, _Customer)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Customer As Customer) As Guid
            Customer.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Customer, "upsertCustomer")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Customer As Customer) As Guid
            Customer.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Customer, "upsertCustomer")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteCustomerbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteCustomerbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertCustomer")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertCustomer")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Customer

            Dim _C As Customer = Nothing

            If DR IsNot Nothing Then
                _C = New Customer()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Name = DR("name").ToString.Trim
                    _C._Active = GetBoolean(DR("active"))
                    _C._UrlSalesforce = DR("url_salesforce").ToString.Trim
                    _C._UrlScreenconnect = DR("url_screenconnect").ToString.Trim
                    _C._SfAccount = DR("sf_account").ToString.Trim
                    _C._SfProduct = DR("sf_product").ToString.Trim
                    _C._SfLicenseExp = GetDate(DR("sf_license_exp"))
                    _C._SfLast = GetDateTime(DR("sf_last"))
                    _C._LastCheckin = GetDateTime(DR("last_checkin"))
                    _C._LastUpdate = GetDateTime(DR("last_update"))

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Customer)

            Dim _CustomerList As New List(Of Customer)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _CustomerList.Add(PropertiesFromData(_DR))
            Next

            Return _CustomerList

        End Function


#End Region

    End Class

End Namespace
