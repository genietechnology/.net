﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomer
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.tcCustomer = New Care.Controls.CareTab()
        Me.tabCustomer = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btnEndpoints = New Care.Controls.CareButton()
        Me.cdtExpires = New Care.Controls.CareDateTime()
        Me.txtLastSync = New Care.Controls.CareTextBox()
        Me.CareLabel12 = New Care.Controls.CareLabel()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.btnSalesforce = New Care.Controls.CareButton()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.CareLabel9 = New Care.Controls.CareLabel()
        Me.txtProduct = New Care.Controls.CareTextBox()
        Me.txtAccountNumber = New Care.Controls.CareTextBox()
        Me.CareLabel10 = New Care.Controls.CareLabel()
        Me.txtUrlSalesforce = New Care.Controls.CareTextBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkActive = New Care.Controls.CareCheckBox()
        Me.CareLabel11 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtLastUpdate = New Care.Controls.CareTextBox()
        Me.txtLastCheckIn = New Care.Controls.CareTextBox()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.btnScreenConnect = New Care.Controls.CareButton()
        Me.btnCopyID = New Care.Controls.CareButton()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.txtUrlScreenconnect = New Care.Controls.CareTextBox()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.tabApplications = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxApp = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel13 = New Care.Controls.CareLabel()
        Me.cbxAppType = New Care.Controls.CareComboBox()
        Me.txtConnString = New DevExpress.XtraEditors.MemoEdit()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.txtAppServer = New Care.Controls.CareTextBox()
        Me.txtAppPath = New Care.Controls.CareTextBox()
        Me.txtAppName = New Care.Controls.CareTextBox()
        Me.btnAppCancel = New Care.Controls.CareButton()
        Me.btnAppOK = New Care.Controls.CareButton()
        Me.CareLabel30 = New Care.Controls.CareLabel()
        Me.CareLabel32 = New Care.Controls.CareLabel()
        Me.cgApps = New Care.Controls.CareGridWithButtons()
        Me.tabEndpoints = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxEndpoint = New DevExpress.XtraEditors.GroupControl()
        Me.chkTablet = New Care.Controls.CareCheckBox()
        Me.chkTouchscreen = New Care.Controls.CareCheckBox()
        Me.chkService = New Care.Controls.CareCheckBox()
        Me.chkDatabase = New Care.Controls.CareCheckBox()
        Me.cdtEndpointExpires = New Care.Controls.CareDateTime()
        Me.chkWorkstation = New Care.Controls.CareCheckBox()
        Me.CareLabel14 = New Care.Controls.CareLabel()
        Me.cbxEndpointStatus = New Care.Controls.CareComboBox()
        Me.CareLabel15 = New Care.Controls.CareLabel()
        Me.txtEndpointComputerName = New Care.Controls.CareTextBox()
        Me.btnEndpointCancel = New Care.Controls.CareButton()
        Me.btnEndpointOK = New Care.Controls.CareButton()
        Me.CareLabel17 = New Care.Controls.CareLabel()
        Me.CareLabel18 = New Care.Controls.CareLabel()
        Me.cgEndpoints = New Care.Controls.CareGridWithButtons()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tcCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcCustomer.SuspendLayout()
        Me.tabCustomer.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cdtExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtExpires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastSync.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProduct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrlSalesforce.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastUpdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastCheckIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrlScreenconnect.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabApplications.SuspendLayout()
        CType(Me.gbxApp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxApp.SuspendLayout()
        CType(Me.cbxAppType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConnString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAppServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAppPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAppName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabEndpoints.SuspendLayout()
        CType(Me.gbxEndpoint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEndpoint.SuspendLayout()
        CType(Me.chkTablet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTouchscreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkService.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDatabase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEndpointExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEndpointExpires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWorkstation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxEndpointStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEndpointComputerName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(787, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(696, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 625)
        Me.Panel1.Size = New System.Drawing.Size(884, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tcCustomer
        '
        Me.tcCustomer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcCustomer.Location = New System.Drawing.Point(12, 53)
        Me.tcCustomer.Name = "tcCustomer"
        Me.tcCustomer.SelectedTabPage = Me.tabCustomer
        Me.tcCustomer.Size = New System.Drawing.Size(865, 569)
        Me.tcCustomer.TabIndex = 0
        Me.tcCustomer.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabCustomer, Me.tabApplications, Me.tabEndpoints})
        '
        'tabCustomer
        '
        Me.tabCustomer.Controls.Add(Me.GroupControl2)
        Me.tabCustomer.Controls.Add(Me.GroupControl1)
        Me.tabCustomer.Name = "tabCustomer"
        Me.tabCustomer.Size = New System.Drawing.Size(859, 541)
        Me.tabCustomer.Text = "General"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnEndpoints)
        Me.GroupControl2.Controls.Add(Me.cdtExpires)
        Me.GroupControl2.Controls.Add(Me.txtLastSync)
        Me.GroupControl2.Controls.Add(Me.CareLabel12)
        Me.GroupControl2.Controls.Add(Me.CareLabel7)
        Me.GroupControl2.Controls.Add(Me.btnSalesforce)
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Controls.Add(Me.txtProduct)
        Me.GroupControl2.Controls.Add(Me.txtAccountNumber)
        Me.GroupControl2.Controls.Add(Me.CareLabel10)
        Me.GroupControl2.Controls.Add(Me.txtUrlSalesforce)
        Me.GroupControl2.Location = New System.Drawing.Point(7, 185)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(572, 173)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Salesforce Integration"
        '
        'btnEndpoints
        '
        Me.btnEndpoints.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEndpoints.Appearance.Options.UseFont = True
        Me.btnEndpoints.CausesValidation = False
        Me.btnEndpoints.Location = New System.Drawing.Point(229, 113)
        Me.btnEndpoints.Name = "btnEndpoints"
        Me.btnEndpoints.Size = New System.Drawing.Size(143, 23)
        Me.btnEndpoints.TabIndex = 9
        Me.btnEndpoints.Tag = ""
        Me.btnEndpoints.Text = "Change All Endpoints"
        '
        'cdtExpires
        '
        Me.cdtExpires.EditValue = Nothing
        Me.cdtExpires.EnterMoveNextControl = True
        Me.cdtExpires.Location = New System.Drawing.Point(123, 114)
        Me.cdtExpires.Name = "cdtExpires"
        Me.cdtExpires.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtExpires.Properties.Appearance.Options.UseFont = True
        Me.cdtExpires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtExpires.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtExpires.Size = New System.Drawing.Size(100, 22)
        Me.cdtExpires.TabIndex = 8
        Me.cdtExpires.Tag = "AE"
        Me.cdtExpires.Value = Nothing
        '
        'txtLastSync
        '
        Me.txtLastSync.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastSync.EnterMoveNextControl = True
        Me.txtLastSync.Location = New System.Drawing.Point(123, 142)
        Me.txtLastSync.MaxLength = 0
        Me.txtLastSync.Name = "txtLastSync"
        Me.txtLastSync.NumericAllowNegatives = False
        Me.txtLastSync.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastSync.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastSync.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastSync.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastSync.Properties.Appearance.Options.UseFont = True
        Me.txtLastSync.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastSync.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastSync.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastSync.Size = New System.Drawing.Size(292, 22)
        Me.txtLastSync.TabIndex = 11
        Me.txtLastSync.Tag = "R"
        Me.txtLastSync.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastSync.ToolTipText = ""
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(13, 145)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel12.TabIndex = 10
        Me.CareLabel12.Text = "Last Sync"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(13, 117)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel7.TabIndex = 7
        Me.CareLabel7.Text = "License Expires"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSalesforce
        '
        Me.btnSalesforce.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSalesforce.Appearance.Options.UseFont = True
        Me.btnSalesforce.CausesValidation = False
        Me.btnSalesforce.Location = New System.Drawing.Point(421, 29)
        Me.btnSalesforce.Name = "btnSalesforce"
        Me.btnSalesforce.Size = New System.Drawing.Size(143, 23)
        Me.btnSalesforce.TabIndex = 2
        Me.btnSalesforce.Tag = ""
        Me.btnSalesforce.Text = "Goto Salesforce"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(13, 89)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel8.TabIndex = 5
        Me.CareLabel8.Text = "Product"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(13, 61)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel9.TabIndex = 3
        Me.CareLabel9.Text = "URL"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProduct
        '
        Me.txtProduct.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtProduct.EnterMoveNextControl = True
        Me.txtProduct.Location = New System.Drawing.Point(123, 86)
        Me.txtProduct.MaxLength = 0
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.NumericAllowNegatives = False
        Me.txtProduct.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtProduct.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtProduct.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtProduct.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtProduct.Properties.Appearance.Options.UseFont = True
        Me.txtProduct.Properties.Appearance.Options.UseTextOptions = True
        Me.txtProduct.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtProduct.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtProduct.Size = New System.Drawing.Size(292, 22)
        Me.txtProduct.TabIndex = 6
        Me.txtProduct.Tag = "R"
        Me.txtProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProduct.ToolTipText = ""
        '
        'txtAccountNumber
        '
        Me.txtAccountNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAccountNumber.EnterMoveNextControl = True
        Me.txtAccountNumber.Location = New System.Drawing.Point(123, 30)
        Me.txtAccountNumber.MaxLength = 0
        Me.txtAccountNumber.Name = "txtAccountNumber"
        Me.txtAccountNumber.NumericAllowNegatives = False
        Me.txtAccountNumber.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAccountNumber.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAccountNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAccountNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAccountNumber.Properties.Appearance.Options.UseFont = True
        Me.txtAccountNumber.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAccountNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAccountNumber.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAccountNumber.Size = New System.Drawing.Size(292, 22)
        Me.txtAccountNumber.TabIndex = 1
        Me.txtAccountNumber.Tag = "AEM"
        Me.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAccountNumber.ToolTipText = ""
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(13, 33)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Account Number"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUrlSalesforce
        '
        Me.txtUrlSalesforce.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUrlSalesforce.EnterMoveNextControl = True
        Me.txtUrlSalesforce.Location = New System.Drawing.Point(123, 58)
        Me.txtUrlSalesforce.MaxLength = 0
        Me.txtUrlSalesforce.Name = "txtUrlSalesforce"
        Me.txtUrlSalesforce.NumericAllowNegatives = False
        Me.txtUrlSalesforce.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUrlSalesforce.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUrlSalesforce.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUrlSalesforce.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUrlSalesforce.Properties.Appearance.Options.UseFont = True
        Me.txtUrlSalesforce.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUrlSalesforce.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUrlSalesforce.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUrlSalesforce.Size = New System.Drawing.Size(292, 22)
        Me.txtUrlSalesforce.TabIndex = 4
        Me.txtUrlSalesforce.Tag = "AE"
        Me.txtUrlSalesforce.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUrlSalesforce.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.chkActive)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtLastUpdate)
        Me.GroupControl1.Controls.Add(Me.txtLastCheckIn)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.btnScreenConnect)
        Me.GroupControl1.Controls.Add(Me.btnCopyID)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtUrlScreenconnect)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 7)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(572, 172)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Customer Details"
        '
        'chkActive
        '
        Me.chkActive.EnterMoveNextControl = True
        Me.chkActive.Location = New System.Drawing.Point(123, 87)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Properties.Caption = ""
        Me.chkActive.Size = New System.Drawing.Size(19, 19)
        Me.chkActive.TabIndex = 7
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(13, 145)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel11.TabIndex = 10
        Me.CareLabel11.Text = "Last Update"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(13, 117)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Last CheckIn"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastUpdate
        '
        Me.txtLastUpdate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastUpdate.EnterMoveNextControl = True
        Me.txtLastUpdate.Location = New System.Drawing.Point(123, 142)
        Me.txtLastUpdate.MaxLength = 0
        Me.txtLastUpdate.Name = "txtLastUpdate"
        Me.txtLastUpdate.NumericAllowNegatives = False
        Me.txtLastUpdate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastUpdate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastUpdate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastUpdate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastUpdate.Properties.Appearance.Options.UseFont = True
        Me.txtLastUpdate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastUpdate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastUpdate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastUpdate.Size = New System.Drawing.Size(292, 22)
        Me.txtLastUpdate.TabIndex = 11
        Me.txtLastUpdate.Tag = "R"
        Me.txtLastUpdate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastUpdate.ToolTipText = ""
        '
        'txtLastCheckIn
        '
        Me.txtLastCheckIn.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastCheckIn.EnterMoveNextControl = True
        Me.txtLastCheckIn.Location = New System.Drawing.Point(123, 114)
        Me.txtLastCheckIn.MaxLength = 0
        Me.txtLastCheckIn.Name = "txtLastCheckIn"
        Me.txtLastCheckIn.NumericAllowNegatives = False
        Me.txtLastCheckIn.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastCheckIn.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastCheckIn.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastCheckIn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastCheckIn.Properties.Appearance.Options.UseFont = True
        Me.txtLastCheckIn.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastCheckIn.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastCheckIn.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastCheckIn.Size = New System.Drawing.Size(292, 22)
        Me.txtLastCheckIn.TabIndex = 9
        Me.txtLastCheckIn.Tag = "R"
        Me.txtLastCheckIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastCheckIn.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(13, 89)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel6.TabIndex = 6
        Me.CareLabel6.Text = "Active"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnScreenConnect
        '
        Me.btnScreenConnect.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnScreenConnect.Appearance.Options.UseFont = True
        Me.btnScreenConnect.CausesValidation = False
        Me.btnScreenConnect.Location = New System.Drawing.Point(421, 57)
        Me.btnScreenConnect.Name = "btnScreenConnect"
        Me.btnScreenConnect.Size = New System.Drawing.Size(143, 23)
        Me.btnScreenConnect.TabIndex = 5
        Me.btnScreenConnect.Tag = ""
        Me.btnScreenConnect.Text = "Goto ScreenConnect"
        '
        'btnCopyID
        '
        Me.btnCopyID.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCopyID.Appearance.Options.UseFont = True
        Me.btnCopyID.CausesValidation = False
        Me.btnCopyID.Location = New System.Drawing.Point(421, 29)
        Me.btnCopyID.Name = "btnCopyID"
        Me.btnCopyID.Size = New System.Drawing.Size(143, 23)
        Me.btnCopyID.TabIndex = 2
        Me.btnCopyID.Tag = ""
        Me.btnCopyID.Text = "Copy Customer ID"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(13, 61)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "ScreenConnect URL"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUrlScreenconnect
        '
        Me.txtUrlScreenconnect.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUrlScreenconnect.EnterMoveNextControl = True
        Me.txtUrlScreenconnect.Location = New System.Drawing.Point(123, 58)
        Me.txtUrlScreenconnect.MaxLength = 0
        Me.txtUrlScreenconnect.Name = "txtUrlScreenconnect"
        Me.txtUrlScreenconnect.NumericAllowNegatives = False
        Me.txtUrlScreenconnect.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUrlScreenconnect.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUrlScreenconnect.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUrlScreenconnect.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUrlScreenconnect.Properties.Appearance.Options.UseFont = True
        Me.txtUrlScreenconnect.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUrlScreenconnect.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUrlScreenconnect.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUrlScreenconnect.Size = New System.Drawing.Size(292, 22)
        Me.txtUrlScreenconnect.TabIndex = 4
        Me.txtUrlScreenconnect.Tag = "AE"
        Me.txtUrlScreenconnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUrlScreenconnect.ToolTipText = ""
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(123, 30)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Size = New System.Drawing.Size(292, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(13, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabApplications
        '
        Me.tabApplications.Controls.Add(Me.gbxApp)
        Me.tabApplications.Controls.Add(Me.cgApps)
        Me.tabApplications.Name = "tabApplications"
        Me.tabApplications.Size = New System.Drawing.Size(859, 541)
        Me.tabApplications.Text = "Applications"
        '
        'gbxApp
        '
        Me.gbxApp.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxApp.Controls.Add(Me.CareLabel13)
        Me.gbxApp.Controls.Add(Me.cbxAppType)
        Me.gbxApp.Controls.Add(Me.txtConnString)
        Me.gbxApp.Controls.Add(Me.CareLabel4)
        Me.gbxApp.Controls.Add(Me.CareLabel5)
        Me.gbxApp.Controls.Add(Me.txtAppServer)
        Me.gbxApp.Controls.Add(Me.txtAppPath)
        Me.gbxApp.Controls.Add(Me.txtAppName)
        Me.gbxApp.Controls.Add(Me.btnAppCancel)
        Me.gbxApp.Controls.Add(Me.btnAppOK)
        Me.gbxApp.Controls.Add(Me.CareLabel30)
        Me.gbxApp.Controls.Add(Me.CareLabel32)
        Me.gbxApp.Location = New System.Drawing.Point(153, 114)
        Me.gbxApp.Name = "gbxApp"
        Me.gbxApp.Size = New System.Drawing.Size(553, 246)
        Me.gbxApp.TabIndex = 1
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel13.TabIndex = 2
        Me.CareLabel13.Text = "Application Type"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel13.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cbxAppType
        '
        Me.cbxAppType.AllowBlank = False
        Me.cbxAppType.DataSource = Nothing
        Me.cbxAppType.DisplayMember = Nothing
        Me.cbxAppType.EnterMoveNextControl = True
        Me.cbxAppType.Location = New System.Drawing.Point(119, 57)
        Me.cbxAppType.Name = "cbxAppType"
        Me.cbxAppType.Properties.AccessibleName = "List Type"
        Me.cbxAppType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAppType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxAppType.Properties.Appearance.Options.UseFont = True
        Me.cbxAppType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAppType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAppType.SelectedValue = Nothing
        Me.cbxAppType.Size = New System.Drawing.Size(420, 22)
        Me.cbxAppType.TabIndex = 3
        Me.cbxAppType.Tag = "AEM"
        Me.cbxAppType.ValueMember = Nothing
        '
        'txtConnString
        '
        Me.txtConnString.Location = New System.Drawing.Point(119, 141)
        Me.txtConnString.Name = "txtConnString"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtConnString, True)
        Me.txtConnString.Size = New System.Drawing.Size(420, 69)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtConnString, OptionsSpelling2)
        Me.txtConnString.TabIndex = 9
        Me.txtConnString.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 116)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Server"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel4.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 142)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(66, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "Conn. String"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAppServer
        '
        Me.txtAppServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAppServer.EnterMoveNextControl = True
        Me.txtAppServer.Location = New System.Drawing.Point(119, 113)
        Me.txtAppServer.MaxLength = 0
        Me.txtAppServer.Name = "txtAppServer"
        Me.txtAppServer.NumericAllowNegatives = False
        Me.txtAppServer.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAppServer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAppServer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAppServer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAppServer.Properties.Appearance.Options.UseFont = True
        Me.txtAppServer.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAppServer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAppServer.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAppServer.Size = New System.Drawing.Size(420, 22)
        Me.txtAppServer.TabIndex = 7
        Me.txtAppServer.Tag = "AEM"
        Me.txtAppServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAppServer.ToolTipText = ""
        '
        'txtAppPath
        '
        Me.txtAppPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAppPath.EnterMoveNextControl = True
        Me.txtAppPath.Location = New System.Drawing.Point(119, 85)
        Me.txtAppPath.MaxLength = 0
        Me.txtAppPath.Name = "txtAppPath"
        Me.txtAppPath.NumericAllowNegatives = False
        Me.txtAppPath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAppPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAppPath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAppPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAppPath.Properties.Appearance.Options.UseFont = True
        Me.txtAppPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAppPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAppPath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAppPath.Size = New System.Drawing.Size(420, 22)
        Me.txtAppPath.TabIndex = 5
        Me.txtAppPath.Tag = "AEM"
        Me.txtAppPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAppPath.ToolTipText = ""
        '
        'txtAppName
        '
        Me.txtAppName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAppName.EnterMoveNextControl = True
        Me.txtAppName.Location = New System.Drawing.Point(119, 29)
        Me.txtAppName.MaxLength = 0
        Me.txtAppName.Name = "txtAppName"
        Me.txtAppName.NumericAllowNegatives = False
        Me.txtAppName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAppName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAppName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAppName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAppName.Properties.Appearance.Options.UseFont = True
        Me.txtAppName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAppName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAppName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAppName.Size = New System.Drawing.Size(420, 22)
        Me.txtAppName.TabIndex = 1
        Me.txtAppName.Tag = "AEM"
        Me.txtAppName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAppName.ToolTipText = ""
        '
        'btnAppCancel
        '
        Me.btnAppCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAppCancel.Appearance.Options.UseFont = True
        Me.btnAppCancel.CausesValidation = False
        Me.btnAppCancel.Location = New System.Drawing.Point(454, 217)
        Me.btnAppCancel.Name = "btnAppCancel"
        Me.btnAppCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnAppCancel.TabIndex = 11
        Me.btnAppCancel.Text = "Cancel"
        '
        'btnAppOK
        '
        Me.btnAppOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAppOK.Appearance.Options.UseFont = True
        Me.btnAppOK.CausesValidation = False
        Me.btnAppOK.Location = New System.Drawing.Point(363, 217)
        Me.btnAppOK.Name = "btnAppOK"
        Me.btnAppOK.Size = New System.Drawing.Size(85, 23)
        Me.btnAppOK.TabIndex = 10
        Me.btnAppOK.Tag = "AE"
        Me.btnAppOK.Text = "OK"
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel30.TabIndex = 0
        Me.CareLabel30.Text = "Name"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel30.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel30.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(12, 88)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel32.TabIndex = 4
        Me.CareLabel32.Text = "Path"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgApps
        '
        Me.cgApps.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgApps.ButtonsEnabled = False
        Me.cgApps.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgApps.HideFirstColumn = False
        Me.cgApps.Location = New System.Drawing.Point(3, 3)
        Me.cgApps.Name = "cgApps"
        Me.cgApps.PreviewColumn = ""
        Me.cgApps.Size = New System.Drawing.Size(853, 535)
        Me.cgApps.TabIndex = 0
        '
        'tabEndpoints
        '
        Me.tabEndpoints.Controls.Add(Me.gbxEndpoint)
        Me.tabEndpoints.Controls.Add(Me.cgEndpoints)
        Me.tabEndpoints.Name = "tabEndpoints"
        Me.tabEndpoints.Size = New System.Drawing.Size(859, 541)
        Me.tabEndpoints.Text = "Endpoints"
        '
        'gbxEndpoint
        '
        Me.gbxEndpoint.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxEndpoint.Controls.Add(Me.chkTablet)
        Me.gbxEndpoint.Controls.Add(Me.chkTouchscreen)
        Me.gbxEndpoint.Controls.Add(Me.chkService)
        Me.gbxEndpoint.Controls.Add(Me.chkDatabase)
        Me.gbxEndpoint.Controls.Add(Me.cdtEndpointExpires)
        Me.gbxEndpoint.Controls.Add(Me.chkWorkstation)
        Me.gbxEndpoint.Controls.Add(Me.CareLabel14)
        Me.gbxEndpoint.Controls.Add(Me.cbxEndpointStatus)
        Me.gbxEndpoint.Controls.Add(Me.CareLabel15)
        Me.gbxEndpoint.Controls.Add(Me.txtEndpointComputerName)
        Me.gbxEndpoint.Controls.Add(Me.btnEndpointCancel)
        Me.gbxEndpoint.Controls.Add(Me.btnEndpointOK)
        Me.gbxEndpoint.Controls.Add(Me.CareLabel17)
        Me.gbxEndpoint.Controls.Add(Me.CareLabel18)
        Me.gbxEndpoint.Location = New System.Drawing.Point(153, 132)
        Me.gbxEndpoint.Name = "gbxEndpoint"
        Me.gbxEndpoint.Size = New System.Drawing.Size(553, 250)
        Me.gbxEndpoint.TabIndex = 2
        '
        'chkTablet
        '
        Me.chkTablet.EnterMoveNextControl = True
        Me.chkTablet.Location = New System.Drawing.Point(119, 214)
        Me.chkTablet.Name = "chkTablet"
        Me.chkTablet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTablet.Properties.Appearance.Options.UseFont = True
        Me.chkTablet.Properties.AutoWidth = True
        Me.chkTablet.Properties.Caption = "Offline Tablet"
        Me.chkTablet.Size = New System.Drawing.Size(94, 19)
        Me.chkTablet.TabIndex = 17
        Me.chkTablet.Tag = "AE"
        '
        'chkTouchscreen
        '
        Me.chkTouchscreen.EnterMoveNextControl = True
        Me.chkTouchscreen.Location = New System.Drawing.Point(119, 189)
        Me.chkTouchscreen.Name = "chkTouchscreen"
        Me.chkTouchscreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTouchscreen.Properties.Appearance.Options.UseFont = True
        Me.chkTouchscreen.Properties.AutoWidth = True
        Me.chkTouchscreen.Properties.Caption = "Touchscreen"
        Me.chkTouchscreen.Size = New System.Drawing.Size(90, 19)
        Me.chkTouchscreen.TabIndex = 16
        Me.chkTouchscreen.Tag = "AE"
        '
        'chkService
        '
        Me.chkService.EnterMoveNextControl = True
        Me.chkService.Location = New System.Drawing.Point(119, 164)
        Me.chkService.Name = "chkService"
        Me.chkService.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkService.Properties.Appearance.Options.UseFont = True
        Me.chkService.Properties.AutoWidth = True
        Me.chkService.Properties.Caption = "Service"
        Me.chkService.Size = New System.Drawing.Size(59, 19)
        Me.chkService.TabIndex = 15
        Me.chkService.Tag = "AE"
        '
        'chkDatabase
        '
        Me.chkDatabase.EnterMoveNextControl = True
        Me.chkDatabase.Location = New System.Drawing.Point(119, 139)
        Me.chkDatabase.Name = "chkDatabase"
        Me.chkDatabase.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDatabase.Properties.Appearance.Options.UseFont = True
        Me.chkDatabase.Properties.AutoWidth = True
        Me.chkDatabase.Properties.Caption = "Database"
        Me.chkDatabase.Size = New System.Drawing.Size(70, 19)
        Me.chkDatabase.TabIndex = 14
        Me.chkDatabase.Tag = "AE"
        '
        'cdtEndpointExpires
        '
        Me.cdtEndpointExpires.EditValue = Nothing
        Me.cdtEndpointExpires.EnterMoveNextControl = True
        Me.cdtEndpointExpires.Location = New System.Drawing.Point(119, 85)
        Me.cdtEndpointExpires.Name = "cdtEndpointExpires"
        Me.cdtEndpointExpires.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtEndpointExpires.Properties.Appearance.Options.UseFont = True
        Me.cdtEndpointExpires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEndpointExpires.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEndpointExpires.Size = New System.Drawing.Size(100, 22)
        Me.cdtEndpointExpires.TabIndex = 13
        Me.cdtEndpointExpires.Tag = "AE"
        Me.cdtEndpointExpires.Value = Nothing
        '
        'chkWorkstation
        '
        Me.chkWorkstation.EnterMoveNextControl = True
        Me.chkWorkstation.Location = New System.Drawing.Point(119, 114)
        Me.chkWorkstation.Name = "chkWorkstation"
        Me.chkWorkstation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkWorkstation.Properties.Appearance.Options.UseFont = True
        Me.chkWorkstation.Properties.AutoWidth = True
        Me.chkWorkstation.Properties.Caption = "Workstation"
        Me.chkWorkstation.Size = New System.Drawing.Size(86, 19)
        Me.chkWorkstation.TabIndex = 12
        Me.chkWorkstation.Tag = "AE"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel14.TabIndex = 2
        Me.CareLabel14.Text = "License Status"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel14.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cbxEndpointStatus
        '
        Me.cbxEndpointStatus.AllowBlank = False
        Me.cbxEndpointStatus.DataSource = Nothing
        Me.cbxEndpointStatus.DisplayMember = Nothing
        Me.cbxEndpointStatus.EnterMoveNextControl = True
        Me.cbxEndpointStatus.Location = New System.Drawing.Point(119, 57)
        Me.cbxEndpointStatus.Name = "cbxEndpointStatus"
        Me.cbxEndpointStatus.Properties.AccessibleName = "List Type"
        Me.cbxEndpointStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxEndpointStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxEndpointStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxEndpointStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEndpointStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxEndpointStatus.SelectedValue = Nothing
        Me.cbxEndpointStatus.Size = New System.Drawing.Size(100, 22)
        Me.cbxEndpointStatus.TabIndex = 3
        Me.cbxEndpointStatus.Tag = "AEM"
        Me.cbxEndpointStatus.ValueMember = Nothing
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(12, 116)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(89, 15)
        Me.CareLabel15.TabIndex = 6
        Me.CareLabel15.Text = "Permitted Types:"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel15.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'txtEndpointComputerName
        '
        Me.txtEndpointComputerName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEndpointComputerName.EnterMoveNextControl = True
        Me.txtEndpointComputerName.Location = New System.Drawing.Point(119, 29)
        Me.txtEndpointComputerName.MaxLength = 0
        Me.txtEndpointComputerName.Name = "txtEndpointComputerName"
        Me.txtEndpointComputerName.NumericAllowNegatives = False
        Me.txtEndpointComputerName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEndpointComputerName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEndpointComputerName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEndpointComputerName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEndpointComputerName.Properties.Appearance.Options.UseFont = True
        Me.txtEndpointComputerName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEndpointComputerName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEndpointComputerName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEndpointComputerName.Size = New System.Drawing.Size(420, 22)
        Me.txtEndpointComputerName.TabIndex = 1
        Me.txtEndpointComputerName.Tag = "R"
        Me.txtEndpointComputerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEndpointComputerName.ToolTipText = ""
        '
        'btnEndpointCancel
        '
        Me.btnEndpointCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEndpointCancel.Appearance.Options.UseFont = True
        Me.btnEndpointCancel.CausesValidation = False
        Me.btnEndpointCancel.Location = New System.Drawing.Point(454, 217)
        Me.btnEndpointCancel.Name = "btnEndpointCancel"
        Me.btnEndpointCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnEndpointCancel.TabIndex = 11
        Me.btnEndpointCancel.Text = "Cancel"
        '
        'btnEndpointOK
        '
        Me.btnEndpointOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEndpointOK.Appearance.Options.UseFont = True
        Me.btnEndpointOK.CausesValidation = False
        Me.btnEndpointOK.Location = New System.Drawing.Point(363, 217)
        Me.btnEndpointOK.Name = "btnEndpointOK"
        Me.btnEndpointOK.Size = New System.Drawing.Size(85, 23)
        Me.btnEndpointOK.TabIndex = 10
        Me.btnEndpointOK.Tag = "AE"
        Me.btnEndpointOK.Text = "OK"
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel17.TabIndex = 0
        Me.CareLabel17.Text = "Name"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel17.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(12, 88)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel18.TabIndex = 4
        Me.CareLabel18.Text = "License Expires"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgEndpoints
        '
        Me.cgEndpoints.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgEndpoints.ButtonsEnabled = True
        Me.cgEndpoints.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgEndpoints.HideFirstColumn = False
        Me.cgEndpoints.Location = New System.Drawing.Point(3, 3)
        Me.cgEndpoints.Name = "cgEndpoints"
        Me.cgEndpoints.PreviewColumn = ""
        Me.cgEndpoints.Size = New System.Drawing.Size(853, 535)
        Me.cgEndpoints.TabIndex = 0
        '
        'frmCustomer
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(884, 661)
        Me.Controls.Add(Me.tcCustomer)
        Me.Name = "frmCustomer"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tcCustomer, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tcCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcCustomer.ResumeLayout(False)
        Me.tabCustomer.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cdtExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtExpires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastSync.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProduct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrlSalesforce.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastUpdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastCheckIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrlScreenconnect.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabApplications.ResumeLayout(False)
        CType(Me.gbxApp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxApp.ResumeLayout(False)
        Me.gbxApp.PerformLayout()
        CType(Me.cbxAppType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConnString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAppServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAppPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAppName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabEndpoints.ResumeLayout(False)
        CType(Me.gbxEndpoint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEndpoint.ResumeLayout(False)
        Me.gbxEndpoint.PerformLayout()
        CType(Me.chkTablet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTouchscreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkService.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDatabase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEndpointExpires.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEndpointExpires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWorkstation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxEndpointStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEndpointComputerName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcCustomer As Care.Controls.CareTab
    Friend WithEvents tabCustomer As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents tabApplications As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabEndpoints As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtUrlScreenconnect As Care.Controls.CareTextBox
    Friend WithEvents txtUrlSalesforce As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cgApps As Care.Controls.CareGridWithButtons
    Friend WithEvents cgEndpoints As Care.Controls.CareGridWithButtons
    Friend WithEvents gbxApp As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnAppCancel As Care.Controls.CareButton
    Friend WithEvents btnAppOK As Care.Controls.CareButton
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents txtConnString As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtAppServer As Care.Controls.CareTextBox
    Friend WithEvents txtAppPath As Care.Controls.CareTextBox
    Friend WithEvents txtAppName As Care.Controls.CareTextBox
    Friend WithEvents btnCopyID As Care.Controls.CareButton
    Friend WithEvents btnScreenConnect As Care.Controls.CareButton
    Friend WithEvents btnSalesforce As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLastSync As Care.Controls.CareTextBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtProduct As Care.Controls.CareTextBox
    Friend WithEvents txtAccountNumber As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtLastUpdate As Care.Controls.CareTextBox
    Friend WithEvents txtLastCheckIn As Care.Controls.CareTextBox
    Friend WithEvents chkActive As Care.Controls.CareCheckBox
    Friend cdtExpires As Care.Controls.CareDateTime
    Friend WithEvents btnEndpoints As Care.Controls.CareButton
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents cbxAppType As Care.Controls.CareComboBox
    Friend WithEvents gbxEndpoint As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents cbxEndpointStatus As Care.Controls.CareComboBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtEndpointComputerName As Care.Controls.CareTextBox
    Friend WithEvents btnEndpointCancel As Care.Controls.CareButton
    Friend WithEvents btnEndpointOK As Care.Controls.CareButton
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents chkTablet As Care.Controls.CareCheckBox
    Friend WithEvents chkTouchscreen As Care.Controls.CareCheckBox
    Friend WithEvents chkService As Care.Controls.CareCheckBox
    Friend WithEvents chkDatabase As Care.Controls.CareCheckBox
    Friend WithEvents cdtEndpointExpires As Care.Controls.CareDateTime
    Friend WithEvents chkWorkstation As Care.Controls.CareCheckBox

End Class
