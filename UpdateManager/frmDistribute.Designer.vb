﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDistribute
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cgCustomers = New Care.Controls.CareGrid()
        Me.cgApps = New Care.Controls.CareGrid()
        Me.cgEndpoints = New Care.Controls.CareGrid()
        Me.btnDist = New Care.Controls.CareButton()
        Me.cgJobs = New Care.Controls.CareGrid()
        Me.btnImmediate = New Care.Controls.CareButton()
        Me.gbxCustomers = New DevExpress.XtraEditors.GroupControl()
        Me.gbxJobs = New DevExpress.XtraEditors.GroupControl()
        Me.gbxApps = New DevExpress.XtraEditors.GroupControl()
        Me.gbxEndpoints = New DevExpress.XtraEditors.GroupControl()
        CType(Me.gbxCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCustomers.SuspendLayout()
        CType(Me.gbxJobs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxJobs.SuspendLayout()
        CType(Me.gbxApps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxApps.SuspendLayout()
        CType(Me.gbxEndpoints, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEndpoints.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgCustomers
        '
        Me.cgCustomers.AllowBuildColumns = True
        Me.cgCustomers.AllowEdit = False
        Me.cgCustomers.AllowHorizontalScroll = False
        Me.cgCustomers.AllowMultiSelect = False
        Me.cgCustomers.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgCustomers.Appearance.Options.UseFont = True
        Me.cgCustomers.AutoSizeByData = True
        Me.cgCustomers.DisableAutoSize = False
        Me.cgCustomers.DisableDataFormatting = False
        Me.cgCustomers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgCustomers.FocusedRowHandle = -2147483648
        Me.cgCustomers.HideFirstColumn = False
        Me.cgCustomers.Location = New System.Drawing.Point(2, 20)
        Me.cgCustomers.Name = "cgCustomers"
        Me.cgCustomers.PreviewColumn = ""
        Me.cgCustomers.QueryID = Nothing
        Me.cgCustomers.RowAutoHeight = False
        Me.cgCustomers.SearchAsYouType = True
        Me.cgCustomers.ShowAutoFilterRow = False
        Me.cgCustomers.ShowFindPanel = True
        Me.cgCustomers.ShowGroupByBox = False
        Me.cgCustomers.ShowLoadingPanel = False
        Me.cgCustomers.ShowNavigator = True
        Me.cgCustomers.Size = New System.Drawing.Size(271, 390)
        Me.cgCustomers.TabIndex = 0
        '
        'cgApps
        '
        Me.cgApps.AllowBuildColumns = True
        Me.cgApps.AllowEdit = False
        Me.cgApps.AllowHorizontalScroll = False
        Me.cgApps.AllowMultiSelect = False
        Me.cgApps.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgApps.Appearance.Options.UseFont = True
        Me.cgApps.AutoSizeByData = True
        Me.cgApps.DisableAutoSize = False
        Me.cgApps.DisableDataFormatting = False
        Me.cgApps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgApps.FocusedRowHandle = -2147483648
        Me.cgApps.HideFirstColumn = False
        Me.cgApps.Location = New System.Drawing.Point(2, 20)
        Me.cgApps.Name = "cgApps"
        Me.cgApps.PreviewColumn = ""
        Me.cgApps.QueryID = Nothing
        Me.cgApps.RowAutoHeight = False
        Me.cgApps.SearchAsYouType = True
        Me.cgApps.ShowAutoFilterRow = False
        Me.cgApps.ShowFindPanel = True
        Me.cgApps.ShowGroupByBox = False
        Me.cgApps.ShowLoadingPanel = False
        Me.cgApps.ShowNavigator = True
        Me.cgApps.Size = New System.Drawing.Size(371, 390)
        Me.cgApps.TabIndex = 1
        '
        'cgEndpoints
        '
        Me.cgEndpoints.AllowBuildColumns = True
        Me.cgEndpoints.AllowEdit = False
        Me.cgEndpoints.AllowHorizontalScroll = False
        Me.cgEndpoints.AllowMultiSelect = True
        Me.cgEndpoints.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgEndpoints.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgEndpoints.Appearance.Options.UseFont = True
        Me.cgEndpoints.AutoSizeByData = True
        Me.cgEndpoints.DisableAutoSize = False
        Me.cgEndpoints.DisableDataFormatting = False
        Me.cgEndpoints.FocusedRowHandle = -2147483648
        Me.cgEndpoints.HideFirstColumn = False
        Me.cgEndpoints.Location = New System.Drawing.Point(5, 24)
        Me.cgEndpoints.Name = "cgEndpoints"
        Me.cgEndpoints.PreviewColumn = ""
        Me.cgEndpoints.QueryID = Nothing
        Me.cgEndpoints.RowAutoHeight = False
        Me.cgEndpoints.SearchAsYouType = True
        Me.cgEndpoints.ShowAutoFilterRow = False
        Me.cgEndpoints.ShowFindPanel = True
        Me.cgEndpoints.ShowGroupByBox = False
        Me.cgEndpoints.ShowLoadingPanel = False
        Me.cgEndpoints.ShowNavigator = True
        Me.cgEndpoints.Size = New System.Drawing.Size(239, 354)
        Me.cgEndpoints.TabIndex = 2
        '
        'btnDist
        '
        Me.btnDist.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDist.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDist.Appearance.Options.UseFont = True
        Me.btnDist.CausesValidation = False
        Me.btnDist.Location = New System.Drawing.Point(5, 384)
        Me.btnDist.Name = "btnDist"
        Me.btnDist.Size = New System.Drawing.Size(125, 23)
        Me.btnDist.TabIndex = 15
        Me.btnDist.Tag = ""
        Me.btnDist.Text = "Send to Endpoints"
        '
        'cgJobs
        '
        Me.cgJobs.AllowBuildColumns = True
        Me.cgJobs.AllowEdit = False
        Me.cgJobs.AllowHorizontalScroll = False
        Me.cgJobs.AllowMultiSelect = False
        Me.cgJobs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgJobs.Appearance.Options.UseFont = True
        Me.cgJobs.AutoSizeByData = True
        Me.cgJobs.DisableAutoSize = False
        Me.cgJobs.DisableDataFormatting = False
        Me.cgJobs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgJobs.FocusedRowHandle = -2147483648
        Me.cgJobs.HideFirstColumn = False
        Me.cgJobs.Location = New System.Drawing.Point(2, 20)
        Me.cgJobs.Name = "cgJobs"
        Me.cgJobs.PreviewColumn = ""
        Me.cgJobs.QueryID = Nothing
        Me.cgJobs.RowAutoHeight = False
        Me.cgJobs.SearchAsYouType = True
        Me.cgJobs.ShowAutoFilterRow = False
        Me.cgJobs.ShowFindPanel = True
        Me.cgJobs.ShowGroupByBox = False
        Me.cgJobs.ShowLoadingPanel = False
        Me.cgJobs.ShowNavigator = True
        Me.cgJobs.Size = New System.Drawing.Size(296, 390)
        Me.cgJobs.TabIndex = 16
        '
        'btnImmediate
        '
        Me.btnImmediate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnImmediate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImmediate.Appearance.Options.UseFont = True
        Me.btnImmediate.CausesValidation = False
        Me.btnImmediate.Location = New System.Drawing.Point(136, 384)
        Me.btnImmediate.Name = "btnImmediate"
        Me.btnImmediate.Size = New System.Drawing.Size(276, 23)
        Me.btnImmediate.TabIndex = 17
        Me.btnImmediate.Tag = ""
        Me.btnImmediate.Text = "Send to Endpoint for Immediate Install"
        '
        'gbxCustomers
        '
        Me.gbxCustomers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbxCustomers.Controls.Add(Me.cgCustomers)
        Me.gbxCustomers.Location = New System.Drawing.Point(12, 12)
        Me.gbxCustomers.Name = "gbxCustomers"
        Me.gbxCustomers.Size = New System.Drawing.Size(275, 412)
        Me.gbxCustomers.TabIndex = 18
        Me.gbxCustomers.Text = "Customers"
        '
        'gbxJobs
        '
        Me.gbxJobs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbxJobs.Controls.Add(Me.cgJobs)
        Me.gbxJobs.Location = New System.Drawing.Point(674, 12)
        Me.gbxJobs.Name = "gbxJobs"
        Me.gbxJobs.Size = New System.Drawing.Size(300, 412)
        Me.gbxJobs.TabIndex = 19
        Me.gbxJobs.Text = "Jobs"
        '
        'gbxApps
        '
        Me.gbxApps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbxApps.Controls.Add(Me.cgApps)
        Me.gbxApps.Location = New System.Drawing.Point(293, 12)
        Me.gbxApps.Name = "gbxApps"
        Me.gbxApps.Size = New System.Drawing.Size(375, 412)
        Me.gbxApps.TabIndex = 20
        Me.gbxApps.Text = "Apps"
        '
        'gbxEndpoints
        '
        Me.gbxEndpoints.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxEndpoints.Controls.Add(Me.cgEndpoints)
        Me.gbxEndpoints.Controls.Add(Me.btnDist)
        Me.gbxEndpoints.Controls.Add(Me.btnImmediate)
        Me.gbxEndpoints.Location = New System.Drawing.Point(980, 12)
        Me.gbxEndpoints.Name = "gbxEndpoints"
        Me.gbxEndpoints.Size = New System.Drawing.Size(249, 412)
        Me.gbxEndpoints.TabIndex = 21
        Me.gbxEndpoints.Text = "Endpoints"
        '
        'frmDistribute
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1241, 436)
        Me.Controls.Add(Me.gbxEndpoints)
        Me.Controls.Add(Me.gbxApps)
        Me.Controls.Add(Me.gbxJobs)
        Me.Controls.Add(Me.gbxCustomers)
        Me.LoadMaximised = True
        Me.Name = "frmDistribute"
        Me.Text = "frmDistribute"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCustomers.ResumeLayout(False)
        CType(Me.gbxJobs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxJobs.ResumeLayout(False)
        CType(Me.gbxApps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxApps.ResumeLayout(False)
        CType(Me.gbxEndpoints, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEndpoints.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgCustomers As Care.Controls.CareGrid
    Friend WithEvents cgApps As Care.Controls.CareGrid
    Friend WithEvents cgEndpoints As Care.Controls.CareGrid
    Friend WithEvents btnDist As Care.Controls.CareButton
    Friend WithEvents cgJobs As Care.Controls.CareGrid
    Friend WithEvents btnImmediate As Care.Controls.CareButton
    Friend WithEvents gbxCustomers As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxJobs As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxApps As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxEndpoints As DevExpress.XtraEditors.GroupControl
End Class
