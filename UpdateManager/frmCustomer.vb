﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports UpdateShared.Business
Imports System.Windows.Forms
Imports System.IO

Public Class frmCustomer

    Private m_PrivateKey As String = ""
    Private m_Customer As Customer

    Private Sub frmSites_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxAppType.AddItem("Server")
        cbxAppType.AddItem("Service Host")
        cbxAppType.AddItem("Workstation")
        cbxAppType.AddItem("Touchscreen")
        cbxAppType.AddItem("Tablet")

        cbxEndpointStatus.AddItem("OK", "0")
        cbxEndpointStatus.AddItem("Update", "1")
        cbxEndpointStatus.AddItem("Revoke", "-1")
        cbxEndpointStatus.AddItem("Disabled", "-99")

        m_PrivateKey = File.ReadAllText(Environment.CurrentDirectory + "\licenses\privateKey.xml")

        gbxApp.Hide()
        gbxEndpoint.Hide()

    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Customer = New Customer
        bs.DataSource = m_Customer

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtUrlScreenconnect.DataBindings.Add("Text", bs, "_UrlScreenconnect")
        chkActive.DataBindings.Add("Checked", bs, "_Active")
        txtLastCheckIn.DataBindings.Add("Text", bs, "_LastCheckin")
        txtLastUpdate.DataBindings.Add("Text", bs, "_LastUpdate")

        txtAccountNumber.DataBindings.Add("Text", bs, "_SfAccount")
        txtUrlSalesforce.DataBindings.Add("Text", bs, "_UrlSalesforce")
        txtProduct.DataBindings.Add("Text", bs, "_SfProduct")
        'cdtExpires.DataBindings.Add("EditValue", bs, "_SfLicenseExp")
        txtLastSync.DataBindings.Add("Text", bs, "_LastUpdate")

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If InputBox("Type Delete to Continue", "Delete Customer").ToUpper = "DELETE" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        'delete endpoint jobs
        Dim _Endpoints As List(Of Endpoint) = Endpoint.RetreiveByCustomer(Session.ConnectionString, m_Customer._ID.Value)
        For Each _e In _Endpoints
            _SQL = "delete from EndpointJobs where endpoint_id = '" + _e._ID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        Next

        'delete endpoints
        _SQL = "delete from Endpoints where customer_id = '" + m_Customer._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        'delete apps
        _SQL = "delete from CustomerApps where customer_id = '" + m_Customer._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        'delete customer
        _SQL = "delete from Customers where ID = '" + m_Customer._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Customer = CType(bs.Item(bs.Position), Customer)
        m_Customer.Store()

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name', active as 'Active' from Customers"

        With _Find
            .Caption = "Find Customer"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Customer = Customer.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Customer

        End If

    End Sub

    Protected Overrides Sub AfterAdd()
        MyBase.AfterAdd()
        txtName.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()

        MyBase.AfterEdit()
        txtName.Focus()

        Select Case tcCustomer.SelectedTabPage.Name

            Case "tabApplications"
                cgApps.ButtonsEnabled = True

            Case "tabEndpoints"
                cgEndpoints.ButtonsEnabled = True

        End Select

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        cgApps.ButtonsEnabled = False
        cgEndpoints.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cgApps.ButtonsEnabled = False
        cgEndpoints.ButtonsEnabled = False
    End Sub

#End Region

    Private Sub cgApps_AddClick(sender As Object, e As EventArgs) Handles cgApps.AddClick
        ShowApp("")
    End Sub

    Private Sub cgApps_EditClick(sender As Object, e As EventArgs) Handles cgApps.EditClick
        Dim _ID As Guid? = ReturnDialogID(cgApps)
        If _ID.HasValue Then
            ShowApp(_ID.Value.ToString)
        End If
    End Sub

    Private Sub cgApps_RemoveClick(sender As Object, e As EventArgs) Handles cgApps.RemoveClick

    End Sub

    Private Sub ShowApp(ByVal ID As String)

        gbxApp.Tag = ID

        If ID <> "" Then
            txtAppName.Text = cgApps.CurrentRow("app_name").ToString
            cbxAppType.Text = cgApps.CurrentRow("app_type").ToString
            txtAppPath.Text = cgApps.CurrentRow("app_path").ToString
            txtAppServer.Text = cgApps.CurrentRow("app_server").ToString
            txtConnString.Text = cgApps.CurrentRow("app_cs").ToString
        Else
            txtAppName.Text = ""
            cbxAppType.SelectedIndex = -1
            txtAppPath.Text = ""
            txtAppServer.Text = ""
            txtConnString.Text = ""
        End If

        gbxApp.Show()
        txtAppName.Focus()

    End Sub

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGrid) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGridWithButtons) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Sub btnAppOK_Click(sender As Object, e As EventArgs) Handles btnAppOK.Click

        Dim _App As New CustomerApp
        If gbxApp.Tag.ToString <> "" Then
            _App = CustomerApp.RetreiveByID(New Guid(gbxApp.Tag.ToString))
        Else
            _App._CustomerId = m_Customer._ID
        End If

        _App._AppName = txtAppName.Text
        _App._AppPath = txtAppPath.Text
        _App._AppServer = txtAppServer.Text
        _App._AppCs = txtConnString.Text
        _App._AppType = cbxAppType.Text

        _App.Store()

        DisplayApps()

        gbxApp.Hide()

    End Sub

    Private Sub btnAppCancel_Click(sender As Object, e As EventArgs) Handles btnAppCancel.Click
        gbxApp.Hide()
    End Sub

    Private Sub tcCustomer_Selected(sender As Object, e As DevExpress.XtraTab.TabPageEventArgs) Handles tcCustomer.Selected

        If m_Customer Is Nothing Then Exit Sub
        Select Case e.Page.Name

            Case "tabCustomer"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabApplications"
                DisplayApps()
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabEndpoints"
                DisplayEndpoints()
                Me.ToolbarMode = ToolbarEnum.FindandEdit

        End Select

    End Sub

    Private Sub DisplayApps()

        Dim _SQL As String = ""

        _SQL += "select ID, app_type, app_name, app_server, app_path, app_cs from CustomerApps"
        _SQL += " where customer_id = '" + m_Customer._ID.Value.ToString + "'"
        _SQL += " order by app_name"

        cgApps.HideFirstColumn = True
        cgApps.Populate(Session.ConnectionString, _SQL)

        cgApps.Columns("app_name").Caption = "Name"
        cgApps.Columns("app_type").Caption = "Type"
        cgApps.Columns("app_server").Caption = "Server"
        cgApps.Columns("app_path").Visible = False
        cgApps.Columns("app_cs").Visible = False

    End Sub

    Private Sub DisplayEndpoints()

        Dim _SQL As String = ""

        _SQL += "select ID, computer_name, license_status, license_expires,"
        _SQL += " allow_workstation, allow_db, allow_service, allow_touchscreen, allow_tablet, last_checkin from Endpoints"
        _SQL += " where customer_id = '" + m_Customer._ID.Value.ToString + "'"
        _SQL += " order by computer_name"

        cgEndpoints.HideFirstColumn = True
        cgEndpoints.Populate(Session.ConnectionString, _SQL)

        cgEndpoints.Columns("computer_name").Caption = "Computer Name"
        cgEndpoints.Columns("license_status").Caption = "Status"
        cgEndpoints.Columns("license_expires").Caption = "Expires"
        cgEndpoints.Columns("allow_workstation").Caption = "Workstation?"
        cgEndpoints.Columns("allow_db").Caption = "DB?"
        cgEndpoints.Columns("allow_service").Caption = "Service?"
        cgEndpoints.Columns("allow_touchscreen").Caption = "Touchscreen?"
        cgEndpoints.Columns("allow_tablet").Caption = "Tablet?"

        cgEndpoints.Columns("last_checkin").Caption = "Last Checkin"
        cgEndpoints.Columns("last_checkin").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgEndpoints.Columns("last_checkin").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        cgEndpoints.AutoSizeColumns()

    End Sub

    Private Sub btnCopyID_Click(sender As Object, e As EventArgs) Handles btnCopyID.Click
        Clipboard.SetText(m_Customer._ID.Value.ToString)
    End Sub

    Private Sub btnSalesforce_Click(sender As Object, e As EventArgs) Handles btnSalesforce.Click
        If txtUrlSalesforce.Text = "" Then Exit Sub
        Process.Start(txtUrlSalesforce.Text)
    End Sub

    Private Sub btnScreenConnect_Click(sender As Object, e As EventArgs) Handles btnScreenConnect.Click
        If txtUrlScreenconnect.Text = "" Then Exit Sub
        Process.Start(txtUrlScreenconnect.Text)
    End Sub

    Private Sub btnEndpoints_Click(sender As Object, e As EventArgs) Handles btnEndpoints.Click

        If cdtExpires.Value Is Nothing Then Exit Sub
        If Not cdtExpires.Value.HasValue Then Exit Sub

        btnEndpoints.Enabled = False
        Session.CursorWaiting()

        Dim _EndPoints As List(Of Endpoint) = UpdateShared.Business.Endpoint.RetreiveByCustomer(Session.ConnectionString, m_Customer._ID.Value)
        If _EndPoints IsNot Nothing Then

            If _EndPoints.Count > 0 Then

                Session.SetupProgressBar("Generating Licenses...", _EndPoints.Count)

                For Each _e As Endpoint In _EndPoints
                    _e._LicenseExpires = cdtExpires.Value.Value
                    _e._LicenseStatus = 1
                    _e._LicenseData = Licensing.ReturnLicense(m_PrivateKey, m_Customer._Name, m_Customer._SfProduct, _e._LicenseExpires.Value, _e._ComputerName)
                    _e.Store()
                    Session.StepProgressBar()
                Next

            End If

        End If

        btnEndpoints.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub cgEndpoints_AddClick(sender As Object, e As EventArgs) Handles cgEndpoints.AddClick
        CareMessage("You cannot add an Endpoint from here - You must deploy the Update Client on the Customers machine.", MessageBoxIcon.Exclamation, "Add Endpoint")
    End Sub

    Private Sub cgEndpoints_EditClick(sender As Object, e As EventArgs) Handles cgEndpoints.EditClick
        Dim _ID As Guid? = ReturnDialogID(cgEndpoints)
        If _ID.HasValue Then
            ShowEndpoint(_ID.Value.ToString)
        End If
    End Sub

    Private Sub ShowEndpoint(ByVal ID As String)

        gbxEndpoint.Tag = ID

        If ID <> "" Then

            txtEndpointComputerName.Text = cgEndpoints.CurrentRow("computer_name").ToString
            cbxEndpointStatus.SelectedValue = cgEndpoints.CurrentRow("license_status").ToString
            cdtEndpointExpires.Value = ValueHandler.ConvertDate(cgEndpoints.CurrentRow("license_expires"))

            chkWorkstation.Checked = ValueHandler.ConvertBoolean(cgEndpoints.CurrentRow("allow_workstation"))
            chkDatabase.Checked = ValueHandler.ConvertBoolean(cgEndpoints.CurrentRow("allow_db"))
            chkService.Checked = ValueHandler.ConvertBoolean(cgEndpoints.CurrentRow("allow_service"))
            chkTouchscreen.Checked = ValueHandler.ConvertBoolean(cgEndpoints.CurrentRow("allow_touchscreen"))
            chkTablet.Checked = ValueHandler.ConvertBoolean(cgEndpoints.CurrentRow("allow_tablet"))

            gbxEndpoint.Show()
            cbxEndpointStatus.Focus()

        End If

    End Sub

    Private Sub btnEndpointCancel_Click(sender As Object, e As EventArgs) Handles btnEndpointCancel.Click
        gbxEndpoint.Hide()
    End Sub

    Private Sub btnEndpointOK_Click(sender As Object, e As EventArgs) Handles btnEndpointOK.Click

        Dim _EndPoint As New Endpoint
        If gbxEndpoint.Tag.ToString <> "" Then

            _EndPoint = Endpoint.RetreiveByID(New Guid(gbxEndpoint.Tag.ToString))

            _EndPoint._LicenseStatus = CInt(cbxEndpointStatus.SelectedValue)
            _EndPoint._LicenseExpires = cdtEndpointExpires.Value
            _EndPoint._LicenseData = Licensing.ReturnLicense(m_PrivateKey, m_Customer._Name, m_Customer._SfProduct, _EndPoint._LicenseExpires, _EndPoint._ComputerName.ToUpper)

            _EndPoint._AllowWorkstation = chkWorkstation.Checked
            _EndPoint._AllowDb = chkDatabase.Checked
            _EndPoint._AllowService = chkService.Checked
            _EndPoint._AllowTouchscreen = chkTouchscreen.Checked
            _EndPoint._AllowTablet = chkTablet.Checked

            _EndPoint.Store()

            DisplayEndpoints()

            gbxEndpoint.Hide()

        End If

    End Sub
End Class
