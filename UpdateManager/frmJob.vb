﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports UpdateShared.Business
Imports System.Windows.Forms

Public Class frmJob

    Private m_Job As Job

    Private Sub frmSites_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxStepType.AddItem("Create Folder")
        cbxStepType.AddItem("Download File")
        cbxStepType.AddItem("SQL Script")
        cbxStepType.AddItem("Process Pack")
        cbxStepType.AddItem("Run Batch File")
        cbxStepType.AddItem("Kill Process")
        cbxStepType.AddItem("Start Service")
        cbxStepType.AddItem("Stop Service")
        cbxStepType.AddItem("Write File")
        cbxStepType.AddItem("Copy File")
        cbxStepType.AddItem("Move File")

        gbxStep.Hide()

    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Job = New Job
        bs.DataSource = m_Job

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtSeq.DataBindings.Add("Text", bs, "_Seq")

        chkEnabled.DataBindings.Add("Checked", bs, "_Enabled")

        chkWks.DataBindings.Add("Checked", bs, "_TargetWorkstation")
        chkDB.DataBindings.Add("Checked", bs, "_TargetDb")
        chkService.DataBindings.Add("Checked", bs, "_TargetService")
        chkTS.DataBindings.Add("Checked", bs, "_TargetTouchscreen")
        chkTablet.DataBindings.Add("Checked", bs, "_TargetTablet")

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If InputBox("Type Delete to Continue", "Delete Job").ToUpper = "DELETE" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        'delete steps
        _SQL = "delete from JobSteps where customer_id = '" + m_Job._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        'delete job
        _SQL = "delete from Jobs where ID = '" + m_Job._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub CommitUpdate()
        m_Job = CType(bs.Item(bs.Position), Job)
        m_Job.Store()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name', enabled as 'Enabled',"
        _SQL += " target_workstation as 'Workstation', target_db as 'Database', target_service as 'Service',"
        _SQL += " target_touchscreen as 'Touchscreen', target_tablet as 'Offline Tablet'"
        _SQL += " from Jobs"

        With _Find
            .Caption = "Find Job"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridWhereClause = ""
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .FormWidth = 900
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Job = Job.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Job

            DisplayRecord()

        End If

    End Sub

    Protected Overrides Sub AfterAdd()
        MyBase.AfterAdd()
        txtName.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()

        MyBase.AfterEdit()
        txtName.Focus()

        If tcMain.SelectedTabPage.Name = "tabSteps" Then
            cgSteps.ButtonsEnabled = True
        End If

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        cgSteps.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cgSteps.ButtonsEnabled = False
    End Sub

#End Region

    Private Sub cgSteps_AddClick(sender As Object, e As EventArgs) Handles cgSteps.AddClick
        ShowStep("")
    End Sub

    Private Sub cgSteps_EditClick(sender As Object, e As EventArgs) Handles cgSteps.EditClick
        Dim _ID As Guid? = ReturnDialogID(cgSteps)
        If _ID.HasValue Then
            ShowStep(_ID.Value.ToString)
        End If
    End Sub

    Private Sub cgSteps_RemoveClick(sender As Object, e As EventArgs) Handles cgSteps.RemoveClick
        Dim _ID As Guid? = ReturnDialogID(cgSteps)
        If _ID.HasValue Then
            Dim _SQL As String = "delete from JobSteps where ID = '" + _ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
            DisplaySteps()
        End If
    End Sub

    Private Sub ShowStep(ByVal ID As String)

        gbxStep.Tag = ID

        If ID <> "" Then
            txtStepName.Text = cgSteps.CurrentRow("name").ToString
            cbxStepType.Text = cgSteps.CurrentRow("type").ToString
            txtArg1.Text = cgSteps.CurrentRow("arg_1").ToString
            txtArg2.Text = cgSteps.CurrentRow("arg_2").ToString
            txtArg3.Text = cgSteps.CurrentRow("arg_3").ToString
            txtMemo.Text = cgSteps.CurrentRow("memo").ToString
            txtStepSeq.Text = cgSteps.CurrentRow("seq").ToString
        Else
            txtStepName.Text = ""
            cbxStepType.SelectedIndex = -1
            txtArg1.Text = ""
            txtArg2.Text = ""
            txtArg3.Text = ""
            txtMemo.Text = ""
            txtStepSeq.Text = ""
        End If

        gbxStep.Show()
        txtStepName.Focus()

    End Sub

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGrid) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGridWithButtons) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Sub btnStepOK_Click(sender As Object, e As EventArgs) Handles btnStepOK.Click

        Dim _Step As New JobStep
        If gbxStep.Tag.ToString <> "" Then
            _Step = JobStep.RetreiveByID(New Guid(gbxStep.Tag.ToString))
        Else
            _Step._JobId = m_Job._ID
        End If

        _Step._Name = txtStepName.Text
        _Step._Type = cbxStepType.Text

        _Step._Arg1 = txtArg1.Text
        _Step._Arg2 = txtArg2.Text
        _Step._Arg3 = txtArg3.Text

        _Step._Memo = txtMemo.Text
        _Step._Seq = ValueHandler.ConvertInteger(txtStepSeq.Text)

        _Step.Store()

        DisplaySteps()

        gbxStep.Hide()

    End Sub

    Private Sub btnStepCancel_Click(sender As Object, e As EventArgs) Handles btnStepCancel.Click
        gbxStep.Hide()
    End Sub

    Private Sub DisplayRecord()

        If m_Job Is Nothing Then Exit Sub
        Select Case tcMain.SelectedTabPage.Name

            Case "tabCustomer"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabSteps"
                DisplaySteps()
                Me.ToolbarMode = ToolbarEnum.FindandEdit

        End Select

    End Sub

    Private Sub DisplaySteps()

        Dim _SQL As String = ""

        _SQL += "select * from JobSteps"
        _SQL += " where job_id = '" + m_Job._ID.ToString + "'"
        _SQL += " order by seq"

        cgSteps.HideFirstColumn = True
        cgSteps.Populate(Session.ConnectionString, _SQL)

        cgSteps.Columns("ID").Visible = False
        cgSteps.Columns("job_id").Visible = False

        cgSteps.Columns("name").Caption = "Name"
        cgSteps.Columns("type").Caption = "Type"

        cgSteps.Columns("arg_1").Caption = "Arg 1"
        cgSteps.Columns("arg_2").Caption = "Arg 2"
        cgSteps.Columns("arg_3").Visible = False

        cgSteps.Columns("memo").Visible = False

        cgSteps.Columns("seq").Caption = "Seq"

    End Sub

    Private Sub btnImportStep_Click(sender As Object, e As EventArgs) Handles btnImportStep.Click

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name', type as 'Type' from JobSteps"

        With _Find
            .Caption = "Find tep"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then
            Dim _ID As Guid = New Guid(_Find.ReturnValue)
            Dim _S As JobStep = JobStep.RetreiveByID(_ID)
            If _S IsNot Nothing Then
                txtStepName.Text = "Copy of " + _S._Name
                txtStepSeq.Text = _S._Seq.ToString
                txtArg1.Text = _S._Arg1
                txtArg2.Text = _S._Arg2
                txtArg3.Text = _S._Arg3
                txtMemo.Text = _S._Memo
            End If
        End If

    End Sub

    Private Sub tcMain_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tcMain.SelectedPageChanged
        DisplayRecord()
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Try
            Process.Start("\\cs-api\packs")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnOpen_Click(sender As Object, e As EventArgs) Handles btnOpen.Click
        Try
            Process.Start("\\cs-api\packs\" + m_Job._ID.Value.ToString + ".zip")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCopyID_Click(sender As Object, e As EventArgs) Handles btnCopyID.Click
        Try
            Clipboard.SetText(m_Job._ID.Value.ToString)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCopyJob_Click(sender As Object, e As EventArgs) Handles btnCopyJob.Click

        Dim _Name As String = InputBox("Enter the Name of the New Job", "Copy Job", txtName.Text)
        If _Name = "" Then Exit Sub

        Dim _J As New Job
        With _J
            ._Name = _Name
            ._Enabled = True
            ._Seq = m_Job._Seq
            ._TargetWorkstation = m_Job._TargetWorkstation
            ._TargetDb = m_Job._TargetDb
            ._TargetService = m_Job._TargetService
            ._TargetTouchscreen = m_Job._TargetTouchscreen
            ._TargetTablet = m_Job._TargetTablet
            .Store()
        End With

        For Each _S In JobStep.RetrieveByJobID(m_Job._ID)
            Dim _NS As New JobStep
            With _NS
                ._JobId = _J._ID
                ._Name = _S._Name
                ._Type = _S._Type
                ._Arg1 = _S._Arg1
                ._Arg2 = _S._Arg2
                ._Arg3 = _S._Arg3
                ._Memo = _S._Memo
                ._Seq = _S._Seq
                .Store()
            End With
        Next

        CareMessage("Job Copied.", MessageBoxIcon.Information, "Copy Job")

    End Sub
End Class
