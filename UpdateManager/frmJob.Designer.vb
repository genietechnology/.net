﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJob
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.tcMain = New Care.Controls.CareTab()
        Me.tabJob = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnCopyID = New Care.Controls.CareButton()
        Me.btnOpen = New Care.Controls.CareButton()
        Me.btnBrowse = New Care.Controls.CareButton()
        Me.chkEnabled = New Care.Controls.CareCheckBox()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtSeq = New Care.Controls.CareTextBox()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.tabSteps = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxStep = New DevExpress.XtraEditors.GroupControl()
        Me.btnImportStep = New Care.Controls.CareButton()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.cbxStepType = New Care.Controls.CareComboBox()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.txtArg3 = New Care.Controls.CareTextBox()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.txtArg2 = New Care.Controls.CareTextBox()
        Me.txtMemo = New DevExpress.XtraEditors.MemoEdit()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.txtArg1 = New Care.Controls.CareTextBox()
        Me.txtStepSeq = New Care.Controls.CareTextBox()
        Me.txtStepName = New Care.Controls.CareTextBox()
        Me.btnStepCancel = New Care.Controls.CareButton()
        Me.btnStepOK = New Care.Controls.CareButton()
        Me.CareLabel30 = New Care.Controls.CareLabel()
        Me.CareLabel32 = New Care.Controls.CareLabel()
        Me.cgSteps = New Care.Controls.CareGridWithButtons()
        Me.btnCopyJob = New Care.Controls.CareButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.chkTablet = New Care.Controls.CareCheckBox()
        Me.chkTS = New Care.Controls.CareCheckBox()
        Me.chkService = New Care.Controls.CareCheckBox()
        Me.chkDB = New Care.Controls.CareCheckBox()
        Me.chkWks = New Care.Controls.CareCheckBox()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tcMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcMain.SuspendLayout()
        Me.tabJob.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkEnabled.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSteps.SuspendLayout()
        CType(Me.gbxStep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxStep.SuspendLayout()
        CType(Me.cbxStepType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArg3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArg2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMemo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArg1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStepSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStepName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkTablet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkService.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWks.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(777, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(686, 3)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCopyJob)
        Me.Panel1.Location = New System.Drawing.Point(0, 585)
        Me.Panel1.Size = New System.Drawing.Size(884, 36)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCopyJob, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tcMain
        '
        Me.tcMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcMain.Location = New System.Drawing.Point(12, 53)
        Me.tcMain.Name = "tcMain"
        Me.tcMain.SelectedTabPage = Me.tabJob
        Me.tcMain.Size = New System.Drawing.Size(867, 529)
        Me.tcMain.TabIndex = 0
        Me.tcMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabJob, Me.tabSteps})
        '
        'tabJob
        '
        Me.tabJob.Controls.Add(Me.GroupControl2)
        Me.tabJob.Controls.Add(Me.GroupControl1)
        Me.tabJob.Name = "tabJob"
        Me.tabJob.Size = New System.Drawing.Size(861, 501)
        Me.tabJob.Text = "General"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnCopyID)
        Me.GroupControl1.Controls.Add(Me.btnOpen)
        Me.GroupControl1.Controls.Add(Me.btnBrowse)
        Me.GroupControl1.Controls.Add(Me.chkEnabled)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtSeq)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 7)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(844, 113)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Job Details"
        '
        'btnCopyID
        '
        Me.btnCopyID.Location = New System.Drawing.Point(585, 29)
        Me.btnCopyID.Name = "btnCopyID"
        Me.btnCopyID.Size = New System.Drawing.Size(75, 23)
        Me.btnCopyID.TabIndex = 8
        Me.btnCopyID.Text = "Copy ID"
        '
        'btnOpen
        '
        Me.btnOpen.Location = New System.Drawing.Point(504, 29)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(75, 23)
        Me.btnOpen.TabIndex = 7
        Me.btnOpen.Text = "Open"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(423, 29)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 6
        Me.btnBrowse.Text = "Browse"
        '
        'chkEnabled
        '
        Me.chkEnabled.EnterMoveNextControl = True
        Me.chkEnabled.Location = New System.Drawing.Point(123, 87)
        Me.chkEnabled.Name = "chkEnabled"
        Me.chkEnabled.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEnabled.Properties.Appearance.Options.UseFont = True
        Me.chkEnabled.Properties.Caption = ""
        Me.chkEnabled.Size = New System.Drawing.Size(19, 19)
        Me.chkEnabled.TabIndex = 5
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(13, 89)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Enabled"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(13, 61)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Run Sequence"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSeq
        '
        Me.txtSeq.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSeq.EnterMoveNextControl = True
        Me.txtSeq.Location = New System.Drawing.Point(125, 58)
        Me.txtSeq.MaxLength = 3
        Me.txtSeq.Name = "txtSeq"
        Me.txtSeq.NumericAllowNegatives = False
        Me.txtSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSeq.Properties.Appearance.Options.UseFont = True
        Me.txtSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSeq.Properties.Mask.EditMask = "##########;"
        Me.txtSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSeq.Properties.MaxLength = 3
        Me.txtSeq.Size = New System.Drawing.Size(68, 22)
        Me.txtSeq.TabIndex = 3
        Me.txtSeq.Tag = "AE"
        Me.txtSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSeq.ToolTipText = ""
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(125, 30)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Size = New System.Drawing.Size(292, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(13, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSteps
        '
        Me.tabSteps.Controls.Add(Me.gbxStep)
        Me.tabSteps.Controls.Add(Me.cgSteps)
        Me.tabSteps.Name = "tabSteps"
        Me.tabSteps.Size = New System.Drawing.Size(861, 501)
        Me.tabSteps.Text = "Job Steps"
        '
        'gbxStep
        '
        Me.gbxStep.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxStep.Controls.Add(Me.btnImportStep)
        Me.gbxStep.Controls.Add(Me.CareLabel8)
        Me.gbxStep.Controls.Add(Me.cbxStepType)
        Me.gbxStep.Controls.Add(Me.CareLabel7)
        Me.gbxStep.Controls.Add(Me.txtArg3)
        Me.gbxStep.Controls.Add(Me.CareLabel6)
        Me.gbxStep.Controls.Add(Me.txtArg2)
        Me.gbxStep.Controls.Add(Me.txtMemo)
        Me.gbxStep.Controls.Add(Me.CareLabel4)
        Me.gbxStep.Controls.Add(Me.CareLabel5)
        Me.gbxStep.Controls.Add(Me.txtArg1)
        Me.gbxStep.Controls.Add(Me.txtStepSeq)
        Me.gbxStep.Controls.Add(Me.txtStepName)
        Me.gbxStep.Controls.Add(Me.btnStepCancel)
        Me.gbxStep.Controls.Add(Me.btnStepOK)
        Me.gbxStep.Controls.Add(Me.CareLabel30)
        Me.gbxStep.Controls.Add(Me.CareLabel32)
        Me.gbxStep.Location = New System.Drawing.Point(17, 21)
        Me.gbxStep.Name = "gbxStep"
        Me.gbxStep.Size = New System.Drawing.Size(828, 436)
        Me.gbxStep.TabIndex = 0
        '
        'btnImportStep
        '
        Me.btnImportStep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnImportStep.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImportStep.Appearance.Options.UseFont = True
        Me.btnImportStep.CausesValidation = False
        Me.btnImportStep.Location = New System.Drawing.Point(12, 405)
        Me.btnImportStep.Name = "btnImportStep"
        Me.btnImportStep.Size = New System.Drawing.Size(125, 23)
        Me.btnImportStep.TabIndex = 16
        Me.btnImportStep.Tag = "AE"
        Me.btnImportStep.Text = "Import Existing"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(388, 58)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel8.TabIndex = 4
        Me.CareLabel8.Text = "Display Seq"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStepType
        '
        Me.cbxStepType.AllowBlank = False
        Me.cbxStepType.DataSource = Nothing
        Me.cbxStepType.DisplayMember = Nothing
        Me.cbxStepType.EnterMoveNextControl = True
        Me.cbxStepType.Location = New System.Drawing.Point(103, 55)
        Me.cbxStepType.Name = "cbxStepType"
        Me.cbxStepType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStepType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxStepType.Properties.Appearance.Options.UseFont = True
        Me.cbxStepType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStepType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStepType.SelectedValue = Nothing
        Me.cbxStepType.Size = New System.Drawing.Size(231, 22)
        Me.cbxStepType.TabIndex = 3
        Me.cbxStepType.Tag = "AEM"
        Me.cbxStepType.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(12, 136)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel7.TabIndex = 10
        Me.CareLabel7.Text = "Arg 3"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel7.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'txtArg3
        '
        Me.txtArg3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtArg3.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtArg3.EnterMoveNextControl = True
        Me.txtArg3.Location = New System.Drawing.Point(103, 133)
        Me.txtArg3.MaxLength = 1024
        Me.txtArg3.Name = "txtArg3"
        Me.txtArg3.NumericAllowNegatives = False
        Me.txtArg3.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtArg3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtArg3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtArg3.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArg3.Properties.Appearance.Options.UseFont = True
        Me.txtArg3.Properties.Appearance.Options.UseTextOptions = True
        Me.txtArg3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtArg3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtArg3.Properties.MaxLength = 1024
        Me.txtArg3.Size = New System.Drawing.Size(711, 20)
        Me.txtArg3.TabIndex = 11
        Me.txtArg3.Tag = "AE"
        Me.txtArg3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtArg3.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 110)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel6.TabIndex = 8
        Me.CareLabel6.Text = "Arg 2"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel6.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'txtArg2
        '
        Me.txtArg2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtArg2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtArg2.EnterMoveNextControl = True
        Me.txtArg2.Location = New System.Drawing.Point(103, 107)
        Me.txtArg2.MaxLength = 1024
        Me.txtArg2.Name = "txtArg2"
        Me.txtArg2.NumericAllowNegatives = False
        Me.txtArg2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtArg2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtArg2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtArg2.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArg2.Properties.Appearance.Options.UseFont = True
        Me.txtArg2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtArg2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtArg2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtArg2.Properties.MaxLength = 1024
        Me.txtArg2.Size = New System.Drawing.Size(711, 20)
        Me.txtArg2.TabIndex = 9
        Me.txtArg2.Tag = "AE"
        Me.txtArg2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtArg2.ToolTipText = ""
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(12, 182)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemo.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMemo, True)
        Me.txtMemo.Size = New System.Drawing.Size(802, 217)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMemo, OptionsSpelling2)
        Me.txtMemo.TabIndex = 13
        Me.txtMemo.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 84)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Arg 1"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel4.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 161)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(82, 15)
        Me.CareLabel5.TabIndex = 12
        Me.CareLabel5.Text = "Command Text"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtArg1
        '
        Me.txtArg1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtArg1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtArg1.EnterMoveNextControl = True
        Me.txtArg1.Location = New System.Drawing.Point(103, 81)
        Me.txtArg1.MaxLength = 1024
        Me.txtArg1.Name = "txtArg1"
        Me.txtArg1.NumericAllowNegatives = False
        Me.txtArg1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtArg1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtArg1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtArg1.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArg1.Properties.Appearance.Options.UseFont = True
        Me.txtArg1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtArg1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtArg1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtArg1.Properties.MaxLength = 1024
        Me.txtArg1.Size = New System.Drawing.Size(711, 20)
        Me.txtArg1.TabIndex = 7
        Me.txtArg1.Tag = "AE"
        Me.txtArg1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtArg1.ToolTipText = ""
        '
        'txtStepSeq
        '
        Me.txtStepSeq.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStepSeq.EnterMoveNextControl = True
        Me.txtStepSeq.Location = New System.Drawing.Point(454, 55)
        Me.txtStepSeq.MaxLength = 4
        Me.txtStepSeq.Name = "txtStepSeq"
        Me.txtStepSeq.NumericAllowNegatives = False
        Me.txtStepSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtStepSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStepSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStepSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStepSeq.Properties.Appearance.Options.UseFont = True
        Me.txtStepSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStepSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStepSeq.Properties.Mask.EditMask = "##########;"
        Me.txtStepSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStepSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStepSeq.Properties.MaxLength = 4
        Me.txtStepSeq.Size = New System.Drawing.Size(85, 22)
        Me.txtStepSeq.TabIndex = 5
        Me.txtStepSeq.Tag = "AEM"
        Me.txtStepSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStepSeq.ToolTipText = ""
        '
        'txtStepName
        '
        Me.txtStepName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStepName.EnterMoveNextControl = True
        Me.txtStepName.Location = New System.Drawing.Point(103, 29)
        Me.txtStepName.MaxLength = 0
        Me.txtStepName.Name = "txtStepName"
        Me.txtStepName.NumericAllowNegatives = False
        Me.txtStepName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStepName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStepName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStepName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStepName.Properties.Appearance.Options.UseFont = True
        Me.txtStepName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStepName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStepName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStepName.Size = New System.Drawing.Size(436, 22)
        Me.txtStepName.TabIndex = 1
        Me.txtStepName.Tag = "AEM"
        Me.txtStepName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStepName.ToolTipText = ""
        '
        'btnStepCancel
        '
        Me.btnStepCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStepCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnStepCancel.Appearance.Options.UseFont = True
        Me.btnStepCancel.CausesValidation = False
        Me.btnStepCancel.Location = New System.Drawing.Point(729, 405)
        Me.btnStepCancel.Name = "btnStepCancel"
        Me.btnStepCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnStepCancel.TabIndex = 15
        Me.btnStepCancel.Text = "Cancel"
        '
        'btnStepOK
        '
        Me.btnStepOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStepOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnStepOK.Appearance.Options.UseFont = True
        Me.btnStepOK.CausesValidation = False
        Me.btnStepOK.Location = New System.Drawing.Point(638, 405)
        Me.btnStepOK.Name = "btnStepOK"
        Me.btnStepOK.Size = New System.Drawing.Size(85, 23)
        Me.btnStepOK.TabIndex = 14
        Me.btnStepOK.Tag = "AE"
        Me.btnStepOK.Text = "OK"
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel30.TabIndex = 0
        Me.CareLabel30.Text = "Name"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel30.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " & _
    "purposes."
        Me.CareLabel30.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(12, 58)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel32.TabIndex = 2
        Me.CareLabel32.Text = "Type"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgSteps
        '
        Me.cgSteps.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgSteps.ButtonsEnabled = False
        Me.cgSteps.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgSteps.HideFirstColumn = False
        Me.cgSteps.Location = New System.Drawing.Point(3, 3)
        Me.cgSteps.Name = "cgSteps"
        Me.cgSteps.PreviewColumn = ""
        Me.cgSteps.Size = New System.Drawing.Size(855, 495)
        Me.cgSteps.TabIndex = 1
        '
        'btnCopyJob
        '
        Me.btnCopyJob.Location = New System.Drawing.Point(12, 3)
        Me.btnCopyJob.Name = "btnCopyJob"
        Me.btnCopyJob.Size = New System.Drawing.Size(150, 25)
        Me.btnCopyJob.TabIndex = 7
        Me.btnCopyJob.Text = "Copy Job"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.chkTablet)
        Me.GroupControl2.Controls.Add(Me.chkTS)
        Me.GroupControl2.Controls.Add(Me.chkService)
        Me.GroupControl2.Controls.Add(Me.chkDB)
        Me.GroupControl2.Controls.Add(Me.chkWks)
        Me.GroupControl2.Location = New System.Drawing.Point(8, 126)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(844, 53)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Target Endpoint Type"
        '
        'chkTablet
        '
        Me.chkTablet.EnterMoveNextControl = True
        Me.chkTablet.Location = New System.Drawing.Point(339, 26)
        Me.chkTablet.Name = "chkTablet"
        Me.chkTablet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTablet.Properties.Appearance.Options.UseFont = True
        Me.chkTablet.Properties.AutoWidth = True
        Me.chkTablet.Properties.Caption = "Offline Tablet"
        Me.chkTablet.Size = New System.Drawing.Size(94, 19)
        Me.chkTablet.TabIndex = 4
        Me.chkTablet.Tag = "AE"
        '
        'chkTS
        '
        Me.chkTS.EnterMoveNextControl = True
        Me.chkTS.Location = New System.Drawing.Point(243, 26)
        Me.chkTS.Name = "chkTS"
        Me.chkTS.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTS.Properties.Appearance.Options.UseFont = True
        Me.chkTS.Properties.AutoWidth = True
        Me.chkTS.Properties.Caption = "Touchscreen"
        Me.chkTS.Size = New System.Drawing.Size(90, 19)
        Me.chkTS.TabIndex = 3
        Me.chkTS.Tag = "AE"
        '
        'chkService
        '
        Me.chkService.EnterMoveNextControl = True
        Me.chkService.Location = New System.Drawing.Point(178, 26)
        Me.chkService.Name = "chkService"
        Me.chkService.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkService.Properties.Appearance.Options.UseFont = True
        Me.chkService.Properties.AutoWidth = True
        Me.chkService.Properties.Caption = "Service"
        Me.chkService.Size = New System.Drawing.Size(59, 19)
        Me.chkService.TabIndex = 2
        Me.chkService.Tag = "AE"
        '
        'chkDB
        '
        Me.chkDB.EnterMoveNextControl = True
        Me.chkDB.Location = New System.Drawing.Point(102, 26)
        Me.chkDB.Name = "chkDB"
        Me.chkDB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDB.Properties.Appearance.Options.UseFont = True
        Me.chkDB.Properties.AutoWidth = True
        Me.chkDB.Properties.Caption = "Database"
        Me.chkDB.Size = New System.Drawing.Size(70, 19)
        Me.chkDB.TabIndex = 1
        Me.chkDB.Tag = "AE"
        '
        'chkWks
        '
        Me.chkWks.EnterMoveNextControl = True
        Me.chkWks.Location = New System.Drawing.Point(10, 26)
        Me.chkWks.Name = "chkWks"
        Me.chkWks.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkWks.Properties.Appearance.Options.UseFont = True
        Me.chkWks.Properties.AutoWidth = True
        Me.chkWks.Properties.Caption = "Workstation"
        Me.chkWks.Size = New System.Drawing.Size(86, 19)
        Me.chkWks.TabIndex = 0
        Me.chkWks.Tag = "AE"
        '
        'frmJob
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(884, 621)
        Me.Controls.Add(Me.tcMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Name = "frmJob"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tcMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tcMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcMain.ResumeLayout(False)
        Me.tabJob.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkEnabled.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSteps.ResumeLayout(False)
        CType(Me.gbxStep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxStep.ResumeLayout(False)
        Me.gbxStep.PerformLayout()
        CType(Me.cbxStepType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArg3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArg2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMemo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArg1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStepSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStepName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.chkTablet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkService.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWks.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcMain As Care.Controls.CareTab
    Friend WithEvents tabJob As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents tabSteps As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtSeq As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cgSteps As Care.Controls.CareGridWithButtons
    Friend WithEvents gbxStep As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnStepCancel As Care.Controls.CareButton
    Friend WithEvents btnStepOK As Care.Controls.CareButton
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtArg1 As Care.Controls.CareTextBox
    Friend WithEvents txtStepSeq As Care.Controls.CareTextBox
    Friend WithEvents txtStepName As Care.Controls.CareTextBox
    Friend WithEvents chkEnabled As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents txtArg3 As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtArg2 As Care.Controls.CareTextBox
    Friend WithEvents cbxStepType As Care.Controls.CareComboBox
    Friend WithEvents txtMemo As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents btnImportStep As Care.Controls.CareButton
    Friend WithEvents btnBrowse As Care.Controls.CareButton
    Friend WithEvents btnCopyID As Care.Controls.CareButton
    Friend WithEvents btnOpen As Care.Controls.CareButton
    Friend WithEvents btnCopyJob As Care.Controls.CareButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkTablet As Care.Controls.CareCheckBox
    Friend WithEvents chkTS As Care.Controls.CareCheckBox
    Friend WithEvents chkService As Care.Controls.CareCheckBox
    Friend WithEvents chkDB As Care.Controls.CareCheckBox
    Friend WithEvents chkWks As Care.Controls.CareCheckBox

End Class
