﻿Option Strict On
Imports System.IO
Imports Care.Global
Imports UpdateShared.Business

Public Class frmLicenseManager

    Private m_PrivateKey As String = ""

    Private Sub frmManageJobs_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_PrivateKey = File.ReadAllText(Environment.CurrentDirectory + "\licenses\privateKey.xml")
        RefreshGrid()
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btn30.Enabled = Enabled
        btn60.Enabled = Enabled
        btn90.Enabled = Enabled
        btn6m.Enabled = Enabled
        btn12m.Enabled = Enabled
    End Sub

    Private Sub btn30_Click(sender As Object, e As EventArgs) Handles btn30.Click
        ProcessGrid(30)
    End Sub

    Private Sub ProcessGrid(ByVal Expires As Integer)

        If cgJobs.UnderlyingGridView.GetSelectedRows.Count <= 0 Then
            CareMessage("Please select some jobs...", Windows.Forms.MessageBoxIcon.Exclamation, "Manage Jobs")
        Else

            SetCMDs(False)

            For Each _iRow As Integer In cgJobs.UnderlyingGridView.GetSelectedRows

                Dim _ID As String = cgJobs.GetRowCellValue(_iRow, "ID").ToString

                Dim _C As Customer = Customer.RetreiveByID(New Guid(_ID))
                If _C IsNot Nothing Then

                    Dim _Expires As Date = Today
                    If Expires = 6 Then _Expires = DateAdd(DateInterval.Month, 6, Today)
                    If Expires = 12 Then _Expires = DateAdd(DateInterval.Month, 12, Today)
                    If Expires = 30 Then _Expires = DateAdd(DateInterval.Day, 30, Today)
                    If Expires = 60 Then _Expires = DateAdd(DateInterval.Day, 60, Today)
                    If Expires = 90 Then _Expires = DateAdd(DateInterval.Day, 90, Today)

                    _C._SfLicenseExp = _Expires
                    _C.Store()

                    Session.SetProgressMessage("Generating Licenses for " + _C._Name + "...")
                    GenerateLicenses(_C._ID.Value, _C._Name, _C._SfProduct, _Expires)

                End If

            Next

            Session.HideProgressBar()

            SetCMDs(True)
            RefreshGrid()

        End If

    End Sub

    Private Sub GenerateLicenses(ByVal CustomerID As Guid, ByVal CustomerName As String, ByVal Product As String, ByVal ExpiryDate As Date)
        Dim _EndPoints As List(Of Endpoint) = UpdateShared.Business.Endpoint.RetreiveByCustomer(Session.ConnectionString, CustomerID)
        If _EndPoints IsNot Nothing Then
            If _EndPoints.Count > 0 Then
                For Each _e As Endpoint In _EndPoints
                    If _e._LicenseStatus <> -99 Then
                        _e._LicenseExpires = ExpiryDate
                        _e._LicenseStatus = 1
                        _e._LicenseData = Licensing.ReturnLicense(m_PrivateKey, CustomerName, Product, ExpiryDate, _e._ComputerName)
                        _e.Store()
                    End If
                    Session.StepProgressBar()
                Next
            End If
        End If
    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""

        _SQL += "select ID, name as 'Customer', sf_product as 'Product', sf_license_exp as 'Expires', sf_last as 'Last Sync'"
        _SQL += " from Customers"
        _SQL += " where active = 1"

        If radExpiring.Checked Then
            _SQL += " and sf_license_exp IS NOT NULL AND sf_license_exp > getdate() AND sf_license_exp < dateadd(day, 30, getdate())"
        End If

        If radExpired.Checked Then
            _SQL += " and sf_license_exp IS NULL OR sf_license_exp <= getdate()"
        End If

        _SQL += " order by name"

        cgJobs.HideFirstColumn = True
        cgJobs.Populate(Session.ConnectionString, _SQL)

        cgJobs.AutoSizeColumns()

    End Sub

    Private Sub radPending_CheckedChanged(sender As Object, e As EventArgs) Handles radExpiring.CheckedChanged
        If radExpiring.Checked Then RefreshGrid()
    End Sub

    Private Sub radPendingImmediate_CheckedChanged(sender As Object, e As EventArgs) Handles radExpired.CheckedChanged
        If radExpired.Checked Then RefreshGrid()
    End Sub

    Private Sub btn60_Click(sender As Object, e As EventArgs) Handles btn60.Click
        ProcessGrid(60)
    End Sub

    Private Sub btn90_Click(sender As Object, e As EventArgs) Handles btn90.Click
        ProcessGrid(90)
    End Sub

    Private Sub btn6m_Click(sender As Object, e As EventArgs) Handles btn6m.Click
        ProcessGrid(6)
    End Sub

    Private Sub btn12m_Click(sender As Object, e As EventArgs) Handles btn12m.Click
        ProcessGrid(12)
    End Sub

    Private Sub radActive_CheckedChanged(sender As Object, e As EventArgs) Handles radActive.CheckedChanged
        If radActive.Checked Then RefreshGrid()
    End Sub
End Class