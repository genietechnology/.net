﻿Imports Care.Global
Imports UpdateShared.Business

Public Class frmDistribute

    Private m_CustomerID As String = ""
    Private m_AppID As String = ""
    Private m_JobID As String = ""

    Private m_TargetWorkstation As Boolean = False
    Private m_TargetDB As Boolean = False
    Private m_TargetService As Boolean = False
    Private m_TargetTouchscreen As Boolean = False
    Private m_TargetTablet As Boolean = False

    Private Sub frmDistribute_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cgCustomers.HideFirstColumn = True
        cgCustomers.Populate(Session.ConnectionString, "select ID, name as 'Customer' from Customers where active = 1 order by name")

    End Sub

    Private Sub btnDist_Click(sender As Object, e As EventArgs) Handles btnDist.Click
        ProcessSelected(False)
    End Sub

    Private Sub btnImmediate_Click(sender As Object, e As EventArgs) Handles btnImmediate.Click
        ProcessSelected(True)
    End Sub

    Private Sub ProcessSelected(ByVal Immediate As Boolean)

        If cgEndpoints.UnderlyingGridView.GetSelectedRows.Count <= 0 Then
            CareMessage("Please select some endpoints...", Windows.Forms.MessageBoxIcon.Exclamation, "Distribute")
        Else
            For Each _iRow As Integer In cgEndpoints.UnderlyingGridView.GetSelectedRows
                Dim _EndpointID As String = cgEndpoints.GetRowCellValue(_iRow, "ID").ToString
                SetupJob(_EndpointID, Immediate)
            Next
        End If

    End Sub

    Private Sub SetupJob(ByVal EndpointID As String, ByVal Immediate As Boolean)
        Dim _E As New EndpointJob
        _E._EndpointId = New Guid(EndpointID)
        _E._AppId = New Guid(m_AppID)
        _E._JobId = New Guid(m_JobID)
        _E._Immediate = Immediate
        _E._Status = 0
        _E.Store()
    End Sub

    Private Sub cgCustomers_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgCustomers.FocusedRowChanged

        Dim _CustomerID As String = ReturnID(cgCustomers, e)
        If _CustomerID <> "" Then

            m_CustomerID = cgCustomers.CurrentRow("ID").ToString
            gbxCustomers.Text = "Customers (" + cgCustomers.CurrentRow("Customer").ToString + " selected)"

            Dim _SQL As String = "select ID, app_name as 'Application', app_type as 'Type' from CustomerApps where customer_id = '" + _CustomerID + "' order by app_name"
            cgApps.HideFirstColumn = True
            cgApps.Populate(Session.ConnectionString, _SQL)

        End If

    End Sub

    Private Sub cgApps_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgApps.FocusedRowChanged

        Dim _ReturnID As String = ReturnID(cgApps, e)
        If _ReturnID <> "" Then

            m_AppID = _ReturnID
            gbxApps.Text = "Apps (" + cgApps.CurrentRow("Application").ToString + " selected)"

            Dim _SQL As String = ""
            _SQL += "select ID, name as 'Job',"
            _SQL += " target_workstation, target_db, target_service, target_touchscreen, target_tablet"
            _SQL += " from Jobs"
            _SQL += " where enabled = 1"

            Dim _Type As String = ReturnValue(cgApps, "Type", e).ToString
            Select Case _Type

                Case "Server"
                    _SQL += " and (target_workstation = 1 OR target_db = 1)"

                Case "Service Host"
                    _SQL += " and target_service = 1"

                Case "Workstation"
                    _SQL += " and target_workstation = 1"

                Case "Touchscreen"
                    _SQL += " and target_touchscreen = 1"

                Case "Tablet"
                    _SQL += " and target_tablet = 1"

                Case Else
                    'not defined, so we show everything

            End Select

            _SQL += " order by name"

            cgJobs.HideFirstColumn = True
            cgJobs.Populate(Session.ConnectionString, _SQL)

            cgJobs.Columns("target_workstation").Visible = False
            cgJobs.Columns("target_db").Visible = False
            cgJobs.Columns("target_service").Visible = False
            cgJobs.Columns("target_touchscreen").Visible = False
            cgJobs.Columns("target_tablet").Visible = False

        End If

    End Sub

    Private Sub cgJobs_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgJobs.FocusedRowChanged

        Dim _ID As String = ReturnID(cgJobs, e)
        If _ID <> "" Then

            m_JobID = cgJobs.CurrentRow("ID").ToString
            gbxJobs.Text = "Jobs (" + cgJobs.CurrentRow("Job").ToString + " selected)"

            m_TargetWorkstation = ValueHandler.ConvertBoolean(ReturnValue(cgJobs, "target_workstation", e))
            m_TargetDB = ValueHandler.ConvertBoolean(ReturnValue(cgJobs, "target_db", e))
            m_TargetService = ValueHandler.ConvertBoolean(ReturnValue(cgJobs, "target_service", e))
            m_TargetTouchscreen = ValueHandler.ConvertBoolean(ReturnValue(cgJobs, "target_touchscreen", e))
            m_TargetTablet = ValueHandler.ConvertBoolean(ReturnValue(cgJobs, "target_tablet", e))

            Dim _SQL As String = ""
            _SQL += "select ID, computer_name as 'Endpoint', last_checkin as 'Last Checkin'"
            _SQL += " from Endpoints"
            _SQL += " where customer_id = '" + m_CustomerID + "'"
            _SQL += " and license_status >= 0"

            Dim _Clause As String = ""

            If m_TargetService Then

                If _Clause = "" Then
                    _Clause = " allow_service = 1"
                Else
                    _Clause += " OR allow_service = 1"
                End If

            Else

                If m_TargetWorkstation Then _Clause = "allow_workstation = 1"

                If m_TargetDB Then
                    If _Clause = "" Then
                        _Clause = " allow_db = 1"
                    Else
                        _Clause += " OR allow_db = 1"
                    End If
                End If

                If m_TargetTouchscreen Then
                    If _Clause = "" Then
                        _Clause = " allow_touchscreen = 1"
                    Else
                        _Clause += " OR allow_touchscreen = 1"
                    End If
                End If

                If m_TargetTablet Then
                    If _Clause = "" Then
                        _Clause = " allow_tablet = 1"
                    Else
                        _Clause += " OR allow_tablet = 1"
                    End If
                End If

            End If

            If _Clause <> "" Then
                _SQL += " AND (" + _Clause + ")"
            End If

            _SQL += " order by computer_name"

            cgEndpoints.HideFirstColumn = True
            cgEndpoints.Populate(Session.ConnectionString, _SQL)

        End If

    End Sub

    Private Function ReturnID(ByRef Grid As Care.Controls.CareGrid, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) As String

        If e Is Nothing Then Return ""
        If e.FocusedRowHandle < 0 Then Return ""

        Return Grid.GetRowCellValue(e.FocusedRowHandle, "ID").ToString

    End Function

    Private Function ReturnValue(ByRef Grid As Care.Controls.CareGrid, ByVal ColumnName As String, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) As Object

        If e Is Nothing Then Return ""
        If e.FocusedRowHandle < 0 Then Return ""

        Return Grid.GetRowCellValue(e.FocusedRowHandle, ColumnName).ToString

    End Function

End Class