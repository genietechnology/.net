﻿Imports Care.Security
Imports System.Security.Cryptography

Public Class Licensing

    Public Shared Function ReturnLicense(ByVal PrivateKey As String, ByVal CustomerName As String, ByVal Product As String, ByVal Expires As Date?, ByVal ComputerName As String) As String

        If Not Expires.HasValue Then Return ""

        ' create the licence terms:
        Dim terms As New LicenseTerms
        With terms
            .StartDate = New Date(Today.Year, Today.Month, Today.Day, 0, 1, 0)
            .ExpiryDate = New Date(Expires.Value.Year, Expires.Value.Month, Expires.Value.Day, 23, 59, 0)
            .CompanyName = CustomerName
            .CompanyLocation = Product
            .ComputerName = ComputerName.ToUpper
        End With

        ' create the crypto-service provider:
        Dim dsa As New DSACryptoServiceProvider()

        ' setup the dsa from the private key:e
        dsa.FromXmlString(PrivateKey)

        ' get the byte-array of the licence terms:
        Dim licensebytes As Byte() = terms.GetLicenseData()

        ' get the signature:
        Dim signature As Byte() = dsa.SignData(licensebytes)

        ' now create the license object:
        Dim _lic As New License
        _lic.LicenseTerms = Convert.ToBase64String(licensebytes)
        _lic.Signature = Convert.ToBase64String(signature)

        Return _lic.ToString

    End Function

End Class
