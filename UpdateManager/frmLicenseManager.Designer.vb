﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmLicenseManager
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.radExpired = New Care.Controls.CareRadioButton(Me.components)
        Me.radExpiring = New Care.Controls.CareRadioButton(Me.components)
        Me.btn30 = New Care.Controls.CareButton(Me.components)
        Me.cgJobs = New Care.Controls.CareGrid()
        Me.btn60 = New Care.Controls.CareButton(Me.components)
        Me.btn90 = New Care.Controls.CareButton(Me.components)
        Me.btn6m = New Care.Controls.CareButton(Me.components)
        Me.btn12m = New Care.Controls.CareButton(Me.components)
        Me.radActive = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radExpired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radExpiring.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radActive)
        Me.GroupControl1.Controls.Add(Me.radExpired)
        Me.GroupControl1.Controls.Add(Me.radExpiring)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(876, 30)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'radExpired
        '
        Me.radExpired.Location = New System.Drawing.Point(123, 6)
        Me.radExpired.Name = "radExpired"
        Me.radExpired.Properties.AutoWidth = True
        Me.radExpired.Properties.Caption = "Expired"
        Me.radExpired.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radExpired.Properties.RadioGroupIndex = 0
        Me.radExpired.Size = New System.Drawing.Size(58, 19)
        Me.radExpired.TabIndex = 4
        Me.radExpired.TabStop = False
        '
        'radExpiring
        '
        Me.radExpiring.EditValue = True
        Me.radExpiring.Location = New System.Drawing.Point(5, 6)
        Me.radExpiring.Name = "radExpiring"
        Me.radExpiring.Properties.AutoWidth = True
        Me.radExpiring.Properties.Caption = "Expiring in 30 days"
        Me.radExpiring.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radExpiring.Properties.RadioGroupIndex = 0
        Me.radExpiring.Size = New System.Drawing.Size(112, 19)
        Me.radExpiring.TabIndex = 3
        '
        'btn30
        '
        Me.btn30.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn30.Location = New System.Drawing.Point(12, 386)
        Me.btn30.Name = "btn30"
        Me.btn30.Size = New System.Drawing.Size(140, 23)
        Me.btn30.TabIndex = 1
        Me.btn30.Text = "Expires in 30 days"
        '
        'cgJobs
        '
        Me.cgJobs.AllowBuildColumns = True
        Me.cgJobs.AllowEdit = False
        Me.cgJobs.AllowHorizontalScroll = False
        Me.cgJobs.AllowMultiSelect = True
        Me.cgJobs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgJobs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgJobs.Appearance.Options.UseFont = True
        Me.cgJobs.AutoSizeByData = True
        Me.cgJobs.DisableAutoSize = False
        Me.cgJobs.DisableDataFormatting = False
        Me.cgJobs.FocusedRowHandle = -2147483648
        Me.cgJobs.HideFirstColumn = False
        Me.cgJobs.Location = New System.Drawing.Point(12, 48)
        Me.cgJobs.Name = "cgJobs"
        Me.cgJobs.PreviewColumn = ""
        Me.cgJobs.QueryID = Nothing
        Me.cgJobs.RowAutoHeight = False
        Me.cgJobs.SearchAsYouType = True
        Me.cgJobs.ShowAutoFilterRow = False
        Me.cgJobs.ShowFindPanel = False
        Me.cgJobs.ShowGroupByBox = False
        Me.cgJobs.ShowLoadingPanel = False
        Me.cgJobs.ShowNavigator = False
        Me.cgJobs.Size = New System.Drawing.Size(876, 332)
        Me.cgJobs.TabIndex = 2
        '
        'btn60
        '
        Me.btn60.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn60.Location = New System.Drawing.Point(158, 386)
        Me.btn60.Name = "btn60"
        Me.btn60.Size = New System.Drawing.Size(140, 23)
        Me.btn60.TabIndex = 3
        Me.btn60.Text = "Expires in 60 days"
        '
        'btn90
        '
        Me.btn90.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn90.Location = New System.Drawing.Point(304, 386)
        Me.btn90.Name = "btn90"
        Me.btn90.Size = New System.Drawing.Size(140, 23)
        Me.btn90.TabIndex = 4
        Me.btn90.Text = "Expires in 90 days"
        '
        'btn6m
        '
        Me.btn6m.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn6m.Location = New System.Drawing.Point(450, 386)
        Me.btn6m.Name = "btn6m"
        Me.btn6m.Size = New System.Drawing.Size(140, 23)
        Me.btn6m.TabIndex = 5
        Me.btn6m.Text = "Expires in 6 months"
        '
        'btn12m
        '
        Me.btn12m.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn12m.Location = New System.Drawing.Point(596, 386)
        Me.btn12m.Name = "btn12m"
        Me.btn12m.Size = New System.Drawing.Size(140, 23)
        Me.btn12m.TabIndex = 6
        Me.btn12m.Text = "Expires in 1 year"
        '
        'radActive
        '
        Me.radActive.Location = New System.Drawing.Point(187, 6)
        Me.radActive.Name = "radActive"
        Me.radActive.Properties.AutoWidth = True
        Me.radActive.Properties.Caption = "All Active Customers"
        Me.radActive.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radActive.Properties.RadioGroupIndex = 0
        Me.radActive.Size = New System.Drawing.Size(120, 19)
        Me.radActive.TabIndex = 5
        Me.radActive.TabStop = False
        '
        'frmLicenseManager
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 417)
        Me.Controls.Add(Me.btn12m)
        Me.Controls.Add(Me.btn6m)
        Me.Controls.Add(Me.btn90)
        Me.Controls.Add(Me.btn60)
        Me.Controls.Add(Me.cgJobs)
        Me.Controls.Add(Me.btn30)
        Me.Controls.Add(Me.GroupControl1)
        Me.LoadMaximised = True
        Me.Name = "frmLicenseManager"
        Me.Text = "frmManageJobs"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.radExpired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radExpiring.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radExpiring As Care.Controls.CareRadioButton
    Friend WithEvents btn30 As Care.Controls.CareButton
    Friend WithEvents cgJobs As Care.Controls.CareGrid
    Friend WithEvents radExpired As Care.Controls.CareRadioButton
    Friend WithEvents btn60 As Care.Controls.CareButton
    Friend WithEvents btn90 As Care.Controls.CareButton
    Friend WithEvents btn6m As Care.Controls.CareButton
    Friend WithEvents btn12m As Care.Controls.CareButton
    Friend WithEvents radActive As Care.Controls.CareRadioButton
End Class
