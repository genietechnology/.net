﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManageJobs
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.radCompleted = New Care.Controls.CareRadioButton(Me.components)
        Me.radPendingImmediate = New Care.Controls.CareRadioButton(Me.components)
        Me.radPending = New Care.Controls.CareRadioButton(Me.components)
        Me.btnCancelJob = New Care.Controls.CareButton(Me.components)
        Me.cgJobs = New Care.Controls.CareGrid()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radCompleted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPendingImmediate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPending.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radCompleted)
        Me.GroupControl1.Controls.Add(Me.radPendingImmediate)
        Me.GroupControl1.Controls.Add(Me.radPending)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(876, 30)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'radCompleted
        '
        Me.radCompleted.Location = New System.Drawing.Point(264, 5)
        Me.radCompleted.Name = "radCompleted"
        Me.radCompleted.Properties.AutoWidth = True
        Me.radCompleted.Properties.Caption = "Completed today"
        Me.radCompleted.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCompleted.Properties.RadioGroupIndex = 0
        Me.radCompleted.Size = New System.Drawing.Size(104, 19)
        Me.radCompleted.TabIndex = 5
        '
        'radPendingImmediate
        '
        Me.radPendingImmediate.Location = New System.Drawing.Point(96, 5)
        Me.radPendingImmediate.Name = "radPendingImmediate"
        Me.radPendingImmediate.Properties.AutoWidth = True
        Me.radPendingImmediate.Properties.Caption = "Pending for Immediate Install"
        Me.radPendingImmediate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPendingImmediate.Properties.RadioGroupIndex = 0
        Me.radPendingImmediate.Size = New System.Drawing.Size(162, 19)
        Me.radPendingImmediate.TabIndex = 4
        '
        'radPending
        '
        Me.radPending.EditValue = True
        Me.radPending.Location = New System.Drawing.Point(5, 5)
        Me.radPending.Name = "radPending"
        Me.radPending.Properties.AutoWidth = True
        Me.radPending.Properties.Caption = "Pending Jobs"
        Me.radPending.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPending.Properties.RadioGroupIndex = 0
        Me.radPending.Size = New System.Drawing.Size(85, 19)
        Me.radPending.TabIndex = 3
        '
        'btnCancelJob
        '
        Me.btnCancelJob.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancelJob.Location = New System.Drawing.Point(12, 386)
        Me.btnCancelJob.Name = "btnCancelJob"
        Me.btnCancelJob.Size = New System.Drawing.Size(140, 23)
        Me.btnCancelJob.TabIndex = 1
        Me.btnCancelJob.Text = "Cancel Selected Jobs"
        '
        'cgJobs
        '
        Me.cgJobs.AllowBuildColumns = True
        Me.cgJobs.AllowEdit = False
        Me.cgJobs.AllowHorizontalScroll = False
        Me.cgJobs.AllowMultiSelect = True
        Me.cgJobs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgJobs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgJobs.Appearance.Options.UseFont = True
        Me.cgJobs.AutoSizeByData = True
        Me.cgJobs.DisableAutoSize = False
        Me.cgJobs.DisableDataFormatting = False
        Me.cgJobs.FocusedRowHandle = -2147483648
        Me.cgJobs.HideFirstColumn = False
        Me.cgJobs.Location = New System.Drawing.Point(12, 48)
        Me.cgJobs.Name = "cgJobs"
        Me.cgJobs.QueryID = Nothing
        Me.cgJobs.RowAutoHeight = False
        Me.cgJobs.SearchAsYouType = True
        Me.cgJobs.ShowAutoFilterRow = False
        Me.cgJobs.ShowFindPanel = False
        Me.cgJobs.ShowGroupByBox = False
        Me.cgJobs.ShowNavigator = False
        Me.cgJobs.Size = New System.Drawing.Size(876, 332)
        Me.cgJobs.TabIndex = 2
        '
        'frmManageJobs
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 417)
        Me.Controls.Add(Me.cgJobs)
        Me.Controls.Add(Me.btnCancelJob)
        Me.Controls.Add(Me.GroupControl1)
        Me.LoadMaximised = True
        Me.Name = "frmManageJobs"
        Me.Text = "frmManageJobs"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.radCompleted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPendingImmediate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPending.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radPending As Care.Controls.CareRadioButton
    Friend WithEvents btnCancelJob As Care.Controls.CareButton
    Friend WithEvents cgJobs As Care.Controls.CareGrid
    Friend WithEvents radPendingImmediate As Care.Controls.CareRadioButton
    Friend WithEvents radCompleted As Care.Controls.CareRadioButton
End Class
