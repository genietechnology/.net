﻿Option Strict On

Imports Care.Global
Imports UpdateShared.Business

Public Class frmManageJobs

    Private Sub frmManageJobs_Load(sender As Object, e As EventArgs) Handles Me.Load
        RefreshGrid()
    End Sub

    Private Sub btnCancelJob_Click(sender As Object, e As EventArgs) Handles btnCancelJob.Click

        btnCancelJob.Enabled = False

        If cgJobs.UnderlyingGridView.GetSelectedRows.Count <= 0 Then
            CareMessage("Please select some jobs...", Windows.Forms.MessageBoxIcon.Exclamation, "Manage Jobs")
        Else
            For Each _iRow As Integer In cgJobs.UnderlyingGridView.GetSelectedRows
                Dim _ID As String = cgJobs.GetRowCellValue(_iRow, "ID").ToString
                CancelJob(_ID)
            Next
        End If

        RefreshGrid()

        btnCancelJob.Enabled = True

    End Sub

    Private Sub CancelJob(ByVal ID As String)
        EndpointJob.DeleteRecord(New Guid(ID))
    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""

        _SQL += "select ej.ID, c.name as 'Customer', e.computer_name as 'Endpoint', a.app_name as 'Application',"
        _SQL += " j.name as 'Job', j.enabled as 'Job Enabled', ej.status as 'Status', ej.immediate as 'Immediate',"
        _SQL += " ej.last_log as 'Last Log', ej.last_run as 'Last Run' from EndpointJobs ej"
        _SQL += " left join Jobs j on j.id = ej.job_id"
        _SQL += " left join Endpoints e on e.ID = ej.endpoint_id"
        _SQL += " left join CustomerApps a on a.ID = ej.app_id"
        _SQL += " left join Customers c on c.ID = e.customer_id"

        If radPending.Checked Then
            _SQL += " where ej.status = 0"
        End If

        If radPendingImmediate.Checked Then
            _SQL += " where ej.status = 0"
            _SQL += " and ej.immediate = 1"
        End If

        If radCompleted.Checked Then
            _SQL += " where ej.status = 1"
            _SQL += " and ej.last_run between '" + ValueHandler.SQLDate(Today) + " 00:00:00.000' and '" + ValueHandler.SQLDate(Today) + " 23:59:59.000'"
        End If

        _SQL += " order by c.name, e.computer_name, j.name"

        cgJobs.HideFirstColumn = True
        cgJobs.Populate(Session.ConnectionString, _SQL)

        cgJobs.Columns("Last Run").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgJobs.Columns("Last Run").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        cgJobs.AutoSizeColumns()

    End Sub

    Private Sub radPending_CheckedChanged(sender As Object, e As EventArgs) Handles radPending.CheckedChanged
        If radPending.Checked Then RefreshGrid()
    End Sub

    Private Sub radPendingImmediate_CheckedChanged(sender As Object, e As EventArgs) Handles radPendingImmediate.CheckedChanged
        If radPendingImmediate.Checked Then RefreshGrid()
    End Sub

    Private Sub radCompleted_CheckedChanged(sender As Object, e As EventArgs) Handles radCompleted.CheckedChanged
        If radCompleted.Checked Then RefreshGrid()
    End Sub

End Class