﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Care.Address")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Care Software Ltd")> 
<Assembly: AssemblyProduct("Care.Address")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6dd1d3a4-3fca-46f4-a71b-6cf1f41f4f95")>
<Assembly: AssemblyVersion("1.19.1.3")>
<Assembly: AssemblyFileVersion("1.19.1.3")>



' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 
