﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddressList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gridAddresses = New Care.Controls.CareGrid()
        Me.SuspendLayout()
        '
        'gridAddresses
        '
        Me.gridAddresses.AllowBuildColumns = True
        Me.gridAddresses.AllowHorizontalScroll = False
        Me.gridAddresses.AllowMultiSelect = False
        Me.gridAddresses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridAddresses.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gridAddresses.Appearance.Options.UseFont = True
        Me.gridAddresses.AutoSizeByData = True
        Me.gridAddresses.HideFirstColumn = False
        Me.gridAddresses.Location = New System.Drawing.Point(12, 12)
        Me.gridAddresses.Name = "gridAddresses"
        Me.gridAddresses.QueryID = Nothing
        Me.gridAddresses.SearchAsYouType = True
        Me.gridAddresses.ShowFindPanel = True
        Me.gridAddresses.ShowGroupByBox = False
        Me.gridAddresses.ShowNavigator = False
        Me.gridAddresses.Size = New System.Drawing.Size(717, 373)
        Me.gridAddresses.TabIndex = 0
        '
        'frmAddressList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(741, 397)
        Me.Controls.Add(Me.gridAddresses)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmAddressList"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Address"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridAddresses As Care.Controls.CareGrid
End Class
