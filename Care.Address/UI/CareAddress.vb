﻿Imports System.Windows.Forms
Imports System.ComponentModel

Public Class CareAddress

    Private m_LookupMode As Boolean
    Private m_ChangeColour As System.Drawing.Color
    Private m_UserID As String = ""
    Private m_PAFList As New List(Of Core.PAFData)

    Private m_Populated As Boolean

    Private m_AddressFull As String
    Private m_AddressLine As String
    Private m_AddressLine1 As String

    Private m_CountyRequired As Boolean

    Private m_DisplayMode As EnumDisplayMode = EnumDisplayMode.SplitControlsNoFrame

    Public Enum EnumDisplayMode
        SingleControl
        SplitControls
        SplitControlsNoFrame
    End Enum

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        HideLookup()
        SetLayout()

    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, keyData As Keys) As Boolean

        If Me.Enabled AndAlso Not Me.ReadOnly Then

            'editable etc

            'is lookup displayed
            If m_LookupMode Then

                'yes, check for escape
                If keyData = Keys.Escape Then
                    HideLookup()
                    txtAddress.BackColor = m_ChangeColour
                    txtAddress.Focus()
                    Return True
                End If

            Else

                'no, check for F3
                If keyData = Keys.F3 Then
                    ShowLookup()
                    Return True
                End If

            End If

        Else
            'no editable, to we leave it
        End If

        Return MyBase.ProcessCmdKey(msg, keyData)

    End Function

    Public Sub SetAddress(ByVal AddressBlock As String, ByVal Town As String, ByVal County As String, ByVal PostCode As String, _
                      ByVal Tel As String, ByVal Fax As String, ByVal Email As String)

        txtSplitBlock.Text = AddressBlock
        txtSplitTown.Text = Town
        txtSplitCounty.Text = County
        txtSplitPostCode.Text = PostCode

    End Sub

    Public Sub Clear()

        txtAddress.Text = ""
        txtPostCode.Text = ""

        txtSplitBlock.Text = ""
        txtSplitTown.Text = ""
        txtSplitCounty.Text = ""
        txtSplitPostCode.Text = ""

        m_AddressFull = ""
        m_AddressLine = ""
        m_AddressLine1 = ""

        m_Populated = False

    End Sub

#Region "Address Properties"

    Public ReadOnly Property Populated As Boolean
        Get
            Return m_Populated
        End Get
    End Property

    Public ReadOnly Property Address1 As String
        Get
            Return m_AddressLine1
        End Get
    End Property

    ''' <summary>
    ''' The full address
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Address As String
        Get
            Return txtAddress.Text
        End Get
        Set(value As String)
            txtAddress.Text = value
        End Set
    End Property

    ''' <summary>
    ''' The full address, on one line split by commas
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property AddressLine As String
        Get
            Return m_AddressLine
        End Get
    End Property

    ''' <summary>
    ''' The first few address lines. Street, village etc.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AddressBlock As String
        Get
            Return txtSplitBlock.Text
        End Get
        Set(value As String)
            txtSplitBlock.Text = value
        End Set
    End Property

    Public Property Town As String
        Get
            Return txtSplitTown.Text
        End Get
        Set(value As String)
            txtSplitTown.Text = value
        End Set
    End Property

    Public ReadOnly Property CountyRequired As Boolean
        Get
            Return m_CountyRequired
        End Get
    End Property

    Public Property County As String
        Get
            Return txtSplitCounty.Text
        End Get
        Set(value As String)
            txtSplitCounty.Text = value
        End Set
    End Property

    Public Property PostCode As String
        Get
            Return txtSplitPostCode.Text
        End Get
        Set(value As String)
            txtSplitPostCode.Text = value
        End Set
    End Property

#End Region

#Region "Control Properties"

    <Category("_CareAttributes")>
    Public Property DisplayMode As EnumDisplayMode
        Get
            Return m_DisplayMode
        End Get
        Set(value As EnumDisplayMode)
            m_DisplayMode = value
            SetLayout()
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property BackColor As System.Drawing.Color
        Get
            Try
                Return txtAddress.BackColor
            Catch ex As Exception

            End Try
        End Get
        Set(value As System.Drawing.Color)
            txtAddress.BackColor = value
            txtSplitBlock.BackColor = value
            txtSplitTown.BackColor = value
            txtSplitCounty.BackColor = value
            txtSplitPostCode.BackColor = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property Text As String
        Get
            Return txtAddress.Text
        End Get
        Set(value As String)
            txtAddress.Text = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property [ReadOnly] As Boolean
        Get
            Return txtAddress.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            txtAddress.Properties.ReadOnly = value
            txtSplitBlock.Properties.ReadOnly = value
            txtSplitTown.Properties.ReadOnly = value
            txtSplitCounty.Properties.ReadOnly = value
            txtSplitPostCode.Properties.ReadOnly = value
            btnLookup.Enabled = Not [ReadOnly]
            If value = True Then HideLookup()
            SetToolTips()
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property MaxLength As Integer
        Get
            Return txtAddress.Properties.MaxLength
        End Get
        Set(value As Integer)
            txtAddress.Properties.MaxLength = value
        End Set
    End Property

#End Region

    Private Sub ShowLookup()

        If txtAddress.Properties.ReadOnly Then Exit Sub

        m_LookupMode = True

        m_ChangeColour = txtAddress.BackColor
        txtAddress.ResetBackColor()

        txtPostCode.BackColor = Drawing.Color.LightGreen
        txtPostCode.Text = ""

        panLookup.Show()
        panLookup.BringToFront()
        txtPostCode.Focus()

        SetToolTips()

    End Sub

    Private Sub HideLookup()
        m_LookupMode = False
        panLookup.Hide()
        txtAddress.BackColor = m_ChangeColour
    End Sub

    Private Sub txtPostCode_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles txtPostCode.ButtonClick
        DoLookup()
    End Sub

    Private Sub mnuLookup_Click(sender As Object, e As System.EventArgs) Handles mnuLookup.Click
        ShowLookup()
    End Sub

    Private Sub mnuPopup_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles mnuPopup.Opening
        If txtAddress.Properties.ReadOnly Then e.Cancel = True
    End Sub

    Private Sub DoLookup()

        If txtPostCode.Text = "" Then Exit Sub

        Cursor.Current = Cursors.AppStarting
        Application.DoEvents()

        Dim _API As New Core.CoreClient()
        Dim _Response As Core.PAFResponse = _API.AddressLookupViaPostCode(txtPostCode.Text)

        If _Response IsNot Nothing Then

            Dim _PAFArray As Core.PAFData() = _Response.PAFData

            Cursor.Current = Cursors.Default
            Application.DoEvents()

            If _PAFArray Is Nothing Then

            Else

                m_PAFList = _PAFArray.ToList
                _PAFArray = Nothing

                If m_PAFList.Count = 1 Then
                    BuildReturnAddress(New FormattedAddress(m_PAFList(0)))
                    HideLookup()
                Else
                    SelectAddress()
                End If

            End If

        End If

    End Sub

    Private Sub SelectAddress()

        Dim _frmAddressList As New frmAddressList(m_PAFList)
        _frmAddressList.ShowDialog()

        If _frmAddressList.SelectedAddress IsNot Nothing Then
            BuildReturnAddress(_frmAddressList.SelectedAddress)
            HideLookup()
        End If

        _frmAddressList.Dispose()
        _frmAddressList = Nothing

    End Sub

    Private Sub BuildReturnAddress(ByRef Address As FormattedAddress)

        m_AddressFull = ""
        m_AddressLine = ""
        m_AddressLine1 = ""

        txtSplitBlock.Text = ""
        txtSplitTown.Text = ""
        txtSplitCounty.Text = ""
        txtSplitPostCode.Text = ""
        m_CountyRequired = False

        Dim _Splitter As String = ""

        If Address.PremiseIndependent Then
            _Splitter = vbNewLine
        Else
            _Splitter = " "
        End If

        txtSplitBlock.Text += Address.Premise

        'next we append the double thoroughfare (if there is one)
        If Address.DoubleThoroughFare <> "" Then
            txtSplitBlock.Text += _Splitter
            txtSplitBlock.Text += Address.DoubleThoroughFare
            _Splitter = vbNewLine
        End If

        'append the thoroughfare
        If Address.ThoroughFare <> "" Then
            txtSplitBlock.Text += _Splitter
            txtSplitBlock.Text += Address.ThoroughFare
        End If

        'next we append the double locality (if there is one)
        If Address.DoubleLocality <> "" Then
            txtSplitBlock.Text += vbNewLine
            txtSplitBlock.Text += Address.DoubleLocality
        End If

        'append the locality
        If Address.Locality <> "" Then
            txtSplitBlock.Text += vbNewLine
            txtSplitBlock.Text += Address.Locality
        End If

        txtSplitTown.Text = Address.Town
        txtSplitPostCode.Text = Address.Postcode

        m_AddressFull = txtSplitBlock.Text + vbNewLine + txtSplitTown.Text

        If Address.CountyRequired Then
            m_CountyRequired = True
            txtSplitCounty.Text = Address.County
            m_AddressFull += vbNewLine + Address.County
        End If

        m_AddressFull += vbNewLine + Address.Postcode
        m_AddressLine = Replace(m_AddressFull, vbCrLf, ", ")
        m_AddressLine1 = txtSplitBlock.Lines(0)

        txtAddress.Text = m_AddressFull
        m_Populated = True

    End Sub

    Private Sub SetLayout()

        panAddressSplit.Top = 0
        panAddressSplit.Left = 0

        If m_DisplayMode = EnumDisplayMode.SingleControl Then

            panAddressSplit.Hide()
            txtAddress.Show()
            txtAddress.BringToFront()
            SetTooltips()

        Else

            txtAddress.Hide()
            panAddressSplit.Show()
            panAddressSplit.BringToFront()

            If m_DisplayMode = EnumDisplayMode.SplitControlsNoFrame Then
                panAddressSplit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
                panAddressSplit.BackColor = Drawing.Color.Transparent
            Else
                panAddressSplit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
            End If

        End If

    End Sub

    Private Sub btnLookup_Click(sender As System.Object, e As System.EventArgs) Handles btnLookup.Click
        ShowLookup()
    End Sub

    Private Sub txtPostCode_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtPostCode.Validating
        If txtPostCode.Visible = False Then Exit Sub
        If txtPostCode.Text = "" Then Exit Sub
        DoLookup()
    End Sub

    Private Sub SetToolTips()

        If Me.DisplayMode = EnumDisplayMode.SingleControl Then

            If panLookup.Visible Then
                txtAddress.ShowToolTips = False
            Else
                txtAddress.ShowToolTips = Not txtAddress.Properties.ReadOnly
            End If

        Else
            txtAddress.ShowToolTips = False
        End If

    End Sub

End Class
