﻿Public Class frmAddressList

    Private m_FormattedAddresses As New List(Of FormattedAddress)
    Private m_AddressList As List(Of Core.PAFData)
    Private m_SelectedAddress As FormattedAddress = Nothing

    Public Sub New(ByVal AddressList As List(Of Core.PAFData))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_AddressList = AddressList
        PopulateAddresses()

    End Sub

    Public ReadOnly Property SelectedAddress As FormattedAddress
        Get
            Return m_SelectedAddress
        End Get
    End Property

    Private Sub PopulateAddresses()

        For Each _PAF As Core.PAFData In m_AddressList
            Dim _A As New FormattedAddress(_PAF)
            m_FormattedAddresses.Add(_A)
        Next

        gridAddresses.Populate(m_FormattedAddresses)
        gridAddresses.Columns("NumericPremise").SortIndex = 0
        gridAddresses.Columns("NumericPremise").Visible = False
        gridAddresses.Columns("PremiseIndependent").Visible = False
        gridAddresses.Columns("CountyRequired").Visible = False
        gridAddresses.AutoSizeColumns()

    End Sub

    Private Sub gridAddresses_GridDoubleClick(sender As Object, e As System.EventArgs) Handles gridAddresses.GridDoubleClick
        SelectAddress()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        m_AddressList = Nothing
        m_SelectedAddress = Nothing
    End Sub

    Private Sub gridAddresses_GridKeyDown(sender As Object, e As KeyEventArgs) Handles gridAddresses.GridKeyDown

        Select Case e.KeyCode

            Case Keys.Enter
                SelectAddress()

            Case Keys.Escape
                Me.Close()

        End Select

    End Sub

    Private Sub SelectAddress()
        m_SelectedAddress = m_FormattedAddresses(gridAddresses.RowIndex)
        Me.Close()
    End Sub

    Private Sub frmAddressList_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        gridAddresses.MoveFirst()
    End Sub
End Class