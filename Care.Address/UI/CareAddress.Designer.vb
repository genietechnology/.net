﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CareAddress
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CareAddress))
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.mnuPopup = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuLookup = New System.Windows.Forms.ToolStripMenuItem()
        Me.panLookup = New Care.Controls.CareFrame()
        Me.txtPostCode = New DevExpress.XtraEditors.ButtonEdit()
        Me.ToolTipController1 = New DevExpress.Utils.ToolTipController(Me.components)
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.panAddressSplit = New DevExpress.XtraEditors.PanelControl()
        Me.btnLookup = New DevExpress.XtraEditors.SimpleButton()
        Me.txtSplitBlock = New DevExpress.XtraEditors.MemoEdit()
        Me.txtSplitTown = New DevExpress.XtraEditors.TextEdit()
        Me.txtSplitCounty = New DevExpress.XtraEditors.TextEdit()
        Me.txtSplitPostCode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.mnuPopup.SuspendLayout()
        CType(Me.panLookup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panLookup.SuspendLayout()
        CType(Me.txtPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panAddressSplit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panAddressSplit.SuspendLayout()
        CType(Me.txtSplitBlock.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSplitTown.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSplitCounty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSplitPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuPopup
        '
        Me.mnuPopup.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLookup})
        Me.mnuPopup.Name = "mnuPopup"
        Me.mnuPopup.Size = New System.Drawing.Size(160, 26)
        '
        'mnuLookup
        '
        Me.mnuLookup.Image = CType(resources.GetObject("mnuLookup.Image"), System.Drawing.Image)
        Me.mnuLookup.Name = "mnuLookup"
        Me.mnuLookup.Size = New System.Drawing.Size(159, 22)
        Me.mnuLookup.Text = "Lookup Address"
        '
        'panLookup
        '
        Me.panLookup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panLookup.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panLookup.Appearance.Options.UseFont = True
        Me.panLookup.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panLookup.AppearanceCaption.Options.UseFont = True
        Me.panLookup.Controls.Add(Me.txtPostCode)
        Me.panLookup.Controls.Add(Me.LabelControl1)
        Me.panLookup.Location = New System.Drawing.Point(0, 13)
        Me.panLookup.Name = "panLookup"
        Me.panLookup.Size = New System.Drawing.Size(262, 71)
        Me.panLookup.TabIndex = 1
        Me.panLookup.Text = "Address Lookup"
        '
        'txtPostCode
        '
        Me.txtPostCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPostCode.EditValue = "WW88 WWW"
        Me.txtPostCode.Location = New System.Drawing.Point(71, 32)
        Me.txtPostCode.Name = "txtPostCode"
        Me.txtPostCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostCode.Properties.Appearance.Options.UseFont = True
        Me.txtPostCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("txtPostCode.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, True)})
        Me.txtPostCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPostCode.Properties.MaxLength = 8
        Me.txtPostCode.Properties.ValidateOnEnterKey = True
        Me.txtPostCode.Size = New System.Drawing.Size(178, 30)
        Me.txtPostCode.TabIndex = 1
        Me.txtPostCode.ToolTipController = Me.ToolTipController1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(49, 15)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Postcode"
        '
        'panAddressSplit
        '
        Me.panAddressSplit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panAddressSplit.Controls.Add(Me.btnLookup)
        Me.panAddressSplit.Controls.Add(Me.txtSplitBlock)
        Me.panAddressSplit.Controls.Add(Me.txtSplitTown)
        Me.panAddressSplit.Controls.Add(Me.txtSplitCounty)
        Me.panAddressSplit.Controls.Add(Me.txtSplitPostCode)
        Me.panAddressSplit.Controls.Add(Me.LabelControl5)
        Me.panAddressSplit.Controls.Add(Me.LabelControl4)
        Me.panAddressSplit.Controls.Add(Me.LabelControl3)
        Me.panAddressSplit.Controls.Add(Me.LabelControl2)
        Me.panAddressSplit.Location = New System.Drawing.Point(0, 107)
        Me.panAddressSplit.Name = "panAddressSplit"
        Me.panAddressSplit.Size = New System.Drawing.Size(262, 141)
        Me.panAddressSplit.TabIndex = 2
        '
        'btnLookup
        '
        Me.btnLookup.Location = New System.Drawing.Point(8, 31)
        Me.btnLookup.Name = "btnLookup"
        Me.btnLookup.Size = New System.Drawing.Size(58, 23)
        Me.btnLookup.TabIndex = 8
        Me.btnLookup.TabStop = False
        Me.btnLookup.Text = "Lookup"
        '
        'txtSplitBlock
        '
        Me.txtSplitBlock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSplitBlock.EditValue = ""
        Me.txtSplitBlock.Location = New System.Drawing.Point(74, 9)
        Me.txtSplitBlock.Name = "txtSplitBlock"
        Me.txtSplitBlock.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSplitBlock.Properties.Appearance.Options.UseFont = True
        Me.txtSplitBlock.Properties.MaxLength = 250
        Me.txtSplitBlock.Size = New System.Drawing.Size(177, 46)
        Me.txtSplitBlock.TabIndex = 1
        Me.txtSplitBlock.UseOptimizedRendering = True
        '
        'txtSplitTown
        '
        Me.txtSplitTown.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSplitTown.Location = New System.Drawing.Point(74, 61)
        Me.txtSplitTown.Name = "txtSplitTown"
        Me.txtSplitTown.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSplitTown.Properties.Appearance.Options.UseFont = True
        Me.txtSplitTown.Properties.MaxLength = 40
        Me.txtSplitTown.Size = New System.Drawing.Size(177, 22)
        Me.txtSplitTown.TabIndex = 3
        '
        'txtSplitCounty
        '
        Me.txtSplitCounty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSplitCounty.Location = New System.Drawing.Point(74, 87)
        Me.txtSplitCounty.Name = "txtSplitCounty"
        Me.txtSplitCounty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSplitCounty.Properties.Appearance.Options.UseFont = True
        Me.txtSplitCounty.Properties.MaxLength = 40
        Me.txtSplitCounty.Size = New System.Drawing.Size(177, 22)
        Me.txtSplitCounty.TabIndex = 5
        '
        'txtSplitPostCode
        '
        Me.txtSplitPostCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSplitPostCode.Location = New System.Drawing.Point(74, 113)
        Me.txtSplitPostCode.Name = "txtSplitPostCode"
        Me.txtSplitPostCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSplitPostCode.Properties.Appearance.Options.UseFont = True
        Me.txtSplitPostCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSplitPostCode.Properties.MaxLength = 8
        Me.txtSplitPostCode.Size = New System.Drawing.Size(177, 22)
        Me.txtSplitPostCode.TabIndex = 7
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(12, 116)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(54, 15)
        Me.LabelControl5.TabIndex = 6
        Me.LabelControl5.Text = "Post Code"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(12, 90)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(39, 15)
        Me.LabelControl4.TabIndex = 4
        Me.LabelControl4.Text = "County"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(12, 64)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(30, 15)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Town"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(42, 15)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Address"
        '
        'txtAddress
        '
        Me.txtAddress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAddress.EditValue = ""
        Me.txtAddress.Location = New System.Drawing.Point(0, 0)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Properties.Appearance.Options.UseFont = True
        Me.txtAddress.Properties.MaxLength = 500
        Me.txtAddress.ShowToolTips = False
        Me.txtAddress.Size = New System.Drawing.Size(262, 257)
        Me.txtAddress.TabIndex = 0
        Me.txtAddress.ToolTip = "Dont forget, you can lookup addresses through our Postcode Lookup." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Hit F3 for " & _
    "access." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.txtAddress.ToolTipController = Me.ToolTipController1
        Me.txtAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.txtAddress.ToolTipTitle = "Postcode Lookup"
        Me.txtAddress.UseOptimizedRendering = True
        '
        'CareAddress
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.panLookup)
        Me.Controls.Add(Me.panAddressSplit)
        Me.Controls.Add(Me.txtAddress)
        Me.Name = "CareAddress"
        Me.Size = New System.Drawing.Size(262, 257)
        Me.mnuPopup.ResumeLayout(False)
        CType(Me.panLookup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panLookup.ResumeLayout(False)
        Me.panLookup.PerformLayout()
        CType(Me.txtPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panAddressSplit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panAddressSplit.ResumeLayout(False)
        Me.panAddressSplit.PerformLayout()
        CType(Me.txtSplitBlock.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSplitTown.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSplitCounty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSplitPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panLookup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtPostCode As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents mnuPopup As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuLookup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panAddressSplit As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtSplitTown As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSplitCounty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSplitPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSplitBlock As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnLookup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ToolTipController1 As DevExpress.Utils.ToolTipController

End Class
