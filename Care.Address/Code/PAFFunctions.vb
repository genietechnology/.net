﻿Imports System.Data.SqlClient

Public Class PAFFunctions

    Public Shared Function ValidatePostCode(ByVal PAFDB As String, ByVal PostCode As String, ByVal Premise As String, ByRef SplitAddress As SplitAddress) As Integer

        Dim _Found As Boolean = False
        Dim _Return As Integer = -99

        PostCode = PostCode.Replace(" ", "")
        Dim _PAFData As List(Of Core.PAFData) = ReturnPAFData(PAFDB, PostCode)

        If _PAFData IsNot Nothing Then

            For Each _P As Core.PAFData In _PAFData
                If _P._Premise = Premise Then
                    _Found = True
                    SplitAddress = New SplitAddress(_P)
                    Exit For
                End If
            Next

            If _Found Then
                _Return = 0
            Else
                _Return = -1
            End If

        Else
            'postcode not found
            _Return = -2
        End If

        Return _Return

    End Function

    Public Shared Function ReturnPAFData(ByVal PAFDB As String, ByVal PostCode As String) As List(Of Core.PAFData)

        If PostCode = "" Then Return Nothing

        Dim _CN As New SqlConnection(PAFDB)
        _CN.Open()

        Dim _Return As List(Of Core.PAFData) = Nothing
        Dim _SQL As String = ""

        _SQL += "select A.postcode, A.premises,"
        _SQL += " TF.thoroughfare_name, TFD.thoroughfare_descriptor,"
        _SQL += " DTF.thoroughfare_name as 'double_thoroughfare_name', DTFD.thoroughfare_descriptor as 'double_thoroughfare_descriptor',"
        _SQL += " L.dependent_locality, L.double_dependent_locality,"
        _SQL += " L.post_town"
        _SQL += " from address A"
        _SQL += " LEFT JOIN thoroughfare TF ON TF.thoroughfare_key = A.thoroughfare_key"
        _SQL += " LEFT JOIN thoroughfare_descriptor TFD ON TFD.thoroughfare_descriptor_key = A.thoroughfare_descriptor_key"
        _SQL += " LEFT JOIN thoroughfare DTF ON DTF.thoroughfare_key = A.double_thoroughfare_key"
        _SQL += " LEFT JOIN thoroughfare_descriptor DTFD ON DTFD.thoroughfare_descriptor_key = A.double_thoroughfare_descriptor_key"
        _SQL += " LEFT JOIN locality L ON L.locality_key = A.locality_key"
        _SQL += " WHERE A.postcode = '" + PostCode + "'"

        Dim _CMD As New SqlCommand(_SQL, _CN)
        If _CMD IsNot Nothing Then

            Dim _R As SqlDataReader = _CMD.ExecuteReader
            If _R IsNot Nothing Then

                _Return = New List(Of Core.PAFData)

                While _R.Read

                    Dim _Independent As Boolean = False

                    'split the premise lines
                    Dim _Premises As String() = _R.GetString(1).Split(CType("|", Char()))

                    Dim _Count As Integer = 0
                    For Each _Premise As String In _Premises

                        'the first set of premises are the "independent" ones (usually businesses)
                        If _Count = 0 Then
                            _Independent = True
                        Else
                            _Independent = False
                        End If

                        If _Premise <> "" Then

                            If _Premise.Contains(";") Then

                                'split on semi-colon
                                Dim _Dwellings As String() = _Premise.Split(CType(";", Char()))
                                For Each _D As String In _Dwellings

                                    Dim _PAFData As New Core.PAFData

                                    _PAFData._Premise = _D.Trim
                                    _PAFData._PremiseIndependent = _Independent
                                    _PAFData._DoubleThoroughfare = _R("double_thoroughfare_name").ToString.Trim
                                    _PAFData._DoubleThoroughfareDescriptor = _R("double_thoroughfare_descriptor").ToString.Trim
                                    _PAFData._Thoroughfare = _R("thoroughfare_name").ToString.Trim
                                    _PAFData._ThoroughfareDescriptor = _R("thoroughfare_descriptor").ToString.Trim
                                    _PAFData._DoubleDependentLocality = _R("double_dependent_locality").ToString.Trim
                                    _PAFData._DependentLocality = _R("dependent_locality").ToString.Trim
                                    _PAFData._Town = _R("post_town").ToString.Trim
                                    _PAFData._County = ""
                                    _PAFData._Postcode = _R("postcode").ToString.Trim

                                    _Return.Add(_PAFData)

                                Next

                            Else

                                Dim _PAFData As New Core.PAFData

                                _PAFData._Premise = _Premise.Trim
                                _PAFData._PremiseIndependent = _Independent
                                _PAFData._DoubleThoroughfare = _R("double_thoroughfare_name").ToString.Trim
                                _PAFData._DoubleThoroughfareDescriptor = _R("double_thoroughfare_descriptor").ToString.Trim
                                _PAFData._Thoroughfare = _R("thoroughfare_name").ToString.Trim
                                _PAFData._ThoroughfareDescriptor = _R("thoroughfare_descriptor").ToString.Trim
                                _PAFData._DoubleDependentLocality = _R("double_dependent_locality").ToString.Trim
                                _PAFData._DependentLocality = _R("dependent_locality").ToString.Trim
                                _PAFData._Town = _R("post_town").ToString.Trim
                                _PAFData._County = ""
                                _PAFData._Postcode = _R("postcode").ToString.Trim

                                _Return.Add(_PAFData)

                            End If

                        End If

                        _Count += 1

                    Next

                End While

            End If

            'dispose of reader
            _R.Close()

        End If

        'dispose of command
        _CMD.Dispose()

        'close connection
        _CN.Close()
        If _CN.State = System.Data.ConnectionState.Closed Then _CN.Dispose()

        Return _Return

    End Function

End Class
