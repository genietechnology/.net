﻿Public Class SplitAddress

    Public ReadOnly Property AddressFull As String
        Get
            Return m_AddressFull
        End Get
    End Property

    Public ReadOnly Property AddressLine As String
        Get
            Return m_AddressLine
        End Get
    End Property

    Public ReadOnly Property AddressLine1 As String
        Get
            Return m_AddressLine1
        End Get
    End Property

    Public ReadOnly Property AddressBlock As String
        Get
            Return m_Block
        End Get
    End Property

    Public ReadOnly Property AddressTown As String
        Get
            Return m_Town
        End Get
    End Property

    Public ReadOnly Property AddressCounty As String
        Get
            Return m_County
        End Get
    End Property

    Public ReadOnly Property AddressPostCode As String
        Get
            Return m_PostCode
        End Get
    End Property

    Public ReadOnly Property Populated As Boolean
        Get
            Return m_Populated
        End Get
    End Property

    Public ReadOnly Property CountyRequired As Boolean
        Get
            Return m_CountyRequired
        End Get
    End Property

    Private m_AddressFull As String = ""
    Private m_AddressLine As String = ""
    Private m_AddressLine1 As String = ""
    Private m_Populated As Boolean = False
    Private m_CountyRequired As Boolean = False
    Private m_Block As String = ""
    Private m_County As String = ""
    Private m_Town As String = ""
    Private m_PostCode As String = ""

    Public Sub New()


    End Sub

    Public Sub New(ByVal PAFDataIn As Core.PAFData)
        Convert(New FormattedAddress(PAFDataIn))
    End Sub

    Public Sub New(ByVal AddressIn As FormattedAddress)
        Convert(AddressIn)
    End Sub

    Private Sub Convert(ByRef Address As FormattedAddress)

        Dim _Splitter As String = ""

        If Address.PremiseIndependent Then
            _Splitter = vbNewLine
        Else
            _Splitter = " "
        End If

        m_Block += Address.Premise

        'next we append the double thoroughfare (if there is one)
        If Address.DoubleThoroughFare <> "" Then
            m_Block += _Splitter
            m_Block += Address.DoubleThoroughFare
            _Splitter = vbNewLine
        End If

        'append the thoroughfare
        If Address.ThoroughFare <> "" Then
            m_Block += _Splitter
            m_Block += Address.ThoroughFare
        End If

        'next we append the double locality (if there is one)
        If Address.DoubleLocality <> "" Then
            m_Block += vbNewLine
            m_Block += Address.DoubleLocality
        End If

        'append the locality
        If Address.Locality <> "" Then
            m_Block += vbNewLine
            m_Block += Address.Locality
        End If

        m_Town = Address.Town
        m_PostCode = Address.PostCode

        m_AddressFull = m_Block + vbNewLine + m_Town

        If Address.CountyRequired Then
            m_CountyRequired = True
            m_County = Address.County
            m_AddressFull += vbNewLine + Address.County
        End If

        m_AddressFull += vbNewLine + Address.PostCode
        m_AddressLine = Replace(m_AddressFull, vbCrLf, ", ")
        m_AddressLine1 = m_Block.Split(CType(vbCrLf, Char())).FirstOrDefault

        m_Populated = True

    End Sub

End Class
