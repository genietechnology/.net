﻿
Public Class FormattedAddress

    Public Property NumericPremise As Integer
    Public Property Premise As String
    Public Property PremiseIndependent As Boolean
    Public Property DoubleThoroughFare As String
    Public Property ThoroughFare As String
    Public Property DoubleLocality As String
    Public Property Locality As String
    Public Property Town As String
    Public Property County As String
    Public Property CountyRequired As Boolean
    Public Property PostCode As String

    Public Sub New()


    End Sub

    Public Sub New(ByVal PAF As Core.PAFData)
        ConvertPAF(PAF)
    End Sub

    Public Sub ConvertPAF(ByVal PAF As Core.PAFData)

        With Me
            .NumericPremise = ConvertNumericPremise(PAF._Premise)
            .Premise = PAF._Premise
            .PremiseIndependent = PAF._PremiseIndependent
            .DoubleThoroughFare = JoinLine(PAF._DoubleThoroughfare, PAF._DoubleThoroughfareDescriptor)
            .ThoroughFare = JoinLine(PAF._Thoroughfare, PAF._ThoroughfareDescriptor)
            .DoubleLocality = PAF._DoubleDependentLocality
            .Locality = PAF._DependentLocality
            .Town = PAF._Town
            .County = PAF._County
            .PostCode = FormatPostcode(PAF._Postcode)
        End With

    End Sub

    Private Function FormatPostcode(ByVal PostCodeIn As String) As String
        If PostCodeIn Is Nothing Then Return ""
        If PostCodeIn = "" Then Return ""
        If PostCodeIn.Length = 6 Then Return PostCodeIn.Substring(0, 3) + " " + PostCodeIn.Substring(3)
        If PostCodeIn.Length = 7 Then Return PostCodeIn.Substring(0, 4) + " " + PostCodeIn.Substring(4)
        Return PostCodeIn
    End Function

    Private Function ConvertNumericPremise(ByVal Premise As String) As Integer
        If IsNumeric(Premise) Then
            Return CInt(Premise)
        Else
            Return 0
        End If
    End Function

    Private Function JoinLine(ByVal Value1 As String, ByVal Value2 As String) As String
        If Value1 = "" AndAlso Value2 = "" Then Return ""
        If Value1 <> "" AndAlso Value2 = "" Then Return Value1
        Return Value1 + " " + Value2
    End Function

End Class